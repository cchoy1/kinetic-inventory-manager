//
// KineticInventoryManager
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 30/03/20
//

import Foundation
import SAPCommon
import SAPFiori
import SAPFoundation
import SAPOData

class AInbDeliveryItemTextTypeDetailViewController: FUIFormTableViewController, SAPFioriLoadingIndicator {
    var ec1: Ec1<OnlineODataProvider>!
    private var validity = [String: Bool]()
    var allowsEditableCells = false

    private var _entity: AInbDeliveryItemTextType?
    var entity: AInbDeliveryItemTextType {
        get {
            if self._entity == nil {
                self._entity = self.createEntityWithDefaultValues()
            }
            return self._entity!
        }
        set {
            self._entity = newValue
        }
    }

    private let logger = Logger.shared(named: "AInbDeliveryItemTextTypeMasterViewControllerLogger")
    var loadingIndicator: FUILoadingIndicatorView?
    var entityUpdater: CPIKIMODataEntityUpdaterDelegate?
    var tableUpdater: CPIKIMODataEntitySetUpdaterDelegate?
    private let okTitle = NSLocalizedString("keyOkButtonTitle",
                                            value: "OK",
                                            comment: "XBUT: Title of OK button.")
    var preventNavigationLoop = false
    var entitySetName: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "updateEntity" {
            // Show the Detail view with the current entity, where the properties scan be edited and updated
            self.logger.info("Showing a view to update the selected entity.")
            let dest = segue.destination as! UINavigationController
            let detailViewController = dest.viewControllers[0] as! AInbDeliveryItemTextTypeDetailViewController
            detailViewController.title = NSLocalizedString("keyUpdateEntityTitle", value: "Update Entity", comment: "XTIT: Title of update selected entity screen.")
            detailViewController.ec1 = self.ec1
            detailViewController.entity = self.entity
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: detailViewController, action: #selector(detailViewController.updateEntity))
            detailViewController.navigationItem.rightBarButtonItem = doneButton
            let cancelButton = UIBarButtonItem(title: NSLocalizedString("keyCancelButtonToGoPreviousScreen", value: "Cancel", comment: "XBUT: Title of Cancel button."), style: .plain, target: detailViewController, action: #selector(detailViewController.cancel))
            detailViewController.navigationItem.leftBarButtonItem = cancelButton
            detailViewController.allowsEditableCells = true
            detailViewController.entityUpdater = self
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return self.cellForDeliveryDocument(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemTextType.deliveryDocument)
        case 1:
            return self.cellForDeliveryDocumentItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemTextType.deliveryDocumentItem)
        case 2:
            return self.cellForTextElement(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemTextType.textElement)
        case 3:
            return self.cellForLanguage(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemTextType.language)
        case 4:
            return self.cellForTextElementDescription(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemTextType.textElementDescription)
        case 5:
            return self.cellForTextElementText(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemTextType.textElementText)
        default:
            return UITableViewCell()
        }
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 6
    }

    override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.preventNavigationLoop {
            AlertHelper.displayAlert(with: NSLocalizedString("keyAlertNavigationLoop", value: "No further navigation is possible.", comment: "XTIT: Title of alert message about preventing navigation loop."), error: nil, viewController: self)
            return
        }
        switch indexPath.row {
        default:
            return
        }
    }

    // MARK: - OData property specific cell creators

    private func cellForDeliveryDocument(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemTextType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocument {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocument = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemTextType.deliveryDocument.isOptional || newValue != "" {
                    currentEntity.deliveryDocument = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDocumentItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemTextType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocumentItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocumentItem = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemTextType.deliveryDocumentItem.isOptional || newValue != "" {
                    currentEntity.deliveryDocumentItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTextElement(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemTextType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.textElement {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.textElement = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemTextType.textElement.isOptional || newValue != "" {
                    currentEntity.textElement = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForLanguage(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemTextType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.language {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.language = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemTextType.language.isOptional || newValue != "" {
                    currentEntity.language = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTextElementDescription(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemTextType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.textElementDescription {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.textElementDescription = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemTextType.textElementDescription.isOptional || newValue != "" {
                    currentEntity.textElementDescription = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTextElementText(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemTextType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.textElementText {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.textElementText = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemTextType.textElementText.isOptional || newValue != "" {
                    currentEntity.textElementText = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    // MARK: - OData functionalities

    @objc func createEntity() {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Creating entity in backend.")
        self.ec1.createEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Create entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityCreationTitle", value: "Create entry failed", comment: "XTIT: Title of alert message about entity creation error."), error: error, viewController: self)
                return
            }
            self.logger.info("Create entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Created", comment: "XMSG: Title of alert message about successful entity creation."))
                    self.tableUpdater?.entitySetHasChanged()
                }
            }
        }
    }

    func createEntityWithDefaultValues() -> AInbDeliveryItemTextType {
        let newEntity = AInbDeliveryItemTextType()

        // Key properties without default value should be invalid by default for Create scenario
        if newEntity.deliveryDocument == nil || newEntity.deliveryDocument!.isEmpty {
            self.validity["DeliveryDocument"] = false
        }
        if newEntity.deliveryDocumentItem == nil || newEntity.deliveryDocumentItem!.isEmpty {
            self.validity["DeliveryDocumentItem"] = false
        }
        if newEntity.textElement == nil || newEntity.textElement!.isEmpty {
            self.validity["TextElement"] = false
        }
        if newEntity.language == nil || newEntity.language!.isEmpty {
            self.validity["Language"] = false
        }

        self.barButtonShouldBeEnabled()
        return newEntity
    }

    @objc func updateEntity(_: AnyObject) {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Updating entity in backend.")
        self.ec1.updateEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Update entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityUpdateTitle", value: "Update entry failed", comment: "XTIT: Title of alert message about entity update failure."), error: error, viewController: self)
                return
            }
            self.logger.info("Update entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyUpdateEntityFinishedTitle", value: "Updated", comment: "XTIT: Title of alert message about successful entity update."))
                    self.entityUpdater?.entityHasChanged(self.entity)
                }
            }
        }
    }

    // MARK: - other logic, helper

    @objc func cancel() {
        DispatchQueue.main.async {
            self.dismiss(animated: true)
        }
    }

    // Check if all text fields are valid
    private func barButtonShouldBeEnabled() {
        let anyFieldInvalid = self.validity.values.first { field in
            field == false
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = anyFieldInvalid == nil
    }
}

extension AInbDeliveryItemTextTypeDetailViewController: CPIKIMODataEntityUpdaterDelegate {
    func entityHasChanged(_ entityValue: EntityValue?) {
        if let entity = entityValue {
            let currentEntity = entity as! AInbDeliveryItemTextType
            self.entity = currentEntity
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
}
