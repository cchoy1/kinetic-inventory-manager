//
// KineticInventoryManager
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 30/03/20
//

import Foundation
import SAPCommon
import SAPFiori
import SAPFoundation
import SAPOData

class AInbDeliveryPartnerTypeDetailViewController: FUIFormTableViewController, SAPFioriLoadingIndicator {
    var ec1: Ec1<OnlineODataProvider>!
    private var validity = [String: Bool]()
    var allowsEditableCells = false

    private var _entity: AInbDeliveryPartnerType?
    var entity: AInbDeliveryPartnerType {
        get {
            if self._entity == nil {
                self._entity = self.createEntityWithDefaultValues()
            }
            return self._entity!
        }
        set {
            self._entity = newValue
        }
    }

    private let logger = Logger.shared(named: "AInbDeliveryPartnerTypeMasterViewControllerLogger")
    var loadingIndicator: FUILoadingIndicatorView?
    var entityUpdater: CPIKIMODataEntityUpdaterDelegate?
    var tableUpdater: CPIKIMODataEntitySetUpdaterDelegate?
    private let okTitle = NSLocalizedString("keyOkButtonTitle",
                                            value: "OK",
                                            comment: "XBUT: Title of OK button.")
    var preventNavigationLoop = false
    var entitySetName: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "updateEntity" {
            // Show the Detail view with the current entity, where the properties scan be edited and updated
            self.logger.info("Showing a view to update the selected entity.")
            let dest = segue.destination as! UINavigationController
            let detailViewController = dest.viewControllers[0] as! AInbDeliveryPartnerTypeDetailViewController
            detailViewController.title = NSLocalizedString("keyUpdateEntityTitle", value: "Update Entity", comment: "XTIT: Title of update selected entity screen.")
            detailViewController.ec1 = self.ec1
            detailViewController.entity = self.entity
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: detailViewController, action: #selector(detailViewController.updateEntity))
            detailViewController.navigationItem.rightBarButtonItem = doneButton
            let cancelButton = UIBarButtonItem(title: NSLocalizedString("keyCancelButtonToGoPreviousScreen", value: "Cancel", comment: "XBUT: Title of Cancel button."), style: .plain, target: detailViewController, action: #selector(detailViewController.cancel))
            detailViewController.navigationItem.leftBarButtonItem = cancelButton
            detailViewController.allowsEditableCells = true
            detailViewController.entityUpdater = self
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return self.cellForAddressID(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryPartnerType.addressID)
        case 1:
            return self.cellForContactPerson(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryPartnerType.contactPerson)
        case 2:
            return self.cellForCustomer(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryPartnerType.customer)
        case 3:
            return self.cellForPartnerFunction(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryPartnerType.partnerFunction)
        case 4:
            return self.cellForPersonnel(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryPartnerType.personnel)
        case 5:
            return self.cellForSdDocument(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryPartnerType.sdDocument)
        case 6:
            return self.cellForSdDocumentItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryPartnerType.sdDocumentItem)
        case 7:
            return self.cellForSupplier(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryPartnerType.supplier)
        default:
            return UITableViewCell()
        }
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 8
    }

    override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.preventNavigationLoop {
            AlertHelper.displayAlert(with: NSLocalizedString("keyAlertNavigationLoop", value: "No further navigation is possible.", comment: "XTIT: Title of alert message about preventing navigation loop."), error: nil, viewController: self)
            return
        }
        switch indexPath.row {
        default:
            return
        }
    }

    // MARK: - OData property specific cell creators

    private func cellForAddressID(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryPartnerType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.addressID {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.addressID = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryPartnerType.addressID.isOptional || newValue != "" {
                    currentEntity.addressID = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForContactPerson(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryPartnerType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.contactPerson {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.contactPerson = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryPartnerType.contactPerson.isOptional || newValue != "" {
                    currentEntity.contactPerson = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCustomer(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryPartnerType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.customer {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.customer = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryPartnerType.customer.isOptional || newValue != "" {
                    currentEntity.customer = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPartnerFunction(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryPartnerType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.partnerFunction {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.partnerFunction = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryPartnerType.partnerFunction.isOptional || newValue != "" {
                    currentEntity.partnerFunction = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPersonnel(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryPartnerType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.personnel {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.personnel = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryPartnerType.personnel.isOptional || newValue != "" {
                    currentEntity.personnel = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSdDocument(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryPartnerType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.sdDocument {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.sdDocument = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryPartnerType.sdDocument.isOptional || newValue != "" {
                    currentEntity.sdDocument = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSdDocumentItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryPartnerType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.sdDocumentItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.sdDocumentItem = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryPartnerType.sdDocumentItem.isOptional || newValue != "" {
                    currentEntity.sdDocumentItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSupplier(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryPartnerType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.supplier {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.supplier = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryPartnerType.supplier.isOptional || newValue != "" {
                    currentEntity.supplier = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    // MARK: - OData functionalities

    @objc func createEntity() {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Creating entity in backend.")
        self.ec1.createEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Create entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityCreationTitle", value: "Create entry failed", comment: "XTIT: Title of alert message about entity creation error."), error: error, viewController: self)
                return
            }
            self.logger.info("Create entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Created", comment: "XMSG: Title of alert message about successful entity creation."))
                    self.tableUpdater?.entitySetHasChanged()
                }
            }
        }
    }

    func createEntityWithDefaultValues() -> AInbDeliveryPartnerType {
        let newEntity = AInbDeliveryPartnerType()

        // Key properties without default value should be invalid by default for Create scenario
        if newEntity.partnerFunction == nil || newEntity.partnerFunction!.isEmpty {
            self.validity["PartnerFunction"] = false
        }
        if newEntity.sdDocument == nil || newEntity.sdDocument!.isEmpty {
            self.validity["SDDocument"] = false
        }

        self.barButtonShouldBeEnabled()
        return newEntity
    }

    @objc func updateEntity(_: AnyObject) {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Updating entity in backend.")
        self.ec1.updateEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Update entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityUpdateTitle", value: "Update entry failed", comment: "XTIT: Title of alert message about entity update failure."), error: error, viewController: self)
                return
            }
            self.logger.info("Update entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyUpdateEntityFinishedTitle", value: "Updated", comment: "XTIT: Title of alert message about successful entity update."))
                    self.entityUpdater?.entityHasChanged(self.entity)
                }
            }
        }
    }

    // MARK: - other logic, helper

    @objc func cancel() {
        DispatchQueue.main.async {
            self.dismiss(animated: true)
        }
    }

    // Check if all text fields are valid
    private func barButtonShouldBeEnabled() {
        let anyFieldInvalid = self.validity.values.first { field in
            field == false
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = anyFieldInvalid == nil
    }
}

extension AInbDeliveryPartnerTypeDetailViewController: CPIKIMODataEntityUpdaterDelegate {
    func entityHasChanged(_ entityValue: EntityValue?) {
        if let entity = entityValue {
            let currentEntity = entity as! AInbDeliveryPartnerType
            self.entity = currentEntity
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
}
