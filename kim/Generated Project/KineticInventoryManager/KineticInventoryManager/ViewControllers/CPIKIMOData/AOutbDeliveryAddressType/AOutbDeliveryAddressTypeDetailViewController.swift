//
// KineticInventoryManager
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 30/03/20
//

import Foundation
import SAPCommon
import SAPFiori
import SAPFoundation
import SAPOData

class AOutbDeliveryAddressTypeDetailViewController: FUIFormTableViewController, SAPFioriLoadingIndicator {
    var ec1: Ec1<OnlineODataProvider>!
    private var validity = [String: Bool]()
    var allowsEditableCells = false

    private var _entity: AOutbDeliveryAddressType?
    var entity: AOutbDeliveryAddressType {
        get {
            if self._entity == nil {
                self._entity = self.createEntityWithDefaultValues()
            }
            return self._entity!
        }
        set {
            self._entity = newValue
        }
    }

    private let logger = Logger.shared(named: "AOutbDeliveryAddressTypeMasterViewControllerLogger")
    var loadingIndicator: FUILoadingIndicatorView?
    var entityUpdater: CPIKIMODataEntityUpdaterDelegate?
    var tableUpdater: CPIKIMODataEntitySetUpdaterDelegate?
    private let okTitle = NSLocalizedString("keyOkButtonTitle",
                                            value: "OK",
                                            comment: "XBUT: Title of OK button.")
    var preventNavigationLoop = false
    var entitySetName: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "updateEntity" {
            // Show the Detail view with the current entity, where the properties scan be edited and updated
            self.logger.info("Showing a view to update the selected entity.")
            let dest = segue.destination as! UINavigationController
            let detailViewController = dest.viewControllers[0] as! AOutbDeliveryAddressTypeDetailViewController
            detailViewController.title = NSLocalizedString("keyUpdateEntityTitle", value: "Update Entity", comment: "XTIT: Title of update selected entity screen.")
            detailViewController.ec1 = self.ec1
            detailViewController.entity = self.entity
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: detailViewController, action: #selector(detailViewController.updateEntity))
            detailViewController.navigationItem.rightBarButtonItem = doneButton
            let cancelButton = UIBarButtonItem(title: NSLocalizedString("keyCancelButtonToGoPreviousScreen", value: "Cancel", comment: "XBUT: Title of Cancel button."), style: .plain, target: detailViewController, action: #selector(detailViewController.cancel))
            detailViewController.navigationItem.leftBarButtonItem = cancelButton
            detailViewController.allowsEditableCells = true
            detailViewController.entityUpdater = self
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return self.cellForAdditionalStreetPrefixName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.additionalStreetPrefixName)
        case 1:
            return self.cellForAdditionalStreetSuffixName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.additionalStreetSuffixName)
        case 2:
            return self.cellForAddressID(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.addressID)
        case 3:
            return self.cellForAddressTimeZone(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.addressTimeZone)
        case 4:
            return self.cellForBuilding(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.building)
        case 5:
            return self.cellForBusinessPartnerName1(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.businessPartnerName1)
        case 6:
            return self.cellForBusinessPartnerName2(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.businessPartnerName2)
        case 7:
            return self.cellForBusinessPartnerName3(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.businessPartnerName3)
        case 8:
            return self.cellForBusinessPartnerName4(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.businessPartnerName4)
        case 9:
            return self.cellForCareOfName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.careOfName)
        case 10:
            return self.cellForCityCode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.cityCode)
        case 11:
            return self.cellForCityName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.cityName)
        case 12:
            return self.cellForCitySearch(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.citySearch)
        case 13:
            return self.cellForCompanyPostalCode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.companyPostalCode)
        case 14:
            return self.cellForCorrespondenceLanguage(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.correspondenceLanguage)
        case 15:
            return self.cellForCountry(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.country)
        case 16:
            return self.cellForCounty(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.county)
        case 17:
            return self.cellForDeliveryServiceNumber(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.deliveryServiceNumber)
        case 18:
            return self.cellForDeliveryServiceTypeCode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.deliveryServiceTypeCode)
        case 19:
            return self.cellForDistrict(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.district)
        case 20:
            return self.cellForFaxNumber(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.faxNumber)
        case 21:
            return self.cellForFloor(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.floor)
        case 22:
            return self.cellForFormOfAddress(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.formOfAddress)
        case 23:
            return self.cellForFullName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.fullName)
        case 24:
            return self.cellForHomeCityName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.homeCityName)
        case 25:
            return self.cellForHouseNumber(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.houseNumber)
        case 26:
            return self.cellForHouseNumberSupplementText(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.houseNumberSupplementText)
        case 27:
            return self.cellForNation(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.nation)
        case 28:
            return self.cellForPerson(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.person)
        case 29:
            return self.cellForPhoneNumber(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.phoneNumber)
        case 30:
            return self.cellForPoBox(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.poBox)
        case 31:
            return self.cellForPoBoxDeviatingCityName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.poBoxDeviatingCityName)
        case 32:
            return self.cellForPoBoxDeviatingCountry(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.poBoxDeviatingCountry)
        case 33:
            return self.cellForPoBoxDeviatingRegion(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.poBoxDeviatingRegion)
        case 34:
            return self.cellForPoBoxIsWithoutNumber(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.poBoxIsWithoutNumber)
        case 35:
            return self.cellForPoBoxLobbyName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.poBoxLobbyName)
        case 36:
            return self.cellForPoBoxPostalCode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.poBoxPostalCode)
        case 37:
            return self.cellForPostalCode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.postalCode)
        case 38:
            return self.cellForPrfrdCommMediumType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.prfrdCommMediumType)
        case 39:
            return self.cellForRegion(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.region)
        case 40:
            return self.cellForRoomNumber(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.roomNumber)
        case 41:
            return self.cellForSearchTerm1(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.searchTerm1)
        case 42:
            return self.cellForStreetName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.streetName)
        case 43:
            return self.cellForStreetPrefixName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.streetPrefixName)
        case 44:
            return self.cellForStreetSearch(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.streetSearch)
        case 45:
            return self.cellForStreetSuffixName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.streetSuffixName)
        case 46:
            return self.cellForTaxJurisdiction(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.taxJurisdiction)
        case 47:
            return self.cellForTransportZone(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryAddressType.transportZone)
        default:
            return UITableViewCell()
        }
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 48
    }

    override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.preventNavigationLoop {
            AlertHelper.displayAlert(with: NSLocalizedString("keyAlertNavigationLoop", value: "No further navigation is possible.", comment: "XTIT: Title of alert message about preventing navigation loop."), error: nil, viewController: self)
            return
        }
        switch indexPath.row {
        default:
            return
        }
    }

    // MARK: - OData property specific cell creators

    private func cellForAdditionalStreetPrefixName(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalStreetPrefixName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalStreetPrefixName = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.additionalStreetPrefixName.isOptional || newValue != "" {
                    currentEntity.additionalStreetPrefixName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalStreetSuffixName(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalStreetSuffixName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalStreetSuffixName = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.additionalStreetSuffixName.isOptional || newValue != "" {
                    currentEntity.additionalStreetSuffixName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAddressID(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.addressID {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.addressID = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.addressID.isOptional || newValue != "" {
                    currentEntity.addressID = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAddressTimeZone(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.addressTimeZone {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.addressTimeZone = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.addressTimeZone.isOptional || newValue != "" {
                    currentEntity.addressTimeZone = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBuilding(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.building {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.building = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.building.isOptional || newValue != "" {
                    currentEntity.building = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBusinessPartnerName1(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.businessPartnerName1 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.businessPartnerName1 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.businessPartnerName1.isOptional || newValue != "" {
                    currentEntity.businessPartnerName1 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBusinessPartnerName2(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.businessPartnerName2 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.businessPartnerName2 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.businessPartnerName2.isOptional || newValue != "" {
                    currentEntity.businessPartnerName2 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBusinessPartnerName3(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.businessPartnerName3 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.businessPartnerName3 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.businessPartnerName3.isOptional || newValue != "" {
                    currentEntity.businessPartnerName3 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBusinessPartnerName4(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.businessPartnerName4 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.businessPartnerName4 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.businessPartnerName4.isOptional || newValue != "" {
                    currentEntity.businessPartnerName4 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCareOfName(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.careOfName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.careOfName = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.careOfName.isOptional || newValue != "" {
                    currentEntity.careOfName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCityCode(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.cityCode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.cityCode = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.cityCode.isOptional || newValue != "" {
                    currentEntity.cityCode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCityName(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.cityName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.cityName = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.cityName.isOptional || newValue != "" {
                    currentEntity.cityName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCitySearch(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.citySearch {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.citySearch = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.citySearch.isOptional || newValue != "" {
                    currentEntity.citySearch = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCompanyPostalCode(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.companyPostalCode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.companyPostalCode = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.companyPostalCode.isOptional || newValue != "" {
                    currentEntity.companyPostalCode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCorrespondenceLanguage(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.correspondenceLanguage {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.correspondenceLanguage = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.correspondenceLanguage.isOptional || newValue != "" {
                    currentEntity.correspondenceLanguage = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCountry(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.country {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.country = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.country.isOptional || newValue != "" {
                    currentEntity.country = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCounty(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.county {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.county = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.county.isOptional || newValue != "" {
                    currentEntity.county = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryServiceNumber(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryServiceNumber {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryServiceNumber = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.deliveryServiceNumber.isOptional || newValue != "" {
                    currentEntity.deliveryServiceNumber = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryServiceTypeCode(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryServiceTypeCode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryServiceTypeCode = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.deliveryServiceTypeCode.isOptional || newValue != "" {
                    currentEntity.deliveryServiceTypeCode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDistrict(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.district {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.district = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.district.isOptional || newValue != "" {
                    currentEntity.district = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForFaxNumber(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.faxNumber {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.faxNumber = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.faxNumber.isOptional || newValue != "" {
                    currentEntity.faxNumber = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForFloor(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.floor {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.floor = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.floor.isOptional || newValue != "" {
                    currentEntity.floor = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForFormOfAddress(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.formOfAddress {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.formOfAddress = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.formOfAddress.isOptional || newValue != "" {
                    currentEntity.formOfAddress = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForFullName(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.fullName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.fullName = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.fullName.isOptional || newValue != "" {
                    currentEntity.fullName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHomeCityName(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.homeCityName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.homeCityName = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.homeCityName.isOptional || newValue != "" {
                    currentEntity.homeCityName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHouseNumber(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.houseNumber {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.houseNumber = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.houseNumber.isOptional || newValue != "" {
                    currentEntity.houseNumber = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHouseNumberSupplementText(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.houseNumberSupplementText {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.houseNumberSupplementText = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.houseNumberSupplementText.isOptional || newValue != "" {
                    currentEntity.houseNumberSupplementText = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForNation(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.nation {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.nation = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.nation.isOptional || newValue != "" {
                    currentEntity.nation = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPerson(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.person {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.person = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.person.isOptional || newValue != "" {
                    currentEntity.person = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPhoneNumber(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.phoneNumber {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.phoneNumber = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.phoneNumber.isOptional || newValue != "" {
                    currentEntity.phoneNumber = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPoBox(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.poBox {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.poBox = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.poBox.isOptional || newValue != "" {
                    currentEntity.poBox = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPoBoxDeviatingCityName(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.poBoxDeviatingCityName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.poBoxDeviatingCityName = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.poBoxDeviatingCityName.isOptional || newValue != "" {
                    currentEntity.poBoxDeviatingCityName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPoBoxDeviatingCountry(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.poBoxDeviatingCountry {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.poBoxDeviatingCountry = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.poBoxDeviatingCountry.isOptional || newValue != "" {
                    currentEntity.poBoxDeviatingCountry = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPoBoxDeviatingRegion(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.poBoxDeviatingRegion {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.poBoxDeviatingRegion = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.poBoxDeviatingRegion.isOptional || newValue != "" {
                    currentEntity.poBoxDeviatingRegion = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPoBoxIsWithoutNumber(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.poBoxIsWithoutNumber {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.poBoxIsWithoutNumber = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.poBoxIsWithoutNumber = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPoBoxLobbyName(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.poBoxLobbyName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.poBoxLobbyName = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.poBoxLobbyName.isOptional || newValue != "" {
                    currentEntity.poBoxLobbyName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPoBoxPostalCode(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.poBoxPostalCode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.poBoxPostalCode = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.poBoxPostalCode.isOptional || newValue != "" {
                    currentEntity.poBoxPostalCode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPostalCode(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.postalCode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.postalCode = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.postalCode.isOptional || newValue != "" {
                    currentEntity.postalCode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPrfrdCommMediumType(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.prfrdCommMediumType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.prfrdCommMediumType = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.prfrdCommMediumType.isOptional || newValue != "" {
                    currentEntity.prfrdCommMediumType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForRegion(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.region {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.region = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.region.isOptional || newValue != "" {
                    currentEntity.region = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForRoomNumber(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.roomNumber {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.roomNumber = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.roomNumber.isOptional || newValue != "" {
                    currentEntity.roomNumber = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSearchTerm1(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.searchTerm1 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.searchTerm1 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.searchTerm1.isOptional || newValue != "" {
                    currentEntity.searchTerm1 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStreetName(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.streetName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.streetName = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.streetName.isOptional || newValue != "" {
                    currentEntity.streetName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStreetPrefixName(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.streetPrefixName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.streetPrefixName = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.streetPrefixName.isOptional || newValue != "" {
                    currentEntity.streetPrefixName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStreetSearch(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.streetSearch {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.streetSearch = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.streetSearch.isOptional || newValue != "" {
                    currentEntity.streetSearch = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStreetSuffixName(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.streetSuffixName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.streetSuffixName = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.streetSuffixName.isOptional || newValue != "" {
                    currentEntity.streetSuffixName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTaxJurisdiction(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.taxJurisdiction {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.taxJurisdiction = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.taxJurisdiction.isOptional || newValue != "" {
                    currentEntity.taxJurisdiction = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTransportZone(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryAddressType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.transportZone {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.transportZone = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryAddressType.transportZone.isOptional || newValue != "" {
                    currentEntity.transportZone = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    // MARK: - OData functionalities

    @objc func createEntity() {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Creating entity in backend.")
        self.ec1.createEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Create entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityCreationTitle", value: "Create entry failed", comment: "XTIT: Title of alert message about entity creation error."), error: error, viewController: self)
                return
            }
            self.logger.info("Create entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Created", comment: "XMSG: Title of alert message about successful entity creation."))
                    self.tableUpdater?.entitySetHasChanged()
                }
            }
        }
    }

    func createEntityWithDefaultValues() -> AOutbDeliveryAddressType {
        let newEntity = AOutbDeliveryAddressType()

        // Key properties without default value should be invalid by default for Create scenario
        if newEntity.addressID == nil || newEntity.addressID!.isEmpty {
            self.validity["AddressID"] = false
        }

        self.barButtonShouldBeEnabled()
        return newEntity
    }

    @objc func updateEntity(_: AnyObject) {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Updating entity in backend.")
        self.ec1.updateEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Update entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityUpdateTitle", value: "Update entry failed", comment: "XTIT: Title of alert message about entity update failure."), error: error, viewController: self)
                return
            }
            self.logger.info("Update entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyUpdateEntityFinishedTitle", value: "Updated", comment: "XTIT: Title of alert message about successful entity update."))
                    self.entityUpdater?.entityHasChanged(self.entity)
                }
            }
        }
    }

    // MARK: - other logic, helper

    @objc func cancel() {
        DispatchQueue.main.async {
            self.dismiss(animated: true)
        }
    }

    // Check if all text fields are valid
    private func barButtonShouldBeEnabled() {
        let anyFieldInvalid = self.validity.values.first { field in
            field == false
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = anyFieldInvalid == nil
    }
}

extension AOutbDeliveryAddressTypeDetailViewController: CPIKIMODataEntityUpdaterDelegate {
    func entityHasChanged(_ entityValue: EntityValue?) {
        if let entity = entityValue {
            let currentEntity = entity as! AOutbDeliveryAddressType
            self.entity = currentEntity
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
}
