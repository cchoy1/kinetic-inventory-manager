//
// KineticInventoryManager
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 30/03/20
//

import Foundation
import SAPCommon
import SAPFiori
import SAPFoundation
import SAPOData

class AInbDeliveryHeaderTypeDetailViewController: FUIFormTableViewController, SAPFioriLoadingIndicator {
    var ec1: Ec1<OnlineODataProvider>!
    private var validity = [String: Bool]()
    var allowsEditableCells = false

    private var _entity: AInbDeliveryHeaderType?
    var entity: AInbDeliveryHeaderType {
        get {
            if self._entity == nil {
                self._entity = self.createEntityWithDefaultValues()
            }
            return self._entity!
        }
        set {
            self._entity = newValue
        }
    }

    private let logger = Logger.shared(named: "AInbDeliveryHeaderTypeMasterViewControllerLogger")
    var loadingIndicator: FUILoadingIndicatorView?
    var entityUpdater: CPIKIMODataEntityUpdaterDelegate?
    var tableUpdater: CPIKIMODataEntitySetUpdaterDelegate?
    private let okTitle = NSLocalizedString("keyOkButtonTitle",
                                            value: "OK",
                                            comment: "XBUT: Title of OK button.")
    var preventNavigationLoop = false
    var entitySetName: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "updateEntity" {
            // Show the Detail view with the current entity, where the properties scan be edited and updated
            self.logger.info("Showing a view to update the selected entity.")
            let dest = segue.destination as! UINavigationController
            let detailViewController = dest.viewControllers[0] as! AInbDeliveryHeaderTypeDetailViewController
            detailViewController.title = NSLocalizedString("keyUpdateEntityTitle", value: "Update Entity", comment: "XTIT: Title of update selected entity screen.")
            detailViewController.ec1 = self.ec1
            detailViewController.entity = self.entity
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: detailViewController, action: #selector(detailViewController.updateEntity))
            detailViewController.navigationItem.rightBarButtonItem = doneButton
            let cancelButton = UIBarButtonItem(title: NSLocalizedString("keyCancelButtonToGoPreviousScreen", value: "Cancel", comment: "XBUT: Title of Cancel button."), style: .plain, target: detailViewController, action: #selector(detailViewController.cancel))
            detailViewController.navigationItem.leftBarButtonItem = cancelButton
            detailViewController.allowsEditableCells = true
            detailViewController.entityUpdater = self
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return self.cellForActualDeliveryRoute(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.actualDeliveryRoute)
        case 1:
            return self.cellForActualGoodsMovementDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.actualGoodsMovementDate)
        case 2:
            return self.cellForBillingDocumentDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.billingDocumentDate)
        case 3:
            return self.cellForBillOfLading(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.billOfLading)
        case 4:
            return self.cellForCompleteDeliveryIsDefined(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.completeDeliveryIsDefined)
        case 5:
            return self.cellForCreatedByUser(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.createdByUser)
        case 6:
            return self.cellForCreationDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.creationDate)
        case 7:
            return self.cellForCustomerGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.customerGroup)
        case 8:
            return self.cellForDeliveryBlockReason(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.deliveryBlockReason)
        case 9:
            return self.cellForDeliveryDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.deliveryDate)
        case 10:
            return self.cellForDeliveryDocument(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.deliveryDocument)
        case 11:
            return self.cellForDeliveryDocumentBySupplier(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.deliveryDocumentBySupplier)
        case 12:
            return self.cellForDeliveryDocumentType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.deliveryDocumentType)
        case 13:
            return self.cellForDeliveryIsInPlant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.deliveryIsInPlant)
        case 14:
            return self.cellForDeliveryPriority(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.deliveryPriority)
        case 15:
            return self.cellForDeliveryVersion(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.deliveryVersion)
        case 16:
            return self.cellForDepreciationPercentage(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.depreciationPercentage)
        case 17:
            return self.cellForDistrStatusByDecentralizedWrhs(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.distrStatusByDecentralizedWrhs)
        case 18:
            return self.cellForDocumentDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.documentDate)
        case 19:
            return self.cellForExternalIdentificationType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.externalIdentificationType)
        case 20:
            return self.cellForExternalTransportSystem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.externalTransportSystem)
        case 21:
            return self.cellForFactoryCalendarByCustomer(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.factoryCalendarByCustomer)
        case 22:
            return self.cellForGoodsIssueOrReceiptSlipNumber(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber)
        case 23:
            return self.cellForHandlingUnitInStock(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.handlingUnitInStock)
        case 24:
            return self.cellForHdrGeneralIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.hdrGeneralIncompletionStatus)
        case 25:
            return self.cellForHdrGoodsMvtIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus)
        case 26:
            return self.cellForHeaderBillgIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.headerBillgIncompletionStatus)
        case 27:
            return self.cellForHeaderBillingBlockReason(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.headerBillingBlockReason)
        case 28:
            return self.cellForHeaderDelivIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.headerDelivIncompletionStatus)
        case 29:
            return self.cellForHeaderGrossWeight(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.headerGrossWeight)
        case 30:
            return self.cellForHeaderNetWeight(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.headerNetWeight)
        case 31:
            return self.cellForHeaderPackingIncompletionSts(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.headerPackingIncompletionSts)
        case 32:
            return self.cellForHeaderPickgIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.headerPickgIncompletionStatus)
        case 33:
            return self.cellForHeaderVolume(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.headerVolume)
        case 34:
            return self.cellForHeaderVolumeUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.headerVolumeUnit)
        case 35:
            return self.cellForHeaderWeightUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.headerWeightUnit)
        case 36:
            return self.cellForIncotermsClassification(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.incotermsClassification)
        case 37:
            return self.cellForIncotermsTransferLocation(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.incotermsTransferLocation)
        case 38:
            return self.cellForIntercompanyBillingDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.intercompanyBillingDate)
        case 39:
            return self.cellForInternalFinancialDocument(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.internalFinancialDocument)
        case 40:
            return self.cellForIsDeliveryForSingleWarehouse(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.isDeliveryForSingleWarehouse)
        case 41:
            return self.cellForIsExportDelivery(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.isExportDelivery)
        case 42:
            return self.cellForLastChangeDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.lastChangeDate)
        case 43:
            return self.cellForLastChangedByUser(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.lastChangedByUser)
        case 44:
            return self.cellForLoadingDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.loadingDate)
        case 45:
            return self.cellForLoadingPoint(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.loadingPoint)
        case 46:
            return self.cellForMeansOfTransport(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.meansOfTransport)
        case 47:
            return self.cellForMeansOfTransportRefMaterial(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.meansOfTransportRefMaterial)
        case 48:
            return self.cellForMeansOfTransportType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.meansOfTransportType)
        case 49:
            return self.cellForOrderCombinationIsAllowed(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.orderCombinationIsAllowed)
        case 50:
            return self.cellForOrderID(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.orderID)
        case 51:
            return self.cellForOverallDelivConfStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.overallDelivConfStatus)
        case 52:
            return self.cellForOverallDelivReltdBillgStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.overallDelivReltdBillgStatus)
        case 53:
            return self.cellForOverallGoodsMovementStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.overallGoodsMovementStatus)
        case 54:
            return self.cellForOverallIntcoBillingStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.overallIntcoBillingStatus)
        case 55:
            return self.cellForOverallPackingStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.overallPackingStatus)
        case 56:
            return self.cellForOverallPickingConfStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.overallPickingConfStatus)
        case 57:
            return self.cellForOverallPickingStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.overallPickingStatus)
        case 58:
            return self.cellForOverallProofOfDeliveryStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.overallProofOfDeliveryStatus)
        case 59:
            return self.cellForOverallSDProcessStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.overallSDProcessStatus)
        case 60:
            return self.cellForOverallWarehouseActivityStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.overallWarehouseActivityStatus)
        case 61:
            return self.cellForOvrlItmDelivIncompletionSts(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.ovrlItmDelivIncompletionSts)
        case 62:
            return self.cellForOvrlItmGdsMvtIncompletionSts(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts)
        case 63:
            return self.cellForOvrlItmGeneralIncompletionSts(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.ovrlItmGeneralIncompletionSts)
        case 64:
            return self.cellForOvrlItmPackingIncompletionSts(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.ovrlItmPackingIncompletionSts)
        case 65:
            return self.cellForOvrlItmPickingIncompletionSts(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.ovrlItmPickingIncompletionSts)
        case 66:
            return self.cellForPaymentGuaranteeProcedure(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.paymentGuaranteeProcedure)
        case 67:
            return self.cellForPickedItemsLocation(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.pickedItemsLocation)
        case 68:
            return self.cellForPickingDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.pickingDate)
        case 69:
            return self.cellForPlannedGoodsIssueDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.plannedGoodsIssueDate)
        case 70:
            return self.cellForProofOfDeliveryDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.proofOfDeliveryDate)
        case 71:
            return self.cellForProposedDeliveryRoute(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.proposedDeliveryRoute)
        case 72:
            return self.cellForReceivingPlant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.receivingPlant)
        case 73:
            return self.cellForRouteSchedule(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.routeSchedule)
        case 74:
            return self.cellForSalesDistrict(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.salesDistrict)
        case 75:
            return self.cellForSalesOffice(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.salesOffice)
        case 76:
            return self.cellForSalesOrganization(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.salesOrganization)
        case 77:
            return self.cellForSdDocumentCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.sdDocumentCategory)
        case 78:
            return self.cellForShipmentBlockReason(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.shipmentBlockReason)
        case 79:
            return self.cellForShippingCondition(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.shippingCondition)
        case 80:
            return self.cellForShippingPoint(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.shippingPoint)
        case 81:
            return self.cellForShippingType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.shippingType)
        case 82:
            return self.cellForShipToParty(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.shipToParty)
        case 83:
            return self.cellForSoldToParty(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.soldToParty)
        case 84:
            return self.cellForSpecialProcessingCode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.specialProcessingCode)
        case 85:
            return self.cellForStatisticsCurrency(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.statisticsCurrency)
        case 86:
            return self.cellForSupplier(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.supplier)
        case 87:
            return self.cellForTotalBlockStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.totalBlockStatus)
        case 88:
            return self.cellForTotalCreditCheckStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.totalCreditCheckStatus)
        case 89:
            return self.cellForTotalNumberOfPackage(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.totalNumberOfPackage)
        case 90:
            return self.cellForTransactionCurrency(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.transactionCurrency)
        case 91:
            return self.cellForTransportationGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.transportationGroup)
        case 92:
            return self.cellForTransportationPlanningDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.transportationPlanningDate)
        case 93:
            return self.cellForTransportationPlanningStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.transportationPlanningStatus)
        case 94:
            return self.cellForUnloadingPointName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.unloadingPointName)
        case 95:
            return self.cellForWarehouse(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.warehouse)
        case 96:
            return self.cellForWarehouseGate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.warehouseGate)
        case 97:
            return self.cellForWarehouseStagingArea(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryHeaderType.warehouseStagingArea)
        default:
            return UITableViewCell()
        }
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 98
    }

    override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.preventNavigationLoop {
            AlertHelper.displayAlert(with: NSLocalizedString("keyAlertNavigationLoop", value: "No further navigation is possible.", comment: "XTIT: Title of alert message about preventing navigation loop."), error: nil, viewController: self)
            return
        }
        switch indexPath.row {
        default:
            return
        }
    }

    // MARK: - OData property specific cell creators

    private func cellForActualDeliveryRoute(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.actualDeliveryRoute {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.actualDeliveryRoute = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.actualDeliveryRoute.isOptional || newValue != "" {
                    currentEntity.actualDeliveryRoute = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForActualGoodsMovementDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.actualGoodsMovementDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.actualGoodsMovementDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.actualGoodsMovementDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBillingDocumentDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.billingDocumentDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.billingDocumentDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.billingDocumentDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBillOfLading(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.billOfLading {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.billOfLading = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.billOfLading.isOptional || newValue != "" {
                    currentEntity.billOfLading = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCompleteDeliveryIsDefined(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.completeDeliveryIsDefined {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.completeDeliveryIsDefined = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.completeDeliveryIsDefined = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCreatedByUser(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.createdByUser {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.createdByUser = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.createdByUser.isOptional || newValue != "" {
                    currentEntity.createdByUser = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCreationDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.creationDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.creationDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.creationDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCustomerGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.customerGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.customerGroup = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.customerGroup.isOptional || newValue != "" {
                    currentEntity.customerGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryBlockReason(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryBlockReason {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryBlockReason = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.deliveryBlockReason.isOptional || newValue != "" {
                    currentEntity.deliveryBlockReason = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.deliveryDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDocument(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocument {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocument = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.deliveryDocument.isOptional || newValue != "" {
                    currentEntity.deliveryDocument = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDocumentBySupplier(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocumentBySupplier {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocumentBySupplier = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.deliveryDocumentBySupplier.isOptional || newValue != "" {
                    currentEntity.deliveryDocumentBySupplier = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDocumentType(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocumentType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocumentType = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.deliveryDocumentType.isOptional || newValue != "" {
                    currentEntity.deliveryDocumentType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryIsInPlant(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryIsInPlant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryIsInPlant = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.deliveryIsInPlant = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryPriority(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryPriority {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryPriority = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.deliveryPriority.isOptional || newValue != "" {
                    currentEntity.deliveryPriority = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryVersion(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryVersion {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryVersion = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.deliveryVersion.isOptional || newValue != "" {
                    currentEntity.deliveryVersion = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDepreciationPercentage(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.depreciationPercentage {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.depreciationPercentage = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.depreciationPercentage = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDistrStatusByDecentralizedWrhs(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.distrStatusByDecentralizedWrhs {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.distrStatusByDecentralizedWrhs = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.distrStatusByDecentralizedWrhs.isOptional || newValue != "" {
                    currentEntity.distrStatusByDecentralizedWrhs = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDocumentDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.documentDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.documentDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.documentDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForExternalIdentificationType(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.externalIdentificationType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.externalIdentificationType = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.externalIdentificationType.isOptional || newValue != "" {
                    currentEntity.externalIdentificationType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForExternalTransportSystem(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.externalTransportSystem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.externalTransportSystem = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.externalTransportSystem.isOptional || newValue != "" {
                    currentEntity.externalTransportSystem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForFactoryCalendarByCustomer(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.factoryCalendarByCustomer {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.factoryCalendarByCustomer = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.factoryCalendarByCustomer.isOptional || newValue != "" {
                    currentEntity.factoryCalendarByCustomer = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGoodsIssueOrReceiptSlipNumber(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.goodsIssueOrReceiptSlipNumber {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.goodsIssueOrReceiptSlipNumber = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber.isOptional || newValue != "" {
                    currentEntity.goodsIssueOrReceiptSlipNumber = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHandlingUnitInStock(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.handlingUnitInStock {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.handlingUnitInStock = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.handlingUnitInStock.isOptional || newValue != "" {
                    currentEntity.handlingUnitInStock = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHdrGeneralIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.hdrGeneralIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.hdrGeneralIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.hdrGeneralIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.hdrGeneralIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHdrGoodsMvtIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.hdrGoodsMvtIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.hdrGoodsMvtIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.hdrGoodsMvtIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHeaderBillgIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.headerBillgIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.headerBillgIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.headerBillgIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.headerBillgIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHeaderBillingBlockReason(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.headerBillingBlockReason {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.headerBillingBlockReason = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.headerBillingBlockReason.isOptional || newValue != "" {
                    currentEntity.headerBillingBlockReason = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHeaderDelivIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.headerDelivIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.headerDelivIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.headerDelivIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.headerDelivIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHeaderGrossWeight(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.headerGrossWeight {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.headerGrossWeight = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.headerGrossWeight = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHeaderNetWeight(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.headerNetWeight {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.headerNetWeight = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.headerNetWeight = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHeaderPackingIncompletionSts(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.headerPackingIncompletionSts {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.headerPackingIncompletionSts = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.headerPackingIncompletionSts.isOptional || newValue != "" {
                    currentEntity.headerPackingIncompletionSts = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHeaderPickgIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.headerPickgIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.headerPickgIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.headerPickgIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.headerPickgIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHeaderVolume(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.headerVolume {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.headerVolume = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.headerVolume = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHeaderVolumeUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.headerVolumeUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.headerVolumeUnit = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.headerVolumeUnit.isOptional || newValue != "" {
                    currentEntity.headerVolumeUnit = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHeaderWeightUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.headerWeightUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.headerWeightUnit = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.headerWeightUnit.isOptional || newValue != "" {
                    currentEntity.headerWeightUnit = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIncotermsClassification(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.incotermsClassification {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.incotermsClassification = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.incotermsClassification.isOptional || newValue != "" {
                    currentEntity.incotermsClassification = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIncotermsTransferLocation(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.incotermsTransferLocation {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.incotermsTransferLocation = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.incotermsTransferLocation.isOptional || newValue != "" {
                    currentEntity.incotermsTransferLocation = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIntercompanyBillingDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.intercompanyBillingDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.intercompanyBillingDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.intercompanyBillingDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInternalFinancialDocument(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.internalFinancialDocument {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.internalFinancialDocument = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.internalFinancialDocument.isOptional || newValue != "" {
                    currentEntity.internalFinancialDocument = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIsDeliveryForSingleWarehouse(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.isDeliveryForSingleWarehouse {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.isDeliveryForSingleWarehouse = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.isDeliveryForSingleWarehouse.isOptional || newValue != "" {
                    currentEntity.isDeliveryForSingleWarehouse = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIsExportDelivery(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.isExportDelivery {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.isExportDelivery = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.isExportDelivery.isOptional || newValue != "" {
                    currentEntity.isExportDelivery = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForLastChangeDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.lastChangeDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.lastChangeDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.lastChangeDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForLastChangedByUser(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.lastChangedByUser {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.lastChangedByUser = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.lastChangedByUser.isOptional || newValue != "" {
                    currentEntity.lastChangedByUser = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForLoadingDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.loadingDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.loadingDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.loadingDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForLoadingPoint(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.loadingPoint {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.loadingPoint = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.loadingPoint.isOptional || newValue != "" {
                    currentEntity.loadingPoint = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMeansOfTransport(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.meansOfTransport {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.meansOfTransport = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.meansOfTransport.isOptional || newValue != "" {
                    currentEntity.meansOfTransport = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMeansOfTransportRefMaterial(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.meansOfTransportRefMaterial {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.meansOfTransportRefMaterial = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.meansOfTransportRefMaterial.isOptional || newValue != "" {
                    currentEntity.meansOfTransportRefMaterial = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMeansOfTransportType(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.meansOfTransportType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.meansOfTransportType = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.meansOfTransportType.isOptional || newValue != "" {
                    currentEntity.meansOfTransportType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOrderCombinationIsAllowed(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.orderCombinationIsAllowed {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.orderCombinationIsAllowed = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.orderCombinationIsAllowed = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOrderID(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.orderID {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.orderID = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.orderID.isOptional || newValue != "" {
                    currentEntity.orderID = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOverallDelivConfStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.overallDelivConfStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.overallDelivConfStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.overallDelivConfStatus.isOptional || newValue != "" {
                    currentEntity.overallDelivConfStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOverallDelivReltdBillgStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.overallDelivReltdBillgStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.overallDelivReltdBillgStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.overallDelivReltdBillgStatus.isOptional || newValue != "" {
                    currentEntity.overallDelivReltdBillgStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOverallGoodsMovementStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.overallGoodsMovementStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.overallGoodsMovementStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.overallGoodsMovementStatus.isOptional || newValue != "" {
                    currentEntity.overallGoodsMovementStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOverallIntcoBillingStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.overallIntcoBillingStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.overallIntcoBillingStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.overallIntcoBillingStatus.isOptional || newValue != "" {
                    currentEntity.overallIntcoBillingStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOverallPackingStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.overallPackingStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.overallPackingStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.overallPackingStatus.isOptional || newValue != "" {
                    currentEntity.overallPackingStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOverallPickingConfStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.overallPickingConfStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.overallPickingConfStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.overallPickingConfStatus.isOptional || newValue != "" {
                    currentEntity.overallPickingConfStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOverallPickingStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.overallPickingStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.overallPickingStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.overallPickingStatus.isOptional || newValue != "" {
                    currentEntity.overallPickingStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOverallProofOfDeliveryStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.overallProofOfDeliveryStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.overallProofOfDeliveryStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.overallProofOfDeliveryStatus.isOptional || newValue != "" {
                    currentEntity.overallProofOfDeliveryStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOverallSDProcessStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.overallSDProcessStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.overallSDProcessStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.overallSDProcessStatus.isOptional || newValue != "" {
                    currentEntity.overallSDProcessStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOverallWarehouseActivityStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.overallWarehouseActivityStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.overallWarehouseActivityStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.overallWarehouseActivityStatus.isOptional || newValue != "" {
                    currentEntity.overallWarehouseActivityStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOvrlItmDelivIncompletionSts(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.ovrlItmDelivIncompletionSts {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.ovrlItmDelivIncompletionSts = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.ovrlItmDelivIncompletionSts.isOptional || newValue != "" {
                    currentEntity.ovrlItmDelivIncompletionSts = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOvrlItmGdsMvtIncompletionSts(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.ovrlItmGdsMvtIncompletionSts {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.ovrlItmGdsMvtIncompletionSts = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts.isOptional || newValue != "" {
                    currentEntity.ovrlItmGdsMvtIncompletionSts = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOvrlItmGeneralIncompletionSts(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.ovrlItmGeneralIncompletionSts {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.ovrlItmGeneralIncompletionSts = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.ovrlItmGeneralIncompletionSts.isOptional || newValue != "" {
                    currentEntity.ovrlItmGeneralIncompletionSts = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOvrlItmPackingIncompletionSts(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.ovrlItmPackingIncompletionSts {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.ovrlItmPackingIncompletionSts = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.ovrlItmPackingIncompletionSts.isOptional || newValue != "" {
                    currentEntity.ovrlItmPackingIncompletionSts = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOvrlItmPickingIncompletionSts(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.ovrlItmPickingIncompletionSts {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.ovrlItmPickingIncompletionSts = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.ovrlItmPickingIncompletionSts.isOptional || newValue != "" {
                    currentEntity.ovrlItmPickingIncompletionSts = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPaymentGuaranteeProcedure(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.paymentGuaranteeProcedure {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.paymentGuaranteeProcedure = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.paymentGuaranteeProcedure.isOptional || newValue != "" {
                    currentEntity.paymentGuaranteeProcedure = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPickedItemsLocation(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.pickedItemsLocation {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.pickedItemsLocation = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.pickedItemsLocation.isOptional || newValue != "" {
                    currentEntity.pickedItemsLocation = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPickingDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.pickingDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.pickingDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.pickingDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPlannedGoodsIssueDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.plannedGoodsIssueDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.plannedGoodsIssueDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.plannedGoodsIssueDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProofOfDeliveryDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.proofOfDeliveryDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.proofOfDeliveryDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.proofOfDeliveryDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProposedDeliveryRoute(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.proposedDeliveryRoute {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.proposedDeliveryRoute = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.proposedDeliveryRoute.isOptional || newValue != "" {
                    currentEntity.proposedDeliveryRoute = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReceivingPlant(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.receivingPlant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.receivingPlant = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.receivingPlant.isOptional || newValue != "" {
                    currentEntity.receivingPlant = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForRouteSchedule(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.routeSchedule {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.routeSchedule = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.routeSchedule.isOptional || newValue != "" {
                    currentEntity.routeSchedule = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSalesDistrict(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.salesDistrict {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.salesDistrict = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.salesDistrict.isOptional || newValue != "" {
                    currentEntity.salesDistrict = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSalesOffice(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.salesOffice {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.salesOffice = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.salesOffice.isOptional || newValue != "" {
                    currentEntity.salesOffice = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSalesOrganization(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.salesOrganization {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.salesOrganization = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.salesOrganization.isOptional || newValue != "" {
                    currentEntity.salesOrganization = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSdDocumentCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.sdDocumentCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.sdDocumentCategory = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.sdDocumentCategory.isOptional || newValue != "" {
                    currentEntity.sdDocumentCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForShipmentBlockReason(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.shipmentBlockReason {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.shipmentBlockReason = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.shipmentBlockReason.isOptional || newValue != "" {
                    currentEntity.shipmentBlockReason = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForShippingCondition(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.shippingCondition {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.shippingCondition = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.shippingCondition.isOptional || newValue != "" {
                    currentEntity.shippingCondition = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForShippingPoint(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.shippingPoint {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.shippingPoint = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.shippingPoint.isOptional || newValue != "" {
                    currentEntity.shippingPoint = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForShippingType(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.shippingType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.shippingType = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.shippingType.isOptional || newValue != "" {
                    currentEntity.shippingType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForShipToParty(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.shipToParty {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.shipToParty = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.shipToParty.isOptional || newValue != "" {
                    currentEntity.shipToParty = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSoldToParty(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.soldToParty {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.soldToParty = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.soldToParty.isOptional || newValue != "" {
                    currentEntity.soldToParty = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSpecialProcessingCode(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.specialProcessingCode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.specialProcessingCode = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.specialProcessingCode.isOptional || newValue != "" {
                    currentEntity.specialProcessingCode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStatisticsCurrency(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.statisticsCurrency {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.statisticsCurrency = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.statisticsCurrency.isOptional || newValue != "" {
                    currentEntity.statisticsCurrency = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSupplier(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.supplier {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.supplier = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.supplier.isOptional || newValue != "" {
                    currentEntity.supplier = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTotalBlockStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.totalBlockStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.totalBlockStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.totalBlockStatus.isOptional || newValue != "" {
                    currentEntity.totalBlockStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTotalCreditCheckStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.totalCreditCheckStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.totalCreditCheckStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.totalCreditCheckStatus.isOptional || newValue != "" {
                    currentEntity.totalCreditCheckStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTotalNumberOfPackage(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.totalNumberOfPackage {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.totalNumberOfPackage = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.totalNumberOfPackage.isOptional || newValue != "" {
                    currentEntity.totalNumberOfPackage = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTransactionCurrency(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.transactionCurrency {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.transactionCurrency = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.transactionCurrency.isOptional || newValue != "" {
                    currentEntity.transactionCurrency = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTransportationGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.transportationGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.transportationGroup = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.transportationGroup.isOptional || newValue != "" {
                    currentEntity.transportationGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTransportationPlanningDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.transportationPlanningDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.transportationPlanningDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.transportationPlanningDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTransportationPlanningStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.transportationPlanningStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.transportationPlanningStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.transportationPlanningStatus.isOptional || newValue != "" {
                    currentEntity.transportationPlanningStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForUnloadingPointName(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.unloadingPointName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.unloadingPointName = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.unloadingPointName.isOptional || newValue != "" {
                    currentEntity.unloadingPointName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouse(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouse {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouse = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.warehouse.isOptional || newValue != "" {
                    currentEntity.warehouse = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouseGate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouseGate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouseGate = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.warehouseGate.isOptional || newValue != "" {
                    currentEntity.warehouseGate = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouseStagingArea(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryHeaderType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouseStagingArea {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouseStagingArea = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryHeaderType.warehouseStagingArea.isOptional || newValue != "" {
                    currentEntity.warehouseStagingArea = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    // MARK: - OData functionalities

    @objc func createEntity() {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Creating entity in backend.")
        self.ec1.createEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Create entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityCreationTitle", value: "Create entry failed", comment: "XTIT: Title of alert message about entity creation error."), error: error, viewController: self)
                return
            }
            self.logger.info("Create entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Created", comment: "XMSG: Title of alert message about successful entity creation."))
                    self.tableUpdater?.entitySetHasChanged()
                }
            }
        }
    }

    func createEntityWithDefaultValues() -> AInbDeliveryHeaderType {
        let newEntity = AInbDeliveryHeaderType()

        // Key properties without default value should be invalid by default for Create scenario
        if newEntity.deliveryDocument == nil || newEntity.deliveryDocument!.isEmpty {
            self.validity["DeliveryDocument"] = false
        }

        self.barButtonShouldBeEnabled()
        return newEntity
    }

    @objc func updateEntity(_: AnyObject) {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Updating entity in backend.")
        self.ec1.updateEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Update entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityUpdateTitle", value: "Update entry failed", comment: "XTIT: Title of alert message about entity update failure."), error: error, viewController: self)
                return
            }
            self.logger.info("Update entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyUpdateEntityFinishedTitle", value: "Updated", comment: "XTIT: Title of alert message about successful entity update."))
                    self.entityUpdater?.entityHasChanged(self.entity)
                }
            }
        }
    }

    // MARK: - other logic, helper

    @objc func cancel() {
        DispatchQueue.main.async {
            self.dismiss(animated: true)
        }
    }

    // Check if all text fields are valid
    private func barButtonShouldBeEnabled() {
        let anyFieldInvalid = self.validity.values.first { field in
            field == false
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = anyFieldInvalid == nil
    }
}

extension AInbDeliveryHeaderTypeDetailViewController: CPIKIMODataEntityUpdaterDelegate {
    func entityHasChanged(_ entityValue: EntityValue?) {
        if let entity = entityValue {
            let currentEntity = entity as! AInbDeliveryHeaderType
            self.entity = currentEntity
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
}
