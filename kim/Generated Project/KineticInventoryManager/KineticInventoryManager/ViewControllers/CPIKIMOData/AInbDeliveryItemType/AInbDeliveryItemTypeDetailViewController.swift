//
// KineticInventoryManager
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 30/03/20
//

import Foundation
import SAPCommon
import SAPFiori
import SAPFoundation
import SAPOData

class AInbDeliveryItemTypeDetailViewController: FUIFormTableViewController, SAPFioriLoadingIndicator {
    var ec1: Ec1<OnlineODataProvider>!
    private var validity = [String: Bool]()
    var allowsEditableCells = false

    private var _entity: AInbDeliveryItemType?
    var entity: AInbDeliveryItemType {
        get {
            if self._entity == nil {
                self._entity = self.createEntityWithDefaultValues()
            }
            return self._entity!
        }
        set {
            self._entity = newValue
        }
    }

    private let logger = Logger.shared(named: "AInbDeliveryItemTypeMasterViewControllerLogger")
    var loadingIndicator: FUILoadingIndicatorView?
    var entityUpdater: CPIKIMODataEntityUpdaterDelegate?
    var tableUpdater: CPIKIMODataEntitySetUpdaterDelegate?
    private let okTitle = NSLocalizedString("keyOkButtonTitle",
                                            value: "OK",
                                            comment: "XBUT: Title of OK button.")
    var preventNavigationLoop = false
    var entitySetName: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "updateEntity" {
            // Show the Detail view with the current entity, where the properties scan be edited and updated
            self.logger.info("Showing a view to update the selected entity.")
            let dest = segue.destination as! UINavigationController
            let detailViewController = dest.viewControllers[0] as! AInbDeliveryItemTypeDetailViewController
            detailViewController.title = NSLocalizedString("keyUpdateEntityTitle", value: "Update Entity", comment: "XTIT: Title of update selected entity screen.")
            detailViewController.ec1 = self.ec1
            detailViewController.entity = self.entity
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: detailViewController, action: #selector(detailViewController.updateEntity))
            detailViewController.navigationItem.rightBarButtonItem = doneButton
            let cancelButton = UIBarButtonItem(title: NSLocalizedString("keyCancelButtonToGoPreviousScreen", value: "Cancel", comment: "XBUT: Title of Cancel button."), style: .plain, target: detailViewController, action: #selector(detailViewController.cancel))
            detailViewController.navigationItem.leftBarButtonItem = cancelButton
            detailViewController.allowsEditableCells = true
            detailViewController.entityUpdater = self
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return self.cellForActualDeliveredQtyInBaseUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.actualDeliveredQtyInBaseUnit)
        case 1:
            return self.cellForActualDeliveryQuantity(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.actualDeliveryQuantity)
        case 2:
            return self.cellForAdditionalCustomerGroup1(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.additionalCustomerGroup1)
        case 3:
            return self.cellForAdditionalCustomerGroup2(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.additionalCustomerGroup2)
        case 4:
            return self.cellForAdditionalCustomerGroup3(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.additionalCustomerGroup3)
        case 5:
            return self.cellForAdditionalCustomerGroup4(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.additionalCustomerGroup4)
        case 6:
            return self.cellForAdditionalCustomerGroup5(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.additionalCustomerGroup5)
        case 7:
            return self.cellForAdditionalMaterialGroup1(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.additionalMaterialGroup1)
        case 8:
            return self.cellForAdditionalMaterialGroup2(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.additionalMaterialGroup2)
        case 9:
            return self.cellForAdditionalMaterialGroup3(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.additionalMaterialGroup3)
        case 10:
            return self.cellForAdditionalMaterialGroup4(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.additionalMaterialGroup4)
        case 11:
            return self.cellForAdditionalMaterialGroup5(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.additionalMaterialGroup5)
        case 12:
            return self.cellForAlternateProductNumber(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.alternateProductNumber)
        case 13:
            return self.cellForBaseUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.baseUnit)
        case 14:
            return self.cellForBatch(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.batch)
        case 15:
            return self.cellForBatchBySupplier(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.batchBySupplier)
        case 16:
            return self.cellForBatchClassification(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.batchClassification)
        case 17:
            return self.cellForBomExplosion(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.bomExplosion)
        case 18:
            return self.cellForBusinessArea(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.businessArea)
        case 19:
            return self.cellForConsumptionPosting(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.consumptionPosting)
        case 20:
            return self.cellForControllingArea(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.controllingArea)
        case 21:
            return self.cellForCostCenter(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.costCenter)
        case 22:
            return self.cellForCreatedByUser(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.createdByUser)
        case 23:
            return self.cellForCreationDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.creationDate)
        case 24:
            return self.cellForCustEngineeringChgStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.custEngineeringChgStatus)
        case 25:
            return self.cellForDeliveryDocument(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.deliveryDocument)
        case 26:
            return self.cellForDeliveryDocumentItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.deliveryDocumentItem)
        case 27:
            return self.cellForDeliveryDocumentItemCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.deliveryDocumentItemCategory)
        case 28:
            return self.cellForDeliveryDocumentItemText(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.deliveryDocumentItemText)
        case 29:
            return self.cellForDeliveryGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.deliveryGroup)
        case 30:
            return self.cellForDeliveryQuantityUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.deliveryQuantityUnit)
        case 31:
            return self.cellForDeliveryRelatedBillingStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.deliveryRelatedBillingStatus)
        case 32:
            return self.cellForDeliveryToBaseQuantityDnmntr(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.deliveryToBaseQuantityDnmntr)
        case 33:
            return self.cellForDeliveryToBaseQuantityNmrtr(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.deliveryToBaseQuantityNmrtr)
        case 34:
            return self.cellForDepartmentClassificationByCust(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.departmentClassificationByCust)
        case 35:
            return self.cellForDistributionChannel(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.distributionChannel)
        case 36:
            return self.cellForDivision(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.division)
        case 37:
            return self.cellForFixedShipgProcgDurationInDays(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.fixedShipgProcgDurationInDays)
        case 38:
            return self.cellForGlAccount(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.glAccount)
        case 39:
            return self.cellForGoodsMovementReasonCode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.goodsMovementReasonCode)
        case 40:
            return self.cellForGoodsMovementStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.goodsMovementStatus)
        case 41:
            return self.cellForGoodsMovementType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.goodsMovementType)
        case 42:
            return self.cellForHigherLevelItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.higherLevelItem)
        case 43:
            return self.cellForInspectionLot(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.inspectionLot)
        case 44:
            return self.cellForInspectionPartialLot(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.inspectionPartialLot)
        case 45:
            return self.cellForIntercompanyBillingStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.intercompanyBillingStatus)
        case 46:
            return self.cellForInternationalArticleNumber(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.internationalArticleNumber)
        case 47:
            return self.cellForInventorySpecialStockType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.inventorySpecialStockType)
        case 48:
            return self.cellForInventoryValuationType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.inventoryValuationType)
        case 49:
            return self.cellForIsCompletelyDelivered(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.isCompletelyDelivered)
        case 50:
            return self.cellForIsNotGoodsMovementsRelevant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.isNotGoodsMovementsRelevant)
        case 51:
            return self.cellForIsSeparateValuation(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.isSeparateValuation)
        case 52:
            return self.cellForIssgOrRcvgBatch(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.issgOrRcvgBatch)
        case 53:
            return self.cellForIssgOrRcvgMaterial(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.issgOrRcvgMaterial)
        case 54:
            return self.cellForIssgOrRcvgSpclStockInd(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.issgOrRcvgSpclStockInd)
        case 55:
            return self.cellForIssgOrRcvgStockCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.issgOrRcvgStockCategory)
        case 56:
            return self.cellForIssgOrRcvgValuationType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.issgOrRcvgValuationType)
        case 57:
            return self.cellForIssuingOrReceivingPlant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.issuingOrReceivingPlant)
        case 58:
            return self.cellForIssuingOrReceivingStorageLoc(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.issuingOrReceivingStorageLoc)
        case 59:
            return self.cellForItemBillingBlockReason(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemBillingBlockReason)
        case 60:
            return self.cellForItemBillingIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemBillingIncompletionStatus)
        case 61:
            return self.cellForItemDeliveryIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemDeliveryIncompletionStatus)
        case 62:
            return self.cellForItemGdsMvtIncompletionSts(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemGdsMvtIncompletionSts)
        case 63:
            return self.cellForItemGeneralIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemGeneralIncompletionStatus)
        case 64:
            return self.cellForItemGrossWeight(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemGrossWeight)
        case 65:
            return self.cellForItemIsBillingRelevant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemIsBillingRelevant)
        case 66:
            return self.cellForItemNetWeight(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemNetWeight)
        case 67:
            return self.cellForItemPackingIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemPackingIncompletionStatus)
        case 68:
            return self.cellForItemPickingIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemPickingIncompletionStatus)
        case 69:
            return self.cellForItemVolume(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemVolume)
        case 70:
            return self.cellForItemVolumeUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemVolumeUnit)
        case 71:
            return self.cellForItemWeightUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.itemWeightUnit)
        case 72:
            return self.cellForLastChangeDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.lastChangeDate)
        case 73:
            return self.cellForLoadingGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.loadingGroup)
        case 74:
            return self.cellForManufactureDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.manufactureDate)
        case 75:
            return self.cellForMaterial(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.material)
        case 76:
            return self.cellForMaterialByCustomer(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.materialByCustomer)
        case 77:
            return self.cellForMaterialFreightGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.materialFreightGroup)
        case 78:
            return self.cellForMaterialGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.materialGroup)
        case 79:
            return self.cellForMaterialIsBatchManaged(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.materialIsBatchManaged)
        case 80:
            return self.cellForMaterialIsIntBatchManaged(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.materialIsIntBatchManaged)
        case 81:
            return self.cellForNumberOfSerialNumbers(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.numberOfSerialNumbers)
        case 82:
            return self.cellForOrderID(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.orderID)
        case 83:
            return self.cellForOrderItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.orderItem)
        case 84:
            return self.cellForOriginalDeliveryQuantity(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.originalDeliveryQuantity)
        case 85:
            return self.cellForOriginallyRequestedMaterial(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.originallyRequestedMaterial)
        case 86:
            return self.cellForOverdelivTolrtdLmtRatioInPct(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.overdelivTolrtdLmtRatioInPct)
        case 87:
            return self.cellForPackingStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.packingStatus)
        case 88:
            return self.cellForPartialDeliveryIsAllowed(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.partialDeliveryIsAllowed)
        case 89:
            return self.cellForPaymentGuaranteeForm(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.paymentGuaranteeForm)
        case 90:
            return self.cellForPickingConfirmationStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.pickingConfirmationStatus)
        case 91:
            return self.cellForPickingControl(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.pickingControl)
        case 92:
            return self.cellForPickingStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.pickingStatus)
        case 93:
            return self.cellForPlant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.plant)
        case 94:
            return self.cellForPrimaryPostingSwitch(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.primaryPostingSwitch)
        case 95:
            return self.cellForProductAvailabilityDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.productAvailabilityDate)
        case 96:
            return self.cellForProductConfiguration(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.productConfiguration)
        case 97:
            return self.cellForProductHierarchyNode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.productHierarchyNode)
        case 98:
            return self.cellForProfitabilitySegment(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.profitabilitySegment)
        case 99:
            return self.cellForProfitCenter(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.profitCenter)
        case 100:
            return self.cellForProofOfDeliveryRelevanceCode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.proofOfDeliveryRelevanceCode)
        case 101:
            return self.cellForProofOfDeliveryStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.proofOfDeliveryStatus)
        case 102:
            return self.cellForQuantityIsFixed(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.quantityIsFixed)
        case 103:
            return self.cellForReceivingPoint(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.receivingPoint)
        case 104:
            return self.cellForReferenceDocumentLogicalSystem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.referenceDocumentLogicalSystem)
        case 105:
            return self.cellForReferenceSDDocument(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.referenceSDDocument)
        case 106:
            return self.cellForReferenceSDDocumentCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.referenceSDDocumentCategory)
        case 107:
            return self.cellForReferenceSDDocumentItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.referenceSDDocumentItem)
        case 108:
            return self.cellForRetailPromotion(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.retailPromotion)
        case 109:
            return self.cellForSalesDocumentItemType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.salesDocumentItemType)
        case 110:
            return self.cellForSalesGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.salesGroup)
        case 111:
            return self.cellForSalesOffice(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.salesOffice)
        case 112:
            return self.cellForSdDocumentCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.sdDocumentCategory)
        case 113:
            return self.cellForSdProcessStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.sdProcessStatus)
        case 114:
            return self.cellForShelfLifeExpirationDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.shelfLifeExpirationDate)
        case 115:
            return self.cellForStatisticsDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.statisticsDate)
        case 116:
            return self.cellForStockType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.stockType)
        case 117:
            return self.cellForStorageBin(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.storageBin)
        case 118:
            return self.cellForStorageLocation(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.storageLocation)
        case 119:
            return self.cellForStorageType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.storageType)
        case 120:
            return self.cellForSubsequentMovementType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.subsequentMovementType)
        case 121:
            return self.cellForTransportationGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.transportationGroup)
        case 122:
            return self.cellForUnderdelivTolrtdLmtRatioInPct(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.underdelivTolrtdLmtRatioInPct)
        case 123:
            return self.cellForUnlimitedOverdeliveryIsAllowed(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.unlimitedOverdeliveryIsAllowed)
        case 124:
            return self.cellForVarblShipgProcgDurationInDays(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.varblShipgProcgDurationInDays)
        case 125:
            return self.cellForWarehouse(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.warehouse)
        case 126:
            return self.cellForWarehouseActivityStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.warehouseActivityStatus)
        case 127:
            return self.cellForWarehouseStagingArea(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.warehouseStagingArea)
        case 128:
            return self.cellForDeliveryVersion(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.deliveryVersion)
        case 129:
            return self.cellForWarehouseStockCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.warehouseStockCategory)
        case 130:
            return self.cellForWarehouseStorageBin(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.warehouseStorageBin)
        case 131:
            return self.cellForStockSegment(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.stockSegment)
        case 132:
            return self.cellForRequirementSegment(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryItemType.requirementSegment)
        default:
            return UITableViewCell()
        }
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 133
    }

    override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.preventNavigationLoop {
            AlertHelper.displayAlert(with: NSLocalizedString("keyAlertNavigationLoop", value: "No further navigation is possible.", comment: "XTIT: Title of alert message about preventing navigation loop."), error: nil, viewController: self)
            return
        }
        switch indexPath.row {
        default:
            return
        }
    }

    // MARK: - OData property specific cell creators

    private func cellForActualDeliveredQtyInBaseUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.actualDeliveredQtyInBaseUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.actualDeliveredQtyInBaseUnit = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.actualDeliveredQtyInBaseUnit = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForActualDeliveryQuantity(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.actualDeliveryQuantity {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.actualDeliveryQuantity = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.actualDeliveryQuantity = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalCustomerGroup1(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalCustomerGroup1 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalCustomerGroup1 = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.additionalCustomerGroup1.isOptional || newValue != "" {
                    currentEntity.additionalCustomerGroup1 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalCustomerGroup2(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalCustomerGroup2 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalCustomerGroup2 = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.additionalCustomerGroup2.isOptional || newValue != "" {
                    currentEntity.additionalCustomerGroup2 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalCustomerGroup3(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalCustomerGroup3 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalCustomerGroup3 = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.additionalCustomerGroup3.isOptional || newValue != "" {
                    currentEntity.additionalCustomerGroup3 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalCustomerGroup4(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalCustomerGroup4 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalCustomerGroup4 = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.additionalCustomerGroup4.isOptional || newValue != "" {
                    currentEntity.additionalCustomerGroup4 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalCustomerGroup5(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalCustomerGroup5 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalCustomerGroup5 = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.additionalCustomerGroup5.isOptional || newValue != "" {
                    currentEntity.additionalCustomerGroup5 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalMaterialGroup1(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalMaterialGroup1 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalMaterialGroup1 = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.additionalMaterialGroup1.isOptional || newValue != "" {
                    currentEntity.additionalMaterialGroup1 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalMaterialGroup2(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalMaterialGroup2 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalMaterialGroup2 = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.additionalMaterialGroup2.isOptional || newValue != "" {
                    currentEntity.additionalMaterialGroup2 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalMaterialGroup3(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalMaterialGroup3 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalMaterialGroup3 = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.additionalMaterialGroup3.isOptional || newValue != "" {
                    currentEntity.additionalMaterialGroup3 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalMaterialGroup4(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalMaterialGroup4 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalMaterialGroup4 = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.additionalMaterialGroup4.isOptional || newValue != "" {
                    currentEntity.additionalMaterialGroup4 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalMaterialGroup5(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalMaterialGroup5 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalMaterialGroup5 = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.additionalMaterialGroup5.isOptional || newValue != "" {
                    currentEntity.additionalMaterialGroup5 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAlternateProductNumber(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.alternateProductNumber {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.alternateProductNumber = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.alternateProductNumber.isOptional || newValue != "" {
                    currentEntity.alternateProductNumber = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBaseUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.baseUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.baseUnit = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.baseUnit.isOptional || newValue != "" {
                    currentEntity.baseUnit = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBatch(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.batch {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.batch = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.batch.isOptional || newValue != "" {
                    currentEntity.batch = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBatchBySupplier(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.batchBySupplier {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.batchBySupplier = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.batchBySupplier.isOptional || newValue != "" {
                    currentEntity.batchBySupplier = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBatchClassification(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.batchClassification {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.batchClassification = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.batchClassification.isOptional || newValue != "" {
                    currentEntity.batchClassification = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBomExplosion(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.bomExplosion {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.bomExplosion = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.bomExplosion.isOptional || newValue != "" {
                    currentEntity.bomExplosion = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBusinessArea(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.businessArea {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.businessArea = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.businessArea.isOptional || newValue != "" {
                    currentEntity.businessArea = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForConsumptionPosting(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.consumptionPosting {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.consumptionPosting = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.consumptionPosting.isOptional || newValue != "" {
                    currentEntity.consumptionPosting = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForControllingArea(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.controllingArea {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.controllingArea = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.controllingArea.isOptional || newValue != "" {
                    currentEntity.controllingArea = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCostCenter(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.costCenter {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.costCenter = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.costCenter.isOptional || newValue != "" {
                    currentEntity.costCenter = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCreatedByUser(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.createdByUser {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.createdByUser = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.createdByUser.isOptional || newValue != "" {
                    currentEntity.createdByUser = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCreationDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.creationDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.creationDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.creationDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCustEngineeringChgStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.custEngineeringChgStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.custEngineeringChgStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.custEngineeringChgStatus.isOptional || newValue != "" {
                    currentEntity.custEngineeringChgStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDocument(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocument {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocument = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.deliveryDocument.isOptional || newValue != "" {
                    currentEntity.deliveryDocument = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDocumentItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocumentItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocumentItem = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.deliveryDocumentItem.isOptional || newValue != "" {
                    currentEntity.deliveryDocumentItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDocumentItemCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocumentItemCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocumentItemCategory = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.deliveryDocumentItemCategory.isOptional || newValue != "" {
                    currentEntity.deliveryDocumentItemCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDocumentItemText(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocumentItemText {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocumentItemText = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.deliveryDocumentItemText.isOptional || newValue != "" {
                    currentEntity.deliveryDocumentItemText = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryGroup = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.deliveryGroup.isOptional || newValue != "" {
                    currentEntity.deliveryGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryQuantityUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryQuantityUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryQuantityUnit = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.deliveryQuantityUnit.isOptional || newValue != "" {
                    currentEntity.deliveryQuantityUnit = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryRelatedBillingStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryRelatedBillingStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryRelatedBillingStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.deliveryRelatedBillingStatus.isOptional || newValue != "" {
                    currentEntity.deliveryRelatedBillingStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryToBaseQuantityDnmntr(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryToBaseQuantityDnmntr {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryToBaseQuantityDnmntr = nil
                isNewValueValid = true
            } else {
                if let validValue = BigInteger.parse(newValue) {
                    currentEntity.deliveryToBaseQuantityDnmntr = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryToBaseQuantityNmrtr(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryToBaseQuantityNmrtr {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryToBaseQuantityNmrtr = nil
                isNewValueValid = true
            } else {
                if let validValue = BigInteger.parse(newValue) {
                    currentEntity.deliveryToBaseQuantityNmrtr = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDepartmentClassificationByCust(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.departmentClassificationByCust {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.departmentClassificationByCust = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.departmentClassificationByCust.isOptional || newValue != "" {
                    currentEntity.departmentClassificationByCust = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDistributionChannel(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.distributionChannel {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.distributionChannel = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.distributionChannel.isOptional || newValue != "" {
                    currentEntity.distributionChannel = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDivision(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.division {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.division = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.division.isOptional || newValue != "" {
                    currentEntity.division = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForFixedShipgProcgDurationInDays(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.fixedShipgProcgDurationInDays {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.fixedShipgProcgDurationInDays = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.fixedShipgProcgDurationInDays = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGlAccount(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.glAccount {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.glAccount = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.glAccount.isOptional || newValue != "" {
                    currentEntity.glAccount = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGoodsMovementReasonCode(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.goodsMovementReasonCode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.goodsMovementReasonCode = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.goodsMovementReasonCode.isOptional || newValue != "" {
                    currentEntity.goodsMovementReasonCode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGoodsMovementStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.goodsMovementStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.goodsMovementStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.goodsMovementStatus.isOptional || newValue != "" {
                    currentEntity.goodsMovementStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGoodsMovementType(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.goodsMovementType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.goodsMovementType = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.goodsMovementType.isOptional || newValue != "" {
                    currentEntity.goodsMovementType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHigherLevelItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.higherLevelItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.higherLevelItem = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.higherLevelItem.isOptional || newValue != "" {
                    currentEntity.higherLevelItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInspectionLot(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.inspectionLot {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.inspectionLot = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.inspectionLot.isOptional || newValue != "" {
                    currentEntity.inspectionLot = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInspectionPartialLot(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.inspectionPartialLot {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.inspectionPartialLot = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.inspectionPartialLot.isOptional || newValue != "" {
                    currentEntity.inspectionPartialLot = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIntercompanyBillingStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.intercompanyBillingStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.intercompanyBillingStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.intercompanyBillingStatus.isOptional || newValue != "" {
                    currentEntity.intercompanyBillingStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInternationalArticleNumber(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.internationalArticleNumber {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.internationalArticleNumber = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.internationalArticleNumber.isOptional || newValue != "" {
                    currentEntity.internationalArticleNumber = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInventorySpecialStockType(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.inventorySpecialStockType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.inventorySpecialStockType = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.inventorySpecialStockType.isOptional || newValue != "" {
                    currentEntity.inventorySpecialStockType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInventoryValuationType(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.inventoryValuationType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.inventoryValuationType = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.inventoryValuationType.isOptional || newValue != "" {
                    currentEntity.inventoryValuationType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIsCompletelyDelivered(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.isCompletelyDelivered {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.isCompletelyDelivered = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.isCompletelyDelivered = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIsNotGoodsMovementsRelevant(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.isNotGoodsMovementsRelevant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.isNotGoodsMovementsRelevant = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.isNotGoodsMovementsRelevant.isOptional || newValue != "" {
                    currentEntity.isNotGoodsMovementsRelevant = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIsSeparateValuation(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.isSeparateValuation {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.isSeparateValuation = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.isSeparateValuation = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgBatch(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgBatch {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgBatch = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.issgOrRcvgBatch.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgBatch = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgMaterial(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgMaterial {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgMaterial = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.issgOrRcvgMaterial.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgMaterial = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgSpclStockInd(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgSpclStockInd {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgSpclStockInd = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.issgOrRcvgSpclStockInd.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgSpclStockInd = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgStockCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgStockCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgStockCategory = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.issgOrRcvgStockCategory.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgStockCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgValuationType(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgValuationType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgValuationType = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.issgOrRcvgValuationType.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgValuationType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssuingOrReceivingPlant(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issuingOrReceivingPlant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issuingOrReceivingPlant = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.issuingOrReceivingPlant.isOptional || newValue != "" {
                    currentEntity.issuingOrReceivingPlant = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssuingOrReceivingStorageLoc(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issuingOrReceivingStorageLoc {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issuingOrReceivingStorageLoc = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.issuingOrReceivingStorageLoc.isOptional || newValue != "" {
                    currentEntity.issuingOrReceivingStorageLoc = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemBillingBlockReason(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemBillingBlockReason {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemBillingBlockReason = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.itemBillingBlockReason.isOptional || newValue != "" {
                    currentEntity.itemBillingBlockReason = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemBillingIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemBillingIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemBillingIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.itemBillingIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.itemBillingIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemDeliveryIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemDeliveryIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemDeliveryIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.itemDeliveryIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.itemDeliveryIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemGdsMvtIncompletionSts(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemGdsMvtIncompletionSts {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemGdsMvtIncompletionSts = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.itemGdsMvtIncompletionSts.isOptional || newValue != "" {
                    currentEntity.itemGdsMvtIncompletionSts = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemGeneralIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemGeneralIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemGeneralIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.itemGeneralIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.itemGeneralIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemGrossWeight(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemGrossWeight {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemGrossWeight = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.itemGrossWeight = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemIsBillingRelevant(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemIsBillingRelevant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemIsBillingRelevant = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.itemIsBillingRelevant.isOptional || newValue != "" {
                    currentEntity.itemIsBillingRelevant = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemNetWeight(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemNetWeight {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemNetWeight = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.itemNetWeight = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemPackingIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemPackingIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemPackingIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.itemPackingIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.itemPackingIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemPickingIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemPickingIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemPickingIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.itemPickingIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.itemPickingIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemVolume(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemVolume {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemVolume = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.itemVolume = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemVolumeUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemVolumeUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemVolumeUnit = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.itemVolumeUnit.isOptional || newValue != "" {
                    currentEntity.itemVolumeUnit = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemWeightUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemWeightUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemWeightUnit = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.itemWeightUnit.isOptional || newValue != "" {
                    currentEntity.itemWeightUnit = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForLastChangeDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.lastChangeDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.lastChangeDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.lastChangeDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForLoadingGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.loadingGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.loadingGroup = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.loadingGroup.isOptional || newValue != "" {
                    currentEntity.loadingGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForManufactureDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.manufactureDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.manufactureDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.manufactureDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterial(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.material {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.material = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.material.isOptional || newValue != "" {
                    currentEntity.material = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialByCustomer(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialByCustomer {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialByCustomer = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.materialByCustomer.isOptional || newValue != "" {
                    currentEntity.materialByCustomer = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialFreightGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialFreightGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialFreightGroup = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.materialFreightGroup.isOptional || newValue != "" {
                    currentEntity.materialFreightGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialGroup = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.materialGroup.isOptional || newValue != "" {
                    currentEntity.materialGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialIsBatchManaged(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialIsBatchManaged {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialIsBatchManaged = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.materialIsBatchManaged = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialIsIntBatchManaged(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialIsIntBatchManaged {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialIsIntBatchManaged = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.materialIsIntBatchManaged = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForNumberOfSerialNumbers(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.numberOfSerialNumbers {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.numberOfSerialNumbers = nil
                isNewValueValid = true
            } else {
                if let validValue = Int(newValue) {
                    currentEntity.numberOfSerialNumbers = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOrderID(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.orderID {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.orderID = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.orderID.isOptional || newValue != "" {
                    currentEntity.orderID = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOrderItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.orderItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.orderItem = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.orderItem.isOptional || newValue != "" {
                    currentEntity.orderItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOriginalDeliveryQuantity(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.originalDeliveryQuantity {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.originalDeliveryQuantity = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.originalDeliveryQuantity = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOriginallyRequestedMaterial(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.originallyRequestedMaterial {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.originallyRequestedMaterial = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.originallyRequestedMaterial.isOptional || newValue != "" {
                    currentEntity.originallyRequestedMaterial = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOverdelivTolrtdLmtRatioInPct(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.overdelivTolrtdLmtRatioInPct {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.overdelivTolrtdLmtRatioInPct = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.overdelivTolrtdLmtRatioInPct = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPackingStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.packingStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.packingStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.packingStatus.isOptional || newValue != "" {
                    currentEntity.packingStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPartialDeliveryIsAllowed(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.partialDeliveryIsAllowed {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.partialDeliveryIsAllowed = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.partialDeliveryIsAllowed.isOptional || newValue != "" {
                    currentEntity.partialDeliveryIsAllowed = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPaymentGuaranteeForm(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.paymentGuaranteeForm {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.paymentGuaranteeForm = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.paymentGuaranteeForm.isOptional || newValue != "" {
                    currentEntity.paymentGuaranteeForm = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPickingConfirmationStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.pickingConfirmationStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.pickingConfirmationStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.pickingConfirmationStatus.isOptional || newValue != "" {
                    currentEntity.pickingConfirmationStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPickingControl(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.pickingControl {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.pickingControl = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.pickingControl.isOptional || newValue != "" {
                    currentEntity.pickingControl = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPickingStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.pickingStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.pickingStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.pickingStatus.isOptional || newValue != "" {
                    currentEntity.pickingStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPlant(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.plant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.plant = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.plant.isOptional || newValue != "" {
                    currentEntity.plant = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPrimaryPostingSwitch(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.primaryPostingSwitch {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.primaryPostingSwitch = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.primaryPostingSwitch.isOptional || newValue != "" {
                    currentEntity.primaryPostingSwitch = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProductAvailabilityDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.productAvailabilityDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.productAvailabilityDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.productAvailabilityDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProductConfiguration(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.productConfiguration {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.productConfiguration = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.productConfiguration.isOptional || newValue != "" {
                    currentEntity.productConfiguration = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProductHierarchyNode(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.productHierarchyNode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.productHierarchyNode = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.productHierarchyNode.isOptional || newValue != "" {
                    currentEntity.productHierarchyNode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProfitabilitySegment(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.profitabilitySegment {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.profitabilitySegment = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.profitabilitySegment.isOptional || newValue != "" {
                    currentEntity.profitabilitySegment = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProfitCenter(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.profitCenter {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.profitCenter = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.profitCenter.isOptional || newValue != "" {
                    currentEntity.profitCenter = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProofOfDeliveryRelevanceCode(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.proofOfDeliveryRelevanceCode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.proofOfDeliveryRelevanceCode = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.proofOfDeliveryRelevanceCode.isOptional || newValue != "" {
                    currentEntity.proofOfDeliveryRelevanceCode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProofOfDeliveryStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.proofOfDeliveryStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.proofOfDeliveryStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.proofOfDeliveryStatus.isOptional || newValue != "" {
                    currentEntity.proofOfDeliveryStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForQuantityIsFixed(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.quantityIsFixed {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.quantityIsFixed = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.quantityIsFixed = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReceivingPoint(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.receivingPoint {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.receivingPoint = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.receivingPoint.isOptional || newValue != "" {
                    currentEntity.receivingPoint = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReferenceDocumentLogicalSystem(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.referenceDocumentLogicalSystem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.referenceDocumentLogicalSystem = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.referenceDocumentLogicalSystem.isOptional || newValue != "" {
                    currentEntity.referenceDocumentLogicalSystem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReferenceSDDocument(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.referenceSDDocument {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.referenceSDDocument = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.referenceSDDocument.isOptional || newValue != "" {
                    currentEntity.referenceSDDocument = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReferenceSDDocumentCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.referenceSDDocumentCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.referenceSDDocumentCategory = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.referenceSDDocumentCategory.isOptional || newValue != "" {
                    currentEntity.referenceSDDocumentCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReferenceSDDocumentItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.referenceSDDocumentItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.referenceSDDocumentItem = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.referenceSDDocumentItem.isOptional || newValue != "" {
                    currentEntity.referenceSDDocumentItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForRetailPromotion(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.retailPromotion {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.retailPromotion = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.retailPromotion.isOptional || newValue != "" {
                    currentEntity.retailPromotion = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSalesDocumentItemType(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.salesDocumentItemType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.salesDocumentItemType = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.salesDocumentItemType.isOptional || newValue != "" {
                    currentEntity.salesDocumentItemType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSalesGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.salesGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.salesGroup = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.salesGroup.isOptional || newValue != "" {
                    currentEntity.salesGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSalesOffice(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.salesOffice {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.salesOffice = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.salesOffice.isOptional || newValue != "" {
                    currentEntity.salesOffice = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSdDocumentCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.sdDocumentCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.sdDocumentCategory = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.sdDocumentCategory.isOptional || newValue != "" {
                    currentEntity.sdDocumentCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSdProcessStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.sdProcessStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.sdProcessStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.sdProcessStatus.isOptional || newValue != "" {
                    currentEntity.sdProcessStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForShelfLifeExpirationDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.shelfLifeExpirationDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.shelfLifeExpirationDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.shelfLifeExpirationDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStatisticsDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.statisticsDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.statisticsDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.statisticsDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStockType(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.stockType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.stockType = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.stockType.isOptional || newValue != "" {
                    currentEntity.stockType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStorageBin(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.storageBin {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.storageBin = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.storageBin.isOptional || newValue != "" {
                    currentEntity.storageBin = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStorageLocation(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.storageLocation {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.storageLocation = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.storageLocation.isOptional || newValue != "" {
                    currentEntity.storageLocation = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStorageType(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.storageType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.storageType = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.storageType.isOptional || newValue != "" {
                    currentEntity.storageType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSubsequentMovementType(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.subsequentMovementType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.subsequentMovementType = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.subsequentMovementType.isOptional || newValue != "" {
                    currentEntity.subsequentMovementType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTransportationGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.transportationGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.transportationGroup = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.transportationGroup.isOptional || newValue != "" {
                    currentEntity.transportationGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForUnderdelivTolrtdLmtRatioInPct(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.underdelivTolrtdLmtRatioInPct {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.underdelivTolrtdLmtRatioInPct = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.underdelivTolrtdLmtRatioInPct = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForUnlimitedOverdeliveryIsAllowed(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.unlimitedOverdeliveryIsAllowed {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.unlimitedOverdeliveryIsAllowed = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.unlimitedOverdeliveryIsAllowed = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForVarblShipgProcgDurationInDays(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.varblShipgProcgDurationInDays {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.varblShipgProcgDurationInDays = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.varblShipgProcgDurationInDays = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouse(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouse {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouse = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.warehouse.isOptional || newValue != "" {
                    currentEntity.warehouse = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouseActivityStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouseActivityStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouseActivityStatus = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.warehouseActivityStatus.isOptional || newValue != "" {
                    currentEntity.warehouseActivityStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouseStagingArea(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouseStagingArea {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouseStagingArea = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.warehouseStagingArea.isOptional || newValue != "" {
                    currentEntity.warehouseStagingArea = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryVersion(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryVersion {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryVersion = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.deliveryVersion.isOptional || newValue != "" {
                    currentEntity.deliveryVersion = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouseStockCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouseStockCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouseStockCategory = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.warehouseStockCategory.isOptional || newValue != "" {
                    currentEntity.warehouseStockCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouseStorageBin(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouseStorageBin {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouseStorageBin = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.warehouseStorageBin.isOptional || newValue != "" {
                    currentEntity.warehouseStorageBin = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStockSegment(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.stockSegment {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.stockSegment = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.stockSegment.isOptional || newValue != "" {
                    currentEntity.stockSegment = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForRequirementSegment(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.requirementSegment {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.requirementSegment = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryItemType.requirementSegment.isOptional || newValue != "" {
                    currentEntity.requirementSegment = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    // MARK: - OData functionalities

    @objc func createEntity() {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Creating entity in backend.")
        self.ec1.createEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Create entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityCreationTitle", value: "Create entry failed", comment: "XTIT: Title of alert message about entity creation error."), error: error, viewController: self)
                return
            }
            self.logger.info("Create entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Created", comment: "XMSG: Title of alert message about successful entity creation."))
                    self.tableUpdater?.entitySetHasChanged()
                }
            }
        }
    }

    func createEntityWithDefaultValues() -> AInbDeliveryItemType {
        let newEntity = AInbDeliveryItemType()

        // Key properties without default value should be invalid by default for Create scenario
        if newEntity.deliveryDocument == nil || newEntity.deliveryDocument!.isEmpty {
            self.validity["DeliveryDocument"] = false
        }
        if newEntity.deliveryDocumentItem == nil || newEntity.deliveryDocumentItem!.isEmpty {
            self.validity["DeliveryDocumentItem"] = false
        }

        self.barButtonShouldBeEnabled()
        return newEntity
    }

    @objc func updateEntity(_: AnyObject) {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Updating entity in backend.")
        self.ec1.updateEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Update entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityUpdateTitle", value: "Update entry failed", comment: "XTIT: Title of alert message about entity update failure."), error: error, viewController: self)
                return
            }
            self.logger.info("Update entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyUpdateEntityFinishedTitle", value: "Updated", comment: "XTIT: Title of alert message about successful entity update."))
                    self.entityUpdater?.entityHasChanged(self.entity)
                }
            }
        }
    }

    // MARK: - other logic, helper

    @objc func cancel() {
        DispatchQueue.main.async {
            self.dismiss(animated: true)
        }
    }

    // Check if all text fields are valid
    private func barButtonShouldBeEnabled() {
        let anyFieldInvalid = self.validity.values.first { field in
            field == false
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = anyFieldInvalid == nil
    }
}

extension AInbDeliveryItemTypeDetailViewController: CPIKIMODataEntityUpdaterDelegate {
    func entityHasChanged(_ entityValue: EntityValue?) {
        if let entity = entityValue {
            let currentEntity = entity as! AInbDeliveryItemType
            self.entity = currentEntity
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
}
