//
// KineticInventoryManager
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 30/03/20
//

import Foundation
import SAPCommon
import SAPFiori
import SAPFoundation
import SAPOData

class AMaterialDocumentItemTypeDetailViewController: FUIFormTableViewController, SAPFioriLoadingIndicator {
    var ec1: Ec1<OnlineODataProvider>!
    private var validity = [String: Bool]()
    var allowsEditableCells = false

    private var _entity: AMaterialDocumentItemType?
    var entity: AMaterialDocumentItemType {
        get {
            if self._entity == nil {
                self._entity = self.createEntityWithDefaultValues()
            }
            return self._entity!
        }
        set {
            self._entity = newValue
        }
    }

    private let logger = Logger.shared(named: "AMaterialDocumentItemTypeMasterViewControllerLogger")
    var loadingIndicator: FUILoadingIndicatorView?
    var entityUpdater: CPIKIMODataEntityUpdaterDelegate?
    var tableUpdater: CPIKIMODataEntitySetUpdaterDelegate?
    private let okTitle = NSLocalizedString("keyOkButtonTitle",
                                            value: "OK",
                                            comment: "XBUT: Title of OK button.")
    var preventNavigationLoop = false
    var entitySetName: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "updateEntity" {
            // Show the Detail view with the current entity, where the properties scan be edited and updated
            self.logger.info("Showing a view to update the selected entity.")
            let dest = segue.destination as! UINavigationController
            let detailViewController = dest.viewControllers[0] as! AMaterialDocumentItemTypeDetailViewController
            detailViewController.title = NSLocalizedString("keyUpdateEntityTitle", value: "Update Entity", comment: "XTIT: Title of update selected entity screen.")
            detailViewController.ec1 = self.ec1
            detailViewController.entity = self.entity
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: detailViewController, action: #selector(detailViewController.updateEntity))
            detailViewController.navigationItem.rightBarButtonItem = doneButton
            let cancelButton = UIBarButtonItem(title: NSLocalizedString("keyCancelButtonToGoPreviousScreen", value: "Cancel", comment: "XBUT: Title of Cancel button."), style: .plain, target: detailViewController, action: #selector(detailViewController.cancel))
            detailViewController.navigationItem.leftBarButtonItem = cancelButton
            detailViewController.allowsEditableCells = true
            detailViewController.entityUpdater = self
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return self.cellForMaterialDocumentYear(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.materialDocumentYear)
        case 1:
            return self.cellForMaterialDocument(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.materialDocument)
        case 2:
            return self.cellForMaterialDocumentItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.materialDocumentItem)
        case 3:
            return self.cellForMaterial(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.material)
        case 4:
            return self.cellForPlant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.plant)
        case 5:
            return self.cellForStorageLocation(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.storageLocation)
        case 6:
            return self.cellForBatch(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.batch)
        case 7:
            return self.cellForGoodsMovementType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.goodsMovementType)
        case 8:
            return self.cellForInventoryStockType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.inventoryStockType)
        case 9:
            return self.cellForInventoryValuationType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.inventoryValuationType)
        case 10:
            return self.cellForInventorySpecialStockType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.inventorySpecialStockType)
        case 11:
            return self.cellForSupplier(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.supplier)
        case 12:
            return self.cellForCustomer(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.customer)
        case 13:
            return self.cellForSalesOrder(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.salesOrder)
        case 14:
            return self.cellForSalesOrderItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.salesOrderItem)
        case 15:
            return self.cellForSalesOrderScheduleLine(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.salesOrderScheduleLine)
        case 16:
            return self.cellForPurchaseOrder(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.purchaseOrder)
        case 17:
            return self.cellForPurchaseOrderItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.purchaseOrderItem)
        case 18:
            return self.cellForWbsElement(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.wbsElement)
        case 19:
            return self.cellForManufacturingOrder(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.manufacturingOrder)
        case 20:
            return self.cellForManufacturingOrderItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.manufacturingOrderItem)
        case 21:
            return self.cellForGoodsMovementRefDocType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.goodsMovementRefDocType)
        case 22:
            return self.cellForGoodsMovementReasonCode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.goodsMovementReasonCode)
        case 23:
            return self.cellForAccountAssignmentCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.accountAssignmentCategory)
        case 24:
            return self.cellForCostCenter(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.costCenter)
        case 25:
            return self.cellForControllingArea(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.controllingArea)
        case 26:
            return self.cellForCostObject(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.costObject)
        case 27:
            return self.cellForProfitabilitySegment(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.profitabilitySegment)
        case 28:
            return self.cellForProfitCenter(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.profitCenter)
        case 29:
            return self.cellForGlAccount(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.glAccount)
        case 30:
            return self.cellForFunctionalArea(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.functionalArea)
        case 31:
            return self.cellForMaterialBaseUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.materialBaseUnit)
        case 32:
            return self.cellForQuantityInBaseUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.quantityInBaseUnit)
        case 33:
            return self.cellForEntryUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.entryUnit)
        case 34:
            return self.cellForQuantityInEntryUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.quantityInEntryUnit)
        case 35:
            return self.cellForCompanyCodeCurrency(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.companyCodeCurrency)
        case 36:
            return self.cellForGdsMvtExtAmtInCoCodeCrcy(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.gdsMvtExtAmtInCoCodeCrcy)
        case 37:
            return self.cellForSlsPrcAmtInclVATInCoCodeCrcy(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.slsPrcAmtInclVATInCoCodeCrcy)
        case 38:
            return self.cellForFiscalYear(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.fiscalYear)
        case 39:
            return self.cellForFiscalYearPeriod(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.fiscalYearPeriod)
        case 40:
            return self.cellForFiscalYearVariant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.fiscalYearVariant)
        case 41:
            return self.cellForIssgOrRcvgMaterial(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.issgOrRcvgMaterial)
        case 42:
            return self.cellForIssgOrRcvgBatch(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.issgOrRcvgBatch)
        case 43:
            return self.cellForIssuingOrReceivingPlant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.issuingOrReceivingPlant)
        case 44:
            return self.cellForIssuingOrReceivingStorageLoc(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.issuingOrReceivingStorageLoc)
        case 45:
            return self.cellForIssuingOrReceivingStockType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.issuingOrReceivingStockType)
        case 46:
            return self.cellForIssgOrRcvgSpclStockInd(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.issgOrRcvgSpclStockInd)
        case 47:
            return self.cellForIssuingOrReceivingValType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.issuingOrReceivingValType)
        case 48:
            return self.cellForIsCompletelyDelivered(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.isCompletelyDelivered)
        case 49:
            return self.cellForMaterialDocumentItemText(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.materialDocumentItemText)
        case 50:
            return self.cellForUnloadingPointName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.unloadingPointName)
        case 51:
            return self.cellForShelfLifeExpirationDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.shelfLifeExpirationDate)
        case 52:
            return self.cellForManufactureDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.manufactureDate)
        case 53:
            return self.cellForReservation(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.reservation)
        case 54:
            return self.cellForReservationItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.reservationItem)
        case 55:
            return self.cellForReservationIsFinallyIssued(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.reservationIsFinallyIssued)
        case 56:
            return self.cellForSpecialStockIdfgSalesOrder(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.specialStockIdfgSalesOrder)
        case 57:
            return self.cellForSpecialStockIdfgSalesOrderItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.specialStockIdfgSalesOrderItem)
        case 58:
            return self.cellForSpecialStockIdfgWBSElement(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.specialStockIdfgWBSElement)
        case 59:
            return self.cellForIsAutomaticallyCreated(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.isAutomaticallyCreated)
        case 60:
            return self.cellForMaterialDocumentLine(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.materialDocumentLine)
        case 61:
            return self.cellForMaterialDocumentParentLine(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.materialDocumentParentLine)
        case 62:
            return self.cellForHierarchyNodeLevel(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.hierarchyNodeLevel)
        case 63:
            return self.cellForGoodsMovementIsCancelled(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.goodsMovementIsCancelled)
        case 64:
            return self.cellForReversedMaterialDocumentYear(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.reversedMaterialDocumentYear)
        case 65:
            return self.cellForReversedMaterialDocument(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.reversedMaterialDocument)
        case 66:
            return self.cellForReversedMaterialDocumentItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AMaterialDocumentItemType.reversedMaterialDocumentItem)
        default:
            return UITableViewCell()
        }
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 67
    }

    override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.preventNavigationLoop {
            AlertHelper.displayAlert(with: NSLocalizedString("keyAlertNavigationLoop", value: "No further navigation is possible.", comment: "XTIT: Title of alert message about preventing navigation loop."), error: nil, viewController: self)
            return
        }
        switch indexPath.row {
        default:
            return
        }
    }

    // MARK: - OData property specific cell creators

    private func cellForMaterialDocumentYear(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialDocumentYear {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialDocumentYear = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.materialDocumentYear.isOptional || newValue != "" {
                    currentEntity.materialDocumentYear = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialDocument(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialDocument {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialDocument = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.materialDocument.isOptional || newValue != "" {
                    currentEntity.materialDocument = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialDocumentItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialDocumentItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialDocumentItem = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.materialDocumentItem.isOptional || newValue != "" {
                    currentEntity.materialDocumentItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterial(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.material {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.material = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.material.isOptional || newValue != "" {
                    currentEntity.material = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPlant(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.plant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.plant = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.plant.isOptional || newValue != "" {
                    currentEntity.plant = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStorageLocation(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.storageLocation {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.storageLocation = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.storageLocation.isOptional || newValue != "" {
                    currentEntity.storageLocation = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBatch(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.batch {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.batch = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.batch.isOptional || newValue != "" {
                    currentEntity.batch = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGoodsMovementType(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.goodsMovementType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.goodsMovementType = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.goodsMovementType.isOptional || newValue != "" {
                    currentEntity.goodsMovementType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInventoryStockType(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.inventoryStockType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.inventoryStockType = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.inventoryStockType.isOptional || newValue != "" {
                    currentEntity.inventoryStockType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInventoryValuationType(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.inventoryValuationType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.inventoryValuationType = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.inventoryValuationType.isOptional || newValue != "" {
                    currentEntity.inventoryValuationType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInventorySpecialStockType(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.inventorySpecialStockType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.inventorySpecialStockType = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.inventorySpecialStockType.isOptional || newValue != "" {
                    currentEntity.inventorySpecialStockType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSupplier(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.supplier {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.supplier = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.supplier.isOptional || newValue != "" {
                    currentEntity.supplier = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCustomer(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.customer {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.customer = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.customer.isOptional || newValue != "" {
                    currentEntity.customer = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSalesOrder(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.salesOrder {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.salesOrder = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.salesOrder.isOptional || newValue != "" {
                    currentEntity.salesOrder = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSalesOrderItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.salesOrderItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.salesOrderItem = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.salesOrderItem.isOptional || newValue != "" {
                    currentEntity.salesOrderItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSalesOrderScheduleLine(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.salesOrderScheduleLine {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.salesOrderScheduleLine = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.salesOrderScheduleLine.isOptional || newValue != "" {
                    currentEntity.salesOrderScheduleLine = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPurchaseOrder(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.purchaseOrder {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.purchaseOrder = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.purchaseOrder.isOptional || newValue != "" {
                    currentEntity.purchaseOrder = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPurchaseOrderItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.purchaseOrderItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.purchaseOrderItem = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.purchaseOrderItem.isOptional || newValue != "" {
                    currentEntity.purchaseOrderItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWbsElement(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.wbsElement {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.wbsElement = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.wbsElement.isOptional || newValue != "" {
                    currentEntity.wbsElement = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForManufacturingOrder(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.manufacturingOrder {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.manufacturingOrder = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.manufacturingOrder.isOptional || newValue != "" {
                    currentEntity.manufacturingOrder = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForManufacturingOrderItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.manufacturingOrderItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.manufacturingOrderItem = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.manufacturingOrderItem.isOptional || newValue != "" {
                    currentEntity.manufacturingOrderItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGoodsMovementRefDocType(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.goodsMovementRefDocType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.goodsMovementRefDocType = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.goodsMovementRefDocType.isOptional || newValue != "" {
                    currentEntity.goodsMovementRefDocType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGoodsMovementReasonCode(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.goodsMovementReasonCode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.goodsMovementReasonCode = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.goodsMovementReasonCode.isOptional || newValue != "" {
                    currentEntity.goodsMovementReasonCode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAccountAssignmentCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.accountAssignmentCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.accountAssignmentCategory = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.accountAssignmentCategory.isOptional || newValue != "" {
                    currentEntity.accountAssignmentCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCostCenter(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.costCenter {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.costCenter = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.costCenter.isOptional || newValue != "" {
                    currentEntity.costCenter = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForControllingArea(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.controllingArea {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.controllingArea = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.controllingArea.isOptional || newValue != "" {
                    currentEntity.controllingArea = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCostObject(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.costObject {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.costObject = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.costObject.isOptional || newValue != "" {
                    currentEntity.costObject = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProfitabilitySegment(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.profitabilitySegment {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.profitabilitySegment = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.profitabilitySegment.isOptional || newValue != "" {
                    currentEntity.profitabilitySegment = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProfitCenter(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.profitCenter {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.profitCenter = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.profitCenter.isOptional || newValue != "" {
                    currentEntity.profitCenter = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGlAccount(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.glAccount {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.glAccount = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.glAccount.isOptional || newValue != "" {
                    currentEntity.glAccount = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForFunctionalArea(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.functionalArea {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.functionalArea = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.functionalArea.isOptional || newValue != "" {
                    currentEntity.functionalArea = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialBaseUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialBaseUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialBaseUnit = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.materialBaseUnit.isOptional || newValue != "" {
                    currentEntity.materialBaseUnit = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForQuantityInBaseUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.quantityInBaseUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.quantityInBaseUnit = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.quantityInBaseUnit = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForEntryUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.entryUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.entryUnit = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.entryUnit.isOptional || newValue != "" {
                    currentEntity.entryUnit = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForQuantityInEntryUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.quantityInEntryUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.quantityInEntryUnit = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.quantityInEntryUnit = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCompanyCodeCurrency(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.companyCodeCurrency {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.companyCodeCurrency = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.companyCodeCurrency.isOptional || newValue != "" {
                    currentEntity.companyCodeCurrency = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGdsMvtExtAmtInCoCodeCrcy(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.gdsMvtExtAmtInCoCodeCrcy {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.gdsMvtExtAmtInCoCodeCrcy = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.gdsMvtExtAmtInCoCodeCrcy = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSlsPrcAmtInclVATInCoCodeCrcy(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.slsPrcAmtInclVATInCoCodeCrcy {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.slsPrcAmtInclVATInCoCodeCrcy = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.slsPrcAmtInclVATInCoCodeCrcy = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForFiscalYear(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.fiscalYear {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.fiscalYear = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.fiscalYear.isOptional || newValue != "" {
                    currentEntity.fiscalYear = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForFiscalYearPeriod(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.fiscalYearPeriod {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.fiscalYearPeriod = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.fiscalYearPeriod.isOptional || newValue != "" {
                    currentEntity.fiscalYearPeriod = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForFiscalYearVariant(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.fiscalYearVariant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.fiscalYearVariant = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.fiscalYearVariant.isOptional || newValue != "" {
                    currentEntity.fiscalYearVariant = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgMaterial(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgMaterial {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgMaterial = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.issgOrRcvgMaterial.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgMaterial = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgBatch(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgBatch {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgBatch = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.issgOrRcvgBatch.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgBatch = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssuingOrReceivingPlant(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issuingOrReceivingPlant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issuingOrReceivingPlant = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.issuingOrReceivingPlant.isOptional || newValue != "" {
                    currentEntity.issuingOrReceivingPlant = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssuingOrReceivingStorageLoc(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issuingOrReceivingStorageLoc {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issuingOrReceivingStorageLoc = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.issuingOrReceivingStorageLoc.isOptional || newValue != "" {
                    currentEntity.issuingOrReceivingStorageLoc = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssuingOrReceivingStockType(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issuingOrReceivingStockType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issuingOrReceivingStockType = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.issuingOrReceivingStockType.isOptional || newValue != "" {
                    currentEntity.issuingOrReceivingStockType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgSpclStockInd(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgSpclStockInd {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgSpclStockInd = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.issgOrRcvgSpclStockInd.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgSpclStockInd = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssuingOrReceivingValType(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issuingOrReceivingValType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issuingOrReceivingValType = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.issuingOrReceivingValType.isOptional || newValue != "" {
                    currentEntity.issuingOrReceivingValType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIsCompletelyDelivered(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.isCompletelyDelivered {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.isCompletelyDelivered = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.isCompletelyDelivered = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialDocumentItemText(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialDocumentItemText {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialDocumentItemText = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.materialDocumentItemText.isOptional || newValue != "" {
                    currentEntity.materialDocumentItemText = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForUnloadingPointName(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.unloadingPointName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.unloadingPointName = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.unloadingPointName.isOptional || newValue != "" {
                    currentEntity.unloadingPointName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForShelfLifeExpirationDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.shelfLifeExpirationDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.shelfLifeExpirationDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.shelfLifeExpirationDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForManufactureDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.manufactureDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.manufactureDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.manufactureDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReservation(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.reservation {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.reservation = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.reservation.isOptional || newValue != "" {
                    currentEntity.reservation = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReservationItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.reservationItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.reservationItem = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.reservationItem.isOptional || newValue != "" {
                    currentEntity.reservationItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReservationIsFinallyIssued(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.reservationIsFinallyIssued {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.reservationIsFinallyIssued = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.reservationIsFinallyIssued = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSpecialStockIdfgSalesOrder(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.specialStockIdfgSalesOrder {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.specialStockIdfgSalesOrder = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.specialStockIdfgSalesOrder.isOptional || newValue != "" {
                    currentEntity.specialStockIdfgSalesOrder = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSpecialStockIdfgSalesOrderItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.specialStockIdfgSalesOrderItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.specialStockIdfgSalesOrderItem = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.specialStockIdfgSalesOrderItem.isOptional || newValue != "" {
                    currentEntity.specialStockIdfgSalesOrderItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSpecialStockIdfgWBSElement(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.specialStockIdfgWBSElement {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.specialStockIdfgWBSElement = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.specialStockIdfgWBSElement.isOptional || newValue != "" {
                    currentEntity.specialStockIdfgWBSElement = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIsAutomaticallyCreated(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.isAutomaticallyCreated {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.isAutomaticallyCreated = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.isAutomaticallyCreated.isOptional || newValue != "" {
                    currentEntity.isAutomaticallyCreated = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialDocumentLine(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialDocumentLine {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialDocumentLine = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.materialDocumentLine.isOptional || newValue != "" {
                    currentEntity.materialDocumentLine = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialDocumentParentLine(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialDocumentParentLine {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialDocumentParentLine = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.materialDocumentParentLine.isOptional || newValue != "" {
                    currentEntity.materialDocumentParentLine = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHierarchyNodeLevel(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.hierarchyNodeLevel {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.hierarchyNodeLevel = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.hierarchyNodeLevel.isOptional || newValue != "" {
                    currentEntity.hierarchyNodeLevel = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGoodsMovementIsCancelled(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.goodsMovementIsCancelled {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.goodsMovementIsCancelled = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.goodsMovementIsCancelled = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReversedMaterialDocumentYear(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.reversedMaterialDocumentYear {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.reversedMaterialDocumentYear = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.reversedMaterialDocumentYear.isOptional || newValue != "" {
                    currentEntity.reversedMaterialDocumentYear = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReversedMaterialDocument(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.reversedMaterialDocument {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.reversedMaterialDocument = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.reversedMaterialDocument.isOptional || newValue != "" {
                    currentEntity.reversedMaterialDocument = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReversedMaterialDocumentItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AMaterialDocumentItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.reversedMaterialDocumentItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.reversedMaterialDocumentItem = nil
                isNewValueValid = true
            } else {
                if AMaterialDocumentItemType.reversedMaterialDocumentItem.isOptional || newValue != "" {
                    currentEntity.reversedMaterialDocumentItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    // MARK: - OData functionalities

    @objc func createEntity() {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Creating entity in backend.")
        self.ec1.createEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Create entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityCreationTitle", value: "Create entry failed", comment: "XTIT: Title of alert message about entity creation error."), error: error, viewController: self)
                return
            }
            self.logger.info("Create entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Created", comment: "XMSG: Title of alert message about successful entity creation."))
                    self.tableUpdater?.entitySetHasChanged()
                }
            }
        }
    }

    func createEntityWithDefaultValues() -> AMaterialDocumentItemType {
        let newEntity = AMaterialDocumentItemType()

        // Key properties without default value should be invalid by default for Create scenario
        if newEntity.materialDocumentYear == nil || newEntity.materialDocumentYear!.isEmpty {
            self.validity["MaterialDocumentYear"] = false
        }
        if newEntity.materialDocument == nil || newEntity.materialDocument!.isEmpty {
            self.validity["MaterialDocument"] = false
        }
        if newEntity.materialDocumentItem == nil || newEntity.materialDocumentItem!.isEmpty {
            self.validity["MaterialDocumentItem"] = false
        }

        self.barButtonShouldBeEnabled()
        return newEntity
    }

    @objc func updateEntity(_: AnyObject) {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Updating entity in backend.")
        self.ec1.updateEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Update entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityUpdateTitle", value: "Update entry failed", comment: "XTIT: Title of alert message about entity update failure."), error: error, viewController: self)
                return
            }
            self.logger.info("Update entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyUpdateEntityFinishedTitle", value: "Updated", comment: "XTIT: Title of alert message about successful entity update."))
                    self.entityUpdater?.entityHasChanged(self.entity)
                }
            }
        }
    }

    // MARK: - other logic, helper

    @objc func cancel() {
        DispatchQueue.main.async {
            self.dismiss(animated: true)
        }
    }

    // Check if all text fields are valid
    private func barButtonShouldBeEnabled() {
        let anyFieldInvalid = self.validity.values.first { field in
            field == false
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = anyFieldInvalid == nil
    }
}

extension AMaterialDocumentItemTypeDetailViewController: CPIKIMODataEntityUpdaterDelegate {
    func entityHasChanged(_ entityValue: EntityValue?) {
        if let entity = entityValue {
            let currentEntity = entity as! AMaterialDocumentItemType
            self.entity = currentEntity
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
}
