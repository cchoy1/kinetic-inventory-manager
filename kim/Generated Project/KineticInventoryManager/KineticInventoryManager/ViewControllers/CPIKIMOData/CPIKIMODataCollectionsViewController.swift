//
// KineticInventoryManager
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 30/03/20
//

import SAPFiori
import SAPFioriFlows
import SAPFoundation
import SAPOData

protocol CPIKIMODataEntityUpdaterDelegate {
    func entityHasChanged(_ entity: EntityValue?)
}

protocol CPIKIMODataEntitySetUpdaterDelegate {
    func entitySetHasChanged()
}

class CPIKIMODataCollectionsViewController: FUIFormTableViewController {
    private var collections = CPIKIMODataCollectionType.all
    let destinations = FileConfigurationProvider("AppParameters").provideConfiguration().configuration["Destinations"] as! NSDictionary

    // Variable to store the selected index path
    private var selectedIndex: IndexPath?

    private let okTitle = NSLocalizedString("keyOkButtonTitle",
                                            value: "OK",
                                            comment: "XBUT: Title of OK button.")

    var isPresentedInSplitView: Bool {
        return !(self.splitViewController?.isCollapsed ?? true)
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredContentSize = CGSize(width: 320, height: 480)

        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.makeSelection()
    }

    override func viewWillTransition(to _: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: nil, completion: { _ in
            let isNotInSplitView = !self.isPresentedInSplitView
            self.tableView.visibleCells.forEach { cell in
                // To refresh the disclosure indicator of each cell
                cell.accessoryType = isNotInSplitView ? .disclosureIndicator : .none
            }
            self.makeSelection()
        })
    }

    // MARK: - UITableViewDelegate

    override func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return collections.count
    }

    override func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return 44
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FUIObjectTableViewCell.reuseIdentifier, for: indexPath) as! FUIObjectTableViewCell
        cell.headlineLabel.text = self.collections[indexPath.row].rawValue
        cell.accessoryType = !self.isPresentedInSplitView ? .disclosureIndicator : .none
        cell.isMomentarySelection = false
        return cell
    }

    override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.collectionSelected(at: indexPath)
    }

    // CollectionType selection helper
    private func collectionSelected(at indexPath: IndexPath) {
        // Load the EntityType specific ViewController from the specific storyboard"
        var masterViewController: UIViewController!
        guard let odataController = OnboardingSessionManager.shared.onboardingSession?.odataControllers[destinations["CPIKIMOData"] as! String] as? CPIKIMODataOnlineODataController, let ec1 = odataController.ec1 else {
            AlertHelper.displayAlert(with: "OData service is not reachable, please onboard again.", error: nil, viewController: self)
            return
        }
        self.selectedIndex = indexPath

        switch self.collections[indexPath.row] {
        case .aMaterialDocumentHeader:
            let aMaterialDocumentHeaderTypeStoryBoard = UIStoryboard(name: "AMaterialDocumentHeaderType", bundle: nil)
            let aMaterialDocumentHeaderTypeMasterViewController = aMaterialDocumentHeaderTypeStoryBoard.instantiateViewController(withIdentifier: "AMaterialDocumentHeaderTypeMaster") as! AMaterialDocumentHeaderTypeMasterViewController
            aMaterialDocumentHeaderTypeMasterViewController.ec1 = ec1
            aMaterialDocumentHeaderTypeMasterViewController.entitySetName = "AMaterialDocumentHeader"
            func fetchAMaterialDocumentHeader(_ completionHandler: @escaping ([AMaterialDocumentHeaderType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAMaterialDocumentHeader(matching: query, completionHandler: completionHandler)
                }
            }
            aMaterialDocumentHeaderTypeMasterViewController.loadEntitiesBlock = fetchAMaterialDocumentHeader
            aMaterialDocumentHeaderTypeMasterViewController.navigationItem.title = "AMaterialDocumentHeaderType"
            masterViewController = aMaterialDocumentHeaderTypeMasterViewController
        case .aMaterialDocumentItemTypeSet:
            let aMaterialDocumentItemTypeStoryBoard = UIStoryboard(name: "AMaterialDocumentItemType", bundle: nil)
            let aMaterialDocumentItemTypeMasterViewController = aMaterialDocumentItemTypeStoryBoard.instantiateViewController(withIdentifier: "AMaterialDocumentItemTypeMaster") as! AMaterialDocumentItemTypeMasterViewController
            aMaterialDocumentItemTypeMasterViewController.ec1 = ec1
            aMaterialDocumentItemTypeMasterViewController.entitySetName = "AMaterialDocumentItemTypeSet"
            func fetchAMaterialDocumentItemTypeSet(_ completionHandler: @escaping ([AMaterialDocumentItemType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAMaterialDocumentItemTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            aMaterialDocumentItemTypeMasterViewController.loadEntitiesBlock = fetchAMaterialDocumentItemTypeSet
            aMaterialDocumentItemTypeMasterViewController.navigationItem.title = "AMaterialDocumentItemType"
            masterViewController = aMaterialDocumentItemTypeMasterViewController
        case .aOutbDeliveryAddressTypeSet:
            let aOutbDeliveryAddressTypeStoryBoard = UIStoryboard(name: "AOutbDeliveryAddressType", bundle: nil)
            let aOutbDeliveryAddressTypeMasterViewController = aOutbDeliveryAddressTypeStoryBoard.instantiateViewController(withIdentifier: "AOutbDeliveryAddressTypeMaster") as! AOutbDeliveryAddressTypeMasterViewController
            aOutbDeliveryAddressTypeMasterViewController.ec1 = ec1
            aOutbDeliveryAddressTypeMasterViewController.entitySetName = "AOutbDeliveryAddressTypeSet"
            func fetchAOutbDeliveryAddressTypeSet(_ completionHandler: @escaping ([AOutbDeliveryAddressType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAOutbDeliveryAddressTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            aOutbDeliveryAddressTypeMasterViewController.loadEntitiesBlock = fetchAOutbDeliveryAddressTypeSet
            aOutbDeliveryAddressTypeMasterViewController.navigationItem.title = "AOutbDeliveryAddressType"
            masterViewController = aOutbDeliveryAddressTypeMasterViewController
        case .aOutbDeliveryPartnerTypeSet:
            let aOutbDeliveryPartnerTypeStoryBoard = UIStoryboard(name: "AOutbDeliveryPartnerType", bundle: nil)
            let aOutbDeliveryPartnerTypeMasterViewController = aOutbDeliveryPartnerTypeStoryBoard.instantiateViewController(withIdentifier: "AOutbDeliveryPartnerTypeMaster") as! AOutbDeliveryPartnerTypeMasterViewController
            aOutbDeliveryPartnerTypeMasterViewController.ec1 = ec1
            aOutbDeliveryPartnerTypeMasterViewController.entitySetName = "AOutbDeliveryPartnerTypeSet"
            func fetchAOutbDeliveryPartnerTypeSet(_ completionHandler: @escaping ([AOutbDeliveryPartnerType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAOutbDeliveryPartnerTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            aOutbDeliveryPartnerTypeMasterViewController.loadEntitiesBlock = fetchAOutbDeliveryPartnerTypeSet
            aOutbDeliveryPartnerTypeMasterViewController.navigationItem.title = "AOutbDeliveryPartnerType"
            masterViewController = aOutbDeliveryPartnerTypeMasterViewController
        case .aInbDeliverySerialNmbrTypeSet:
            let aInbDeliverySerialNmbrTypeStoryBoard = UIStoryboard(name: "AInbDeliverySerialNmbrType", bundle: nil)
            let aInbDeliverySerialNmbrTypeMasterViewController = aInbDeliverySerialNmbrTypeStoryBoard.instantiateViewController(withIdentifier: "AInbDeliverySerialNmbrTypeMaster") as! AInbDeliverySerialNmbrTypeMasterViewController
            aInbDeliverySerialNmbrTypeMasterViewController.ec1 = ec1
            aInbDeliverySerialNmbrTypeMasterViewController.entitySetName = "AInbDeliverySerialNmbrTypeSet"
            func fetchAInbDeliverySerialNmbrTypeSet(_ completionHandler: @escaping ([AInbDeliverySerialNmbrType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAInbDeliverySerialNmbrTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            aInbDeliverySerialNmbrTypeMasterViewController.loadEntitiesBlock = fetchAInbDeliverySerialNmbrTypeSet
            aInbDeliverySerialNmbrTypeMasterViewController.navigationItem.title = "AInbDeliverySerialNmbrType"
            masterViewController = aInbDeliverySerialNmbrTypeMasterViewController
        case .aSerialNmbrDeliveryTypeSet:
            let aSerialNmbrDeliveryTypeStoryBoard = UIStoryboard(name: "ASerialNmbrDeliveryType", bundle: nil)
            let aSerialNmbrDeliveryTypeMasterViewController = aSerialNmbrDeliveryTypeStoryBoard.instantiateViewController(withIdentifier: "ASerialNmbrDeliveryTypeMaster") as! ASerialNmbrDeliveryTypeMasterViewController
            aSerialNmbrDeliveryTypeMasterViewController.ec1 = ec1
            aSerialNmbrDeliveryTypeMasterViewController.entitySetName = "ASerialNmbrDeliveryTypeSet"
            func fetchASerialNmbrDeliveryTypeSet(_ completionHandler: @escaping ([ASerialNmbrDeliveryType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchASerialNmbrDeliveryTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            aSerialNmbrDeliveryTypeMasterViewController.loadEntitiesBlock = fetchASerialNmbrDeliveryTypeSet
            aSerialNmbrDeliveryTypeMasterViewController.navigationItem.title = "ASerialNmbrDeliveryType"
            masterViewController = aSerialNmbrDeliveryTypeMasterViewController
        case .aMaintenanceItemObjListTypeSet:
            let aMaintenanceItemObjListTypeStoryBoard = UIStoryboard(name: "AMaintenanceItemObjListType", bundle: nil)
            let aMaintenanceItemObjListTypeMasterViewController = aMaintenanceItemObjListTypeStoryBoard.instantiateViewController(withIdentifier: "AMaintenanceItemObjListTypeMaster") as! AMaintenanceItemObjListTypeMasterViewController
            aMaintenanceItemObjListTypeMasterViewController.ec1 = ec1
            aMaintenanceItemObjListTypeMasterViewController.entitySetName = "AMaintenanceItemObjListTypeSet"
            func fetchAMaintenanceItemObjListTypeSet(_ completionHandler: @escaping ([AMaintenanceItemObjListType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAMaintenanceItemObjListTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            aMaintenanceItemObjListTypeMasterViewController.loadEntitiesBlock = fetchAMaintenanceItemObjListTypeSet
            aMaintenanceItemObjListTypeMasterViewController.navigationItem.title = "AMaintenanceItemObjListType"
            masterViewController = aMaintenanceItemObjListTypeMasterViewController
        case .aInbDeliveryPartner:
            let aInbDeliveryPartnerTypeStoryBoard = UIStoryboard(name: "AInbDeliveryPartnerType", bundle: nil)
            let aInbDeliveryPartnerTypeMasterViewController = aInbDeliveryPartnerTypeStoryBoard.instantiateViewController(withIdentifier: "AInbDeliveryPartnerTypeMaster") as! AInbDeliveryPartnerTypeMasterViewController
            aInbDeliveryPartnerTypeMasterViewController.ec1 = ec1
            aInbDeliveryPartnerTypeMasterViewController.entitySetName = "AInbDeliveryPartner"
            func fetchAInbDeliveryPartner(_ completionHandler: @escaping ([AInbDeliveryPartnerType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAInbDeliveryPartner(matching: query, completionHandler: completionHandler)
                }
            }
            aInbDeliveryPartnerTypeMasterViewController.loadEntitiesBlock = fetchAInbDeliveryPartner
            aInbDeliveryPartnerTypeMasterViewController.navigationItem.title = "AInbDeliveryPartnerType"
            masterViewController = aInbDeliveryPartnerTypeMasterViewController
        case .aInbDeliveryItem:
            let aInbDeliveryItemTypeStoryBoard = UIStoryboard(name: "AInbDeliveryItemType", bundle: nil)
            let aInbDeliveryItemTypeMasterViewController = aInbDeliveryItemTypeStoryBoard.instantiateViewController(withIdentifier: "AInbDeliveryItemTypeMaster") as! AInbDeliveryItemTypeMasterViewController
            aInbDeliveryItemTypeMasterViewController.ec1 = ec1
            aInbDeliveryItemTypeMasterViewController.entitySetName = "AInbDeliveryItem"
            func fetchAInbDeliveryItem(_ completionHandler: @escaping ([AInbDeliveryItemType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAInbDeliveryItem(matching: query, completionHandler: completionHandler)
                }
            }
            aInbDeliveryItemTypeMasterViewController.loadEntitiesBlock = fetchAInbDeliveryItem
            aInbDeliveryItemTypeMasterViewController.navigationItem.title = "AInbDeliveryItemType"
            masterViewController = aInbDeliveryItemTypeMasterViewController
        case .aInbDeliveryAddressTypeSet:
            let aInbDeliveryAddressTypeStoryBoard = UIStoryboard(name: "AInbDeliveryAddressType", bundle: nil)
            let aInbDeliveryAddressTypeMasterViewController = aInbDeliveryAddressTypeStoryBoard.instantiateViewController(withIdentifier: "AInbDeliveryAddressTypeMaster") as! AInbDeliveryAddressTypeMasterViewController
            aInbDeliveryAddressTypeMasterViewController.ec1 = ec1
            aInbDeliveryAddressTypeMasterViewController.entitySetName = "AInbDeliveryAddressTypeSet"
            func fetchAInbDeliveryAddressTypeSet(_ completionHandler: @escaping ([AInbDeliveryAddressType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAInbDeliveryAddressTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            aInbDeliveryAddressTypeMasterViewController.loadEntitiesBlock = fetchAInbDeliveryAddressTypeSet
            aInbDeliveryAddressTypeMasterViewController.navigationItem.title = "AInbDeliveryAddressType"
            masterViewController = aInbDeliveryAddressTypeMasterViewController
        case .aOutbDeliveryDocFlowTypeSet:
            let aOutbDeliveryDocFlowTypeStoryBoard = UIStoryboard(name: "AOutbDeliveryDocFlowType", bundle: nil)
            let aOutbDeliveryDocFlowTypeMasterViewController = aOutbDeliveryDocFlowTypeStoryBoard.instantiateViewController(withIdentifier: "AOutbDeliveryDocFlowTypeMaster") as! AOutbDeliveryDocFlowTypeMasterViewController
            aOutbDeliveryDocFlowTypeMasterViewController.ec1 = ec1
            aOutbDeliveryDocFlowTypeMasterViewController.entitySetName = "AOutbDeliveryDocFlowTypeSet"
            func fetchAOutbDeliveryDocFlowTypeSet(_ completionHandler: @escaping ([AOutbDeliveryDocFlowType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAOutbDeliveryDocFlowTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            aOutbDeliveryDocFlowTypeMasterViewController.loadEntitiesBlock = fetchAOutbDeliveryDocFlowTypeSet
            aOutbDeliveryDocFlowTypeMasterViewController.navigationItem.title = "AOutbDeliveryDocFlowType"
            masterViewController = aOutbDeliveryDocFlowTypeMasterViewController
        case .aInbDeliveryHeaderTextTypeSet:
            let aInbDeliveryHeaderTextTypeStoryBoard = UIStoryboard(name: "AInbDeliveryHeaderTextType", bundle: nil)
            let aInbDeliveryHeaderTextTypeMasterViewController = aInbDeliveryHeaderTextTypeStoryBoard.instantiateViewController(withIdentifier: "AInbDeliveryHeaderTextTypeMaster") as! AInbDeliveryHeaderTextTypeMasterViewController
            aInbDeliveryHeaderTextTypeMasterViewController.ec1 = ec1
            aInbDeliveryHeaderTextTypeMasterViewController.entitySetName = "AInbDeliveryHeaderTextTypeSet"
            func fetchAInbDeliveryHeaderTextTypeSet(_ completionHandler: @escaping ([AInbDeliveryHeaderTextType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAInbDeliveryHeaderTextTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            aInbDeliveryHeaderTextTypeMasterViewController.loadEntitiesBlock = fetchAInbDeliveryHeaderTextTypeSet
            aInbDeliveryHeaderTextTypeMasterViewController.navigationItem.title = "AInbDeliveryHeaderTextType"
            masterViewController = aInbDeliveryHeaderTextTypeMasterViewController
        case .aPlant:
            let aPlantTypeStoryBoard = UIStoryboard(name: "APlantType", bundle: nil)
            let aPlantTypeMasterViewController = aPlantTypeStoryBoard.instantiateViewController(withIdentifier: "APlantTypeMaster") as! APlantTypeMasterViewController
            aPlantTypeMasterViewController.ec1 = ec1
            aPlantTypeMasterViewController.entitySetName = "APlant"
            func fetchAPlant(_ completionHandler: @escaping ([APlantType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAPlant(matching: query, completionHandler: completionHandler)
                }
            }
            aPlantTypeMasterViewController.loadEntitiesBlock = fetchAPlant
            aPlantTypeMasterViewController.navigationItem.title = "APlantType"
            masterViewController = aPlantTypeMasterViewController
        case .aOutbDeliveryItem:
            let aOutbDeliveryItemTypeStoryBoard = UIStoryboard(name: "AOutbDeliveryItemType", bundle: nil)
            let aOutbDeliveryItemTypeMasterViewController = aOutbDeliveryItemTypeStoryBoard.instantiateViewController(withIdentifier: "AOutbDeliveryItemTypeMaster") as! AOutbDeliveryItemTypeMasterViewController
            aOutbDeliveryItemTypeMasterViewController.ec1 = ec1
            aOutbDeliveryItemTypeMasterViewController.entitySetName = "AOutbDeliveryItem"
            func fetchAOutbDeliveryItem(_ completionHandler: @escaping ([AOutbDeliveryItemType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAOutbDeliveryItem(matching: query, completionHandler: completionHandler)
                }
            }
            aOutbDeliveryItemTypeMasterViewController.loadEntitiesBlock = fetchAOutbDeliveryItem
            aOutbDeliveryItemTypeMasterViewController.navigationItem.title = "AOutbDeliveryItemType"
            masterViewController = aOutbDeliveryItemTypeMasterViewController
        case .aInbDeliveryDocFlowTypeSet:
            let aInbDeliveryDocFlowTypeStoryBoard = UIStoryboard(name: "AInbDeliveryDocFlowType", bundle: nil)
            let aInbDeliveryDocFlowTypeMasterViewController = aInbDeliveryDocFlowTypeStoryBoard.instantiateViewController(withIdentifier: "AInbDeliveryDocFlowTypeMaster") as! AInbDeliveryDocFlowTypeMasterViewController
            aInbDeliveryDocFlowTypeMasterViewController.ec1 = ec1
            aInbDeliveryDocFlowTypeMasterViewController.entitySetName = "AInbDeliveryDocFlowTypeSet"
            func fetchAInbDeliveryDocFlowTypeSet(_ completionHandler: @escaping ([AInbDeliveryDocFlowType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAInbDeliveryDocFlowTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            aInbDeliveryDocFlowTypeMasterViewController.loadEntitiesBlock = fetchAInbDeliveryDocFlowTypeSet
            aInbDeliveryDocFlowTypeMasterViewController.navigationItem.title = "AInbDeliveryDocFlowType"
            masterViewController = aInbDeliveryDocFlowTypeMasterViewController
        case .aOutbDeliveryHeader:
            let aOutbDeliveryHeaderTypeStoryBoard = UIStoryboard(name: "AOutbDeliveryHeaderType", bundle: nil)
            let aOutbDeliveryHeaderTypeMasterViewController = aOutbDeliveryHeaderTypeStoryBoard.instantiateViewController(withIdentifier: "AOutbDeliveryHeaderTypeMaster") as! AOutbDeliveryHeaderTypeMasterViewController
            aOutbDeliveryHeaderTypeMasterViewController.ec1 = ec1
            aOutbDeliveryHeaderTypeMasterViewController.entitySetName = "AOutbDeliveryHeader"
            func fetchAOutbDeliveryHeader(_ completionHandler: @escaping ([AOutbDeliveryHeaderType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAOutbDeliveryHeader(matching: query, completionHandler: completionHandler)
                }
            }
            aOutbDeliveryHeaderTypeMasterViewController.loadEntitiesBlock = fetchAOutbDeliveryHeader
            aOutbDeliveryHeaderTypeMasterViewController.navigationItem.title = "AOutbDeliveryHeaderType"
            masterViewController = aOutbDeliveryHeaderTypeMasterViewController
        case .aInbDeliveryItemTextTypeSet:
            let aInbDeliveryItemTextTypeStoryBoard = UIStoryboard(name: "AInbDeliveryItemTextType", bundle: nil)
            let aInbDeliveryItemTextTypeMasterViewController = aInbDeliveryItemTextTypeStoryBoard.instantiateViewController(withIdentifier: "AInbDeliveryItemTextTypeMaster") as! AInbDeliveryItemTextTypeMasterViewController
            aInbDeliveryItemTextTypeMasterViewController.ec1 = ec1
            aInbDeliveryItemTextTypeMasterViewController.entitySetName = "AInbDeliveryItemTextTypeSet"
            func fetchAInbDeliveryItemTextTypeSet(_ completionHandler: @escaping ([AInbDeliveryItemTextType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAInbDeliveryItemTextTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            aInbDeliveryItemTextTypeMasterViewController.loadEntitiesBlock = fetchAInbDeliveryItemTextTypeSet
            aInbDeliveryItemTextTypeMasterViewController.navigationItem.title = "AInbDeliveryItemTextType"
            masterViewController = aInbDeliveryItemTextTypeMasterViewController
        case .aMaintenanceItemObjectTypeSet:
            let aMaintenanceItemObjectTypeStoryBoard = UIStoryboard(name: "AMaintenanceItemObjectType", bundle: nil)
            let aMaintenanceItemObjectTypeMasterViewController = aMaintenanceItemObjectTypeStoryBoard.instantiateViewController(withIdentifier: "AMaintenanceItemObjectTypeMaster") as! AMaintenanceItemObjectTypeMasterViewController
            aMaintenanceItemObjectTypeMasterViewController.ec1 = ec1
            aMaintenanceItemObjectTypeMasterViewController.entitySetName = "AMaintenanceItemObjectTypeSet"
            func fetchAMaintenanceItemObjectTypeSet(_ completionHandler: @escaping ([AMaintenanceItemObjectType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAMaintenanceItemObjectTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            aMaintenanceItemObjectTypeMasterViewController.loadEntitiesBlock = fetchAMaintenanceItemObjectTypeSet
            aMaintenanceItemObjectTypeMasterViewController.navigationItem.title = "AMaintenanceItemObjectType"
            masterViewController = aMaintenanceItemObjectTypeMasterViewController
        case .aInbDeliveryHeader:
            let aInbDeliveryHeaderTypeStoryBoard = UIStoryboard(name: "AInbDeliveryHeaderType", bundle: nil)
            let aInbDeliveryHeaderTypeMasterViewController = aInbDeliveryHeaderTypeStoryBoard.instantiateViewController(withIdentifier: "AInbDeliveryHeaderTypeMaster") as! AInbDeliveryHeaderTypeMasterViewController
            aInbDeliveryHeaderTypeMasterViewController.ec1 = ec1
            aInbDeliveryHeaderTypeMasterViewController.entitySetName = "AInbDeliveryHeader"
            func fetchAInbDeliveryHeader(_ completionHandler: @escaping ([AInbDeliveryHeaderType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    ec1.fetchAInbDeliveryHeader(matching: query, completionHandler: completionHandler)
                }
            }
            aInbDeliveryHeaderTypeMasterViewController.loadEntitiesBlock = fetchAInbDeliveryHeader
            aInbDeliveryHeaderTypeMasterViewController.navigationItem.title = "AInbDeliveryHeaderType"
            masterViewController = aInbDeliveryHeaderTypeMasterViewController
        case .none:
            masterViewController = UIViewController()
        }

        // Load the NavigationController and present with the EntityType specific ViewController
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let rightNavigationController = mainStoryBoard.instantiateViewController(withIdentifier: "RightNavigationController") as! UINavigationController
        rightNavigationController.viewControllers = [masterViewController]
        self.splitViewController?.showDetailViewController(rightNavigationController, sender: nil)
    }

    // MARK: - Handle highlighting of selected cell

    private func makeSelection() {
        if let selectedIndex = selectedIndex {
            tableView.selectRow(at: selectedIndex, animated: true, scrollPosition: .none)
            tableView.scrollToRow(at: selectedIndex, at: .none, animated: true)
        } else {
            selectDefault()
        }
    }

    private func selectDefault() {
        // Automatically select first element if we have two panels (iPhone plus and iPad only)
        guard let odataController = OnboardingSessionManager.shared.onboardingSession?.odataControllers[destinations["CPIKIMOData"] as! String] as? CPIKIMODataOnlineODataController else {
            AlertHelper.displayAlert(with: "OData service is not reachable, please onboard again.", error: nil, viewController: self)
            return
        }

        if self.splitViewController!.isCollapsed || odataController.ec1 == nil {
            return
        }
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        self.collectionSelected(at: indexPath)
    }
}
