//
// KineticInventoryManager
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 30/03/20
//

import Foundation
import SAPCommon
import SAPFiori
import SAPFoundation
import SAPOData

class AOutbDeliveryItemTypeDetailViewController: FUIFormTableViewController, SAPFioriLoadingIndicator {
    var ec1: Ec1<OnlineODataProvider>!
    private var validity = [String: Bool]()
    var allowsEditableCells = false

    private var _entity: AOutbDeliveryItemType?
    var entity: AOutbDeliveryItemType {
        get {
            if self._entity == nil {
                self._entity = self.createEntityWithDefaultValues()
            }
            return self._entity!
        }
        set {
            self._entity = newValue
        }
    }

    private let logger = Logger.shared(named: "AOutbDeliveryItemTypeMasterViewControllerLogger")
    var loadingIndicator: FUILoadingIndicatorView?
    var entityUpdater: CPIKIMODataEntityUpdaterDelegate?
    var tableUpdater: CPIKIMODataEntitySetUpdaterDelegate?
    private let okTitle = NSLocalizedString("keyOkButtonTitle",
                                            value: "OK",
                                            comment: "XBUT: Title of OK button.")
    var preventNavigationLoop = false
    var entitySetName: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "updateEntity" {
            // Show the Detail view with the current entity, where the properties scan be edited and updated
            self.logger.info("Showing a view to update the selected entity.")
            let dest = segue.destination as! UINavigationController
            let detailViewController = dest.viewControllers[0] as! AOutbDeliveryItemTypeDetailViewController
            detailViewController.title = NSLocalizedString("keyUpdateEntityTitle", value: "Update Entity", comment: "XTIT: Title of update selected entity screen.")
            detailViewController.ec1 = self.ec1
            detailViewController.entity = self.entity
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: detailViewController, action: #selector(detailViewController.updateEntity))
            detailViewController.navigationItem.rightBarButtonItem = doneButton
            let cancelButton = UIBarButtonItem(title: NSLocalizedString("keyCancelButtonToGoPreviousScreen", value: "Cancel", comment: "XBUT: Title of Cancel button."), style: .plain, target: detailViewController, action: #selector(detailViewController.cancel))
            detailViewController.navigationItem.leftBarButtonItem = cancelButton
            detailViewController.allowsEditableCells = true
            detailViewController.entityUpdater = self
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return self.cellForActualDeliveredQtyInBaseUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.actualDeliveredQtyInBaseUnit)
        case 1:
            return self.cellForDeliveryVersion(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.deliveryVersion)
        case 2:
            return self.cellForActualDeliveryQuantity(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.actualDeliveryQuantity)
        case 3:
            return self.cellForAdditionalCustomerGroup1(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.additionalCustomerGroup1)
        case 4:
            return self.cellForAdditionalCustomerGroup2(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.additionalCustomerGroup2)
        case 5:
            return self.cellForAdditionalCustomerGroup3(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.additionalCustomerGroup3)
        case 6:
            return self.cellForAdditionalCustomerGroup4(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.additionalCustomerGroup4)
        case 7:
            return self.cellForAdditionalCustomerGroup5(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.additionalCustomerGroup5)
        case 8:
            return self.cellForAdditionalMaterialGroup1(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.additionalMaterialGroup1)
        case 9:
            return self.cellForAdditionalMaterialGroup2(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.additionalMaterialGroup2)
        case 10:
            return self.cellForAdditionalMaterialGroup3(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.additionalMaterialGroup3)
        case 11:
            return self.cellForAdditionalMaterialGroup4(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.additionalMaterialGroup4)
        case 12:
            return self.cellForAdditionalMaterialGroup5(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.additionalMaterialGroup5)
        case 13:
            return self.cellForAlternateProductNumber(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.alternateProductNumber)
        case 14:
            return self.cellForBaseUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.baseUnit)
        case 15:
            return self.cellForBatch(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.batch)
        case 16:
            return self.cellForBatchBySupplier(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.batchBySupplier)
        case 17:
            return self.cellForBatchClassification(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.batchClassification)
        case 18:
            return self.cellForBomExplosion(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.bomExplosion)
        case 19:
            return self.cellForBusinessArea(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.businessArea)
        case 20:
            return self.cellForConsumptionPosting(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.consumptionPosting)
        case 21:
            return self.cellForControllingArea(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.controllingArea)
        case 22:
            return self.cellForCostCenter(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.costCenter)
        case 23:
            return self.cellForCreatedByUser(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.createdByUser)
        case 24:
            return self.cellForCreationDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.creationDate)
        case 25:
            return self.cellForCustEngineeringChgStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.custEngineeringChgStatus)
        case 26:
            return self.cellForDeliveryDocument(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.deliveryDocument)
        case 27:
            return self.cellForDeliveryDocumentItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.deliveryDocumentItem)
        case 28:
            return self.cellForDeliveryDocumentItemCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.deliveryDocumentItemCategory)
        case 29:
            return self.cellForDeliveryDocumentItemText(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.deliveryDocumentItemText)
        case 30:
            return self.cellForDeliveryGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.deliveryGroup)
        case 31:
            return self.cellForDeliveryQuantityUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.deliveryQuantityUnit)
        case 32:
            return self.cellForDeliveryRelatedBillingStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.deliveryRelatedBillingStatus)
        case 33:
            return self.cellForDeliveryToBaseQuantityDnmntr(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.deliveryToBaseQuantityDnmntr)
        case 34:
            return self.cellForDeliveryToBaseQuantityNmrtr(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.deliveryToBaseQuantityNmrtr)
        case 35:
            return self.cellForDepartmentClassificationByCust(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.departmentClassificationByCust)
        case 36:
            return self.cellForDistributionChannel(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.distributionChannel)
        case 37:
            return self.cellForDivision(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.division)
        case 38:
            return self.cellForFixedShipgProcgDurationInDays(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.fixedShipgProcgDurationInDays)
        case 39:
            return self.cellForGlAccount(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.glAccount)
        case 40:
            return self.cellForGoodsMovementReasonCode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.goodsMovementReasonCode)
        case 41:
            return self.cellForGoodsMovementStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.goodsMovementStatus)
        case 42:
            return self.cellForGoodsMovementType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.goodsMovementType)
        case 43:
            return self.cellForHigherLvlItmOfBatSpltItm(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.higherLvlItmOfBatSpltItm)
        case 44:
            return self.cellForHigherLevelItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.higherLevelItem)
        case 45:
            return self.cellForInspectionLot(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.inspectionLot)
        case 46:
            return self.cellForInspectionPartialLot(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.inspectionPartialLot)
        case 47:
            return self.cellForIntercompanyBillingStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.intercompanyBillingStatus)
        case 48:
            return self.cellForInternationalArticleNumber(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.internationalArticleNumber)
        case 49:
            return self.cellForInventorySpecialStockType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.inventorySpecialStockType)
        case 50:
            return self.cellForInventoryValuationType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.inventoryValuationType)
        case 51:
            return self.cellForIsCompletelyDelivered(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.isCompletelyDelivered)
        case 52:
            return self.cellForIsNotGoodsMovementsRelevant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.isNotGoodsMovementsRelevant)
        case 53:
            return self.cellForIsSeparateValuation(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.isSeparateValuation)
        case 54:
            return self.cellForIssgOrRcvgBatch(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.issgOrRcvgBatch)
        case 55:
            return self.cellForIssgOrRcvgMaterial(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.issgOrRcvgMaterial)
        case 56:
            return self.cellForIssgOrRcvgSpclStockInd(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.issgOrRcvgSpclStockInd)
        case 57:
            return self.cellForIssgOrRcvgStockCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.issgOrRcvgStockCategory)
        case 58:
            return self.cellForIssgOrRcvgValuationType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.issgOrRcvgValuationType)
        case 59:
            return self.cellForIssuingOrReceivingPlant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.issuingOrReceivingPlant)
        case 60:
            return self.cellForIssuingOrReceivingStorageLoc(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.issuingOrReceivingStorageLoc)
        case 61:
            return self.cellForItemBillingBlockReason(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemBillingBlockReason)
        case 62:
            return self.cellForItemBillingIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemBillingIncompletionStatus)
        case 63:
            return self.cellForItemDeliveryIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemDeliveryIncompletionStatus)
        case 64:
            return self.cellForItemGdsMvtIncompletionSts(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemGdsMvtIncompletionSts)
        case 65:
            return self.cellForItemGeneralIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemGeneralIncompletionStatus)
        case 66:
            return self.cellForItemGrossWeight(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemGrossWeight)
        case 67:
            return self.cellForItemIsBillingRelevant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemIsBillingRelevant)
        case 68:
            return self.cellForItemNetWeight(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemNetWeight)
        case 69:
            return self.cellForItemPackingIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemPackingIncompletionStatus)
        case 70:
            return self.cellForItemPickingIncompletionStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemPickingIncompletionStatus)
        case 71:
            return self.cellForItemVolume(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemVolume)
        case 72:
            return self.cellForItemVolumeUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemVolumeUnit)
        case 73:
            return self.cellForItemWeightUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.itemWeightUnit)
        case 74:
            return self.cellForLastChangeDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.lastChangeDate)
        case 75:
            return self.cellForLoadingGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.loadingGroup)
        case 76:
            return self.cellForManufactureDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.manufactureDate)
        case 77:
            return self.cellForMaterial(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.material)
        case 78:
            return self.cellForMaterialByCustomer(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.materialByCustomer)
        case 79:
            return self.cellForMaterialFreightGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.materialFreightGroup)
        case 80:
            return self.cellForMaterialGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.materialGroup)
        case 81:
            return self.cellForMaterialIsBatchManaged(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.materialIsBatchManaged)
        case 82:
            return self.cellForMaterialIsIntBatchManaged(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.materialIsIntBatchManaged)
        case 83:
            return self.cellForNumberOfSerialNumbers(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.numberOfSerialNumbers)
        case 84:
            return self.cellForOrderID(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.orderID)
        case 85:
            return self.cellForOrderItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.orderItem)
        case 86:
            return self.cellForOriginalDeliveryQuantity(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.originalDeliveryQuantity)
        case 87:
            return self.cellForOriginallyRequestedMaterial(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.originallyRequestedMaterial)
        case 88:
            return self.cellForOverdelivTolrtdLmtRatioInPct(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.overdelivTolrtdLmtRatioInPct)
        case 89:
            return self.cellForPackingStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.packingStatus)
        case 90:
            return self.cellForPartialDeliveryIsAllowed(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.partialDeliveryIsAllowed)
        case 91:
            return self.cellForPaymentGuaranteeForm(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.paymentGuaranteeForm)
        case 92:
            return self.cellForPickingConfirmationStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.pickingConfirmationStatus)
        case 93:
            return self.cellForPickingControl(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.pickingControl)
        case 94:
            return self.cellForPickingStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.pickingStatus)
        case 95:
            return self.cellForPlant(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.plant)
        case 96:
            return self.cellForPrimaryPostingSwitch(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.primaryPostingSwitch)
        case 97:
            return self.cellForProductAvailabilityDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.productAvailabilityDate)
        case 98:
            return self.cellForProductConfiguration(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.productConfiguration)
        case 99:
            return self.cellForProductHierarchyNode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.productHierarchyNode)
        case 100:
            return self.cellForProfitabilitySegment(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.profitabilitySegment)
        case 101:
            return self.cellForProfitCenter(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.profitCenter)
        case 102:
            return self.cellForProofOfDeliveryRelevanceCode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.proofOfDeliveryRelevanceCode)
        case 103:
            return self.cellForProofOfDeliveryStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.proofOfDeliveryStatus)
        case 104:
            return self.cellForQuantityIsFixed(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.quantityIsFixed)
        case 105:
            return self.cellForReceivingPoint(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.receivingPoint)
        case 106:
            return self.cellForReferenceDocumentLogicalSystem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.referenceDocumentLogicalSystem)
        case 107:
            return self.cellForReferenceSDDocument(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.referenceSDDocument)
        case 108:
            return self.cellForReferenceSDDocumentCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.referenceSDDocumentCategory)
        case 109:
            return self.cellForReferenceSDDocumentItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.referenceSDDocumentItem)
        case 110:
            return self.cellForRetailPromotion(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.retailPromotion)
        case 111:
            return self.cellForSalesDocumentItemType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.salesDocumentItemType)
        case 112:
            return self.cellForSalesGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.salesGroup)
        case 113:
            return self.cellForSalesOffice(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.salesOffice)
        case 114:
            return self.cellForSdDocumentCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.sdDocumentCategory)
        case 115:
            return self.cellForSdProcessStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.sdProcessStatus)
        case 116:
            return self.cellForShelfLifeExpirationDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.shelfLifeExpirationDate)
        case 117:
            return self.cellForStatisticsDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.statisticsDate)
        case 118:
            return self.cellForStockType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.stockType)
        case 119:
            return self.cellForStorageBin(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.storageBin)
        case 120:
            return self.cellForStorageLocation(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.storageLocation)
        case 121:
            return self.cellForStorageType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.storageType)
        case 122:
            return self.cellForSubsequentMovementType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.subsequentMovementType)
        case 123:
            return self.cellForTransportationGroup(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.transportationGroup)
        case 124:
            return self.cellForUnderdelivTolrtdLmtRatioInPct(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.underdelivTolrtdLmtRatioInPct)
        case 125:
            return self.cellForUnlimitedOverdeliveryIsAllowed(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.unlimitedOverdeliveryIsAllowed)
        case 126:
            return self.cellForVarblShipgProcgDurationInDays(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.varblShipgProcgDurationInDays)
        case 127:
            return self.cellForWarehouse(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.warehouse)
        case 128:
            return self.cellForWarehouseActivityStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.warehouseActivityStatus)
        case 129:
            return self.cellForWarehouseStagingArea(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.warehouseStagingArea)
        case 130:
            return self.cellForWarehouseStockCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.warehouseStockCategory)
        case 131:
            return self.cellForWarehouseStorageBin(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.warehouseStorageBin)
        case 132:
            return self.cellForStockSegment(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.stockSegment)
        case 133:
            return self.cellForRequirementSegment(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AOutbDeliveryItemType.requirementSegment)
        default:
            return UITableViewCell()
        }
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 134
    }

    override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.preventNavigationLoop {
            AlertHelper.displayAlert(with: NSLocalizedString("keyAlertNavigationLoop", value: "No further navigation is possible.", comment: "XTIT: Title of alert message about preventing navigation loop."), error: nil, viewController: self)
            return
        }
        switch indexPath.row {
        default:
            return
        }
    }

    // MARK: - OData property specific cell creators

    private func cellForActualDeliveredQtyInBaseUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.actualDeliveredQtyInBaseUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.actualDeliveredQtyInBaseUnit = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.actualDeliveredQtyInBaseUnit = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryVersion(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryVersion {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryVersion = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.deliveryVersion.isOptional || newValue != "" {
                    currentEntity.deliveryVersion = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForActualDeliveryQuantity(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.actualDeliveryQuantity {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.actualDeliveryQuantity = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.actualDeliveryQuantity = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalCustomerGroup1(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalCustomerGroup1 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalCustomerGroup1 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.additionalCustomerGroup1.isOptional || newValue != "" {
                    currentEntity.additionalCustomerGroup1 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalCustomerGroup2(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalCustomerGroup2 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalCustomerGroup2 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.additionalCustomerGroup2.isOptional || newValue != "" {
                    currentEntity.additionalCustomerGroup2 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalCustomerGroup3(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalCustomerGroup3 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalCustomerGroup3 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.additionalCustomerGroup3.isOptional || newValue != "" {
                    currentEntity.additionalCustomerGroup3 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalCustomerGroup4(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalCustomerGroup4 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalCustomerGroup4 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.additionalCustomerGroup4.isOptional || newValue != "" {
                    currentEntity.additionalCustomerGroup4 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalCustomerGroup5(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalCustomerGroup5 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalCustomerGroup5 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.additionalCustomerGroup5.isOptional || newValue != "" {
                    currentEntity.additionalCustomerGroup5 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalMaterialGroup1(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalMaterialGroup1 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalMaterialGroup1 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.additionalMaterialGroup1.isOptional || newValue != "" {
                    currentEntity.additionalMaterialGroup1 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalMaterialGroup2(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalMaterialGroup2 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalMaterialGroup2 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.additionalMaterialGroup2.isOptional || newValue != "" {
                    currentEntity.additionalMaterialGroup2 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalMaterialGroup3(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalMaterialGroup3 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalMaterialGroup3 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.additionalMaterialGroup3.isOptional || newValue != "" {
                    currentEntity.additionalMaterialGroup3 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalMaterialGroup4(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalMaterialGroup4 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalMaterialGroup4 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.additionalMaterialGroup4.isOptional || newValue != "" {
                    currentEntity.additionalMaterialGroup4 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAdditionalMaterialGroup5(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.additionalMaterialGroup5 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.additionalMaterialGroup5 = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.additionalMaterialGroup5.isOptional || newValue != "" {
                    currentEntity.additionalMaterialGroup5 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForAlternateProductNumber(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.alternateProductNumber {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.alternateProductNumber = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.alternateProductNumber.isOptional || newValue != "" {
                    currentEntity.alternateProductNumber = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBaseUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.baseUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.baseUnit = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.baseUnit.isOptional || newValue != "" {
                    currentEntity.baseUnit = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBatch(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.batch {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.batch = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.batch.isOptional || newValue != "" {
                    currentEntity.batch = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBatchBySupplier(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.batchBySupplier {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.batchBySupplier = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.batchBySupplier.isOptional || newValue != "" {
                    currentEntity.batchBySupplier = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBatchClassification(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.batchClassification {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.batchClassification = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.batchClassification.isOptional || newValue != "" {
                    currentEntity.batchClassification = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBomExplosion(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.bomExplosion {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.bomExplosion = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.bomExplosion.isOptional || newValue != "" {
                    currentEntity.bomExplosion = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBusinessArea(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.businessArea {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.businessArea = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.businessArea.isOptional || newValue != "" {
                    currentEntity.businessArea = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForConsumptionPosting(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.consumptionPosting {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.consumptionPosting = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.consumptionPosting.isOptional || newValue != "" {
                    currentEntity.consumptionPosting = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForControllingArea(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.controllingArea {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.controllingArea = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.controllingArea.isOptional || newValue != "" {
                    currentEntity.controllingArea = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCostCenter(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.costCenter {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.costCenter = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.costCenter.isOptional || newValue != "" {
                    currentEntity.costCenter = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCreatedByUser(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.createdByUser {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.createdByUser = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.createdByUser.isOptional || newValue != "" {
                    currentEntity.createdByUser = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCreationDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.creationDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.creationDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.creationDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCustEngineeringChgStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.custEngineeringChgStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.custEngineeringChgStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.custEngineeringChgStatus.isOptional || newValue != "" {
                    currentEntity.custEngineeringChgStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDocument(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocument {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocument = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.deliveryDocument.isOptional || newValue != "" {
                    currentEntity.deliveryDocument = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDocumentItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocumentItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocumentItem = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.deliveryDocumentItem.isOptional || newValue != "" {
                    currentEntity.deliveryDocumentItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDocumentItemCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocumentItemCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocumentItemCategory = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.deliveryDocumentItemCategory.isOptional || newValue != "" {
                    currentEntity.deliveryDocumentItemCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryDocumentItemText(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryDocumentItemText {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryDocumentItemText = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.deliveryDocumentItemText.isOptional || newValue != "" {
                    currentEntity.deliveryDocumentItemText = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryGroup = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.deliveryGroup.isOptional || newValue != "" {
                    currentEntity.deliveryGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryQuantityUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryQuantityUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryQuantityUnit = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.deliveryQuantityUnit.isOptional || newValue != "" {
                    currentEntity.deliveryQuantityUnit = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryRelatedBillingStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryRelatedBillingStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryRelatedBillingStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.deliveryRelatedBillingStatus.isOptional || newValue != "" {
                    currentEntity.deliveryRelatedBillingStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryToBaseQuantityDnmntr(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryToBaseQuantityDnmntr {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryToBaseQuantityDnmntr = nil
                isNewValueValid = true
            } else {
                if let validValue = BigInteger.parse(newValue) {
                    currentEntity.deliveryToBaseQuantityDnmntr = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDeliveryToBaseQuantityNmrtr(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryToBaseQuantityNmrtr {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryToBaseQuantityNmrtr = nil
                isNewValueValid = true
            } else {
                if let validValue = BigInteger.parse(newValue) {
                    currentEntity.deliveryToBaseQuantityNmrtr = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDepartmentClassificationByCust(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.departmentClassificationByCust {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.departmentClassificationByCust = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.departmentClassificationByCust.isOptional || newValue != "" {
                    currentEntity.departmentClassificationByCust = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDistributionChannel(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.distributionChannel {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.distributionChannel = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.distributionChannel.isOptional || newValue != "" {
                    currentEntity.distributionChannel = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForDivision(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.division {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.division = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.division.isOptional || newValue != "" {
                    currentEntity.division = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForFixedShipgProcgDurationInDays(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.fixedShipgProcgDurationInDays {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.fixedShipgProcgDurationInDays = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.fixedShipgProcgDurationInDays = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGlAccount(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.glAccount {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.glAccount = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.glAccount.isOptional || newValue != "" {
                    currentEntity.glAccount = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGoodsMovementReasonCode(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.goodsMovementReasonCode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.goodsMovementReasonCode = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.goodsMovementReasonCode.isOptional || newValue != "" {
                    currentEntity.goodsMovementReasonCode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGoodsMovementStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.goodsMovementStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.goodsMovementStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.goodsMovementStatus.isOptional || newValue != "" {
                    currentEntity.goodsMovementStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGoodsMovementType(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.goodsMovementType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.goodsMovementType = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.goodsMovementType.isOptional || newValue != "" {
                    currentEntity.goodsMovementType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHigherLvlItmOfBatSpltItm(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.higherLvlItmOfBatSpltItm {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.higherLvlItmOfBatSpltItm = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.higherLvlItmOfBatSpltItm.isOptional || newValue != "" {
                    currentEntity.higherLvlItmOfBatSpltItm = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHigherLevelItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.higherLevelItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.higherLevelItem = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.higherLevelItem.isOptional || newValue != "" {
                    currentEntity.higherLevelItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInspectionLot(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.inspectionLot {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.inspectionLot = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.inspectionLot.isOptional || newValue != "" {
                    currentEntity.inspectionLot = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInspectionPartialLot(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.inspectionPartialLot {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.inspectionPartialLot = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.inspectionPartialLot.isOptional || newValue != "" {
                    currentEntity.inspectionPartialLot = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIntercompanyBillingStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.intercompanyBillingStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.intercompanyBillingStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.intercompanyBillingStatus.isOptional || newValue != "" {
                    currentEntity.intercompanyBillingStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInternationalArticleNumber(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.internationalArticleNumber {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.internationalArticleNumber = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.internationalArticleNumber.isOptional || newValue != "" {
                    currentEntity.internationalArticleNumber = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInventorySpecialStockType(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.inventorySpecialStockType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.inventorySpecialStockType = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.inventorySpecialStockType.isOptional || newValue != "" {
                    currentEntity.inventorySpecialStockType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForInventoryValuationType(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.inventoryValuationType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.inventoryValuationType = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.inventoryValuationType.isOptional || newValue != "" {
                    currentEntity.inventoryValuationType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIsCompletelyDelivered(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.isCompletelyDelivered {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.isCompletelyDelivered = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.isCompletelyDelivered = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIsNotGoodsMovementsRelevant(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.isNotGoodsMovementsRelevant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.isNotGoodsMovementsRelevant = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.isNotGoodsMovementsRelevant.isOptional || newValue != "" {
                    currentEntity.isNotGoodsMovementsRelevant = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIsSeparateValuation(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.isSeparateValuation {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.isSeparateValuation = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.isSeparateValuation = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgBatch(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgBatch {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgBatch = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.issgOrRcvgBatch.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgBatch = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgMaterial(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgMaterial {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgMaterial = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.issgOrRcvgMaterial.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgMaterial = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgSpclStockInd(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgSpclStockInd {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgSpclStockInd = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.issgOrRcvgSpclStockInd.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgSpclStockInd = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgStockCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgStockCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgStockCategory = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.issgOrRcvgStockCategory.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgStockCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssgOrRcvgValuationType(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issgOrRcvgValuationType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issgOrRcvgValuationType = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.issgOrRcvgValuationType.isOptional || newValue != "" {
                    currentEntity.issgOrRcvgValuationType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssuingOrReceivingPlant(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issuingOrReceivingPlant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issuingOrReceivingPlant = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.issuingOrReceivingPlant.isOptional || newValue != "" {
                    currentEntity.issuingOrReceivingPlant = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForIssuingOrReceivingStorageLoc(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.issuingOrReceivingStorageLoc {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.issuingOrReceivingStorageLoc = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.issuingOrReceivingStorageLoc.isOptional || newValue != "" {
                    currentEntity.issuingOrReceivingStorageLoc = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemBillingBlockReason(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemBillingBlockReason {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemBillingBlockReason = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.itemBillingBlockReason.isOptional || newValue != "" {
                    currentEntity.itemBillingBlockReason = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemBillingIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemBillingIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemBillingIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.itemBillingIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.itemBillingIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemDeliveryIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemDeliveryIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemDeliveryIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.itemDeliveryIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.itemDeliveryIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemGdsMvtIncompletionSts(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemGdsMvtIncompletionSts {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemGdsMvtIncompletionSts = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.itemGdsMvtIncompletionSts.isOptional || newValue != "" {
                    currentEntity.itemGdsMvtIncompletionSts = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemGeneralIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemGeneralIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemGeneralIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.itemGeneralIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.itemGeneralIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemGrossWeight(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemGrossWeight {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemGrossWeight = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.itemGrossWeight = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemIsBillingRelevant(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemIsBillingRelevant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemIsBillingRelevant = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.itemIsBillingRelevant.isOptional || newValue != "" {
                    currentEntity.itemIsBillingRelevant = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemNetWeight(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemNetWeight {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemNetWeight = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.itemNetWeight = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemPackingIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemPackingIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemPackingIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.itemPackingIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.itemPackingIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemPickingIncompletionStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemPickingIncompletionStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemPickingIncompletionStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.itemPickingIncompletionStatus.isOptional || newValue != "" {
                    currentEntity.itemPickingIncompletionStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemVolume(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemVolume {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemVolume = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.itemVolume = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemVolumeUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemVolumeUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemVolumeUnit = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.itemVolumeUnit.isOptional || newValue != "" {
                    currentEntity.itemVolumeUnit = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForItemWeightUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.itemWeightUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.itemWeightUnit = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.itemWeightUnit.isOptional || newValue != "" {
                    currentEntity.itemWeightUnit = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForLastChangeDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.lastChangeDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.lastChangeDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.lastChangeDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForLoadingGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.loadingGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.loadingGroup = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.loadingGroup.isOptional || newValue != "" {
                    currentEntity.loadingGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForManufactureDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.manufactureDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.manufactureDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.manufactureDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterial(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.material {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.material = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.material.isOptional || newValue != "" {
                    currentEntity.material = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialByCustomer(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialByCustomer {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialByCustomer = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.materialByCustomer.isOptional || newValue != "" {
                    currentEntity.materialByCustomer = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialFreightGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialFreightGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialFreightGroup = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.materialFreightGroup.isOptional || newValue != "" {
                    currentEntity.materialFreightGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialGroup = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.materialGroup.isOptional || newValue != "" {
                    currentEntity.materialGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialIsBatchManaged(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialIsBatchManaged {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialIsBatchManaged = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.materialIsBatchManaged = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMaterialIsIntBatchManaged(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.materialIsIntBatchManaged {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.materialIsIntBatchManaged = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.materialIsIntBatchManaged = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForNumberOfSerialNumbers(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.numberOfSerialNumbers {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.numberOfSerialNumbers = nil
                isNewValueValid = true
            } else {
                if let validValue = Int(newValue) {
                    currentEntity.numberOfSerialNumbers = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOrderID(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.orderID {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.orderID = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.orderID.isOptional || newValue != "" {
                    currentEntity.orderID = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOrderItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.orderItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.orderItem = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.orderItem.isOptional || newValue != "" {
                    currentEntity.orderItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOriginalDeliveryQuantity(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.originalDeliveryQuantity {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.originalDeliveryQuantity = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.originalDeliveryQuantity = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOriginallyRequestedMaterial(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.originallyRequestedMaterial {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.originallyRequestedMaterial = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.originallyRequestedMaterial.isOptional || newValue != "" {
                    currentEntity.originallyRequestedMaterial = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForOverdelivTolrtdLmtRatioInPct(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.overdelivTolrtdLmtRatioInPct {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.overdelivTolrtdLmtRatioInPct = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.overdelivTolrtdLmtRatioInPct = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPackingStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.packingStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.packingStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.packingStatus.isOptional || newValue != "" {
                    currentEntity.packingStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPartialDeliveryIsAllowed(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.partialDeliveryIsAllowed {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.partialDeliveryIsAllowed = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.partialDeliveryIsAllowed.isOptional || newValue != "" {
                    currentEntity.partialDeliveryIsAllowed = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPaymentGuaranteeForm(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.paymentGuaranteeForm {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.paymentGuaranteeForm = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.paymentGuaranteeForm.isOptional || newValue != "" {
                    currentEntity.paymentGuaranteeForm = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPickingConfirmationStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.pickingConfirmationStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.pickingConfirmationStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.pickingConfirmationStatus.isOptional || newValue != "" {
                    currentEntity.pickingConfirmationStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPickingControl(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.pickingControl {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.pickingControl = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.pickingControl.isOptional || newValue != "" {
                    currentEntity.pickingControl = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPickingStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.pickingStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.pickingStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.pickingStatus.isOptional || newValue != "" {
                    currentEntity.pickingStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPlant(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.plant {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.plant = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.plant.isOptional || newValue != "" {
                    currentEntity.plant = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPrimaryPostingSwitch(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.primaryPostingSwitch {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.primaryPostingSwitch = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.primaryPostingSwitch.isOptional || newValue != "" {
                    currentEntity.primaryPostingSwitch = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProductAvailabilityDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.productAvailabilityDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.productAvailabilityDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.productAvailabilityDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProductConfiguration(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.productConfiguration {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.productConfiguration = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.productConfiguration.isOptional || newValue != "" {
                    currentEntity.productConfiguration = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProductHierarchyNode(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.productHierarchyNode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.productHierarchyNode = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.productHierarchyNode.isOptional || newValue != "" {
                    currentEntity.productHierarchyNode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProfitabilitySegment(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.profitabilitySegment {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.profitabilitySegment = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.profitabilitySegment.isOptional || newValue != "" {
                    currentEntity.profitabilitySegment = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProfitCenter(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.profitCenter {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.profitCenter = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.profitCenter.isOptional || newValue != "" {
                    currentEntity.profitCenter = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProofOfDeliveryRelevanceCode(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.proofOfDeliveryRelevanceCode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.proofOfDeliveryRelevanceCode = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.proofOfDeliveryRelevanceCode.isOptional || newValue != "" {
                    currentEntity.proofOfDeliveryRelevanceCode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForProofOfDeliveryStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.proofOfDeliveryStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.proofOfDeliveryStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.proofOfDeliveryStatus.isOptional || newValue != "" {
                    currentEntity.proofOfDeliveryStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForQuantityIsFixed(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.quantityIsFixed {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.quantityIsFixed = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.quantityIsFixed = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReceivingPoint(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.receivingPoint {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.receivingPoint = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.receivingPoint.isOptional || newValue != "" {
                    currentEntity.receivingPoint = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReferenceDocumentLogicalSystem(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.referenceDocumentLogicalSystem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.referenceDocumentLogicalSystem = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.referenceDocumentLogicalSystem.isOptional || newValue != "" {
                    currentEntity.referenceDocumentLogicalSystem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReferenceSDDocument(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.referenceSDDocument {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.referenceSDDocument = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.referenceSDDocument.isOptional || newValue != "" {
                    currentEntity.referenceSDDocument = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReferenceSDDocumentCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.referenceSDDocumentCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.referenceSDDocumentCategory = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.referenceSDDocumentCategory.isOptional || newValue != "" {
                    currentEntity.referenceSDDocumentCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReferenceSDDocumentItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.referenceSDDocumentItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.referenceSDDocumentItem = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.referenceSDDocumentItem.isOptional || newValue != "" {
                    currentEntity.referenceSDDocumentItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForRetailPromotion(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.retailPromotion {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.retailPromotion = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.retailPromotion.isOptional || newValue != "" {
                    currentEntity.retailPromotion = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSalesDocumentItemType(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.salesDocumentItemType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.salesDocumentItemType = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.salesDocumentItemType.isOptional || newValue != "" {
                    currentEntity.salesDocumentItemType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSalesGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.salesGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.salesGroup = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.salesGroup.isOptional || newValue != "" {
                    currentEntity.salesGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSalesOffice(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.salesOffice {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.salesOffice = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.salesOffice.isOptional || newValue != "" {
                    currentEntity.salesOffice = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSdDocumentCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.sdDocumentCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.sdDocumentCategory = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.sdDocumentCategory.isOptional || newValue != "" {
                    currentEntity.sdDocumentCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSdProcessStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.sdProcessStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.sdProcessStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.sdProcessStatus.isOptional || newValue != "" {
                    currentEntity.sdProcessStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForShelfLifeExpirationDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.shelfLifeExpirationDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.shelfLifeExpirationDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.shelfLifeExpirationDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStatisticsDate(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.statisticsDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.statisticsDate = nil
                isNewValueValid = true
            } else {
                if let validValue = LocalDateTime.parse(newValue) { // This is just a simple solution to handle UTC only
                    currentEntity.statisticsDate = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStockType(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.stockType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.stockType = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.stockType.isOptional || newValue != "" {
                    currentEntity.stockType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStorageBin(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.storageBin {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.storageBin = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.storageBin.isOptional || newValue != "" {
                    currentEntity.storageBin = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStorageLocation(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.storageLocation {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.storageLocation = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.storageLocation.isOptional || newValue != "" {
                    currentEntity.storageLocation = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStorageType(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.storageType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.storageType = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.storageType.isOptional || newValue != "" {
                    currentEntity.storageType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSubsequentMovementType(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.subsequentMovementType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.subsequentMovementType = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.subsequentMovementType.isOptional || newValue != "" {
                    currentEntity.subsequentMovementType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTransportationGroup(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.transportationGroup {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.transportationGroup = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.transportationGroup.isOptional || newValue != "" {
                    currentEntity.transportationGroup = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForUnderdelivTolrtdLmtRatioInPct(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.underdelivTolrtdLmtRatioInPct {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.underdelivTolrtdLmtRatioInPct = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.underdelivTolrtdLmtRatioInPct = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForUnlimitedOverdeliveryIsAllowed(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.unlimitedOverdeliveryIsAllowed {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.unlimitedOverdeliveryIsAllowed = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.unlimitedOverdeliveryIsAllowed = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForVarblShipgProcgDurationInDays(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.varblShipgProcgDurationInDays {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.varblShipgProcgDurationInDays = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.varblShipgProcgDurationInDays = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouse(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouse {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouse = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.warehouse.isOptional || newValue != "" {
                    currentEntity.warehouse = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouseActivityStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouseActivityStatus {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouseActivityStatus = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.warehouseActivityStatus.isOptional || newValue != "" {
                    currentEntity.warehouseActivityStatus = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouseStagingArea(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouseStagingArea {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouseStagingArea = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.warehouseStagingArea.isOptional || newValue != "" {
                    currentEntity.warehouseStagingArea = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouseStockCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouseStockCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouseStockCategory = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.warehouseStockCategory.isOptional || newValue != "" {
                    currentEntity.warehouseStockCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWarehouseStorageBin(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.warehouseStorageBin {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.warehouseStorageBin = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.warehouseStorageBin.isOptional || newValue != "" {
                    currentEntity.warehouseStorageBin = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStockSegment(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.stockSegment {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.stockSegment = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.stockSegment.isOptional || newValue != "" {
                    currentEntity.stockSegment = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForRequirementSegment(tableView: UITableView, indexPath: IndexPath, currentEntity: AOutbDeliveryItemType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.requirementSegment {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.requirementSegment = nil
                isNewValueValid = true
            } else {
                if AOutbDeliveryItemType.requirementSegment.isOptional || newValue != "" {
                    currentEntity.requirementSegment = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    // MARK: - OData functionalities

    @objc func createEntity() {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Creating entity in backend.")
        self.ec1.createEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Create entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityCreationTitle", value: "Create entry failed", comment: "XTIT: Title of alert message about entity creation error."), error: error, viewController: self)
                return
            }
            self.logger.info("Create entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Created", comment: "XMSG: Title of alert message about successful entity creation."))
                    self.tableUpdater?.entitySetHasChanged()
                }
            }
        }
    }

    func createEntityWithDefaultValues() -> AOutbDeliveryItemType {
        let newEntity = AOutbDeliveryItemType()

        // Key properties without default value should be invalid by default for Create scenario
        if newEntity.deliveryDocument == nil || newEntity.deliveryDocument!.isEmpty {
            self.validity["DeliveryDocument"] = false
        }
        if newEntity.deliveryDocumentItem == nil || newEntity.deliveryDocumentItem!.isEmpty {
            self.validity["DeliveryDocumentItem"] = false
        }

        self.barButtonShouldBeEnabled()
        return newEntity
    }

    @objc func updateEntity(_: AnyObject) {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Updating entity in backend.")
        self.ec1.updateEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Update entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityUpdateTitle", value: "Update entry failed", comment: "XTIT: Title of alert message about entity update failure."), error: error, viewController: self)
                return
            }
            self.logger.info("Update entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyUpdateEntityFinishedTitle", value: "Updated", comment: "XTIT: Title of alert message about successful entity update."))
                    self.entityUpdater?.entityHasChanged(self.entity)
                }
            }
        }
    }

    // MARK: - other logic, helper

    @objc func cancel() {
        DispatchQueue.main.async {
            self.dismiss(animated: true)
        }
    }

    // Check if all text fields are valid
    private func barButtonShouldBeEnabled() {
        let anyFieldInvalid = self.validity.values.first { field in
            field == false
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = anyFieldInvalid == nil
    }
}

extension AOutbDeliveryItemTypeDetailViewController: CPIKIMODataEntityUpdaterDelegate {
    func entityHasChanged(_ entityValue: EntityValue?) {
        if let entity = entityValue {
            let currentEntity = entity as! AOutbDeliveryItemType
            self.entity = currentEntity
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
}
