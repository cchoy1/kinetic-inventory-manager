//
// KineticInventoryManager
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 30/03/20
//

import Foundation
import SAPCommon
import SAPFiori
import SAPFoundation
import SAPOData

class AInbDeliveryDocFlowTypeDetailViewController: FUIFormTableViewController, SAPFioriLoadingIndicator {
    var ec1: Ec1<OnlineODataProvider>!
    private var validity = [String: Bool]()
    var allowsEditableCells = false

    private var _entity: AInbDeliveryDocFlowType?
    var entity: AInbDeliveryDocFlowType {
        get {
            if self._entity == nil {
                self._entity = self.createEntityWithDefaultValues()
            }
            return self._entity!
        }
        set {
            self._entity = newValue
        }
    }

    private let logger = Logger.shared(named: "AInbDeliveryDocFlowTypeMasterViewControllerLogger")
    var loadingIndicator: FUILoadingIndicatorView?
    var entityUpdater: CPIKIMODataEntityUpdaterDelegate?
    var tableUpdater: CPIKIMODataEntitySetUpdaterDelegate?
    private let okTitle = NSLocalizedString("keyOkButtonTitle",
                                            value: "OK",
                                            comment: "XBUT: Title of OK button.")
    var preventNavigationLoop = false
    var entitySetName: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "updateEntity" {
            // Show the Detail view with the current entity, where the properties scan be edited and updated
            self.logger.info("Showing a view to update the selected entity.")
            let dest = segue.destination as! UINavigationController
            let detailViewController = dest.viewControllers[0] as! AInbDeliveryDocFlowTypeDetailViewController
            detailViewController.title = NSLocalizedString("keyUpdateEntityTitle", value: "Update Entity", comment: "XTIT: Title of update selected entity screen.")
            detailViewController.ec1 = self.ec1
            detailViewController.entity = self.entity
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: detailViewController, action: #selector(detailViewController.updateEntity))
            detailViewController.navigationItem.rightBarButtonItem = doneButton
            let cancelButton = UIBarButtonItem(title: NSLocalizedString("keyCancelButtonToGoPreviousScreen", value: "Cancel", comment: "XBUT: Title of Cancel button."), style: .plain, target: detailViewController, action: #selector(detailViewController.cancel))
            detailViewController.navigationItem.leftBarButtonItem = cancelButton
            detailViewController.allowsEditableCells = true
            detailViewController.entityUpdater = self
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return self.cellForDeliveryVersion(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryDocFlowType.deliveryVersion)
        case 1:
            return self.cellForPrecedingDocument(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryDocFlowType.precedingDocument)
        case 2:
            return self.cellForPrecedingDocumentCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryDocFlowType.precedingDocumentCategory)
        case 3:
            return self.cellForPrecedingDocumentItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryDocFlowType.precedingDocumentItem)
        case 4:
            return self.cellForQuantityInBaseUnit(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryDocFlowType.quantityInBaseUnit)
        case 5:
            return self.cellForSdFulfillmentCalculationRule(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryDocFlowType.sdFulfillmentCalculationRule)
        case 6:
            return self.cellForSubsequentDocument(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryDocFlowType.subsequentDocument)
        case 7:
            return self.cellForSubsequentDocumentCategory(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryDocFlowType.subsequentDocumentCategory)
        case 8:
            return self.cellForSubsequentDocumentItem(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryDocFlowType.subsequentDocumentItem)
        case 9:
            return self.cellForTransferOrderInWrhsMgmtIsConfd(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: AInbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd)
        default:
            return UITableViewCell()
        }
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 10
    }

    override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.preventNavigationLoop {
            AlertHelper.displayAlert(with: NSLocalizedString("keyAlertNavigationLoop", value: "No further navigation is possible.", comment: "XTIT: Title of alert message about preventing navigation loop."), error: nil, viewController: self)
            return
        }
        switch indexPath.row {
        default:
            return
        }
    }

    // MARK: - OData property specific cell creators

    private func cellForDeliveryVersion(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryDocFlowType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.deliveryVersion {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.deliveryVersion = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryDocFlowType.deliveryVersion.isOptional || newValue != "" {
                    currentEntity.deliveryVersion = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPrecedingDocument(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryDocFlowType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.precedingDocument {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.precedingDocument = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryDocFlowType.precedingDocument.isOptional || newValue != "" {
                    currentEntity.precedingDocument = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPrecedingDocumentCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryDocFlowType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.precedingDocumentCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.precedingDocumentCategory = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryDocFlowType.precedingDocumentCategory.isOptional || newValue != "" {
                    currentEntity.precedingDocumentCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPrecedingDocumentItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryDocFlowType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.precedingDocumentItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.precedingDocumentItem = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryDocFlowType.precedingDocumentItem.isOptional || newValue != "" {
                    currentEntity.precedingDocumentItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForQuantityInBaseUnit(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryDocFlowType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.quantityInBaseUnit {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.quantityInBaseUnit = nil
                isNewValueValid = true
            } else {
                if let validValue = BigDecimal.parse(newValue) {
                    currentEntity.quantityInBaseUnit = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSdFulfillmentCalculationRule(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryDocFlowType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.sdFulfillmentCalculationRule {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.sdFulfillmentCalculationRule = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryDocFlowType.sdFulfillmentCalculationRule.isOptional || newValue != "" {
                    currentEntity.sdFulfillmentCalculationRule = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSubsequentDocument(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryDocFlowType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.subsequentDocument {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.subsequentDocument = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryDocFlowType.subsequentDocument.isOptional || newValue != "" {
                    currentEntity.subsequentDocument = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSubsequentDocumentCategory(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryDocFlowType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.subsequentDocumentCategory {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.subsequentDocumentCategory = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryDocFlowType.subsequentDocumentCategory.isOptional || newValue != "" {
                    currentEntity.subsequentDocumentCategory = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSubsequentDocumentItem(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryDocFlowType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.subsequentDocumentItem {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.subsequentDocumentItem = nil
                isNewValueValid = true
            } else {
                if AInbDeliveryDocFlowType.subsequentDocumentItem.isOptional || newValue != "" {
                    currentEntity.subsequentDocumentItem = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTransferOrderInWrhsMgmtIsConfd(tableView: UITableView, indexPath: IndexPath, currentEntity: AInbDeliveryDocFlowType, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.transferOrderInWrhsMgmtIsConfd {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.transferOrderInWrhsMgmtIsConfd = nil
                isNewValueValid = true
            } else {
                if let validValue = Bool(newValue) {
                    currentEntity.transferOrderInWrhsMgmtIsConfd = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    // MARK: - OData functionalities

    @objc func createEntity() {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Creating entity in backend.")
        self.ec1.createEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Create entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityCreationTitle", value: "Create entry failed", comment: "XTIT: Title of alert message about entity creation error."), error: error, viewController: self)
                return
            }
            self.logger.info("Create entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Created", comment: "XMSG: Title of alert message about successful entity creation."))
                    self.tableUpdater?.entitySetHasChanged()
                }
            }
        }
    }

    func createEntityWithDefaultValues() -> AInbDeliveryDocFlowType {
        let newEntity = AInbDeliveryDocFlowType()

        // Key properties without default value should be invalid by default for Create scenario
        if newEntity.precedingDocument == nil || newEntity.precedingDocument!.isEmpty {
            self.validity["PrecedingDocument"] = false
        }
        if newEntity.precedingDocumentItem == nil || newEntity.precedingDocumentItem!.isEmpty {
            self.validity["PrecedingDocumentItem"] = false
        }
        if newEntity.subsequentDocumentCategory == nil || newEntity.subsequentDocumentCategory!.isEmpty {
            self.validity["SubsequentDocumentCategory"] = false
        }

        self.barButtonShouldBeEnabled()
        return newEntity
    }

    @objc func updateEntity(_: AnyObject) {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Updating entity in backend.")
        self.ec1.updateEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Update entry failed. Error: \(error)", error: error)
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorEntityUpdateTitle", value: "Update entry failed", comment: "XTIT: Title of alert message about entity update failure."), error: error, viewController: self)
                return
            }
            self.logger.info("Update entry finished successfully.")
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyUpdateEntityFinishedTitle", value: "Updated", comment: "XTIT: Title of alert message about successful entity update."))
                    self.entityUpdater?.entityHasChanged(self.entity)
                }
            }
        }
    }

    // MARK: - other logic, helper

    @objc func cancel() {
        DispatchQueue.main.async {
            self.dismiss(animated: true)
        }
    }

    // Check if all text fields are valid
    private func barButtonShouldBeEnabled() {
        let anyFieldInvalid = self.validity.values.first { field in
            field == false
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = anyFieldInvalid == nil
    }
}

extension AInbDeliveryDocFlowTypeDetailViewController: CPIKIMODataEntityUpdaterDelegate {
    func entityHasChanged(_ entityValue: EntityValue?) {
        if let entity = entityValue {
            let currentEntity = entity as! AInbDeliveryDocFlowType
            self.entity = currentEntity
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
}
