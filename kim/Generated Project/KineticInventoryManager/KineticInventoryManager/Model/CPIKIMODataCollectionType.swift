//
// KineticInventoryManager
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 30/03/20
//

import Foundation

enum CPIKIMODataCollectionType: String {
    case aMaterialDocumentHeader = "AMaterialDocumentHeader"
    case aMaterialDocumentItemTypeSet = "AMaterialDocumentItemTypeSet"
    case aOutbDeliveryAddressTypeSet = "AOutbDeliveryAddressTypeSet"
    case aOutbDeliveryPartnerTypeSet = "AOutbDeliveryPartnerTypeSet"
    case aInbDeliverySerialNmbrTypeSet = "AInbDeliverySerialNmbrTypeSet"
    case aSerialNmbrDeliveryTypeSet = "ASerialNmbrDeliveryTypeSet"
    case aMaintenanceItemObjListTypeSet = "AMaintenanceItemObjListTypeSet"
    case aInbDeliveryPartner = "AInbDeliveryPartner"
    case aInbDeliveryItem = "AInbDeliveryItem"
    case aInbDeliveryAddressTypeSet = "AInbDeliveryAddressTypeSet"
    case aOutbDeliveryDocFlowTypeSet = "AOutbDeliveryDocFlowTypeSet"
    case aInbDeliveryHeaderTextTypeSet = "AInbDeliveryHeaderTextTypeSet"
    case aPlant = "APlant"
    case aOutbDeliveryItem = "AOutbDeliveryItem"
    case aInbDeliveryDocFlowTypeSet = "AInbDeliveryDocFlowTypeSet"
    case aOutbDeliveryHeader = "AOutbDeliveryHeader"
    case aInbDeliveryItemTextTypeSet = "AInbDeliveryItemTextTypeSet"
    case aMaintenanceItemObjectTypeSet = "AMaintenanceItemObjectTypeSet"
    case aInbDeliveryHeader = "AInbDeliveryHeader"
    case none = ""
    static let all = [aMaterialDocumentHeader, aMaterialDocumentItemTypeSet, aOutbDeliveryAddressTypeSet, aOutbDeliveryPartnerTypeSet, aInbDeliverySerialNmbrTypeSet, aSerialNmbrDeliveryTypeSet, aMaintenanceItemObjListTypeSet, aInbDeliveryPartner, aInbDeliveryItem, aInbDeliveryAddressTypeSet, aOutbDeliveryDocFlowTypeSet, aInbDeliveryHeaderTextTypeSet, aPlant, aOutbDeliveryItem, aInbDeliveryDocFlowTypeSet, aOutbDeliveryHeader, aInbDeliveryItemTextTypeSet, aMaintenanceItemObjectTypeSet, aInbDeliveryHeader]
}
