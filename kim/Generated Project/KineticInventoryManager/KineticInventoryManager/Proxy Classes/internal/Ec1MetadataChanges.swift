// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

internal class Ec1MetadataChanges {
    static func merge(metadata: CSDLDocument) {
        metadata.hasGeneratedProxies = true
        Ec1Metadata.document = metadata
        Ec1MetadataChanges.merge1(metadata: metadata)
        Ec1MetadataChanges.merge2(metadata: metadata)
        try! Ec1Factory.registerAll()
    }

    private static func merge1(metadata: CSDLDocument) {
        Ignore.valueOf_any(metadata)
        if !Ec1Metadata.ComplexTypes.deliveryMessage.isRemoved {
            Ec1Metadata.ComplexTypes.deliveryMessage = metadata.complexType(withName: "S1.DeliveryMessage")
        }
        if !Ec1Metadata.ComplexTypes.pickingReport.isRemoved {
            Ec1Metadata.ComplexTypes.pickingReport = metadata.complexType(withName: "S1.PickingReport")
        }
        if !Ec1Metadata.ComplexTypes.putawayReport.isRemoved {
            Ec1Metadata.ComplexTypes.putawayReport = metadata.complexType(withName: "S1.PutawayReport")
        }
        if !Ec1Metadata.ComplexTypes.return.isRemoved {
            Ec1Metadata.ComplexTypes.return = metadata.complexType(withName: "S1.Return")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryAddressType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryAddressType = metadata.entityType(withName: "S1.A_InbDeliveryAddressType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType = metadata.entityType(withName: "S1.A_InbDeliveryDocFlowType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType = metadata.entityType(withName: "S1.A_InbDeliveryHeaderTextType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryHeaderType = metadata.entityType(withName: "S1.A_InbDeliveryHeaderType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryItemTextType = metadata.entityType(withName: "S1.A_InbDeliveryItemTextType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryItemType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryItemType = metadata.entityType(withName: "S1.A_InbDeliveryItemType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryPartnerType = metadata.entityType(withName: "S1.A_InbDeliveryPartnerType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType = metadata.entityType(withName: "S1.A_InbDeliverySerialNmbrType")
        }
        if !Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.isRemoved {
            Ec1Metadata.EntityTypes.aMaintenanceItemObjListType = metadata.entityType(withName: "S1.A_MaintenanceItemObjListType")
        }
        if !Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.isRemoved {
            Ec1Metadata.EntityTypes.aMaintenanceItemObjectType = metadata.entityType(withName: "S1.A_MaintenanceItemObjectType")
        }
        if !Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.isRemoved {
            Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType = metadata.entityType(withName: "S1.A_MaterialDocumentHeaderType")
        }
        if !Ec1Metadata.EntityTypes.aMaterialDocumentItemType.isRemoved {
            Ec1Metadata.EntityTypes.aMaterialDocumentItemType = metadata.entityType(withName: "S1.A_MaterialDocumentItemType")
        }
        if !Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.isRemoved {
            Ec1Metadata.EntityTypes.aOutbDeliveryAddressType = metadata.entityType(withName: "S1.A_OutbDeliveryAddressType")
        }
        if !Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.isRemoved {
            Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType = metadata.entityType(withName: "S1.A_OutbDeliveryDocFlowType")
        }
        if !Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.isRemoved {
            Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType = metadata.entityType(withName: "S1.A_OutbDeliveryHeaderType")
        }
        if !Ec1Metadata.EntityTypes.aOutbDeliveryItemType.isRemoved {
            Ec1Metadata.EntityTypes.aOutbDeliveryItemType = metadata.entityType(withName: "S1.A_OutbDeliveryItemType")
        }
        if !Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.isRemoved {
            Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType = metadata.entityType(withName: "S1.A_OutbDeliveryPartnerType")
        }
        if !Ec1Metadata.EntityTypes.aPlantType.isRemoved {
            Ec1Metadata.EntityTypes.aPlantType = metadata.entityType(withName: "S1.A_PlantType")
        }
        if !Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.isRemoved {
            Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType = metadata.entityType(withName: "S1.A_SerialNmbrDeliveryType")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryAddressTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryAddressTypeSet = metadata.entitySet(withName: "A_InbDeliveryAddressTypeSet")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryDocFlowTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryDocFlowTypeSet = metadata.entitySet(withName: "A_InbDeliveryDocFlowTypeSet")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryHeader.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryHeader = metadata.entitySet(withName: "A_InbDeliveryHeader")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryHeaderTextTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryHeaderTextTypeSet = metadata.entitySet(withName: "A_InbDeliveryHeaderTextTypeSet")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryItem.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryItem = metadata.entitySet(withName: "A_InbDeliveryItem")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryItemTextTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryItemTextTypeSet = metadata.entitySet(withName: "A_InbDeliveryItemTextTypeSet")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryPartner.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryPartner = metadata.entitySet(withName: "A_InbDeliveryPartner")
        }
        if !Ec1Metadata.EntitySets.aInbDeliverySerialNmbrTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliverySerialNmbrTypeSet = metadata.entitySet(withName: "A_InbDeliverySerialNmbrTypeSet")
        }
        if !Ec1Metadata.EntitySets.aMaintenanceItemObjListTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aMaintenanceItemObjListTypeSet = metadata.entitySet(withName: "A_MaintenanceItemObjListTypeSet")
        }
        if !Ec1Metadata.EntitySets.aMaintenanceItemObjectTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aMaintenanceItemObjectTypeSet = metadata.entitySet(withName: "A_MaintenanceItemObjectTypeSet")
        }
        if !Ec1Metadata.EntitySets.aMaterialDocumentHeader.isRemoved {
            Ec1Metadata.EntitySets.aMaterialDocumentHeader = metadata.entitySet(withName: "A_MaterialDocumentHeader")
        }
        if !Ec1Metadata.EntitySets.aMaterialDocumentItemTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aMaterialDocumentItemTypeSet = metadata.entitySet(withName: "A_MaterialDocumentItemTypeSet")
        }
        if !Ec1Metadata.EntitySets.aOutbDeliveryAddressTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aOutbDeliveryAddressTypeSet = metadata.entitySet(withName: "A_OutbDeliveryAddressTypeSet")
        }
        if !Ec1Metadata.EntitySets.aOutbDeliveryDocFlowTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aOutbDeliveryDocFlowTypeSet = metadata.entitySet(withName: "A_OutbDeliveryDocFlowTypeSet")
        }
        if !Ec1Metadata.EntitySets.aOutbDeliveryHeader.isRemoved {
            Ec1Metadata.EntitySets.aOutbDeliveryHeader = metadata.entitySet(withName: "A_OutbDeliveryHeader")
        }
        if !Ec1Metadata.EntitySets.aOutbDeliveryItem.isRemoved {
            Ec1Metadata.EntitySets.aOutbDeliveryItem = metadata.entitySet(withName: "A_OutbDeliveryItem")
        }
        if !Ec1Metadata.EntitySets.aOutbDeliveryPartnerTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aOutbDeliveryPartnerTypeSet = metadata.entitySet(withName: "A_OutbDeliveryPartnerTypeSet")
        }
        if !Ec1Metadata.EntitySets.aPlant.isRemoved {
            Ec1Metadata.EntitySets.aPlant = metadata.entitySet(withName: "A_Plant")
        }
        if !Ec1Metadata.EntitySets.aSerialNmbrDeliveryTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aSerialNmbrDeliveryTypeSet = metadata.entitySet(withName: "A_SerialNmbrDeliveryTypeSet")
        }
        if !Ec1Metadata.ActionImports.cancel.isRemoved {
            Ec1Metadata.ActionImports.cancel = metadata.dataMethod(withName: "Cancel")
        }
        if !Ec1Metadata.ActionImports.cancelItem.isRemoved {
            Ec1Metadata.ActionImports.cancelItem = metadata.dataMethod(withName: "CancelItem")
        }
        if !Ec1Metadata.ActionImports.confirmPickingAllItems.isRemoved {
            Ec1Metadata.ActionImports.confirmPickingAllItems = metadata.dataMethod(withName: "ConfirmPickingAllItems")
        }
        if !Ec1Metadata.ActionImports.confirmPickingOneItem.isRemoved {
            Ec1Metadata.ActionImports.confirmPickingOneItem = metadata.dataMethod(withName: "ConfirmPickingOneItem")
        }
        if !Ec1Metadata.ActionImports.confirmPutawayAllItems.isRemoved {
            Ec1Metadata.ActionImports.confirmPutawayAllItems = metadata.dataMethod(withName: "ConfirmPutawayAllItems")
        }
        if !Ec1Metadata.ActionImports.confirmPutawayOneItem.isRemoved {
            Ec1Metadata.ActionImports.confirmPutawayOneItem = metadata.dataMethod(withName: "ConfirmPutawayOneItem")
        }
        if !Ec1Metadata.ActionImports.pickAllItems.isRemoved {
            Ec1Metadata.ActionImports.pickAllItems = metadata.dataMethod(withName: "PickAllItems")
        }
        if !Ec1Metadata.ActionImports.pickAndBatchSplitOneItem.isRemoved {
            Ec1Metadata.ActionImports.pickAndBatchSplitOneItem = metadata.dataMethod(withName: "PickAndBatchSplitOneItem")
        }
        if !Ec1Metadata.ActionImports.pickOneItem.isRemoved {
            Ec1Metadata.ActionImports.pickOneItem = metadata.dataMethod(withName: "PickOneItem")
        }
        if !Ec1Metadata.ActionImports.pickOneItemWithBaseQuantity.isRemoved {
            Ec1Metadata.ActionImports.pickOneItemWithBaseQuantity = metadata.dataMethod(withName: "PickOneItemWithBaseQuantity")
        }
        if !Ec1Metadata.ActionImports.pickOneItemWithSalesQuantity.isRemoved {
            Ec1Metadata.ActionImports.pickOneItemWithSalesQuantity = metadata.dataMethod(withName: "PickOneItemWithSalesQuantity")
        }
        if !Ec1Metadata.ActionImports.postGoodsIssue.isRemoved {
            Ec1Metadata.ActionImports.postGoodsIssue = metadata.dataMethod(withName: "PostGoodsIssue")
        }
        if !Ec1Metadata.ActionImports.postGoodsReceipt.isRemoved {
            Ec1Metadata.ActionImports.postGoodsReceipt = metadata.dataMethod(withName: "PostGoodsReceipt")
        }
        if !Ec1Metadata.ActionImports.putawayAllItems.isRemoved {
            Ec1Metadata.ActionImports.putawayAllItems = metadata.dataMethod(withName: "PutawayAllItems")
        }
        if !Ec1Metadata.ActionImports.putawayOneItem.isRemoved {
            Ec1Metadata.ActionImports.putawayOneItem = metadata.dataMethod(withName: "PutawayOneItem")
        }
        if !Ec1Metadata.ActionImports.reverseGoodsIssue.isRemoved {
            Ec1Metadata.ActionImports.reverseGoodsIssue = metadata.dataMethod(withName: "ReverseGoodsIssue")
        }
        if !Ec1Metadata.ActionImports.reverseGoodsReceipt.isRemoved {
            Ec1Metadata.ActionImports.reverseGoodsReceipt = metadata.dataMethod(withName: "ReverseGoodsReceipt")
        }
        if !Ec1Metadata.ActionImports.setPickingQuantityWithBaseQuantity.isRemoved {
            Ec1Metadata.ActionImports.setPickingQuantityWithBaseQuantity = metadata.dataMethod(withName: "SetPickingQuantityWithBaseQuantity")
        }
        if !Ec1Metadata.ActionImports.setPutawayQuantityWithBaseQuantity.isRemoved {
            Ec1Metadata.ActionImports.setPutawayQuantityWithBaseQuantity = metadata.dataMethod(withName: "SetPutawayQuantityWithBaseQuantity")
        }
        if !DeliveryMessage.collectiveProcessing.isRemoved {
            DeliveryMessage.collectiveProcessing = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "CollectiveProcessing")
        }
        if !DeliveryMessage.deliveryDocument.isRemoved {
            DeliveryMessage.deliveryDocument = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "DeliveryDocument")
        }
        if !DeliveryMessage.deliveryDocumentItem.isRemoved {
            DeliveryMessage.deliveryDocumentItem = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "DeliveryDocumentItem")
        }
        if !DeliveryMessage.scheduleLine.isRemoved {
            DeliveryMessage.scheduleLine = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "ScheduleLine")
        }
        if !DeliveryMessage.collectiveProcessingMsgCounter.isRemoved {
            DeliveryMessage.collectiveProcessingMsgCounter = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "CollectiveProcessingMsgCounter")
        }
        if !DeliveryMessage.systemMessageIdentification.isRemoved {
            DeliveryMessage.systemMessageIdentification = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageIdentification")
        }
        if !DeliveryMessage.systemMessageNumber.isRemoved {
            DeliveryMessage.systemMessageNumber = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageNumber")
        }
        if !DeliveryMessage.systemMessageType.isRemoved {
            DeliveryMessage.systemMessageType = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageType")
        }
        if !DeliveryMessage.systemMessageVariable1.isRemoved {
            DeliveryMessage.systemMessageVariable1 = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageVariable1")
        }
        if !DeliveryMessage.systemMessageVariable2.isRemoved {
            DeliveryMessage.systemMessageVariable2 = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageVariable2")
        }
        if !DeliveryMessage.systemMessageVariable3.isRemoved {
            DeliveryMessage.systemMessageVariable3 = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageVariable3")
        }
        if !DeliveryMessage.systemMessageVariable4.isRemoved {
            DeliveryMessage.systemMessageVariable4 = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageVariable4")
        }
        if !DeliveryMessage.collectiveProcessingType.isRemoved {
            DeliveryMessage.collectiveProcessingType = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "CollectiveProcessingType")
        }
        if !PickingReport.systemMessageIdentification.isRemoved {
            PickingReport.systemMessageIdentification = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageIdentification")
        }
        if !PickingReport.systemMessageNumber.isRemoved {
            PickingReport.systemMessageNumber = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageNumber")
        }
        if !PickingReport.systemMessageType.isRemoved {
            PickingReport.systemMessageType = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageType")
        }
        if !PickingReport.systemMessageVariable1.isRemoved {
            PickingReport.systemMessageVariable1 = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageVariable1")
        }
        if !PickingReport.systemMessageVariable2.isRemoved {
            PickingReport.systemMessageVariable2 = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageVariable2")
        }
        if !PickingReport.systemMessageVariable3.isRemoved {
            PickingReport.systemMessageVariable3 = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageVariable3")
        }
        if !PickingReport.systemMessageVariable4.isRemoved {
            PickingReport.systemMessageVariable4 = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageVariable4")
        }
        if !PickingReport.batch.isRemoved {
            PickingReport.batch = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "Batch")
        }
        if !PickingReport.deliveryQuantityUnit.isRemoved {
            PickingReport.deliveryQuantityUnit = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "DeliveryQuantityUnit")
        }
        if !PickingReport.actualDeliveryQuantity.isRemoved {
            PickingReport.actualDeliveryQuantity = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "ActualDeliveryQuantity")
        }
        if !PickingReport.deliveryDocumentItemText.isRemoved {
            PickingReport.deliveryDocumentItemText = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "DeliveryDocumentItemText")
        }
        if !PickingReport.material.isRemoved {
            PickingReport.material = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "Material")
        }
        if !PickingReport.deliveryDocumentItem.isRemoved {
            PickingReport.deliveryDocumentItem = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "DeliveryDocumentItem")
        }
        if !PickingReport.deliveryDocument.isRemoved {
            PickingReport.deliveryDocument = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "DeliveryDocument")
        }
        if !PutawayReport.systemMessageIdentification.isRemoved {
            PutawayReport.systemMessageIdentification = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageIdentification")
        }
        if !PutawayReport.systemMessageNumber.isRemoved {
            PutawayReport.systemMessageNumber = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageNumber")
        }
        if !PutawayReport.systemMessageType.isRemoved {
            PutawayReport.systemMessageType = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageType")
        }
        if !PutawayReport.systemMessageVariable1.isRemoved {
            PutawayReport.systemMessageVariable1 = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageVariable1")
        }
        if !PutawayReport.systemMessageVariable2.isRemoved {
            PutawayReport.systemMessageVariable2 = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageVariable2")
        }
        if !PutawayReport.systemMessageVariable3.isRemoved {
            PutawayReport.systemMessageVariable3 = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageVariable3")
        }
        if !PutawayReport.systemMessageVariable4.isRemoved {
            PutawayReport.systemMessageVariable4 = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageVariable4")
        }
        if !PutawayReport.batch.isRemoved {
            PutawayReport.batch = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "Batch")
        }
        if !PutawayReport.deliveryQuantityUnit.isRemoved {
            PutawayReport.deliveryQuantityUnit = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "DeliveryQuantityUnit")
        }
        if !PutawayReport.actualDeliveryQuantity.isRemoved {
            PutawayReport.actualDeliveryQuantity = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "ActualDeliveryQuantity")
        }
        if !PutawayReport.deliveryDocumentItemText.isRemoved {
            PutawayReport.deliveryDocumentItemText = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "DeliveryDocumentItemText")
        }
        if !PutawayReport.material.isRemoved {
            PutawayReport.material = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "Material")
        }
        if !PutawayReport.deliveryDocumentItem.isRemoved {
            PutawayReport.deliveryDocumentItem = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "DeliveryDocumentItem")
        }
        if !PutawayReport.deliveryDocument.isRemoved {
            PutawayReport.deliveryDocument = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "DeliveryDocument")
        }
        if !Return.collectiveProcessing.isRemoved {
            Return.collectiveProcessing = Ec1Metadata.ComplexTypes.return.property(withName: "CollectiveProcessing")
        }
        if !Return.deliveryDocument.isRemoved {
            Return.deliveryDocument = Ec1Metadata.ComplexTypes.return.property(withName: "DeliveryDocument")
        }
        if !Return.deliveryDocumentItem.isRemoved {
            Return.deliveryDocumentItem = Ec1Metadata.ComplexTypes.return.property(withName: "DeliveryDocumentItem")
        }
        if !Return.scheduleLine.isRemoved {
            Return.scheduleLine = Ec1Metadata.ComplexTypes.return.property(withName: "ScheduleLine")
        }
        if !Return.collectiveProcessingMsgCounter.isRemoved {
            Return.collectiveProcessingMsgCounter = Ec1Metadata.ComplexTypes.return.property(withName: "CollectiveProcessingMsgCounter")
        }
        if !Return.systemMessageIdentification.isRemoved {
            Return.systemMessageIdentification = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageIdentification")
        }
        if !Return.systemMessageNumber.isRemoved {
            Return.systemMessageNumber = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageNumber")
        }
        if !Return.systemMessageType.isRemoved {
            Return.systemMessageType = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageType")
        }
        if !Return.systemMessageVariable1.isRemoved {
            Return.systemMessageVariable1 = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageVariable1")
        }
        if !Return.systemMessageVariable2.isRemoved {
            Return.systemMessageVariable2 = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageVariable2")
        }
        if !Return.systemMessageVariable3.isRemoved {
            Return.systemMessageVariable3 = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageVariable3")
        }
        if !Return.systemMessageVariable4.isRemoved {
            Return.systemMessageVariable4 = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageVariable4")
        }
        if !Return.collectiveProcessingType.isRemoved {
            Return.collectiveProcessingType = Ec1Metadata.ComplexTypes.return.property(withName: "CollectiveProcessingType")
        }
        if !AInbDeliveryAddressType.addressID.isRemoved {
            AInbDeliveryAddressType.addressID = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "AddressID")
        }
        if !AInbDeliveryAddressType.additionalStreetPrefixName.isRemoved {
            AInbDeliveryAddressType.additionalStreetPrefixName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "AdditionalStreetPrefixName")
        }
        if !AInbDeliveryAddressType.additionalStreetSuffixName.isRemoved {
            AInbDeliveryAddressType.additionalStreetSuffixName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "AdditionalStreetSuffixName")
        }
        if !AInbDeliveryAddressType.addressTimeZone.isRemoved {
            AInbDeliveryAddressType.addressTimeZone = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "AddressTimeZone")
        }
        if !AInbDeliveryAddressType.building.isRemoved {
            AInbDeliveryAddressType.building = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Building")
        }
        if !AInbDeliveryAddressType.businessPartnerName1.isRemoved {
            AInbDeliveryAddressType.businessPartnerName1 = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "BusinessPartnerName1")
        }
        if !AInbDeliveryAddressType.businessPartnerName2.isRemoved {
            AInbDeliveryAddressType.businessPartnerName2 = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "BusinessPartnerName2")
        }
        if !AInbDeliveryAddressType.businessPartnerName3.isRemoved {
            AInbDeliveryAddressType.businessPartnerName3 = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "BusinessPartnerName3")
        }
        if !AInbDeliveryAddressType.businessPartnerName4.isRemoved {
            AInbDeliveryAddressType.businessPartnerName4 = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "BusinessPartnerName4")
        }
        if !AInbDeliveryAddressType.careOfName.isRemoved {
            AInbDeliveryAddressType.careOfName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CareOfName")
        }
        if !AInbDeliveryAddressType.cityCode.isRemoved {
            AInbDeliveryAddressType.cityCode = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CityCode")
        }
        if !AInbDeliveryAddressType.cityName.isRemoved {
            AInbDeliveryAddressType.cityName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CityName")
        }
        if !AInbDeliveryAddressType.citySearch.isRemoved {
            AInbDeliveryAddressType.citySearch = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CitySearch")
        }
        if !AInbDeliveryAddressType.companyPostalCode.isRemoved {
            AInbDeliveryAddressType.companyPostalCode = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CompanyPostalCode")
        }
        if !AInbDeliveryAddressType.correspondenceLanguage.isRemoved {
            AInbDeliveryAddressType.correspondenceLanguage = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CorrespondenceLanguage")
        }
        if !AInbDeliveryAddressType.country.isRemoved {
            AInbDeliveryAddressType.country = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Country")
        }
        if !AInbDeliveryAddressType.county.isRemoved {
            AInbDeliveryAddressType.county = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "County")
        }
        if !AInbDeliveryAddressType.deliveryServiceNumber.isRemoved {
            AInbDeliveryAddressType.deliveryServiceNumber = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "DeliveryServiceNumber")
        }
        if !AInbDeliveryAddressType.deliveryServiceTypeCode.isRemoved {
            AInbDeliveryAddressType.deliveryServiceTypeCode = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "DeliveryServiceTypeCode")
        }
        if !AInbDeliveryAddressType.district.isRemoved {
            AInbDeliveryAddressType.district = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "District")
        }
        if !AInbDeliveryAddressType.faxNumber.isRemoved {
            AInbDeliveryAddressType.faxNumber = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "FaxNumber")
        }
        if !AInbDeliveryAddressType.floor.isRemoved {
            AInbDeliveryAddressType.floor = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Floor")
        }
        if !AInbDeliveryAddressType.formOfAddress.isRemoved {
            AInbDeliveryAddressType.formOfAddress = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "FormOfAddress")
        }
        if !AInbDeliveryAddressType.fullName.isRemoved {
            AInbDeliveryAddressType.fullName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "FullName")
        }
        if !AInbDeliveryAddressType.homeCityName.isRemoved {
            AInbDeliveryAddressType.homeCityName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "HomeCityName")
        }
        if !AInbDeliveryAddressType.houseNumber.isRemoved {
            AInbDeliveryAddressType.houseNumber = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "HouseNumber")
        }
        if !AInbDeliveryAddressType.houseNumberSupplementText.isRemoved {
            AInbDeliveryAddressType.houseNumberSupplementText = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "HouseNumberSupplementText")
        }
        if !AInbDeliveryAddressType.nation.isRemoved {
            AInbDeliveryAddressType.nation = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Nation")
        }
        if !AInbDeliveryAddressType.person.isRemoved {
            AInbDeliveryAddressType.person = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Person")
        }
        if !AInbDeliveryAddressType.phoneNumber.isRemoved {
            AInbDeliveryAddressType.phoneNumber = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "PhoneNumber")
        }
        if !AInbDeliveryAddressType.poBox.isRemoved {
            AInbDeliveryAddressType.poBox = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBox")
        }
        if !AInbDeliveryAddressType.poBoxDeviatingCityName.isRemoved {
            AInbDeliveryAddressType.poBoxDeviatingCityName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxDeviatingCityName")
        }
        if !AInbDeliveryAddressType.poBoxDeviatingCountry.isRemoved {
            AInbDeliveryAddressType.poBoxDeviatingCountry = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxDeviatingCountry")
        }
        if !AInbDeliveryAddressType.poBoxDeviatingRegion.isRemoved {
            AInbDeliveryAddressType.poBoxDeviatingRegion = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxDeviatingRegion")
        }
        if !AInbDeliveryAddressType.poBoxIsWithoutNumber.isRemoved {
            AInbDeliveryAddressType.poBoxIsWithoutNumber = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxIsWithoutNumber")
        }
        if !AInbDeliveryAddressType.poBoxLobbyName.isRemoved {
            AInbDeliveryAddressType.poBoxLobbyName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxLobbyName")
        }
        if !AInbDeliveryAddressType.poBoxPostalCode.isRemoved {
            AInbDeliveryAddressType.poBoxPostalCode = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxPostalCode")
        }
        if !AInbDeliveryAddressType.postalCode.isRemoved {
            AInbDeliveryAddressType.postalCode = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "PostalCode")
        }
        if !AInbDeliveryAddressType.prfrdCommMediumType.isRemoved {
            AInbDeliveryAddressType.prfrdCommMediumType = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "PrfrdCommMediumType")
        }
        if !AInbDeliveryAddressType.region.isRemoved {
            AInbDeliveryAddressType.region = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Region")
        }
        if !AInbDeliveryAddressType.roomNumber.isRemoved {
            AInbDeliveryAddressType.roomNumber = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "RoomNumber")
        }
        if !AInbDeliveryAddressType.searchTerm1.isRemoved {
            AInbDeliveryAddressType.searchTerm1 = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "SearchTerm1")
        }
        if !AInbDeliveryAddressType.streetName.isRemoved {
            AInbDeliveryAddressType.streetName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "StreetName")
        }
        if !AInbDeliveryAddressType.streetPrefixName.isRemoved {
            AInbDeliveryAddressType.streetPrefixName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "StreetPrefixName")
        }
        if !AInbDeliveryAddressType.streetSearch.isRemoved {
            AInbDeliveryAddressType.streetSearch = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "StreetSearch")
        }
        if !AInbDeliveryAddressType.streetSuffixName.isRemoved {
            AInbDeliveryAddressType.streetSuffixName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "StreetSuffixName")
        }
        if !AInbDeliveryAddressType.taxJurisdiction.isRemoved {
            AInbDeliveryAddressType.taxJurisdiction = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "TaxJurisdiction")
        }
        if !AInbDeliveryAddressType.transportZone.isRemoved {
            AInbDeliveryAddressType.transportZone = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "TransportZone")
        }
        if !AInbDeliveryDocFlowType.deliveryVersion.isRemoved {
            AInbDeliveryDocFlowType.deliveryVersion = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "DeliveryVersion")
        }
        if !AInbDeliveryDocFlowType.precedingDocument.isRemoved {
            AInbDeliveryDocFlowType.precedingDocument = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "PrecedingDocument")
        }
        if !AInbDeliveryDocFlowType.precedingDocumentCategory.isRemoved {
            AInbDeliveryDocFlowType.precedingDocumentCategory = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "PrecedingDocumentCategory")
        }
        if !AInbDeliveryDocFlowType.precedingDocumentItem.isRemoved {
            AInbDeliveryDocFlowType.precedingDocumentItem = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "PrecedingDocumentItem")
        }
        if !AInbDeliveryDocFlowType.quantityInBaseUnit.isRemoved {
            AInbDeliveryDocFlowType.quantityInBaseUnit = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "QuantityInBaseUnit")
        }
        if !AInbDeliveryDocFlowType.sdFulfillmentCalculationRule.isRemoved {
            AInbDeliveryDocFlowType.sdFulfillmentCalculationRule = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "SDFulfillmentCalculationRule")
        }
        if !AInbDeliveryDocFlowType.subsequentDocument.isRemoved {
            AInbDeliveryDocFlowType.subsequentDocument = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "SubsequentDocument")
        }
        if !AInbDeliveryDocFlowType.subsequentDocumentCategory.isRemoved {
            AInbDeliveryDocFlowType.subsequentDocumentCategory = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "SubsequentDocumentCategory")
        }
        if !AInbDeliveryDocFlowType.subsequentDocumentItem.isRemoved {
            AInbDeliveryDocFlowType.subsequentDocumentItem = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "SubsequentDocumentItem")
        }
        if !AInbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd.isRemoved {
            AInbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "TransferOrderInWrhsMgmtIsConfd")
        }
        if !AInbDeliveryHeaderTextType.deliveryDocument.isRemoved {
            AInbDeliveryHeaderTextType.deliveryDocument = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "DeliveryDocument")
        }
        if !AInbDeliveryHeaderTextType.textElement.isRemoved {
            AInbDeliveryHeaderTextType.textElement = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "TextElement")
        }
        if !AInbDeliveryHeaderTextType.language.isRemoved {
            AInbDeliveryHeaderTextType.language = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "Language")
        }
        if !AInbDeliveryHeaderTextType.textElementDescription.isRemoved {
            AInbDeliveryHeaderTextType.textElementDescription = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "TextElementDescription")
        }
        if !AInbDeliveryHeaderTextType.textElementText.isRemoved {
            AInbDeliveryHeaderTextType.textElementText = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "TextElementText")
        }
        if !AInbDeliveryHeaderType.actualDeliveryRoute.isRemoved {
            AInbDeliveryHeaderType.actualDeliveryRoute = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ActualDeliveryRoute")
        }
        if !AInbDeliveryHeaderType.actualGoodsMovementDate.isRemoved {
            AInbDeliveryHeaderType.actualGoodsMovementDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ActualGoodsMovementDate")
        }
        if !AInbDeliveryHeaderType.actualGoodsMovementTime.isRemoved {
            AInbDeliveryHeaderType.actualGoodsMovementTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ActualGoodsMovementTime")
        }
        if !AInbDeliveryHeaderType.billingDocumentDate.isRemoved {
            AInbDeliveryHeaderType.billingDocumentDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "BillingDocumentDate")
        }
        if !AInbDeliveryHeaderType.billOfLading.isRemoved {
            AInbDeliveryHeaderType.billOfLading = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "BillOfLading")
        }
        if !AInbDeliveryHeaderType.completeDeliveryIsDefined.isRemoved {
            AInbDeliveryHeaderType.completeDeliveryIsDefined = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CompleteDeliveryIsDefined")
        }
        if !AInbDeliveryHeaderType.confirmationTime.isRemoved {
            AInbDeliveryHeaderType.confirmationTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ConfirmationTime")
        }
        if !AInbDeliveryHeaderType.createdByUser.isRemoved {
            AInbDeliveryHeaderType.createdByUser = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CreatedByUser")
        }
        if !AInbDeliveryHeaderType.creationDate.isRemoved {
            AInbDeliveryHeaderType.creationDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CreationDate")
        }
        if !AInbDeliveryHeaderType.creationTime.isRemoved {
            AInbDeliveryHeaderType.creationTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CreationTime")
        }
        if !AInbDeliveryHeaderType.customerGroup.isRemoved {
            AInbDeliveryHeaderType.customerGroup = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CustomerGroup")
        }
        if !AInbDeliveryHeaderType.deliveryBlockReason.isRemoved {
            AInbDeliveryHeaderType.deliveryBlockReason = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryBlockReason")
        }
        if !AInbDeliveryHeaderType.deliveryDate.isRemoved {
            AInbDeliveryHeaderType.deliveryDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryDate")
        }
        if !AInbDeliveryHeaderType.deliveryDocument.isRemoved {
            AInbDeliveryHeaderType.deliveryDocument = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryDocument")
        }
        if !AInbDeliveryHeaderType.deliveryDocumentBySupplier.isRemoved {
            AInbDeliveryHeaderType.deliveryDocumentBySupplier = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryDocumentBySupplier")
        }
        if !AInbDeliveryHeaderType.deliveryDocumentType.isRemoved {
            AInbDeliveryHeaderType.deliveryDocumentType = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryDocumentType")
        }
        if !AInbDeliveryHeaderType.deliveryIsInPlant.isRemoved {
            AInbDeliveryHeaderType.deliveryIsInPlant = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryIsInPlant")
        }
        if !AInbDeliveryHeaderType.deliveryPriority.isRemoved {
            AInbDeliveryHeaderType.deliveryPriority = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryPriority")
        }
        if !AInbDeliveryHeaderType.deliveryTime.isRemoved {
            AInbDeliveryHeaderType.deliveryTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryTime")
        }
        if !AInbDeliveryHeaderType.deliveryVersion.isRemoved {
            AInbDeliveryHeaderType.deliveryVersion = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryVersion")
        }
        if !AInbDeliveryHeaderType.depreciationPercentage.isRemoved {
            AInbDeliveryHeaderType.depreciationPercentage = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DepreciationPercentage")
        }
        if !AInbDeliveryHeaderType.distrStatusByDecentralizedWrhs.isRemoved {
            AInbDeliveryHeaderType.distrStatusByDecentralizedWrhs = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DistrStatusByDecentralizedWrhs")
        }
        if !AInbDeliveryHeaderType.documentDate.isRemoved {
            AInbDeliveryHeaderType.documentDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DocumentDate")
        }
        if !AInbDeliveryHeaderType.externalIdentificationType.isRemoved {
            AInbDeliveryHeaderType.externalIdentificationType = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ExternalIdentificationType")
        }
        if !AInbDeliveryHeaderType.externalTransportSystem.isRemoved {
            AInbDeliveryHeaderType.externalTransportSystem = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ExternalTransportSystem")
        }
        if !AInbDeliveryHeaderType.factoryCalendarByCustomer.isRemoved {
            AInbDeliveryHeaderType.factoryCalendarByCustomer = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "FactoryCalendarByCustomer")
        }
        if !AInbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber.isRemoved {
            AInbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "GoodsIssueOrReceiptSlipNumber")
        }
        if !AInbDeliveryHeaderType.goodsIssueTime.isRemoved {
            AInbDeliveryHeaderType.goodsIssueTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "GoodsIssueTime")
        }
        if !AInbDeliveryHeaderType.handlingUnitInStock.isRemoved {
            AInbDeliveryHeaderType.handlingUnitInStock = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HandlingUnitInStock")
        }
        if !AInbDeliveryHeaderType.hdrGeneralIncompletionStatus.isRemoved {
            AInbDeliveryHeaderType.hdrGeneralIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HdrGeneralIncompletionStatus")
        }
        if !AInbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus.isRemoved {
            AInbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HdrGoodsMvtIncompletionStatus")
        }
        if !AInbDeliveryHeaderType.headerBillgIncompletionStatus.isRemoved {
            AInbDeliveryHeaderType.headerBillgIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderBillgIncompletionStatus")
        }
        if !AInbDeliveryHeaderType.headerBillingBlockReason.isRemoved {
            AInbDeliveryHeaderType.headerBillingBlockReason = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderBillingBlockReason")
        }
        if !AInbDeliveryHeaderType.headerDelivIncompletionStatus.isRemoved {
            AInbDeliveryHeaderType.headerDelivIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderDelivIncompletionStatus")
        }
        if !AInbDeliveryHeaderType.headerGrossWeight.isRemoved {
            AInbDeliveryHeaderType.headerGrossWeight = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderGrossWeight")
        }
        if !AInbDeliveryHeaderType.headerNetWeight.isRemoved {
            AInbDeliveryHeaderType.headerNetWeight = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderNetWeight")
        }
        if !AInbDeliveryHeaderType.headerPackingIncompletionSts.isRemoved {
            AInbDeliveryHeaderType.headerPackingIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderPackingIncompletionSts")
        }
        if !AInbDeliveryHeaderType.headerPickgIncompletionStatus.isRemoved {
            AInbDeliveryHeaderType.headerPickgIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderPickgIncompletionStatus")
        }
        if !AInbDeliveryHeaderType.headerVolume.isRemoved {
            AInbDeliveryHeaderType.headerVolume = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderVolume")
        }
        if !AInbDeliveryHeaderType.headerVolumeUnit.isRemoved {
            AInbDeliveryHeaderType.headerVolumeUnit = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderVolumeUnit")
        }
        if !AInbDeliveryHeaderType.headerWeightUnit.isRemoved {
            AInbDeliveryHeaderType.headerWeightUnit = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderWeightUnit")
        }
        if !AInbDeliveryHeaderType.incotermsClassification.isRemoved {
            AInbDeliveryHeaderType.incotermsClassification = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IncotermsClassification")
        }
        if !AInbDeliveryHeaderType.incotermsTransferLocation.isRemoved {
            AInbDeliveryHeaderType.incotermsTransferLocation = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IncotermsTransferLocation")
        }
        if !AInbDeliveryHeaderType.intercompanyBillingDate.isRemoved {
            AInbDeliveryHeaderType.intercompanyBillingDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IntercompanyBillingDate")
        }
        if !AInbDeliveryHeaderType.internalFinancialDocument.isRemoved {
            AInbDeliveryHeaderType.internalFinancialDocument = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "InternalFinancialDocument")
        }
        if !AInbDeliveryHeaderType.isDeliveryForSingleWarehouse.isRemoved {
            AInbDeliveryHeaderType.isDeliveryForSingleWarehouse = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IsDeliveryForSingleWarehouse")
        }
        if !AInbDeliveryHeaderType.isExportDelivery.isRemoved {
            AInbDeliveryHeaderType.isExportDelivery = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IsExportDelivery")
        }
        if !AInbDeliveryHeaderType.lastChangeDate.isRemoved {
            AInbDeliveryHeaderType.lastChangeDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LastChangeDate")
        }
        if !AInbDeliveryHeaderType.lastChangedByUser.isRemoved {
            AInbDeliveryHeaderType.lastChangedByUser = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LastChangedByUser")
        }
        if !AInbDeliveryHeaderType.loadingDate.isRemoved {
            AInbDeliveryHeaderType.loadingDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LoadingDate")
        }
        if !AInbDeliveryHeaderType.loadingPoint.isRemoved {
            AInbDeliveryHeaderType.loadingPoint = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LoadingPoint")
        }
        if !AInbDeliveryHeaderType.loadingTime.isRemoved {
            AInbDeliveryHeaderType.loadingTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LoadingTime")
        }
        if !AInbDeliveryHeaderType.meansOfTransport.isRemoved {
            AInbDeliveryHeaderType.meansOfTransport = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "MeansOfTransport")
        }
        if !AInbDeliveryHeaderType.meansOfTransportRefMaterial.isRemoved {
            AInbDeliveryHeaderType.meansOfTransportRefMaterial = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "MeansOfTransportRefMaterial")
        }
        if !AInbDeliveryHeaderType.meansOfTransportType.isRemoved {
            AInbDeliveryHeaderType.meansOfTransportType = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "MeansOfTransportType")
        }
        if !AInbDeliveryHeaderType.orderCombinationIsAllowed.isRemoved {
            AInbDeliveryHeaderType.orderCombinationIsAllowed = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OrderCombinationIsAllowed")
        }
        if !AInbDeliveryHeaderType.orderID.isRemoved {
            AInbDeliveryHeaderType.orderID = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OrderID")
        }
        if !AInbDeliveryHeaderType.overallDelivConfStatus.isRemoved {
            AInbDeliveryHeaderType.overallDelivConfStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallDelivConfStatus")
        }
        if !AInbDeliveryHeaderType.overallDelivReltdBillgStatus.isRemoved {
            AInbDeliveryHeaderType.overallDelivReltdBillgStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallDelivReltdBillgStatus")
        }
        if !AInbDeliveryHeaderType.overallGoodsMovementStatus.isRemoved {
            AInbDeliveryHeaderType.overallGoodsMovementStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallGoodsMovementStatus")
        }
        if !AInbDeliveryHeaderType.overallIntcoBillingStatus.isRemoved {
            AInbDeliveryHeaderType.overallIntcoBillingStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallIntcoBillingStatus")
        }
        if !AInbDeliveryHeaderType.overallPackingStatus.isRemoved {
            AInbDeliveryHeaderType.overallPackingStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallPackingStatus")
        }
        if !AInbDeliveryHeaderType.overallPickingConfStatus.isRemoved {
            AInbDeliveryHeaderType.overallPickingConfStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallPickingConfStatus")
        }
        if !AInbDeliveryHeaderType.overallPickingStatus.isRemoved {
            AInbDeliveryHeaderType.overallPickingStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallPickingStatus")
        }
        if !AInbDeliveryHeaderType.overallProofOfDeliveryStatus.isRemoved {
            AInbDeliveryHeaderType.overallProofOfDeliveryStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallProofOfDeliveryStatus")
        }
        if !AInbDeliveryHeaderType.overallSDProcessStatus.isRemoved {
            AInbDeliveryHeaderType.overallSDProcessStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallSDProcessStatus")
        }
        if !AInbDeliveryHeaderType.overallWarehouseActivityStatus.isRemoved {
            AInbDeliveryHeaderType.overallWarehouseActivityStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallWarehouseActivityStatus")
        }
        if !AInbDeliveryHeaderType.ovrlItmDelivIncompletionSts.isRemoved {
            AInbDeliveryHeaderType.ovrlItmDelivIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmDelivIncompletionSts")
        }
        if !AInbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts.isRemoved {
            AInbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmGdsMvtIncompletionSts")
        }
        if !AInbDeliveryHeaderType.ovrlItmGeneralIncompletionSts.isRemoved {
            AInbDeliveryHeaderType.ovrlItmGeneralIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmGeneralIncompletionSts")
        }
        if !AInbDeliveryHeaderType.ovrlItmPackingIncompletionSts.isRemoved {
            AInbDeliveryHeaderType.ovrlItmPackingIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmPackingIncompletionSts")
        }
        if !AInbDeliveryHeaderType.ovrlItmPickingIncompletionSts.isRemoved {
            AInbDeliveryHeaderType.ovrlItmPickingIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmPickingIncompletionSts")
        }
        if !AInbDeliveryHeaderType.paymentGuaranteeProcedure.isRemoved {
            AInbDeliveryHeaderType.paymentGuaranteeProcedure = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PaymentGuaranteeProcedure")
        }
        if !AInbDeliveryHeaderType.pickedItemsLocation.isRemoved {
            AInbDeliveryHeaderType.pickedItemsLocation = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PickedItemsLocation")
        }
        if !AInbDeliveryHeaderType.pickingDate.isRemoved {
            AInbDeliveryHeaderType.pickingDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PickingDate")
        }
        if !AInbDeliveryHeaderType.pickingTime.isRemoved {
            AInbDeliveryHeaderType.pickingTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PickingTime")
        }
        if !AInbDeliveryHeaderType.plannedGoodsIssueDate.isRemoved {
            AInbDeliveryHeaderType.plannedGoodsIssueDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PlannedGoodsIssueDate")
        }
        if !AInbDeliveryHeaderType.proofOfDeliveryDate.isRemoved {
            AInbDeliveryHeaderType.proofOfDeliveryDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ProofOfDeliveryDate")
        }
        if !AInbDeliveryHeaderType.proposedDeliveryRoute.isRemoved {
            AInbDeliveryHeaderType.proposedDeliveryRoute = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ProposedDeliveryRoute")
        }
        if !AInbDeliveryHeaderType.receivingPlant.isRemoved {
            AInbDeliveryHeaderType.receivingPlant = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ReceivingPlant")
        }
        if !AInbDeliveryHeaderType.routeSchedule.isRemoved {
            AInbDeliveryHeaderType.routeSchedule = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "RouteSchedule")
        }
        if !AInbDeliveryHeaderType.salesDistrict.isRemoved {
            AInbDeliveryHeaderType.salesDistrict = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SalesDistrict")
        }
        if !AInbDeliveryHeaderType.salesOffice.isRemoved {
            AInbDeliveryHeaderType.salesOffice = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SalesOffice")
        }
        if !AInbDeliveryHeaderType.salesOrganization.isRemoved {
            AInbDeliveryHeaderType.salesOrganization = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SalesOrganization")
        }
        if !AInbDeliveryHeaderType.sdDocumentCategory.isRemoved {
            AInbDeliveryHeaderType.sdDocumentCategory = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SDDocumentCategory")
        }
        if !AInbDeliveryHeaderType.shipmentBlockReason.isRemoved {
            AInbDeliveryHeaderType.shipmentBlockReason = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShipmentBlockReason")
        }
        if !AInbDeliveryHeaderType.shippingCondition.isRemoved {
            AInbDeliveryHeaderType.shippingCondition = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShippingCondition")
        }
        if !AInbDeliveryHeaderType.shippingPoint.isRemoved {
            AInbDeliveryHeaderType.shippingPoint = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShippingPoint")
        }
        if !AInbDeliveryHeaderType.shippingType.isRemoved {
            AInbDeliveryHeaderType.shippingType = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShippingType")
        }
        if !AInbDeliveryHeaderType.shipToParty.isRemoved {
            AInbDeliveryHeaderType.shipToParty = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShipToParty")
        }
        if !AInbDeliveryHeaderType.soldToParty.isRemoved {
            AInbDeliveryHeaderType.soldToParty = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SoldToParty")
        }
        if !AInbDeliveryHeaderType.specialProcessingCode.isRemoved {
            AInbDeliveryHeaderType.specialProcessingCode = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SpecialProcessingCode")
        }
        if !AInbDeliveryHeaderType.statisticsCurrency.isRemoved {
            AInbDeliveryHeaderType.statisticsCurrency = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "StatisticsCurrency")
        }
        if !AInbDeliveryHeaderType.supplier.isRemoved {
            AInbDeliveryHeaderType.supplier = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "Supplier")
        }
        if !AInbDeliveryHeaderType.totalBlockStatus.isRemoved {
            AInbDeliveryHeaderType.totalBlockStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TotalBlockStatus")
        }
        if !AInbDeliveryHeaderType.totalCreditCheckStatus.isRemoved {
            AInbDeliveryHeaderType.totalCreditCheckStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TotalCreditCheckStatus")
        }
        if !AInbDeliveryHeaderType.totalNumberOfPackage.isRemoved {
            AInbDeliveryHeaderType.totalNumberOfPackage = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TotalNumberOfPackage")
        }
        if !AInbDeliveryHeaderType.transactionCurrency.isRemoved {
            AInbDeliveryHeaderType.transactionCurrency = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransactionCurrency")
        }
        if !AInbDeliveryHeaderType.transportationGroup.isRemoved {
            AInbDeliveryHeaderType.transportationGroup = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransportationGroup")
        }
        if !AInbDeliveryHeaderType.transportationPlanningDate.isRemoved {
            AInbDeliveryHeaderType.transportationPlanningDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransportationPlanningDate")
        }
        if !AInbDeliveryHeaderType.transportationPlanningStatus.isRemoved {
            AInbDeliveryHeaderType.transportationPlanningStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransportationPlanningStatus")
        }
        if !AInbDeliveryHeaderType.transportationPlanningTime.isRemoved {
            AInbDeliveryHeaderType.transportationPlanningTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransportationPlanningTime")
        }
        if !AInbDeliveryHeaderType.unloadingPointName.isRemoved {
            AInbDeliveryHeaderType.unloadingPointName = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "UnloadingPointName")
        }
        if !AInbDeliveryHeaderType.warehouse.isRemoved {
            AInbDeliveryHeaderType.warehouse = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "Warehouse")
        }
        if !AInbDeliveryHeaderType.warehouseGate.isRemoved {
            AInbDeliveryHeaderType.warehouseGate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "WarehouseGate")
        }
        if !AInbDeliveryHeaderType.warehouseStagingArea.isRemoved {
            AInbDeliveryHeaderType.warehouseStagingArea = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "WarehouseStagingArea")
        }
        if !AInbDeliveryItemTextType.deliveryDocument.isRemoved {
            AInbDeliveryItemTextType.deliveryDocument = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "DeliveryDocument")
        }
        if !AInbDeliveryItemTextType.deliveryDocumentItem.isRemoved {
            AInbDeliveryItemTextType.deliveryDocumentItem = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "DeliveryDocumentItem")
        }
        if !AInbDeliveryItemTextType.textElement.isRemoved {
            AInbDeliveryItemTextType.textElement = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "TextElement")
        }
        if !AInbDeliveryItemTextType.language.isRemoved {
            AInbDeliveryItemTextType.language = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "Language")
        }
        if !AInbDeliveryItemTextType.textElementDescription.isRemoved {
            AInbDeliveryItemTextType.textElementDescription = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "TextElementDescription")
        }
        if !AInbDeliveryItemTextType.textElementText.isRemoved {
            AInbDeliveryItemTextType.textElementText = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "TextElementText")
        }
        if !AInbDeliveryItemType.actualDeliveredQtyInBaseUnit.isRemoved {
            AInbDeliveryItemType.actualDeliveredQtyInBaseUnit = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ActualDeliveredQtyInBaseUnit")
        }
        if !AInbDeliveryItemType.actualDeliveryQuantity.isRemoved {
            AInbDeliveryItemType.actualDeliveryQuantity = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ActualDeliveryQuantity")
        }
        if !AInbDeliveryItemType.additionalCustomerGroup1.isRemoved {
            AInbDeliveryItemType.additionalCustomerGroup1 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup1")
        }
        if !AInbDeliveryItemType.additionalCustomerGroup2.isRemoved {
            AInbDeliveryItemType.additionalCustomerGroup2 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup2")
        }
        if !AInbDeliveryItemType.additionalCustomerGroup3.isRemoved {
            AInbDeliveryItemType.additionalCustomerGroup3 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup3")
        }
        if !AInbDeliveryItemType.additionalCustomerGroup4.isRemoved {
            AInbDeliveryItemType.additionalCustomerGroup4 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup4")
        }
        if !AInbDeliveryItemType.additionalCustomerGroup5.isRemoved {
            AInbDeliveryItemType.additionalCustomerGroup5 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup5")
        }
        if !AInbDeliveryItemType.additionalMaterialGroup1.isRemoved {
            AInbDeliveryItemType.additionalMaterialGroup1 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup1")
        }
        if !AInbDeliveryItemType.additionalMaterialGroup2.isRemoved {
            AInbDeliveryItemType.additionalMaterialGroup2 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup2")
        }
        if !AInbDeliveryItemType.additionalMaterialGroup3.isRemoved {
            AInbDeliveryItemType.additionalMaterialGroup3 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup3")
        }
        if !AInbDeliveryItemType.additionalMaterialGroup4.isRemoved {
            AInbDeliveryItemType.additionalMaterialGroup4 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup4")
        }
        if !AInbDeliveryItemType.additionalMaterialGroup5.isRemoved {
            AInbDeliveryItemType.additionalMaterialGroup5 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup5")
        }
        if !AInbDeliveryItemType.alternateProductNumber.isRemoved {
            AInbDeliveryItemType.alternateProductNumber = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AlternateProductNumber")
        }
        if !AInbDeliveryItemType.baseUnit.isRemoved {
            AInbDeliveryItemType.baseUnit = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BaseUnit")
        }
        if !AInbDeliveryItemType.batch.isRemoved {
            AInbDeliveryItemType.batch = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Batch")
        }
        if !AInbDeliveryItemType.batchBySupplier.isRemoved {
            AInbDeliveryItemType.batchBySupplier = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BatchBySupplier")
        }
        if !AInbDeliveryItemType.batchClassification.isRemoved {
            AInbDeliveryItemType.batchClassification = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BatchClassification")
        }
        if !AInbDeliveryItemType.bomExplosion.isRemoved {
            AInbDeliveryItemType.bomExplosion = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BOMExplosion")
        }
        if !AInbDeliveryItemType.businessArea.isRemoved {
            AInbDeliveryItemType.businessArea = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BusinessArea")
        }
        if !AInbDeliveryItemType.consumptionPosting.isRemoved {
            AInbDeliveryItemType.consumptionPosting = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ConsumptionPosting")
        }
        if !AInbDeliveryItemType.controllingArea.isRemoved {
            AInbDeliveryItemType.controllingArea = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ControllingArea")
        }
        if !AInbDeliveryItemType.costCenter.isRemoved {
            AInbDeliveryItemType.costCenter = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CostCenter")
        }
        if !AInbDeliveryItemType.createdByUser.isRemoved {
            AInbDeliveryItemType.createdByUser = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CreatedByUser")
        }
        if !AInbDeliveryItemType.creationDate.isRemoved {
            AInbDeliveryItemType.creationDate = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CreationDate")
        }
        if !AInbDeliveryItemType.creationTime.isRemoved {
            AInbDeliveryItemType.creationTime = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CreationTime")
        }
        if !AInbDeliveryItemType.custEngineeringChgStatus.isRemoved {
            AInbDeliveryItemType.custEngineeringChgStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CustEngineeringChgStatus")
        }
        if !AInbDeliveryItemType.deliveryDocument.isRemoved {
            AInbDeliveryItemType.deliveryDocument = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryDocument")
        }
        if !AInbDeliveryItemType.deliveryDocumentItem.isRemoved {
            AInbDeliveryItemType.deliveryDocumentItem = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryDocumentItem")
        }
        if !AInbDeliveryItemType.deliveryDocumentItemCategory.isRemoved {
            AInbDeliveryItemType.deliveryDocumentItemCategory = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryDocumentItemCategory")
        }
        if !AInbDeliveryItemType.deliveryDocumentItemText.isRemoved {
            AInbDeliveryItemType.deliveryDocumentItemText = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryDocumentItemText")
        }
        if !AInbDeliveryItemType.deliveryGroup.isRemoved {
            AInbDeliveryItemType.deliveryGroup = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryGroup")
        }
        if !AInbDeliveryItemType.deliveryQuantityUnit.isRemoved {
            AInbDeliveryItemType.deliveryQuantityUnit = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryQuantityUnit")
        }
        if !AInbDeliveryItemType.deliveryRelatedBillingStatus.isRemoved {
            AInbDeliveryItemType.deliveryRelatedBillingStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryRelatedBillingStatus")
        }
        if !AInbDeliveryItemType.deliveryToBaseQuantityDnmntr.isRemoved {
            AInbDeliveryItemType.deliveryToBaseQuantityDnmntr = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryToBaseQuantityDnmntr")
        }
        if !AInbDeliveryItemType.deliveryToBaseQuantityNmrtr.isRemoved {
            AInbDeliveryItemType.deliveryToBaseQuantityNmrtr = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryToBaseQuantityNmrtr")
        }
        if !AInbDeliveryItemType.departmentClassificationByCust.isRemoved {
            AInbDeliveryItemType.departmentClassificationByCust = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DepartmentClassificationByCust")
        }
        if !AInbDeliveryItemType.distributionChannel.isRemoved {
            AInbDeliveryItemType.distributionChannel = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DistributionChannel")
        }
        if !AInbDeliveryItemType.division.isRemoved {
            AInbDeliveryItemType.division = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Division")
        }
        if !AInbDeliveryItemType.fixedShipgProcgDurationInDays.isRemoved {
            AInbDeliveryItemType.fixedShipgProcgDurationInDays = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "FixedShipgProcgDurationInDays")
        }
        if !AInbDeliveryItemType.glAccount.isRemoved {
            AInbDeliveryItemType.glAccount = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "GLAccount")
        }
        if !AInbDeliveryItemType.goodsMovementReasonCode.isRemoved {
            AInbDeliveryItemType.goodsMovementReasonCode = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "GoodsMovementReasonCode")
        }
        if !AInbDeliveryItemType.goodsMovementStatus.isRemoved {
            AInbDeliveryItemType.goodsMovementStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "GoodsMovementStatus")
        }
        if !AInbDeliveryItemType.goodsMovementType.isRemoved {
            AInbDeliveryItemType.goodsMovementType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "GoodsMovementType")
        }
        if !AInbDeliveryItemType.higherLevelItem.isRemoved {
            AInbDeliveryItemType.higherLevelItem = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "HigherLevelItem")
        }
        if !AInbDeliveryItemType.inspectionLot.isRemoved {
            AInbDeliveryItemType.inspectionLot = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InspectionLot")
        }
        if !AInbDeliveryItemType.inspectionPartialLot.isRemoved {
            AInbDeliveryItemType.inspectionPartialLot = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InspectionPartialLot")
        }
        if !AInbDeliveryItemType.intercompanyBillingStatus.isRemoved {
            AInbDeliveryItemType.intercompanyBillingStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IntercompanyBillingStatus")
        }
        if !AInbDeliveryItemType.internationalArticleNumber.isRemoved {
            AInbDeliveryItemType.internationalArticleNumber = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InternationalArticleNumber")
        }
        if !AInbDeliveryItemType.inventorySpecialStockType.isRemoved {
            AInbDeliveryItemType.inventorySpecialStockType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InventorySpecialStockType")
        }
        if !AInbDeliveryItemType.inventoryValuationType.isRemoved {
            AInbDeliveryItemType.inventoryValuationType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InventoryValuationType")
        }
        if !AInbDeliveryItemType.isCompletelyDelivered.isRemoved {
            AInbDeliveryItemType.isCompletelyDelivered = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IsCompletelyDelivered")
        }
        if !AInbDeliveryItemType.isNotGoodsMovementsRelevant.isRemoved {
            AInbDeliveryItemType.isNotGoodsMovementsRelevant = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IsNotGoodsMovementsRelevant")
        }
        if !AInbDeliveryItemType.isSeparateValuation.isRemoved {
            AInbDeliveryItemType.isSeparateValuation = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IsSeparateValuation")
        }
        if !AInbDeliveryItemType.issgOrRcvgBatch.isRemoved {
            AInbDeliveryItemType.issgOrRcvgBatch = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgBatch")
        }
        if !AInbDeliveryItemType.issgOrRcvgMaterial.isRemoved {
            AInbDeliveryItemType.issgOrRcvgMaterial = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgMaterial")
        }
        if !AInbDeliveryItemType.issgOrRcvgSpclStockInd.isRemoved {
            AInbDeliveryItemType.issgOrRcvgSpclStockInd = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgSpclStockInd")
        }
        if !AInbDeliveryItemType.issgOrRcvgStockCategory.isRemoved {
            AInbDeliveryItemType.issgOrRcvgStockCategory = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgStockCategory")
        }
        if !AInbDeliveryItemType.issgOrRcvgValuationType.isRemoved {
            AInbDeliveryItemType.issgOrRcvgValuationType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgValuationType")
        }
        if !AInbDeliveryItemType.issuingOrReceivingPlant.isRemoved {
            AInbDeliveryItemType.issuingOrReceivingPlant = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssuingOrReceivingPlant")
        }
        if !AInbDeliveryItemType.issuingOrReceivingStorageLoc.isRemoved {
            AInbDeliveryItemType.issuingOrReceivingStorageLoc = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssuingOrReceivingStorageLoc")
        }
        if !AInbDeliveryItemType.itemBillingBlockReason.isRemoved {
            AInbDeliveryItemType.itemBillingBlockReason = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemBillingBlockReason")
        }
        if !AInbDeliveryItemType.itemBillingIncompletionStatus.isRemoved {
            AInbDeliveryItemType.itemBillingIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemBillingIncompletionStatus")
        }
        if !AInbDeliveryItemType.itemDeliveryIncompletionStatus.isRemoved {
            AInbDeliveryItemType.itemDeliveryIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemDeliveryIncompletionStatus")
        }
        if !AInbDeliveryItemType.itemGdsMvtIncompletionSts.isRemoved {
            AInbDeliveryItemType.itemGdsMvtIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemGdsMvtIncompletionSts")
        }
        if !AInbDeliveryItemType.itemGeneralIncompletionStatus.isRemoved {
            AInbDeliveryItemType.itemGeneralIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemGeneralIncompletionStatus")
        }
        if !AInbDeliveryItemType.itemGrossWeight.isRemoved {
            AInbDeliveryItemType.itemGrossWeight = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemGrossWeight")
        }
        if !AInbDeliveryItemType.itemIsBillingRelevant.isRemoved {
            AInbDeliveryItemType.itemIsBillingRelevant = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemIsBillingRelevant")
        }
        if !AInbDeliveryItemType.itemNetWeight.isRemoved {
            AInbDeliveryItemType.itemNetWeight = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemNetWeight")
        }
        if !AInbDeliveryItemType.itemPackingIncompletionStatus.isRemoved {
            AInbDeliveryItemType.itemPackingIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemPackingIncompletionStatus")
        }
        if !AInbDeliveryItemType.itemPickingIncompletionStatus.isRemoved {
            AInbDeliveryItemType.itemPickingIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemPickingIncompletionStatus")
        }
        if !AInbDeliveryItemType.itemVolume.isRemoved {
            AInbDeliveryItemType.itemVolume = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemVolume")
        }
        if !AInbDeliveryItemType.itemVolumeUnit.isRemoved {
            AInbDeliveryItemType.itemVolumeUnit = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemVolumeUnit")
        }
        if !AInbDeliveryItemType.itemWeightUnit.isRemoved {
            AInbDeliveryItemType.itemWeightUnit = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemWeightUnit")
        }
        if !AInbDeliveryItemType.lastChangeDate.isRemoved {
            AInbDeliveryItemType.lastChangeDate = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "LastChangeDate")
        }
        if !AInbDeliveryItemType.loadingGroup.isRemoved {
            AInbDeliveryItemType.loadingGroup = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "LoadingGroup")
        }
        if !AInbDeliveryItemType.manufactureDate.isRemoved {
            AInbDeliveryItemType.manufactureDate = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ManufactureDate")
        }
        if !AInbDeliveryItemType.material.isRemoved {
            AInbDeliveryItemType.material = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Material")
        }
        if !AInbDeliveryItemType.materialByCustomer.isRemoved {
            AInbDeliveryItemType.materialByCustomer = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialByCustomer")
        }
        if !AInbDeliveryItemType.materialFreightGroup.isRemoved {
            AInbDeliveryItemType.materialFreightGroup = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialFreightGroup")
        }
        if !AInbDeliveryItemType.materialGroup.isRemoved {
            AInbDeliveryItemType.materialGroup = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialGroup")
        }
        if !AInbDeliveryItemType.materialIsBatchManaged.isRemoved {
            AInbDeliveryItemType.materialIsBatchManaged = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialIsBatchManaged")
        }
        if !AInbDeliveryItemType.materialIsIntBatchManaged.isRemoved {
            AInbDeliveryItemType.materialIsIntBatchManaged = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialIsIntBatchManaged")
        }
        if !AInbDeliveryItemType.numberOfSerialNumbers.isRemoved {
            AInbDeliveryItemType.numberOfSerialNumbers = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "NumberOfSerialNumbers")
        }
        if !AInbDeliveryItemType.orderID.isRemoved {
            AInbDeliveryItemType.orderID = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OrderID")
        }
        if !AInbDeliveryItemType.orderItem.isRemoved {
            AInbDeliveryItemType.orderItem = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OrderItem")
        }
        if !AInbDeliveryItemType.originalDeliveryQuantity.isRemoved {
            AInbDeliveryItemType.originalDeliveryQuantity = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OriginalDeliveryQuantity")
        }
        if !AInbDeliveryItemType.originallyRequestedMaterial.isRemoved {
            AInbDeliveryItemType.originallyRequestedMaterial = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OriginallyRequestedMaterial")
        }
        if !AInbDeliveryItemType.overdelivTolrtdLmtRatioInPct.isRemoved {
            AInbDeliveryItemType.overdelivTolrtdLmtRatioInPct = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OverdelivTolrtdLmtRatioInPct")
        }
        if !AInbDeliveryItemType.packingStatus.isRemoved {
            AInbDeliveryItemType.packingStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PackingStatus")
        }
        if !AInbDeliveryItemType.partialDeliveryIsAllowed.isRemoved {
            AInbDeliveryItemType.partialDeliveryIsAllowed = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PartialDeliveryIsAllowed")
        }
        if !AInbDeliveryItemType.paymentGuaranteeForm.isRemoved {
            AInbDeliveryItemType.paymentGuaranteeForm = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PaymentGuaranteeForm")
        }
        if !AInbDeliveryItemType.pickingConfirmationStatus.isRemoved {
            AInbDeliveryItemType.pickingConfirmationStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PickingConfirmationStatus")
        }
        if !AInbDeliveryItemType.pickingControl.isRemoved {
            AInbDeliveryItemType.pickingControl = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PickingControl")
        }
        if !AInbDeliveryItemType.pickingStatus.isRemoved {
            AInbDeliveryItemType.pickingStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PickingStatus")
        }
        if !AInbDeliveryItemType.plant.isRemoved {
            AInbDeliveryItemType.plant = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Plant")
        }
        if !AInbDeliveryItemType.primaryPostingSwitch.isRemoved {
            AInbDeliveryItemType.primaryPostingSwitch = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PrimaryPostingSwitch")
        }
        if !AInbDeliveryItemType.productAvailabilityDate.isRemoved {
            AInbDeliveryItemType.productAvailabilityDate = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProductAvailabilityDate")
        }
        if !AInbDeliveryItemType.productAvailabilityTime.isRemoved {
            AInbDeliveryItemType.productAvailabilityTime = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProductAvailabilityTime")
        }
        if !AInbDeliveryItemType.productConfiguration.isRemoved {
            AInbDeliveryItemType.productConfiguration = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProductConfiguration")
        }
        if !AInbDeliveryItemType.productHierarchyNode.isRemoved {
            AInbDeliveryItemType.productHierarchyNode = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProductHierarchyNode")
        }
        if !AInbDeliveryItemType.profitabilitySegment.isRemoved {
            AInbDeliveryItemType.profitabilitySegment = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProfitabilitySegment")
        }
        if !AInbDeliveryItemType.profitCenter.isRemoved {
            AInbDeliveryItemType.profitCenter = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProfitCenter")
        }
        if !AInbDeliveryItemType.proofOfDeliveryRelevanceCode.isRemoved {
            AInbDeliveryItemType.proofOfDeliveryRelevanceCode = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProofOfDeliveryRelevanceCode")
        }
        if !AInbDeliveryItemType.proofOfDeliveryStatus.isRemoved {
            AInbDeliveryItemType.proofOfDeliveryStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProofOfDeliveryStatus")
        }
        if !AInbDeliveryItemType.quantityIsFixed.isRemoved {
            AInbDeliveryItemType.quantityIsFixed = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "QuantityIsFixed")
        }
        if !AInbDeliveryItemType.receivingPoint.isRemoved {
            AInbDeliveryItemType.receivingPoint = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReceivingPoint")
        }
        if !AInbDeliveryItemType.referenceDocumentLogicalSystem.isRemoved {
            AInbDeliveryItemType.referenceDocumentLogicalSystem = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReferenceDocumentLogicalSystem")
        }
        if !AInbDeliveryItemType.referenceSDDocument.isRemoved {
            AInbDeliveryItemType.referenceSDDocument = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReferenceSDDocument")
        }
        if !AInbDeliveryItemType.referenceSDDocumentCategory.isRemoved {
            AInbDeliveryItemType.referenceSDDocumentCategory = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReferenceSDDocumentCategory")
        }
        if !AInbDeliveryItemType.referenceSDDocumentItem.isRemoved {
            AInbDeliveryItemType.referenceSDDocumentItem = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReferenceSDDocumentItem")
        }
        if !AInbDeliveryItemType.retailPromotion.isRemoved {
            AInbDeliveryItemType.retailPromotion = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "RetailPromotion")
        }
        if !AInbDeliveryItemType.salesDocumentItemType.isRemoved {
            AInbDeliveryItemType.salesDocumentItemType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SalesDocumentItemType")
        }
        if !AInbDeliveryItemType.salesGroup.isRemoved {
            AInbDeliveryItemType.salesGroup = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SalesGroup")
        }
        if !AInbDeliveryItemType.salesOffice.isRemoved {
            AInbDeliveryItemType.salesOffice = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SalesOffice")
        }
        if !AInbDeliveryItemType.sdDocumentCategory.isRemoved {
            AInbDeliveryItemType.sdDocumentCategory = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SDDocumentCategory")
        }
        if !AInbDeliveryItemType.sdProcessStatus.isRemoved {
            AInbDeliveryItemType.sdProcessStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SDProcessStatus")
        }
        if !AInbDeliveryItemType.shelfLifeExpirationDate.isRemoved {
            AInbDeliveryItemType.shelfLifeExpirationDate = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ShelfLifeExpirationDate")
        }
        if !AInbDeliveryItemType.statisticsDate.isRemoved {
            AInbDeliveryItemType.statisticsDate = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StatisticsDate")
        }
        if !AInbDeliveryItemType.stockType.isRemoved {
            AInbDeliveryItemType.stockType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StockType")
        }
        if !AInbDeliveryItemType.storageBin.isRemoved {
            AInbDeliveryItemType.storageBin = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StorageBin")
        }
        if !AInbDeliveryItemType.storageLocation.isRemoved {
            AInbDeliveryItemType.storageLocation = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StorageLocation")
        }
        if !AInbDeliveryItemType.storageType.isRemoved {
            AInbDeliveryItemType.storageType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StorageType")
        }
        if !AInbDeliveryItemType.subsequentMovementType.isRemoved {
            AInbDeliveryItemType.subsequentMovementType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SubsequentMovementType")
        }
        if !AInbDeliveryItemType.transportationGroup.isRemoved {
            AInbDeliveryItemType.transportationGroup = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "TransportationGroup")
        }
        if !AInbDeliveryItemType.underdelivTolrtdLmtRatioInPct.isRemoved {
            AInbDeliveryItemType.underdelivTolrtdLmtRatioInPct = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "UnderdelivTolrtdLmtRatioInPct")
        }
        if !AInbDeliveryItemType.unlimitedOverdeliveryIsAllowed.isRemoved {
            AInbDeliveryItemType.unlimitedOverdeliveryIsAllowed = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "UnlimitedOverdeliveryIsAllowed")
        }
        if !AInbDeliveryItemType.varblShipgProcgDurationInDays.isRemoved {
            AInbDeliveryItemType.varblShipgProcgDurationInDays = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "VarblShipgProcgDurationInDays")
        }
        if !AInbDeliveryItemType.warehouse.isRemoved {
            AInbDeliveryItemType.warehouse = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Warehouse")
        }
        if !AInbDeliveryItemType.warehouseActivityStatus.isRemoved {
            AInbDeliveryItemType.warehouseActivityStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "WarehouseActivityStatus")
        }
        if !AInbDeliveryItemType.warehouseStagingArea.isRemoved {
            AInbDeliveryItemType.warehouseStagingArea = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "WarehouseStagingArea")
        }
        if !AInbDeliveryItemType.deliveryVersion.isRemoved {
            AInbDeliveryItemType.deliveryVersion = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryVersion")
        }
        if !AInbDeliveryItemType.warehouseStockCategory.isRemoved {
            AInbDeliveryItemType.warehouseStockCategory = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "WarehouseStockCategory")
        }
        if !AInbDeliveryItemType.warehouseStorageBin.isRemoved {
            AInbDeliveryItemType.warehouseStorageBin = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "WarehouseStorageBin")
        }
        if !AInbDeliveryItemType.stockSegment.isRemoved {
            AInbDeliveryItemType.stockSegment = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StockSegment")
        }
        if !AInbDeliveryItemType.requirementSegment.isRemoved {
            AInbDeliveryItemType.requirementSegment = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "RequirementSegment")
        }
        if !AInbDeliveryPartnerType.addressID.isRemoved {
            AInbDeliveryPartnerType.addressID = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "AddressID")
        }
        if !AInbDeliveryPartnerType.contactPerson.isRemoved {
            AInbDeliveryPartnerType.contactPerson = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "ContactPerson")
        }
        if !AInbDeliveryPartnerType.customer.isRemoved {
            AInbDeliveryPartnerType.customer = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "Customer")
        }
        if !AInbDeliveryPartnerType.partnerFunction.isRemoved {
            AInbDeliveryPartnerType.partnerFunction = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "PartnerFunction")
        }
        if !AInbDeliveryPartnerType.personnel.isRemoved {
            AInbDeliveryPartnerType.personnel = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "Personnel")
        }
        if !AInbDeliveryPartnerType.sdDocument.isRemoved {
            AInbDeliveryPartnerType.sdDocument = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "SDDocument")
        }
        if !AInbDeliveryPartnerType.sdDocumentItem.isRemoved {
            AInbDeliveryPartnerType.sdDocumentItem = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "SDDocumentItem")
        }
        if !AInbDeliveryPartnerType.supplier.isRemoved {
            AInbDeliveryPartnerType.supplier = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "Supplier")
        }
        if !AInbDeliverySerialNmbrType.deliveryDate.isRemoved {
            AInbDeliverySerialNmbrType.deliveryDate = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "DeliveryDate")
        }
        if !AInbDeliverySerialNmbrType.deliveryDocument.isRemoved {
            AInbDeliverySerialNmbrType.deliveryDocument = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "DeliveryDocument")
        }
        if !AInbDeliverySerialNmbrType.deliveryDocumentItem.isRemoved {
            AInbDeliverySerialNmbrType.deliveryDocumentItem = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "DeliveryDocumentItem")
        }
        if !AInbDeliverySerialNmbrType.maintenanceItemObjectList.isRemoved {
            AInbDeliverySerialNmbrType.maintenanceItemObjectList = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "MaintenanceItemObjectList")
        }
        if !AInbDeliverySerialNmbrType.sdDocumentCategory.isRemoved {
            AInbDeliverySerialNmbrType.sdDocumentCategory = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "SDDocumentCategory")
        }
        if !AMaintenanceItemObjListType.assembly.isRemoved {
            AMaintenanceItemObjListType.assembly = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "Assembly")
        }
        if !AMaintenanceItemObjListType.equipment.isRemoved {
            AMaintenanceItemObjListType.equipment = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "Equipment")
        }
        if !AMaintenanceItemObjListType.functionalLocation.isRemoved {
            AMaintenanceItemObjListType.functionalLocation = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "FunctionalLocation")
        }
        if !AMaintenanceItemObjListType.maintenanceItemObject.isRemoved {
            AMaintenanceItemObjListType.maintenanceItemObject = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "MaintenanceItemObject")
        }
        if !AMaintenanceItemObjListType.maintenanceItemObjectList.isRemoved {
            AMaintenanceItemObjListType.maintenanceItemObjectList = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "MaintenanceItemObjectList")
        }
        if !AMaintenanceItemObjListType.maintenanceNotification.isRemoved {
            AMaintenanceItemObjListType.maintenanceNotification = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "MaintenanceNotification")
        }
        if !AMaintenanceItemObjListType.maintObjectLocAcctAssgmtNmbr.isRemoved {
            AMaintenanceItemObjListType.maintObjectLocAcctAssgmtNmbr = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "MaintObjectLocAcctAssgmtNmbr")
        }
        if !AMaintenanceItemObjListType.material.isRemoved {
            AMaintenanceItemObjListType.material = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "Material")
        }
        if !AMaintenanceItemObjListType.serialNumber.isRemoved {
            AMaintenanceItemObjListType.serialNumber = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "SerialNumber")
        }
        if !AMaintenanceItemObjectType.assembly.isRemoved {
            AMaintenanceItemObjectType.assembly = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "Assembly")
        }
        if !AMaintenanceItemObjectType.equipment.isRemoved {
            AMaintenanceItemObjectType.equipment = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "Equipment")
        }
        if !AMaintenanceItemObjectType.functionalLocation.isRemoved {
            AMaintenanceItemObjectType.functionalLocation = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "FunctionalLocation")
        }
        if !AMaintenanceItemObjectType.maintenanceItemObject.isRemoved {
            AMaintenanceItemObjectType.maintenanceItemObject = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "MaintenanceItemObject")
        }
        if !AMaintenanceItemObjectType.maintenanceItemObjectList.isRemoved {
            AMaintenanceItemObjectType.maintenanceItemObjectList = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "MaintenanceItemObjectList")
        }
        if !AMaintenanceItemObjectType.maintenanceNotification.isRemoved {
            AMaintenanceItemObjectType.maintenanceNotification = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "MaintenanceNotification")
        }
        if !AMaintenanceItemObjectType.maintObjectLocAcctAssgmtNmbr.isRemoved {
            AMaintenanceItemObjectType.maintObjectLocAcctAssgmtNmbr = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "MaintObjectLocAcctAssgmtNmbr")
        }
        if !AMaintenanceItemObjectType.material.isRemoved {
            AMaintenanceItemObjectType.material = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "Material")
        }
        if !AMaintenanceItemObjectType.serialNumber.isRemoved {
            AMaintenanceItemObjectType.serialNumber = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "SerialNumber")
        }
        if !AMaterialDocumentHeaderType.materialDocumentYear.isRemoved {
            AMaterialDocumentHeaderType.materialDocumentYear = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "MaterialDocumentYear")
        }
        if !AMaterialDocumentHeaderType.materialDocument.isRemoved {
            AMaterialDocumentHeaderType.materialDocument = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "MaterialDocument")
        }
        if !AMaterialDocumentHeaderType.inventoryTransactionType.isRemoved {
            AMaterialDocumentHeaderType.inventoryTransactionType = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "InventoryTransactionType")
        }
        if !AMaterialDocumentHeaderType.documentDate.isRemoved {
            AMaterialDocumentHeaderType.documentDate = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "DocumentDate")
        }
        if !AMaterialDocumentHeaderType.postingDate.isRemoved {
            AMaterialDocumentHeaderType.postingDate = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "PostingDate")
        }
        if !AMaterialDocumentHeaderType.creationDate.isRemoved {
            AMaterialDocumentHeaderType.creationDate = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "CreationDate")
        }
        if !AMaterialDocumentHeaderType.creationTime.isRemoved {
            AMaterialDocumentHeaderType.creationTime = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "CreationTime")
        }
        if !AMaterialDocumentHeaderType.createdByUser.isRemoved {
            AMaterialDocumentHeaderType.createdByUser = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "CreatedByUser")
        }
        if !AMaterialDocumentHeaderType.materialDocumentHeaderText.isRemoved {
            AMaterialDocumentHeaderType.materialDocumentHeaderText = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "MaterialDocumentHeaderText")
        }
        if !AMaterialDocumentHeaderType.referenceDocument.isRemoved {
            AMaterialDocumentHeaderType.referenceDocument = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "ReferenceDocument")
        }
        if !AMaterialDocumentHeaderType.goodsMovementCode.isRemoved {
            AMaterialDocumentHeaderType.goodsMovementCode = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "GoodsMovementCode")
        }
        if !AMaterialDocumentItemType.materialDocumentYear.isRemoved {
            AMaterialDocumentItemType.materialDocumentYear = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentYear")
        }
        if !AMaterialDocumentItemType.materialDocument.isRemoved {
            AMaterialDocumentItemType.materialDocument = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocument")
        }
        if !AMaterialDocumentItemType.materialDocumentItem.isRemoved {
            AMaterialDocumentItemType.materialDocumentItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentItem")
        }
        if !AMaterialDocumentItemType.material.isRemoved {
            AMaterialDocumentItemType.material = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Material")
        }
        if !AMaterialDocumentItemType.plant.isRemoved {
            AMaterialDocumentItemType.plant = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Plant")
        }
        if !AMaterialDocumentItemType.storageLocation.isRemoved {
            AMaterialDocumentItemType.storageLocation = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "StorageLocation")
        }
        if !AMaterialDocumentItemType.batch.isRemoved {
            AMaterialDocumentItemType.batch = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Batch")
        }
        if !AMaterialDocumentItemType.goodsMovementType.isRemoved {
            AMaterialDocumentItemType.goodsMovementType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GoodsMovementType")
        }
        if !AMaterialDocumentItemType.inventoryStockType.isRemoved {
            AMaterialDocumentItemType.inventoryStockType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "InventoryStockType")
        }
        if !AMaterialDocumentItemType.inventoryValuationType.isRemoved {
            AMaterialDocumentItemType.inventoryValuationType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "InventoryValuationType")
        }
        if !AMaterialDocumentItemType.inventorySpecialStockType.isRemoved {
            AMaterialDocumentItemType.inventorySpecialStockType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "InventorySpecialStockType")
        }
        if !AMaterialDocumentItemType.supplier.isRemoved {
            AMaterialDocumentItemType.supplier = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Supplier")
        }
        if !AMaterialDocumentItemType.customer.isRemoved {
            AMaterialDocumentItemType.customer = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Customer")
        }
        if !AMaterialDocumentItemType.salesOrder.isRemoved {
            AMaterialDocumentItemType.salesOrder = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SalesOrder")
        }
        if !AMaterialDocumentItemType.salesOrderItem.isRemoved {
            AMaterialDocumentItemType.salesOrderItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SalesOrderItem")
        }
        if !AMaterialDocumentItemType.salesOrderScheduleLine.isRemoved {
            AMaterialDocumentItemType.salesOrderScheduleLine = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SalesOrderScheduleLine")
        }
        if !AMaterialDocumentItemType.purchaseOrder.isRemoved {
            AMaterialDocumentItemType.purchaseOrder = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "PurchaseOrder")
        }
        if !AMaterialDocumentItemType.purchaseOrderItem.isRemoved {
            AMaterialDocumentItemType.purchaseOrderItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "PurchaseOrderItem")
        }
        if !AMaterialDocumentItemType.wbsElement.isRemoved {
            AMaterialDocumentItemType.wbsElement = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "WBSElement")
        }
        if !AMaterialDocumentItemType.manufacturingOrder.isRemoved {
            AMaterialDocumentItemType.manufacturingOrder = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ManufacturingOrder")
        }
        if !AMaterialDocumentItemType.manufacturingOrderItem.isRemoved {
            AMaterialDocumentItemType.manufacturingOrderItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ManufacturingOrderItem")
        }
        if !AMaterialDocumentItemType.goodsMovementRefDocType.isRemoved {
            AMaterialDocumentItemType.goodsMovementRefDocType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GoodsMovementRefDocType")
        }
        if !AMaterialDocumentItemType.goodsMovementReasonCode.isRemoved {
            AMaterialDocumentItemType.goodsMovementReasonCode = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GoodsMovementReasonCode")
        }
        if !AMaterialDocumentItemType.accountAssignmentCategory.isRemoved {
            AMaterialDocumentItemType.accountAssignmentCategory = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "AccountAssignmentCategory")
        }
        if !AMaterialDocumentItemType.costCenter.isRemoved {
            AMaterialDocumentItemType.costCenter = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "CostCenter")
        }
        if !AMaterialDocumentItemType.controllingArea.isRemoved {
            AMaterialDocumentItemType.controllingArea = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ControllingArea")
        }
        if !AMaterialDocumentItemType.costObject.isRemoved {
            AMaterialDocumentItemType.costObject = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "CostObject")
        }
        if !AMaterialDocumentItemType.profitabilitySegment.isRemoved {
            AMaterialDocumentItemType.profitabilitySegment = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ProfitabilitySegment")
        }
        if !AMaterialDocumentItemType.profitCenter.isRemoved {
            AMaterialDocumentItemType.profitCenter = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ProfitCenter")
        }
        if !AMaterialDocumentItemType.glAccount.isRemoved {
            AMaterialDocumentItemType.glAccount = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GLAccount")
        }
        if !AMaterialDocumentItemType.functionalArea.isRemoved {
            AMaterialDocumentItemType.functionalArea = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "FunctionalArea")
        }
        if !AMaterialDocumentItemType.materialBaseUnit.isRemoved {
            AMaterialDocumentItemType.materialBaseUnit = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialBaseUnit")
        }
        if !AMaterialDocumentItemType.quantityInBaseUnit.isRemoved {
            AMaterialDocumentItemType.quantityInBaseUnit = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "QuantityInBaseUnit")
        }
    }

    private static func merge2(metadata: CSDLDocument) {
        Ignore.valueOf_any(metadata)
        if !AMaterialDocumentItemType.entryUnit.isRemoved {
            AMaterialDocumentItemType.entryUnit = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "EntryUnit")
        }
        if !AMaterialDocumentItemType.quantityInEntryUnit.isRemoved {
            AMaterialDocumentItemType.quantityInEntryUnit = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "QuantityInEntryUnit")
        }
        if !AMaterialDocumentItemType.companyCodeCurrency.isRemoved {
            AMaterialDocumentItemType.companyCodeCurrency = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "CompanyCodeCurrency")
        }
        if !AMaterialDocumentItemType.gdsMvtExtAmtInCoCodeCrcy.isRemoved {
            AMaterialDocumentItemType.gdsMvtExtAmtInCoCodeCrcy = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GdsMvtExtAmtInCoCodeCrcy")
        }
        if !AMaterialDocumentItemType.slsPrcAmtInclVATInCoCodeCrcy.isRemoved {
            AMaterialDocumentItemType.slsPrcAmtInclVATInCoCodeCrcy = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SlsPrcAmtInclVATInCoCodeCrcy")
        }
        if !AMaterialDocumentItemType.fiscalYear.isRemoved {
            AMaterialDocumentItemType.fiscalYear = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "FiscalYear")
        }
        if !AMaterialDocumentItemType.fiscalYearPeriod.isRemoved {
            AMaterialDocumentItemType.fiscalYearPeriod = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "FiscalYearPeriod")
        }
        if !AMaterialDocumentItemType.fiscalYearVariant.isRemoved {
            AMaterialDocumentItemType.fiscalYearVariant = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "FiscalYearVariant")
        }
        if !AMaterialDocumentItemType.issgOrRcvgMaterial.isRemoved {
            AMaterialDocumentItemType.issgOrRcvgMaterial = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssgOrRcvgMaterial")
        }
        if !AMaterialDocumentItemType.issgOrRcvgBatch.isRemoved {
            AMaterialDocumentItemType.issgOrRcvgBatch = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssgOrRcvgBatch")
        }
        if !AMaterialDocumentItemType.issuingOrReceivingPlant.isRemoved {
            AMaterialDocumentItemType.issuingOrReceivingPlant = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssuingOrReceivingPlant")
        }
        if !AMaterialDocumentItemType.issuingOrReceivingStorageLoc.isRemoved {
            AMaterialDocumentItemType.issuingOrReceivingStorageLoc = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssuingOrReceivingStorageLoc")
        }
        if !AMaterialDocumentItemType.issuingOrReceivingStockType.isRemoved {
            AMaterialDocumentItemType.issuingOrReceivingStockType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssuingOrReceivingStockType")
        }
        if !AMaterialDocumentItemType.issgOrRcvgSpclStockInd.isRemoved {
            AMaterialDocumentItemType.issgOrRcvgSpclStockInd = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssgOrRcvgSpclStockInd")
        }
        if !AMaterialDocumentItemType.issuingOrReceivingValType.isRemoved {
            AMaterialDocumentItemType.issuingOrReceivingValType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssuingOrReceivingValType")
        }
        if !AMaterialDocumentItemType.isCompletelyDelivered.isRemoved {
            AMaterialDocumentItemType.isCompletelyDelivered = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IsCompletelyDelivered")
        }
        if !AMaterialDocumentItemType.materialDocumentItemText.isRemoved {
            AMaterialDocumentItemType.materialDocumentItemText = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentItemText")
        }
        if !AMaterialDocumentItemType.unloadingPointName.isRemoved {
            AMaterialDocumentItemType.unloadingPointName = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "UnloadingPointName")
        }
        if !AMaterialDocumentItemType.shelfLifeExpirationDate.isRemoved {
            AMaterialDocumentItemType.shelfLifeExpirationDate = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ShelfLifeExpirationDate")
        }
        if !AMaterialDocumentItemType.manufactureDate.isRemoved {
            AMaterialDocumentItemType.manufactureDate = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ManufactureDate")
        }
        if !AMaterialDocumentItemType.reservation.isRemoved {
            AMaterialDocumentItemType.reservation = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Reservation")
        }
        if !AMaterialDocumentItemType.reservationItem.isRemoved {
            AMaterialDocumentItemType.reservationItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReservationItem")
        }
        if !AMaterialDocumentItemType.reservationIsFinallyIssued.isRemoved {
            AMaterialDocumentItemType.reservationIsFinallyIssued = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReservationIsFinallyIssued")
        }
        if !AMaterialDocumentItemType.specialStockIdfgSalesOrder.isRemoved {
            AMaterialDocumentItemType.specialStockIdfgSalesOrder = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SpecialStockIdfgSalesOrder")
        }
        if !AMaterialDocumentItemType.specialStockIdfgSalesOrderItem.isRemoved {
            AMaterialDocumentItemType.specialStockIdfgSalesOrderItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SpecialStockIdfgSalesOrderItem")
        }
        if !AMaterialDocumentItemType.specialStockIdfgWBSElement.isRemoved {
            AMaterialDocumentItemType.specialStockIdfgWBSElement = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SpecialStockIdfgWBSElement")
        }
        if !AMaterialDocumentItemType.isAutomaticallyCreated.isRemoved {
            AMaterialDocumentItemType.isAutomaticallyCreated = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IsAutomaticallyCreated")
        }
        if !AMaterialDocumentItemType.materialDocumentLine.isRemoved {
            AMaterialDocumentItemType.materialDocumentLine = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentLine")
        }
        if !AMaterialDocumentItemType.materialDocumentParentLine.isRemoved {
            AMaterialDocumentItemType.materialDocumentParentLine = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentParentLine")
        }
        if !AMaterialDocumentItemType.hierarchyNodeLevel.isRemoved {
            AMaterialDocumentItemType.hierarchyNodeLevel = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "HierarchyNodeLevel")
        }
        if !AMaterialDocumentItemType.goodsMovementIsCancelled.isRemoved {
            AMaterialDocumentItemType.goodsMovementIsCancelled = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GoodsMovementIsCancelled")
        }
        if !AMaterialDocumentItemType.reversedMaterialDocumentYear.isRemoved {
            AMaterialDocumentItemType.reversedMaterialDocumentYear = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReversedMaterialDocumentYear")
        }
        if !AMaterialDocumentItemType.reversedMaterialDocument.isRemoved {
            AMaterialDocumentItemType.reversedMaterialDocument = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReversedMaterialDocument")
        }
        if !AMaterialDocumentItemType.reversedMaterialDocumentItem.isRemoved {
            AMaterialDocumentItemType.reversedMaterialDocumentItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReversedMaterialDocumentItem")
        }
        if !AOutbDeliveryAddressType.additionalStreetPrefixName.isRemoved {
            AOutbDeliveryAddressType.additionalStreetPrefixName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "AdditionalStreetPrefixName")
        }
        if !AOutbDeliveryAddressType.additionalStreetSuffixName.isRemoved {
            AOutbDeliveryAddressType.additionalStreetSuffixName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "AdditionalStreetSuffixName")
        }
        if !AOutbDeliveryAddressType.addressID.isRemoved {
            AOutbDeliveryAddressType.addressID = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "AddressID")
        }
        if !AOutbDeliveryAddressType.addressTimeZone.isRemoved {
            AOutbDeliveryAddressType.addressTimeZone = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "AddressTimeZone")
        }
        if !AOutbDeliveryAddressType.building.isRemoved {
            AOutbDeliveryAddressType.building = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Building")
        }
        if !AOutbDeliveryAddressType.businessPartnerName1.isRemoved {
            AOutbDeliveryAddressType.businessPartnerName1 = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "BusinessPartnerName1")
        }
        if !AOutbDeliveryAddressType.businessPartnerName2.isRemoved {
            AOutbDeliveryAddressType.businessPartnerName2 = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "BusinessPartnerName2")
        }
        if !AOutbDeliveryAddressType.businessPartnerName3.isRemoved {
            AOutbDeliveryAddressType.businessPartnerName3 = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "BusinessPartnerName3")
        }
        if !AOutbDeliveryAddressType.businessPartnerName4.isRemoved {
            AOutbDeliveryAddressType.businessPartnerName4 = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "BusinessPartnerName4")
        }
        if !AOutbDeliveryAddressType.careOfName.isRemoved {
            AOutbDeliveryAddressType.careOfName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CareOfName")
        }
        if !AOutbDeliveryAddressType.cityCode.isRemoved {
            AOutbDeliveryAddressType.cityCode = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CityCode")
        }
        if !AOutbDeliveryAddressType.cityName.isRemoved {
            AOutbDeliveryAddressType.cityName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CityName")
        }
        if !AOutbDeliveryAddressType.citySearch.isRemoved {
            AOutbDeliveryAddressType.citySearch = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CitySearch")
        }
        if !AOutbDeliveryAddressType.companyPostalCode.isRemoved {
            AOutbDeliveryAddressType.companyPostalCode = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CompanyPostalCode")
        }
        if !AOutbDeliveryAddressType.correspondenceLanguage.isRemoved {
            AOutbDeliveryAddressType.correspondenceLanguage = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CorrespondenceLanguage")
        }
        if !AOutbDeliveryAddressType.country.isRemoved {
            AOutbDeliveryAddressType.country = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Country")
        }
        if !AOutbDeliveryAddressType.county.isRemoved {
            AOutbDeliveryAddressType.county = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "County")
        }
        if !AOutbDeliveryAddressType.deliveryServiceNumber.isRemoved {
            AOutbDeliveryAddressType.deliveryServiceNumber = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "DeliveryServiceNumber")
        }
        if !AOutbDeliveryAddressType.deliveryServiceTypeCode.isRemoved {
            AOutbDeliveryAddressType.deliveryServiceTypeCode = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "DeliveryServiceTypeCode")
        }
        if !AOutbDeliveryAddressType.district.isRemoved {
            AOutbDeliveryAddressType.district = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "District")
        }
        if !AOutbDeliveryAddressType.faxNumber.isRemoved {
            AOutbDeliveryAddressType.faxNumber = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "FaxNumber")
        }
        if !AOutbDeliveryAddressType.floor.isRemoved {
            AOutbDeliveryAddressType.floor = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Floor")
        }
        if !AOutbDeliveryAddressType.formOfAddress.isRemoved {
            AOutbDeliveryAddressType.formOfAddress = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "FormOfAddress")
        }
        if !AOutbDeliveryAddressType.fullName.isRemoved {
            AOutbDeliveryAddressType.fullName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "FullName")
        }
        if !AOutbDeliveryAddressType.homeCityName.isRemoved {
            AOutbDeliveryAddressType.homeCityName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "HomeCityName")
        }
        if !AOutbDeliveryAddressType.houseNumber.isRemoved {
            AOutbDeliveryAddressType.houseNumber = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "HouseNumber")
        }
        if !AOutbDeliveryAddressType.houseNumberSupplementText.isRemoved {
            AOutbDeliveryAddressType.houseNumberSupplementText = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "HouseNumberSupplementText")
        }
        if !AOutbDeliveryAddressType.nation.isRemoved {
            AOutbDeliveryAddressType.nation = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Nation")
        }
        if !AOutbDeliveryAddressType.person.isRemoved {
            AOutbDeliveryAddressType.person = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Person")
        }
        if !AOutbDeliveryAddressType.phoneNumber.isRemoved {
            AOutbDeliveryAddressType.phoneNumber = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "PhoneNumber")
        }
        if !AOutbDeliveryAddressType.poBox.isRemoved {
            AOutbDeliveryAddressType.poBox = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBox")
        }
        if !AOutbDeliveryAddressType.poBoxDeviatingCityName.isRemoved {
            AOutbDeliveryAddressType.poBoxDeviatingCityName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxDeviatingCityName")
        }
        if !AOutbDeliveryAddressType.poBoxDeviatingCountry.isRemoved {
            AOutbDeliveryAddressType.poBoxDeviatingCountry = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxDeviatingCountry")
        }
        if !AOutbDeliveryAddressType.poBoxDeviatingRegion.isRemoved {
            AOutbDeliveryAddressType.poBoxDeviatingRegion = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxDeviatingRegion")
        }
        if !AOutbDeliveryAddressType.poBoxIsWithoutNumber.isRemoved {
            AOutbDeliveryAddressType.poBoxIsWithoutNumber = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxIsWithoutNumber")
        }
        if !AOutbDeliveryAddressType.poBoxLobbyName.isRemoved {
            AOutbDeliveryAddressType.poBoxLobbyName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxLobbyName")
        }
        if !AOutbDeliveryAddressType.poBoxPostalCode.isRemoved {
            AOutbDeliveryAddressType.poBoxPostalCode = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxPostalCode")
        }
        if !AOutbDeliveryAddressType.postalCode.isRemoved {
            AOutbDeliveryAddressType.postalCode = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "PostalCode")
        }
        if !AOutbDeliveryAddressType.prfrdCommMediumType.isRemoved {
            AOutbDeliveryAddressType.prfrdCommMediumType = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "PrfrdCommMediumType")
        }
        if !AOutbDeliveryAddressType.region.isRemoved {
            AOutbDeliveryAddressType.region = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Region")
        }
        if !AOutbDeliveryAddressType.roomNumber.isRemoved {
            AOutbDeliveryAddressType.roomNumber = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "RoomNumber")
        }
        if !AOutbDeliveryAddressType.searchTerm1.isRemoved {
            AOutbDeliveryAddressType.searchTerm1 = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "SearchTerm1")
        }
        if !AOutbDeliveryAddressType.streetName.isRemoved {
            AOutbDeliveryAddressType.streetName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "StreetName")
        }
        if !AOutbDeliveryAddressType.streetPrefixName.isRemoved {
            AOutbDeliveryAddressType.streetPrefixName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "StreetPrefixName")
        }
        if !AOutbDeliveryAddressType.streetSearch.isRemoved {
            AOutbDeliveryAddressType.streetSearch = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "StreetSearch")
        }
        if !AOutbDeliveryAddressType.streetSuffixName.isRemoved {
            AOutbDeliveryAddressType.streetSuffixName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "StreetSuffixName")
        }
        if !AOutbDeliveryAddressType.taxJurisdiction.isRemoved {
            AOutbDeliveryAddressType.taxJurisdiction = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "TaxJurisdiction")
        }
        if !AOutbDeliveryAddressType.transportZone.isRemoved {
            AOutbDeliveryAddressType.transportZone = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "TransportZone")
        }
        if !AOutbDeliveryDocFlowType.deliveryversion.isRemoved {
            AOutbDeliveryDocFlowType.deliveryversion = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "Deliveryversion")
        }
        if !AOutbDeliveryDocFlowType.precedingDocument.isRemoved {
            AOutbDeliveryDocFlowType.precedingDocument = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "PrecedingDocument")
        }
        if !AOutbDeliveryDocFlowType.precedingDocumentCategory.isRemoved {
            AOutbDeliveryDocFlowType.precedingDocumentCategory = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "PrecedingDocumentCategory")
        }
        if !AOutbDeliveryDocFlowType.precedingDocumentItem.isRemoved {
            AOutbDeliveryDocFlowType.precedingDocumentItem = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "PrecedingDocumentItem")
        }
        if !AOutbDeliveryDocFlowType.subsequentdocument.isRemoved {
            AOutbDeliveryDocFlowType.subsequentdocument = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "Subsequentdocument")
        }
        if !AOutbDeliveryDocFlowType.quantityInBaseUnit.isRemoved {
            AOutbDeliveryDocFlowType.quantityInBaseUnit = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "QuantityInBaseUnit")
        }
        if !AOutbDeliveryDocFlowType.subsequentDocumentItem.isRemoved {
            AOutbDeliveryDocFlowType.subsequentDocumentItem = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "SubsequentDocumentItem")
        }
        if !AOutbDeliveryDocFlowType.sdFulfillmentCalculationRule.isRemoved {
            AOutbDeliveryDocFlowType.sdFulfillmentCalculationRule = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "SDFulfillmentCalculationRule")
        }
        if !AOutbDeliveryDocFlowType.subsequentDocumentCategory.isRemoved {
            AOutbDeliveryDocFlowType.subsequentDocumentCategory = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "SubsequentDocumentCategory")
        }
        if !AOutbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd.isRemoved {
            AOutbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "TransferOrderInWrhsMgmtIsConfd")
        }
        if !AOutbDeliveryHeaderType.actualDeliveryRoute.isRemoved {
            AOutbDeliveryHeaderType.actualDeliveryRoute = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ActualDeliveryRoute")
        }
        if !AOutbDeliveryHeaderType.shippinglocationtimezone.isRemoved {
            AOutbDeliveryHeaderType.shippinglocationtimezone = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "Shippinglocationtimezone")
        }
        if !AOutbDeliveryHeaderType.receivinglocationtimezone.isRemoved {
            AOutbDeliveryHeaderType.receivinglocationtimezone = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "Receivinglocationtimezone")
        }
        if !AOutbDeliveryHeaderType.actualGoodsMovementDate.isRemoved {
            AOutbDeliveryHeaderType.actualGoodsMovementDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ActualGoodsMovementDate")
        }
        if !AOutbDeliveryHeaderType.actualGoodsMovementTime.isRemoved {
            AOutbDeliveryHeaderType.actualGoodsMovementTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ActualGoodsMovementTime")
        }
        if !AOutbDeliveryHeaderType.billingDocumentDate.isRemoved {
            AOutbDeliveryHeaderType.billingDocumentDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "BillingDocumentDate")
        }
        if !AOutbDeliveryHeaderType.billOfLading.isRemoved {
            AOutbDeliveryHeaderType.billOfLading = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "BillOfLading")
        }
        if !AOutbDeliveryHeaderType.completeDeliveryIsDefined.isRemoved {
            AOutbDeliveryHeaderType.completeDeliveryIsDefined = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CompleteDeliveryIsDefined")
        }
        if !AOutbDeliveryHeaderType.confirmationTime.isRemoved {
            AOutbDeliveryHeaderType.confirmationTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ConfirmationTime")
        }
        if !AOutbDeliveryHeaderType.createdByUser.isRemoved {
            AOutbDeliveryHeaderType.createdByUser = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CreatedByUser")
        }
        if !AOutbDeliveryHeaderType.creationDate.isRemoved {
            AOutbDeliveryHeaderType.creationDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CreationDate")
        }
        if !AOutbDeliveryHeaderType.creationTime.isRemoved {
            AOutbDeliveryHeaderType.creationTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CreationTime")
        }
        if !AOutbDeliveryHeaderType.customerGroup.isRemoved {
            AOutbDeliveryHeaderType.customerGroup = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CustomerGroup")
        }
        if !AOutbDeliveryHeaderType.deliveryBlockReason.isRemoved {
            AOutbDeliveryHeaderType.deliveryBlockReason = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryBlockReason")
        }
        if !AOutbDeliveryHeaderType.deliveryDate.isRemoved {
            AOutbDeliveryHeaderType.deliveryDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryDate")
        }
        if !AOutbDeliveryHeaderType.deliveryDocument.isRemoved {
            AOutbDeliveryHeaderType.deliveryDocument = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryDocument")
        }
        if !AOutbDeliveryHeaderType.deliveryDocumentBySupplier.isRemoved {
            AOutbDeliveryHeaderType.deliveryDocumentBySupplier = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryDocumentBySupplier")
        }
        if !AOutbDeliveryHeaderType.deliveryDocumentType.isRemoved {
            AOutbDeliveryHeaderType.deliveryDocumentType = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryDocumentType")
        }
        if !AOutbDeliveryHeaderType.deliveryIsInPlant.isRemoved {
            AOutbDeliveryHeaderType.deliveryIsInPlant = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryIsInPlant")
        }
        if !AOutbDeliveryHeaderType.deliveryPriority.isRemoved {
            AOutbDeliveryHeaderType.deliveryPriority = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryPriority")
        }
        if !AOutbDeliveryHeaderType.deliveryTime.isRemoved {
            AOutbDeliveryHeaderType.deliveryTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryTime")
        }
        if !AOutbDeliveryHeaderType.deliveryVersion.isRemoved {
            AOutbDeliveryHeaderType.deliveryVersion = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryVersion")
        }
        if !AOutbDeliveryHeaderType.depreciationPercentage.isRemoved {
            AOutbDeliveryHeaderType.depreciationPercentage = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DepreciationPercentage")
        }
        if !AOutbDeliveryHeaderType.distrStatusByDecentralizedWrhs.isRemoved {
            AOutbDeliveryHeaderType.distrStatusByDecentralizedWrhs = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DistrStatusByDecentralizedWrhs")
        }
        if !AOutbDeliveryHeaderType.documentDate.isRemoved {
            AOutbDeliveryHeaderType.documentDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DocumentDate")
        }
        if !AOutbDeliveryHeaderType.externalIdentificationType.isRemoved {
            AOutbDeliveryHeaderType.externalIdentificationType = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ExternalIdentificationType")
        }
        if !AOutbDeliveryHeaderType.externalTransportSystem.isRemoved {
            AOutbDeliveryHeaderType.externalTransportSystem = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ExternalTransportSystem")
        }
        if !AOutbDeliveryHeaderType.factoryCalendarByCustomer.isRemoved {
            AOutbDeliveryHeaderType.factoryCalendarByCustomer = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "FactoryCalendarByCustomer")
        }
        if !AOutbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber.isRemoved {
            AOutbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "GoodsIssueOrReceiptSlipNumber")
        }
        if !AOutbDeliveryHeaderType.goodsIssueTime.isRemoved {
            AOutbDeliveryHeaderType.goodsIssueTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "GoodsIssueTime")
        }
        if !AOutbDeliveryHeaderType.handlingUnitInStock.isRemoved {
            AOutbDeliveryHeaderType.handlingUnitInStock = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HandlingUnitInStock")
        }
        if !AOutbDeliveryHeaderType.hdrGeneralIncompletionStatus.isRemoved {
            AOutbDeliveryHeaderType.hdrGeneralIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HdrGeneralIncompletionStatus")
        }
        if !AOutbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus.isRemoved {
            AOutbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HdrGoodsMvtIncompletionStatus")
        }
        if !AOutbDeliveryHeaderType.headerBillgIncompletionStatus.isRemoved {
            AOutbDeliveryHeaderType.headerBillgIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderBillgIncompletionStatus")
        }
        if !AOutbDeliveryHeaderType.headerBillingBlockReason.isRemoved {
            AOutbDeliveryHeaderType.headerBillingBlockReason = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderBillingBlockReason")
        }
        if !AOutbDeliveryHeaderType.headerDelivIncompletionStatus.isRemoved {
            AOutbDeliveryHeaderType.headerDelivIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderDelivIncompletionStatus")
        }
        if !AOutbDeliveryHeaderType.headerGrossWeight.isRemoved {
            AOutbDeliveryHeaderType.headerGrossWeight = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderGrossWeight")
        }
        if !AOutbDeliveryHeaderType.headerNetWeight.isRemoved {
            AOutbDeliveryHeaderType.headerNetWeight = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderNetWeight")
        }
        if !AOutbDeliveryHeaderType.headerPackingIncompletionSts.isRemoved {
            AOutbDeliveryHeaderType.headerPackingIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderPackingIncompletionSts")
        }
        if !AOutbDeliveryHeaderType.headerPickgIncompletionStatus.isRemoved {
            AOutbDeliveryHeaderType.headerPickgIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderPickgIncompletionStatus")
        }
        if !AOutbDeliveryHeaderType.headerVolume.isRemoved {
            AOutbDeliveryHeaderType.headerVolume = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderVolume")
        }
        if !AOutbDeliveryHeaderType.headerVolumeUnit.isRemoved {
            AOutbDeliveryHeaderType.headerVolumeUnit = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderVolumeUnit")
        }
        if !AOutbDeliveryHeaderType.headerWeightUnit.isRemoved {
            AOutbDeliveryHeaderType.headerWeightUnit = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderWeightUnit")
        }
        if !AOutbDeliveryHeaderType.incotermsClassification.isRemoved {
            AOutbDeliveryHeaderType.incotermsClassification = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IncotermsClassification")
        }
        if !AOutbDeliveryHeaderType.incotermsTransferLocation.isRemoved {
            AOutbDeliveryHeaderType.incotermsTransferLocation = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IncotermsTransferLocation")
        }
        if !AOutbDeliveryHeaderType.intercompanyBillingDate.isRemoved {
            AOutbDeliveryHeaderType.intercompanyBillingDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IntercompanyBillingDate")
        }
        if !AOutbDeliveryHeaderType.internalFinancialDocument.isRemoved {
            AOutbDeliveryHeaderType.internalFinancialDocument = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "InternalFinancialDocument")
        }
        if !AOutbDeliveryHeaderType.isDeliveryForSingleWarehouse.isRemoved {
            AOutbDeliveryHeaderType.isDeliveryForSingleWarehouse = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IsDeliveryForSingleWarehouse")
        }
        if !AOutbDeliveryHeaderType.isExportDelivery.isRemoved {
            AOutbDeliveryHeaderType.isExportDelivery = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IsExportDelivery")
        }
        if !AOutbDeliveryHeaderType.lastChangeDate.isRemoved {
            AOutbDeliveryHeaderType.lastChangeDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LastChangeDate")
        }
        if !AOutbDeliveryHeaderType.lastChangedByUser.isRemoved {
            AOutbDeliveryHeaderType.lastChangedByUser = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LastChangedByUser")
        }
        if !AOutbDeliveryHeaderType.loadingDate.isRemoved {
            AOutbDeliveryHeaderType.loadingDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LoadingDate")
        }
        if !AOutbDeliveryHeaderType.loadingPoint.isRemoved {
            AOutbDeliveryHeaderType.loadingPoint = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LoadingPoint")
        }
        if !AOutbDeliveryHeaderType.loadingTime.isRemoved {
            AOutbDeliveryHeaderType.loadingTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LoadingTime")
        }
        if !AOutbDeliveryHeaderType.meansOfTransport.isRemoved {
            AOutbDeliveryHeaderType.meansOfTransport = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "MeansOfTransport")
        }
        if !AOutbDeliveryHeaderType.meansOfTransportRefMaterial.isRemoved {
            AOutbDeliveryHeaderType.meansOfTransportRefMaterial = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "MeansOfTransportRefMaterial")
        }
        if !AOutbDeliveryHeaderType.meansOfTransportType.isRemoved {
            AOutbDeliveryHeaderType.meansOfTransportType = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "MeansOfTransportType")
        }
        if !AOutbDeliveryHeaderType.orderCombinationIsAllowed.isRemoved {
            AOutbDeliveryHeaderType.orderCombinationIsAllowed = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OrderCombinationIsAllowed")
        }
        if !AOutbDeliveryHeaderType.orderID.isRemoved {
            AOutbDeliveryHeaderType.orderID = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OrderID")
        }
        if !AOutbDeliveryHeaderType.overallDelivConfStatus.isRemoved {
            AOutbDeliveryHeaderType.overallDelivConfStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallDelivConfStatus")
        }
        if !AOutbDeliveryHeaderType.overallDelivReltdBillgStatus.isRemoved {
            AOutbDeliveryHeaderType.overallDelivReltdBillgStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallDelivReltdBillgStatus")
        }
        if !AOutbDeliveryHeaderType.overallGoodsMovementStatus.isRemoved {
            AOutbDeliveryHeaderType.overallGoodsMovementStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallGoodsMovementStatus")
        }
        if !AOutbDeliveryHeaderType.overallIntcoBillingStatus.isRemoved {
            AOutbDeliveryHeaderType.overallIntcoBillingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallIntcoBillingStatus")
        }
        if !AOutbDeliveryHeaderType.overallPackingStatus.isRemoved {
            AOutbDeliveryHeaderType.overallPackingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallPackingStatus")
        }
        if !AOutbDeliveryHeaderType.overallPickingConfStatus.isRemoved {
            AOutbDeliveryHeaderType.overallPickingConfStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallPickingConfStatus")
        }
        if !AOutbDeliveryHeaderType.overallPickingStatus.isRemoved {
            AOutbDeliveryHeaderType.overallPickingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallPickingStatus")
        }
        if !AOutbDeliveryHeaderType.overallProofOfDeliveryStatus.isRemoved {
            AOutbDeliveryHeaderType.overallProofOfDeliveryStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallProofOfDeliveryStatus")
        }
        if !AOutbDeliveryHeaderType.overallSDProcessStatus.isRemoved {
            AOutbDeliveryHeaderType.overallSDProcessStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallSDProcessStatus")
        }
        if !AOutbDeliveryHeaderType.overallWarehouseActivityStatus.isRemoved {
            AOutbDeliveryHeaderType.overallWarehouseActivityStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallWarehouseActivityStatus")
        }
        if !AOutbDeliveryHeaderType.ovrlItmDelivIncompletionSts.isRemoved {
            AOutbDeliveryHeaderType.ovrlItmDelivIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmDelivIncompletionSts")
        }
        if !AOutbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts.isRemoved {
            AOutbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmGdsMvtIncompletionSts")
        }
        if !AOutbDeliveryHeaderType.ovrlItmGeneralIncompletionSts.isRemoved {
            AOutbDeliveryHeaderType.ovrlItmGeneralIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmGeneralIncompletionSts")
        }
        if !AOutbDeliveryHeaderType.ovrlItmPackingIncompletionSts.isRemoved {
            AOutbDeliveryHeaderType.ovrlItmPackingIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmPackingIncompletionSts")
        }
        if !AOutbDeliveryHeaderType.ovrlItmPickingIncompletionSts.isRemoved {
            AOutbDeliveryHeaderType.ovrlItmPickingIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmPickingIncompletionSts")
        }
        if !AOutbDeliveryHeaderType.paymentGuaranteeProcedure.isRemoved {
            AOutbDeliveryHeaderType.paymentGuaranteeProcedure = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PaymentGuaranteeProcedure")
        }
        if !AOutbDeliveryHeaderType.pickedItemsLocation.isRemoved {
            AOutbDeliveryHeaderType.pickedItemsLocation = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PickedItemsLocation")
        }
        if !AOutbDeliveryHeaderType.pickingDate.isRemoved {
            AOutbDeliveryHeaderType.pickingDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PickingDate")
        }
        if !AOutbDeliveryHeaderType.pickingTime.isRemoved {
            AOutbDeliveryHeaderType.pickingTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PickingTime")
        }
        if !AOutbDeliveryHeaderType.plannedGoodsIssueDate.isRemoved {
            AOutbDeliveryHeaderType.plannedGoodsIssueDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PlannedGoodsIssueDate")
        }
        if !AOutbDeliveryHeaderType.proofOfDeliveryDate.isRemoved {
            AOutbDeliveryHeaderType.proofOfDeliveryDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ProofOfDeliveryDate")
        }
        if !AOutbDeliveryHeaderType.proposedDeliveryRoute.isRemoved {
            AOutbDeliveryHeaderType.proposedDeliveryRoute = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ProposedDeliveryRoute")
        }
        if !AOutbDeliveryHeaderType.receivingPlant.isRemoved {
            AOutbDeliveryHeaderType.receivingPlant = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ReceivingPlant")
        }
        if !AOutbDeliveryHeaderType.routeSchedule.isRemoved {
            AOutbDeliveryHeaderType.routeSchedule = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "RouteSchedule")
        }
        if !AOutbDeliveryHeaderType.salesDistrict.isRemoved {
            AOutbDeliveryHeaderType.salesDistrict = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SalesDistrict")
        }
        if !AOutbDeliveryHeaderType.salesOffice.isRemoved {
            AOutbDeliveryHeaderType.salesOffice = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SalesOffice")
        }
        if !AOutbDeliveryHeaderType.salesOrganization.isRemoved {
            AOutbDeliveryHeaderType.salesOrganization = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SalesOrganization")
        }
        if !AOutbDeliveryHeaderType.sdDocumentCategory.isRemoved {
            AOutbDeliveryHeaderType.sdDocumentCategory = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SDDocumentCategory")
        }
        if !AOutbDeliveryHeaderType.shipmentBlockReason.isRemoved {
            AOutbDeliveryHeaderType.shipmentBlockReason = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShipmentBlockReason")
        }
        if !AOutbDeliveryHeaderType.shippingCondition.isRemoved {
            AOutbDeliveryHeaderType.shippingCondition = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShippingCondition")
        }
        if !AOutbDeliveryHeaderType.shippingPoint.isRemoved {
            AOutbDeliveryHeaderType.shippingPoint = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShippingPoint")
        }
        if !AOutbDeliveryHeaderType.shippingType.isRemoved {
            AOutbDeliveryHeaderType.shippingType = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShippingType")
        }
        if !AOutbDeliveryHeaderType.shipToParty.isRemoved {
            AOutbDeliveryHeaderType.shipToParty = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShipToParty")
        }
        if !AOutbDeliveryHeaderType.soldToParty.isRemoved {
            AOutbDeliveryHeaderType.soldToParty = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SoldToParty")
        }
        if !AOutbDeliveryHeaderType.specialProcessingCode.isRemoved {
            AOutbDeliveryHeaderType.specialProcessingCode = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SpecialProcessingCode")
        }
        if !AOutbDeliveryHeaderType.statisticsCurrency.isRemoved {
            AOutbDeliveryHeaderType.statisticsCurrency = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "StatisticsCurrency")
        }
        if !AOutbDeliveryHeaderType.supplier.isRemoved {
            AOutbDeliveryHeaderType.supplier = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "Supplier")
        }
        if !AOutbDeliveryHeaderType.totalBlockStatus.isRemoved {
            AOutbDeliveryHeaderType.totalBlockStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TotalBlockStatus")
        }
        if !AOutbDeliveryHeaderType.totalCreditCheckStatus.isRemoved {
            AOutbDeliveryHeaderType.totalCreditCheckStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TotalCreditCheckStatus")
        }
        if !AOutbDeliveryHeaderType.totalNumberOfPackage.isRemoved {
            AOutbDeliveryHeaderType.totalNumberOfPackage = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TotalNumberOfPackage")
        }
        if !AOutbDeliveryHeaderType.transactionCurrency.isRemoved {
            AOutbDeliveryHeaderType.transactionCurrency = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransactionCurrency")
        }
        if !AOutbDeliveryHeaderType.transportationGroup.isRemoved {
            AOutbDeliveryHeaderType.transportationGroup = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransportationGroup")
        }
        if !AOutbDeliveryHeaderType.transportationPlanningDate.isRemoved {
            AOutbDeliveryHeaderType.transportationPlanningDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransportationPlanningDate")
        }
        if !AOutbDeliveryHeaderType.transportationPlanningStatus.isRemoved {
            AOutbDeliveryHeaderType.transportationPlanningStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransportationPlanningStatus")
        }
        if !AOutbDeliveryHeaderType.transportationPlanningTime.isRemoved {
            AOutbDeliveryHeaderType.transportationPlanningTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransportationPlanningTime")
        }
        if !AOutbDeliveryHeaderType.unloadingPointName.isRemoved {
            AOutbDeliveryHeaderType.unloadingPointName = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "UnloadingPointName")
        }
        if !AOutbDeliveryHeaderType.warehouse.isRemoved {
            AOutbDeliveryHeaderType.warehouse = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "Warehouse")
        }
        if !AOutbDeliveryHeaderType.warehouseGate.isRemoved {
            AOutbDeliveryHeaderType.warehouseGate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "WarehouseGate")
        }
        if !AOutbDeliveryHeaderType.warehouseStagingArea.isRemoved {
            AOutbDeliveryHeaderType.warehouseStagingArea = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "WarehouseStagingArea")
        }
        if !AOutbDeliveryItemType.actualDeliveredQtyInBaseUnit.isRemoved {
            AOutbDeliveryItemType.actualDeliveredQtyInBaseUnit = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ActualDeliveredQtyInBaseUnit")
        }
        if !AOutbDeliveryItemType.deliveryVersion.isRemoved {
            AOutbDeliveryItemType.deliveryVersion = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryVersion")
        }
        if !AOutbDeliveryItemType.actualDeliveryQuantity.isRemoved {
            AOutbDeliveryItemType.actualDeliveryQuantity = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ActualDeliveryQuantity")
        }
        if !AOutbDeliveryItemType.additionalCustomerGroup1.isRemoved {
            AOutbDeliveryItemType.additionalCustomerGroup1 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup1")
        }
        if !AOutbDeliveryItemType.additionalCustomerGroup2.isRemoved {
            AOutbDeliveryItemType.additionalCustomerGroup2 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup2")
        }
        if !AOutbDeliveryItemType.additionalCustomerGroup3.isRemoved {
            AOutbDeliveryItemType.additionalCustomerGroup3 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup3")
        }
        if !AOutbDeliveryItemType.additionalCustomerGroup4.isRemoved {
            AOutbDeliveryItemType.additionalCustomerGroup4 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup4")
        }
        if !AOutbDeliveryItemType.additionalCustomerGroup5.isRemoved {
            AOutbDeliveryItemType.additionalCustomerGroup5 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup5")
        }
        if !AOutbDeliveryItemType.additionalMaterialGroup1.isRemoved {
            AOutbDeliveryItemType.additionalMaterialGroup1 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup1")
        }
        if !AOutbDeliveryItemType.additionalMaterialGroup2.isRemoved {
            AOutbDeliveryItemType.additionalMaterialGroup2 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup2")
        }
        if !AOutbDeliveryItemType.additionalMaterialGroup3.isRemoved {
            AOutbDeliveryItemType.additionalMaterialGroup3 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup3")
        }
        if !AOutbDeliveryItemType.additionalMaterialGroup4.isRemoved {
            AOutbDeliveryItemType.additionalMaterialGroup4 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup4")
        }
        if !AOutbDeliveryItemType.additionalMaterialGroup5.isRemoved {
            AOutbDeliveryItemType.additionalMaterialGroup5 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup5")
        }
        if !AOutbDeliveryItemType.alternateProductNumber.isRemoved {
            AOutbDeliveryItemType.alternateProductNumber = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AlternateProductNumber")
        }
        if !AOutbDeliveryItemType.baseUnit.isRemoved {
            AOutbDeliveryItemType.baseUnit = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BaseUnit")
        }
        if !AOutbDeliveryItemType.batch.isRemoved {
            AOutbDeliveryItemType.batch = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Batch")
        }
        if !AOutbDeliveryItemType.batchBySupplier.isRemoved {
            AOutbDeliveryItemType.batchBySupplier = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BatchBySupplier")
        }
        if !AOutbDeliveryItemType.batchClassification.isRemoved {
            AOutbDeliveryItemType.batchClassification = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BatchClassification")
        }
        if !AOutbDeliveryItemType.bomExplosion.isRemoved {
            AOutbDeliveryItemType.bomExplosion = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BOMExplosion")
        }
        if !AOutbDeliveryItemType.businessArea.isRemoved {
            AOutbDeliveryItemType.businessArea = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BusinessArea")
        }
        if !AOutbDeliveryItemType.consumptionPosting.isRemoved {
            AOutbDeliveryItemType.consumptionPosting = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ConsumptionPosting")
        }
        if !AOutbDeliveryItemType.controllingArea.isRemoved {
            AOutbDeliveryItemType.controllingArea = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ControllingArea")
        }
        if !AOutbDeliveryItemType.costCenter.isRemoved {
            AOutbDeliveryItemType.costCenter = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CostCenter")
        }
        if !AOutbDeliveryItemType.createdByUser.isRemoved {
            AOutbDeliveryItemType.createdByUser = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CreatedByUser")
        }
        if !AOutbDeliveryItemType.creationDate.isRemoved {
            AOutbDeliveryItemType.creationDate = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CreationDate")
        }
        if !AOutbDeliveryItemType.creationTime.isRemoved {
            AOutbDeliveryItemType.creationTime = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CreationTime")
        }
        if !AOutbDeliveryItemType.custEngineeringChgStatus.isRemoved {
            AOutbDeliveryItemType.custEngineeringChgStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CustEngineeringChgStatus")
        }
        if !AOutbDeliveryItemType.deliveryDocument.isRemoved {
            AOutbDeliveryItemType.deliveryDocument = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryDocument")
        }
        if !AOutbDeliveryItemType.deliveryDocumentItem.isRemoved {
            AOutbDeliveryItemType.deliveryDocumentItem = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryDocumentItem")
        }
        if !AOutbDeliveryItemType.deliveryDocumentItemCategory.isRemoved {
            AOutbDeliveryItemType.deliveryDocumentItemCategory = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryDocumentItemCategory")
        }
        if !AOutbDeliveryItemType.deliveryDocumentItemText.isRemoved {
            AOutbDeliveryItemType.deliveryDocumentItemText = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryDocumentItemText")
        }
        if !AOutbDeliveryItemType.deliveryGroup.isRemoved {
            AOutbDeliveryItemType.deliveryGroup = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryGroup")
        }
        if !AOutbDeliveryItemType.deliveryQuantityUnit.isRemoved {
            AOutbDeliveryItemType.deliveryQuantityUnit = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryQuantityUnit")
        }
        if !AOutbDeliveryItemType.deliveryRelatedBillingStatus.isRemoved {
            AOutbDeliveryItemType.deliveryRelatedBillingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryRelatedBillingStatus")
        }
        if !AOutbDeliveryItemType.deliveryToBaseQuantityDnmntr.isRemoved {
            AOutbDeliveryItemType.deliveryToBaseQuantityDnmntr = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryToBaseQuantityDnmntr")
        }
        if !AOutbDeliveryItemType.deliveryToBaseQuantityNmrtr.isRemoved {
            AOutbDeliveryItemType.deliveryToBaseQuantityNmrtr = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryToBaseQuantityNmrtr")
        }
        if !AOutbDeliveryItemType.departmentClassificationByCust.isRemoved {
            AOutbDeliveryItemType.departmentClassificationByCust = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DepartmentClassificationByCust")
        }
        if !AOutbDeliveryItemType.distributionChannel.isRemoved {
            AOutbDeliveryItemType.distributionChannel = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DistributionChannel")
        }
        if !AOutbDeliveryItemType.division.isRemoved {
            AOutbDeliveryItemType.division = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Division")
        }
        if !AOutbDeliveryItemType.fixedShipgProcgDurationInDays.isRemoved {
            AOutbDeliveryItemType.fixedShipgProcgDurationInDays = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "FixedShipgProcgDurationInDays")
        }
        if !AOutbDeliveryItemType.glAccount.isRemoved {
            AOutbDeliveryItemType.glAccount = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "GLAccount")
        }
        if !AOutbDeliveryItemType.goodsMovementReasonCode.isRemoved {
            AOutbDeliveryItemType.goodsMovementReasonCode = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "GoodsMovementReasonCode")
        }
        if !AOutbDeliveryItemType.goodsMovementStatus.isRemoved {
            AOutbDeliveryItemType.goodsMovementStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "GoodsMovementStatus")
        }
        if !AOutbDeliveryItemType.goodsMovementType.isRemoved {
            AOutbDeliveryItemType.goodsMovementType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "GoodsMovementType")
        }
        if !AOutbDeliveryItemType.higherLvlItmOfBatSpltItm.isRemoved {
            AOutbDeliveryItemType.higherLvlItmOfBatSpltItm = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "HigherLvlItmOfBatSpltItm")
        }
        if !AOutbDeliveryItemType.higherLevelItem.isRemoved {
            AOutbDeliveryItemType.higherLevelItem = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "HigherLevelItem")
        }
        if !AOutbDeliveryItemType.inspectionLot.isRemoved {
            AOutbDeliveryItemType.inspectionLot = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InspectionLot")
        }
        if !AOutbDeliveryItemType.inspectionPartialLot.isRemoved {
            AOutbDeliveryItemType.inspectionPartialLot = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InspectionPartialLot")
        }
        if !AOutbDeliveryItemType.intercompanyBillingStatus.isRemoved {
            AOutbDeliveryItemType.intercompanyBillingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IntercompanyBillingStatus")
        }
        if !AOutbDeliveryItemType.internationalArticleNumber.isRemoved {
            AOutbDeliveryItemType.internationalArticleNumber = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InternationalArticleNumber")
        }
        if !AOutbDeliveryItemType.inventorySpecialStockType.isRemoved {
            AOutbDeliveryItemType.inventorySpecialStockType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InventorySpecialStockType")
        }
        if !AOutbDeliveryItemType.inventoryValuationType.isRemoved {
            AOutbDeliveryItemType.inventoryValuationType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InventoryValuationType")
        }
        if !AOutbDeliveryItemType.isCompletelyDelivered.isRemoved {
            AOutbDeliveryItemType.isCompletelyDelivered = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IsCompletelyDelivered")
        }
        if !AOutbDeliveryItemType.isNotGoodsMovementsRelevant.isRemoved {
            AOutbDeliveryItemType.isNotGoodsMovementsRelevant = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IsNotGoodsMovementsRelevant")
        }
        if !AOutbDeliveryItemType.isSeparateValuation.isRemoved {
            AOutbDeliveryItemType.isSeparateValuation = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IsSeparateValuation")
        }
        if !AOutbDeliveryItemType.issgOrRcvgBatch.isRemoved {
            AOutbDeliveryItemType.issgOrRcvgBatch = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgBatch")
        }
        if !AOutbDeliveryItemType.issgOrRcvgMaterial.isRemoved {
            AOutbDeliveryItemType.issgOrRcvgMaterial = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgMaterial")
        }
        if !AOutbDeliveryItemType.issgOrRcvgSpclStockInd.isRemoved {
            AOutbDeliveryItemType.issgOrRcvgSpclStockInd = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgSpclStockInd")
        }
        if !AOutbDeliveryItemType.issgOrRcvgStockCategory.isRemoved {
            AOutbDeliveryItemType.issgOrRcvgStockCategory = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgStockCategory")
        }
        if !AOutbDeliveryItemType.issgOrRcvgValuationType.isRemoved {
            AOutbDeliveryItemType.issgOrRcvgValuationType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgValuationType")
        }
        if !AOutbDeliveryItemType.issuingOrReceivingPlant.isRemoved {
            AOutbDeliveryItemType.issuingOrReceivingPlant = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssuingOrReceivingPlant")
        }
        if !AOutbDeliveryItemType.issuingOrReceivingStorageLoc.isRemoved {
            AOutbDeliveryItemType.issuingOrReceivingStorageLoc = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssuingOrReceivingStorageLoc")
        }
        if !AOutbDeliveryItemType.itemBillingBlockReason.isRemoved {
            AOutbDeliveryItemType.itemBillingBlockReason = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemBillingBlockReason")
        }
        if !AOutbDeliveryItemType.itemBillingIncompletionStatus.isRemoved {
            AOutbDeliveryItemType.itemBillingIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemBillingIncompletionStatus")
        }
        if !AOutbDeliveryItemType.itemDeliveryIncompletionStatus.isRemoved {
            AOutbDeliveryItemType.itemDeliveryIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemDeliveryIncompletionStatus")
        }
        if !AOutbDeliveryItemType.itemGdsMvtIncompletionSts.isRemoved {
            AOutbDeliveryItemType.itemGdsMvtIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemGdsMvtIncompletionSts")
        }
        if !AOutbDeliveryItemType.itemGeneralIncompletionStatus.isRemoved {
            AOutbDeliveryItemType.itemGeneralIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemGeneralIncompletionStatus")
        }
        if !AOutbDeliveryItemType.itemGrossWeight.isRemoved {
            AOutbDeliveryItemType.itemGrossWeight = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemGrossWeight")
        }
        if !AOutbDeliveryItemType.itemIsBillingRelevant.isRemoved {
            AOutbDeliveryItemType.itemIsBillingRelevant = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemIsBillingRelevant")
        }
        if !AOutbDeliveryItemType.itemNetWeight.isRemoved {
            AOutbDeliveryItemType.itemNetWeight = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemNetWeight")
        }
        if !AOutbDeliveryItemType.itemPackingIncompletionStatus.isRemoved {
            AOutbDeliveryItemType.itemPackingIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemPackingIncompletionStatus")
        }
        if !AOutbDeliveryItemType.itemPickingIncompletionStatus.isRemoved {
            AOutbDeliveryItemType.itemPickingIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemPickingIncompletionStatus")
        }
        if !AOutbDeliveryItemType.itemVolume.isRemoved {
            AOutbDeliveryItemType.itemVolume = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemVolume")
        }
        if !AOutbDeliveryItemType.itemVolumeUnit.isRemoved {
            AOutbDeliveryItemType.itemVolumeUnit = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemVolumeUnit")
        }
        if !AOutbDeliveryItemType.itemWeightUnit.isRemoved {
            AOutbDeliveryItemType.itemWeightUnit = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemWeightUnit")
        }
        if !AOutbDeliveryItemType.lastChangeDate.isRemoved {
            AOutbDeliveryItemType.lastChangeDate = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "LastChangeDate")
        }
        if !AOutbDeliveryItemType.loadingGroup.isRemoved {
            AOutbDeliveryItemType.loadingGroup = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "LoadingGroup")
        }
        if !AOutbDeliveryItemType.manufactureDate.isRemoved {
            AOutbDeliveryItemType.manufactureDate = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ManufactureDate")
        }
        if !AOutbDeliveryItemType.material.isRemoved {
            AOutbDeliveryItemType.material = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Material")
        }
        if !AOutbDeliveryItemType.materialByCustomer.isRemoved {
            AOutbDeliveryItemType.materialByCustomer = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialByCustomer")
        }
        if !AOutbDeliveryItemType.materialFreightGroup.isRemoved {
            AOutbDeliveryItemType.materialFreightGroup = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialFreightGroup")
        }
        if !AOutbDeliveryItemType.materialGroup.isRemoved {
            AOutbDeliveryItemType.materialGroup = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialGroup")
        }
        if !AOutbDeliveryItemType.materialIsBatchManaged.isRemoved {
            AOutbDeliveryItemType.materialIsBatchManaged = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialIsBatchManaged")
        }
        if !AOutbDeliveryItemType.materialIsIntBatchManaged.isRemoved {
            AOutbDeliveryItemType.materialIsIntBatchManaged = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialIsIntBatchManaged")
        }
        if !AOutbDeliveryItemType.numberOfSerialNumbers.isRemoved {
            AOutbDeliveryItemType.numberOfSerialNumbers = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "NumberOfSerialNumbers")
        }
        if !AOutbDeliveryItemType.orderID.isRemoved {
            AOutbDeliveryItemType.orderID = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OrderID")
        }
        if !AOutbDeliveryItemType.orderItem.isRemoved {
            AOutbDeliveryItemType.orderItem = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OrderItem")
        }
        if !AOutbDeliveryItemType.originalDeliveryQuantity.isRemoved {
            AOutbDeliveryItemType.originalDeliveryQuantity = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OriginalDeliveryQuantity")
        }
        if !AOutbDeliveryItemType.originallyRequestedMaterial.isRemoved {
            AOutbDeliveryItemType.originallyRequestedMaterial = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OriginallyRequestedMaterial")
        }
        if !AOutbDeliveryItemType.overdelivTolrtdLmtRatioInPct.isRemoved {
            AOutbDeliveryItemType.overdelivTolrtdLmtRatioInPct = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OverdelivTolrtdLmtRatioInPct")
        }
        if !AOutbDeliveryItemType.packingStatus.isRemoved {
            AOutbDeliveryItemType.packingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PackingStatus")
        }
        if !AOutbDeliveryItemType.partialDeliveryIsAllowed.isRemoved {
            AOutbDeliveryItemType.partialDeliveryIsAllowed = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PartialDeliveryIsAllowed")
        }
        if !AOutbDeliveryItemType.paymentGuaranteeForm.isRemoved {
            AOutbDeliveryItemType.paymentGuaranteeForm = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PaymentGuaranteeForm")
        }
        if !AOutbDeliveryItemType.pickingConfirmationStatus.isRemoved {
            AOutbDeliveryItemType.pickingConfirmationStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PickingConfirmationStatus")
        }
        if !AOutbDeliveryItemType.pickingControl.isRemoved {
            AOutbDeliveryItemType.pickingControl = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PickingControl")
        }
        if !AOutbDeliveryItemType.pickingStatus.isRemoved {
            AOutbDeliveryItemType.pickingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PickingStatus")
        }
        if !AOutbDeliveryItemType.plant.isRemoved {
            AOutbDeliveryItemType.plant = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Plant")
        }
        if !AOutbDeliveryItemType.primaryPostingSwitch.isRemoved {
            AOutbDeliveryItemType.primaryPostingSwitch = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PrimaryPostingSwitch")
        }
        if !AOutbDeliveryItemType.productAvailabilityDate.isRemoved {
            AOutbDeliveryItemType.productAvailabilityDate = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProductAvailabilityDate")
        }
        if !AOutbDeliveryItemType.productAvailabilityTime.isRemoved {
            AOutbDeliveryItemType.productAvailabilityTime = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProductAvailabilityTime")
        }
        if !AOutbDeliveryItemType.productConfiguration.isRemoved {
            AOutbDeliveryItemType.productConfiguration = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProductConfiguration")
        }
        if !AOutbDeliveryItemType.productHierarchyNode.isRemoved {
            AOutbDeliveryItemType.productHierarchyNode = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProductHierarchyNode")
        }
        if !AOutbDeliveryItemType.profitabilitySegment.isRemoved {
            AOutbDeliveryItemType.profitabilitySegment = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProfitabilitySegment")
        }
        if !AOutbDeliveryItemType.profitCenter.isRemoved {
            AOutbDeliveryItemType.profitCenter = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProfitCenter")
        }
        if !AOutbDeliveryItemType.proofOfDeliveryRelevanceCode.isRemoved {
            AOutbDeliveryItemType.proofOfDeliveryRelevanceCode = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProofOfDeliveryRelevanceCode")
        }
        if !AOutbDeliveryItemType.proofOfDeliveryStatus.isRemoved {
            AOutbDeliveryItemType.proofOfDeliveryStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProofOfDeliveryStatus")
        }
        if !AOutbDeliveryItemType.quantityIsFixed.isRemoved {
            AOutbDeliveryItemType.quantityIsFixed = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "QuantityIsFixed")
        }
        if !AOutbDeliveryItemType.receivingPoint.isRemoved {
            AOutbDeliveryItemType.receivingPoint = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReceivingPoint")
        }
        if !AOutbDeliveryItemType.referenceDocumentLogicalSystem.isRemoved {
            AOutbDeliveryItemType.referenceDocumentLogicalSystem = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReferenceDocumentLogicalSystem")
        }
        if !AOutbDeliveryItemType.referenceSDDocument.isRemoved {
            AOutbDeliveryItemType.referenceSDDocument = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReferenceSDDocument")
        }
        if !AOutbDeliveryItemType.referenceSDDocumentCategory.isRemoved {
            AOutbDeliveryItemType.referenceSDDocumentCategory = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReferenceSDDocumentCategory")
        }
        if !AOutbDeliveryItemType.referenceSDDocumentItem.isRemoved {
            AOutbDeliveryItemType.referenceSDDocumentItem = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReferenceSDDocumentItem")
        }
        if !AOutbDeliveryItemType.retailPromotion.isRemoved {
            AOutbDeliveryItemType.retailPromotion = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "RetailPromotion")
        }
        if !AOutbDeliveryItemType.salesDocumentItemType.isRemoved {
            AOutbDeliveryItemType.salesDocumentItemType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SalesDocumentItemType")
        }
        if !AOutbDeliveryItemType.salesGroup.isRemoved {
            AOutbDeliveryItemType.salesGroup = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SalesGroup")
        }
        if !AOutbDeliveryItemType.salesOffice.isRemoved {
            AOutbDeliveryItemType.salesOffice = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SalesOffice")
        }
        if !AOutbDeliveryItemType.sdDocumentCategory.isRemoved {
            AOutbDeliveryItemType.sdDocumentCategory = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SDDocumentCategory")
        }
        if !AOutbDeliveryItemType.sdProcessStatus.isRemoved {
            AOutbDeliveryItemType.sdProcessStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SDProcessStatus")
        }
        if !AOutbDeliveryItemType.shelfLifeExpirationDate.isRemoved {
            AOutbDeliveryItemType.shelfLifeExpirationDate = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ShelfLifeExpirationDate")
        }
        if !AOutbDeliveryItemType.statisticsDate.isRemoved {
            AOutbDeliveryItemType.statisticsDate = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StatisticsDate")
        }
        if !AOutbDeliveryItemType.stockType.isRemoved {
            AOutbDeliveryItemType.stockType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StockType")
        }
        if !AOutbDeliveryItemType.storageBin.isRemoved {
            AOutbDeliveryItemType.storageBin = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StorageBin")
        }
        if !AOutbDeliveryItemType.storageLocation.isRemoved {
            AOutbDeliveryItemType.storageLocation = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StorageLocation")
        }
        if !AOutbDeliveryItemType.storageType.isRemoved {
            AOutbDeliveryItemType.storageType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StorageType")
        }
        if !AOutbDeliveryItemType.subsequentMovementType.isRemoved {
            AOutbDeliveryItemType.subsequentMovementType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SubsequentMovementType")
        }
        if !AOutbDeliveryItemType.transportationGroup.isRemoved {
            AOutbDeliveryItemType.transportationGroup = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "TransportationGroup")
        }
        if !AOutbDeliveryItemType.underdelivTolrtdLmtRatioInPct.isRemoved {
            AOutbDeliveryItemType.underdelivTolrtdLmtRatioInPct = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "UnderdelivTolrtdLmtRatioInPct")
        }
        if !AOutbDeliveryItemType.unlimitedOverdeliveryIsAllowed.isRemoved {
            AOutbDeliveryItemType.unlimitedOverdeliveryIsAllowed = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "UnlimitedOverdeliveryIsAllowed")
        }
        if !AOutbDeliveryItemType.varblShipgProcgDurationInDays.isRemoved {
            AOutbDeliveryItemType.varblShipgProcgDurationInDays = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "VarblShipgProcgDurationInDays")
        }
        if !AOutbDeliveryItemType.warehouse.isRemoved {
            AOutbDeliveryItemType.warehouse = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Warehouse")
        }
        if !AOutbDeliveryItemType.warehouseActivityStatus.isRemoved {
            AOutbDeliveryItemType.warehouseActivityStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "WarehouseActivityStatus")
        }
        if !AOutbDeliveryItemType.warehouseStagingArea.isRemoved {
            AOutbDeliveryItemType.warehouseStagingArea = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "WarehouseStagingArea")
        }
        if !AOutbDeliveryItemType.warehouseStockCategory.isRemoved {
            AOutbDeliveryItemType.warehouseStockCategory = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "WarehouseStockCategory")
        }
        if !AOutbDeliveryItemType.warehouseStorageBin.isRemoved {
            AOutbDeliveryItemType.warehouseStorageBin = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "WarehouseStorageBin")
        }
        if !AOutbDeliveryItemType.stockSegment.isRemoved {
            AOutbDeliveryItemType.stockSegment = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StockSegment")
        }
        if !AOutbDeliveryItemType.requirementSegment.isRemoved {
            AOutbDeliveryItemType.requirementSegment = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "RequirementSegment")
        }
        if !AOutbDeliveryPartnerType.addressID.isRemoved {
            AOutbDeliveryPartnerType.addressID = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "AddressID")
        }
        if !AOutbDeliveryPartnerType.contactPerson.isRemoved {
            AOutbDeliveryPartnerType.contactPerson = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "ContactPerson")
        }
        if !AOutbDeliveryPartnerType.customer.isRemoved {
            AOutbDeliveryPartnerType.customer = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "Customer")
        }
        if !AOutbDeliveryPartnerType.partnerFunction.isRemoved {
            AOutbDeliveryPartnerType.partnerFunction = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "PartnerFunction")
        }
        if !AOutbDeliveryPartnerType.personnel.isRemoved {
            AOutbDeliveryPartnerType.personnel = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "Personnel")
        }
        if !AOutbDeliveryPartnerType.sdDocument.isRemoved {
            AOutbDeliveryPartnerType.sdDocument = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "SDDocument")
        }
        if !AOutbDeliveryPartnerType.sdDocumentItem.isRemoved {
            AOutbDeliveryPartnerType.sdDocumentItem = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "SDDocumentItem")
        }
        if !AOutbDeliveryPartnerType.supplier.isRemoved {
            AOutbDeliveryPartnerType.supplier = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "Supplier")
        }
        if !APlantType.plant.isRemoved {
            APlantType.plant = Ec1Metadata.EntityTypes.aPlantType.property(withName: "Plant")
        }
        if !APlantType.plantName.isRemoved {
            APlantType.plantName = Ec1Metadata.EntityTypes.aPlantType.property(withName: "PlantName")
        }
        if !ASerialNmbrDeliveryType.deliveryDate.isRemoved {
            ASerialNmbrDeliveryType.deliveryDate = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "DeliveryDate")
        }
        if !ASerialNmbrDeliveryType.deliveryDocument.isRemoved {
            ASerialNmbrDeliveryType.deliveryDocument = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "DeliveryDocument")
        }
        if !ASerialNmbrDeliveryType.deliveryDocumentItem.isRemoved {
            ASerialNmbrDeliveryType.deliveryDocumentItem = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "DeliveryDocumentItem")
        }
        if !ASerialNmbrDeliveryType.maintenanceItemObjectList.isRemoved {
            ASerialNmbrDeliveryType.maintenanceItemObjectList = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "MaintenanceItemObjectList")
        }
        if !ASerialNmbrDeliveryType.sdDocumentCategory.isRemoved {
            ASerialNmbrDeliveryType.sdDocumentCategory = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "SDDocumentCategory")
        }
    }
}
