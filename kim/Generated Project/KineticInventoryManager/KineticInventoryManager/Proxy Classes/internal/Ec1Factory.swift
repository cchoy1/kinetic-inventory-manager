// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

internal class Ec1Factory {
    static func registerAll() throws {
        Ec1Metadata.ComplexTypes.deliveryMessage.registerFactory(ObjectFactory.with(create: { DeliveryMessage(withDefaults: false) }, createWithDecoder: { d in try DeliveryMessage(from: d) }))
        Ec1Metadata.ComplexTypes.pickingReport.registerFactory(ObjectFactory.with(create: { PickingReport(withDefaults: false) }, createWithDecoder: { d in try PickingReport(from: d) }))
        Ec1Metadata.ComplexTypes.putawayReport.registerFactory(ObjectFactory.with(create: { PutawayReport(withDefaults: false) }, createWithDecoder: { d in try PutawayReport(from: d) }))
        Ec1Metadata.ComplexTypes.return.registerFactory(ObjectFactory.with(create: { Return(withDefaults: false) }, createWithDecoder: { d in try Return(from: d) }))
        Ec1Metadata.EntityTypes.aInbDeliveryAddressType.registerFactory(ObjectFactory.with(create: { AInbDeliveryAddressType(withDefaults: false) }, createWithDecoder: { d in try AInbDeliveryAddressType(from: d) }))
        Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.registerFactory(ObjectFactory.with(create: { AInbDeliveryDocFlowType(withDefaults: false) }, createWithDecoder: { d in try AInbDeliveryDocFlowType(from: d) }))
        Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.registerFactory(ObjectFactory.with(create: { AInbDeliveryHeaderTextType(withDefaults: false) }, createWithDecoder: { d in try AInbDeliveryHeaderTextType(from: d) }))
        Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.registerFactory(ObjectFactory.with(create: { AInbDeliveryHeaderType(withDefaults: false) }, createWithDecoder: { d in try AInbDeliveryHeaderType(from: d) }))
        Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.registerFactory(ObjectFactory.with(create: { AInbDeliveryItemTextType(withDefaults: false) }, createWithDecoder: { d in try AInbDeliveryItemTextType(from: d) }))
        Ec1Metadata.EntityTypes.aInbDeliveryItemType.registerFactory(ObjectFactory.with(create: { AInbDeliveryItemType(withDefaults: false) }, createWithDecoder: { d in try AInbDeliveryItemType(from: d) }))
        Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.registerFactory(ObjectFactory.with(create: { AInbDeliveryPartnerType(withDefaults: false) }, createWithDecoder: { d in try AInbDeliveryPartnerType(from: d) }))
        Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.registerFactory(ObjectFactory.with(create: { AInbDeliverySerialNmbrType(withDefaults: false) }, createWithDecoder: { d in try AInbDeliverySerialNmbrType(from: d) }))
        Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.registerFactory(ObjectFactory.with(create: { AMaintenanceItemObjListType(withDefaults: false) }, createWithDecoder: { d in try AMaintenanceItemObjListType(from: d) }))
        Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.registerFactory(ObjectFactory.with(create: { AMaintenanceItemObjectType(withDefaults: false) }, createWithDecoder: { d in try AMaintenanceItemObjectType(from: d) }))
        Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.registerFactory(ObjectFactory.with(create: { AMaterialDocumentHeaderType(withDefaults: false) }, createWithDecoder: { d in try AMaterialDocumentHeaderType(from: d) }))
        Ec1Metadata.EntityTypes.aMaterialDocumentItemType.registerFactory(ObjectFactory.with(create: { AMaterialDocumentItemType(withDefaults: false) }, createWithDecoder: { d in try AMaterialDocumentItemType(from: d) }))
        Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.registerFactory(ObjectFactory.with(create: { AOutbDeliveryAddressType(withDefaults: false) }, createWithDecoder: { d in try AOutbDeliveryAddressType(from: d) }))
        Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.registerFactory(ObjectFactory.with(create: { AOutbDeliveryDocFlowType(withDefaults: false) }, createWithDecoder: { d in try AOutbDeliveryDocFlowType(from: d) }))
        Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.registerFactory(ObjectFactory.with(create: { AOutbDeliveryHeaderType(withDefaults: false) }, createWithDecoder: { d in try AOutbDeliveryHeaderType(from: d) }))
        Ec1Metadata.EntityTypes.aOutbDeliveryItemType.registerFactory(ObjectFactory.with(create: { AOutbDeliveryItemType(withDefaults: false) }, createWithDecoder: { d in try AOutbDeliveryItemType(from: d) }))
        Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.registerFactory(ObjectFactory.with(create: { AOutbDeliveryPartnerType(withDefaults: false) }, createWithDecoder: { d in try AOutbDeliveryPartnerType(from: d) }))
        Ec1Metadata.EntityTypes.aPlantType.registerFactory(ObjectFactory.with(create: { APlantType(withDefaults: false) }, createWithDecoder: { d in try APlantType(from: d) }))
        Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.registerFactory(ObjectFactory.with(create: { ASerialNmbrDeliveryType(withDefaults: false) }, createWithDecoder: { d in try ASerialNmbrDeliveryType(from: d) }))
        Ec1StaticResolver.resolve()
    }
}
