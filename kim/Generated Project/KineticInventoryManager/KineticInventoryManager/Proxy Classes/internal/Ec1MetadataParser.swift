// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

internal class Ec1MetadataParser {
    internal static let options: Int = (CSDLOption.allowCaseConflicts | CSDLOption.disableFacetWarnings | CSDLOption.disableNameValidation | CSDLOption.processMixedVersions | CSDLOption.ignoreUndefinedTerms)

    internal static let parsed: CSDLDocument = Ec1MetadataParser.parse()

    static func parse() -> CSDLDocument {
        let parser = CSDLParser()
        parser.logWarnings = false
        parser.csdlOptions = Ec1MetadataParser.options
        let metadata = parser.parseInProxy(Ec1MetadataText.xml, url: "S1")
        metadata.proxyVersion = "20.1.0-110dca-20191219"
        return metadata
    }
}
