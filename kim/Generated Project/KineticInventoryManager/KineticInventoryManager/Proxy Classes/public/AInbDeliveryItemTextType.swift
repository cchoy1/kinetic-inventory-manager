// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AInbDeliveryItemTextType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var deliveryDocument_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "DeliveryDocument")

    private static var deliveryDocumentItem_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "DeliveryDocumentItem")

    private static var textElement_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "TextElement")

    private static var language_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "Language")

    private static var textElementDescription_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "TextElementDescription")

    private static var textElementText_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "TextElementText")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aInbDeliveryItemTextType)
    }

    open class func array(from: EntityValueList) -> [AInbDeliveryItemTextType] {
        return ArrayConverter.convert(from.toArray(), [AInbDeliveryItemTextType]())
    }

    open func copy() -> AInbDeliveryItemTextType {
        return CastRequired<AInbDeliveryItemTextType>.from(self.copyEntity())
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(AInbDeliveryItemTextType.self)
            defer { objc_sync_exit(AInbDeliveryItemTextType.self) }
            do {
                return AInbDeliveryItemTextType.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemTextType.self)
            defer { objc_sync_exit(AInbDeliveryItemTextType.self) }
            do {
                AInbDeliveryItemTextType.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemTextType.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemTextType.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItem: Property {
        get {
            objc_sync_enter(AInbDeliveryItemTextType.self)
            defer { objc_sync_exit(AInbDeliveryItemTextType.self) }
            do {
                return AInbDeliveryItemTextType.deliveryDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemTextType.self)
            defer { objc_sync_exit(AInbDeliveryItemTextType.self) }
            do {
                AInbDeliveryItemTextType.deliveryDocumentItem_ = value
            }
        }
    }

    open var deliveryDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemTextType.deliveryDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemTextType.deliveryDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(deliveryDocument: String?, deliveryDocumentItem: String?, textElement: String?, language: String?) -> EntityKey {
        return EntityKey().with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem)).with(name: "TextElement", value: StringValue.of(optional: textElement)).with(name: "Language", value: StringValue.of(optional: language))
    }

    open class var language: Property {
        get {
            objc_sync_enter(AInbDeliveryItemTextType.self)
            defer { objc_sync_exit(AInbDeliveryItemTextType.self) }
            do {
                return AInbDeliveryItemTextType.language_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemTextType.self)
            defer { objc_sync_exit(AInbDeliveryItemTextType.self) }
            do {
                AInbDeliveryItemTextType.language_ = value
            }
        }
    }

    open var language: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemTextType.language))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemTextType.language, to: StringValue.of(optional: value))
        }
    }

    open var old: AInbDeliveryItemTextType {
        return CastRequired<AInbDeliveryItemTextType>.from(self.oldEntity)
    }

    open class var textElement: Property {
        get {
            objc_sync_enter(AInbDeliveryItemTextType.self)
            defer { objc_sync_exit(AInbDeliveryItemTextType.self) }
            do {
                return AInbDeliveryItemTextType.textElement_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemTextType.self)
            defer { objc_sync_exit(AInbDeliveryItemTextType.self) }
            do {
                AInbDeliveryItemTextType.textElement_ = value
            }
        }
    }

    open var textElement: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemTextType.textElement))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemTextType.textElement, to: StringValue.of(optional: value))
        }
    }

    open class var textElementDescription: Property {
        get {
            objc_sync_enter(AInbDeliveryItemTextType.self)
            defer { objc_sync_exit(AInbDeliveryItemTextType.self) }
            do {
                return AInbDeliveryItemTextType.textElementDescription_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemTextType.self)
            defer { objc_sync_exit(AInbDeliveryItemTextType.self) }
            do {
                AInbDeliveryItemTextType.textElementDescription_ = value
            }
        }
    }

    open var textElementDescription: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemTextType.textElementDescription))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemTextType.textElementDescription, to: StringValue.of(optional: value))
        }
    }

    open class var textElementText: Property {
        get {
            objc_sync_enter(AInbDeliveryItemTextType.self)
            defer { objc_sync_exit(AInbDeliveryItemTextType.self) }
            do {
                return AInbDeliveryItemTextType.textElementText_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemTextType.self)
            defer { objc_sync_exit(AInbDeliveryItemTextType.self) }
            do {
                AInbDeliveryItemTextType.textElementText_ = value
            }
        }
    }

    open var textElementText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemTextType.textElementText))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemTextType.textElementText, to: StringValue.of(optional: value))
        }
    }
}
