// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class Ec1<Provider: DataServiceProvider>: DataService<Provider> {
    public override init(provider: Provider) {
        super.init(provider: provider)
        self.provider.metadata = Ec1Metadata.document
    }

    open func cancel(materialDocumentYear: String?, materialDocument: String?, postingDate: LocalDateTime?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.cancel, ParameterList(capacity: 3 as Int).with(name: "MaterialDocumentYear", value: StringValue.of(optional: materialDocumentYear)).with(name: "MaterialDocument", value: StringValue.of(optional: materialDocument)).with(name: "PostingDate", value: postingDate)), headers: var_headers, options: var_options)
    }

    open func cancel(materialDocumentYear: String?, materialDocument: String?, postingDate: LocalDateTime?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.cancel(materialDocumentYear: materialDocumentYear, materialDocument: materialDocument, postingDate: postingDate, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func cancelItem(materialDocumentYear: String?, materialDocument: String?, materialDocumentItem: String?, postingDate: LocalDateTime?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.cancelItem, ParameterList(capacity: 4 as Int).with(name: "MaterialDocumentYear", value: StringValue.of(optional: materialDocumentYear)).with(name: "MaterialDocument", value: StringValue.of(optional: materialDocument)).with(name: "MaterialDocumentItem", value: StringValue.of(optional: materialDocumentItem)).with(name: "PostingDate", value: postingDate)), headers: var_headers, options: var_options)
    }

    open func cancelItem(materialDocumentYear: String?, materialDocument: String?, materialDocumentItem: String?, postingDate: LocalDateTime?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.cancelItem(materialDocumentYear: materialDocumentYear, materialDocument: materialDocument, materialDocumentItem: materialDocumentItem, postingDate: postingDate, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func confirmPickingAllItems(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.confirmPickingAllItems, ParameterList(capacity: 1 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument))), headers: var_headers, options: var_options)
    }

    open func confirmPickingAllItems(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.confirmPickingAllItems(deliveryDocument: deliveryDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func confirmPickingOneItem(deliveryDocumentItem: String?, deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.confirmPickingOneItem, ParameterList(capacity: 2 as Int).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem)).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument))), headers: var_headers, options: var_options)
    }

    open func confirmPickingOneItem(deliveryDocumentItem: String?, deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.confirmPickingOneItem(deliveryDocumentItem: deliveryDocumentItem, deliveryDocument: deliveryDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func confirmPutawayAllItems(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.confirmPutawayAllItems, ParameterList(capacity: 1 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument))), headers: var_headers, options: var_options)
    }

    open func confirmPutawayAllItems(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.confirmPutawayAllItems(deliveryDocument: deliveryDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func confirmPutawayOneItem(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.confirmPutawayOneItem, ParameterList(capacity: 2 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem))), headers: var_headers, options: var_options)
    }

    open func confirmPutawayOneItem(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.confirmPutawayOneItem(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func fetchAInbDeliveryAddressType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryAddressType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryAddressType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryAddressTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryAddressType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryAddressType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryAddressType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryAddressTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryAddressType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryAddressType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryAddressTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryAddressTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryAddressType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryAddressTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryAddressTypeWithKey(addressID: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryAddressType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryAddressType(matching: var_query.withKey(AInbDeliveryAddressType.key(addressID: addressID)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryAddressTypeWithKey(addressID: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryAddressType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryAddressTypeWithKey(addressID: addressID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryDocFlowType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryDocFlowType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryDocFlowType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryDocFlowTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryDocFlowType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryDocFlowType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryDocFlowType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryDocFlowTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryDocFlowType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryDocFlowType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryDocFlowTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryDocFlowTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryDocFlowType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryDocFlowTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryDocFlowTypeWithKey(precedingDocument: String?, precedingDocumentItem: String?, subsequentDocumentCategory: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryDocFlowType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryDocFlowType(matching: var_query.withKey(AInbDeliveryDocFlowType.key(precedingDocument: precedingDocument, precedingDocumentItem: precedingDocumentItem, subsequentDocumentCategory: subsequentDocumentCategory)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryDocFlowTypeWithKey(precedingDocument: String?, precedingDocumentItem: String?, subsequentDocumentCategory: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryDocFlowType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryDocFlowTypeWithKey(precedingDocument: precedingDocument, precedingDocumentItem: precedingDocumentItem, subsequentDocumentCategory: subsequentDocumentCategory, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryHeader(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryHeaderType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryHeaderType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryHeader), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryHeader(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryHeaderType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryHeader(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryHeaderTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryHeaderTextType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryHeaderTextType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryHeaderTextTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryHeaderTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryHeaderTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryHeaderTextType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryHeaderTextTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryHeaderTextType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryHeaderTextType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryHeaderTextTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryHeaderTextTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryHeaderTextType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryHeaderTextTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryHeaderTextTypeWithKey(deliveryDocument: String?, textElement: String?, language: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryHeaderTextType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryHeaderTextType(matching: var_query.withKey(AInbDeliveryHeaderTextType.key(deliveryDocument: deliveryDocument, textElement: textElement, language: language)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryHeaderTextTypeWithKey(deliveryDocument: String?, textElement: String?, language: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryHeaderTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryHeaderTextTypeWithKey(deliveryDocument: deliveryDocument, textElement: textElement, language: language, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryHeaderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryHeaderType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryHeaderType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryHeader), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryHeaderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryHeaderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryHeaderType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryHeaderTypeWithKey(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryHeaderType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryHeaderType(matching: var_query.withKey(AInbDeliveryHeaderType.key(deliveryDocument: deliveryDocument)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryHeaderTypeWithKey(deliveryDocument: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryHeaderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryHeaderTypeWithKey(deliveryDocument: deliveryDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryItem(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryItemType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryItemType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryItem), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryItem(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryItemType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryItem(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryItemTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryItemTextType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryItemTextType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryItemTextTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryItemTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryItemTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryItemTextType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryItemTextTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryItemTextType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryItemTextType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryItemTextTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryItemTextTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryItemTextType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryItemTextTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryItemTextTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, textElement: String?, language: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryItemTextType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryItemTextType(matching: var_query.withKey(AInbDeliveryItemTextType.key(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, textElement: textElement, language: language)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryItemTextTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, textElement: String?, language: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryItemTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryItemTextTypeWithKey(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, textElement: textElement, language: language, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryItemType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryItemType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryItem), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryItemType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryItemTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryItemType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryItemType(matching: var_query.withKey(AInbDeliveryItemType.key(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryItemTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryItemTypeWithKey(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryPartner(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryPartnerType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryPartnerType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryPartner), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryPartner(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryPartnerType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryPartner(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryPartnerType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryPartnerType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryPartner), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryPartnerType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryPartnerTypeWithKey(partnerFunction: String?, sdDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryPartnerType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryPartnerType(matching: var_query.withKey(AInbDeliveryPartnerType.key(partnerFunction: partnerFunction, sdDocument: sdDocument)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryPartnerTypeWithKey(partnerFunction: String?, sdDocument: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryPartnerTypeWithKey(partnerFunction: partnerFunction, sdDocument: sdDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliverySerialNmbrType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliverySerialNmbrType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliverySerialNmbrType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliverySerialNmbrTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliverySerialNmbrType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliverySerialNmbrType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliverySerialNmbrType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliverySerialNmbrTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliverySerialNmbrType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliverySerialNmbrType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliverySerialNmbrTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliverySerialNmbrTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliverySerialNmbrType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliverySerialNmbrTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliverySerialNmbrTypeWithKey(maintenanceItemObjectList: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliverySerialNmbrType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliverySerialNmbrType(matching: var_query.withKey(AInbDeliverySerialNmbrType.key(maintenanceItemObjectList: maintenanceItemObjectList)), headers: headers, options: options)
    }

    open func fetchAInbDeliverySerialNmbrTypeWithKey(maintenanceItemObjectList: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliverySerialNmbrType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliverySerialNmbrTypeWithKey(maintenanceItemObjectList: maintenanceItemObjectList, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaintenanceItemObjListType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaintenanceItemObjListType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AMaintenanceItemObjListType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aMaintenanceItemObjListTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAMaintenanceItemObjListType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaintenanceItemObjListType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaintenanceItemObjListType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaintenanceItemObjListTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AMaintenanceItemObjListType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AMaintenanceItemObjListType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aMaintenanceItemObjListTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAMaintenanceItemObjListTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AMaintenanceItemObjListType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaintenanceItemObjListTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaintenanceItemObjListTypeWithKey(maintenanceItemObject: Int?, maintenanceItemObjectList: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaintenanceItemObjListType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAMaintenanceItemObjListType(matching: var_query.withKey(AMaintenanceItemObjListType.key(maintenanceItemObject: maintenanceItemObject, maintenanceItemObjectList: maintenanceItemObjectList)), headers: headers, options: options)
    }

    open func fetchAMaintenanceItemObjListTypeWithKey(maintenanceItemObject: Int?, maintenanceItemObjectList: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaintenanceItemObjListType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaintenanceItemObjListTypeWithKey(maintenanceItemObject: maintenanceItemObject, maintenanceItemObjectList: maintenanceItemObjectList, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaintenanceItemObjectType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaintenanceItemObjectType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AMaintenanceItemObjectType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aMaintenanceItemObjectTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAMaintenanceItemObjectType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaintenanceItemObjectType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaintenanceItemObjectType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaintenanceItemObjectTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AMaintenanceItemObjectType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AMaintenanceItemObjectType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aMaintenanceItemObjectTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAMaintenanceItemObjectTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AMaintenanceItemObjectType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaintenanceItemObjectTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaintenanceItemObjectTypeWithKey(maintenanceItemObject: Int?, maintenanceItemObjectList: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaintenanceItemObjectType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAMaintenanceItemObjectType(matching: var_query.withKey(AMaintenanceItemObjectType.key(maintenanceItemObject: maintenanceItemObject, maintenanceItemObjectList: maintenanceItemObjectList)), headers: headers, options: options)
    }

    open func fetchAMaintenanceItemObjectTypeWithKey(maintenanceItemObject: Int?, maintenanceItemObjectList: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaintenanceItemObjectType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaintenanceItemObjectTypeWithKey(maintenanceItemObject: maintenanceItemObject, maintenanceItemObjectList: maintenanceItemObjectList, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaterialDocumentHeader(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AMaterialDocumentHeaderType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AMaterialDocumentHeaderType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aMaterialDocumentHeader), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAMaterialDocumentHeader(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AMaterialDocumentHeaderType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaterialDocumentHeader(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaterialDocumentHeaderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaterialDocumentHeaderType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AMaterialDocumentHeaderType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aMaterialDocumentHeader), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAMaterialDocumentHeaderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaterialDocumentHeaderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaterialDocumentHeaderType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaterialDocumentHeaderTypeWithKey(materialDocumentYear: String?, materialDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaterialDocumentHeaderType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAMaterialDocumentHeaderType(matching: var_query.withKey(AMaterialDocumentHeaderType.key(materialDocumentYear: materialDocumentYear, materialDocument: materialDocument)), headers: headers, options: options)
    }

    open func fetchAMaterialDocumentHeaderTypeWithKey(materialDocumentYear: String?, materialDocument: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaterialDocumentHeaderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaterialDocumentHeaderTypeWithKey(materialDocumentYear: materialDocumentYear, materialDocument: materialDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaterialDocumentItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaterialDocumentItemType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AMaterialDocumentItemType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aMaterialDocumentItemTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAMaterialDocumentItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaterialDocumentItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaterialDocumentItemType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaterialDocumentItemTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AMaterialDocumentItemType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AMaterialDocumentItemType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aMaterialDocumentItemTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAMaterialDocumentItemTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AMaterialDocumentItemType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaterialDocumentItemTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaterialDocumentItemTypeWithKey(materialDocumentYear: String?, materialDocument: String?, materialDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaterialDocumentItemType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAMaterialDocumentItemType(matching: var_query.withKey(AMaterialDocumentItemType.key(materialDocumentYear: materialDocumentYear, materialDocument: materialDocument, materialDocumentItem: materialDocumentItem)), headers: headers, options: options)
    }

    open func fetchAMaterialDocumentItemTypeWithKey(materialDocumentYear: String?, materialDocument: String?, materialDocumentItem: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaterialDocumentItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaterialDocumentItemTypeWithKey(materialDocumentYear: materialDocumentYear, materialDocument: materialDocument, materialDocumentItem: materialDocumentItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryAddressType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryAddressType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AOutbDeliveryAddressType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryAddressTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAOutbDeliveryAddressType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryAddressType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryAddressType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryAddressTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AOutbDeliveryAddressType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AOutbDeliveryAddressType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryAddressTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAOutbDeliveryAddressTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AOutbDeliveryAddressType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryAddressTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryAddressTypeWithKey(addressID: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryAddressType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAOutbDeliveryAddressType(matching: var_query.withKey(AOutbDeliveryAddressType.key(addressID: addressID)), headers: headers, options: options)
    }

    open func fetchAOutbDeliveryAddressTypeWithKey(addressID: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryAddressType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryAddressTypeWithKey(addressID: addressID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryDocFlowType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryDocFlowType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AOutbDeliveryDocFlowType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryDocFlowTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAOutbDeliveryDocFlowType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryDocFlowType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryDocFlowType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryDocFlowTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AOutbDeliveryDocFlowType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AOutbDeliveryDocFlowType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryDocFlowTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAOutbDeliveryDocFlowTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AOutbDeliveryDocFlowType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryDocFlowTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryDocFlowTypeWithKey(precedingDocument: String?, precedingDocumentItem: String?, subsequentDocumentCategory: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryDocFlowType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAOutbDeliveryDocFlowType(matching: var_query.withKey(AOutbDeliveryDocFlowType.key(precedingDocument: precedingDocument, precedingDocumentItem: precedingDocumentItem, subsequentDocumentCategory: subsequentDocumentCategory)), headers: headers, options: options)
    }

    open func fetchAOutbDeliveryDocFlowTypeWithKey(precedingDocument: String?, precedingDocumentItem: String?, subsequentDocumentCategory: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryDocFlowType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryDocFlowTypeWithKey(precedingDocument: precedingDocument, precedingDocumentItem: precedingDocumentItem, subsequentDocumentCategory: subsequentDocumentCategory, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryHeader(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AOutbDeliveryHeaderType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AOutbDeliveryHeaderType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryHeader), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAOutbDeliveryHeader(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AOutbDeliveryHeaderType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryHeader(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryHeaderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryHeaderType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AOutbDeliveryHeaderType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryHeader), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAOutbDeliveryHeaderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryHeaderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryHeaderType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryHeaderTypeWithKey(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryHeaderType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAOutbDeliveryHeaderType(matching: var_query.withKey(AOutbDeliveryHeaderType.key(deliveryDocument: deliveryDocument)), headers: headers, options: options)
    }

    open func fetchAOutbDeliveryHeaderTypeWithKey(deliveryDocument: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryHeaderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryHeaderTypeWithKey(deliveryDocument: deliveryDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryItem(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AOutbDeliveryItemType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AOutbDeliveryItemType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryItem), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAOutbDeliveryItem(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AOutbDeliveryItemType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryItem(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryItemType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AOutbDeliveryItemType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryItem), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAOutbDeliveryItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryItemType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryItemTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryItemType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAOutbDeliveryItemType(matching: var_query.withKey(AOutbDeliveryItemType.key(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem)), headers: headers, options: options)
    }

    open func fetchAOutbDeliveryItemTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryItemTypeWithKey(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryPartnerType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AOutbDeliveryPartnerType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryPartnerTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAOutbDeliveryPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryPartnerType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryPartnerTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AOutbDeliveryPartnerType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AOutbDeliveryPartnerType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryPartnerTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAOutbDeliveryPartnerTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AOutbDeliveryPartnerType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryPartnerTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryPartnerTypeWithKey(partnerFunction: String?, sdDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryPartnerType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAOutbDeliveryPartnerType(matching: var_query.withKey(AOutbDeliveryPartnerType.key(partnerFunction: partnerFunction, sdDocument: sdDocument)), headers: headers, options: options)
    }

    open func fetchAOutbDeliveryPartnerTypeWithKey(partnerFunction: String?, sdDocument: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryPartnerTypeWithKey(partnerFunction: partnerFunction, sdDocument: sdDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPlant(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [APlantType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try APlantType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aPlant), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAPlant(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([APlantType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPlant(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPlantType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APlantType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<APlantType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aPlant), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAPlantType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APlantType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPlantType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPlantTypeWithKey(plant: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APlantType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAPlantType(matching: var_query.withKey(APlantType.key(plant: plant)), headers: headers, options: options)
    }

    open func fetchAPlantTypeWithKey(plant: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APlantType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPlantTypeWithKey(plant: plant, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchASerialNmbrDeliveryType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ASerialNmbrDeliveryType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<ASerialNmbrDeliveryType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aSerialNmbrDeliveryTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchASerialNmbrDeliveryType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ASerialNmbrDeliveryType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchASerialNmbrDeliveryType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchASerialNmbrDeliveryTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [ASerialNmbrDeliveryType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ASerialNmbrDeliveryType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aSerialNmbrDeliveryTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchASerialNmbrDeliveryTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([ASerialNmbrDeliveryType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchASerialNmbrDeliveryTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchASerialNmbrDeliveryTypeWithKey(maintenanceItemObjectList: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ASerialNmbrDeliveryType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchASerialNmbrDeliveryType(matching: var_query.withKey(ASerialNmbrDeliveryType.key(maintenanceItemObjectList: maintenanceItemObjectList)), headers: headers, options: options)
    }

    open func fetchASerialNmbrDeliveryTypeWithKey(maintenanceItemObjectList: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ASerialNmbrDeliveryType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchASerialNmbrDeliveryTypeWithKey(maintenanceItemObjectList: maintenanceItemObjectList, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open override var metadataLock: MetadataLock {
        return Ec1Metadata.lock
    }

    open func pickAllItems(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.pickAllItems, ParameterList(capacity: 1 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument))), headers: var_headers, options: var_options)
    }

    open func pickAllItems(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.pickAllItems(deliveryDocument: deliveryDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func pickAndBatchSplitOneItem(deliveryDocument: String?, deliveryDocumentItem: String?, batch: String?, splitQuantity: BigInteger?, splitQuantityUnit: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.pickAndBatchSplitOneItem, ParameterList(capacity: 5 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem)).with(name: "Batch", value: StringValue.of(optional: batch)).with(name: "SplitQuantity", value: IntegerValue.of(optional: splitQuantity)).with(name: "SplitQuantityUnit", value: StringValue.of(optional: splitQuantityUnit))), headers: var_headers, options: var_options)
    }

    open func pickAndBatchSplitOneItem(deliveryDocument: String?, deliveryDocumentItem: String?, batch: String?, splitQuantity: BigInteger?, splitQuantityUnit: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.pickAndBatchSplitOneItem(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, batch: batch, splitQuantity: splitQuantity, splitQuantityUnit: splitQuantityUnit, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func pickOneItem(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.pickOneItem, ParameterList(capacity: 2 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem))), headers: var_headers, options: var_options)
    }

    open func pickOneItem(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.pickOneItem(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func pickOneItemWithBaseQuantity(deliveryDocument: String?, deliveryDocumentItem: String?, actualDeliveredQtyInBaseUnit: BigInteger?, baseUnit: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.pickOneItemWithBaseQuantity, ParameterList(capacity: 4 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem)).with(name: "ActualDeliveredQtyInBaseUnit", value: IntegerValue.of(optional: actualDeliveredQtyInBaseUnit)).with(name: "BaseUnit", value: StringValue.of(optional: baseUnit))), headers: var_headers, options: var_options)
    }

    open func pickOneItemWithBaseQuantity(deliveryDocument: String?, deliveryDocumentItem: String?, actualDeliveredQtyInBaseUnit: BigInteger?, baseUnit: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.pickOneItemWithBaseQuantity(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, actualDeliveredQtyInBaseUnit: actualDeliveredQtyInBaseUnit, baseUnit: baseUnit, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func pickOneItemWithSalesQuantity(actualDeliveryQuantity: BigInteger?, deliveryDocument: String?, deliveryDocumentItem: String?, deliveryQuantityUnit: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.pickOneItemWithSalesQuantity, ParameterList(capacity: 4 as Int).with(name: "ActualDeliveryQuantity", value: IntegerValue.of(optional: actualDeliveryQuantity)).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem)).with(name: "DeliveryQuantityUnit", value: StringValue.of(optional: deliveryQuantityUnit))), headers: var_headers, options: var_options)
    }

    open func pickOneItemWithSalesQuantity(actualDeliveryQuantity: BigInteger?, deliveryDocument: String?, deliveryDocumentItem: String?, deliveryQuantityUnit: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.pickOneItemWithSalesQuantity(actualDeliveryQuantity: actualDeliveryQuantity, deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, deliveryQuantityUnit: deliveryQuantityUnit, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func postGoodsIssue(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.postGoodsIssue, ParameterList(capacity: 1 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument))), headers: var_headers, options: var_options)
    }

    open func postGoodsIssue(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.postGoodsIssue(deliveryDocument: deliveryDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func postGoodsReceipt(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.postGoodsReceipt, ParameterList(capacity: 1 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument))), headers: var_headers, options: var_options)
    }

    open func postGoodsReceipt(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.postGoodsReceipt(deliveryDocument: deliveryDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func putawayAllItems(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.putawayAllItems, ParameterList(capacity: 1 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument))), headers: var_headers, options: var_options)
    }

    open func putawayAllItems(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.putawayAllItems(deliveryDocument: deliveryDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func putawayOneItem(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.putawayOneItem, ParameterList(capacity: 2 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem))), headers: var_headers, options: var_options)
    }

    open func putawayOneItem(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.putawayOneItem(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open override func refreshMetadata() throws {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        do {
            try ProxyInternal.refreshMetadataWithLock(service: self, fetcher: nil, options: Ec1MetadataParser.options, mergeAction: { Ec1MetadataChanges.merge(metadata: self.metadata) })
        }
    }

    open func reverseGoodsIssue(deliveryDocument: String?, actualGoodsMovementDate: LocalDateTime?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.reverseGoodsIssue, ParameterList(capacity: 2 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "ActualGoodsMovementDate", value: actualGoodsMovementDate)), headers: var_headers, options: var_options)
    }

    open func reverseGoodsIssue(deliveryDocument: String?, actualGoodsMovementDate: LocalDateTime?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.reverseGoodsIssue(deliveryDocument: deliveryDocument, actualGoodsMovementDate: actualGoodsMovementDate, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func reverseGoodsReceipt(deliveryDocument: String?, actualGoodsMovementDate: LocalDateTime?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.reverseGoodsReceipt, ParameterList(capacity: 2 as Int).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "ActualGoodsMovementDate", value: actualGoodsMovementDate)), headers: var_headers, options: var_options)
    }

    open func reverseGoodsReceipt(deliveryDocument: String?, actualGoodsMovementDate: LocalDateTime?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.reverseGoodsReceipt(deliveryDocument: deliveryDocument, actualGoodsMovementDate: actualGoodsMovementDate, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func setPickingQuantityWithBaseQuantity(actualDeliveredQtyInBaseUnit: BigInteger?, baseUnit: String?, deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.setPickingQuantityWithBaseQuantity, ParameterList(capacity: 4 as Int).with(name: "ActualDeliveredQtyInBaseUnit", value: IntegerValue.of(optional: actualDeliveredQtyInBaseUnit)).with(name: "BaseUnit", value: StringValue.of(optional: baseUnit)).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem))), headers: var_headers, options: var_options)
    }

    open func setPickingQuantityWithBaseQuantity(actualDeliveredQtyInBaseUnit: BigInteger?, baseUnit: String?, deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.setPickingQuantityWithBaseQuantity(actualDeliveredQtyInBaseUnit: actualDeliveredQtyInBaseUnit, baseUnit: baseUnit, deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }

    open func setPutawayQuantityWithBaseQuantity(actualDeliveredQtyInBaseUnit: BigInteger?, baseUnit: String?, deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        _ = try self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.setPutawayQuantityWithBaseQuantity, ParameterList(capacity: 4 as Int).with(name: "ActualDeliveredQtyInBaseUnit", value: IntegerValue.of(optional: actualDeliveredQtyInBaseUnit)).with(name: "BaseUnit", value: StringValue.of(optional: baseUnit)).with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem))), headers: var_headers, options: var_options)
    }

    open func setPutawayQuantityWithBaseQuantity(actualDeliveredQtyInBaseUnit: BigInteger?, baseUnit: String?, deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Error?) -> Void) {
        self.addBackgroundOperationForAction {
            do {
                try self.setPutawayQuantityWithBaseQuantity(actualDeliveredQtyInBaseUnit: actualDeliveredQtyInBaseUnit, baseUnit: baseUnit, deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(error)
                }
            }
        }
    }
}
