// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class DeliveryMessage: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var collectiveProcessing_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "CollectiveProcessing")

    private static var deliveryDocument_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "DeliveryDocument")

    private static var deliveryDocumentItem_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "DeliveryDocumentItem")

    private static var scheduleLine_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "ScheduleLine")

    private static var collectiveProcessingMsgCounter_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "CollectiveProcessingMsgCounter")

    private static var systemMessageIdentification_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageIdentification")

    private static var systemMessageNumber_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageNumber")

    private static var systemMessageType_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageType")

    private static var systemMessageVariable1_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageVariable1")

    private static var systemMessageVariable2_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageVariable2")

    private static var systemMessageVariable3_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageVariable3")

    private static var systemMessageVariable4_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "SystemMessageVariable4")

    private static var collectiveProcessingType_: Property = Ec1Metadata.ComplexTypes.deliveryMessage.property(withName: "CollectiveProcessingType")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.ComplexTypes.deliveryMessage)
    }

    open class var collectiveProcessing: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.collectiveProcessing_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.collectiveProcessing_ = value
            }
        }
    }

    open var collectiveProcessing: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.collectiveProcessing))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.collectiveProcessing, to: StringValue.of(optional: value))
        }
    }

    open class var collectiveProcessingMsgCounter: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.collectiveProcessingMsgCounter_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.collectiveProcessingMsgCounter_ = value
            }
        }
    }

    open var collectiveProcessingMsgCounter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.collectiveProcessingMsgCounter))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.collectiveProcessingMsgCounter, to: StringValue.of(optional: value))
        }
    }

    open class var collectiveProcessingType: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.collectiveProcessingType_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.collectiveProcessingType_ = value
            }
        }
    }

    open var collectiveProcessingType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.collectiveProcessingType))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.collectiveProcessingType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> DeliveryMessage {
        return CastRequired<DeliveryMessage>.from(self.copyComplex())
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItem: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.deliveryDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.deliveryDocumentItem_ = value
            }
        }
    }

    open var deliveryDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.deliveryDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.deliveryDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open var old: DeliveryMessage {
        return CastRequired<DeliveryMessage>.from(self.oldComplex)
    }

    open class var scheduleLine: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.scheduleLine_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.scheduleLine_ = value
            }
        }
    }

    open var scheduleLine: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.scheduleLine))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.scheduleLine, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageIdentification: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.systemMessageIdentification_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.systemMessageIdentification_ = value
            }
        }
    }

    open var systemMessageIdentification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.systemMessageIdentification))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.systemMessageIdentification, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageNumber: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.systemMessageNumber_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.systemMessageNumber_ = value
            }
        }
    }

    open var systemMessageNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.systemMessageNumber))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.systemMessageNumber, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageType: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.systemMessageType_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.systemMessageType_ = value
            }
        }
    }

    open var systemMessageType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.systemMessageType))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.systemMessageType, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageVariable1: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.systemMessageVariable1_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.systemMessageVariable1_ = value
            }
        }
    }

    open var systemMessageVariable1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.systemMessageVariable1))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.systemMessageVariable1, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageVariable2: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.systemMessageVariable2_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.systemMessageVariable2_ = value
            }
        }
    }

    open var systemMessageVariable2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.systemMessageVariable2))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.systemMessageVariable2, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageVariable3: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.systemMessageVariable3_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.systemMessageVariable3_ = value
            }
        }
    }

    open var systemMessageVariable3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.systemMessageVariable3))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.systemMessageVariable3, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageVariable4: Property {
        get {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                return DeliveryMessage.systemMessageVariable4_
            }
        }
        set(value) {
            objc_sync_enter(DeliveryMessage.self)
            defer { objc_sync_exit(DeliveryMessage.self) }
            do {
                DeliveryMessage.systemMessageVariable4_ = value
            }
        }
    }

    open var systemMessageVariable4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DeliveryMessage.systemMessageVariable4))
        }
        set(value) {
            self.setOptionalValue(for: DeliveryMessage.systemMessageVariable4, to: StringValue.of(optional: value))
        }
    }
}
