// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class APlantType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var plant_: Property = Ec1Metadata.EntityTypes.aPlantType.property(withName: "Plant")

    private static var plantName_: Property = Ec1Metadata.EntityTypes.aPlantType.property(withName: "PlantName")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aPlantType)
    }

    open class func array(from: EntityValueList) -> [APlantType] {
        return ArrayConverter.convert(from.toArray(), [APlantType]())
    }

    open func copy() -> APlantType {
        return CastRequired<APlantType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(plant: String?) -> EntityKey {
        return EntityKey().with(name: "Plant", value: StringValue.of(optional: plant))
    }

    open var old: APlantType {
        return CastRequired<APlantType>.from(self.oldEntity)
    }

    open class var plant: Property {
        get {
            objc_sync_enter(APlantType.self)
            defer { objc_sync_exit(APlantType.self) }
            do {
                return APlantType.plant_
            }
        }
        set(value) {
            objc_sync_enter(APlantType.self)
            defer { objc_sync_exit(APlantType.self) }
            do {
                APlantType.plant_ = value
            }
        }
    }

    open var plant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APlantType.plant))
        }
        set(value) {
            self.setOptionalValue(for: APlantType.plant, to: StringValue.of(optional: value))
        }
    }

    open class var plantName: Property {
        get {
            objc_sync_enter(APlantType.self)
            defer { objc_sync_exit(APlantType.self) }
            do {
                return APlantType.plantName_
            }
        }
        set(value) {
            objc_sync_enter(APlantType.self)
            defer { objc_sync_exit(APlantType.self) }
            do {
                APlantType.plantName_ = value
            }
        }
    }

    open var plantName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APlantType.plantName))
        }
        set(value) {
            self.setOptionalValue(for: APlantType.plantName, to: StringValue.of(optional: value))
        }
    }
}
