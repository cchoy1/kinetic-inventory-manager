// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AMaterialDocumentHeaderType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var materialDocumentYear_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "MaterialDocumentYear")

    private static var materialDocument_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "MaterialDocument")

    private static var inventoryTransactionType_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "InventoryTransactionType")

    private static var documentDate_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "DocumentDate")

    private static var postingDate_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "PostingDate")

    private static var creationDate_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "CreationDate")

    private static var creationTime_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "CreationTime")

    private static var createdByUser_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "CreatedByUser")

    private static var materialDocumentHeaderText_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "MaterialDocumentHeaderText")

    private static var referenceDocument_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "ReferenceDocument")

    private static var goodsMovementCode_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "GoodsMovementCode")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType)
    }

    open class func array(from: EntityValueList) -> [AMaterialDocumentHeaderType] {
        return ArrayConverter.convert(from.toArray(), [AMaterialDocumentHeaderType]())
    }

    open func copy() -> AMaterialDocumentHeaderType {
        return CastRequired<AMaterialDocumentHeaderType>.from(self.copyEntity())
    }

    open class var createdByUser: Property {
        get {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                return AMaterialDocumentHeaderType.createdByUser_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                AMaterialDocumentHeaderType.createdByUser_ = value
            }
        }
    }

    open var createdByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentHeaderType.createdByUser))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentHeaderType.createdByUser, to: StringValue.of(optional: value))
        }
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                return AMaterialDocumentHeaderType.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                AMaterialDocumentHeaderType.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AMaterialDocumentHeaderType.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentHeaderType.creationDate, to: value)
        }
    }

    open class var creationTime: Property {
        get {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                return AMaterialDocumentHeaderType.creationTime_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                AMaterialDocumentHeaderType.creationTime_ = value
            }
        }
    }

    open var creationTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AMaterialDocumentHeaderType.creationTime))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentHeaderType.creationTime, to: value)
        }
    }

    open class var documentDate: Property {
        get {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                return AMaterialDocumentHeaderType.documentDate_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                AMaterialDocumentHeaderType.documentDate_ = value
            }
        }
    }

    open var documentDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AMaterialDocumentHeaderType.documentDate))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentHeaderType.documentDate, to: value)
        }
    }

    open class var goodsMovementCode: Property {
        get {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                return AMaterialDocumentHeaderType.goodsMovementCode_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                AMaterialDocumentHeaderType.goodsMovementCode_ = value
            }
        }
    }

    open var goodsMovementCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentHeaderType.goodsMovementCode))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentHeaderType.goodsMovementCode, to: StringValue.of(optional: value))
        }
    }

    open class var inventoryTransactionType: Property {
        get {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                return AMaterialDocumentHeaderType.inventoryTransactionType_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                AMaterialDocumentHeaderType.inventoryTransactionType_ = value
            }
        }
    }

    open var inventoryTransactionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentHeaderType.inventoryTransactionType))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentHeaderType.inventoryTransactionType, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(materialDocumentYear: String?, materialDocument: String?) -> EntityKey {
        return EntityKey().with(name: "MaterialDocumentYear", value: StringValue.of(optional: materialDocumentYear)).with(name: "MaterialDocument", value: StringValue.of(optional: materialDocument))
    }

    open class var materialDocument: Property {
        get {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                return AMaterialDocumentHeaderType.materialDocument_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                AMaterialDocumentHeaderType.materialDocument_ = value
            }
        }
    }

    open var materialDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentHeaderType.materialDocument))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentHeaderType.materialDocument, to: StringValue.of(optional: value))
        }
    }

    open class var materialDocumentHeaderText: Property {
        get {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                return AMaterialDocumentHeaderType.materialDocumentHeaderText_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                AMaterialDocumentHeaderType.materialDocumentHeaderText_ = value
            }
        }
    }

    open var materialDocumentHeaderText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentHeaderType.materialDocumentHeaderText))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentHeaderType.materialDocumentHeaderText, to: StringValue.of(optional: value))
        }
    }

    open class var materialDocumentYear: Property {
        get {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                return AMaterialDocumentHeaderType.materialDocumentYear_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                AMaterialDocumentHeaderType.materialDocumentYear_ = value
            }
        }
    }

    open var materialDocumentYear: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentHeaderType.materialDocumentYear))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentHeaderType.materialDocumentYear, to: StringValue.of(optional: value))
        }
    }

    open var old: AMaterialDocumentHeaderType {
        return CastRequired<AMaterialDocumentHeaderType>.from(self.oldEntity)
    }

    open class var postingDate: Property {
        get {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                return AMaterialDocumentHeaderType.postingDate_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                AMaterialDocumentHeaderType.postingDate_ = value
            }
        }
    }

    open var postingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AMaterialDocumentHeaderType.postingDate))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentHeaderType.postingDate, to: value)
        }
    }

    open class var referenceDocument: Property {
        get {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                return AMaterialDocumentHeaderType.referenceDocument_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentHeaderType.self)
            defer { objc_sync_exit(AMaterialDocumentHeaderType.self) }
            do {
                AMaterialDocumentHeaderType.referenceDocument_ = value
            }
        }
    }

    open var referenceDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentHeaderType.referenceDocument))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentHeaderType.referenceDocument, to: StringValue.of(optional: value))
        }
    }
}
