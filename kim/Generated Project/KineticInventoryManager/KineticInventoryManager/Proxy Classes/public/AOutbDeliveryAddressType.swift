// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AOutbDeliveryAddressType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var additionalStreetPrefixName_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "AdditionalStreetPrefixName")

    private static var additionalStreetSuffixName_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "AdditionalStreetSuffixName")

    private static var addressID_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "AddressID")

    private static var addressTimeZone_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "AddressTimeZone")

    private static var building_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Building")

    private static var businessPartnerName1_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "BusinessPartnerName1")

    private static var businessPartnerName2_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "BusinessPartnerName2")

    private static var businessPartnerName3_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "BusinessPartnerName3")

    private static var businessPartnerName4_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "BusinessPartnerName4")

    private static var careOfName_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CareOfName")

    private static var cityCode_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CityCode")

    private static var cityName_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CityName")

    private static var citySearch_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CitySearch")

    private static var companyPostalCode_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CompanyPostalCode")

    private static var correspondenceLanguage_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CorrespondenceLanguage")

    private static var country_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Country")

    private static var county_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "County")

    private static var deliveryServiceNumber_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "DeliveryServiceNumber")

    private static var deliveryServiceTypeCode_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "DeliveryServiceTypeCode")

    private static var district_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "District")

    private static var faxNumber_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "FaxNumber")

    private static var floor_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Floor")

    private static var formOfAddress_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "FormOfAddress")

    private static var fullName_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "FullName")

    private static var homeCityName_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "HomeCityName")

    private static var houseNumber_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "HouseNumber")

    private static var houseNumberSupplementText_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "HouseNumberSupplementText")

    private static var nation_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Nation")

    private static var person_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Person")

    private static var phoneNumber_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "PhoneNumber")

    private static var poBox_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBox")

    private static var poBoxDeviatingCityName_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxDeviatingCityName")

    private static var poBoxDeviatingCountry_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxDeviatingCountry")

    private static var poBoxDeviatingRegion_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxDeviatingRegion")

    private static var poBoxIsWithoutNumber_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxIsWithoutNumber")

    private static var poBoxLobbyName_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxLobbyName")

    private static var poBoxPostalCode_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxPostalCode")

    private static var postalCode_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "PostalCode")

    private static var prfrdCommMediumType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "PrfrdCommMediumType")

    private static var region_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Region")

    private static var roomNumber_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "RoomNumber")

    private static var searchTerm1_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "SearchTerm1")

    private static var streetName_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "StreetName")

    private static var streetPrefixName_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "StreetPrefixName")

    private static var streetSearch_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "StreetSearch")

    private static var streetSuffixName_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "StreetSuffixName")

    private static var taxJurisdiction_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "TaxJurisdiction")

    private static var transportZone_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "TransportZone")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aOutbDeliveryAddressType)
    }

    open class var additionalStreetPrefixName: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.additionalStreetPrefixName_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.additionalStreetPrefixName_ = value
            }
        }
    }

    open var additionalStreetPrefixName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.additionalStreetPrefixName))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.additionalStreetPrefixName, to: StringValue.of(optional: value))
        }
    }

    open class var additionalStreetSuffixName: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.additionalStreetSuffixName_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.additionalStreetSuffixName_ = value
            }
        }
    }

    open var additionalStreetSuffixName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.additionalStreetSuffixName))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.additionalStreetSuffixName, to: StringValue.of(optional: value))
        }
    }

    open class var addressID: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.addressID_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.addressID_ = value
            }
        }
    }

    open var addressID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.addressID))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.addressID, to: StringValue.of(optional: value))
        }
    }

    open class var addressTimeZone: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.addressTimeZone_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.addressTimeZone_ = value
            }
        }
    }

    open var addressTimeZone: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.addressTimeZone))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.addressTimeZone, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [AOutbDeliveryAddressType] {
        return ArrayConverter.convert(from.toArray(), [AOutbDeliveryAddressType]())
    }

    open class var building: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.building_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.building_ = value
            }
        }
    }

    open var building: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.building))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.building, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerName1: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.businessPartnerName1_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.businessPartnerName1_ = value
            }
        }
    }

    open var businessPartnerName1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.businessPartnerName1))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.businessPartnerName1, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerName2: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.businessPartnerName2_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.businessPartnerName2_ = value
            }
        }
    }

    open var businessPartnerName2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.businessPartnerName2))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.businessPartnerName2, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerName3: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.businessPartnerName3_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.businessPartnerName3_ = value
            }
        }
    }

    open var businessPartnerName3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.businessPartnerName3))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.businessPartnerName3, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerName4: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.businessPartnerName4_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.businessPartnerName4_ = value
            }
        }
    }

    open var businessPartnerName4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.businessPartnerName4))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.businessPartnerName4, to: StringValue.of(optional: value))
        }
    }

    open class var careOfName: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.careOfName_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.careOfName_ = value
            }
        }
    }

    open var careOfName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.careOfName))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.careOfName, to: StringValue.of(optional: value))
        }
    }

    open class var cityCode: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.cityCode_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.cityCode_ = value
            }
        }
    }

    open var cityCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.cityCode))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.cityCode, to: StringValue.of(optional: value))
        }
    }

    open class var cityName: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.cityName_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.cityName_ = value
            }
        }
    }

    open var cityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.cityName))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.cityName, to: StringValue.of(optional: value))
        }
    }

    open class var citySearch: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.citySearch_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.citySearch_ = value
            }
        }
    }

    open var citySearch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.citySearch))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.citySearch, to: StringValue.of(optional: value))
        }
    }

    open class var companyPostalCode: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.companyPostalCode_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.companyPostalCode_ = value
            }
        }
    }

    open var companyPostalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.companyPostalCode))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.companyPostalCode, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> AOutbDeliveryAddressType {
        return CastRequired<AOutbDeliveryAddressType>.from(self.copyEntity())
    }

    open class var correspondenceLanguage: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.correspondenceLanguage_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.correspondenceLanguage_ = value
            }
        }
    }

    open var correspondenceLanguage: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.correspondenceLanguage))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.correspondenceLanguage, to: StringValue.of(optional: value))
        }
    }

    open class var country: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.country_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.country_ = value
            }
        }
    }

    open var country: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.country))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.country, to: StringValue.of(optional: value))
        }
    }

    open class var county: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.county_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.county_ = value
            }
        }
    }

    open var county: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.county))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.county, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryServiceNumber: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.deliveryServiceNumber_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.deliveryServiceNumber_ = value
            }
        }
    }

    open var deliveryServiceNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.deliveryServiceNumber))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.deliveryServiceNumber, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryServiceTypeCode: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.deliveryServiceTypeCode_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.deliveryServiceTypeCode_ = value
            }
        }
    }

    open var deliveryServiceTypeCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.deliveryServiceTypeCode))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.deliveryServiceTypeCode, to: StringValue.of(optional: value))
        }
    }

    open class var district: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.district_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.district_ = value
            }
        }
    }

    open var district: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.district))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.district, to: StringValue.of(optional: value))
        }
    }

    open class var faxNumber: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.faxNumber_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.faxNumber_ = value
            }
        }
    }

    open var faxNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.faxNumber))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.faxNumber, to: StringValue.of(optional: value))
        }
    }

    open class var floor: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.floor_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.floor_ = value
            }
        }
    }

    open var floor: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.floor))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.floor, to: StringValue.of(optional: value))
        }
    }

    open class var formOfAddress: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.formOfAddress_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.formOfAddress_ = value
            }
        }
    }

    open var formOfAddress: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.formOfAddress))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.formOfAddress, to: StringValue.of(optional: value))
        }
    }

    open class var fullName: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.fullName_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.fullName_ = value
            }
        }
    }

    open var fullName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.fullName))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.fullName, to: StringValue.of(optional: value))
        }
    }

    open class var homeCityName: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.homeCityName_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.homeCityName_ = value
            }
        }
    }

    open var homeCityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.homeCityName))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.homeCityName, to: StringValue.of(optional: value))
        }
    }

    open class var houseNumber: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.houseNumber_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.houseNumber_ = value
            }
        }
    }

    open var houseNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.houseNumber))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.houseNumber, to: StringValue.of(optional: value))
        }
    }

    open class var houseNumberSupplementText: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.houseNumberSupplementText_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.houseNumberSupplementText_ = value
            }
        }
    }

    open var houseNumberSupplementText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.houseNumberSupplementText))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.houseNumberSupplementText, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(addressID: String?) -> EntityKey {
        return EntityKey().with(name: "AddressID", value: StringValue.of(optional: addressID))
    }

    open class var nation: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.nation_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.nation_ = value
            }
        }
    }

    open var nation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.nation))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.nation, to: StringValue.of(optional: value))
        }
    }

    open var old: AOutbDeliveryAddressType {
        return CastRequired<AOutbDeliveryAddressType>.from(self.oldEntity)
    }

    open class var person: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.person_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.person_ = value
            }
        }
    }

    open var person: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.person))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.person, to: StringValue.of(optional: value))
        }
    }

    open class var phoneNumber: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.phoneNumber_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.phoneNumber_ = value
            }
        }
    }

    open var phoneNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.phoneNumber))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.phoneNumber, to: StringValue.of(optional: value))
        }
    }

    open class var poBox: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.poBox_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.poBox_ = value
            }
        }
    }

    open var poBox: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.poBox))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.poBox, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxDeviatingCityName: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.poBoxDeviatingCityName_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.poBoxDeviatingCityName_ = value
            }
        }
    }

    open var poBoxDeviatingCityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.poBoxDeviatingCityName))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.poBoxDeviatingCityName, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxDeviatingCountry: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.poBoxDeviatingCountry_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.poBoxDeviatingCountry_ = value
            }
        }
    }

    open var poBoxDeviatingCountry: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.poBoxDeviatingCountry))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.poBoxDeviatingCountry, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxDeviatingRegion: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.poBoxDeviatingRegion_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.poBoxDeviatingRegion_ = value
            }
        }
    }

    open var poBoxDeviatingRegion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.poBoxDeviatingRegion))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.poBoxDeviatingRegion, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxIsWithoutNumber: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.poBoxIsWithoutNumber_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.poBoxIsWithoutNumber_ = value
            }
        }
    }

    open var poBoxIsWithoutNumber: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.poBoxIsWithoutNumber))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.poBoxIsWithoutNumber, to: BooleanValue.of(optional: value))
        }
    }

    open class var poBoxLobbyName: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.poBoxLobbyName_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.poBoxLobbyName_ = value
            }
        }
    }

    open var poBoxLobbyName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.poBoxLobbyName))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.poBoxLobbyName, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxPostalCode: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.poBoxPostalCode_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.poBoxPostalCode_ = value
            }
        }
    }

    open var poBoxPostalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.poBoxPostalCode))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.poBoxPostalCode, to: StringValue.of(optional: value))
        }
    }

    open class var postalCode: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.postalCode_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.postalCode_ = value
            }
        }
    }

    open var postalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.postalCode))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.postalCode, to: StringValue.of(optional: value))
        }
    }

    open class var prfrdCommMediumType: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.prfrdCommMediumType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.prfrdCommMediumType_ = value
            }
        }
    }

    open var prfrdCommMediumType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.prfrdCommMediumType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.prfrdCommMediumType, to: StringValue.of(optional: value))
        }
    }

    open class var region: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.region_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.region_ = value
            }
        }
    }

    open var region: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.region))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.region, to: StringValue.of(optional: value))
        }
    }

    open class var roomNumber: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.roomNumber_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.roomNumber_ = value
            }
        }
    }

    open var roomNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.roomNumber))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.roomNumber, to: StringValue.of(optional: value))
        }
    }

    open class var searchTerm1: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.searchTerm1_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.searchTerm1_ = value
            }
        }
    }

    open var searchTerm1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.searchTerm1))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.searchTerm1, to: StringValue.of(optional: value))
        }
    }

    open class var streetName: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.streetName_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.streetName_ = value
            }
        }
    }

    open var streetName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.streetName))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.streetName, to: StringValue.of(optional: value))
        }
    }

    open class var streetPrefixName: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.streetPrefixName_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.streetPrefixName_ = value
            }
        }
    }

    open var streetPrefixName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.streetPrefixName))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.streetPrefixName, to: StringValue.of(optional: value))
        }
    }

    open class var streetSearch: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.streetSearch_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.streetSearch_ = value
            }
        }
    }

    open var streetSearch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.streetSearch))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.streetSearch, to: StringValue.of(optional: value))
        }
    }

    open class var streetSuffixName: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.streetSuffixName_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.streetSuffixName_ = value
            }
        }
    }

    open var streetSuffixName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.streetSuffixName))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.streetSuffixName, to: StringValue.of(optional: value))
        }
    }

    open class var taxJurisdiction: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.taxJurisdiction_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.taxJurisdiction_ = value
            }
        }
    }

    open var taxJurisdiction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.taxJurisdiction))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.taxJurisdiction, to: StringValue.of(optional: value))
        }
    }

    open class var transportZone: Property {
        get {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                return AOutbDeliveryAddressType.transportZone_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryAddressType.self)
            defer { objc_sync_exit(AOutbDeliveryAddressType.self) }
            do {
                AOutbDeliveryAddressType.transportZone_ = value
            }
        }
    }

    open var transportZone: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryAddressType.transportZone))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryAddressType.transportZone, to: StringValue.of(optional: value))
        }
    }
}
