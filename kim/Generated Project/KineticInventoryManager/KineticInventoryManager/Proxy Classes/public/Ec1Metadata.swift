// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

public class Ec1Metadata {
    private static var document_: CSDLDocument = Ec1Metadata.resolve()

    public static let lock: MetadataLock = MetadataLock()

    public static var document: CSDLDocument {
        get {
            objc_sync_enter(Ec1Metadata.self)
            defer { objc_sync_exit(Ec1Metadata.self) }
            do {
                return Ec1Metadata.document_
            }
        }
        set(value) {
            objc_sync_enter(Ec1Metadata.self)
            defer { objc_sync_exit(Ec1Metadata.self) }
            do {
                Ec1Metadata.document_ = value
            }
        }
    }

    private static func resolve() -> CSDLDocument {
        try! Ec1Factory.registerAll()
        Ec1MetadataParser.parsed.hasGeneratedProxies = true
        return Ec1MetadataParser.parsed
    }

    public class ComplexTypes {
        private static var deliveryMessage_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.DeliveryMessage")

        private static var pickingReport_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.PickingReport")

        private static var putawayReport_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.PutawayReport")

        private static var return_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.Return")

        public static var `return`: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.return_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.return_ = value
                }
            }
        }

        public static var deliveryMessage: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.deliveryMessage_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.deliveryMessage_ = value
                }
            }
        }

        public static var pickingReport: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.pickingReport_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.pickingReport_ = value
                }
            }
        }

        public static var putawayReport: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.putawayReport_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.putawayReport_ = value
                }
            }
        }
    }

    public class EntityTypes {
        private static var aInbDeliveryAddressType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryAddressType")

        private static var aInbDeliveryDocFlowType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryDocFlowType")

        private static var aInbDeliveryHeaderTextType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryHeaderTextType")

        private static var aInbDeliveryHeaderType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryHeaderType")

        private static var aInbDeliveryItemTextType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryItemTextType")

        private static var aInbDeliveryItemType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryItemType")

        private static var aInbDeliveryPartnerType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryPartnerType")

        private static var aInbDeliverySerialNmbrType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliverySerialNmbrType")

        private static var aMaintenanceItemObjListType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_MaintenanceItemObjListType")

        private static var aMaintenanceItemObjectType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_MaintenanceItemObjectType")

        private static var aMaterialDocumentHeaderType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_MaterialDocumentHeaderType")

        private static var aMaterialDocumentItemType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_MaterialDocumentItemType")

        private static var aOutbDeliveryAddressType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_OutbDeliveryAddressType")

        private static var aOutbDeliveryDocFlowType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_OutbDeliveryDocFlowType")

        private static var aOutbDeliveryHeaderType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_OutbDeliveryHeaderType")

        private static var aOutbDeliveryItemType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_OutbDeliveryItemType")

        private static var aOutbDeliveryPartnerType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_OutbDeliveryPartnerType")

        private static var aPlantType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_PlantType")

        private static var aSerialNmbrDeliveryType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_SerialNmbrDeliveryType")

        public static var aInbDeliveryAddressType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryAddressType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryAddressType_ = value
                }
            }
        }

        public static var aInbDeliveryDocFlowType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType_ = value
                }
            }
        }

        public static var aInbDeliveryHeaderTextType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType_ = value
                }
            }
        }

        public static var aInbDeliveryHeaderType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryHeaderType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryHeaderType_ = value
                }
            }
        }

        public static var aInbDeliveryItemTextType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryItemTextType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryItemTextType_ = value
                }
            }
        }

        public static var aInbDeliveryItemType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryItemType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryItemType_ = value
                }
            }
        }

        public static var aInbDeliveryPartnerType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryPartnerType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryPartnerType_ = value
                }
            }
        }

        public static var aInbDeliverySerialNmbrType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType_ = value
                }
            }
        }

        public static var aMaintenanceItemObjListType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aMaintenanceItemObjListType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aMaintenanceItemObjListType_ = value
                }
            }
        }

        public static var aMaintenanceItemObjectType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aMaintenanceItemObjectType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aMaintenanceItemObjectType_ = value
                }
            }
        }

        public static var aMaterialDocumentHeaderType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType_ = value
                }
            }
        }

        public static var aMaterialDocumentItemType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aMaterialDocumentItemType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aMaterialDocumentItemType_ = value
                }
            }
        }

        public static var aOutbDeliveryAddressType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aOutbDeliveryAddressType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aOutbDeliveryAddressType_ = value
                }
            }
        }

        public static var aOutbDeliveryDocFlowType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType_ = value
                }
            }
        }

        public static var aOutbDeliveryHeaderType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType_ = value
                }
            }
        }

        public static var aOutbDeliveryItemType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aOutbDeliveryItemType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aOutbDeliveryItemType_ = value
                }
            }
        }

        public static var aOutbDeliveryPartnerType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType_ = value
                }
            }
        }

        public static var aPlantType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aPlantType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aPlantType_ = value
                }
            }
        }

        public static var aSerialNmbrDeliveryType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType_ = value
                }
            }
        }
    }

    public class EntitySets {
        private static var aInbDeliveryAddressTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryAddressTypeSet")

        private static var aInbDeliveryDocFlowTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryDocFlowTypeSet")

        private static var aInbDeliveryHeader_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryHeader")

        private static var aInbDeliveryHeaderTextTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryHeaderTextTypeSet")

        private static var aInbDeliveryItem_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryItem")

        private static var aInbDeliveryItemTextTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryItemTextTypeSet")

        private static var aInbDeliveryPartner_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryPartner")

        private static var aInbDeliverySerialNmbrTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliverySerialNmbrTypeSet")

        private static var aMaintenanceItemObjListTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_MaintenanceItemObjListTypeSet")

        private static var aMaintenanceItemObjectTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_MaintenanceItemObjectTypeSet")

        private static var aMaterialDocumentHeader_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_MaterialDocumentHeader")

        private static var aMaterialDocumentItemTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_MaterialDocumentItemTypeSet")

        private static var aOutbDeliveryAddressTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_OutbDeliveryAddressTypeSet")

        private static var aOutbDeliveryDocFlowTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_OutbDeliveryDocFlowTypeSet")

        private static var aOutbDeliveryHeader_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_OutbDeliveryHeader")

        private static var aOutbDeliveryItem_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_OutbDeliveryItem")

        private static var aOutbDeliveryPartnerTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_OutbDeliveryPartnerTypeSet")

        private static var aPlant_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_Plant")

        private static var aSerialNmbrDeliveryTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_SerialNmbrDeliveryTypeSet")

        public static var aInbDeliveryAddressTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryAddressTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryAddressTypeSet_ = value
                }
            }
        }

        public static var aInbDeliveryDocFlowTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryDocFlowTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryDocFlowTypeSet_ = value
                }
            }
        }

        public static var aInbDeliveryHeader: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryHeader_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryHeader_ = value
                }
            }
        }

        public static var aInbDeliveryHeaderTextTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryHeaderTextTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryHeaderTextTypeSet_ = value
                }
            }
        }

        public static var aInbDeliveryItem: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryItem_ = value
                }
            }
        }

        public static var aInbDeliveryItemTextTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryItemTextTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryItemTextTypeSet_ = value
                }
            }
        }

        public static var aInbDeliveryPartner: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryPartner_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryPartner_ = value
                }
            }
        }

        public static var aInbDeliverySerialNmbrTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliverySerialNmbrTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliverySerialNmbrTypeSet_ = value
                }
            }
        }

        public static var aMaintenanceItemObjListTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aMaintenanceItemObjListTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aMaintenanceItemObjListTypeSet_ = value
                }
            }
        }

        public static var aMaintenanceItemObjectTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aMaintenanceItemObjectTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aMaintenanceItemObjectTypeSet_ = value
                }
            }
        }

        public static var aMaterialDocumentHeader: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aMaterialDocumentHeader_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aMaterialDocumentHeader_ = value
                }
            }
        }

        public static var aMaterialDocumentItemTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aMaterialDocumentItemTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aMaterialDocumentItemTypeSet_ = value
                }
            }
        }

        public static var aOutbDeliveryAddressTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aOutbDeliveryAddressTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aOutbDeliveryAddressTypeSet_ = value
                }
            }
        }

        public static var aOutbDeliveryDocFlowTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aOutbDeliveryDocFlowTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aOutbDeliveryDocFlowTypeSet_ = value
                }
            }
        }

        public static var aOutbDeliveryHeader: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aOutbDeliveryHeader_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aOutbDeliveryHeader_ = value
                }
            }
        }

        public static var aOutbDeliveryItem: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aOutbDeliveryItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aOutbDeliveryItem_ = value
                }
            }
        }

        public static var aOutbDeliveryPartnerTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aOutbDeliveryPartnerTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aOutbDeliveryPartnerTypeSet_ = value
                }
            }
        }

        public static var aPlant: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aPlant_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aPlant_ = value
                }
            }
        }

        public static var aSerialNmbrDeliveryTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aSerialNmbrDeliveryTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aSerialNmbrDeliveryTypeSet_ = value
                }
            }
        }
    }

    public class ActionImports {
        private static var cancel_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "Cancel")

        private static var cancelItem_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "CancelItem")

        private static var confirmPickingAllItems_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "ConfirmPickingAllItems")

        private static var confirmPickingOneItem_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "ConfirmPickingOneItem")

        private static var confirmPutawayAllItems_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "ConfirmPutawayAllItems")

        private static var confirmPutawayOneItem_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "ConfirmPutawayOneItem")

        private static var pickAllItems_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "PickAllItems")

        private static var pickAndBatchSplitOneItem_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "PickAndBatchSplitOneItem")

        private static var pickOneItem_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "PickOneItem")

        private static var pickOneItemWithBaseQuantity_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "PickOneItemWithBaseQuantity")

        private static var pickOneItemWithSalesQuantity_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "PickOneItemWithSalesQuantity")

        private static var postGoodsIssue_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "PostGoodsIssue")

        private static var postGoodsReceipt_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "PostGoodsReceipt")

        private static var putawayAllItems_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "PutawayAllItems")

        private static var putawayOneItem_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "PutawayOneItem")

        private static var reverseGoodsIssue_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "ReverseGoodsIssue")

        private static var reverseGoodsReceipt_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "ReverseGoodsReceipt")

        private static var setPickingQuantityWithBaseQuantity_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "SetPickingQuantityWithBaseQuantity")

        private static var setPutawayQuantityWithBaseQuantity_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "SetPutawayQuantityWithBaseQuantity")

        public static var cancel: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.cancel_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.cancel_ = value
                }
            }
        }

        public static var cancelItem: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.cancelItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.cancelItem_ = value
                }
            }
        }

        public static var confirmPickingAllItems: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.confirmPickingAllItems_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.confirmPickingAllItems_ = value
                }
            }
        }

        public static var confirmPickingOneItem: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.confirmPickingOneItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.confirmPickingOneItem_ = value
                }
            }
        }

        public static var confirmPutawayAllItems: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.confirmPutawayAllItems_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.confirmPutawayAllItems_ = value
                }
            }
        }

        public static var confirmPutawayOneItem: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.confirmPutawayOneItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.confirmPutawayOneItem_ = value
                }
            }
        }

        public static var pickAllItems: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.pickAllItems_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.pickAllItems_ = value
                }
            }
        }

        public static var pickAndBatchSplitOneItem: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.pickAndBatchSplitOneItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.pickAndBatchSplitOneItem_ = value
                }
            }
        }

        public static var pickOneItem: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.pickOneItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.pickOneItem_ = value
                }
            }
        }

        public static var pickOneItemWithBaseQuantity: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.pickOneItemWithBaseQuantity_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.pickOneItemWithBaseQuantity_ = value
                }
            }
        }

        public static var pickOneItemWithSalesQuantity: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.pickOneItemWithSalesQuantity_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.pickOneItemWithSalesQuantity_ = value
                }
            }
        }

        public static var postGoodsIssue: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.postGoodsIssue_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.postGoodsIssue_ = value
                }
            }
        }

        public static var postGoodsReceipt: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.postGoodsReceipt_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.postGoodsReceipt_ = value
                }
            }
        }

        public static var putawayAllItems: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.putawayAllItems_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.putawayAllItems_ = value
                }
            }
        }

        public static var putawayOneItem: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.putawayOneItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.putawayOneItem_ = value
                }
            }
        }

        public static var reverseGoodsIssue: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.reverseGoodsIssue_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.reverseGoodsIssue_ = value
                }
            }
        }

        public static var reverseGoodsReceipt: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.reverseGoodsReceipt_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.reverseGoodsReceipt_ = value
                }
            }
        }

        public static var setPickingQuantityWithBaseQuantity: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.setPickingQuantityWithBaseQuantity_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.setPickingQuantityWithBaseQuantity_ = value
                }
            }
        }

        public static var setPutawayQuantityWithBaseQuantity: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.setPutawayQuantityWithBaseQuantity_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.setPutawayQuantityWithBaseQuantity_ = value
                }
            }
        }
    }
}
