// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class PutawayReport: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var systemMessageIdentification_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageIdentification")

    private static var systemMessageNumber_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageNumber")

    private static var systemMessageType_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageType")

    private static var systemMessageVariable1_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageVariable1")

    private static var systemMessageVariable2_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageVariable2")

    private static var systemMessageVariable3_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageVariable3")

    private static var systemMessageVariable4_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "SystemMessageVariable4")

    private static var batch_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "Batch")

    private static var deliveryQuantityUnit_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "DeliveryQuantityUnit")

    private static var actualDeliveryQuantity_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "ActualDeliveryQuantity")

    private static var deliveryDocumentItemText_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "DeliveryDocumentItemText")

    private static var material_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "Material")

    private static var deliveryDocumentItem_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "DeliveryDocumentItem")

    private static var deliveryDocument_: Property = Ec1Metadata.ComplexTypes.putawayReport.property(withName: "DeliveryDocument")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.ComplexTypes.putawayReport)
    }

    open class var actualDeliveryQuantity: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.actualDeliveryQuantity_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.actualDeliveryQuantity_ = value
            }
        }
    }

    open var actualDeliveryQuantity: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: PutawayReport.actualDeliveryQuantity))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.actualDeliveryQuantity, to: IntegerValue.of(optional: value))
        }
    }

    open class var batch: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.batch_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.batch_ = value
            }
        }
    }

    open var batch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.batch))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.batch, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> PutawayReport {
        return CastRequired<PutawayReport>.from(self.copyComplex())
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItem: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.deliveryDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.deliveryDocumentItem_ = value
            }
        }
    }

    open var deliveryDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.deliveryDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.deliveryDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItemText: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.deliveryDocumentItemText_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.deliveryDocumentItemText_ = value
            }
        }
    }

    open var deliveryDocumentItemText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.deliveryDocumentItemText))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.deliveryDocumentItemText, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryQuantityUnit: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.deliveryQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.deliveryQuantityUnit_ = value
            }
        }
    }

    open var deliveryQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.deliveryQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.deliveryQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var material: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.material_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.material_ = value
            }
        }
    }

    open var material: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.material))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.material, to: StringValue.of(optional: value))
        }
    }

    open var old: PutawayReport {
        return CastRequired<PutawayReport>.from(self.oldComplex)
    }

    open class var systemMessageIdentification: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.systemMessageIdentification_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.systemMessageIdentification_ = value
            }
        }
    }

    open var systemMessageIdentification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.systemMessageIdentification))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.systemMessageIdentification, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageNumber: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.systemMessageNumber_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.systemMessageNumber_ = value
            }
        }
    }

    open var systemMessageNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.systemMessageNumber))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.systemMessageNumber, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageType: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.systemMessageType_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.systemMessageType_ = value
            }
        }
    }

    open var systemMessageType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.systemMessageType))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.systemMessageType, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageVariable1: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.systemMessageVariable1_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.systemMessageVariable1_ = value
            }
        }
    }

    open var systemMessageVariable1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.systemMessageVariable1))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.systemMessageVariable1, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageVariable2: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.systemMessageVariable2_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.systemMessageVariable2_ = value
            }
        }
    }

    open var systemMessageVariable2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.systemMessageVariable2))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.systemMessageVariable2, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageVariable3: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.systemMessageVariable3_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.systemMessageVariable3_ = value
            }
        }
    }

    open var systemMessageVariable3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.systemMessageVariable3))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.systemMessageVariable3, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageVariable4: Property {
        get {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                return PutawayReport.systemMessageVariable4_
            }
        }
        set(value) {
            objc_sync_enter(PutawayReport.self)
            defer { objc_sync_exit(PutawayReport.self) }
            do {
                PutawayReport.systemMessageVariable4_ = value
            }
        }
    }

    open var systemMessageVariable4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PutawayReport.systemMessageVariable4))
        }
        set(value) {
            self.setOptionalValue(for: PutawayReport.systemMessageVariable4, to: StringValue.of(optional: value))
        }
    }
}
