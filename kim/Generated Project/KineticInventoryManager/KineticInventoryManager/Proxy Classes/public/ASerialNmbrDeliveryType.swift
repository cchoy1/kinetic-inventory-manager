// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class ASerialNmbrDeliveryType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var deliveryDate_: Property = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "DeliveryDate")

    private static var deliveryDocument_: Property = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "DeliveryDocument")

    private static var deliveryDocumentItem_: Property = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "DeliveryDocumentItem")

    private static var maintenanceItemObjectList_: Property = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "MaintenanceItemObjectList")

    private static var sdDocumentCategory_: Property = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "SDDocumentCategory")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType)
    }

    open class func array(from: EntityValueList) -> [ASerialNmbrDeliveryType] {
        return ArrayConverter.convert(from.toArray(), [ASerialNmbrDeliveryType]())
    }

    open func copy() -> ASerialNmbrDeliveryType {
        return CastRequired<ASerialNmbrDeliveryType>.from(self.copyEntity())
    }

    open class var deliveryDate: Property {
        get {
            objc_sync_enter(ASerialNmbrDeliveryType.self)
            defer { objc_sync_exit(ASerialNmbrDeliveryType.self) }
            do {
                return ASerialNmbrDeliveryType.deliveryDate_
            }
        }
        set(value) {
            objc_sync_enter(ASerialNmbrDeliveryType.self)
            defer { objc_sync_exit(ASerialNmbrDeliveryType.self) }
            do {
                ASerialNmbrDeliveryType.deliveryDate_ = value
            }
        }
    }

    open var deliveryDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: ASerialNmbrDeliveryType.deliveryDate))
        }
        set(value) {
            self.setOptionalValue(for: ASerialNmbrDeliveryType.deliveryDate, to: value)
        }
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(ASerialNmbrDeliveryType.self)
            defer { objc_sync_exit(ASerialNmbrDeliveryType.self) }
            do {
                return ASerialNmbrDeliveryType.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(ASerialNmbrDeliveryType.self)
            defer { objc_sync_exit(ASerialNmbrDeliveryType.self) }
            do {
                ASerialNmbrDeliveryType.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ASerialNmbrDeliveryType.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: ASerialNmbrDeliveryType.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItem: Property {
        get {
            objc_sync_enter(ASerialNmbrDeliveryType.self)
            defer { objc_sync_exit(ASerialNmbrDeliveryType.self) }
            do {
                return ASerialNmbrDeliveryType.deliveryDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(ASerialNmbrDeliveryType.self)
            defer { objc_sync_exit(ASerialNmbrDeliveryType.self) }
            do {
                ASerialNmbrDeliveryType.deliveryDocumentItem_ = value
            }
        }
    }

    open var deliveryDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ASerialNmbrDeliveryType.deliveryDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: ASerialNmbrDeliveryType.deliveryDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(maintenanceItemObjectList: Int64?) -> EntityKey {
        return EntityKey().with(name: "MaintenanceItemObjectList", value: LongValue.of(optional: maintenanceItemObjectList))
    }

    open class var maintenanceItemObjectList: Property {
        get {
            objc_sync_enter(ASerialNmbrDeliveryType.self)
            defer { objc_sync_exit(ASerialNmbrDeliveryType.self) }
            do {
                return ASerialNmbrDeliveryType.maintenanceItemObjectList_
            }
        }
        set(value) {
            objc_sync_enter(ASerialNmbrDeliveryType.self)
            defer { objc_sync_exit(ASerialNmbrDeliveryType.self) }
            do {
                ASerialNmbrDeliveryType.maintenanceItemObjectList_ = value
            }
        }
    }

    open var maintenanceItemObjectList: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: ASerialNmbrDeliveryType.maintenanceItemObjectList))
        }
        set(value) {
            self.setOptionalValue(for: ASerialNmbrDeliveryType.maintenanceItemObjectList, to: LongValue.of(optional: value))
        }
    }

    open var old: ASerialNmbrDeliveryType {
        return CastRequired<ASerialNmbrDeliveryType>.from(self.oldEntity)
    }

    open class var sdDocumentCategory: Property {
        get {
            objc_sync_enter(ASerialNmbrDeliveryType.self)
            defer { objc_sync_exit(ASerialNmbrDeliveryType.self) }
            do {
                return ASerialNmbrDeliveryType.sdDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(ASerialNmbrDeliveryType.self)
            defer { objc_sync_exit(ASerialNmbrDeliveryType.self) }
            do {
                ASerialNmbrDeliveryType.sdDocumentCategory_ = value
            }
        }
    }

    open var sdDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ASerialNmbrDeliveryType.sdDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: ASerialNmbrDeliveryType.sdDocumentCategory, to: StringValue.of(optional: value))
        }
    }
}
