// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AOutbDeliveryItemType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var actualDeliveredQtyInBaseUnit_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ActualDeliveredQtyInBaseUnit")

    private static var deliveryVersion_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryVersion")

    private static var actualDeliveryQuantity_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ActualDeliveryQuantity")

    private static var additionalCustomerGroup1_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup1")

    private static var additionalCustomerGroup2_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup2")

    private static var additionalCustomerGroup3_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup3")

    private static var additionalCustomerGroup4_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup4")

    private static var additionalCustomerGroup5_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup5")

    private static var additionalMaterialGroup1_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup1")

    private static var additionalMaterialGroup2_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup2")

    private static var additionalMaterialGroup3_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup3")

    private static var additionalMaterialGroup4_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup4")

    private static var additionalMaterialGroup5_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup5")

    private static var alternateProductNumber_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AlternateProductNumber")

    private static var baseUnit_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BaseUnit")

    private static var batch_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Batch")

    private static var batchBySupplier_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BatchBySupplier")

    private static var batchClassification_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BatchClassification")

    private static var bomExplosion_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BOMExplosion")

    private static var businessArea_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BusinessArea")

    private static var consumptionPosting_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ConsumptionPosting")

    private static var controllingArea_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ControllingArea")

    private static var costCenter_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CostCenter")

    private static var createdByUser_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CreatedByUser")

    private static var creationDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CreationDate")

    private static var creationTime_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CreationTime")

    private static var custEngineeringChgStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CustEngineeringChgStatus")

    private static var deliveryDocument_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryDocument")

    private static var deliveryDocumentItem_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryDocumentItem")

    private static var deliveryDocumentItemCategory_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryDocumentItemCategory")

    private static var deliveryDocumentItemText_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryDocumentItemText")

    private static var deliveryGroup_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryGroup")

    private static var deliveryQuantityUnit_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryQuantityUnit")

    private static var deliveryRelatedBillingStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryRelatedBillingStatus")

    private static var deliveryToBaseQuantityDnmntr_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryToBaseQuantityDnmntr")

    private static var deliveryToBaseQuantityNmrtr_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryToBaseQuantityNmrtr")

    private static var departmentClassificationByCust_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DepartmentClassificationByCust")

    private static var distributionChannel_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DistributionChannel")

    private static var division_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Division")

    private static var fixedShipgProcgDurationInDays_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "FixedShipgProcgDurationInDays")

    private static var glAccount_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "GLAccount")

    private static var goodsMovementReasonCode_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "GoodsMovementReasonCode")

    private static var goodsMovementStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "GoodsMovementStatus")

    private static var goodsMovementType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "GoodsMovementType")

    private static var higherLvlItmOfBatSpltItm_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "HigherLvlItmOfBatSpltItm")

    private static var higherLevelItem_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "HigherLevelItem")

    private static var inspectionLot_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InspectionLot")

    private static var inspectionPartialLot_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InspectionPartialLot")

    private static var intercompanyBillingStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IntercompanyBillingStatus")

    private static var internationalArticleNumber_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InternationalArticleNumber")

    private static var inventorySpecialStockType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InventorySpecialStockType")

    private static var inventoryValuationType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InventoryValuationType")

    private static var isCompletelyDelivered_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IsCompletelyDelivered")

    private static var isNotGoodsMovementsRelevant_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IsNotGoodsMovementsRelevant")

    private static var isSeparateValuation_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IsSeparateValuation")

    private static var issgOrRcvgBatch_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgBatch")

    private static var issgOrRcvgMaterial_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgMaterial")

    private static var issgOrRcvgSpclStockInd_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgSpclStockInd")

    private static var issgOrRcvgStockCategory_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgStockCategory")

    private static var issgOrRcvgValuationType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgValuationType")

    private static var issuingOrReceivingPlant_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssuingOrReceivingPlant")

    private static var issuingOrReceivingStorageLoc_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssuingOrReceivingStorageLoc")

    private static var itemBillingBlockReason_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemBillingBlockReason")

    private static var itemBillingIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemBillingIncompletionStatus")

    private static var itemDeliveryIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemDeliveryIncompletionStatus")

    private static var itemGdsMvtIncompletionSts_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemGdsMvtIncompletionSts")

    private static var itemGeneralIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemGeneralIncompletionStatus")

    private static var itemGrossWeight_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemGrossWeight")

    private static var itemIsBillingRelevant_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemIsBillingRelevant")

    private static var itemNetWeight_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemNetWeight")

    private static var itemPackingIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemPackingIncompletionStatus")

    private static var itemPickingIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemPickingIncompletionStatus")

    private static var itemVolume_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemVolume")

    private static var itemVolumeUnit_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemVolumeUnit")

    private static var itemWeightUnit_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemWeightUnit")

    private static var lastChangeDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "LastChangeDate")

    private static var loadingGroup_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "LoadingGroup")

    private static var manufactureDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ManufactureDate")

    private static var material_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Material")

    private static var materialByCustomer_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialByCustomer")

    private static var materialFreightGroup_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialFreightGroup")

    private static var materialGroup_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialGroup")

    private static var materialIsBatchManaged_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialIsBatchManaged")

    private static var materialIsIntBatchManaged_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialIsIntBatchManaged")

    private static var numberOfSerialNumbers_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "NumberOfSerialNumbers")

    private static var orderID_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OrderID")

    private static var orderItem_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OrderItem")

    private static var originalDeliveryQuantity_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OriginalDeliveryQuantity")

    private static var originallyRequestedMaterial_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OriginallyRequestedMaterial")

    private static var overdelivTolrtdLmtRatioInPct_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OverdelivTolrtdLmtRatioInPct")

    private static var packingStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PackingStatus")

    private static var partialDeliveryIsAllowed_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PartialDeliveryIsAllowed")

    private static var paymentGuaranteeForm_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PaymentGuaranteeForm")

    private static var pickingConfirmationStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PickingConfirmationStatus")

    private static var pickingControl_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PickingControl")

    private static var pickingStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PickingStatus")

    private static var plant_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Plant")

    private static var primaryPostingSwitch_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PrimaryPostingSwitch")

    private static var productAvailabilityDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProductAvailabilityDate")

    private static var productAvailabilityTime_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProductAvailabilityTime")

    private static var productConfiguration_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProductConfiguration")

    private static var productHierarchyNode_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProductHierarchyNode")

    private static var profitabilitySegment_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProfitabilitySegment")

    private static var profitCenter_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProfitCenter")

    private static var proofOfDeliveryRelevanceCode_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProofOfDeliveryRelevanceCode")

    private static var proofOfDeliveryStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProofOfDeliveryStatus")

    private static var quantityIsFixed_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "QuantityIsFixed")

    private static var receivingPoint_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReceivingPoint")

    private static var referenceDocumentLogicalSystem_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReferenceDocumentLogicalSystem")

    private static var referenceSDDocument_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReferenceSDDocument")

    private static var referenceSDDocumentCategory_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReferenceSDDocumentCategory")

    private static var referenceSDDocumentItem_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReferenceSDDocumentItem")

    private static var retailPromotion_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "RetailPromotion")

    private static var salesDocumentItemType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SalesDocumentItemType")

    private static var salesGroup_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SalesGroup")

    private static var salesOffice_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SalesOffice")

    private static var sdDocumentCategory_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SDDocumentCategory")

    private static var sdProcessStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SDProcessStatus")

    private static var shelfLifeExpirationDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ShelfLifeExpirationDate")

    private static var statisticsDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StatisticsDate")

    private static var stockType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StockType")

    private static var storageBin_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StorageBin")

    private static var storageLocation_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StorageLocation")

    private static var storageType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StorageType")

    private static var subsequentMovementType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SubsequentMovementType")

    private static var transportationGroup_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "TransportationGroup")

    private static var underdelivTolrtdLmtRatioInPct_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "UnderdelivTolrtdLmtRatioInPct")

    private static var unlimitedOverdeliveryIsAllowed_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "UnlimitedOverdeliveryIsAllowed")

    private static var varblShipgProcgDurationInDays_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "VarblShipgProcgDurationInDays")

    private static var warehouse_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Warehouse")

    private static var warehouseActivityStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "WarehouseActivityStatus")

    private static var warehouseStagingArea_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "WarehouseStagingArea")

    private static var warehouseStockCategory_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "WarehouseStockCategory")

    private static var warehouseStorageBin_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "WarehouseStorageBin")

    private static var stockSegment_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StockSegment")

    private static var requirementSegment_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "RequirementSegment")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aOutbDeliveryItemType)
    }

    open class var actualDeliveredQtyInBaseUnit: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.actualDeliveredQtyInBaseUnit_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.actualDeliveredQtyInBaseUnit_ = value
            }
        }
    }

    open var actualDeliveredQtyInBaseUnit: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryItemType.actualDeliveredQtyInBaseUnit))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.actualDeliveredQtyInBaseUnit, to: DecimalValue.of(optional: value))
        }
    }

    open class var actualDeliveryQuantity: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.actualDeliveryQuantity_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.actualDeliveryQuantity_ = value
            }
        }
    }

    open var actualDeliveryQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryItemType.actualDeliveryQuantity))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.actualDeliveryQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var additionalCustomerGroup1: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.additionalCustomerGroup1_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.additionalCustomerGroup1_ = value
            }
        }
    }

    open var additionalCustomerGroup1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.additionalCustomerGroup1))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.additionalCustomerGroup1, to: StringValue.of(optional: value))
        }
    }

    open class var additionalCustomerGroup2: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.additionalCustomerGroup2_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.additionalCustomerGroup2_ = value
            }
        }
    }

    open var additionalCustomerGroup2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.additionalCustomerGroup2))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.additionalCustomerGroup2, to: StringValue.of(optional: value))
        }
    }

    open class var additionalCustomerGroup3: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.additionalCustomerGroup3_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.additionalCustomerGroup3_ = value
            }
        }
    }

    open var additionalCustomerGroup3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.additionalCustomerGroup3))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.additionalCustomerGroup3, to: StringValue.of(optional: value))
        }
    }

    open class var additionalCustomerGroup4: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.additionalCustomerGroup4_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.additionalCustomerGroup4_ = value
            }
        }
    }

    open var additionalCustomerGroup4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.additionalCustomerGroup4))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.additionalCustomerGroup4, to: StringValue.of(optional: value))
        }
    }

    open class var additionalCustomerGroup5: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.additionalCustomerGroup5_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.additionalCustomerGroup5_ = value
            }
        }
    }

    open var additionalCustomerGroup5: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.additionalCustomerGroup5))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.additionalCustomerGroup5, to: StringValue.of(optional: value))
        }
    }

    open class var additionalMaterialGroup1: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.additionalMaterialGroup1_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.additionalMaterialGroup1_ = value
            }
        }
    }

    open var additionalMaterialGroup1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.additionalMaterialGroup1))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.additionalMaterialGroup1, to: StringValue.of(optional: value))
        }
    }

    open class var additionalMaterialGroup2: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.additionalMaterialGroup2_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.additionalMaterialGroup2_ = value
            }
        }
    }

    open var additionalMaterialGroup2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.additionalMaterialGroup2))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.additionalMaterialGroup2, to: StringValue.of(optional: value))
        }
    }

    open class var additionalMaterialGroup3: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.additionalMaterialGroup3_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.additionalMaterialGroup3_ = value
            }
        }
    }

    open var additionalMaterialGroup3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.additionalMaterialGroup3))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.additionalMaterialGroup3, to: StringValue.of(optional: value))
        }
    }

    open class var additionalMaterialGroup4: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.additionalMaterialGroup4_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.additionalMaterialGroup4_ = value
            }
        }
    }

    open var additionalMaterialGroup4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.additionalMaterialGroup4))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.additionalMaterialGroup4, to: StringValue.of(optional: value))
        }
    }

    open class var additionalMaterialGroup5: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.additionalMaterialGroup5_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.additionalMaterialGroup5_ = value
            }
        }
    }

    open var additionalMaterialGroup5: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.additionalMaterialGroup5))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.additionalMaterialGroup5, to: StringValue.of(optional: value))
        }
    }

    open class var alternateProductNumber: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.alternateProductNumber_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.alternateProductNumber_ = value
            }
        }
    }

    open var alternateProductNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.alternateProductNumber))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.alternateProductNumber, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [AOutbDeliveryItemType] {
        return ArrayConverter.convert(from.toArray(), [AOutbDeliveryItemType]())
    }

    open class var baseUnit: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.baseUnit_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.baseUnit_ = value
            }
        }
    }

    open var baseUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.baseUnit))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.baseUnit, to: StringValue.of(optional: value))
        }
    }

    open class var batch: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.batch_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.batch_ = value
            }
        }
    }

    open var batch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.batch))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.batch, to: StringValue.of(optional: value))
        }
    }

    open class var batchBySupplier: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.batchBySupplier_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.batchBySupplier_ = value
            }
        }
    }

    open var batchBySupplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.batchBySupplier))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.batchBySupplier, to: StringValue.of(optional: value))
        }
    }

    open class var batchClassification: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.batchClassification_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.batchClassification_ = value
            }
        }
    }

    open var batchClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.batchClassification))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.batchClassification, to: StringValue.of(optional: value))
        }
    }

    open class var bomExplosion: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.bomExplosion_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.bomExplosion_ = value
            }
        }
    }

    open var bomExplosion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.bomExplosion))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.bomExplosion, to: StringValue.of(optional: value))
        }
    }

    open class var businessArea: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.businessArea_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.businessArea_ = value
            }
        }
    }

    open var businessArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.businessArea))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.businessArea, to: StringValue.of(optional: value))
        }
    }

    open class var consumptionPosting: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.consumptionPosting_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.consumptionPosting_ = value
            }
        }
    }

    open var consumptionPosting: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.consumptionPosting))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.consumptionPosting, to: StringValue.of(optional: value))
        }
    }

    open class var controllingArea: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.controllingArea_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.controllingArea_ = value
            }
        }
    }

    open var controllingArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.controllingArea))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.controllingArea, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> AOutbDeliveryItemType {
        return CastRequired<AOutbDeliveryItemType>.from(self.copyEntity())
    }

    open class var costCenter: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.costCenter_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.costCenter_ = value
            }
        }
    }

    open var costCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.costCenter))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.costCenter, to: StringValue.of(optional: value))
        }
    }

    open class var createdByUser: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.createdByUser_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.createdByUser_ = value
            }
        }
    }

    open var createdByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.createdByUser))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.createdByUser, to: StringValue.of(optional: value))
        }
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryItemType.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.creationDate, to: value)
        }
    }

    open class var creationTime: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.creationTime_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.creationTime_ = value
            }
        }
    }

    open var creationTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AOutbDeliveryItemType.creationTime))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.creationTime, to: value)
        }
    }

    open class var custEngineeringChgStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.custEngineeringChgStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.custEngineeringChgStatus_ = value
            }
        }
    }

    open var custEngineeringChgStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.custEngineeringChgStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.custEngineeringChgStatus, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItem: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.deliveryDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.deliveryDocumentItem_ = value
            }
        }
    }

    open var deliveryDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.deliveryDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.deliveryDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItemCategory: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.deliveryDocumentItemCategory_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.deliveryDocumentItemCategory_ = value
            }
        }
    }

    open var deliveryDocumentItemCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.deliveryDocumentItemCategory))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.deliveryDocumentItemCategory, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItemText: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.deliveryDocumentItemText_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.deliveryDocumentItemText_ = value
            }
        }
    }

    open var deliveryDocumentItemText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.deliveryDocumentItemText))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.deliveryDocumentItemText, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryGroup: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.deliveryGroup_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.deliveryGroup_ = value
            }
        }
    }

    open var deliveryGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.deliveryGroup))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.deliveryGroup, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryQuantityUnit: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.deliveryQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.deliveryQuantityUnit_ = value
            }
        }
    }

    open var deliveryQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.deliveryQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.deliveryQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryRelatedBillingStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.deliveryRelatedBillingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.deliveryRelatedBillingStatus_ = value
            }
        }
    }

    open var deliveryRelatedBillingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.deliveryRelatedBillingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.deliveryRelatedBillingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryToBaseQuantityDnmntr: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.deliveryToBaseQuantityDnmntr_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.deliveryToBaseQuantityDnmntr_ = value
            }
        }
    }

    open var deliveryToBaseQuantityDnmntr: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: AOutbDeliveryItemType.deliveryToBaseQuantityDnmntr))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.deliveryToBaseQuantityDnmntr, to: IntegerValue.of(optional: value))
        }
    }

    open class var deliveryToBaseQuantityNmrtr: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.deliveryToBaseQuantityNmrtr_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.deliveryToBaseQuantityNmrtr_ = value
            }
        }
    }

    open var deliveryToBaseQuantityNmrtr: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: AOutbDeliveryItemType.deliveryToBaseQuantityNmrtr))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.deliveryToBaseQuantityNmrtr, to: IntegerValue.of(optional: value))
        }
    }

    open class var deliveryVersion: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.deliveryVersion_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.deliveryVersion_ = value
            }
        }
    }

    open var deliveryVersion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.deliveryVersion))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.deliveryVersion, to: StringValue.of(optional: value))
        }
    }

    open class var departmentClassificationByCust: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.departmentClassificationByCust_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.departmentClassificationByCust_ = value
            }
        }
    }

    open var departmentClassificationByCust: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.departmentClassificationByCust))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.departmentClassificationByCust, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var division: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.division_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.division_ = value
            }
        }
    }

    open var division: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.division))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.division, to: StringValue.of(optional: value))
        }
    }

    open class var fixedShipgProcgDurationInDays: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.fixedShipgProcgDurationInDays_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.fixedShipgProcgDurationInDays_ = value
            }
        }
    }

    open var fixedShipgProcgDurationInDays: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryItemType.fixedShipgProcgDurationInDays))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.fixedShipgProcgDurationInDays, to: DecimalValue.of(optional: value))
        }
    }

    open class var glAccount: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.glAccount_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.glAccount_ = value
            }
        }
    }

    open var glAccount: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.glAccount))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.glAccount, to: StringValue.of(optional: value))
        }
    }

    open class var goodsMovementReasonCode: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.goodsMovementReasonCode_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.goodsMovementReasonCode_ = value
            }
        }
    }

    open var goodsMovementReasonCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.goodsMovementReasonCode))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.goodsMovementReasonCode, to: StringValue.of(optional: value))
        }
    }

    open class var goodsMovementStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.goodsMovementStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.goodsMovementStatus_ = value
            }
        }
    }

    open var goodsMovementStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.goodsMovementStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.goodsMovementStatus, to: StringValue.of(optional: value))
        }
    }

    open class var goodsMovementType: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.goodsMovementType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.goodsMovementType_ = value
            }
        }
    }

    open var goodsMovementType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.goodsMovementType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.goodsMovementType, to: StringValue.of(optional: value))
        }
    }

    open class var higherLevelItem: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.higherLevelItem_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.higherLevelItem_ = value
            }
        }
    }

    open var higherLevelItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.higherLevelItem))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.higherLevelItem, to: StringValue.of(optional: value))
        }
    }

    open class var higherLvlItmOfBatSpltItm: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.higherLvlItmOfBatSpltItm_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.higherLvlItmOfBatSpltItm_ = value
            }
        }
    }

    open var higherLvlItmOfBatSpltItm: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.higherLvlItmOfBatSpltItm))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.higherLvlItmOfBatSpltItm, to: StringValue.of(optional: value))
        }
    }

    open class var inspectionLot: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.inspectionLot_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.inspectionLot_ = value
            }
        }
    }

    open var inspectionLot: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.inspectionLot))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.inspectionLot, to: StringValue.of(optional: value))
        }
    }

    open class var inspectionPartialLot: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.inspectionPartialLot_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.inspectionPartialLot_ = value
            }
        }
    }

    open var inspectionPartialLot: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.inspectionPartialLot))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.inspectionPartialLot, to: StringValue.of(optional: value))
        }
    }

    open class var intercompanyBillingStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.intercompanyBillingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.intercompanyBillingStatus_ = value
            }
        }
    }

    open var intercompanyBillingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.intercompanyBillingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.intercompanyBillingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var internationalArticleNumber: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.internationalArticleNumber_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.internationalArticleNumber_ = value
            }
        }
    }

    open var internationalArticleNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.internationalArticleNumber))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.internationalArticleNumber, to: StringValue.of(optional: value))
        }
    }

    open class var inventorySpecialStockType: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.inventorySpecialStockType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.inventorySpecialStockType_ = value
            }
        }
    }

    open var inventorySpecialStockType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.inventorySpecialStockType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.inventorySpecialStockType, to: StringValue.of(optional: value))
        }
    }

    open class var inventoryValuationType: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.inventoryValuationType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.inventoryValuationType_ = value
            }
        }
    }

    open var inventoryValuationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.inventoryValuationType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.inventoryValuationType, to: StringValue.of(optional: value))
        }
    }

    open class var isCompletelyDelivered: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.isCompletelyDelivered_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.isCompletelyDelivered_ = value
            }
        }
    }

    open var isCompletelyDelivered: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AOutbDeliveryItemType.isCompletelyDelivered))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.isCompletelyDelivered, to: BooleanValue.of(optional: value))
        }
    }

    open class var isNotGoodsMovementsRelevant: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.isNotGoodsMovementsRelevant_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.isNotGoodsMovementsRelevant_ = value
            }
        }
    }

    open var isNotGoodsMovementsRelevant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.isNotGoodsMovementsRelevant))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.isNotGoodsMovementsRelevant, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var isSeparateValuation: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.isSeparateValuation_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.isSeparateValuation_ = value
            }
        }
    }

    open var isSeparateValuation: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AOutbDeliveryItemType.isSeparateValuation))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.isSeparateValuation, to: BooleanValue.of(optional: value))
        }
    }

    open class var issgOrRcvgBatch: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.issgOrRcvgBatch_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.issgOrRcvgBatch_ = value
            }
        }
    }

    open var issgOrRcvgBatch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.issgOrRcvgBatch))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.issgOrRcvgBatch, to: StringValue.of(optional: value))
        }
    }

    open class var issgOrRcvgMaterial: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.issgOrRcvgMaterial_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.issgOrRcvgMaterial_ = value
            }
        }
    }

    open var issgOrRcvgMaterial: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.issgOrRcvgMaterial))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.issgOrRcvgMaterial, to: StringValue.of(optional: value))
        }
    }

    open class var issgOrRcvgSpclStockInd: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.issgOrRcvgSpclStockInd_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.issgOrRcvgSpclStockInd_ = value
            }
        }
    }

    open var issgOrRcvgSpclStockInd: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.issgOrRcvgSpclStockInd))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.issgOrRcvgSpclStockInd, to: StringValue.of(optional: value))
        }
    }

    open class var issgOrRcvgStockCategory: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.issgOrRcvgStockCategory_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.issgOrRcvgStockCategory_ = value
            }
        }
    }

    open var issgOrRcvgStockCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.issgOrRcvgStockCategory))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.issgOrRcvgStockCategory, to: StringValue.of(optional: value))
        }
    }

    open class var issgOrRcvgValuationType: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.issgOrRcvgValuationType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.issgOrRcvgValuationType_ = value
            }
        }
    }

    open var issgOrRcvgValuationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.issgOrRcvgValuationType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.issgOrRcvgValuationType, to: StringValue.of(optional: value))
        }
    }

    open class var issuingOrReceivingPlant: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.issuingOrReceivingPlant_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.issuingOrReceivingPlant_ = value
            }
        }
    }

    open var issuingOrReceivingPlant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.issuingOrReceivingPlant))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.issuingOrReceivingPlant, to: StringValue.of(optional: value))
        }
    }

    open class var issuingOrReceivingStorageLoc: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.issuingOrReceivingStorageLoc_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.issuingOrReceivingStorageLoc_ = value
            }
        }
    }

    open var issuingOrReceivingStorageLoc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.issuingOrReceivingStorageLoc))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.issuingOrReceivingStorageLoc, to: StringValue.of(optional: value))
        }
    }

    open class var itemBillingBlockReason: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemBillingBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemBillingBlockReason_ = value
            }
        }
    }

    open var itemBillingBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemBillingBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemBillingBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var itemBillingIncompletionStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemBillingIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemBillingIncompletionStatus_ = value
            }
        }
    }

    open var itemBillingIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemBillingIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemBillingIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var itemDeliveryIncompletionStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemDeliveryIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemDeliveryIncompletionStatus_ = value
            }
        }
    }

    open var itemDeliveryIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemDeliveryIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemDeliveryIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var itemGdsMvtIncompletionSts: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemGdsMvtIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemGdsMvtIncompletionSts_ = value
            }
        }
    }

    open var itemGdsMvtIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemGdsMvtIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemGdsMvtIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var itemGeneralIncompletionStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemGeneralIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemGeneralIncompletionStatus_ = value
            }
        }
    }

    open var itemGeneralIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemGeneralIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemGeneralIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var itemGrossWeight: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemGrossWeight_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemGrossWeight_ = value
            }
        }
    }

    open var itemGrossWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemGrossWeight))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemGrossWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemIsBillingRelevant: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemIsBillingRelevant_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemIsBillingRelevant_ = value
            }
        }
    }

    open var itemIsBillingRelevant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemIsBillingRelevant))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemIsBillingRelevant, to: StringValue.of(optional: value))
        }
    }

    open class var itemNetWeight: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemNetWeight_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemNetWeight_ = value
            }
        }
    }

    open var itemNetWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemNetWeight))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemNetWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemPackingIncompletionStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemPackingIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemPackingIncompletionStatus_ = value
            }
        }
    }

    open var itemPackingIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemPackingIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemPackingIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var itemPickingIncompletionStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemPickingIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemPickingIncompletionStatus_ = value
            }
        }
    }

    open var itemPickingIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemPickingIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemPickingIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var itemVolume: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemVolume_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemVolume_ = value
            }
        }
    }

    open var itemVolume: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemVolume))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemVolume, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemVolumeUnit: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemVolumeUnit_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemVolumeUnit_ = value
            }
        }
    }

    open var itemVolumeUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemVolumeUnit))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemVolumeUnit, to: StringValue.of(optional: value))
        }
    }

    open class var itemWeightUnit: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.itemWeightUnit_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.itemWeightUnit_ = value
            }
        }
    }

    open var itemWeightUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.itemWeightUnit))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.itemWeightUnit, to: StringValue.of(optional: value))
        }
    }

    open class func key(deliveryDocument: String?, deliveryDocumentItem: String?) -> EntityKey {
        return EntityKey().with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem))
    }

    open class var lastChangeDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.lastChangeDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.lastChangeDate_ = value
            }
        }
    }

    open var lastChangeDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryItemType.lastChangeDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.lastChangeDate, to: value)
        }
    }

    open class var loadingGroup: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.loadingGroup_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.loadingGroup_ = value
            }
        }
    }

    open var loadingGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.loadingGroup))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.loadingGroup, to: StringValue.of(optional: value))
        }
    }

    open class var manufactureDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.manufactureDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.manufactureDate_ = value
            }
        }
    }

    open var manufactureDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryItemType.manufactureDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.manufactureDate, to: value)
        }
    }

    open class var material: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.material_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.material_ = value
            }
        }
    }

    open var material: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.material))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.material, to: StringValue.of(optional: value))
        }
    }

    open class var materialByCustomer: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.materialByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.materialByCustomer_ = value
            }
        }
    }

    open var materialByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.materialByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.materialByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var materialFreightGroup: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.materialFreightGroup_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.materialFreightGroup_ = value
            }
        }
    }

    open var materialFreightGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.materialFreightGroup))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.materialFreightGroup, to: StringValue.of(optional: value))
        }
    }

    open class var materialGroup: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.materialGroup_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.materialGroup_ = value
            }
        }
    }

    open var materialGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.materialGroup))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.materialGroup, to: StringValue.of(optional: value))
        }
    }

    open class var materialIsBatchManaged: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.materialIsBatchManaged_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.materialIsBatchManaged_ = value
            }
        }
    }

    open var materialIsBatchManaged: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AOutbDeliveryItemType.materialIsBatchManaged))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.materialIsBatchManaged, to: BooleanValue.of(optional: value))
        }
    }

    open class var materialIsIntBatchManaged: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.materialIsIntBatchManaged_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.materialIsIntBatchManaged_ = value
            }
        }
    }

    open var materialIsIntBatchManaged: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AOutbDeliveryItemType.materialIsIntBatchManaged))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.materialIsIntBatchManaged, to: BooleanValue.of(optional: value))
        }
    }

    open class var numberOfSerialNumbers: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.numberOfSerialNumbers_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.numberOfSerialNumbers_ = value
            }
        }
    }

    open var numberOfSerialNumbers: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: AOutbDeliveryItemType.numberOfSerialNumbers))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.numberOfSerialNumbers, to: IntValue.of(optional: value))
        }
    }

    open var old: AOutbDeliveryItemType {
        return CastRequired<AOutbDeliveryItemType>.from(self.oldEntity)
    }

    open class var orderID: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.orderID_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.orderID_ = value
            }
        }
    }

    open var orderID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.orderID))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.orderID, to: StringValue.of(optional: value))
        }
    }

    open class var orderItem: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.orderItem_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.orderItem_ = value
            }
        }
    }

    open var orderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.orderItem))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.orderItem, to: StringValue.of(optional: value))
        }
    }

    open class var originalDeliveryQuantity: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.originalDeliveryQuantity_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.originalDeliveryQuantity_ = value
            }
        }
    }

    open var originalDeliveryQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryItemType.originalDeliveryQuantity))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.originalDeliveryQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var originallyRequestedMaterial: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.originallyRequestedMaterial_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.originallyRequestedMaterial_ = value
            }
        }
    }

    open var originallyRequestedMaterial: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.originallyRequestedMaterial))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.originallyRequestedMaterial, to: StringValue.of(optional: value))
        }
    }

    open class var overdelivTolrtdLmtRatioInPct: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.overdelivTolrtdLmtRatioInPct_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.overdelivTolrtdLmtRatioInPct_ = value
            }
        }
    }

    open var overdelivTolrtdLmtRatioInPct: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryItemType.overdelivTolrtdLmtRatioInPct))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.overdelivTolrtdLmtRatioInPct, to: DecimalValue.of(optional: value))
        }
    }

    open class var packingStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.packingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.packingStatus_ = value
            }
        }
    }

    open var packingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.packingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.packingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var partialDeliveryIsAllowed: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.partialDeliveryIsAllowed_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.partialDeliveryIsAllowed_ = value
            }
        }
    }

    open var partialDeliveryIsAllowed: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.partialDeliveryIsAllowed))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.partialDeliveryIsAllowed, to: StringValue.of(optional: value))
        }
    }

    open class var paymentGuaranteeForm: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.paymentGuaranteeForm_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.paymentGuaranteeForm_ = value
            }
        }
    }

    open var paymentGuaranteeForm: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.paymentGuaranteeForm))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.paymentGuaranteeForm, to: StringValue.of(optional: value))
        }
    }

    open class var pickingConfirmationStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.pickingConfirmationStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.pickingConfirmationStatus_ = value
            }
        }
    }

    open var pickingConfirmationStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.pickingConfirmationStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.pickingConfirmationStatus, to: StringValue.of(optional: value))
        }
    }

    open class var pickingControl: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.pickingControl_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.pickingControl_ = value
            }
        }
    }

    open var pickingControl: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.pickingControl))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.pickingControl, to: StringValue.of(optional: value))
        }
    }

    open class var pickingStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.pickingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.pickingStatus_ = value
            }
        }
    }

    open var pickingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.pickingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.pickingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var plant: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.plant_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.plant_ = value
            }
        }
    }

    open var plant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.plant))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.plant, to: StringValue.of(optional: value))
        }
    }

    open class var primaryPostingSwitch: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.primaryPostingSwitch_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.primaryPostingSwitch_ = value
            }
        }
    }

    open var primaryPostingSwitch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.primaryPostingSwitch))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.primaryPostingSwitch, to: StringValue.of(optional: value))
        }
    }

    open class var productAvailabilityDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.productAvailabilityDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.productAvailabilityDate_ = value
            }
        }
    }

    open var productAvailabilityDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryItemType.productAvailabilityDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.productAvailabilityDate, to: value)
        }
    }

    open class var productAvailabilityTime: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.productAvailabilityTime_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.productAvailabilityTime_ = value
            }
        }
    }

    open var productAvailabilityTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AOutbDeliveryItemType.productAvailabilityTime))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.productAvailabilityTime, to: value)
        }
    }

    open class var productConfiguration: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.productConfiguration_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.productConfiguration_ = value
            }
        }
    }

    open var productConfiguration: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.productConfiguration))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.productConfiguration, to: StringValue.of(optional: value))
        }
    }

    open class var productHierarchyNode: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.productHierarchyNode_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.productHierarchyNode_ = value
            }
        }
    }

    open var productHierarchyNode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.productHierarchyNode))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.productHierarchyNode, to: StringValue.of(optional: value))
        }
    }

    open class var profitCenter: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.profitCenter_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.profitCenter_ = value
            }
        }
    }

    open var profitCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.profitCenter))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.profitCenter, to: StringValue.of(optional: value))
        }
    }

    open class var profitabilitySegment: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.profitabilitySegment_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.profitabilitySegment_ = value
            }
        }
    }

    open var profitabilitySegment: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.profitabilitySegment))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.profitabilitySegment, to: StringValue.of(optional: value))
        }
    }

    open class var proofOfDeliveryRelevanceCode: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.proofOfDeliveryRelevanceCode_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.proofOfDeliveryRelevanceCode_ = value
            }
        }
    }

    open var proofOfDeliveryRelevanceCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.proofOfDeliveryRelevanceCode))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.proofOfDeliveryRelevanceCode, to: StringValue.of(optional: value))
        }
    }

    open class var proofOfDeliveryStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.proofOfDeliveryStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.proofOfDeliveryStatus_ = value
            }
        }
    }

    open var proofOfDeliveryStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.proofOfDeliveryStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.proofOfDeliveryStatus, to: StringValue.of(optional: value))
        }
    }

    open class var quantityIsFixed: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.quantityIsFixed_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.quantityIsFixed_ = value
            }
        }
    }

    open var quantityIsFixed: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AOutbDeliveryItemType.quantityIsFixed))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.quantityIsFixed, to: BooleanValue.of(optional: value))
        }
    }

    open class var receivingPoint: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.receivingPoint_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.receivingPoint_ = value
            }
        }
    }

    open var receivingPoint: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.receivingPoint))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.receivingPoint, to: StringValue.of(optional: value))
        }
    }

    open class var referenceDocumentLogicalSystem: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.referenceDocumentLogicalSystem_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.referenceDocumentLogicalSystem_ = value
            }
        }
    }

    open var referenceDocumentLogicalSystem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.referenceDocumentLogicalSystem))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.referenceDocumentLogicalSystem, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocument: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.referenceSDDocument_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.referenceSDDocument_ = value
            }
        }
    }

    open var referenceSDDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.referenceSDDocument))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.referenceSDDocument, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocumentCategory: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.referenceSDDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.referenceSDDocumentCategory_ = value
            }
        }
    }

    open var referenceSDDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.referenceSDDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.referenceSDDocumentCategory, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocumentItem: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.referenceSDDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.referenceSDDocumentItem_ = value
            }
        }
    }

    open var referenceSDDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.referenceSDDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.referenceSDDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var requirementSegment: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.requirementSegment_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.requirementSegment_ = value
            }
        }
    }

    open var requirementSegment: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.requirementSegment))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.requirementSegment, to: StringValue.of(optional: value))
        }
    }

    open class var retailPromotion: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.retailPromotion_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.retailPromotion_ = value
            }
        }
    }

    open var retailPromotion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.retailPromotion))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.retailPromotion, to: StringValue.of(optional: value))
        }
    }

    open class var salesDocumentItemType: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.salesDocumentItemType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.salesDocumentItemType_ = value
            }
        }
    }

    open var salesDocumentItemType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.salesDocumentItemType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.salesDocumentItemType, to: StringValue.of(optional: value))
        }
    }

    open class var salesGroup: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.salesGroup_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.salesGroup_ = value
            }
        }
    }

    open var salesGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.salesGroup))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.salesGroup, to: StringValue.of(optional: value))
        }
    }

    open class var salesOffice: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.salesOffice_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.salesOffice_ = value
            }
        }
    }

    open var salesOffice: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.salesOffice))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.salesOffice, to: StringValue.of(optional: value))
        }
    }

    open class var sdDocumentCategory: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.sdDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.sdDocumentCategory_ = value
            }
        }
    }

    open var sdDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.sdDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.sdDocumentCategory, to: StringValue.of(optional: value))
        }
    }

    open class var sdProcessStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.sdProcessStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.sdProcessStatus_ = value
            }
        }
    }

    open var sdProcessStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.sdProcessStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.sdProcessStatus, to: StringValue.of(optional: value))
        }
    }

    open class var shelfLifeExpirationDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.shelfLifeExpirationDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.shelfLifeExpirationDate_ = value
            }
        }
    }

    open var shelfLifeExpirationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryItemType.shelfLifeExpirationDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.shelfLifeExpirationDate, to: value)
        }
    }

    open class var statisticsDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.statisticsDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.statisticsDate_ = value
            }
        }
    }

    open var statisticsDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryItemType.statisticsDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.statisticsDate, to: value)
        }
    }

    open class var stockSegment: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.stockSegment_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.stockSegment_ = value
            }
        }
    }

    open var stockSegment: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.stockSegment))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.stockSegment, to: StringValue.of(optional: value))
        }
    }

    open class var stockType: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.stockType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.stockType_ = value
            }
        }
    }

    open var stockType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.stockType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.stockType, to: StringValue.of(optional: value))
        }
    }

    open class var storageBin: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.storageBin_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.storageBin_ = value
            }
        }
    }

    open var storageBin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.storageBin))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.storageBin, to: StringValue.of(optional: value))
        }
    }

    open class var storageLocation: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.storageLocation_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.storageLocation_ = value
            }
        }
    }

    open var storageLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.storageLocation))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.storageLocation, to: StringValue.of(optional: value))
        }
    }

    open class var storageType: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.storageType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.storageType_ = value
            }
        }
    }

    open var storageType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.storageType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.storageType, to: StringValue.of(optional: value))
        }
    }

    open class var subsequentMovementType: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.subsequentMovementType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.subsequentMovementType_ = value
            }
        }
    }

    open var subsequentMovementType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.subsequentMovementType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.subsequentMovementType, to: StringValue.of(optional: value))
        }
    }

    open class var transportationGroup: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.transportationGroup_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.transportationGroup_ = value
            }
        }
    }

    open var transportationGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.transportationGroup))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.transportationGroup, to: StringValue.of(optional: value))
        }
    }

    open class var underdelivTolrtdLmtRatioInPct: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.underdelivTolrtdLmtRatioInPct_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.underdelivTolrtdLmtRatioInPct_ = value
            }
        }
    }

    open var underdelivTolrtdLmtRatioInPct: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryItemType.underdelivTolrtdLmtRatioInPct))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.underdelivTolrtdLmtRatioInPct, to: DecimalValue.of(optional: value))
        }
    }

    open class var unlimitedOverdeliveryIsAllowed: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.unlimitedOverdeliveryIsAllowed_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.unlimitedOverdeliveryIsAllowed_ = value
            }
        }
    }

    open var unlimitedOverdeliveryIsAllowed: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AOutbDeliveryItemType.unlimitedOverdeliveryIsAllowed))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.unlimitedOverdeliveryIsAllowed, to: BooleanValue.of(optional: value))
        }
    }

    open class var varblShipgProcgDurationInDays: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.varblShipgProcgDurationInDays_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.varblShipgProcgDurationInDays_ = value
            }
        }
    }

    open var varblShipgProcgDurationInDays: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryItemType.varblShipgProcgDurationInDays))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.varblShipgProcgDurationInDays, to: DecimalValue.of(optional: value))
        }
    }

    open class var warehouse: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.warehouse_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.warehouse_ = value
            }
        }
    }

    open var warehouse: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.warehouse))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.warehouse, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseActivityStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.warehouseActivityStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.warehouseActivityStatus_ = value
            }
        }
    }

    open var warehouseActivityStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.warehouseActivityStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.warehouseActivityStatus, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseStagingArea: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.warehouseStagingArea_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.warehouseStagingArea_ = value
            }
        }
    }

    open var warehouseStagingArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.warehouseStagingArea))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.warehouseStagingArea, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseStockCategory: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.warehouseStockCategory_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.warehouseStockCategory_ = value
            }
        }
    }

    open var warehouseStockCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.warehouseStockCategory))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.warehouseStockCategory, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseStorageBin: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                return AOutbDeliveryItemType.warehouseStorageBin_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemType.self)
            defer { objc_sync_exit(AOutbDeliveryItemType.self) }
            do {
                AOutbDeliveryItemType.warehouseStorageBin_ = value
            }
        }
    }

    open var warehouseStorageBin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemType.warehouseStorageBin))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemType.warehouseStorageBin, to: StringValue.of(optional: value))
        }
    }
}
