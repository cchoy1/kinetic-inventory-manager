// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AInbDeliverySerialNmbrType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var deliveryDate_: Property = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "DeliveryDate")

    private static var deliveryDocument_: Property = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "DeliveryDocument")

    private static var deliveryDocumentItem_: Property = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "DeliveryDocumentItem")

    private static var maintenanceItemObjectList_: Property = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "MaintenanceItemObjectList")

    private static var sdDocumentCategory_: Property = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "SDDocumentCategory")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType)
    }

    open class func array(from: EntityValueList) -> [AInbDeliverySerialNmbrType] {
        return ArrayConverter.convert(from.toArray(), [AInbDeliverySerialNmbrType]())
    }

    open func copy() -> AInbDeliverySerialNmbrType {
        return CastRequired<AInbDeliverySerialNmbrType>.from(self.copyEntity())
    }

    open class var deliveryDate: Property {
        get {
            objc_sync_enter(AInbDeliverySerialNmbrType.self)
            defer { objc_sync_exit(AInbDeliverySerialNmbrType.self) }
            do {
                return AInbDeliverySerialNmbrType.deliveryDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliverySerialNmbrType.self)
            defer { objc_sync_exit(AInbDeliverySerialNmbrType.self) }
            do {
                AInbDeliverySerialNmbrType.deliveryDate_ = value
            }
        }
    }

    open var deliveryDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliverySerialNmbrType.deliveryDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliverySerialNmbrType.deliveryDate, to: value)
        }
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(AInbDeliverySerialNmbrType.self)
            defer { objc_sync_exit(AInbDeliverySerialNmbrType.self) }
            do {
                return AInbDeliverySerialNmbrType.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliverySerialNmbrType.self)
            defer { objc_sync_exit(AInbDeliverySerialNmbrType.self) }
            do {
                AInbDeliverySerialNmbrType.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliverySerialNmbrType.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliverySerialNmbrType.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItem: Property {
        get {
            objc_sync_enter(AInbDeliverySerialNmbrType.self)
            defer { objc_sync_exit(AInbDeliverySerialNmbrType.self) }
            do {
                return AInbDeliverySerialNmbrType.deliveryDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliverySerialNmbrType.self)
            defer { objc_sync_exit(AInbDeliverySerialNmbrType.self) }
            do {
                AInbDeliverySerialNmbrType.deliveryDocumentItem_ = value
            }
        }
    }

    open var deliveryDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliverySerialNmbrType.deliveryDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliverySerialNmbrType.deliveryDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(maintenanceItemObjectList: Int64?) -> EntityKey {
        return EntityKey().with(name: "MaintenanceItemObjectList", value: LongValue.of(optional: maintenanceItemObjectList))
    }

    open class var maintenanceItemObjectList: Property {
        get {
            objc_sync_enter(AInbDeliverySerialNmbrType.self)
            defer { objc_sync_exit(AInbDeliverySerialNmbrType.self) }
            do {
                return AInbDeliverySerialNmbrType.maintenanceItemObjectList_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliverySerialNmbrType.self)
            defer { objc_sync_exit(AInbDeliverySerialNmbrType.self) }
            do {
                AInbDeliverySerialNmbrType.maintenanceItemObjectList_ = value
            }
        }
    }

    open var maintenanceItemObjectList: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: AInbDeliverySerialNmbrType.maintenanceItemObjectList))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliverySerialNmbrType.maintenanceItemObjectList, to: LongValue.of(optional: value))
        }
    }

    open var old: AInbDeliverySerialNmbrType {
        return CastRequired<AInbDeliverySerialNmbrType>.from(self.oldEntity)
    }

    open class var sdDocumentCategory: Property {
        get {
            objc_sync_enter(AInbDeliverySerialNmbrType.self)
            defer { objc_sync_exit(AInbDeliverySerialNmbrType.self) }
            do {
                return AInbDeliverySerialNmbrType.sdDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliverySerialNmbrType.self)
            defer { objc_sync_exit(AInbDeliverySerialNmbrType.self) }
            do {
                AInbDeliverySerialNmbrType.sdDocumentCategory_ = value
            }
        }
    }

    open var sdDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliverySerialNmbrType.sdDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliverySerialNmbrType.sdDocumentCategory, to: StringValue.of(optional: value))
        }
    }
}
