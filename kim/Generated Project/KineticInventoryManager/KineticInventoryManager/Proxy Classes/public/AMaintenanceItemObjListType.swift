// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AMaintenanceItemObjListType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var assembly_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "Assembly")

    private static var equipment_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "Equipment")

    private static var functionalLocation_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "FunctionalLocation")

    private static var maintenanceItemObject_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "MaintenanceItemObject")

    private static var maintenanceItemObjectList_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "MaintenanceItemObjectList")

    private static var maintenanceNotification_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "MaintenanceNotification")

    private static var maintObjectLocAcctAssgmtNmbr_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "MaintObjectLocAcctAssgmtNmbr")

    private static var material_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "Material")

    private static var serialNumber_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "SerialNumber")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aMaintenanceItemObjListType)
    }

    open class func array(from: EntityValueList) -> [AMaintenanceItemObjListType] {
        return ArrayConverter.convert(from.toArray(), [AMaintenanceItemObjListType]())
    }

    open class var assembly: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                return AMaintenanceItemObjListType.assembly_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                AMaintenanceItemObjListType.assembly_ = value
            }
        }
    }

    open var assembly: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjListType.assembly))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjListType.assembly, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> AMaintenanceItemObjListType {
        return CastRequired<AMaintenanceItemObjListType>.from(self.copyEntity())
    }

    open class var equipment: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                return AMaintenanceItemObjListType.equipment_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                AMaintenanceItemObjListType.equipment_ = value
            }
        }
    }

    open var equipment: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjListType.equipment))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjListType.equipment, to: StringValue.of(optional: value))
        }
    }

    open class var functionalLocation: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                return AMaintenanceItemObjListType.functionalLocation_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                AMaintenanceItemObjListType.functionalLocation_ = value
            }
        }
    }

    open var functionalLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjListType.functionalLocation))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjListType.functionalLocation, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(maintenanceItemObject: Int?, maintenanceItemObjectList: Int64?) -> EntityKey {
        return EntityKey().with(name: "MaintenanceItemObject", value: IntValue.of(optional: maintenanceItemObject)).with(name: "MaintenanceItemObjectList", value: LongValue.of(optional: maintenanceItemObjectList))
    }

    open class var maintObjectLocAcctAssgmtNmbr: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                return AMaintenanceItemObjListType.maintObjectLocAcctAssgmtNmbr_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                AMaintenanceItemObjListType.maintObjectLocAcctAssgmtNmbr_ = value
            }
        }
    }

    open var maintObjectLocAcctAssgmtNmbr: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjListType.maintObjectLocAcctAssgmtNmbr))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjListType.maintObjectLocAcctAssgmtNmbr, to: StringValue.of(optional: value))
        }
    }

    open class var maintenanceItemObject: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                return AMaintenanceItemObjListType.maintenanceItemObject_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                AMaintenanceItemObjListType.maintenanceItemObject_ = value
            }
        }
    }

    open var maintenanceItemObject: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: AMaintenanceItemObjListType.maintenanceItemObject))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjListType.maintenanceItemObject, to: IntValue.of(optional: value))
        }
    }

    open class var maintenanceItemObjectList: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                return AMaintenanceItemObjListType.maintenanceItemObjectList_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                AMaintenanceItemObjListType.maintenanceItemObjectList_ = value
            }
        }
    }

    open var maintenanceItemObjectList: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: AMaintenanceItemObjListType.maintenanceItemObjectList))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjListType.maintenanceItemObjectList, to: LongValue.of(optional: value))
        }
    }

    open class var maintenanceNotification: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                return AMaintenanceItemObjListType.maintenanceNotification_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                AMaintenanceItemObjListType.maintenanceNotification_ = value
            }
        }
    }

    open var maintenanceNotification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjListType.maintenanceNotification))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjListType.maintenanceNotification, to: StringValue.of(optional: value))
        }
    }

    open class var material: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                return AMaintenanceItemObjListType.material_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                AMaintenanceItemObjListType.material_ = value
            }
        }
    }

    open var material: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjListType.material))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjListType.material, to: StringValue.of(optional: value))
        }
    }

    open var old: AMaintenanceItemObjListType {
        return CastRequired<AMaintenanceItemObjListType>.from(self.oldEntity)
    }

    open class var serialNumber: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                return AMaintenanceItemObjListType.serialNumber_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjListType.self)
            defer { objc_sync_exit(AMaintenanceItemObjListType.self) }
            do {
                AMaintenanceItemObjListType.serialNumber_ = value
            }
        }
    }

    open var serialNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjListType.serialNumber))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjListType.serialNumber, to: StringValue.of(optional: value))
        }
    }
}
