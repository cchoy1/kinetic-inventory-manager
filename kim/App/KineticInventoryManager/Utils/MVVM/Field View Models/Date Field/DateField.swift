//
//  DateField.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 21/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

enum SelectionFieldType: String {
    case day
    case time
    case courier
    case none
}

class DateField: Field {
    var date: Date
    
    init(with id: String, date: Date, title: String, detail: String = String(), selectionFieldType: SelectionFieldType, accessoryType: UITableViewCell.AccessoryType) {
        self.date = date
        super.init(with: id, title: title, detail: detail, selectable: true, accessoryType: accessoryType, selectionFieldType: selectionFieldType)
    }
}
