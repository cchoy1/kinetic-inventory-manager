//
//  NotesField.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 19/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

class NotesField: Field {
    var placeholder: String
    
    init(with note: String, placeholder: String, editable: Bool) {
        self.placeholder = placeholder
        super.init(title: note, editable: editable)
    }
}
