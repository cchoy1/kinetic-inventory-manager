//
//  Product ImageField.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 20/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

class ProductImageField: Field {
    var imageInfo: ImageInfo
    
    init(with product: [String : Any]) {
        self.imageInfo = ImageInfo(imageURL: product["imageURL"] as? String ?? " ", imageType: .asset)
        let title = product["description"] as? String ?? " "
        super.init(title: title)
    }
}
