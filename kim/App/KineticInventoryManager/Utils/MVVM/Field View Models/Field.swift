//
//  Field.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class Field: FieldViewModelable {
    var id: String
    var title: String
    var detail: String
    var editable: Bool
    var selectable: Bool
    var accessoryType: UITableViewCell.AccessoryType
    var type: SelectionFieldType
    
    init(with id: String = UUID().uuidString, title: String, detail: String = String(), editable: Bool = false, selectable: Bool = false, accessoryType: UITableViewCell.AccessoryType = .none, selectionFieldType: SelectionFieldType = .none) {
        self.id = id
        self.title = title
        self.detail = detail
        self.editable = editable
        self.selectable = selectable
        self.accessoryType = accessoryType
        self.type = selectionFieldType
    }
    
    func update(with detail: String = String(), editable: Bool = false, selectable: Bool = false, accessoryType: UITableViewCell.AccessoryType = .none) {
        self.detail = detail
        self.editable = editable
        self.selectable = selectable
        self.accessoryType = accessoryType
    }
}
