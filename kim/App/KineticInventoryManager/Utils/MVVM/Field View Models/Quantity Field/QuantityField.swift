//
//  QuantityField.swift
//  KineticInventoryManager
//
//  Created by Raka Chowdhury on 26/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

class QuantityField: Field {
    var quantity: Int
    var maxQuantity: Int
    
    init(with quantity: Int, maxQuantity: Int = 0, editable: Bool) {
        self.quantity = quantity
        self.maxQuantity = maxQuantity
        super.init(title: "\(self.quantity)", editable: true)
    }
}
