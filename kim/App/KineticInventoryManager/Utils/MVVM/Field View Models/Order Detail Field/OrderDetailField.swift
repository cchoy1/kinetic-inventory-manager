//
//  OrderDetailField.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 23/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class OrderDetailField: Field {
    var imageURL: String
    var quantity: String
    
    init(with inboundOrder: DLVHead) {
        let orderStatus = GoodsHelper.fetchStatus(for: inboundOrder)
        self.imageURL = orderStatus.imageName
        
        let quantityInt = inboundOrder.numberOfItems
        self.quantity = R.string.goods.qty("\(quantityInt ?? 0)")
        
        let id = inboundOrder.inboundDelivery ?? UUID().uuidString
        let name = R.string.goods.deliveryFrom(inboundOrder.shipFromPartyName ?? "")
        var time = ""
        if let date = inboundOrder.plannedDeliveryEndDate?.utc() {
            if inboundOrder.completionStatusName == "Completed" {
                time = GoodsHelper.format(date)
            } else {
                /// To be updated when we have data coming in daily - at time of build this app was for demo pruposes
                // time = GoodsHelper.format(date)
                let timeString = DateFormatter.simpleTimeFormatter.string(from: date).lowercased()
                time = R.string.generic.todayAt(timeString)
            }
        }
        super.init(with: id, title: name, detail: time, editable: false, selectable: true, accessoryType: .disclosureIndicator)
    }
    
    init(with outboundOrder: OUTBDLVHead) {
        let orderStatus = GoodsHelper.fetchStatus(for: outboundOrder)
        self.imageURL = orderStatus.imageName
        
        let quantityInt = outboundOrder.numberOfItems
        self.quantity = R.string.goods.qty("\(quantityInt ?? 0)")
        
        let id = outboundOrder.outboundDelivery ?? UUID().uuidString
        let name = R.string.goods.deliveryTo(outboundOrder.shipToPartyName ?? "")
        
        var time = ""
        switch orderStatus {
        case .pending, .packed:
            if let date = outboundOrder.outOfYardDatePlan?.utc() {
                /// To be updated when we have data coming in daily - at time of build this app was for demo pruposes
                // time = GoodsHelper.format(date)
                time = DateFormatter.simpleTimeFormatter.string(from: date).lowercased()
            } else if let date = outboundOrder.deliveryDatePlan?.utc() {
                /// To be updated when we have data coming in daily - at time of build this app was for demo pruposes
                // time = GoodsHelper.format(date)
                time = DateFormatter.simpleTimeFormatter.string(from: date).lowercased()
            }
            time = R.string.generic.todayAt(time)
        case .inTransit, .delivered:
            if let date = outboundOrder.deliveryDatePlan?.utc() {
                /// To be updated when we have data coming in daily - at time of build this app was for demo pruposes
                time = GoodsHelper.format(date)
            }
        }
        super.init(with: id, title: name, detail: time, editable: false, selectable: true, accessoryType: .disclosureIndicator)
    }
}
