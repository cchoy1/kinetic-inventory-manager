//
//  ProductDetailField.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 20/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

class ProductDetailField: Field {
    var sku: String
    var quantity: String
    var scannedQuantity: String?
    var colour: String
    var box: String
    var imageInfo: ImageInfo
    
    init(withRequested productItem: ProductItem) {
        self.quantity = "1"
        self.colour = "Black"
        self.sku = productItem.product.product ?? ""
        self.box = "Box 1"
        self.imageInfo = ImageInfo(imageURL: "ShoeCollected", imageType: .asset)
        
        let name = productItem.product.product ?? "Unkown"
        let quantityString = "Qty. " + self.quantity
        let id = productItem.product.product ?? UUID().uuidString
        
        super.init(with: id, title: name, detail: quantityString, editable: false, selectable: true, accessoryType: .disclosureIndicator)
    }
    
    init(withRequested product: [String : Any]) {
        let quantityInt = Int(product["quantity"] as? Int ?? 0)
        self.quantity = String(quantityInt)
        self.colour = product["colour"] as? String ?? " "
        self.sku = product["sku"] as? String ?? " "
        self.box = product["box"] as? String ?? " "
        self.imageInfo = ImageInfo(imageURL: "ShoePending", imageType: .asset)
        
        let name = product["name"] as? String ?? " "
        let quantityString = "Qty. " + self.quantity
        let id = product["id"] as? String ?? UUID().uuidString
        
        super.init(with: id, title: name, detail: quantityString, editable: false, selectable: true, accessoryType: .disclosureIndicator)
    }
    
    init(withOutbound product: OUTBDLVItem) {
        let quantityInt = product.itemDeliveryQuantity?.intValue() ?? 0
        var scannedQuantityInt = 0
        self.imageInfo = ImageInfo(imageURL: "ShoePending", imageType: .asset)
        if product.packingStatusName == "Completed" {
            scannedQuantityInt = quantityInt
            self.imageInfo = ImageInfo(imageURL: "ShoeCollected", imageType: .asset, tintColour: .green)
        }
        self.quantity = String(quantityInt)
        self.colour = "Unknown"
        self.sku = product.stockDocument ?? " "
        self.box = "Box 1"
        
        let name = product.productName ?? " "
        let quantityString = String(format: "%d/%d", scannedQuantityInt, quantityInt)
        
        let id = product.outboundDeliveryItem ?? UUID().uuidString
        
        super.init(with: id, title: name, detail: quantityString, editable: false, selectable: true, accessoryType: .disclosureIndicator)
    }
    
    init(withDelivered product: DLVItem) {
        let quantityInt = product.itemDeliveryQuantity?.intValue() ?? 0
        let scannedQuantityInt = product.packOpenQuantity?.intValue() ?? 0
        self.quantity = String(quantityInt)
        self.scannedQuantity = String(scannedQuantityInt)
        self.colour = "Unknown"
        self.imageInfo = ImageInfo(imageURL: "ShoeCollected", imageType: .asset, tintColour: .green)
        self.sku = "SKU: 1234567"
        self.box = String(product.uxFcBatch ?? 0)
        
        let quantityString = String(format: "%d/%d", scannedQuantityInt, quantityInt)
        let name = product.productName ?? " "
        let id = product.inboundDeliveryItem ?? " "
        
        super.init(with: id, title: name, detail: quantityString, editable: false, selectable: true, accessoryType: .disclosureIndicator)
    }
}
