//
//  ImageField.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 23/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class ImageField: Field {
    var imageInfo: ImageInfo
    
    init(with id: String = UUID().uuidString, title: String = String(), detail: String = String(), imageInfo: ImageInfo) {
        self.imageInfo = imageInfo
        super.init(with: id, title: title, detail: detail, editable: false, selectable: true)
    }
}
