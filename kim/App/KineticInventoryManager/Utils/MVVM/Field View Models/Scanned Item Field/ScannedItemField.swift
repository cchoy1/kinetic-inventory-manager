
//
//  ScannedItemField.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 26/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

class ScannedItemField: Field {
    var hasStepper: Bool
    var quantity: Int
    var scannedCount: Int
    var imageInfo: ImageInfo?
    
    init(forItemWithTitle title: String, detail: String? = nil, hasStepper: Bool = false, scannedStatus: ScannedStatus) {
        self.hasStepper = hasStepper
        if hasStepper == false {
            switch scannedStatus {
            case .pending:
                self.hasStepper = true
            case .scanned, .none:
                self.hasStepper = false
            }
        }
        if scannedStatus != .none {
            self.imageInfo = ImageInfo(imageURL: scannedStatus.imageURL, imageType: .asset, tintColour: scannedStatus.tintColour)
        }
        var updatedDetail = scannedStatus.title
        if let currentDetail = detail  {
            updatedDetail = currentDetail
        }
        self.quantity = 0
        self.scannedCount = 0
        super.init(title: title, detail: updatedDetail, editable: hasStepper, selectable: false, accessoryType: .none)
    }
    
    init(for inboundOrderItem: DLVItem) {
        self.hasStepper = false
        self.quantity = inboundOrderItem.itemDeliveryQuantity?.intValue() ?? 0
        self.scannedCount = inboundOrderItem.packedQuantity?.intValue() ?? 0
        let title = inboundOrderItem.productName ?? ""
        let detail = inboundOrderItem.itemDeliveryQuantity?.toString() ?? "0"
        super.init(title: title, detail: detail, selectable: false, accessoryType: .none)
    }
    
    init(for outboundOrderItem: OUTBDLVItem, hasStepper: Bool) {
        var scannedStatus = ScannedStatus.none
        self.hasStepper = hasStepper
        self.scannedCount = 0
        self.quantity = outboundOrderItem.itemDeliveryQuantity?.intValue() ?? 0
        
        if outboundOrderItem.packingStatusName == "Completed" {
            scannedStatus = .scanned
            self.scannedCount = self.quantity
        } else {
            scannedStatus = .pending
        }
        if scannedStatus != .none {
            self.imageInfo = ImageInfo(imageURL: scannedStatus.imageURL, imageType: .asset, tintColour: scannedStatus.tintColour)
        }
        let title = outboundOrderItem.productName ?? ""
        let detail = "\(scannedCount)/\(self.quantity)"
        super.init(title: title, detail: detail, editable: hasStepper, selectable: false, accessoryType: .none)
    }
    
    init(forProduct product: [String : Any], hasStepper: Bool, scannedStatus: ScannedStatus) {
        self.hasStepper = hasStepper
        let title = product["name"] as? String ?? ""
        if scannedStatus != .none {
            imageInfo = ImageInfo(imageURL: scannedStatus.imageURL, imageType: .asset, tintColour: scannedStatus.tintColour)
        }
        quantity = Int(product["quantity"] as? String ?? "0") ?? 0
        scannedCount = Int(product["scannedCount"] as? String ?? "0") ?? 0
        super.init(title: title, editable: hasStepper, selectable: false, accessoryType: .none)
    }
}
