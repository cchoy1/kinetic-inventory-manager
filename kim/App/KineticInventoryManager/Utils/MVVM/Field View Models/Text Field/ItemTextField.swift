//
//  ItemTextField.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 20/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class ItemTextField: Field {
    var placeholder: String
    
    init(with title: String, text: String, placeholder: String, editable: Bool) {
        self.placeholder = placeholder
        super.init(title: title, detail: text, editable: true)
    }
}
