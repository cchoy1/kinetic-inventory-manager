//
//  FieldTypes.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 18/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

enum FieldType: Int, CaseIterable {
    case date
    
    var title: String {
        switch self {
        case .date:
            return R.string.generic.date()
        }
    }
}
