//
//  ButtonField.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 19/03/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit
import Kingfisher

class ButtonField: Field {
    var imageInfo: ImageInfo?
    
    init(with title: String, imageInfo: ImageInfo? = nil) {
        self.imageInfo = imageInfo
        super.init(title: title)
    }
}
