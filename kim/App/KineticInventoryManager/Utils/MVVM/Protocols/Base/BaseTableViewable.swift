//
//  BaseView.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

protocol BaseTableViewable {
    var tableView: FioriBaseTableView! { get }
    var dataProvider: TableDataProviderable! { get set }

    func setupTableView(with dataSource: UITableViewDataSource, delegate: UITableViewDelegate)
    
    func refresh()
}

extension BaseTableViewable {
    func setupTableView(with dataSource: UITableViewDataSource, delegate: UITableViewDelegate) {
        tableView.dataSource = dataSource
        tableView.delegate = delegate
    }
}
