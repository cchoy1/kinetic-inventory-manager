//
//  FieldViewModelable.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

protocol FieldViewModelable {
    var id: String { get set }
    var title: String { get set }
    var detail: String { get set }
}

