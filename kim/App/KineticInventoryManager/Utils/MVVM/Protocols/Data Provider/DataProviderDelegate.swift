//
//  DataProviderDelegate.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

protocol DataProviderDelegate: class {
    func reloadData()
    func didSelect(_ field: Field)
}
