//
//  TableViewDataProvider.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class TableViewDataProvider: NSObject, TableDataProviderable {
    var tableView: UITableView
    var viewModel: ViewModelable
    weak var delegate: DataProviderDelegate?
    
    required init(with tableView: UITableView, viewModel: ViewModelable, delegate: DataProviderDelegate?) {
        self.tableView = tableView
        self.viewModel = viewModel
        self.delegate = delegate
    }
}

// MARK: - UITableViewDataSource
extension TableViewDataProvider: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfItems(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if let field = viewModel.item(for: indexPath) as? Field {
            cell.textLabel?.text = field.title
            cell.detailTextLabel?.text = field.detail
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard tableView.style == .grouped else {
            return nil
        }
        return viewModel.title(for: section).uppercased()
    }
}

// MARK: - UITableViewDelegate
extension TableViewDataProvider: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let field = viewModel.item(for: indexPath) as? Field {
            delegate?.didSelect(field)
        }
    }
}

