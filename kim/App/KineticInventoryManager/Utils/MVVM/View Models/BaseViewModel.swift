//
//  BaseViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

class BaseViewModel: ViewModelable {
    var sections: [SectionItem]
    
    init(with sections: [SectionItem]) {
        self.sections = sections
    }
}
