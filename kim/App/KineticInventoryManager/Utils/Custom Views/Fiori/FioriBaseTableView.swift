//
//  FioriBaseTableView.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 18/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit
import SAPFiori

class FioriBaseTableView: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()
        registerCells()
        setupDimensions()
    }
    
    func registerCells() {
        register(DateSelectionTableViewCell.nib(), forCellReuseIdentifier: DateSelectionTableViewCell.className)
        register(ObjectCellTableViewCell.nib(), forCellReuseIdentifier: ObjectCellTableViewCell.className)
        register(ProductDetailTableViewCell.nib(), forCellReuseIdentifier: ProductDetailTableViewCell.className)
        register(ImageTableViewCell.nib(), forCellReuseIdentifier: ImageTableViewCell.className)
        register(TitleDetailTableViewCell.nib(), forCellReuseIdentifier: TitleDetailTableViewCell.className)
        register(TitleImageTableViewCell.nib(), forCellReuseIdentifier: TitleImageTableViewCell.className)
        register(TextFieldTableViewCell.nib(), forCellReuseIdentifier: TextFieldTableViewCell.className)
        register(NoteTableViewCell.nib(), forCellReuseIdentifier: NoteTableViewCell.className)
        register(CardImageTableViewCell.nib(), forCellReuseIdentifier: CardImageTableViewCell.className)
    }
    
    func setupDimensions() {
        estimatedRowHeight = 120
        rowHeight = UITableView.automaticDimension
        estimatedSectionHeaderHeight = 44
        sectionHeaderHeight = UITableView.automaticDimension
        cellLayoutMarginsFollowReadableWidth = true
    }
}
