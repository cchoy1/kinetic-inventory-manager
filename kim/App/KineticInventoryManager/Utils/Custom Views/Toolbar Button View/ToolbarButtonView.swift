//
//  ToolbarButtonView.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 25/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

protocol ToolbarButtonViewDelegate: class {
    func didTouchUpInsideToolbarButtonView()
}

class ToolbarButtonView: NibView {
    @IBOutlet private var button: UIButton!
    
    private weak var delegate: ToolbarButtonViewDelegate!
    
    func update(with delegate: ToolbarButtonViewDelegate) {
        self.delegate = delegate
    }
    
    func update(with field: Field) {
        guard let buttonField = field as? ButtonField else {
            return
        }
        button.setTitle(buttonField.title, for: .normal)
        if let imageInfo = buttonField.imageInfo, let image = ImageInfo.image(with: imageInfo) {
            button.setImage(image, for: .normal)
            button.setTitle(" " + buttonField.title, for: .normal)
        }
    }
    
    @IBAction private func didTouchUpInsideButton(_ sender: Any) {
        delegate.didTouchUpInsideToolbarButtonView()
    }
}
