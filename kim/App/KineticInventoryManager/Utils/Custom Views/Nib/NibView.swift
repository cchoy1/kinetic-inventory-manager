//
//  NibView.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit
import SAPFiori

@IBDesignable
class NibView: UIView {

    var view: UIView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        if let nibName = classForCoder.description().components(separatedBy: ".").last {
            setupNib(String(nibName))
        }
    }
    
    func setupNib(_ nibName: String) {
        if let view = loadViewFromNib(nibName) {
            self.view = view
            self.view?.frame = bounds
            self.view?.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
            addSubview(view)
        }
    }
    
    func loadViewFromNib(_ nibName: String) -> UIView? {
        let bundle = Bundle(for: type(of: self))
        if bundle.path(forResource: nibName, ofType: "nib") != nil {
            let nib = UINib(nibName: nibName, bundle: bundle)
            if let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView {
                return view
            }
        }
        return nil
    }
}
