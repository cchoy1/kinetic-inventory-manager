//
//  QuantutyCell.swift
//  KineticInventoryManager
//
//  Created by Raka Chowdhury on 26/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class QuantityCell: UITableViewCell {
    @IBOutlet var stepper: UIStepper!
    @IBOutlet var quantityLabel: UILabel!
}

extension QuantityCell {
    func update(with quantityField: QuantityField) {
        if quantityField.editable == false {
            stepper.isHidden = true
        } else {
            stepper.maximumValue = Double(quantityField.maxQuantity)
        }
        stepper.value = Double(quantityField.quantity)
        quantityLabel.text = Int(stepper.value).description
    }
    
    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        quantityLabel.text = Int(sender.value).description
    }
}
