//
//  DateSelectionTableViewCell.swift
//  Concessions
//
//  Created by Pena, Cristian (UK - London) on 15/02/2018.
//  Copyright © 2019 Deloitte. All rights reserved.
//

import UIKit

typealias DatePickerMode = UIDatePicker.Mode

protocol DateSelectionCellDelegate: class {
    func didFinishEditing(value: Any?, for key: String, reloadTableView: Bool)
}

class DateSelectionTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateTitleLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerButton: UIButton!
    
    let dateFormatter = DateFormatter()
    weak var delegate: DateSelectionCellDelegate?
    
    override func awakeFromNib() {
        setupView()
    }
    
    func setupView() {
        setPicker()
        setupVariables()
        updateLabels()
        showHideDatePicker()
    }

    func setPicker() {
        datePicker.datePickerMode = .date
    }
    
    func setupVariables() {
        dateFormatter.dateFormat = "yyyy MMM dd"
    }
    
    func updateLabels() {
        titleLabel.font = UIFont.preferredFioriFont(forTextStyle: .body)
        titleLabel.textColor = UIColor.preferredFioriColor(forStyle: .primary2)
        dateTitleLabel.font = UIFont.preferredFioriFont(forTextStyle: .body)
        dateTitleLabel.textColor = UIColor.preferredFioriColor(forStyle: .tintColorDark)
        update(with: datePicker.date)
    }
    
    func updateModel() {
       delegate?.didFinishEditing(value: datePicker.date, for: FieldType.date.title, reloadTableView: true)
    }
    
    func update(with date: Date) {
        self.dateTitleLabel.text = dateFormatter.string(from: date)
    }
    
    func configure(with date: Date) {
        update(with: date)
    }
    
    func showHideDatePicker() {
        datePicker.isHidden = datePicker.isHidden ? false : true
        updateModel()
    }
    
    @IBAction func datePickerButtonPressed(_ sender: Any) {
        showHideDatePicker()
    }
    
    @IBAction func dateChanged(_ sender: UIDatePicker) {
        updateModel()
    }
}
