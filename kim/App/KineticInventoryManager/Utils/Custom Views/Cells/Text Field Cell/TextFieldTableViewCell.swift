//
//  TextFieldTableViewCell.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 01/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {

    @IBOutlet var textField: UITextField!
    
    func update(with itemTextField: ItemTextField) {
        textField.placeholder = itemTextField.placeholder
        textField.text = itemTextField.title
    }
}
