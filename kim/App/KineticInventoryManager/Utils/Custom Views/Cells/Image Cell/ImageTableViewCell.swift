//
//  ImageTableViewCell.swift
//  KineticInventoryManager
//
//  Created by Raka Chowdhury on 27/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    @IBOutlet var cellImageView: UIImageView!
    
    func update(with imageField: ImageField) {
        cellImageView.update(with: imageField.imageInfo)
    }
}
