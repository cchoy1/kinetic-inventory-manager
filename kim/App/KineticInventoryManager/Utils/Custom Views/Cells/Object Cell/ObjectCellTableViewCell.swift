//
//  ObjectCellTableViewCell.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 31/03/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

class ObjectCellTableViewCell: UITableViewCell {
    @IBOutlet var objectImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    
    func update(with field: Field) {
        if let orderDetailField = field as? OrderDetailField {
            update(with: orderDetailField)
        }
    }
    
    func update(with orderDetailField: OrderDetailField) {
        objectImageView.image = UIImage(named: orderDetailField.imageURL)
        titleLabel.text = orderDetailField.title
        detailLabel.text = orderDetailField.detail
        statusLabel.text = orderDetailField.quantity
        accessoryType = orderDetailField.accessoryType
    }
}
