//
//  TitleDetailTableViewCell.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 31/03/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

class TitleDetailTableViewCell: UITableViewCell {
    func update(with titleDetail: Field) {
        textLabel?.text = titleDetail.title
        detailTextLabel?.text = titleDetail.detail
        accessoryType = titleDetail.accessoryType
    }
}
