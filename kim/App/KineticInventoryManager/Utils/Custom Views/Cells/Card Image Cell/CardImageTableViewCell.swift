//
//  CardImageTableViewCell.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

class CardImageTableViewCell: UITableViewCell {
    @IBOutlet var containerView: UIView!
    @IBOutlet var cardImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 12.0
    }
    
    func update(with imageField: ImageField) {
        cardImageView.update(with: imageField.imageInfo)
    }
}
