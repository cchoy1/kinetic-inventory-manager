//
//  NoteTableViewCell.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 01/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {
    @IBOutlet private(set) weak var titleLabel: UILabel!
    @IBOutlet private(set) weak var textView: UITextView!
    @IBOutlet private(set) weak var separatorView: UIView!
    
    @IBOutlet var titleLabelTopConstraint: NSLayoutConstraint!
    
    var placeholder: String?
    var textViewText: String {
        get {
            return textView.text
        }
        set {
            textView.text = newValue
        }
    }
    
    var key: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.font = .preferredFont(forTextStyle: .subheadline)
        textView.font = .preferredFont(forTextStyle: .body)
        textView.delegate = self
    }
    
    func update(with field: NotesField) {
        placeholder = field.placeholder
        textView.text = placeholder
        titleLabel.text = field.title
        textView.sizeToFit()
        textView.isUserInteractionEnabled = field.editable
        updateTextViewText()
        updateTitleLabelTopConstraint()
    }
    
    func updateModel() {
        guard let key = key else {
            return
        }
        textViewText = textView.text == placeholder ? "" : textView.text
    }
    
    func setAsFirstResponder() {
        textView.becomeFirstResponder()
    }
    
    private func updateTitleLabelTopConstraint() {
        if titleLabel.text?.isEmpty == true {
            titleLabelTopConstraint.constant = 0.0
            setNeedsLayout()
        }
    }
}

extension NoteTableViewCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        updateTextViewText()
        updateModel()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        removePlaceholder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        updateTextViewText()
    }
    
    func removePlaceholder() {
        if textView.text == placeholder {
            textView.text = ""
            textView.textColor = .label
        }
    }
    
    func updateTextViewText() {
        if textView.text.isEmpty || textView.text == placeholder {
            textView.text = placeholder
            textView.textColor = .systemGray2
        } else {
            textView.textColor = .label
        }
    }
}

