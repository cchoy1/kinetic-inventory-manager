//
//  TitleImageTableViewCell.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 31/03/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

class TitleImageTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var objectImageView: UIImageView!
    
    func update(with productImageField: ProductImageField) {
        titleLabel.text = productImageField.title
        objectImageView.update(with: productImageField.imageInfo)
     }
}
