//
//  ButtonHeaderView.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 19/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

protocol ButtonHeaderViewDelegate: class {
    func didTouchUpInsideHeaderViewButton()
}

class ButtonHeaderView: NibView {
    @IBOutlet private var headerButton: UIButton!
    
    private weak var delegate: ButtonHeaderViewDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.headerButton.layer.cornerRadius = 14.0
    }
    
    @IBAction private func didTouchUpInsideButton(_ sender: Any) {
        delegate.didTouchUpInsideHeaderViewButton()
    }
}

extension ButtonHeaderView {
    func update(with delegate: ButtonHeaderViewDelegate) {
        self.delegate = delegate
    }
    
    func update(with field: Field) {
        guard let buttonField = field as? ButtonField else {
            return
        }
        headerButton.setTitle(buttonField.title, for: .normal)
        if let imageInfo = buttonField.imageInfo {
            headerButton.update(with: imageInfo)
        }
    }
}
