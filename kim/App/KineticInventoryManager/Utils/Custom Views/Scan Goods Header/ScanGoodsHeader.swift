//
//  ScanGoodsHeader.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 01/05/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

protocol ScanGoodsHeaderDelegate: class {
    func didTouchUpInsideSearchProductViewButton()
    func didTouchUpInsideKeyboardProductViewButton()
}

class ScanGoodsHeader: NibView {
    private weak var delegate: ScanGoodsHeaderDelegate!
    
    @IBAction private func didTouchUpInsideKeyboardButton(_ sender: Any) {
        delegate.didTouchUpInsideKeyboardProductViewButton()
    }
    
    @IBAction private func didTouchUpInsideSearchButton(_ sender: Any) {
        delegate.didTouchUpInsideSearchProductViewButton()
    }
}

extension ScanGoodsHeader {
    func update(with delegate: ScanGoodsHeaderDelegate) {
        self.delegate = delegate
    }
}
