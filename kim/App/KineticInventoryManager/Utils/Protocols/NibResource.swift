//
//  NibResource.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 18/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation
import UIKit

protocol NibResource: class {
    static func nib() -> UINib?
}

extension NibResource where Self: NSObject {
    static func nib() -> UINib? {
        return UINib(nibName: self.className, bundle: nil)
    }
}

extension UITableViewCell: NibResource {}

extension UITableViewHeaderFooterView: NibResource {}

extension UICollectionReusableView: NibResource {}
