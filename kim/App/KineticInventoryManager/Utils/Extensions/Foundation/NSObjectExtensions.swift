//
//  NSObjectExtensions.swift
//  allora
//
//  Created by Pena, Cristian (UK - London) on 11/10/17.
//  Copyright © 2018 Zurich. All rights reserved.
//

import Foundation

extension NSObject {
    var className: String {
        return "\(type(of: self))"
    }
    
    class var className: String {
        return "\(self)"
    }
}
