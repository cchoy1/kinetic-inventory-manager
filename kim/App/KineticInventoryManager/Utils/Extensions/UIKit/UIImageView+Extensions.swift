//
//  UIImageView+Extensions.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 25/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

extension UIImageView {
    func update(with imageInfo: ImageInfo) {
        if imageInfo.imageType == .url {
            kf.setImage(with: URL(string: imageInfo.imageURL ))
        } else if let returnedImage = ImageInfo.image(with: imageInfo) {
            image = returnedImage
        }
        clipsToBounds = true
        tintColor = imageInfo.tintColour
    }
}
