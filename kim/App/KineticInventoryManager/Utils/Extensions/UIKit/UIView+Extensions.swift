//
//  UIView+Extensions.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 03/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

extension UIView {
    func setRoundedCorners() {
        layer.cornerRadius = (min(bounds.width, bounds.height))/2
        clipsToBounds = true
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func smoothRoundCorners(to radius: CGFloat) {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(
            roundedRect: bounds,
            cornerRadius: radius
        ).cgPath
        
        layer.mask = maskLayer
    }
    
    func updateBackground(with effectStyle: UIBlurEffect.Style) {
        let effectStyle = UIBlurEffect(style: effectStyle)
        let effectView = UIVisualEffectView(effect: effectStyle)
        var backgroundView: UIView!
        backgroundView = effectView
        backgroundView.frame = self.bounds
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.clipsToBounds = true
        self.insertSubview(backgroundView, at: 0)
        
        let backgroundViewConstraints = [
            backgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: kVerticalLeeway),
            backgroundView.topAnchor.constraint(equalTo: self.topAnchor)
        ]
        
        for constraint in backgroundViewConstraints {
            constraint.isActive = true
        }
        self.backgroundColor = UIColor.clear
    }
}
