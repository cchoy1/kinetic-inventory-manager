//
//  UIButton+Extensions.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 25/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

extension UIButton {
    func update(with imageInfo: ImageInfo) {
        if let image = ImageInfo.image(with: imageInfo) {
            setImage(image, for: .normal)
            setTitle(" \(String(describing: titleLabel?.text))", for: .normal)
            tintColor = imageInfo.tintColour
        }
    }
}
