//
//  Date+Extensions.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 04/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

extension Date {
    /// Creates a normalized date, i.e. only day, month and year components
    ///
    /// - Returns: normalized date of receiver.  E.g. for 12/10/2016 01:43:12, returns 12/10/2016 00:00:00
    func normalizedDate() -> Date? {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: self)
        return calendar.date(from: components)
    }
    
    /// Creates a normalized date, i.e. only day, month and year components
    ///
    /// - Returns: normalized tomorrow date of receiver.  E.g. for 12/10/2016 01:43:12, returns 13/10/2016 00:00:00
    func normalizedDateTomorrow() -> Date? {
        let normalizedToday = self.normalizedDate()
        return normalizedToday?.dateByAdding(numberOfDays: 1)
    }
    
    /// Creates a normalized date, i.e. only day, month and year components
    ///
    /// - Returns: normalized yesterday date of receiver.  E.g. for 12/10/2016 01:43:12, returns 11/10/2016 00:00:00
    func normalizedDateYesterday() -> Date? {
        let normalizedToday = self.normalizedDate()
        return normalizedToday?.dateByAdding(numberOfDays: -1)
    }
    
    /// Check if date is today
    ///
    /// - Returns: true if receiver is today, false if not
    func isToday() -> Bool {
        let calendar = Calendar.current
        return calendar.isDateInToday(self)
    }
    
    /// Check if date is yesteday
    ///
    /// - Returns: true if receiver is yesteday, false if not
    func isYesterday() -> Bool {
        let calendar = Calendar.current
        return calendar.isDateInYesterday(self)
    }
    
    /// Check if date is tommorow
    ///
    /// - Returns: true if receiver is tommorow, false if not
    func isTommorow() -> Bool {
        let calendar = Calendar.current
        return calendar.isDateInTomorrow(self)
    }
    
    /// Adds the number of Days to the receiver and returns a new Date
    ///
    /// - Parameter numberOfDays: The number of days to advance or recede the date
    /// - Returns: A new date with advanced/receded number of days from the receiver
    func dateByAdding(numberOfDays: Int) -> Date? {
        let calendar = Calendar.current
        var components = DateComponents()
        components.day = numberOfDays
        return calendar.date(byAdding: components, to: self)
    }
    
    /// Convenience method to cast Date to NSDate
    ///
    /// - Returns: The Date as an NSDate
    func toNSDate() -> NSDate {
        return self as NSDate
    }
    
    
    /// Updates the time for the current date
    /// - Parameters:
    ///   - hour: hour value
    ///   - minute: minutes value
    ///   - second: seconds value
    ///   - timeZone: time zone
    /// - Returns: updated date
    public func setTime(hour: Int, minute: Int, second: Int) -> Date? {
        let dateComponents: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]
        let calendar = Calendar.current
        var components = calendar.dateComponents(dateComponents, from: self)
        
        components.timeZone = TimeZone.current
        components.hour = hour
        components.minute = minute
        components.second = second
        
        return calendar.date(from: components)
    }
}

extension NSDate {
    
    /// Convenience method to cast NSDate to Date
    ///
    /// - Returns: The NSDate as a Date
    func toDate() -> Date {
        return self as Date
    }
}
