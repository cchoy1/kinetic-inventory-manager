//
//  DefaultWarehouseHelper.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 08/05/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation
import SAPCommon
import SAPFiori
import SAPFoundation
import SAPOData

class DefaultWarehouseHelper {
    enum WarehouseKey: String {
        case id = "warehouseID"
        case name  = "warehouseName"
    }
    
    static private func updateDefaultWarehouse(with warehouseID: String, warehouseName: String) {
        let defaults = UserDefaults.standard
        defaults.set(warehouseID, forKey: WarehouseKey.id.rawValue)
        defaults.set(warehouseName, forKey: WarehouseKey.name.rawValue)
    }
}

// MARK: Public Methods
extension DefaultWarehouseHelper {
    static func fetchDefaultWarehouseId() -> String {
        guard let id = UserDefaults.standard.string(forKey: WarehouseKey.id.rawValue) else {
            return "1710"
        }
        return id
    }
    
    static func fetchDefaultWarehouseName() -> String {
        guard let name = UserDefaults.standard.string(forKey: WarehouseKey.name.rawValue) else {
            return "Plant 1 US"
        }
        return name
    }
    
    static func saveDefaultWarehouse(with onlineODataProvider: Ec1<OnlineODataProvider>, id: String, name: String, completionHandler: @escaping (Error?) -> Void) {
        let customWarehouse = WarehouseType()
        customWarehouse.warehouse = id
        // This method does not actually create an entity
        // It hits this API https://nextgencf.it-cpi003-rt.cfapps.us10.hana.ondemand.com/gw/odata/SAP/KIMODATA;v=1/Warehouse
        // and updates the users default warehouseId
        onlineODataProvider.createEntity(customWarehouse) { error in
            if let returnedError = error {
                completionHandler(returnedError)
                return
            } else {
                DefaultWarehouseHelper.updateDefaultWarehouse(with: id, warehouseName: name)
                completionHandler(nil)
            }
        }
    }
}
