//
//  FetchError.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 01/05/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

enum FetchError: Error {
    case fetchFailed
    case unknownError
}
