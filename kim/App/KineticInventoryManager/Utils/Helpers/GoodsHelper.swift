//
//  GoodsHelper.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 04/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

final class GoodsHelper {
    static func fetchStatus(for inboundOrder: DLVHead) -> OrderStatus {
        if inboundOrder.completionStatusName == "Completed" {
            return .delivered
        }
        return .inTransit
    }
    
    static func fetchStatus(for outboundOrder: OUTBDLVHead) -> OrderStatus {
        if outboundOrder.packingStatusName == "Completed" {
            if outboundOrder.goodsIssueStatusName == "Completed" {
                if outboundOrder.completionStatusName == "Completed" {
                    return .inTransit
                } else {
                    return .packed
                }
            } else {
                return .packed
            }
        }
        return .pending
    }
    
    static func format(_ date: Date) -> String {
        var formatter: DateFormatter!
        
        /// Put back following code when we have data coming in daily
        /// For demo purposes we will only show the time
        if date.isYesterday() {
            formatter = DateFormatter.simpleTimeFormatter
            let timeString = formatter.string(from: date).lowercased()
            return R.string.generic.yestedayAt(timeString)
        }else if date.isToday() {
            formatter = DateFormatter.simpleTimeFormatter
            let timeString = formatter.string(from: date).lowercased()
            return R.string.generic.todayAt(timeString)
        } else if date.isTommorow() {
            formatter = DateFormatter.simpleTimeFormatter
            let timeString = formatter.string(from: date).lowercased()
            return R.string.generic.tommorowAt(timeString)
        } else {
            formatter = DateFormatter.mediumStyleDateTimeFormatter
            return formatter.string(from: date)
        }
    }
}
