//
//  FioriBaseTableDataProvider.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 19/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit
import SAPFiori

class FioriBaseTableDataProvider: TableViewDataProvider {}

// MARK: - Override UITableViewDataSource
extension FioriBaseTableDataProvider {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FUITextFieldFormCell.reuseIdentifier, for: indexPath) as? FUITextFieldFormCell else {
            return UITableViewCell()
        }
        if let field = viewModel.item(for: indexPath) as? Field {
            cell.keyName = field.title
            cell.value = field.detail.isEmpty ? " " : field.detail
            cell.isEditable = field.editable
        }
        return cell
    }
}
