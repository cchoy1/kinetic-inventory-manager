//
//  GoodsScannedStatus.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 26/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

enum ScannedStatus: String {
    case scanned
    case pending
    case none
    
    var imageURL: String {
        switch self {
        case .scanned, .pending:
            return "checkmark.circle.fill"
        case .none:
            return ""
        }
    }
    
    var tintColour: UIColor {
        switch self {
        case .scanned:
            return .systemGreen
        case .pending:
            if #available(iOS 13.0, *) {
                return .link
            } else {
                return .systemBlue
            }
        case .none:
            return .clear
        }
    }
    
    var title: String {
        switch self {
        case .scanned:
            return R.string.goods.scanned()
        case .pending:
            return R.string.goods.pending()
        case .none:
            return ""
        }
    }
}
