//
//  OrderStatus.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 23/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

enum OrderStatus {
    case pending
    case inTransit
    case packed
    case delivered
    
    var title: String {
        switch self {
        case .pending:
            return R.string.goods.goodsPending()
        case .inTransit:
            return R.string.goods.goodsInTransit()
        case .packed:
            return R.string.goods.goodsPacked()
        case .delivered:
            return R.string.goods.goodsDelivered()
        }
    }
    
    var imageName: String {
        switch self {
        case .pending:
            return "PendingPacking"
        case .inTransit:
            return "InTransit"
        case .packed:
            return "Packed"
        case .delivered:
            return "Delivered"
        }
    }
}
