//
//  ProductsService.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 27/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit
import Foundation
import SAPOData
import SAPFioriFlows
import SAPFoundation

class PurchaseOrderItemsService {
    static func fetchPurchaseOrderProductItems(with onlineODataProvider: Ec1<OnlineODataProvider>, warehouseId: String, completionHandler: @escaping (Error?, [ProductItem]?) -> Void) {
        let query = DataQuery().filter(AProductPlantType.plant.equal(warehouseId))
        onlineODataProvider.fetchAProductPlant (matching: query) { (returnedProductPlants, error) in
            if let returnedError = error {
                completionHandler(returnedError, nil)
                return
            } else {
                PurchaseOrderItemsService.fetchProducts(with: onlineODataProvider, warehouseId: warehouseId) { (error, returnedProducts) in
                    if let returnedError = error {
                        completionHandler(returnedError, nil)
                        return
                    } else {
                        guard let products = returnedProducts, let productPlants = returnedProductPlants else {
                            completionHandler(FetchError.fetchFailed, nil)
                            return
                        }
                        let returnedProductItems = PurchaseOrderItemsService.productItems(with: products, productPlants: productPlants)
                        completionHandler(error, returnedProductItems)
                    }
                }
            }
        }
    }
    
    static func fetchProducts(with onlineODataProvider: Ec1<OnlineODataProvider>, warehouseId: String, completionHandler: @escaping (Error?, [AProductType]?) -> Void) {
        onlineODataProvider.fetchAProduct (completionHandler: { (entities, error) in
            if let returnedError = error {
                completionHandler(returnedError, nil)
                return
            } else {
                completionHandler(error, entities)
            }
        })
    }
    
    static func productItems(with products: [AProductType], productPlants: [AProductPlantType]) -> [ProductItem] {
        var productItems = [ProductItem]()
        for productPlant in productPlants {
            if let product = products.first(where: { $0.product == productPlant.product }) {
                let productItem = ProductItem(productPlantType: productPlant, product: product)
                productItems.append(productItem)
            }
        }
        return productItems
    }
}
