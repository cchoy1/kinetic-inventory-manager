//
//  PackGoodsService.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 01/05/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation
import SAPOData
import SAPFioriFlows
import SAPFoundation

class PackGoodsService {
    static func submitOutboundOrder(with onlineODataProvider: Ec1<OnlineODataProvider>,
                                    warehouseID: String,
                                    outboundOrder: OUTBDLVHead,
                                    completionHandler: @escaping (Error?, Bool) -> Void) {
        let warehouseTask = CreateOutbWarehouseTaskType()
        warehouseTask.outboundDeliveryUUID = outboundOrder.outboundDeliveryUUID
        onlineODataProvider.createEntity(warehouseTask) { error in
            if let returnedError = error {
                completionHandler(returnedError, false)
                return
            } else {
                confirmPick(with: onlineODataProvider, warehouseID: warehouseID, outboundOrder: outboundOrder) { (error, successful) in
                    if let returnedError = error {
                        completionHandler(returnedError, false)
                        return
                    } else {
                        completionHandler(nil, true)
                    }
                }
            }
        }
    }
    
    static func confirmPick(with onlineODataProvider: Ec1<OnlineODataProvider>,
                            warehouseID: String,
                            outboundOrder: OUTBDLVHead,
                            completionHandler: @escaping (Error?, Bool) -> Void) {
        var completeCount = 0
        for outboundDeliveryItem in outboundOrder.items {
            let confirmPick = ConfirmPickType()
            confirmPick.outboundDeliveryUUID = outboundOrder.outboundDeliveryUUID
            confirmPick.outboundDeliveryOrderItemUUID = outboundDeliveryItem.outboundDeliveryOrderItemUUID
            onlineODataProvider.createEntity(confirmPick) { error in
                if let returnedError = error {
                    completionHandler(returnedError, false)
                    return
                } else {
                    createHandlingUnits(with: onlineODataProvider, warehouseID: warehouseID, outboundOrder: outboundOrder) { (error, succesfull) in
                        if let returnedError = error {
                            completionHandler(returnedError, false)
                        } else {
                            completeCount = completeCount + 1
                            if completeCount == outboundOrder.items.count {
                                completionHandler(nil, true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    static func createHandlingUnits(with onlineODataProvider: Ec1<OnlineODataProvider>,
                                    warehouseID: String,
                                    outboundOrder: OUTBDLVHead,
                                    completionHandler: @escaping (Error?, Bool) -> Void) {
        let handlingUnitItem = Hu()
        handlingUnitItem.huID = ""
        handlingUnitItem.lgnum = warehouseID
        handlingUnitItem.workstation = "Y921"
        handlingUnitItem.packmat = "EWMS4-PAL01"
        handlingUnitItem.docno = ""
        handlingUnitItem.bin = outboundOrder.stagingAreaBin ?? ""
        handlingUnitItem.type_ = "1"
        onlineODataProvider.createEntity(handlingUnitItem) { error in
            if let returnedError = error {
                completionHandler(returnedError, false)
                return
            } else {
                performPacking(with: onlineODataProvider, handlingUnit: handlingUnitItem, outboundOrder: outboundOrder) { (error, result) in
                    if let returnedError = error {
                        completionHandler(returnedError, false)
                    } else {
                        completionHandler(nil, true)
                    }
                }
            }
        }
    }
    
    static func performPacking(with onlineODataProvider: Ec1<OnlineODataProvider>,
                               handlingUnit: Hu,
                               outboundOrder: OUTBDLVHead,
                               completionHandler: @escaping (Error?, Bool) -> Void) {
        let perfomPackType = PerformPackType()
        perfomPackType.lgnum = handlingUnit.lgnum
        perfomPackType.workstation = handlingUnit.workstation
        perfomPackType.bin = handlingUnit.bin
        perfomPackType.sourceID = outboundOrder.outboundDelivery
        perfomPackType.shippingHUID = handlingUnit.huID
        perfomPackType.sourceType = "3"
        perfomPackType.isPackAll = true
        perfomPackType.packMat = handlingUnit.packmat
        onlineODataProvider.createEntity(perfomPackType) { error in
            if let returnedError = error {
                completionHandler(returnedError, false)
                return
            } else {
                completionHandler(nil, true)
            }
        }
    }
}
