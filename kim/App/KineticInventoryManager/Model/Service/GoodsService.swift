//
//  GoodsService.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 02/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import SAPOData
import SAPFioriFlows
import SAPFoundation

class GoodsService {
    static func fetchInboundGoods(with onlineODataProvider: Ec1<OnlineODataProvider>, warehouseId: String, completionHandler: @escaping (Error?, [DLVHead]?) -> Void) {
        let currentDate = Date().setTime(hour: 00, minute: 00, second: 00) ?? Date()
        let daysInPast = -5
        var dateComponent = DateComponents()
        dateComponent.day = daysInPast
        let pastDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)!
        let fromDate = LocalDateTime.from(utc: pastDate)
        //let toDate = LocalDateTime.from(utc: Date().setTime(hour: 00, minute: 00, second: 00) ?? Date())
        let query = DataQuery().filter(QueryOperator.greaterEqual(DLVHead.plannedDeliveryEndDate, fromDate)).expand(DLVHead.items, withQuery: DataQuery().expand(DLVItem.topHU))
        //let query = DataQuery().filter(DLVHead.warehouseNumber.equal(warehouseId)).expand(DLVHead.items, withQuery: DataQuery().expand(DLVItem.topHU))
        onlineODataProvider.fetchDLVHeadSet (matching: query) { (entities, error) in
            if let returnedError = error {
                completionHandler(returnedError, nil)
                return
            } else {
                completionHandler(error, entities)
            }
        }
    }
    
    static func fetchOutboundGoods(with onlineODataProvider: Ec1<OnlineODataProvider>, warehouseId: String, completionHandler: @escaping (Error?, [OUTBDLVHead]?) -> Void) {
        let currentDate = Date().setTime(hour: 00, minute: 00, second: 00) ?? Date()
        let daysInPast = -2
        var dateComponent = DateComponents()
        dateComponent.day = daysInPast
        let pastDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)!
        let fromDate = LocalDateTime.from(utc: pastDate)
        let query = DataQuery().filter(QueryOperator.greaterEqual(OUTBDLVHead.outOfYardDatePlan, fromDate)).expand(OUTBDLVHead.items)
        //let query = DataQuery().filter(OUTBDLVHead.warehouseNumber.equal(warehouseId)).expand(OUTBDLVHead.items)
        onlineODataProvider.fetchOUTBDLVHeadSet(matching: query) { (entities, error) in
            if let returnedError = error {
                completionHandler(returnedError, nil)
                return
            } else {
                completionHandler(error, entities)
            }
        }
    }
}
