//
//  HomeFeedService.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 01/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation
import SAPOData
import SAPFioriFlows
import SAPFoundation

class HomeFeedService {
    var ec1: Ec1<OnlineODataProvider>
    
    init(with ec1: Ec1<OnlineODataProvider>) {
        self.ec1 = ec1
    }
    
    func fetchHomeFeedData(with warehouseId: String, completionHandler: @escaping (Error?, HomeFeedData?) -> Void) {
        var homeFeedData = HomeFeedData(goodsIn: [], goodsOut: [])
        // 1. Fetch goods in
        GoodsService.fetchInboundGoods(with: self.ec1, warehouseId: warehouseId, completionHandler: { (error, goodsIn) in
            var finalError: Error?
            if let returnedError = error {
                finalError = returnedError
            }
            homeFeedData.goodsIn = goodsIn ?? []
            
            // 2. Fetch goods out
            GoodsService.fetchOutboundGoods(with: self.ec1, warehouseId: warehouseId) { (secondError, goodsOut) in
                homeFeedData.goodsOut = goodsOut ?? []
                if let returnedError = secondError {
                    completionHandler(returnedError, homeFeedData)
                }
                completionHandler(finalError, homeFeedData)
            }
        })
    }
}
