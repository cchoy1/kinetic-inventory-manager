//
//  ProductsService.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 27/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation
import SAPOData
import SAPFioriFlows
import SAPFoundation

class ProductsService {
    static func fetchInboundGoods(with onlineODataProvider: Ec1<OnlineODataProvider>, warehouseId: String, completionHandler: @escaping (Error?, [APurchaseOrderItemType]?) -> Void) {
        let query = DataQuery().filter(APurchaseOrderItemType.plant.equal(warehouseId))
        onlineODataProvider.fetchAPurchaseOrderItem (matching: query) { (entities, error) in
            if let returnedError = error {
                completionHandler(returnedError, nil)
                return
            } else {
                completionHandler(error, entities)
            }
        }
    }
}
