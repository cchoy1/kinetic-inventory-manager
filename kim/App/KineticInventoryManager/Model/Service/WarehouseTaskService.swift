//
//  WarehouseTaskService.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 20/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import SAPOData
import SAPFioriFlows
import SAPFoundation

class WarehouseTaskService {
    static func submitInboundOrder(with onlineODataProvider: Ec1<OnlineODataProvider>,
                                   deliveredOrder: DLVHead,
                                   completionHandler: @escaping (Error?, Bool) -> Void) {
        let warehouseTask = CreateWarehouseTaskType()
        warehouseTask.inboundDeliveryUUID = deliveredOrder.inboundDeliveryUUID
        onlineODataProvider.createEntity(warehouseTask) { error in
            if let returnedError = error {
                completionHandler(returnedError, false)
                return
            } else {
                fetchWarehouseTaskId(with: onlineODataProvider, deliveredOrder: deliveredOrder) { (error, succesfull) in
                    if let returnedError = error {
                        completionHandler(returnedError, false)
                    } else {
                        completionHandler(nil, true)
                    }
                }
            }
        }
    }
    
    static func fetchWarehouseTaskId(with onlineODataProvider: Ec1<OnlineODataProvider>,
                                     deliveredOrder: DLVHead,
                                     completionHandler: @escaping (Error?, Bool) -> Void) {
        if deliveredOrder.items.isEmpty == false {
            let topItem = deliveredOrder.items[0]
            let topHU = topItem.topHU
            guard let hu = topHU[0].handlingUnitID else {
                completionHandler(FetchError.fetchFailed, false)
                return
            }
            let query = DataQuery().filter(WarehouseTask.vlenr.equal("00" + hu))
            onlineODataProvider.fetchWarehouseTaskSet(matching: query) { (entities, error) in
                if let returnedError = error {
                    completionHandler(returnedError, false)
                    return
                } else {
                    if let warehouses = entities, warehouses.count > 0 {
                        let warehouse = warehouses[0]
                        guard let warehouseTaskID = warehouse.tanum, let warehouseID = warehouse.lgnum else {
                            completionHandler(FetchError.fetchFailed, false)
                            return
                        }
                        confirmTask(with: onlineODataProvider, warehouseID: warehouseTaskID, warehouseTaskID: warehouseID) { (error, succesfull) in
                            if let returnedError = error {
                                completionHandler(returnedError, false)
                            } else {
                                completionHandler(nil, true)
                            }
                        }
                    }
                }
            }
        } else {
            completionHandler(FetchError.fetchFailed, false)
        }
    }
    
    static func confirmTask(with onlineODataProvider: Ec1<OnlineODataProvider>,
                                   warehouseID: String,
                                   warehouseTaskID: String,
                                   completionHandler: @escaping (Error?, Bool) -> Void) {
        let simpleConfirm = WTSimpleConfirm()
        simpleConfirm.lgnum = warehouseTaskID
        simpleConfirm.tanum = warehouseID
        onlineODataProvider.createEntity(simpleConfirm) { error in
            if let returnedError = error {
                completionHandler(returnedError, false)
                return
            } else {
                completionHandler(nil, true)
            }
        }
    }
}
