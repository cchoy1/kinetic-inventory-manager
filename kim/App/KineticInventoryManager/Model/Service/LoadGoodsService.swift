//
//  LoadGoodsService.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 30/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import SAPOData
import SAPFioriFlows
import SAPFoundation

class LoadGoodsService {
    static func loadGoods(with onlineODataProvider: Ec1<OnlineODataProvider>,
                          outboundOrder: OUTBDLVHead,
                          completionHandler: @escaping (Error?, Bool) -> Void) {
        let warehouseTask = GoodsIssueType()
        warehouseTask.outboundDeliveryUUID = outboundOrder.outboundDeliveryUUID
        onlineODataProvider.createEntity(warehouseTask) { error in
            if let returnedError = error {
                completionHandler(returnedError, false)
                return
            } else {
                completionHandler(nil, true)
            }
        }
    }
}
