//
//  OUTBDLVHead+Extensions.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 16/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation
import SAPOData

extension OUTBDLVHead {
    func setPackedStatusComplete(for outboundItem: OUTBDLVItem) -> Bool {
        let itemId = outboundItem.outboundDeliveryItem ?? ""
        guard let index = items.firstIndex(where: { $0.outboundDeliveryItem ?? "" == itemId }) else {
            return false
        }
        outboundItem.packingStatusName = "Completed"
        items[index] = outboundItem
        updatePackedStatus()
        return true
    }
    
    func updatePackedStatus() {
        if allItemsPacked() {
            packingStatusName = "Completed"
        }
    }
    
    func allItemsPacked() -> Bool {
        for item in items {
            guard item.packingStatusName == "Completed" else {
                return false
            }
        }
        return true
    }
}

extension DLVHead {
    func setUnloadedStatusComplete(for outboundItem: DLVItem) -> Bool {
        let itemId = outboundItem.inboundDeliveryItem ?? ""
        guard let index = items.firstIndex(where: { $0.inboundDeliveryItem ?? "" == itemId }) else {
            return false
        }
        let quantity = outboundItem.itemDeliveryQuantity?.intValue() ?? 0
        outboundItem.packOpenQuantity = BigDecimal(quantity)
        outboundItem.goodsReceiptStatus = "Completed"
        items[index] = outboundItem
        updateUnloadedStatus()
        return true
    }
    
    func unloadAllItems() {
        var unloadedCount = 0
        for item in items {
            let unloaded = setUnloadedStatusComplete(for: item)
            if unloaded {
                unloadedCount = unloadedCount + 1
            }
        }
        if unloadedCount == items.count {
            updateUnloadedStatus()
        }
    }
    
    func updateUnloadedStatus() {
        if allItemsUnloaded() {
            let quantity = numberOfItems
            numberOfOpenItems = quantity
            goodsReceiptStatus = "Completed"
        }
    }
    
    func allItemsUnloaded() -> Bool {
        for item in items {
            guard item.goodsReceiptStatus == "Completed" else {
                return false
            }
        }
        return true
    }
}
