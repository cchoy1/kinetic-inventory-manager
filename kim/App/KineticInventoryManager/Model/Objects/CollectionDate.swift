//
//  CollectionDate.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 21/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

struct CollectionDate {
    var date: Date
    var hasSetTime: Bool
}
