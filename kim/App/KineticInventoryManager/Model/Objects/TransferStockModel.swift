//
//  TransferStockModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 21/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

struct TransferStockModel {
    static func dateTime(with collectionDate: CollectionDate) -> [[String: Any]] {
        var day = ""
        if collectionDate.date.isToday() {
            day = "Today"
        } else if collectionDate.date.isTommorow() {
            day = "Tommorow"
        } else if collectionDate.date.normalizedDate() == TransferStockModel.dateFromTodayByAdding(days: 2).normalizedDate() {
            day = DateFormatter.dayFormatter.string(from: TransferStockModel.dateFromTodayByAdding(days: 2))
        } else if collectionDate.date.normalizedDate() == TransferStockModel.dateFromTodayByAdding(days: 3).normalizedDate() {
            day = DateFormatter.dayFormatter.string(from: TransferStockModel.dateFromTodayByAdding(days: 3))
        } else {
            day = "Today"
        }
        
        var time = ""
        if collectionDate.hasSetTime {
            time = DateFormatter.simpleTimeFormatter.string(from: collectionDate.date)
        }
        
        let items: [[String : Any]] = [
            [
                "id": "day",
                "title": "Day",
                "detail": day,
                "type": "day",
                "accessoryType": UITableViewCell.AccessoryType.disclosureIndicator,
            ],
            [
                "id": "time",
                "title": "Time",
                "detail": time,
                "type": "time",
                "accessoryType": UITableViewCell.AccessoryType.disclosureIndicator,
            ],
        ]
        return items
    }
    
    static func format(collectionDate: CollectionDate) -> String {
        var day = ""
        if collectionDate.date.isToday() {
            day = "Today"
        } else if collectionDate.date.isTommorow() {
            day = "Tommorow"
        } else if collectionDate.date.normalizedDate() == TransferStockModel.dateFromTodayByAdding(days: 2).normalizedDate() {
            day = DateFormatter.dayFormatter.string(from: TransferStockModel.dateFromTodayByAdding(days: 2))
        } else if collectionDate.date.normalizedDate() == TransferStockModel.dateFromTodayByAdding(days: 3).normalizedDate() {
            day = DateFormatter.dayFormatter.string(from: TransferStockModel.dateFromTodayByAdding(days: 3))
        } else {
            day = "Today"
        }
        
        var time = ""
        if collectionDate.hasSetTime {
            time = DateFormatter.simpleTimeFormatter.string(from: collectionDate.date)
        }
        
        return "\(day) at \(time)"
    }
    
    static func days() -> [[String: Any]] {
        let items: [[String : Any]] = [
            [
                "id": UUID().uuidString,
                "title": "Today",
                "type": "day",
                "date": Date(),
            ],
            [
                "id": UUID().uuidString,
                "title": "Tomorrow",
                "type": "day",
                "date": TransferStockModel.dateFromTodayByAdding(days: 1),
            ],
            [
                "id": UUID().uuidString,
                "title": DateFormatter.dayFormatter.string(from: TransferStockModel.dateFromTodayByAdding(days: 2)),
                "type": "day",
                "date": TransferStockModel.dateFromTodayByAdding(days: 2),
            ],
            [
                "id": UUID().uuidString,
                "title": DateFormatter.dayFormatter.string(from: TransferStockModel.dateFromTodayByAdding(days: 3)),
                "type": "day",
                "date": TransferStockModel.dateFromTodayByAdding(days: 3),
            ]
        ]
        return items
    }
    
    static func timeSlots(with date: Date) -> [[String: Any]] {
        let items: [[String : Any]] = [
            [
                "id": UUID().uuidString,
                "title": "2pm",
                "type": "time",
                "date": date.setTime(hour: 14, minute: 0, second: 0)!,
            ],
            [
                "id": UUID().uuidString,
                "title": "6pm",
                "type": "time",
                "date": date.setTime(hour: 18, minute: 0, second: 0)!,
            ],
            [
                "id": UUID().uuidString,
                "title": "8pm",
                "type": "time",
                "date": date.setTime(hour: 20, minute: 0, second: 0)!,
            ],
            [
                "id": UUID().uuidString,
                "title": "10pm",
                "type": "time",
                "date": date.setTime(hour: 22, minute: 0, second: 0)!,
            ]
        ]
        return items
    }
    
    static func couriers() -> [[String: Any]] {
        let items: [[String : Any]] = [
            [
                "id": UUID().uuidString,
                "title": "DHL",
                "detail": "Only use this courier when shipping is urgent and\nweighing less than 5kg.",
                "type": "courier",
            ],
            [
                "id": UUID().uuidString,
                "title": "Internal Courieer (Recommended)",
                "detail": "This is recommended for shipping day-to-day packages\n of any size.",
                "type": "courier",
            ],
            [
                "id": UUID().uuidString,
                "title": "ParcelForce",
                "detail": "Only use this courier when shippong is irgent and\nweighing more than 5g.",
                "type": "courier",
            ]
        ]
        return items
    }
    
    static func dateFromTodayByAdding(days: Int) -> Date {
        return Date().dateByAdding(numberOfDays: days)!
    }
}
