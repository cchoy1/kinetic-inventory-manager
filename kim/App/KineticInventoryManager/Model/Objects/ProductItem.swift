//
//  ProductItem.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 27/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

struct ProductItem {
    let productPlantType: AProductPlantType
    let product: AProductType
}
