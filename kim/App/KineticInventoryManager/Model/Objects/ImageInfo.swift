//
//  ImageInfo.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 25/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

enum ImageType {
    case sfSymbol
    case asset
    case url
}

struct ImageInfo {
    let imageURL: String
    let imageType: ImageType
    var tintColour: UIColor = .systemBlue
}

extension ImageInfo {
    static func image(with imageInfo: ImageInfo) -> UIImage? {
        switch imageInfo.imageType {
        case .sfSymbol:
            if #available(iOS 13.0, *) {
                return UIImage(systemName: imageInfo.imageURL)
            } else {
                return nil
            }
        case .asset:
            return UIImage(named: imageInfo.imageURL)
        case .url:
            return nil
        }
    }
}
