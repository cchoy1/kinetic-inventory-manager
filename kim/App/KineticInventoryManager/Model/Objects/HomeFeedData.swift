//
//  HomeFeedData.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 14/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

import Foundation
import SAPOData
import SAPFioriFlows
import SAPFoundation

struct HomeFeedData {
    var goodsIn: [DLVHead]
    var goodsOut: [OUTBDLVHead]
}
