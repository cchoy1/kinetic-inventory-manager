// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class APurchaseOrderItemType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var purchaseOrder_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseOrder")

    private static var purchaseOrderItem_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseOrderItem")

    private static var purchasingDocumentDeletionCode_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchasingDocumentDeletionCode")

    private static var purchaseOrderItemText_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseOrderItemText")

    private static var plant_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "Plant")

    private static var storageLocation_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "StorageLocation")

    private static var materialGroup_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "MaterialGroup")

    private static var purchasingInfoRecord_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchasingInfoRecord")

    private static var supplierMaterialNumber_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "SupplierMaterialNumber")

    private static var orderQuantity_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "OrderQuantity")

    private static var purchaseOrderQuantityUnit_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseOrderQuantityUnit")

    private static var orderPriceUnit_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "OrderPriceUnit")

    private static var orderPriceUnitToOrderUnitNmrtr_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "OrderPriceUnitToOrderUnitNmrtr")

    private static var ordPriceUnitToOrderUnitDnmntr_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "OrdPriceUnitToOrderUnitDnmntr")

    private static var documentCurrency_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DocumentCurrency")

    private static var netPriceAmount_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "NetPriceAmount")

    private static var netPriceQuantity_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "NetPriceQuantity")

    private static var taxCode_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "TaxCode")

    private static var priceIsToBePrinted_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PriceIsToBePrinted")

    private static var overdelivTolrtdLmtRatioInPct_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "OverdelivTolrtdLmtRatioInPct")

    private static var unlimitedOverdeliveryIsAllowed_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "UnlimitedOverdeliveryIsAllowed")

    private static var underdelivTolrtdLmtRatioInPct_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "UnderdelivTolrtdLmtRatioInPct")

    private static var valuationType_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ValuationType")

    private static var isCompletelyDelivered_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IsCompletelyDelivered")

    private static var isFinallyInvoiced_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IsFinallyInvoiced")

    private static var purchaseOrderItemCategory_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseOrderItemCategory")

    private static var accountAssignmentCategory_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "AccountAssignmentCategory")

    private static var multipleAcctAssgmtDistribution_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "MultipleAcctAssgmtDistribution")

    private static var partialInvoiceDistribution_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PartialInvoiceDistribution")

    private static var goodsReceiptIsExpected_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "GoodsReceiptIsExpected")

    private static var goodsReceiptIsNonValuated_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "GoodsReceiptIsNonValuated")

    private static var invoiceIsExpected_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "InvoiceIsExpected")

    private static var invoiceIsGoodsReceiptBased_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "InvoiceIsGoodsReceiptBased")

    private static var purchaseContract_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseContract")

    private static var purchaseContractItem_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseContractItem")

    private static var customer_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "Customer")

    private static var itemNetWeight_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ItemNetWeight")

    private static var itemWeightUnit_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ItemWeightUnit")

    private static var taxJurisdiction_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "TaxJurisdiction")

    private static var pricingDateControl_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PricingDateControl")

    private static var itemVolume_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ItemVolume")

    private static var itemVolumeUnit_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ItemVolumeUnit")

    private static var supplierConfirmationControlKey_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "SupplierConfirmationControlKey")

    private static var incotermsClassification_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IncotermsClassification")

    private static var incotermsTransferLocation_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IncotermsTransferLocation")

    private static var evaldRcptSettlmtIsAllowed_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "EvaldRcptSettlmtIsAllowed")

    private static var purchaseRequisition_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseRequisition")

    private static var purchaseRequisitionItem_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseRequisitionItem")

    private static var isReturnsItem_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IsReturnsItem")

    private static var requisitionerName_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "RequisitionerName")

    private static var servicePackage_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ServicePackage")

    private static var earmarkedFunds_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "EarmarkedFunds")

    private static var earmarkedFundsDocument_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "EarmarkedFundsDocument")

    private static var earmarkedFundsItem_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "EarmarkedFundsItem")

    private static var earmarkedFundsDocumentItem_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "EarmarkedFundsDocumentItem")

    private static var incotermsLocation1_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IncotermsLocation1")

    private static var incotermsLocation2_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IncotermsLocation2")

    private static var material_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "Material")

    private static var internationalArticleNumber_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "InternationalArticleNumber")

    private static var manufacturerMaterial_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ManufacturerMaterial")

    private static var servicePerformer_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ServicePerformer")

    private static var productType_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ProductType")

    private static var expectedOverallLimitAmount_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ExpectedOverallLimitAmount")

    private static var overallLimitAmount_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "OverallLimitAmount")

    private static var deliveryAddressID_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressID")

    private static var deliveryAddressName_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressName")

    private static var deliveryAddressStreetName_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressStreetName")

    private static var deliveryAddressHouseNumber_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressHouseNumber")

    private static var deliveryAddressCityName_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressCityName")

    private static var deliveryAddressPostalCode_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressPostalCode")

    private static var deliveryAddressRegion_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressRegion")

    private static var deliveryAddressCountry_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressCountry")

    private static var brMaterialUsage_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "BR_MaterialUsage")

    private static var brMaterialOrigin_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "BR_MaterialOrigin")

    private static var brCFOPCategory_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "BR_CFOPCategory")

    private static var brIsProducedInHouse_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "BR_IsProducedInHouse")

    private static var consumptionTaxCtrlCode_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ConsumptionTaxCtrlCode")

    private static var stockSegment_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "StockSegment")

    private static var requirementSegment_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "RequirementSegment")

    private static var toAccountAssignment_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "to_AccountAssignment")

    private static var toPurchaseOrderPricingElement_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "to_PurchaseOrderPricingElement")

    private static var toScheduleLine_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "to_ScheduleLine")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aPurchaseOrderItemType)
    }

    open class var accountAssignmentCategory: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.accountAssignmentCategory_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.accountAssignmentCategory_ = value
            }
        }
    }

    open var accountAssignmentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.accountAssignmentCategory))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.accountAssignmentCategory, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [APurchaseOrderItemType] {
        return ArrayConverter.convert(from.toArray(), [APurchaseOrderItemType]())
    }

    open class var brCFOPCategory: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.brCFOPCategory_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.brCFOPCategory_ = value
            }
        }
    }

    open var brCFOPCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.brCFOPCategory))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.brCFOPCategory, to: StringValue.of(optional: value))
        }
    }

    open class var brIsProducedInHouse: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.brIsProducedInHouse_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.brIsProducedInHouse_ = value
            }
        }
    }

    open var brIsProducedInHouse: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderItemType.brIsProducedInHouse))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.brIsProducedInHouse, to: BooleanValue.of(optional: value))
        }
    }

    open class var brMaterialOrigin: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.brMaterialOrigin_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.brMaterialOrigin_ = value
            }
        }
    }

    open var brMaterialOrigin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.brMaterialOrigin))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.brMaterialOrigin, to: StringValue.of(optional: value))
        }
    }

    open class var brMaterialUsage: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.brMaterialUsage_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.brMaterialUsage_ = value
            }
        }
    }

    open var brMaterialUsage: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.brMaterialUsage))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.brMaterialUsage, to: StringValue.of(optional: value))
        }
    }

    open class var consumptionTaxCtrlCode: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.consumptionTaxCtrlCode_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.consumptionTaxCtrlCode_ = value
            }
        }
    }

    open var consumptionTaxCtrlCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.consumptionTaxCtrlCode))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.consumptionTaxCtrlCode, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> APurchaseOrderItemType {
        return CastRequired<APurchaseOrderItemType>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.customer_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.customer))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryAddressCityName: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.deliveryAddressCityName_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.deliveryAddressCityName_ = value
            }
        }
    }

    open var deliveryAddressCityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.deliveryAddressCityName))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.deliveryAddressCityName, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryAddressCountry: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.deliveryAddressCountry_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.deliveryAddressCountry_ = value
            }
        }
    }

    open var deliveryAddressCountry: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.deliveryAddressCountry))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.deliveryAddressCountry, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryAddressHouseNumber: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.deliveryAddressHouseNumber_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.deliveryAddressHouseNumber_ = value
            }
        }
    }

    open var deliveryAddressHouseNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.deliveryAddressHouseNumber))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.deliveryAddressHouseNumber, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryAddressID: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.deliveryAddressID_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.deliveryAddressID_ = value
            }
        }
    }

    open var deliveryAddressID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.deliveryAddressID))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.deliveryAddressID, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryAddressName: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.deliveryAddressName_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.deliveryAddressName_ = value
            }
        }
    }

    open var deliveryAddressName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.deliveryAddressName))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.deliveryAddressName, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryAddressPostalCode: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.deliveryAddressPostalCode_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.deliveryAddressPostalCode_ = value
            }
        }
    }

    open var deliveryAddressPostalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.deliveryAddressPostalCode))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.deliveryAddressPostalCode, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryAddressRegion: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.deliveryAddressRegion_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.deliveryAddressRegion_ = value
            }
        }
    }

    open var deliveryAddressRegion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.deliveryAddressRegion))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.deliveryAddressRegion, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryAddressStreetName: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.deliveryAddressStreetName_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.deliveryAddressStreetName_ = value
            }
        }
    }

    open var deliveryAddressStreetName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.deliveryAddressStreetName))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.deliveryAddressStreetName, to: StringValue.of(optional: value))
        }
    }

    open class var documentCurrency: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.documentCurrency_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.documentCurrency_ = value
            }
        }
    }

    open var documentCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.documentCurrency))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.documentCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var earmarkedFunds: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.earmarkedFunds_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.earmarkedFunds_ = value
            }
        }
    }

    open var earmarkedFunds: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.earmarkedFunds))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.earmarkedFunds, to: StringValue.of(optional: value))
        }
    }

    open class var earmarkedFundsDocument: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.earmarkedFundsDocument_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.earmarkedFundsDocument_ = value
            }
        }
    }

    open var earmarkedFundsDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.earmarkedFundsDocument))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.earmarkedFundsDocument, to: StringValue.of(optional: value))
        }
    }

    open class var earmarkedFundsDocumentItem: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.earmarkedFundsDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.earmarkedFundsDocumentItem_ = value
            }
        }
    }

    open var earmarkedFundsDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.earmarkedFundsDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.earmarkedFundsDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var earmarkedFundsItem: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.earmarkedFundsItem_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.earmarkedFundsItem_ = value
            }
        }
    }

    open var earmarkedFundsItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.earmarkedFundsItem))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.earmarkedFundsItem, to: StringValue.of(optional: value))
        }
    }

    open class var evaldRcptSettlmtIsAllowed: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.evaldRcptSettlmtIsAllowed_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.evaldRcptSettlmtIsAllowed_ = value
            }
        }
    }

    open var evaldRcptSettlmtIsAllowed: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderItemType.evaldRcptSettlmtIsAllowed))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.evaldRcptSettlmtIsAllowed, to: BooleanValue.of(optional: value))
        }
    }

    open class var expectedOverallLimitAmount: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.expectedOverallLimitAmount_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.expectedOverallLimitAmount_ = value
            }
        }
    }

    open var expectedOverallLimitAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurchaseOrderItemType.expectedOverallLimitAmount))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.expectedOverallLimitAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var goodsReceiptIsExpected: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.goodsReceiptIsExpected_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.goodsReceiptIsExpected_ = value
            }
        }
    }

    open var goodsReceiptIsExpected: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderItemType.goodsReceiptIsExpected))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.goodsReceiptIsExpected, to: BooleanValue.of(optional: value))
        }
    }

    open class var goodsReceiptIsNonValuated: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.goodsReceiptIsNonValuated_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.goodsReceiptIsNonValuated_ = value
            }
        }
    }

    open var goodsReceiptIsNonValuated: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderItemType.goodsReceiptIsNonValuated))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.goodsReceiptIsNonValuated, to: BooleanValue.of(optional: value))
        }
    }

    open class var incotermsClassification: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.incotermsClassification_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.incotermsClassification_ = value
            }
        }
    }

    open var incotermsClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.incotermsClassification))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.incotermsClassification, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation1: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.incotermsLocation1_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.incotermsLocation1_ = value
            }
        }
    }

    open var incotermsLocation1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.incotermsLocation1))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.incotermsLocation1, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation2: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.incotermsLocation2_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.incotermsLocation2_ = value
            }
        }
    }

    open var incotermsLocation2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.incotermsLocation2))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.incotermsLocation2, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsTransferLocation: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.incotermsTransferLocation_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.incotermsTransferLocation_ = value
            }
        }
    }

    open var incotermsTransferLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.incotermsTransferLocation))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.incotermsTransferLocation, to: StringValue.of(optional: value))
        }
    }

    open class var internationalArticleNumber: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.internationalArticleNumber_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.internationalArticleNumber_ = value
            }
        }
    }

    open var internationalArticleNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.internationalArticleNumber))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.internationalArticleNumber, to: StringValue.of(optional: value))
        }
    }

    open class var invoiceIsExpected: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.invoiceIsExpected_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.invoiceIsExpected_ = value
            }
        }
    }

    open var invoiceIsExpected: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderItemType.invoiceIsExpected))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.invoiceIsExpected, to: BooleanValue.of(optional: value))
        }
    }

    open class var invoiceIsGoodsReceiptBased: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.invoiceIsGoodsReceiptBased_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.invoiceIsGoodsReceiptBased_ = value
            }
        }
    }

    open var invoiceIsGoodsReceiptBased: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderItemType.invoiceIsGoodsReceiptBased))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.invoiceIsGoodsReceiptBased, to: BooleanValue.of(optional: value))
        }
    }

    open class var isCompletelyDelivered: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.isCompletelyDelivered_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.isCompletelyDelivered_ = value
            }
        }
    }

    open var isCompletelyDelivered: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderItemType.isCompletelyDelivered))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.isCompletelyDelivered, to: BooleanValue.of(optional: value))
        }
    }

    open class var isFinallyInvoiced: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.isFinallyInvoiced_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.isFinallyInvoiced_ = value
            }
        }
    }

    open var isFinallyInvoiced: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderItemType.isFinallyInvoiced))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.isFinallyInvoiced, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var isReturnsItem: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.isReturnsItem_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.isReturnsItem_ = value
            }
        }
    }

    open var isReturnsItem: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderItemType.isReturnsItem))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.isReturnsItem, to: BooleanValue.of(optional: value))
        }
    }

    open class var itemNetWeight: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.itemNetWeight_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.itemNetWeight_ = value
            }
        }
    }

    open var itemNetWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurchaseOrderItemType.itemNetWeight))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.itemNetWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemVolume: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.itemVolume_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.itemVolume_ = value
            }
        }
    }

    open var itemVolume: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurchaseOrderItemType.itemVolume))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.itemVolume, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemVolumeUnit: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.itemVolumeUnit_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.itemVolumeUnit_ = value
            }
        }
    }

    open var itemVolumeUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.itemVolumeUnit))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.itemVolumeUnit, to: StringValue.of(optional: value))
        }
    }

    open class var itemWeightUnit: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.itemWeightUnit_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.itemWeightUnit_ = value
            }
        }
    }

    open var itemWeightUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.itemWeightUnit))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.itemWeightUnit, to: StringValue.of(optional: value))
        }
    }

    open class func key(purchaseOrder: String?, purchaseOrderItem: String?) -> EntityKey {
        return EntityKey().with(name: "PurchaseOrder", value: StringValue.of(optional: purchaseOrder)).with(name: "PurchaseOrderItem", value: StringValue.of(optional: purchaseOrderItem))
    }

    open class var manufacturerMaterial: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.manufacturerMaterial_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.manufacturerMaterial_ = value
            }
        }
    }

    open var manufacturerMaterial: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.manufacturerMaterial))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.manufacturerMaterial, to: StringValue.of(optional: value))
        }
    }

    open class var material: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.material_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.material_ = value
            }
        }
    }

    open var material: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.material))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.material, to: StringValue.of(optional: value))
        }
    }

    open class var materialGroup: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.materialGroup_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.materialGroup_ = value
            }
        }
    }

    open var materialGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.materialGroup))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.materialGroup, to: StringValue.of(optional: value))
        }
    }

    open class var multipleAcctAssgmtDistribution: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.multipleAcctAssgmtDistribution_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.multipleAcctAssgmtDistribution_ = value
            }
        }
    }

    open var multipleAcctAssgmtDistribution: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.multipleAcctAssgmtDistribution))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.multipleAcctAssgmtDistribution, to: StringValue.of(optional: value))
        }
    }

    open class var netPriceAmount: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.netPriceAmount_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.netPriceAmount_ = value
            }
        }
    }

    open var netPriceAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurchaseOrderItemType.netPriceAmount))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.netPriceAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var netPriceQuantity: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.netPriceQuantity_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.netPriceQuantity_ = value
            }
        }
    }

    open var netPriceQuantity: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: APurchaseOrderItemType.netPriceQuantity))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.netPriceQuantity, to: IntegerValue.of(optional: value))
        }
    }

    open var old: APurchaseOrderItemType {
        return CastRequired<APurchaseOrderItemType>.from(self.oldEntity)
    }

    open class var ordPriceUnitToOrderUnitDnmntr: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.ordPriceUnitToOrderUnitDnmntr_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.ordPriceUnitToOrderUnitDnmntr_ = value
            }
        }
    }

    open var ordPriceUnitToOrderUnitDnmntr: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: APurchaseOrderItemType.ordPriceUnitToOrderUnitDnmntr))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.ordPriceUnitToOrderUnitDnmntr, to: IntegerValue.of(optional: value))
        }
    }

    open class var orderPriceUnit: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.orderPriceUnit_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.orderPriceUnit_ = value
            }
        }
    }

    open var orderPriceUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.orderPriceUnit))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.orderPriceUnit, to: StringValue.of(optional: value))
        }
    }

    open class var orderPriceUnitToOrderUnitNmrtr: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.orderPriceUnitToOrderUnitNmrtr_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.orderPriceUnitToOrderUnitNmrtr_ = value
            }
        }
    }

    open var orderPriceUnitToOrderUnitNmrtr: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: APurchaseOrderItemType.orderPriceUnitToOrderUnitNmrtr))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.orderPriceUnitToOrderUnitNmrtr, to: IntegerValue.of(optional: value))
        }
    }

    open class var orderQuantity: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.orderQuantity_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.orderQuantity_ = value
            }
        }
    }

    open var orderQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurchaseOrderItemType.orderQuantity))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.orderQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var overallLimitAmount: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.overallLimitAmount_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.overallLimitAmount_ = value
            }
        }
    }

    open var overallLimitAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurchaseOrderItemType.overallLimitAmount))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.overallLimitAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var overdelivTolrtdLmtRatioInPct: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.overdelivTolrtdLmtRatioInPct_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.overdelivTolrtdLmtRatioInPct_ = value
            }
        }
    }

    open var overdelivTolrtdLmtRatioInPct: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurchaseOrderItemType.overdelivTolrtdLmtRatioInPct))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.overdelivTolrtdLmtRatioInPct, to: DecimalValue.of(optional: value))
        }
    }

    open class var partialInvoiceDistribution: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.partialInvoiceDistribution_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.partialInvoiceDistribution_ = value
            }
        }
    }

    open var partialInvoiceDistribution: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.partialInvoiceDistribution))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.partialInvoiceDistribution, to: StringValue.of(optional: value))
        }
    }

    open class var plant: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.plant_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.plant_ = value
            }
        }
    }

    open var plant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.plant))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.plant, to: StringValue.of(optional: value))
        }
    }

    open class var priceIsToBePrinted: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.priceIsToBePrinted_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.priceIsToBePrinted_ = value
            }
        }
    }

    open var priceIsToBePrinted: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderItemType.priceIsToBePrinted))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.priceIsToBePrinted, to: BooleanValue.of(optional: value))
        }
    }

    open class var pricingDateControl: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.pricingDateControl_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.pricingDateControl_ = value
            }
        }
    }

    open var pricingDateControl: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.pricingDateControl))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.pricingDateControl, to: StringValue.of(optional: value))
        }
    }

    open class var productType: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.productType_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.productType_ = value
            }
        }
    }

    open var productType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.productType))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.productType, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseContract: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.purchaseContract_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.purchaseContract_ = value
            }
        }
    }

    open var purchaseContract: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.purchaseContract))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.purchaseContract, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseContractItem: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.purchaseContractItem_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.purchaseContractItem_ = value
            }
        }
    }

    open var purchaseContractItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.purchaseContractItem))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.purchaseContractItem, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrder: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.purchaseOrder_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.purchaseOrder_ = value
            }
        }
    }

    open var purchaseOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.purchaseOrder))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.purchaseOrder, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderItem: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.purchaseOrderItem_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.purchaseOrderItem_ = value
            }
        }
    }

    open var purchaseOrderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.purchaseOrderItem))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.purchaseOrderItem, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderItemCategory: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.purchaseOrderItemCategory_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.purchaseOrderItemCategory_ = value
            }
        }
    }

    open var purchaseOrderItemCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.purchaseOrderItemCategory))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.purchaseOrderItemCategory, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderItemText: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.purchaseOrderItemText_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.purchaseOrderItemText_ = value
            }
        }
    }

    open var purchaseOrderItemText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.purchaseOrderItemText))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.purchaseOrderItemText, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderQuantityUnit: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.purchaseOrderQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.purchaseOrderQuantityUnit_ = value
            }
        }
    }

    open var purchaseOrderQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.purchaseOrderQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.purchaseOrderQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseRequisition: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.purchaseRequisition_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.purchaseRequisition_ = value
            }
        }
    }

    open var purchaseRequisition: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.purchaseRequisition))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.purchaseRequisition, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseRequisitionItem: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.purchaseRequisitionItem_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.purchaseRequisitionItem_ = value
            }
        }
    }

    open var purchaseRequisitionItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.purchaseRequisitionItem))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.purchaseRequisitionItem, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingDocumentDeletionCode: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.purchasingDocumentDeletionCode_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.purchasingDocumentDeletionCode_ = value
            }
        }
    }

    open var purchasingDocumentDeletionCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.purchasingDocumentDeletionCode))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.purchasingDocumentDeletionCode, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingInfoRecord: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.purchasingInfoRecord_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.purchasingInfoRecord_ = value
            }
        }
    }

    open var purchasingInfoRecord: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.purchasingInfoRecord))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.purchasingInfoRecord, to: StringValue.of(optional: value))
        }
    }

    open class var requirementSegment: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.requirementSegment_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.requirementSegment_ = value
            }
        }
    }

    open var requirementSegment: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.requirementSegment))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.requirementSegment, to: StringValue.of(optional: value))
        }
    }

    open class var requisitionerName: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.requisitionerName_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.requisitionerName_ = value
            }
        }
    }

    open var requisitionerName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.requisitionerName))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.requisitionerName, to: StringValue.of(optional: value))
        }
    }

    open class var servicePackage: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.servicePackage_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.servicePackage_ = value
            }
        }
    }

    open var servicePackage: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.servicePackage))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.servicePackage, to: StringValue.of(optional: value))
        }
    }

    open class var servicePerformer: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.servicePerformer_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.servicePerformer_ = value
            }
        }
    }

    open var servicePerformer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.servicePerformer))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.servicePerformer, to: StringValue.of(optional: value))
        }
    }

    open class var stockSegment: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.stockSegment_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.stockSegment_ = value
            }
        }
    }

    open var stockSegment: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.stockSegment))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.stockSegment, to: StringValue.of(optional: value))
        }
    }

    open class var storageLocation: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.storageLocation_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.storageLocation_ = value
            }
        }
    }

    open var storageLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.storageLocation))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.storageLocation, to: StringValue.of(optional: value))
        }
    }

    open class var supplierConfirmationControlKey: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.supplierConfirmationControlKey_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.supplierConfirmationControlKey_ = value
            }
        }
    }

    open var supplierConfirmationControlKey: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.supplierConfirmationControlKey))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.supplierConfirmationControlKey, to: StringValue.of(optional: value))
        }
    }

    open class var supplierMaterialNumber: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.supplierMaterialNumber_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.supplierMaterialNumber_ = value
            }
        }
    }

    open var supplierMaterialNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.supplierMaterialNumber))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.supplierMaterialNumber, to: StringValue.of(optional: value))
        }
    }

    open class var taxCode: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.taxCode_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.taxCode_ = value
            }
        }
    }

    open var taxCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.taxCode))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.taxCode, to: StringValue.of(optional: value))
        }
    }

    open class var taxJurisdiction: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.taxJurisdiction_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.taxJurisdiction_ = value
            }
        }
    }

    open var taxJurisdiction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.taxJurisdiction))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.taxJurisdiction, to: StringValue.of(optional: value))
        }
    }

    open class var toAccountAssignment: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.toAccountAssignment_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.toAccountAssignment_ = value
            }
        }
    }

    open var toAccountAssignment: [APurOrdAccountAssignmentType] {
        get {
            return ArrayConverter.convert(APurchaseOrderItemType.toAccountAssignment.entityList(from: self).toArray(), [APurOrdAccountAssignmentType]())
        }
        set(value) {
            APurchaseOrderItemType.toAccountAssignment.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class var toPurchaseOrderPricingElement: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.toPurchaseOrderPricingElement_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.toPurchaseOrderPricingElement_ = value
            }
        }
    }

    open var toPurchaseOrderPricingElement: [APurOrdPricingElementType] {
        get {
            return ArrayConverter.convert(APurchaseOrderItemType.toPurchaseOrderPricingElement.entityList(from: self).toArray(), [APurOrdPricingElementType]())
        }
        set(value) {
            APurchaseOrderItemType.toPurchaseOrderPricingElement.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class var toScheduleLine: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.toScheduleLine_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.toScheduleLine_ = value
            }
        }
    }

    open var toScheduleLine: [APurchaseOrderScheduleLineType] {
        get {
            return ArrayConverter.convert(APurchaseOrderItemType.toScheduleLine.entityList(from: self).toArray(), [APurchaseOrderScheduleLineType]())
        }
        set(value) {
            APurchaseOrderItemType.toScheduleLine.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class var underdelivTolrtdLmtRatioInPct: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.underdelivTolrtdLmtRatioInPct_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.underdelivTolrtdLmtRatioInPct_ = value
            }
        }
    }

    open var underdelivTolrtdLmtRatioInPct: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurchaseOrderItemType.underdelivTolrtdLmtRatioInPct))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.underdelivTolrtdLmtRatioInPct, to: DecimalValue.of(optional: value))
        }
    }

    open class var unlimitedOverdeliveryIsAllowed: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.unlimitedOverdeliveryIsAllowed_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.unlimitedOverdeliveryIsAllowed_ = value
            }
        }
    }

    open var unlimitedOverdeliveryIsAllowed: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderItemType.unlimitedOverdeliveryIsAllowed))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.unlimitedOverdeliveryIsAllowed, to: BooleanValue.of(optional: value))
        }
    }

    open class var valuationType: Property {
        get {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                return APurchaseOrderItemType.valuationType_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderItemType.self)
            defer { objc_sync_exit(APurchaseOrderItemType.self) }
            do {
                APurchaseOrderItemType.valuationType_ = value
            }
        }
    }

    open var valuationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderItemType.valuationType))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderItemType.valuationType, to: StringValue.of(optional: value))
        }
    }
}
