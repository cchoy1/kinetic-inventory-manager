// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class Ec1<Provider: DataServiceProvider>: DataService<Provider> {
    public override init(provider: Provider) {
        super.init(provider: provider)
        self.provider.metadata = Ec1Metadata.document
    }

    open func createTask(inboundDeliveryUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ReturnActionsType {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<ReturnActionsType>.from(self.executeQuery(var_query.invoke(Ec1Metadata.ActionImports.createTask, ParameterList(capacity: 1 as Int).with(name: "InboundDeliveryUUID", value: inboundDeliveryUUID)), headers: var_headers, options: var_options).result)
    }

    open func createTask(inboundDeliveryUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ReturnActionsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.createTask(inboundDeliveryUUID: inboundDeliveryUUID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryAddressType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryAddressType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryAddressType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryAddressTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryAddressType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryAddressType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryAddressType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryAddressTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryAddressType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryAddressType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryAddressTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryAddressTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryAddressType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryAddressTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryAddressTypeWithKey(addressID: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryAddressType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryAddressType(matching: var_query.withKey(AInbDeliveryAddressType.key(addressID: addressID)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryAddressTypeWithKey(addressID: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryAddressType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryAddressTypeWithKey(addressID: addressID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryDocFlowType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryDocFlowType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryDocFlowType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryDocFlowTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryDocFlowType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryDocFlowType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryDocFlowType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryDocFlowTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryDocFlowType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryDocFlowType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryDocFlowTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryDocFlowTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryDocFlowType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryDocFlowTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryDocFlowTypeWithKey(precedingDocument: String?, precedingDocumentItem: String?, subsequentDocumentCategory: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryDocFlowType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryDocFlowType(matching: var_query.withKey(AInbDeliveryDocFlowType.key(precedingDocument: precedingDocument, precedingDocumentItem: precedingDocumentItem, subsequentDocumentCategory: subsequentDocumentCategory)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryDocFlowTypeWithKey(precedingDocument: String?, precedingDocumentItem: String?, subsequentDocumentCategory: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryDocFlowType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryDocFlowTypeWithKey(precedingDocument: precedingDocument, precedingDocumentItem: precedingDocumentItem, subsequentDocumentCategory: subsequentDocumentCategory, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryHeader(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryHeaderType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryHeaderType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryHeader), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryHeader(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryHeaderType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryHeader(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryHeaderTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryHeaderTextType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryHeaderTextType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryHeaderTextTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryHeaderTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryHeaderTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryHeaderTextType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryHeaderTextTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryHeaderTextType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryHeaderTextType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryHeaderTextTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryHeaderTextTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryHeaderTextType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryHeaderTextTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryHeaderTextTypeWithKey(deliveryDocument: String?, textElement: String?, language: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryHeaderTextType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryHeaderTextType(matching: var_query.withKey(AInbDeliveryHeaderTextType.key(deliveryDocument: deliveryDocument, textElement: textElement, language: language)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryHeaderTextTypeWithKey(deliveryDocument: String?, textElement: String?, language: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryHeaderTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryHeaderTextTypeWithKey(deliveryDocument: deliveryDocument, textElement: textElement, language: language, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryHeaderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryHeaderType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryHeaderType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryHeader), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryHeaderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryHeaderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryHeaderType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryHeaderTypeWithKey(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryHeaderType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryHeaderType(matching: var_query.withKey(AInbDeliveryHeaderType.key(deliveryDocument: deliveryDocument)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryHeaderTypeWithKey(deliveryDocument: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryHeaderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryHeaderTypeWithKey(deliveryDocument: deliveryDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryItem(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryItemType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryItemType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryItem), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryItem(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryItemType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryItem(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryItemTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryItemTextType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryItemTextType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryItemTextTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryItemTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryItemTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryItemTextType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryItemTextTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryItemTextType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryItemTextType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryItemTextTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryItemTextTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryItemTextType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryItemTextTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryItemTextTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, textElement: String?, language: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryItemTextType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryItemTextType(matching: var_query.withKey(AInbDeliveryItemTextType.key(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, textElement: textElement, language: language)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryItemTextTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, textElement: String?, language: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryItemTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryItemTextTypeWithKey(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, textElement: textElement, language: language, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryItemType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryItemType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryItem), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryItemType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryItemTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryItemType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryItemType(matching: var_query.withKey(AInbDeliveryItemType.key(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryItemTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryItemTypeWithKey(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryPartner(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliveryPartnerType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliveryPartnerType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryPartner), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliveryPartner(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliveryPartnerType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryPartner(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryPartnerType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliveryPartnerType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliveryPartner), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliveryPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryPartnerType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliveryPartnerTypeWithKey(partnerFunction: String?, sdDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliveryPartnerType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliveryPartnerType(matching: var_query.withKey(AInbDeliveryPartnerType.key(partnerFunction: partnerFunction, sdDocument: sdDocument)), headers: headers, options: options)
    }

    open func fetchAInbDeliveryPartnerTypeWithKey(partnerFunction: String?, sdDocument: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliveryPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliveryPartnerTypeWithKey(partnerFunction: partnerFunction, sdDocument: sdDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliverySerialNmbrType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliverySerialNmbrType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AInbDeliverySerialNmbrType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aInbDeliverySerialNmbrTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAInbDeliverySerialNmbrType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliverySerialNmbrType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliverySerialNmbrType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliverySerialNmbrTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AInbDeliverySerialNmbrType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AInbDeliverySerialNmbrType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aInbDeliverySerialNmbrTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAInbDeliverySerialNmbrTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AInbDeliverySerialNmbrType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliverySerialNmbrTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAInbDeliverySerialNmbrTypeWithKey(maintenanceItemObjectList: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AInbDeliverySerialNmbrType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAInbDeliverySerialNmbrType(matching: var_query.withKey(AInbDeliverySerialNmbrType.key(maintenanceItemObjectList: maintenanceItemObjectList)), headers: headers, options: options)
    }

    open func fetchAInbDeliverySerialNmbrTypeWithKey(maintenanceItemObjectList: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AInbDeliverySerialNmbrType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAInbDeliverySerialNmbrTypeWithKey(maintenanceItemObjectList: maintenanceItemObjectList, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaintenanceItemObjListType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaintenanceItemObjListType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AMaintenanceItemObjListType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aMaintenanceItemObjListTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAMaintenanceItemObjListType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaintenanceItemObjListType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaintenanceItemObjListType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaintenanceItemObjListTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AMaintenanceItemObjListType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AMaintenanceItemObjListType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aMaintenanceItemObjListTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAMaintenanceItemObjListTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AMaintenanceItemObjListType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaintenanceItemObjListTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaintenanceItemObjListTypeWithKey(maintenanceItemObject: Int?, maintenanceItemObjectList: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaintenanceItemObjListType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAMaintenanceItemObjListType(matching: var_query.withKey(AMaintenanceItemObjListType.key(maintenanceItemObject: maintenanceItemObject, maintenanceItemObjectList: maintenanceItemObjectList)), headers: headers, options: options)
    }

    open func fetchAMaintenanceItemObjListTypeWithKey(maintenanceItemObject: Int?, maintenanceItemObjectList: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaintenanceItemObjListType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaintenanceItemObjListTypeWithKey(maintenanceItemObject: maintenanceItemObject, maintenanceItemObjectList: maintenanceItemObjectList, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaintenanceItemObject(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AMaintenanceItemObjectType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AMaintenanceItemObjectType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aMaintenanceItemObject), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAMaintenanceItemObject(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AMaintenanceItemObjectType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaintenanceItemObject(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaintenanceItemObjectType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaintenanceItemObjectType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AMaintenanceItemObjectType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aMaintenanceItemObject), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAMaintenanceItemObjectType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaintenanceItemObjectType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaintenanceItemObjectType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaintenanceItemObjectTypeWithKey(maintenanceItemObject: Int?, maintenanceItemObjectList: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaintenanceItemObjectType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAMaintenanceItemObjectType(matching: var_query.withKey(AMaintenanceItemObjectType.key(maintenanceItemObject: maintenanceItemObject, maintenanceItemObjectList: maintenanceItemObjectList)), headers: headers, options: options)
    }

    open func fetchAMaintenanceItemObjectTypeWithKey(maintenanceItemObject: Int?, maintenanceItemObjectList: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaintenanceItemObjectType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaintenanceItemObjectTypeWithKey(maintenanceItemObject: maintenanceItemObject, maintenanceItemObjectList: maintenanceItemObjectList, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaterialDocumentHeader(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AMaterialDocumentHeaderType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AMaterialDocumentHeaderType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aMaterialDocumentHeader), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAMaterialDocumentHeader(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AMaterialDocumentHeaderType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaterialDocumentHeader(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaterialDocumentHeaderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaterialDocumentHeaderType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AMaterialDocumentHeaderType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aMaterialDocumentHeader), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAMaterialDocumentHeaderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaterialDocumentHeaderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaterialDocumentHeaderType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaterialDocumentHeaderTypeWithKey(materialDocumentYear: String?, materialDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaterialDocumentHeaderType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAMaterialDocumentHeaderType(matching: var_query.withKey(AMaterialDocumentHeaderType.key(materialDocumentYear: materialDocumentYear, materialDocument: materialDocument)), headers: headers, options: options)
    }

    open func fetchAMaterialDocumentHeaderTypeWithKey(materialDocumentYear: String?, materialDocument: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaterialDocumentHeaderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaterialDocumentHeaderTypeWithKey(materialDocumentYear: materialDocumentYear, materialDocument: materialDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaterialDocumentItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaterialDocumentItemType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AMaterialDocumentItemType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aMaterialDocumentItemTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAMaterialDocumentItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaterialDocumentItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaterialDocumentItemType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaterialDocumentItemTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AMaterialDocumentItemType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AMaterialDocumentItemType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aMaterialDocumentItemTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAMaterialDocumentItemTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AMaterialDocumentItemType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaterialDocumentItemTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAMaterialDocumentItemTypeWithKey(materialDocumentYear: String?, materialDocument: String?, materialDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AMaterialDocumentItemType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAMaterialDocumentItemType(matching: var_query.withKey(AMaterialDocumentItemType.key(materialDocumentYear: materialDocumentYear, materialDocument: materialDocument, materialDocumentItem: materialDocumentItem)), headers: headers, options: options)
    }

    open func fetchAMaterialDocumentItemTypeWithKey(materialDocumentYear: String?, materialDocument: String?, materialDocumentItem: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AMaterialDocumentItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAMaterialDocumentItemTypeWithKey(materialDocumentYear: materialDocumentYear, materialDocument: materialDocument, materialDocumentItem: materialDocumentItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryAddress(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AOutbDeliveryAddressType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AOutbDeliveryAddressType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryAddress), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAOutbDeliveryAddress(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AOutbDeliveryAddressType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryAddress(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryAddressType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryAddressType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AOutbDeliveryAddressType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryAddress), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAOutbDeliveryAddressType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryAddressType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryAddressType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryAddressTypeWithKey(addressID: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryAddressType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAOutbDeliveryAddressType(matching: var_query.withKey(AOutbDeliveryAddressType.key(addressID: addressID)), headers: headers, options: options)
    }

    open func fetchAOutbDeliveryAddressTypeWithKey(addressID: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryAddressType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryAddressTypeWithKey(addressID: addressID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryDocFlow(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AOutbDeliveryDocFlowType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AOutbDeliveryDocFlowType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryDocFlow), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAOutbDeliveryDocFlow(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AOutbDeliveryDocFlowType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryDocFlow(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryDocFlowType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryDocFlowType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AOutbDeliveryDocFlowType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryDocFlow), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAOutbDeliveryDocFlowType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryDocFlowType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryDocFlowType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryDocFlowTypeWithKey(precedingDocument: String?, precedingDocumentItem: String?, subsequentDocumentCategory: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryDocFlowType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAOutbDeliveryDocFlowType(matching: var_query.withKey(AOutbDeliveryDocFlowType.key(precedingDocument: precedingDocument, precedingDocumentItem: precedingDocumentItem, subsequentDocumentCategory: subsequentDocumentCategory)), headers: headers, options: options)
    }

    open func fetchAOutbDeliveryDocFlowTypeWithKey(precedingDocument: String?, precedingDocumentItem: String?, subsequentDocumentCategory: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryDocFlowType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryDocFlowTypeWithKey(precedingDocument: precedingDocument, precedingDocumentItem: precedingDocumentItem, subsequentDocumentCategory: subsequentDocumentCategory, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryHeader(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AOutbDeliveryHeaderType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AOutbDeliveryHeaderType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryHeader), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAOutbDeliveryHeader(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AOutbDeliveryHeaderType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryHeader(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryHeaderText(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AOutbDeliveryHeaderTextType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AOutbDeliveryHeaderTextType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryHeaderText), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAOutbDeliveryHeaderText(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AOutbDeliveryHeaderTextType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryHeaderText(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryHeaderTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryHeaderTextType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AOutbDeliveryHeaderTextType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryHeaderText), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAOutbDeliveryHeaderTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryHeaderTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryHeaderTextType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryHeaderTextTypeWithKey(deliveryDocument: String?, textElement: String?, language: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryHeaderTextType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAOutbDeliveryHeaderTextType(matching: var_query.withKey(AOutbDeliveryHeaderTextType.key(deliveryDocument: deliveryDocument, textElement: textElement, language: language)), headers: headers, options: options)
    }

    open func fetchAOutbDeliveryHeaderTextTypeWithKey(deliveryDocument: String?, textElement: String?, language: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryHeaderTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryHeaderTextTypeWithKey(deliveryDocument: deliveryDocument, textElement: textElement, language: language, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryHeaderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryHeaderType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AOutbDeliveryHeaderType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryHeader), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAOutbDeliveryHeaderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryHeaderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryHeaderType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryHeaderTypeWithKey(deliveryDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryHeaderType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAOutbDeliveryHeaderType(matching: var_query.withKey(AOutbDeliveryHeaderType.key(deliveryDocument: deliveryDocument)), headers: headers, options: options)
    }

    open func fetchAOutbDeliveryHeaderTypeWithKey(deliveryDocument: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryHeaderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryHeaderTypeWithKey(deliveryDocument: deliveryDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryItem(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AOutbDeliveryItemType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AOutbDeliveryItemType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryItem), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAOutbDeliveryItem(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AOutbDeliveryItemType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryItem(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryItemText(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AOutbDeliveryItemTextType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AOutbDeliveryItemTextType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryItemText), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAOutbDeliveryItemText(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AOutbDeliveryItemTextType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryItemText(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryItemTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryItemTextType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AOutbDeliveryItemTextType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryItemText), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAOutbDeliveryItemTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryItemTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryItemTextType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryItemTextTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, textElement: String?, language: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryItemTextType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAOutbDeliveryItemTextType(matching: var_query.withKey(AOutbDeliveryItemTextType.key(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, textElement: textElement, language: language)), headers: headers, options: options)
    }

    open func fetchAOutbDeliveryItemTextTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, textElement: String?, language: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryItemTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryItemTextTypeWithKey(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, textElement: textElement, language: language, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryItemType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AOutbDeliveryItemType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryItem), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAOutbDeliveryItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryItemType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryItemTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryItemType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAOutbDeliveryItemType(matching: var_query.withKey(AOutbDeliveryItemType.key(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem)), headers: headers, options: options)
    }

    open func fetchAOutbDeliveryItemTypeWithKey(deliveryDocument: String?, deliveryDocumentItem: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryItemTypeWithKey(deliveryDocument: deliveryDocument, deliveryDocumentItem: deliveryDocumentItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryPartner(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AOutbDeliveryPartnerType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AOutbDeliveryPartnerType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryPartner), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAOutbDeliveryPartner(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AOutbDeliveryPartnerType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryPartner(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryPartnerType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AOutbDeliveryPartnerType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aOutbDeliveryPartner), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAOutbDeliveryPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryPartnerType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAOutbDeliveryPartnerTypeWithKey(partnerFunction: String?, sdDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AOutbDeliveryPartnerType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAOutbDeliveryPartnerType(matching: var_query.withKey(AOutbDeliveryPartnerType.key(partnerFunction: partnerFunction, sdDocument: sdDocument)), headers: headers, options: options)
    }

    open func fetchAOutbDeliveryPartnerTypeWithKey(partnerFunction: String?, sdDocument: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AOutbDeliveryPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAOutbDeliveryPartnerTypeWithKey(partnerFunction: partnerFunction, sdDocument: sdDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPlant(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [APlantType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try APlantType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aPlant), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAPlant(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([APlantType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPlant(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPlantType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APlantType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<APlantType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aPlant), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAPlantType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APlantType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPlantType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPlantTypeWithKey(plant: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APlantType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAPlantType(matching: var_query.withKey(APlantType.key(plant: plant)), headers: headers, options: options)
    }

    open func fetchAPlantTypeWithKey(plant: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APlantType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPlantTypeWithKey(plant: plant, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAProduct(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AProductType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AProductType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aProduct), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAProduct(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AProductType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAProduct(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAProductDescription(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AProductDescriptionType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AProductDescriptionType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aProductDescription), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAProductDescription(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AProductDescriptionType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAProductDescription(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAProductDescriptionType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AProductDescriptionType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AProductDescriptionType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aProductDescription), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAProductDescriptionType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AProductDescriptionType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAProductDescriptionType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAProductDescriptionTypeWithKey(product: String?, language: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AProductDescriptionType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAProductDescriptionType(matching: var_query.withKey(AProductDescriptionType.key(product: product, language: language)), headers: headers, options: options)
    }

    open func fetchAProductDescriptionTypeWithKey(product: String?, language: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AProductDescriptionType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAProductDescriptionTypeWithKey(product: product, language: language, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAProductPlant(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [AProductPlantType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AProductPlantType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aProductPlant), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAProductPlant(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([AProductPlantType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAProductPlant(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAProductPlantType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AProductPlantType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AProductPlantType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aProductPlant), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAProductPlantType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AProductPlantType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAProductPlantType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAProductPlantTypeWithKey(product: String?, plant: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AProductPlantType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAProductPlantType(matching: var_query.withKey(AProductPlantType.key(product: product, plant: plant)), headers: headers, options: options)
    }

    open func fetchAProductPlantTypeWithKey(product: String?, plant: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AProductPlantType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAProductPlantTypeWithKey(product: product, plant: plant, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAProductType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AProductType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AProductType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aProduct), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAProductType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AProductType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAProductType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAProductTypeWithKey(product: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AProductType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAProductType(matching: var_query.withKey(AProductType.key(product: product)), headers: headers, options: options)
    }

    open func fetchAProductTypeWithKey(product: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AProductType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAProductTypeWithKey(product: product, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurOrdAccountAssignment(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [APurOrdAccountAssignmentType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try APurOrdAccountAssignmentType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aPurOrdAccountAssignment), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAPurOrdAccountAssignment(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([APurOrdAccountAssignmentType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurOrdAccountAssignment(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurOrdAccountAssignmentType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APurOrdAccountAssignmentType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<APurOrdAccountAssignmentType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aPurOrdAccountAssignment), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAPurOrdAccountAssignmentType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APurOrdAccountAssignmentType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurOrdAccountAssignmentType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurOrdAccountAssignmentTypeWithKey(purchaseOrder: String?, purchaseOrderItem: String?, accountAssignmentNumber: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APurOrdAccountAssignmentType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAPurOrdAccountAssignmentType(matching: var_query.withKey(APurOrdAccountAssignmentType.key(purchaseOrder: purchaseOrder, purchaseOrderItem: purchaseOrderItem, accountAssignmentNumber: accountAssignmentNumber)), headers: headers, options: options)
    }

    open func fetchAPurOrdAccountAssignmentTypeWithKey(purchaseOrder: String?, purchaseOrderItem: String?, accountAssignmentNumber: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APurOrdAccountAssignmentType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurOrdAccountAssignmentTypeWithKey(purchaseOrder: purchaseOrder, purchaseOrderItem: purchaseOrderItem, accountAssignmentNumber: accountAssignmentNumber, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurOrdPricingElement(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [APurOrdPricingElementType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try APurOrdPricingElementType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aPurOrdPricingElement), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAPurOrdPricingElement(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([APurOrdPricingElementType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurOrdPricingElement(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurOrdPricingElementType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APurOrdPricingElementType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<APurOrdPricingElementType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aPurOrdPricingElement), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAPurOrdPricingElementType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APurOrdPricingElementType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurOrdPricingElementType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurOrdPricingElementTypeWithKey(purchaseOrder: String?, purchaseOrderItem: String?, pricingDocument: String?, pricingDocumentItem: String?, pricingProcedureStep: String?, pricingProcedureCounter: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APurOrdPricingElementType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAPurOrdPricingElementType(matching: var_query.withKey(APurOrdPricingElementType.key(purchaseOrder: purchaseOrder, purchaseOrderItem: purchaseOrderItem, pricingDocument: pricingDocument, pricingDocumentItem: pricingDocumentItem, pricingProcedureStep: pricingProcedureStep, pricingProcedureCounter: pricingProcedureCounter)), headers: headers, options: options)
    }

    open func fetchAPurOrdPricingElementTypeWithKey(purchaseOrder: String?, purchaseOrderItem: String?, pricingDocument: String?, pricingDocumentItem: String?, pricingProcedureStep: String?, pricingProcedureCounter: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APurOrdPricingElementType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurOrdPricingElementTypeWithKey(purchaseOrder: purchaseOrder, purchaseOrderItem: purchaseOrderItem, pricingDocument: pricingDocument, pricingDocumentItem: pricingDocumentItem, pricingProcedureStep: pricingProcedureStep, pricingProcedureCounter: pricingProcedureCounter, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurchaseOrder(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [APurchaseOrderType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try APurchaseOrderType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aPurchaseOrder), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAPurchaseOrder(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([APurchaseOrderType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurchaseOrder(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurchaseOrderItem(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [APurchaseOrderItemType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try APurchaseOrderItemType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aPurchaseOrderItem), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAPurchaseOrderItem(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([APurchaseOrderItemType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurchaseOrderItem(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurchaseOrderItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APurchaseOrderItemType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<APurchaseOrderItemType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aPurchaseOrderItem), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAPurchaseOrderItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APurchaseOrderItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurchaseOrderItemType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurchaseOrderItemTypeWithKey(purchaseOrder: String?, purchaseOrderItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APurchaseOrderItemType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAPurchaseOrderItemType(matching: var_query.withKey(APurchaseOrderItemType.key(purchaseOrder: purchaseOrder, purchaseOrderItem: purchaseOrderItem)), headers: headers, options: options)
    }

    open func fetchAPurchaseOrderItemTypeWithKey(purchaseOrder: String?, purchaseOrderItem: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APurchaseOrderItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurchaseOrderItemTypeWithKey(purchaseOrder: purchaseOrder, purchaseOrderItem: purchaseOrderItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurchaseOrderScheduleLine(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [APurchaseOrderScheduleLineType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try APurchaseOrderScheduleLineType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aPurchaseOrderScheduleLine), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAPurchaseOrderScheduleLine(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([APurchaseOrderScheduleLineType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurchaseOrderScheduleLine(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurchaseOrderScheduleLineType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APurchaseOrderScheduleLineType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<APurchaseOrderScheduleLineType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aPurchaseOrderScheduleLine), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAPurchaseOrderScheduleLineType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APurchaseOrderScheduleLineType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurchaseOrderScheduleLineType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurchaseOrderScheduleLineTypeWithKey(purchasingDocument: String?, purchasingDocumentItem: String?, scheduleLine: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APurchaseOrderScheduleLineType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAPurchaseOrderScheduleLineType(matching: var_query.withKey(APurchaseOrderScheduleLineType.key(purchasingDocument: purchasingDocument, purchasingDocumentItem: purchasingDocumentItem, scheduleLine: scheduleLine)), headers: headers, options: options)
    }

    open func fetchAPurchaseOrderScheduleLineTypeWithKey(purchasingDocument: String?, purchasingDocumentItem: String?, scheduleLine: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APurchaseOrderScheduleLineType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurchaseOrderScheduleLineTypeWithKey(purchasingDocument: purchasingDocument, purchasingDocumentItem: purchasingDocumentItem, scheduleLine: scheduleLine, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurchaseOrderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APurchaseOrderType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<APurchaseOrderType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aPurchaseOrder), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAPurchaseOrderType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APurchaseOrderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurchaseOrderType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAPurchaseOrderTypeWithKey(purchaseOrder: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> APurchaseOrderType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAPurchaseOrderType(matching: var_query.withKey(APurchaseOrderType.key(purchaseOrder: purchaseOrder)), headers: headers, options: options)
    }

    open func fetchAPurchaseOrderTypeWithKey(purchaseOrder: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (APurchaseOrderType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAPurchaseOrderTypeWithKey(purchaseOrder: purchaseOrder, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchASerialNmbrDelivery(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [ASerialNmbrDeliveryType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ASerialNmbrDeliveryType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.aSerialNmbrDelivery), headers: var_headers, options: var_options).entityList())
    }

    open func fetchASerialNmbrDelivery(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([ASerialNmbrDeliveryType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchASerialNmbrDelivery(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchASerialNmbrDeliveryType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ASerialNmbrDeliveryType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<ASerialNmbrDeliveryType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.aSerialNmbrDelivery), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchASerialNmbrDeliveryType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ASerialNmbrDeliveryType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchASerialNmbrDeliveryType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchASerialNmbrDeliveryTypeWithKey(maintenanceItemObjectList: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ASerialNmbrDeliveryType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchASerialNmbrDeliveryType(matching: var_query.withKey(ASerialNmbrDeliveryType.key(maintenanceItemObjectList: maintenanceItemObjectList)), headers: headers, options: options)
    }

    open func fetchASerialNmbrDeliveryTypeWithKey(maintenanceItemObjectList: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ASerialNmbrDeliveryType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchASerialNmbrDeliveryTypeWithKey(maintenanceItemObjectList: maintenanceItemObjectList, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchConfirmPick(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [ConfirmPickType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ConfirmPickType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.confirmPick), headers: var_headers, options: var_options).entityList())
    }

    open func fetchConfirmPick(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([ConfirmPickType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchConfirmPick(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchConfirmPickType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ConfirmPickType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<ConfirmPickType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.confirmPick), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchConfirmPickType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ConfirmPickType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchConfirmPickType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchConfirmPickTypeWithKey(outboundDeliveryUUID: GuidValue?, outboundDeliveryOrderItemUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ConfirmPickType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchConfirmPickType(matching: var_query.withKey(ConfirmPickType.key(outboundDeliveryUUID: outboundDeliveryUUID, outboundDeliveryOrderItemUUID: outboundDeliveryOrderItemUUID)), headers: headers, options: options)
    }

    open func fetchConfirmPickTypeWithKey(outboundDeliveryUUID: GuidValue?, outboundDeliveryOrderItemUUID: GuidValue?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ConfirmPickType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchConfirmPickTypeWithKey(outboundDeliveryUUID: outboundDeliveryUUID, outboundDeliveryOrderItemUUID: outboundDeliveryOrderItemUUID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCreateOutbDelivery(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [CreateOutbDeliveryType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CreateOutbDeliveryType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.createOutbDelivery), headers: var_headers, options: var_options).entityList())
    }

    open func fetchCreateOutbDelivery(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([CreateOutbDeliveryType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCreateOutbDelivery(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCreateOutbDeliveryType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> CreateOutbDeliveryType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<CreateOutbDeliveryType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.createOutbDelivery), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchCreateOutbDeliveryType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (CreateOutbDeliveryType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCreateOutbDeliveryType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCreateOutbDeliveryTypeWithKey(referenceSDDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> CreateOutbDeliveryType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchCreateOutbDeliveryType(matching: var_query.withKey(CreateOutbDeliveryType.key(referenceSDDocument: referenceSDDocument)), headers: headers, options: options)
    }

    open func fetchCreateOutbDeliveryTypeWithKey(referenceSDDocument: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (CreateOutbDeliveryType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCreateOutbDeliveryTypeWithKey(referenceSDDocument: referenceSDDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCreateOutbWarehouseTask(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [CreateOutbWarehouseTaskType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CreateOutbWarehouseTaskType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.createOutbWarehouseTask), headers: var_headers, options: var_options).entityList())
    }

    open func fetchCreateOutbWarehouseTask(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([CreateOutbWarehouseTaskType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCreateOutbWarehouseTask(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCreateOutbWarehouseTaskType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> CreateOutbWarehouseTaskType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<CreateOutbWarehouseTaskType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.createOutbWarehouseTask), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchCreateOutbWarehouseTaskType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (CreateOutbWarehouseTaskType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCreateOutbWarehouseTaskType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCreateOutbWarehouseTaskTypeWithKey(outboundDeliveryUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> CreateOutbWarehouseTaskType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchCreateOutbWarehouseTaskType(matching: var_query.withKey(CreateOutbWarehouseTaskType.key(outboundDeliveryUUID: outboundDeliveryUUID)), headers: headers, options: options)
    }

    open func fetchCreateOutbWarehouseTaskTypeWithKey(outboundDeliveryUUID: GuidValue?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (CreateOutbWarehouseTaskType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCreateOutbWarehouseTaskTypeWithKey(outboundDeliveryUUID: outboundDeliveryUUID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCreateWarehouseTask(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [CreateWarehouseTaskType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CreateWarehouseTaskType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.createWarehouseTask), headers: var_headers, options: var_options).entityList())
    }

    open func fetchCreateWarehouseTask(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([CreateWarehouseTaskType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCreateWarehouseTask(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCreateWarehouseTaskType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> CreateWarehouseTaskType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<CreateWarehouseTaskType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.createWarehouseTask), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchCreateWarehouseTaskType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (CreateWarehouseTaskType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCreateWarehouseTaskType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCreateWarehouseTaskTypeWithKey(inboundDeliveryUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> CreateWarehouseTaskType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchCreateWarehouseTaskType(matching: var_query.withKey(CreateWarehouseTaskType.key(inboundDeliveryUUID: inboundDeliveryUUID)), headers: headers, options: options)
    }

    open func fetchCreateWarehouseTaskTypeWithKey(inboundDeliveryUUID: GuidValue?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (CreateWarehouseTaskType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCreateWarehouseTaskTypeWithKey(inboundDeliveryUUID: inboundDeliveryUUID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchDLVHead(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> DLVHead {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<DLVHead>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.dlvHeadSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchDLVHead(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (DLVHead?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchDLVHead(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchDLVHeadSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [DLVHead] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try DLVHead.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.dlvHeadSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchDLVHeadSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([DLVHead]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchDLVHeadSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchDLVHeadWithKey(inboundDeliveryUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> DLVHead {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchDLVHead(matching: var_query.withKey(DLVHead.key(inboundDeliveryUUID: inboundDeliveryUUID)), headers: headers, options: options)
    }

    open func fetchDLVHeadWithKey(inboundDeliveryUUID: GuidValue?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (DLVHead?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchDLVHeadWithKey(inboundDeliveryUUID: inboundDeliveryUUID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchDLVItem(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> DLVItem {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<DLVItem>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.dlvItemSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchDLVItem(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (DLVItem?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchDLVItem(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchDLVItemSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [DLVItem] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try DLVItem.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.dlvItemSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchDLVItemSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([DLVItem]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchDLVItemSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchDLVItemWithKey(inboundDeliveryItemUUID: GuidValue?, inboundDeliveryUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> DLVItem {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchDLVItem(matching: var_query.withKey(DLVItem.key(inboundDeliveryItemUUID: inboundDeliveryItemUUID, inboundDeliveryUUID: inboundDeliveryUUID)), headers: headers, options: options)
    }

    open func fetchDLVItemWithKey(inboundDeliveryItemUUID: GuidValue?, inboundDeliveryUUID: GuidValue?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (DLVItem?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchDLVItemWithKey(inboundDeliveryItemUUID: inboundDeliveryItemUUID, inboundDeliveryUUID: inboundDeliveryUUID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchGoodsIssue(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [GoodsIssueType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try GoodsIssueType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.goodsIssue), headers: var_headers, options: var_options).entityList())
    }

    open func fetchGoodsIssue(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([GoodsIssueType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchGoodsIssue(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchGoodsIssueType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> GoodsIssueType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<GoodsIssueType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.goodsIssue), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchGoodsIssueType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (GoodsIssueType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchGoodsIssueType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchGoodsIssueTypeWithKey(outboundDeliveryUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> GoodsIssueType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchGoodsIssueType(matching: var_query.withKey(GoodsIssueType.key(outboundDeliveryUUID: outboundDeliveryUUID)), headers: headers, options: options)
    }

    open func fetchGoodsIssueTypeWithKey(outboundDeliveryUUID: GuidValue?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (GoodsIssueType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchGoodsIssueTypeWithKey(outboundDeliveryUUID: outboundDeliveryUUID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchHUHead(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> HUHead {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<HUHead>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.huHeadSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchHUHead(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (HUHead?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchHUHead(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchHUHeadSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [HUHead] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try HUHead.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.huHeadSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchHUHeadSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([HUHead]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchHUHeadSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchHUHeadWithKey(handlingUnitUUID: GuidValue?, inboundDeliveryUUID: GuidValue?, inboundDeliveryItemUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> HUHead {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchHUHead(matching: var_query.withKey(HUHead.key(handlingUnitUUID: handlingUnitUUID, inboundDeliveryUUID: inboundDeliveryUUID, inboundDeliveryItemUUID: inboundDeliveryItemUUID)), headers: headers, options: options)
    }

    open func fetchHUHeadWithKey(handlingUnitUUID: GuidValue?, inboundDeliveryUUID: GuidValue?, inboundDeliveryItemUUID: GuidValue?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (HUHead?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchHUHeadWithKey(handlingUnitUUID: handlingUnitUUID, inboundDeliveryUUID: inboundDeliveryUUID, inboundDeliveryItemUUID: inboundDeliveryItemUUID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchHUItem(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> HUItem {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<HUItem>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.huItemSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchHUItem(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (HUItem?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchHUItem(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchHUItemSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [HUItem] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try HUItem.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.huItemSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchHUItemSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([HUItem]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchHUItemSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchHUItemWithKey(handlingUnitUUID: GuidValue?, handlingUnitParentUUID: GuidValue?, inboundDeliveryUUID: GuidValue?, inboundDeliveryItemUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> HUItem {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchHUItem(matching: var_query.withKey(HUItem.key(handlingUnitUUID: handlingUnitUUID, handlingUnitParentUUID: handlingUnitParentUUID, inboundDeliveryUUID: inboundDeliveryUUID, inboundDeliveryItemUUID: inboundDeliveryItemUUID)), headers: headers, options: options)
    }

    open func fetchHUItemWithKey(handlingUnitUUID: GuidValue?, handlingUnitParentUUID: GuidValue?, inboundDeliveryUUID: GuidValue?, inboundDeliveryItemUUID: GuidValue?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (HUItem?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchHUItemWithKey(handlingUnitUUID: handlingUnitUUID, handlingUnitParentUUID: handlingUnitParentUUID, inboundDeliveryUUID: inboundDeliveryUUID, inboundDeliveryItemUUID: inboundDeliveryItemUUID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchHUSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Hu] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Hu.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.huSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchHUSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Hu]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchHUSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchHUSingleItem(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> HUSingleItem {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<HUSingleItem>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.huSingleItemSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchHUSingleItem(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (HUSingleItem?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchHUSingleItem(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchHUSingleItemSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [HUSingleItem] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try HUSingleItem.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.huSingleItemSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchHUSingleItemSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([HUSingleItem]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchHUSingleItemSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchHUSingleItemWithKey(handlingUnitItemUUID: GuidValue?, handlingUnitHeadUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> HUSingleItem {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchHUSingleItem(matching: var_query.withKey(HUSingleItem.key(handlingUnitItemUUID: handlingUnitItemUUID, handlingUnitHeadUUID: handlingUnitHeadUUID)), headers: headers, options: options)
    }

    open func fetchHUSingleItemWithKey(handlingUnitItemUUID: GuidValue?, handlingUnitHeadUUID: GuidValue?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (HUSingleItem?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchHUSingleItemWithKey(handlingUnitItemUUID: handlingUnitItemUUID, handlingUnitHeadUUID: handlingUnitHeadUUID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchHu(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Hu {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Hu>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.huSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchHu(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Hu?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchHu(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchHuWithKey(huID: String?, bin: String?, lgnum: String?, workstation: String?, type_: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Hu {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchHu(matching: var_query.withKey(Hu.key(huID: huID, bin: bin, lgnum: lgnum, workstation: workstation, type_: type_)), headers: headers, options: options)
    }

    open func fetchHuWithKey(huID: String?, bin: String?, lgnum: String?, workstation: String?, type_: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Hu?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchHuWithKey(huID: huID, bin: bin, lgnum: lgnum, workstation: workstation, type_: type_, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchItem(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Item {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Item>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.itemSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchItem(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Item?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchItem(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchItemSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Item] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Item.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.itemSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchItemSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Item]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchItemSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchItemWithKey(stockID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Item {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchItem(matching: var_query.withKey(Item.key(stockID: stockID)), headers: headers, options: options)
    }

    open func fetchItemWithKey(stockID: GuidValue?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Item?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchItemWithKey(stockID: stockID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchOUTBDLVHead(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> OUTBDLVHead {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<OUTBDLVHead>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.outbdlvHeadSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchOUTBDLVHead(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (OUTBDLVHead?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchOUTBDLVHead(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchOUTBDLVHeadSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [OUTBDLVHead] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try OUTBDLVHead.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.outbdlvHeadSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchOUTBDLVHeadSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([OUTBDLVHead]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchOUTBDLVHeadSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchOUTBDLVHeadWithKey(outboundDeliveryUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> OUTBDLVHead {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchOUTBDLVHead(matching: var_query.withKey(OUTBDLVHead.key(outboundDeliveryUUID: outboundDeliveryUUID)), headers: headers, options: options)
    }

    open func fetchOUTBDLVHeadWithKey(outboundDeliveryUUID: GuidValue?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (OUTBDLVHead?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchOUTBDLVHeadWithKey(outboundDeliveryUUID: outboundDeliveryUUID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchOUTBDLVItem(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> OUTBDLVItem {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<OUTBDLVItem>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.outbdlvItemSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchOUTBDLVItem(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (OUTBDLVItem?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchOUTBDLVItem(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchOUTBDLVItemSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [OUTBDLVItem] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try OUTBDLVItem.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.outbdlvItemSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchOUTBDLVItemSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([OUTBDLVItem]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchOUTBDLVItemSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchOUTBDLVItemWithKey(outboundDeliveryOrderItemUUID: GuidValue?, outboundDeliveryUUID: GuidValue?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> OUTBDLVItem {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchOUTBDLVItem(matching: var_query.withKey(OUTBDLVItem.key(outboundDeliveryOrderItemUUID: outboundDeliveryOrderItemUUID, outboundDeliveryUUID: outboundDeliveryUUID)), headers: headers, options: options)
    }

    open func fetchOUTBDLVItemWithKey(outboundDeliveryOrderItemUUID: GuidValue?, outboundDeliveryUUID: GuidValue?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (OUTBDLVItem?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchOUTBDLVItemWithKey(outboundDeliveryOrderItemUUID: outboundDeliveryOrderItemUUID, outboundDeliveryUUID: outboundDeliveryUUID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchPerformPack(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [PerformPackType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try PerformPackType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.performPack), headers: var_headers, options: var_options).entityList())
    }

    open func fetchPerformPack(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([PerformPackType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchPerformPack(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchPerformPackType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> PerformPackType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<PerformPackType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.performPack), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchPerformPackType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (PerformPackType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchPerformPackType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchPerformPackTypeWithKey(msgID: String?, msgKey: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> PerformPackType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchPerformPackType(matching: var_query.withKey(PerformPackType.key(msgID: msgID, msgKey: msgKey)), headers: headers, options: options)
    }

    open func fetchPerformPackTypeWithKey(msgID: String?, msgKey: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (PerformPackType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchPerformPackTypeWithKey(msgID: msgID, msgKey: msgKey, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchPurchaseOrderERPCreateRequestConfirmationInV1(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> PurchaseOrderERPCreateRequestConfirmationInV1 {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<PurchaseOrderERPCreateRequestConfirmationInV1>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.purchaseOrderERPCreateRequestConfirmationInV1Set), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchPurchaseOrderERPCreateRequestConfirmationInV1(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (PurchaseOrderERPCreateRequestConfirmationInV1?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchPurchaseOrderERPCreateRequestConfirmationInV1(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchPurchaseOrderERPCreateRequestConfirmationInV1Set(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [PurchaseOrderERPCreateRequestConfirmationInV1] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try PurchaseOrderERPCreateRequestConfirmationInV1.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.purchaseOrderERPCreateRequestConfirmationInV1Set), headers: var_headers, options: var_options).entityList())
    }

    open func fetchPurchaseOrderERPCreateRequestConfirmationInV1Set(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([PurchaseOrderERPCreateRequestConfirmationInV1]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchPurchaseOrderERPCreateRequestConfirmationInV1Set(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchPurchaseOrderERPCreateRequestConfirmationInV1WithKey(id: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> PurchaseOrderERPCreateRequestConfirmationInV1 {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchPurchaseOrderERPCreateRequestConfirmationInV1(matching: var_query.withKey(PurchaseOrderERPCreateRequestConfirmationInV1.key(id: id)), headers: headers, options: options)
    }

    open func fetchPurchaseOrderERPCreateRequestConfirmationInV1WithKey(id: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (PurchaseOrderERPCreateRequestConfirmationInV1?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchPurchaseOrderERPCreateRequestConfirmationInV1WithKey(id: id, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchWTSimpleConfirm(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> WTSimpleConfirm {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<WTSimpleConfirm>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.wtSimpleConfirmSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchWTSimpleConfirm(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (WTSimpleConfirm?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchWTSimpleConfirm(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchWTSimpleConfirmSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [WTSimpleConfirm] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try WTSimpleConfirm.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.wtSimpleConfirmSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchWTSimpleConfirmSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([WTSimpleConfirm]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchWTSimpleConfirmSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchWTSimpleConfirmWithKey(lgnum: String?, tanum: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> WTSimpleConfirm {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchWTSimpleConfirm(matching: var_query.withKey(WTSimpleConfirm.key(lgnum: lgnum, tanum: tanum)), headers: headers, options: options)
    }

    open func fetchWTSimpleConfirmWithKey(lgnum: String?, tanum: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (WTSimpleConfirm?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchWTSimpleConfirmWithKey(lgnum: lgnum, tanum: tanum, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchWarehouse(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [WarehouseType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try WarehouseType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.warehouse), headers: var_headers, options: var_options).entityList())
    }

    open func fetchWarehouse(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([WarehouseType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchWarehouse(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchWarehouseTask(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> WarehouseTask {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<WarehouseTask>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.warehouseTaskSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchWarehouseTask(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (WarehouseTask?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchWarehouseTask(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchWarehouseTaskDELETE(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [WarehouseTaskType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try WarehouseTaskType.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.warehouseTaskDELETE), headers: var_headers, options: var_options).entityList())
    }

    open func fetchWarehouseTaskDELETE(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([WarehouseTaskType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchWarehouseTaskDELETE(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchWarehouseTaskSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [WarehouseTask] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try WarehouseTask.array(from: self.executeQuery(var_query.fromDefault(Ec1Metadata.EntitySets.warehouseTaskSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchWarehouseTaskSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([WarehouseTask]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchWarehouseTaskSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchWarehouseTaskType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> WarehouseTaskType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<WarehouseTaskType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.warehouseTaskDELETE), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchWarehouseTaskType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (WarehouseTaskType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchWarehouseTaskType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchWarehouseTaskTypeWithKey(warehouse: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> WarehouseTaskType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchWarehouseTaskType(matching: var_query.withKey(WarehouseTaskType.key(warehouse: warehouse)), headers: headers, options: options)
    }

    open func fetchWarehouseTaskTypeWithKey(warehouse: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (WarehouseTaskType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchWarehouseTaskTypeWithKey(warehouse: warehouse, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchWarehouseTaskWithKey(tanum: String?, tapos: String?, lgnum: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> WarehouseTask {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchWarehouseTask(matching: var_query.withKey(WarehouseTask.key(tanum: tanum, tapos: tapos, lgnum: lgnum)), headers: headers, options: options)
    }

    open func fetchWarehouseTaskWithKey(tanum: String?, tapos: String?, lgnum: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (WarehouseTask?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchWarehouseTaskWithKey(tanum: tanum, tapos: tapos, lgnum: lgnum, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchWarehouseType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> WarehouseType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<WarehouseType>.from(self.executeQuery(query.fromDefault(Ec1Metadata.EntitySets.warehouse), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchWarehouseType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (WarehouseType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchWarehouseType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchWarehouseTypeWithKey(warehouse: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> WarehouseType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchWarehouseType(matching: var_query.withKey(WarehouseType.key(warehouse: warehouse)), headers: headers, options: options)
    }

    open func fetchWarehouseTypeWithKey(warehouse: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (WarehouseType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchWarehouseTypeWithKey(warehouse: warehouse, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open override var metadataLock: MetadataLock {
        return Ec1Metadata.lock
    }

    open override func refreshMetadata() throws {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        do {
            try ProxyInternal.refreshMetadataWithLock(service: self, fetcher: nil, options: Ec1MetadataParser.options, mergeAction: { Ec1MetadataChanges.merge(metadata: self.metadata) })
        }
    }
}
