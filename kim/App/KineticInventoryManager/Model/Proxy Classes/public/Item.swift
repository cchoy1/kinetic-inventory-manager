// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class Item: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var parentID_: Property = Ec1Metadata.EntityTypes.item.property(withName: "ParentId")

    private static var stockID_: Property = Ec1Metadata.EntityTypes.item.property(withName: "StockId")

    private static var docID_: Property = Ec1Metadata.EntityTypes.item.property(withName: "DocId")

    private static var itmID_: Property = Ec1Metadata.EntityTypes.item.property(withName: "ItmId")

    private static var consGrp_: Property = Ec1Metadata.EntityTypes.item.property(withName: "ConsGrp")

    private static var baseUnit_: Property = Ec1Metadata.EntityTypes.item.property(withName: "BaseUnit")

    private static var alterUnit_: Property = Ec1Metadata.EntityTypes.item.property(withName: "AlterUnit")

    private static var quan_: Property = Ec1Metadata.EntityTypes.item.property(withName: "Quan")

    private static var alterQuan_: Property = Ec1Metadata.EntityTypes.item.property(withName: "AlterQuan")

    private static var docNo_: Property = Ec1Metadata.EntityTypes.item.property(withName: "DocNo")

    private static var huID_: Property = Ec1Metadata.EntityTypes.item.property(withName: "HuId")

    private static var batch_: Property = Ec1Metadata.EntityTypes.item.property(withName: "Batch")

    private static var snReq_: Property = Ec1Metadata.EntityTypes.item.property(withName: "SnReq")

    private static var snList_: Property = Ec1Metadata.EntityTypes.item.property(withName: "SnList")

    private static var product_: Property = Ec1Metadata.EntityTypes.item.property(withName: "Product")

    private static var productDesc_: Property = Ec1Metadata.EntityTypes.item.property(withName: "ProductDesc")

    private static var productWeight_: Property = Ec1Metadata.EntityTypes.item.property(withName: "ProductWeight")

    private static var productUoM_: Property = Ec1Metadata.EntityTypes.item.property(withName: "ProductUoM")

    private static var type__: Property = Ec1Metadata.EntityTypes.item.property(withName: "Type")

    private static var workstation_: Property = Ec1Metadata.EntityTypes.item.property(withName: "Workstation")

    private static var handlingInstr_: Property = Ec1Metadata.EntityTypes.item.property(withName: "HandlingInstr")

    private static var ean_: Property = Ec1Metadata.EntityTypes.item.property(withName: "EAN")

    private static var prdtPicURL_: Property = Ec1Metadata.EntityTypes.item.property(withName: "PrdtPicURL")

    private static var lgnum_: Property = Ec1Metadata.EntityTypes.item.property(withName: "Lgnum")

    private static var bin_: Property = Ec1Metadata.EntityTypes.item.property(withName: "Bin")

    private static var qtyReduced_: Property = Ec1Metadata.EntityTypes.item.property(withName: "QtyReduced")

    private static var isSplit_: Property = Ec1Metadata.EntityTypes.item.property(withName: "isSplit")

    private static var isIuidActive_: Property = Ec1Metadata.EntityTypes.item.property(withName: "isIuidActive")

    private static var stockType_: Property = Ec1Metadata.EntityTypes.item.property(withName: "StockType")

    private static var stockTypeText_: Property = Ec1Metadata.EntityTypes.item.property(withName: "StockTypeText")

    private static var weight_: Property = Ec1Metadata.EntityTypes.item.property(withName: "Weight")

    private static var weightUoM_: Property = Ec1Metadata.EntityTypes.item.property(withName: "WeightUoM")

    private static var volume_: Property = Ec1Metadata.EntityTypes.item.property(withName: "Volume")

    private static var volumeUoM_: Property = Ec1Metadata.EntityTypes.item.property(withName: "VolumeUoM")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.item)
    }

    open class var alterQuan: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.alterQuan_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.alterQuan_ = value
            }
        }
    }

    open var alterQuan: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: Item.alterQuan))
        }
        set(value) {
            self.setOptionalValue(for: Item.alterQuan, to: DecimalValue.of(optional: value))
        }
    }

    open class var alterUnit: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.alterUnit_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.alterUnit_ = value
            }
        }
    }

    open var alterUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.alterUnit))
        }
        set(value) {
            self.setOptionalValue(for: Item.alterUnit, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [Item] {
        return ArrayConverter.convert(from.toArray(), [Item]())
    }

    open class var baseUnit: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.baseUnit_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.baseUnit_ = value
            }
        }
    }

    open var baseUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.baseUnit))
        }
        set(value) {
            self.setOptionalValue(for: Item.baseUnit, to: StringValue.of(optional: value))
        }
    }

    open class var batch: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.batch_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.batch_ = value
            }
        }
    }

    open var batch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.batch))
        }
        set(value) {
            self.setOptionalValue(for: Item.batch, to: StringValue.of(optional: value))
        }
    }

    open class var bin: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.bin_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.bin_ = value
            }
        }
    }

    open var bin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.bin))
        }
        set(value) {
            self.setOptionalValue(for: Item.bin, to: StringValue.of(optional: value))
        }
    }

    open class var consGrp: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.consGrp_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.consGrp_ = value
            }
        }
    }

    open var consGrp: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.consGrp))
        }
        set(value) {
            self.setOptionalValue(for: Item.consGrp, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> Item {
        return CastRequired<Item>.from(self.copyEntity())
    }

    open class var docID: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.docID_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.docID_ = value
            }
        }
    }

    open var docID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: Item.docID))
        }
        set(value) {
            self.setOptionalValue(for: Item.docID, to: value)
        }
    }

    open class var docNo: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.docNo_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.docNo_ = value
            }
        }
    }

    open var docNo: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.docNo))
        }
        set(value) {
            self.setOptionalValue(for: Item.docNo, to: StringValue.of(optional: value))
        }
    }

    open class var ean: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.ean_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.ean_ = value
            }
        }
    }

    open var ean: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.ean))
        }
        set(value) {
            self.setOptionalValue(for: Item.ean, to: StringValue.of(optional: value))
        }
    }

    open class var handlingInstr: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.handlingInstr_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.handlingInstr_ = value
            }
        }
    }

    open var handlingInstr: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.handlingInstr))
        }
        set(value) {
            self.setOptionalValue(for: Item.handlingInstr, to: StringValue.of(optional: value))
        }
    }

    open class var huID: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.huID_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.huID_ = value
            }
        }
    }

    open var huID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.huID))
        }
        set(value) {
            self.setOptionalValue(for: Item.huID, to: StringValue.of(optional: value))
        }
    }

    open class var isIuidActive: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.isIuidActive_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.isIuidActive_ = value
            }
        }
    }

    open var isIuidActive: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.isIuidActive))
        }
        set(value) {
            self.setOptionalValue(for: Item.isIuidActive, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var isSplit: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.isSplit_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.isSplit_ = value
            }
        }
    }

    open var isSplit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.isSplit))
        }
        set(value) {
            self.setOptionalValue(for: Item.isSplit, to: StringValue.of(optional: value))
        }
    }

    open class var itmID: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.itmID_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.itmID_ = value
            }
        }
    }

    open var itmID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: Item.itmID))
        }
        set(value) {
            self.setOptionalValue(for: Item.itmID, to: value)
        }
    }

    open class func key(stockID: GuidValue?) -> EntityKey {
        return EntityKey().with(name: "StockId", value: stockID)
    }

    open class var lgnum: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.lgnum_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.lgnum_ = value
            }
        }
    }

    open var lgnum: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.lgnum))
        }
        set(value) {
            self.setOptionalValue(for: Item.lgnum, to: StringValue.of(optional: value))
        }
    }

    open var old: Item {
        return CastRequired<Item>.from(self.oldEntity)
    }

    open class var parentID: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.parentID_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.parentID_ = value
            }
        }
    }

    open var parentID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: Item.parentID))
        }
        set(value) {
            self.setOptionalValue(for: Item.parentID, to: value)
        }
    }

    open class var prdtPicURL: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.prdtPicURL_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.prdtPicURL_ = value
            }
        }
    }

    open var prdtPicURL: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.prdtPicURL))
        }
        set(value) {
            self.setOptionalValue(for: Item.prdtPicURL, to: StringValue.of(optional: value))
        }
    }

    open class var product: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.product_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.product_ = value
            }
        }
    }

    open var product: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.product))
        }
        set(value) {
            self.setOptionalValue(for: Item.product, to: StringValue.of(optional: value))
        }
    }

    open class var productDesc: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.productDesc_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.productDesc_ = value
            }
        }
    }

    open var productDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.productDesc))
        }
        set(value) {
            self.setOptionalValue(for: Item.productDesc, to: StringValue.of(optional: value))
        }
    }

    open class var productUoM: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.productUoM_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.productUoM_ = value
            }
        }
    }

    open var productUoM: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.productUoM))
        }
        set(value) {
            self.setOptionalValue(for: Item.productUoM, to: StringValue.of(optional: value))
        }
    }

    open class var productWeight: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.productWeight_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.productWeight_ = value
            }
        }
    }

    open var productWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: Item.productWeight))
        }
        set(value) {
            self.setOptionalValue(for: Item.productWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var qtyReduced: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.qtyReduced_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.qtyReduced_ = value
            }
        }
    }

    open var qtyReduced: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: Item.qtyReduced))
        }
        set(value) {
            self.setOptionalValue(for: Item.qtyReduced, to: DecimalValue.of(optional: value))
        }
    }

    open class var quan: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.quan_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.quan_ = value
            }
        }
    }

    open var quan: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: Item.quan))
        }
        set(value) {
            self.setOptionalValue(for: Item.quan, to: DecimalValue.of(optional: value))
        }
    }

    open class var snList: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.snList_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.snList_ = value
            }
        }
    }

    open var snList: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.snList))
        }
        set(value) {
            self.setOptionalValue(for: Item.snList, to: StringValue.of(optional: value))
        }
    }

    open class var snReq: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.snReq_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.snReq_ = value
            }
        }
    }

    open var snReq: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.snReq))
        }
        set(value) {
            self.setOptionalValue(for: Item.snReq, to: StringValue.of(optional: value))
        }
    }

    open class var stockID: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.stockID_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.stockID_ = value
            }
        }
    }

    open var stockID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: Item.stockID))
        }
        set(value) {
            self.setOptionalValue(for: Item.stockID, to: value)
        }
    }

    open class var stockType: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.stockType_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.stockType_ = value
            }
        }
    }

    open var stockType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.stockType))
        }
        set(value) {
            self.setOptionalValue(for: Item.stockType, to: StringValue.of(optional: value))
        }
    }

    open class var stockTypeText: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.stockTypeText_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.stockTypeText_ = value
            }
        }
    }

    open var stockTypeText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.stockTypeText))
        }
        set(value) {
            self.setOptionalValue(for: Item.stockTypeText, to: StringValue.of(optional: value))
        }
    }

    open class var type_: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.type__
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.type__ = value
            }
        }
    }

    open var type_: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.type_))
        }
        set(value) {
            self.setOptionalValue(for: Item.type_, to: StringValue.of(optional: value))
        }
    }

    open class var volume: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.volume_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.volume_ = value
            }
        }
    }

    open var volume: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: Item.volume))
        }
        set(value) {
            self.setOptionalValue(for: Item.volume, to: DecimalValue.of(optional: value))
        }
    }

    open class var volumeUoM: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.volumeUoM_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.volumeUoM_ = value
            }
        }
    }

    open var volumeUoM: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.volumeUoM))
        }
        set(value) {
            self.setOptionalValue(for: Item.volumeUoM, to: StringValue.of(optional: value))
        }
    }

    open class var weight: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.weight_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.weight_ = value
            }
        }
    }

    open var weight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: Item.weight))
        }
        set(value) {
            self.setOptionalValue(for: Item.weight, to: DecimalValue.of(optional: value))
        }
    }

    open class var weightUoM: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.weightUoM_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.weightUoM_ = value
            }
        }
    }

    open var weightUoM: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.weightUoM))
        }
        set(value) {
            self.setOptionalValue(for: Item.weightUoM, to: StringValue.of(optional: value))
        }
    }

    open class var workstation: Property {
        get {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                return Item.workstation_
            }
        }
        set(value) {
            objc_sync_enter(Item.self)
            defer { objc_sync_exit(Item.self) }
            do {
                Item.workstation_ = value
            }
        }
    }

    open var workstation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item.workstation))
        }
        set(value) {
            self.setOptionalValue(for: Item.workstation, to: StringValue.of(optional: value))
        }
    }
}
