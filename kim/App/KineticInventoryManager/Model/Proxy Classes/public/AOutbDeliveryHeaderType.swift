// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AOutbDeliveryHeaderType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var actualDeliveryRoute_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ActualDeliveryRoute")

    private static var shippinglocationtimezone_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "Shippinglocationtimezone")

    private static var receivinglocationtimezone_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "Receivinglocationtimezone")

    private static var actualGoodsMovementDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ActualGoodsMovementDate")

    private static var actualGoodsMovementTime_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ActualGoodsMovementTime")

    private static var billingDocumentDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "BillingDocumentDate")

    private static var billOfLading_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "BillOfLading")

    private static var completeDeliveryIsDefined_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CompleteDeliveryIsDefined")

    private static var confirmationTime_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ConfirmationTime")

    private static var createdByUser_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CreatedByUser")

    private static var creationDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CreationDate")

    private static var creationTime_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CreationTime")

    private static var customerGroup_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CustomerGroup")

    private static var deliveryBlockReason_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryBlockReason")

    private static var deliveryDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryDate")

    private static var deliveryDocument_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryDocument")

    private static var deliveryDocumentBySupplier_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryDocumentBySupplier")

    private static var deliveryDocumentType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryDocumentType")

    private static var deliveryIsInPlant_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryIsInPlant")

    private static var deliveryPriority_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryPriority")

    private static var deliveryTime_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryTime")

    private static var deliveryVersion_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryVersion")

    private static var depreciationPercentage_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DepreciationPercentage")

    private static var distrStatusByDecentralizedWrhs_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DistrStatusByDecentralizedWrhs")

    private static var documentDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DocumentDate")

    private static var externalIdentificationType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ExternalIdentificationType")

    private static var externalTransportSystem_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ExternalTransportSystem")

    private static var factoryCalendarByCustomer_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "FactoryCalendarByCustomer")

    private static var goodsIssueOrReceiptSlipNumber_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "GoodsIssueOrReceiptSlipNumber")

    private static var goodsIssueTime_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "GoodsIssueTime")

    private static var handlingUnitInStock_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HandlingUnitInStock")

    private static var hdrGeneralIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HdrGeneralIncompletionStatus")

    private static var hdrGoodsMvtIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HdrGoodsMvtIncompletionStatus")

    private static var headerBillgIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderBillgIncompletionStatus")

    private static var headerBillingBlockReason_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderBillingBlockReason")

    private static var headerDelivIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderDelivIncompletionStatus")

    private static var headerGrossWeight_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderGrossWeight")

    private static var headerNetWeight_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderNetWeight")

    private static var headerPackingIncompletionSts_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderPackingIncompletionSts")

    private static var headerPickgIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderPickgIncompletionStatus")

    private static var headerVolume_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderVolume")

    private static var headerVolumeUnit_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderVolumeUnit")

    private static var headerWeightUnit_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderWeightUnit")

    private static var incotermsClassification_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IncotermsClassification")

    private static var incotermsTransferLocation_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IncotermsTransferLocation")

    private static var intercompanyBillingDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IntercompanyBillingDate")

    private static var internalFinancialDocument_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "InternalFinancialDocument")

    private static var isDeliveryForSingleWarehouse_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IsDeliveryForSingleWarehouse")

    private static var isExportDelivery_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IsExportDelivery")

    private static var lastChangeDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LastChangeDate")

    private static var lastChangedByUser_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LastChangedByUser")

    private static var loadingDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LoadingDate")

    private static var loadingPoint_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LoadingPoint")

    private static var loadingTime_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LoadingTime")

    private static var meansOfTransport_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "MeansOfTransport")

    private static var meansOfTransportRefMaterial_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "MeansOfTransportRefMaterial")

    private static var meansOfTransportType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "MeansOfTransportType")

    private static var orderCombinationIsAllowed_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OrderCombinationIsAllowed")

    private static var orderID_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OrderID")

    private static var overallDelivConfStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallDelivConfStatus")

    private static var overallDelivReltdBillgStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallDelivReltdBillgStatus")

    private static var overallGoodsMovementStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallGoodsMovementStatus")

    private static var overallIntcoBillingStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallIntcoBillingStatus")

    private static var overallPackingStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallPackingStatus")

    private static var overallPickingConfStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallPickingConfStatus")

    private static var overallPickingStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallPickingStatus")

    private static var overallProofOfDeliveryStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallProofOfDeliveryStatus")

    private static var overallSDProcessStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallSDProcessStatus")

    private static var overallWarehouseActivityStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallWarehouseActivityStatus")

    private static var ovrlItmDelivIncompletionSts_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmDelivIncompletionSts")

    private static var ovrlItmGdsMvtIncompletionSts_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmGdsMvtIncompletionSts")

    private static var ovrlItmGeneralIncompletionSts_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmGeneralIncompletionSts")

    private static var ovrlItmPackingIncompletionSts_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmPackingIncompletionSts")

    private static var ovrlItmPickingIncompletionSts_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmPickingIncompletionSts")

    private static var paymentGuaranteeProcedure_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PaymentGuaranteeProcedure")

    private static var pickedItemsLocation_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PickedItemsLocation")

    private static var pickingDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PickingDate")

    private static var pickingTime_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PickingTime")

    private static var plannedGoodsIssueDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PlannedGoodsIssueDate")

    private static var proofOfDeliveryDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ProofOfDeliveryDate")

    private static var proposedDeliveryRoute_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ProposedDeliveryRoute")

    private static var receivingPlant_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ReceivingPlant")

    private static var routeSchedule_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "RouteSchedule")

    private static var salesDistrict_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SalesDistrict")

    private static var salesOffice_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SalesOffice")

    private static var salesOrganization_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SalesOrganization")

    private static var sdDocumentCategory_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SDDocumentCategory")

    private static var shipmentBlockReason_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShipmentBlockReason")

    private static var shippingCondition_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShippingCondition")

    private static var shippingPoint_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShippingPoint")

    private static var shippingType_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShippingType")

    private static var shipToParty_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShipToParty")

    private static var soldToParty_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SoldToParty")

    private static var specialProcessingCode_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SpecialProcessingCode")

    private static var statisticsCurrency_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "StatisticsCurrency")

    private static var supplier_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "Supplier")

    private static var totalBlockStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TotalBlockStatus")

    private static var totalCreditCheckStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TotalCreditCheckStatus")

    private static var totalNumberOfPackage_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TotalNumberOfPackage")

    private static var transactionCurrency_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransactionCurrency")

    private static var transportationGroup_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransportationGroup")

    private static var transportationPlanningDate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransportationPlanningDate")

    private static var transportationPlanningStatus_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransportationPlanningStatus")

    private static var transportationPlanningTime_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransportationPlanningTime")

    private static var unloadingPointName_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "UnloadingPointName")

    private static var warehouse_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "Warehouse")

    private static var warehouseGate_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "WarehouseGate")

    private static var warehouseStagingArea_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "WarehouseStagingArea")

    private static var toDeliveryDocumentText_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "to_DeliveryDocumentText")

    private static var toDeliveryDocumentPartner_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "to_DeliveryDocumentPartner")

    private static var toDeliveryDocumentItem_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "to_DeliveryDocumentItem")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType)
    }

    open class var actualDeliveryRoute: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.actualDeliveryRoute_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.actualDeliveryRoute_ = value
            }
        }
    }

    open var actualDeliveryRoute: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.actualDeliveryRoute))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.actualDeliveryRoute, to: StringValue.of(optional: value))
        }
    }

    open class var actualGoodsMovementDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.actualGoodsMovementDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.actualGoodsMovementDate_ = value
            }
        }
    }

    open var actualGoodsMovementDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.actualGoodsMovementDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.actualGoodsMovementDate, to: value)
        }
    }

    open class var actualGoodsMovementTime: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.actualGoodsMovementTime_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.actualGoodsMovementTime_ = value
            }
        }
    }

    open var actualGoodsMovementTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.actualGoodsMovementTime))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.actualGoodsMovementTime, to: value)
        }
    }

    open class func array(from: EntityValueList) -> [AOutbDeliveryHeaderType] {
        return ArrayConverter.convert(from.toArray(), [AOutbDeliveryHeaderType]())
    }

    open class var billOfLading: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.billOfLading_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.billOfLading_ = value
            }
        }
    }

    open var billOfLading: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.billOfLading))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.billOfLading, to: StringValue.of(optional: value))
        }
    }

    open class var billingDocumentDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.billingDocumentDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.billingDocumentDate_ = value
            }
        }
    }

    open var billingDocumentDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.billingDocumentDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.billingDocumentDate, to: value)
        }
    }

    open class var completeDeliveryIsDefined: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.completeDeliveryIsDefined_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.completeDeliveryIsDefined_ = value
            }
        }
    }

    open var completeDeliveryIsDefined: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.completeDeliveryIsDefined))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.completeDeliveryIsDefined, to: BooleanValue.of(optional: value))
        }
    }

    open class var confirmationTime: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.confirmationTime_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.confirmationTime_ = value
            }
        }
    }

    open var confirmationTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.confirmationTime))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.confirmationTime, to: value)
        }
    }

    open func copy() -> AOutbDeliveryHeaderType {
        return CastRequired<AOutbDeliveryHeaderType>.from(self.copyEntity())
    }

    open class var createdByUser: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.createdByUser_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.createdByUser_ = value
            }
        }
    }

    open var createdByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.createdByUser))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.createdByUser, to: StringValue.of(optional: value))
        }
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.creationDate, to: value)
        }
    }

    open class var creationTime: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.creationTime_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.creationTime_ = value
            }
        }
    }

    open var creationTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.creationTime))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.creationTime, to: value)
        }
    }

    open class var customerGroup: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.customerGroup_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.customerGroup_ = value
            }
        }
    }

    open var customerGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.customerGroup))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.customerGroup, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryBlockReason: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.deliveryBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.deliveryBlockReason_ = value
            }
        }
    }

    open var deliveryBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.deliveryBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.deliveryBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.deliveryDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.deliveryDate_ = value
            }
        }
    }

    open var deliveryDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.deliveryDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.deliveryDate, to: value)
        }
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentBySupplier: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.deliveryDocumentBySupplier_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.deliveryDocumentBySupplier_ = value
            }
        }
    }

    open var deliveryDocumentBySupplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.deliveryDocumentBySupplier))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.deliveryDocumentBySupplier, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentType: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.deliveryDocumentType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.deliveryDocumentType_ = value
            }
        }
    }

    open var deliveryDocumentType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.deliveryDocumentType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.deliveryDocumentType, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryIsInPlant: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.deliveryIsInPlant_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.deliveryIsInPlant_ = value
            }
        }
    }

    open var deliveryIsInPlant: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.deliveryIsInPlant))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.deliveryIsInPlant, to: BooleanValue.of(optional: value))
        }
    }

    open class var deliveryPriority: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.deliveryPriority_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.deliveryPriority_ = value
            }
        }
    }

    open var deliveryPriority: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.deliveryPriority))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.deliveryPriority, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryTime: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.deliveryTime_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.deliveryTime_ = value
            }
        }
    }

    open var deliveryTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.deliveryTime))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.deliveryTime, to: value)
        }
    }

    open class var deliveryVersion: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.deliveryVersion_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.deliveryVersion_ = value
            }
        }
    }

    open var deliveryVersion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.deliveryVersion))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.deliveryVersion, to: StringValue.of(optional: value))
        }
    }

    open class var depreciationPercentage: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.depreciationPercentage_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.depreciationPercentage_ = value
            }
        }
    }

    open var depreciationPercentage: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.depreciationPercentage))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.depreciationPercentage, to: DecimalValue.of(optional: value))
        }
    }

    open class var distrStatusByDecentralizedWrhs: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.distrStatusByDecentralizedWrhs_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.distrStatusByDecentralizedWrhs_ = value
            }
        }
    }

    open var distrStatusByDecentralizedWrhs: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.distrStatusByDecentralizedWrhs))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.distrStatusByDecentralizedWrhs, to: StringValue.of(optional: value))
        }
    }

    open class var documentDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.documentDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.documentDate_ = value
            }
        }
    }

    open var documentDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.documentDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.documentDate, to: value)
        }
    }

    open class var externalIdentificationType: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.externalIdentificationType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.externalIdentificationType_ = value
            }
        }
    }

    open var externalIdentificationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.externalIdentificationType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.externalIdentificationType, to: StringValue.of(optional: value))
        }
    }

    open class var externalTransportSystem: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.externalTransportSystem_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.externalTransportSystem_ = value
            }
        }
    }

    open var externalTransportSystem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.externalTransportSystem))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.externalTransportSystem, to: StringValue.of(optional: value))
        }
    }

    open class var factoryCalendarByCustomer: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.factoryCalendarByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.factoryCalendarByCustomer_ = value
            }
        }
    }

    open var factoryCalendarByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.factoryCalendarByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.factoryCalendarByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var goodsIssueOrReceiptSlipNumber: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber_ = value
            }
        }
    }

    open var goodsIssueOrReceiptSlipNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber, to: StringValue.of(optional: value))
        }
    }

    open class var goodsIssueTime: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.goodsIssueTime_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.goodsIssueTime_ = value
            }
        }
    }

    open var goodsIssueTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.goodsIssueTime))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.goodsIssueTime, to: value)
        }
    }

    open class var handlingUnitInStock: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.handlingUnitInStock_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.handlingUnitInStock_ = value
            }
        }
    }

    open var handlingUnitInStock: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.handlingUnitInStock))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.handlingUnitInStock, to: StringValue.of(optional: value))
        }
    }

    open class var hdrGeneralIncompletionStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.hdrGeneralIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.hdrGeneralIncompletionStatus_ = value
            }
        }
    }

    open var hdrGeneralIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.hdrGeneralIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.hdrGeneralIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var hdrGoodsMvtIncompletionStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus_ = value
            }
        }
    }

    open var hdrGoodsMvtIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var headerBillgIncompletionStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.headerBillgIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.headerBillgIncompletionStatus_ = value
            }
        }
    }

    open var headerBillgIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.headerBillgIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.headerBillgIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var headerBillingBlockReason: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.headerBillingBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.headerBillingBlockReason_ = value
            }
        }
    }

    open var headerBillingBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.headerBillingBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.headerBillingBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var headerDelivIncompletionStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.headerDelivIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.headerDelivIncompletionStatus_ = value
            }
        }
    }

    open var headerDelivIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.headerDelivIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.headerDelivIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var headerGrossWeight: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.headerGrossWeight_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.headerGrossWeight_ = value
            }
        }
    }

    open var headerGrossWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.headerGrossWeight))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.headerGrossWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var headerNetWeight: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.headerNetWeight_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.headerNetWeight_ = value
            }
        }
    }

    open var headerNetWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.headerNetWeight))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.headerNetWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var headerPackingIncompletionSts: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.headerPackingIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.headerPackingIncompletionSts_ = value
            }
        }
    }

    open var headerPackingIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.headerPackingIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.headerPackingIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var headerPickgIncompletionStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.headerPickgIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.headerPickgIncompletionStatus_ = value
            }
        }
    }

    open var headerPickgIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.headerPickgIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.headerPickgIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var headerVolume: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.headerVolume_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.headerVolume_ = value
            }
        }
    }

    open var headerVolume: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.headerVolume))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.headerVolume, to: DecimalValue.of(optional: value))
        }
    }

    open class var headerVolumeUnit: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.headerVolumeUnit_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.headerVolumeUnit_ = value
            }
        }
    }

    open var headerVolumeUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.headerVolumeUnit))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.headerVolumeUnit, to: StringValue.of(optional: value))
        }
    }

    open class var headerWeightUnit: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.headerWeightUnit_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.headerWeightUnit_ = value
            }
        }
    }

    open var headerWeightUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.headerWeightUnit))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.headerWeightUnit, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsClassification: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.incotermsClassification_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.incotermsClassification_ = value
            }
        }
    }

    open var incotermsClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.incotermsClassification))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.incotermsClassification, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsTransferLocation: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.incotermsTransferLocation_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.incotermsTransferLocation_ = value
            }
        }
    }

    open var incotermsTransferLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.incotermsTransferLocation))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.incotermsTransferLocation, to: StringValue.of(optional: value))
        }
    }

    open class var intercompanyBillingDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.intercompanyBillingDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.intercompanyBillingDate_ = value
            }
        }
    }

    open var intercompanyBillingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.intercompanyBillingDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.intercompanyBillingDate, to: value)
        }
    }

    open class var internalFinancialDocument: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.internalFinancialDocument_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.internalFinancialDocument_ = value
            }
        }
    }

    open var internalFinancialDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.internalFinancialDocument))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.internalFinancialDocument, to: StringValue.of(optional: value))
        }
    }

    open class var isDeliveryForSingleWarehouse: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.isDeliveryForSingleWarehouse_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.isDeliveryForSingleWarehouse_ = value
            }
        }
    }

    open var isDeliveryForSingleWarehouse: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.isDeliveryForSingleWarehouse))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.isDeliveryForSingleWarehouse, to: StringValue.of(optional: value))
        }
    }

    open class var isExportDelivery: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.isExportDelivery_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.isExportDelivery_ = value
            }
        }
    }

    open var isExportDelivery: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.isExportDelivery))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.isExportDelivery, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(deliveryDocument: String?) -> EntityKey {
        return EntityKey().with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument))
    }

    open class var lastChangeDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.lastChangeDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.lastChangeDate_ = value
            }
        }
    }

    open var lastChangeDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.lastChangeDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.lastChangeDate, to: value)
        }
    }

    open class var lastChangedByUser: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.lastChangedByUser_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.lastChangedByUser_ = value
            }
        }
    }

    open var lastChangedByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.lastChangedByUser))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.lastChangedByUser, to: StringValue.of(optional: value))
        }
    }

    open class var loadingDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.loadingDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.loadingDate_ = value
            }
        }
    }

    open var loadingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.loadingDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.loadingDate, to: value)
        }
    }

    open class var loadingPoint: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.loadingPoint_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.loadingPoint_ = value
            }
        }
    }

    open var loadingPoint: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.loadingPoint))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.loadingPoint, to: StringValue.of(optional: value))
        }
    }

    open class var loadingTime: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.loadingTime_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.loadingTime_ = value
            }
        }
    }

    open var loadingTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.loadingTime))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.loadingTime, to: value)
        }
    }

    open class var meansOfTransport: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.meansOfTransport_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.meansOfTransport_ = value
            }
        }
    }

    open var meansOfTransport: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.meansOfTransport))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.meansOfTransport, to: StringValue.of(optional: value))
        }
    }

    open class var meansOfTransportRefMaterial: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.meansOfTransportRefMaterial_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.meansOfTransportRefMaterial_ = value
            }
        }
    }

    open var meansOfTransportRefMaterial: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.meansOfTransportRefMaterial))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.meansOfTransportRefMaterial, to: StringValue.of(optional: value))
        }
    }

    open class var meansOfTransportType: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.meansOfTransportType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.meansOfTransportType_ = value
            }
        }
    }

    open var meansOfTransportType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.meansOfTransportType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.meansOfTransportType, to: StringValue.of(optional: value))
        }
    }

    open var old: AOutbDeliveryHeaderType {
        return CastRequired<AOutbDeliveryHeaderType>.from(self.oldEntity)
    }

    open class var orderCombinationIsAllowed: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.orderCombinationIsAllowed_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.orderCombinationIsAllowed_ = value
            }
        }
    }

    open var orderCombinationIsAllowed: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.orderCombinationIsAllowed))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.orderCombinationIsAllowed, to: BooleanValue.of(optional: value))
        }
    }

    open class var orderID: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.orderID_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.orderID_ = value
            }
        }
    }

    open var orderID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.orderID))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.orderID, to: StringValue.of(optional: value))
        }
    }

    open class var overallDelivConfStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.overallDelivConfStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.overallDelivConfStatus_ = value
            }
        }
    }

    open var overallDelivConfStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.overallDelivConfStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.overallDelivConfStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallDelivReltdBillgStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.overallDelivReltdBillgStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.overallDelivReltdBillgStatus_ = value
            }
        }
    }

    open var overallDelivReltdBillgStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.overallDelivReltdBillgStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.overallDelivReltdBillgStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallGoodsMovementStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.overallGoodsMovementStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.overallGoodsMovementStatus_ = value
            }
        }
    }

    open var overallGoodsMovementStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.overallGoodsMovementStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.overallGoodsMovementStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallIntcoBillingStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.overallIntcoBillingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.overallIntcoBillingStatus_ = value
            }
        }
    }

    open var overallIntcoBillingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.overallIntcoBillingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.overallIntcoBillingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallPackingStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.overallPackingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.overallPackingStatus_ = value
            }
        }
    }

    open var overallPackingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.overallPackingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.overallPackingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallPickingConfStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.overallPickingConfStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.overallPickingConfStatus_ = value
            }
        }
    }

    open var overallPickingConfStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.overallPickingConfStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.overallPickingConfStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallPickingStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.overallPickingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.overallPickingStatus_ = value
            }
        }
    }

    open var overallPickingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.overallPickingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.overallPickingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallProofOfDeliveryStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.overallProofOfDeliveryStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.overallProofOfDeliveryStatus_ = value
            }
        }
    }

    open var overallProofOfDeliveryStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.overallProofOfDeliveryStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.overallProofOfDeliveryStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallSDProcessStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.overallSDProcessStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.overallSDProcessStatus_ = value
            }
        }
    }

    open var overallSDProcessStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.overallSDProcessStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.overallSDProcessStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallWarehouseActivityStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.overallWarehouseActivityStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.overallWarehouseActivityStatus_ = value
            }
        }
    }

    open var overallWarehouseActivityStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.overallWarehouseActivityStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.overallWarehouseActivityStatus, to: StringValue.of(optional: value))
        }
    }

    open class var ovrlItmDelivIncompletionSts: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.ovrlItmDelivIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.ovrlItmDelivIncompletionSts_ = value
            }
        }
    }

    open var ovrlItmDelivIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.ovrlItmDelivIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.ovrlItmDelivIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var ovrlItmGdsMvtIncompletionSts: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts_ = value
            }
        }
    }

    open var ovrlItmGdsMvtIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var ovrlItmGeneralIncompletionSts: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.ovrlItmGeneralIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.ovrlItmGeneralIncompletionSts_ = value
            }
        }
    }

    open var ovrlItmGeneralIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.ovrlItmGeneralIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.ovrlItmGeneralIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var ovrlItmPackingIncompletionSts: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.ovrlItmPackingIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.ovrlItmPackingIncompletionSts_ = value
            }
        }
    }

    open var ovrlItmPackingIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.ovrlItmPackingIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.ovrlItmPackingIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var ovrlItmPickingIncompletionSts: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.ovrlItmPickingIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.ovrlItmPickingIncompletionSts_ = value
            }
        }
    }

    open var ovrlItmPickingIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.ovrlItmPickingIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.ovrlItmPickingIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var paymentGuaranteeProcedure: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.paymentGuaranteeProcedure_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.paymentGuaranteeProcedure_ = value
            }
        }
    }

    open var paymentGuaranteeProcedure: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.paymentGuaranteeProcedure))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.paymentGuaranteeProcedure, to: StringValue.of(optional: value))
        }
    }

    open class var pickedItemsLocation: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.pickedItemsLocation_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.pickedItemsLocation_ = value
            }
        }
    }

    open var pickedItemsLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.pickedItemsLocation))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.pickedItemsLocation, to: StringValue.of(optional: value))
        }
    }

    open class var pickingDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.pickingDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.pickingDate_ = value
            }
        }
    }

    open var pickingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.pickingDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.pickingDate, to: value)
        }
    }

    open class var pickingTime: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.pickingTime_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.pickingTime_ = value
            }
        }
    }

    open var pickingTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.pickingTime))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.pickingTime, to: value)
        }
    }

    open class var plannedGoodsIssueDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.plannedGoodsIssueDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.plannedGoodsIssueDate_ = value
            }
        }
    }

    open var plannedGoodsIssueDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.plannedGoodsIssueDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.plannedGoodsIssueDate, to: value)
        }
    }

    open class var proofOfDeliveryDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.proofOfDeliveryDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.proofOfDeliveryDate_ = value
            }
        }
    }

    open var proofOfDeliveryDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.proofOfDeliveryDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.proofOfDeliveryDate, to: value)
        }
    }

    open class var proposedDeliveryRoute: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.proposedDeliveryRoute_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.proposedDeliveryRoute_ = value
            }
        }
    }

    open var proposedDeliveryRoute: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.proposedDeliveryRoute))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.proposedDeliveryRoute, to: StringValue.of(optional: value))
        }
    }

    open class var receivingPlant: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.receivingPlant_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.receivingPlant_ = value
            }
        }
    }

    open var receivingPlant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.receivingPlant))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.receivingPlant, to: StringValue.of(optional: value))
        }
    }

    open class var receivinglocationtimezone: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.receivinglocationtimezone_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.receivinglocationtimezone_ = value
            }
        }
    }

    open var receivinglocationtimezone: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.receivinglocationtimezone))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.receivinglocationtimezone, to: StringValue.of(optional: value))
        }
    }

    open class var routeSchedule: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.routeSchedule_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.routeSchedule_ = value
            }
        }
    }

    open var routeSchedule: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.routeSchedule))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.routeSchedule, to: StringValue.of(optional: value))
        }
    }

    open class var salesDistrict: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.salesDistrict_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.salesDistrict_ = value
            }
        }
    }

    open var salesDistrict: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.salesDistrict))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.salesDistrict, to: StringValue.of(optional: value))
        }
    }

    open class var salesOffice: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.salesOffice_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.salesOffice_ = value
            }
        }
    }

    open var salesOffice: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.salesOffice))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.salesOffice, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var sdDocumentCategory: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.sdDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.sdDocumentCategory_ = value
            }
        }
    }

    open var sdDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.sdDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.sdDocumentCategory, to: StringValue.of(optional: value))
        }
    }

    open class var shipToParty: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.shipToParty_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.shipToParty_ = value
            }
        }
    }

    open var shipToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.shipToParty))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.shipToParty, to: StringValue.of(optional: value))
        }
    }

    open class var shipmentBlockReason: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.shipmentBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.shipmentBlockReason_ = value
            }
        }
    }

    open var shipmentBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.shipmentBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.shipmentBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var shippingCondition: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.shippingCondition_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.shippingCondition_ = value
            }
        }
    }

    open var shippingCondition: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.shippingCondition))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.shippingCondition, to: StringValue.of(optional: value))
        }
    }

    open class var shippingPoint: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.shippingPoint_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.shippingPoint_ = value
            }
        }
    }

    open var shippingPoint: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.shippingPoint))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.shippingPoint, to: StringValue.of(optional: value))
        }
    }

    open class var shippingType: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.shippingType_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.shippingType_ = value
            }
        }
    }

    open var shippingType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.shippingType))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.shippingType, to: StringValue.of(optional: value))
        }
    }

    open class var shippinglocationtimezone: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.shippinglocationtimezone_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.shippinglocationtimezone_ = value
            }
        }
    }

    open var shippinglocationtimezone: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.shippinglocationtimezone))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.shippinglocationtimezone, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.soldToParty, to: StringValue.of(optional: value))
        }
    }

    open class var specialProcessingCode: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.specialProcessingCode_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.specialProcessingCode_ = value
            }
        }
    }

    open var specialProcessingCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.specialProcessingCode))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.specialProcessingCode, to: StringValue.of(optional: value))
        }
    }

    open class var statisticsCurrency: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.statisticsCurrency_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.statisticsCurrency_ = value
            }
        }
    }

    open var statisticsCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.statisticsCurrency))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.statisticsCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var supplier: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.supplier_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.supplier_ = value
            }
        }
    }

    open var supplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.supplier))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.supplier, to: StringValue.of(optional: value))
        }
    }

    open class var toDeliveryDocumentItem: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.toDeliveryDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.toDeliveryDocumentItem_ = value
            }
        }
    }

    open var toDeliveryDocumentItem: [AOutbDeliveryItemType] {
        get {
            return ArrayConverter.convert(AOutbDeliveryHeaderType.toDeliveryDocumentItem.entityList(from: self).toArray(), [AOutbDeliveryItemType]())
        }
        set(value) {
            AOutbDeliveryHeaderType.toDeliveryDocumentItem.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class var toDeliveryDocumentPartner: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.toDeliveryDocumentPartner_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.toDeliveryDocumentPartner_ = value
            }
        }
    }

    open var toDeliveryDocumentPartner: [AOutbDeliveryPartnerType] {
        get {
            return ArrayConverter.convert(AOutbDeliveryHeaderType.toDeliveryDocumentPartner.entityList(from: self).toArray(), [AOutbDeliveryPartnerType]())
        }
        set(value) {
            AOutbDeliveryHeaderType.toDeliveryDocumentPartner.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class var toDeliveryDocumentText: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.toDeliveryDocumentText_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.toDeliveryDocumentText_ = value
            }
        }
    }

    open var toDeliveryDocumentText: [AOutbDeliveryHeaderTextType] {
        get {
            return ArrayConverter.convert(AOutbDeliveryHeaderType.toDeliveryDocumentText.entityList(from: self).toArray(), [AOutbDeliveryHeaderTextType]())
        }
        set(value) {
            AOutbDeliveryHeaderType.toDeliveryDocumentText.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class var totalBlockStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.totalBlockStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.totalBlockStatus_ = value
            }
        }
    }

    open var totalBlockStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.totalBlockStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.totalBlockStatus, to: StringValue.of(optional: value))
        }
    }

    open class var totalCreditCheckStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.totalCreditCheckStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.totalCreditCheckStatus_ = value
            }
        }
    }

    open var totalCreditCheckStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.totalCreditCheckStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.totalCreditCheckStatus, to: StringValue.of(optional: value))
        }
    }

    open class var totalNumberOfPackage: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.totalNumberOfPackage_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.totalNumberOfPackage_ = value
            }
        }
    }

    open var totalNumberOfPackage: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.totalNumberOfPackage))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.totalNumberOfPackage, to: StringValue.of(optional: value))
        }
    }

    open class var transactionCurrency: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.transactionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.transactionCurrency_ = value
            }
        }
    }

    open var transactionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.transactionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.transactionCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var transportationGroup: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.transportationGroup_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.transportationGroup_ = value
            }
        }
    }

    open var transportationGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.transportationGroup))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.transportationGroup, to: StringValue.of(optional: value))
        }
    }

    open class var transportationPlanningDate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.transportationPlanningDate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.transportationPlanningDate_ = value
            }
        }
    }

    open var transportationPlanningDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.transportationPlanningDate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.transportationPlanningDate, to: value)
        }
    }

    open class var transportationPlanningStatus: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.transportationPlanningStatus_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.transportationPlanningStatus_ = value
            }
        }
    }

    open var transportationPlanningStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.transportationPlanningStatus))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.transportationPlanningStatus, to: StringValue.of(optional: value))
        }
    }

    open class var transportationPlanningTime: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.transportationPlanningTime_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.transportationPlanningTime_ = value
            }
        }
    }

    open var transportationPlanningTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AOutbDeliveryHeaderType.transportationPlanningTime))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.transportationPlanningTime, to: value)
        }
    }

    open class var unloadingPointName: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.unloadingPointName_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.unloadingPointName_ = value
            }
        }
    }

    open var unloadingPointName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.unloadingPointName))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.unloadingPointName, to: StringValue.of(optional: value))
        }
    }

    open class var warehouse: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.warehouse_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.warehouse_ = value
            }
        }
    }

    open var warehouse: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.warehouse))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.warehouse, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseGate: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.warehouseGate_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.warehouseGate_ = value
            }
        }
    }

    open var warehouseGate: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.warehouseGate))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.warehouseGate, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseStagingArea: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                return AOutbDeliveryHeaderType.warehouseStagingArea_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderType.self) }
            do {
                AOutbDeliveryHeaderType.warehouseStagingArea_ = value
            }
        }
    }

    open var warehouseStagingArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderType.warehouseStagingArea))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderType.warehouseStagingArea, to: StringValue.of(optional: value))
        }
    }
}
