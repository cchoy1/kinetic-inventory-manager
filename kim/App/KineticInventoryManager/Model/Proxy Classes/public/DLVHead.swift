// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class DLVHead: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var inboundDeliveryUUID_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "InboundDeliveryUUID")

    private static var bsKeyASN_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "BSKeyASN")

    private static var bsKeyERP_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "BSKeyERP")

    private static var bsKeyPOAggr_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "BSKeyPOAggr")

    private static var carrier_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "Carrier")

    private static var carrierName_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "CarrierName")

    private static var docNoASN_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "DocNoASN")

    private static var docNoERP_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "DocNoERP")

    private static var docNoPOAggr_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "DocNoPOAggr")

    private static var numberOfPODocuments_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "NumberOfPODocuments")

    private static var goodsReceiptStatus_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "GoodsReceiptStatus")

    private static var goodsReceiptStatusText_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "GoodsReceiptStatusText")

    private static var completionStatusName_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "CompletionStatusName")

    private static var inboundDelivery_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "InboundDelivery")

    private static var inboundDeliveryType_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "InboundDeliveryType")

    private static var lastChangedDateTime_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "LastChangedDateTime")

    private static var numberOfHU_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "NumberOfHU")

    private static var numberOfItems_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "NumberOfItems")

    private static var numberOfOpenItems_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "NumberOfOpenItems")

    private static var numberOfProducts_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "NumberOfProducts")

    private static var plannedDeliveryEndDate_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "PlannedDeliveryEndDate")

    private static var product_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "Product")

    private static var productName_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "ProductName")

    private static var shipFromParty_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "ShipFromParty")

    private static var shipFromPartyName_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "ShipFromPartyName")

    private static var includeCompletedDelivery_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "IncludeCompletedDelivery")

    private static var uxFcCarrier_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "UxFc_Carrier")

    private static var uxFcPlannedDeliveryEndDate_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "UxFc_PlannedDeliveryEndDate")

    private static var uxFcADeletable_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "UxFcA_Deletable")

    private static var uxFcAGoodsReceipt_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "UxFcA_GoodsReceipt")

    private static var uxFcAUpdatable_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "UxFcA_Updatable")

    private static var inboundDeliveryTypeText_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "InboundDeliveryTypeText")

    private static var docNoPPOAggr_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "DocNoPPOAggr")

    private static var numberOfPPODocuments_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "NumberOfPPODocuments")

    private static var warehouseNumber_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "WarehouseNumber")

    private static var productionIndicator_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "ProductionIndicator")

    private static var blockedOverallStatus_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "BlockedOverallStatus")

    private static var blockedOverallStatusName_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "BlockedOverallStatusName")

    private static var packingStatus_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "PackingStatus")

    private static var packingStatusName_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "PackingStatusName")

    private static var existsWT_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "ExistsWT")

    private static var items_: Property = Ec1Metadata.EntityTypes.dlvHead.property(withName: "Items")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.dlvHead)
    }

    open class func array(from: EntityValueList) -> [DLVHead] {
        return ArrayConverter.convert(from.toArray(), [DLVHead]())
    }

    open class var blockedOverallStatus: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.blockedOverallStatus_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.blockedOverallStatus_ = value
            }
        }
    }

    open var blockedOverallStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.blockedOverallStatus))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.blockedOverallStatus, to: StringValue.of(optional: value))
        }
    }

    open class var blockedOverallStatusName: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.blockedOverallStatusName_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.blockedOverallStatusName_ = value
            }
        }
    }

    open var blockedOverallStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.blockedOverallStatusName))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.blockedOverallStatusName, to: StringValue.of(optional: value))
        }
    }

    open class var bsKeyASN: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.bsKeyASN_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.bsKeyASN_ = value
            }
        }
    }

    open var bsKeyASN: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.bsKeyASN))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.bsKeyASN, to: StringValue.of(optional: value))
        }
    }

    open class var bsKeyERP: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.bsKeyERP_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.bsKeyERP_ = value
            }
        }
    }

    open var bsKeyERP: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.bsKeyERP))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.bsKeyERP, to: StringValue.of(optional: value))
        }
    }

    open class var bsKeyPOAggr: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.bsKeyPOAggr_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.bsKeyPOAggr_ = value
            }
        }
    }

    open var bsKeyPOAggr: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.bsKeyPOAggr))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.bsKeyPOAggr, to: StringValue.of(optional: value))
        }
    }

    open class var carrier: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.carrier_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.carrier_ = value
            }
        }
    }

    open var carrier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.carrier))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.carrier, to: StringValue.of(optional: value))
        }
    }

    open class var carrierName: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.carrierName_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.carrierName_ = value
            }
        }
    }

    open var carrierName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.carrierName))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.carrierName, to: StringValue.of(optional: value))
        }
    }

    open class var completionStatusName: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.completionStatusName_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.completionStatusName_ = value
            }
        }
    }

    open var completionStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.completionStatusName))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.completionStatusName, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> DLVHead {
        return CastRequired<DLVHead>.from(self.copyEntity())
    }

    open class var docNoASN: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.docNoASN_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.docNoASN_ = value
            }
        }
    }

    open var docNoASN: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.docNoASN))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.docNoASN, to: StringValue.of(optional: value))
        }
    }

    open class var docNoERP: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.docNoERP_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.docNoERP_ = value
            }
        }
    }

    open var docNoERP: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.docNoERP))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.docNoERP, to: StringValue.of(optional: value))
        }
    }

    open class var docNoPOAggr: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.docNoPOAggr_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.docNoPOAggr_ = value
            }
        }
    }

    open var docNoPOAggr: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.docNoPOAggr))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.docNoPOAggr, to: StringValue.of(optional: value))
        }
    }

    open class var docNoPPOAggr: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.docNoPPOAggr_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.docNoPPOAggr_ = value
            }
        }
    }

    open var docNoPPOAggr: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.docNoPPOAggr))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.docNoPPOAggr, to: StringValue.of(optional: value))
        }
    }

    open class var existsWT: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.existsWT_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.existsWT_ = value
            }
        }
    }

    open var existsWT: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVHead.existsWT))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.existsWT, to: BooleanValue.of(optional: value))
        }
    }

    open class var goodsReceiptStatus: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.goodsReceiptStatus_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.goodsReceiptStatus_ = value
            }
        }
    }

    open var goodsReceiptStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.goodsReceiptStatus))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.goodsReceiptStatus, to: StringValue.of(optional: value))
        }
    }

    open class var goodsReceiptStatusText: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.goodsReceiptStatusText_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.goodsReceiptStatusText_ = value
            }
        }
    }

    open var goodsReceiptStatusText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.goodsReceiptStatusText))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.goodsReceiptStatusText, to: StringValue.of(optional: value))
        }
    }

    open class var inboundDelivery: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.inboundDelivery_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.inboundDelivery_ = value
            }
        }
    }

    open var inboundDelivery: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.inboundDelivery))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.inboundDelivery, to: StringValue.of(optional: value))
        }
    }

    open class var inboundDeliveryType: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.inboundDeliveryType_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.inboundDeliveryType_ = value
            }
        }
    }

    open var inboundDeliveryType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.inboundDeliveryType))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.inboundDeliveryType, to: StringValue.of(optional: value))
        }
    }

    open class var inboundDeliveryTypeText: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.inboundDeliveryTypeText_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.inboundDeliveryTypeText_ = value
            }
        }
    }

    open var inboundDeliveryTypeText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.inboundDeliveryTypeText))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.inboundDeliveryTypeText, to: StringValue.of(optional: value))
        }
    }

    open class var inboundDeliveryUUID: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.inboundDeliveryUUID_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.inboundDeliveryUUID_ = value
            }
        }
    }

    open var inboundDeliveryUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: DLVHead.inboundDeliveryUUID))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.inboundDeliveryUUID, to: value)
        }
    }

    open class var includeCompletedDelivery: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.includeCompletedDelivery_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.includeCompletedDelivery_ = value
            }
        }
    }

    open var includeCompletedDelivery: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVHead.includeCompletedDelivery))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.includeCompletedDelivery, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var items: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.items_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.items_ = value
            }
        }
    }

    open var items: [DLVItem] {
        get {
            return ArrayConverter.convert(DLVHead.items.entityList(from: self).toArray(), [DLVItem]())
        }
        set(value) {
            DLVHead.items.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class func key(inboundDeliveryUUID: GuidValue?) -> EntityKey {
        return EntityKey().with(name: "InboundDeliveryUUID", value: inboundDeliveryUUID)
    }

    open class var lastChangedDateTime: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.lastChangedDateTime_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.lastChangedDateTime_ = value
            }
        }
    }

    open var lastChangedDateTime: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: DLVHead.lastChangedDateTime))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.lastChangedDateTime, to: value)
        }
    }

    open class var numberOfHU: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.numberOfHU_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.numberOfHU_ = value
            }
        }
    }

    open var numberOfHU: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: DLVHead.numberOfHU))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.numberOfHU, to: IntValue.of(optional: value))
        }
    }

    open class var numberOfItems: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.numberOfItems_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.numberOfItems_ = value
            }
        }
    }

    open var numberOfItems: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: DLVHead.numberOfItems))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.numberOfItems, to: IntValue.of(optional: value))
        }
    }

    open class var numberOfOpenItems: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.numberOfOpenItems_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.numberOfOpenItems_ = value
            }
        }
    }

    open var numberOfOpenItems: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: DLVHead.numberOfOpenItems))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.numberOfOpenItems, to: IntValue.of(optional: value))
        }
    }

    open class var numberOfPODocuments: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.numberOfPODocuments_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.numberOfPODocuments_ = value
            }
        }
    }

    open var numberOfPODocuments: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: DLVHead.numberOfPODocuments))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.numberOfPODocuments, to: IntValue.of(optional: value))
        }
    }

    open class var numberOfPPODocuments: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.numberOfPPODocuments_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.numberOfPPODocuments_ = value
            }
        }
    }

    open var numberOfPPODocuments: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: DLVHead.numberOfPPODocuments))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.numberOfPPODocuments, to: IntValue.of(optional: value))
        }
    }

    open class var numberOfProducts: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.numberOfProducts_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.numberOfProducts_ = value
            }
        }
    }

    open var numberOfProducts: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: DLVHead.numberOfProducts))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.numberOfProducts, to: IntValue.of(optional: value))
        }
    }

    open var old: DLVHead {
        return CastRequired<DLVHead>.from(self.oldEntity)
    }

    open class var packingStatus: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.packingStatus_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.packingStatus_ = value
            }
        }
    }

    open var packingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.packingStatus))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.packingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var packingStatusName: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.packingStatusName_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.packingStatusName_ = value
            }
        }
    }

    open var packingStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.packingStatusName))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.packingStatusName, to: StringValue.of(optional: value))
        }
    }

    open class var plannedDeliveryEndDate: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.plannedDeliveryEndDate_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.plannedDeliveryEndDate_ = value
            }
        }
    }

    open var plannedDeliveryEndDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: DLVHead.plannedDeliveryEndDate))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.plannedDeliveryEndDate, to: value)
        }
    }

    open class var product: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.product_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.product_ = value
            }
        }
    }

    open var product: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.product))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.product, to: StringValue.of(optional: value))
        }
    }

    open class var productName: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.productName_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.productName_ = value
            }
        }
    }

    open var productName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.productName))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.productName, to: StringValue.of(optional: value))
        }
    }

    open class var productionIndicator: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.productionIndicator_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.productionIndicator_ = value
            }
        }
    }

    open var productionIndicator: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.productionIndicator))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.productionIndicator, to: StringValue.of(optional: value))
        }
    }

    open class var shipFromParty: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.shipFromParty_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.shipFromParty_ = value
            }
        }
    }

    open var shipFromParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.shipFromParty))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.shipFromParty, to: StringValue.of(optional: value))
        }
    }

    open class var shipFromPartyName: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.shipFromPartyName_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.shipFromPartyName_ = value
            }
        }
    }

    open var shipFromPartyName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.shipFromPartyName))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.shipFromPartyName, to: StringValue.of(optional: value))
        }
    }

    open class var uxFcADeletable: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.uxFcADeletable_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.uxFcADeletable_ = value
            }
        }
    }

    open var uxFcADeletable: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVHead.uxFcADeletable))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.uxFcADeletable, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcAGoodsReceipt: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.uxFcAGoodsReceipt_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.uxFcAGoodsReceipt_ = value
            }
        }
    }

    open var uxFcAGoodsReceipt: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVHead.uxFcAGoodsReceipt))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.uxFcAGoodsReceipt, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcAUpdatable: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.uxFcAUpdatable_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.uxFcAUpdatable_ = value
            }
        }
    }

    open var uxFcAUpdatable: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVHead.uxFcAUpdatable))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.uxFcAUpdatable, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcCarrier: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.uxFcCarrier_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.uxFcCarrier_ = value
            }
        }
    }

    open var uxFcCarrier: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: DLVHead.uxFcCarrier))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.uxFcCarrier, to: UnsignedByte.of(optional: value))
        }
    }

    open class var uxFcPlannedDeliveryEndDate: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.uxFcPlannedDeliveryEndDate_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.uxFcPlannedDeliveryEndDate_ = value
            }
        }
    }

    open var uxFcPlannedDeliveryEndDate: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: DLVHead.uxFcPlannedDeliveryEndDate))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.uxFcPlannedDeliveryEndDate, to: UnsignedByte.of(optional: value))
        }
    }

    open class var warehouseNumber: Property {
        get {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                return DLVHead.warehouseNumber_
            }
        }
        set(value) {
            objc_sync_enter(DLVHead.self)
            defer { objc_sync_exit(DLVHead.self) }
            do {
                DLVHead.warehouseNumber_ = value
            }
        }
    }

    open var warehouseNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVHead.warehouseNumber))
        }
        set(value) {
            self.setOptionalValue(for: DLVHead.warehouseNumber, to: StringValue.of(optional: value))
        }
    }
}
