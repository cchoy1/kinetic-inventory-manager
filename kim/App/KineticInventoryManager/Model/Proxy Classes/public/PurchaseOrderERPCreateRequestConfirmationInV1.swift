// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class PurchaseOrderERPCreateRequestConfirmationInV1: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var id_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ID")

    private static var processingTypeCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProcessingTypeCode")

    private static var date_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Date")

    private static var supplyingPlantID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SupplyingPlantID")

    private static var responsiblePurchasingOrganisationParty_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ResponsiblePurchasingOrganisationParty")

    private static var responsiblePurchasingGroupParty_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ResponsiblePurchasingGroupParty")

    private static var internalID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InternalID")

    private static var languageCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "languageCode")

    private static var item_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Item")

    private static var uuid_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "UUID")

    private static var referenceID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ReferenceID")

    private static var referenceUUID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ReferenceUUID")

    private static var creationDate_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CreationDate")

    private static var creationUserAccountID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CreationUserAccountID")

    private static var supplyingPlantName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SupplyingPlantName")

    private static var completenessAndValidationStatusCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CompletenessAndValidationStatusCode")

    private static var releaseStatusCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ReleaseStatusCode")

    private static var unitCurrency_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "UnitCurrency")

    private static var quotedCurrency_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "QuotedCurrency")

    private static var rate_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Rate")

    private static var quotationDateTime_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "QuotationDateTime")

    private static var exchangeRateFixedIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ExchangeRateFixedIndicator")

    private static var sellerReferenceNote_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SellerReferenceNote")

    private static var buyerReferenceNote_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BuyerReferenceNote")

    private static var formattedName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FormattedName")

    private static var roleCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RoleCode")

    private static var classificationCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ClassificationCode")

    private static var transferLocationName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "TransferLocationName")

    private static var code_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Code")

    private static var daysValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DaysValue")

    private static var dayOfMonthValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DayOfMonthValue")

    private static var monthOffsetValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MonthOffsetValue")

    private static var endDate_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "EndDate")

    private static var percent_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Percent")

    private static var fullPaymentDueDaysValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FullPaymentDueDaysValue")

    private static var typeCode__: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "TypeCode")

    private static var majorLevelOrdinalNumberValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MajorLevelOrdinalNumberValue")

    private static var minorLevelOrdinalNumberValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MinorLevelOrdinalNumberValue")

    private static var decimalValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DecimalValue")

    private static var measureUnitCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MeasureUnitCode")

    private static var currencyCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CurrencyCode")

    private static var baseDecimalValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BaseDecimalValue")

    private static var baseMeasureUnitCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BaseMeasureUnitCode")

    private static var baseCurrencyCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BaseCurrencyCode")

    private static var groupID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "GroupID")

    private static var expectedPurchasingDocumentItemConfirmationTypeCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ExpectedPurchasingDocumentItemConfirmationTypeCode")

    private static var blockedIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BlockedIndicator")

    private static var returnsIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ReturnsIndicator")

    private static var deliveryBasedInvoiceVerificationIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DeliveryBasedInvoiceVerificationIndicator")

    private static var evaluatedReceiptSettlementIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "EvaluatedReceiptSettlementIndicator")

    private static var inboundDeliveryCompletedIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InboundDeliveryCompletedIndicator")

    private static var outboundDeliveryCompletedIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "OutboundDeliveryCompletedIndicator")

    private static var invoiceVerificationCompletedIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InvoiceVerificationCompletedIndicator")

    private static var unitCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "unitCode")

    private static var requesterPersonFormattedName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RequesterPersonFormattedName")

    private static var receivingPlantID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ReceivingPlantID")

    private static var productProcurementArrangmentID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProductProcurementArrangmentID")

    private static var retailEventID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RetailEventID")

    private static var retailAllocationID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RetailAllocationID")

    private static var sellerAssortmentProductGroupID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SellerAssortmentProductGroupID")

    private static var currencyCode2_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "currencyCode")

    private static var baseQuantityTypeCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BaseQuantityTypeCode")

    private static var requirementCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RequirementCode")

    private static var parentItemID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ParentItemID")

    private static var description_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Description")

    private static var addressID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AddressID")

    private static var organisationFormattedName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "OrganisationFormattedName")

    private static var formOfAddressCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FormOfAddressCode")

    private static var formOfAddressName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FormOfAddressName")

    private static var givenName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "GivenName")

    private static var middleName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MiddleName")

    private static var familyName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FamilyName")

    private static var additionalFamilyName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalFamilyName")

    private static var birthName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BirthName")

    private static var nickName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NickName")

    private static var initialsName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InitialsName")

    private static var academicTitleCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AcademicTitleCode")

    private static var academicTitleName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AcademicTitleName")

    private static var additionalAcademicTitleCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalAcademicTitleCode")

    private static var additionalAcademicTitleName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalAcademicTitleName")

    private static var namePrefixCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NamePrefixCode")

    private static var namePrefixName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NamePrefixName")

    private static var additionalNamePrefixCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalNamePrefixCode")

    private static var additionalNamePrefixName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalNamePrefixName")

    private static var nameSupplementCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NameSupplementCode")

    private static var nameSupplementName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NameSupplementName")

    private static var deviatingFullName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DeviatingFullName")

    private static var nameFormatCountryCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NameFormatCountryCode")

    private static var nameFormatCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NameFormatCode")

    private static var functionalTitleName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FunctionalTitleName")

    private static var departmentName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DepartmentName")

    private static var buildingID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BuildingID")

    private static var floorID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FloorID")

    private static var roomID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RoomID")

    private static var inhouseMailID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InhouseMailID")

    private static var correspondenceShortName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CorrespondenceShortName")

    private static var countryCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CountryCode")

    private static var regionCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RegionCode")

    private static var regionName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RegionName")

    private static var streetPostalCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "StreetPostalCode")

    private static var poBoxPostalCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxPostalCode")

    private static var companyPostalCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CompanyPostalCode")

    private static var cityName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CityName")

    private static var additionalCityName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalCityName")

    private static var districtName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DistrictName")

    private static var poBoxID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxID")

    private static var poBoxIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxIndicator")

    private static var poBoxCountryCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxCountryCode")

    private static var poBoxRegionCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxRegionCode")

    private static var poBoxRegionName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxRegionName")

    private static var poBoxCityName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxCityName")

    private static var streetName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "StreetName")

    private static var streetPrefixName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "StreetPrefixName")

    private static var streetSuffixName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "StreetSuffixName")

    private static var houseID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "HouseID")

    private static var additionalHouseID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalHouseID")

    private static var careOfName_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CareOfName")

    private static var taxJurisdictionCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "TaxJurisdictionCode")

    private static var timeZoneDifferenceValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "TimeZoneDifferenceValue")

    private static var correspondenceLanguageCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CorrespondenceLanguageCode")

    private static var areaID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AreaID")

    private static var subscriberID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SubscriberID")

    private static var extensionID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ExtensionID")

    private static var countryDiallingCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CountryDiallingCode")

    private static var smsEnabledIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SMSEnabledIndicator")

    private static var numberDefaultIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NumberDefaultIndicator")

    private static var numberUsageDenialIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NumberUsageDenialIndicator")

    private static var uri_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "URI")

    private static var uriDefaultIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "URIDefaultIndicator")

    private static var uriUsageDenialIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "URIUsageDenialIndicator")

    private static var addressGroupCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AddressGroupCode")

    private static var buyerID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BuyerID")

    private static var sellerID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SellerID")

    private static var revisionID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RevisionID")

    private static var inventoryValuationTypeCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InventoryValuationTypeCode")

    private static var serviceProductSpecificationTextCatalogueID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ServiceProductSpecificationTextCatalogueID")

    private static var serviceProductSpecificationTextCatalogueItemID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ServiceProductSpecificationTextCatalogueItemID")

    private static var alternateAllowedIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AlternateAllowedIndicator")

    private static var byBidderProvidedIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ByBidderProvidedIndicator")

    private static var supplementaryIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SupplementaryIndicator")

    private static var priceChangeAllowedIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "PriceChangeAllowedIndicator")

    private static var compensationComponentPayrollCategoryCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CompensationComponentPayrollCategoryCode")

    private static var functionCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FunctionCode")

    private static var firstInputParameterValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FirstInputParameterValue")

    private static var secondInputParameterValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SecondInputParameterValue")

    private static var thirdInputParameterValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ThirdInputParameterValue")

    private static var fourthInputParameterValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FourthInputParameterValue")

    private static var fifthInputParameterValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FifthInputParameterValue")

    private static var startDate_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "StartDate")

    private static var deliveryPriorityCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DeliveryPriorityCode")

    private static var overPercent_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "OverPercent")

    private static var overPercentUnlimitedIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "OverPercentUnlimitedIndicator")

    private static var underPercent_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "UnderPercent")

    private static var transportServiceLevelCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "TransportServiceLevelCode")

    private static var productTaxationCharacteristicsCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProductTaxationCharacteristicsCode")

    private static var amountUnlimitedIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AmountUnlimitedIndicator")

    private static var costElementID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CostElementID")

    private static var serviceProductHierarchyItemSpecificationID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ServiceProductHierarchyItemSpecificationID")

    private static var purchasingContractID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "PurchasingContractID")

    private static var purchasingContractItemID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "PurchasingContractItemID")

    private static var ordinalNumberValue_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "OrdinalNumberValue")

    private static var profitCentreID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProfitCentreID")

    private static var costCentreID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CostCentreID")

    private static var projectElementID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProjectElementID")

    private static var projectElementTypeCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProjectElementTypeCode")

    private static var fundsManagementCentreID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FundsManagementCentreID")

    private static var internalOrderID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InternalOrderID")

    private static var salesOrderID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SalesOrderID")

    private static var salesOrderItemID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SalesOrderItemID")

    private static var productionOrderID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProductionOrderID")

    private static var maintenanceOrderID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MaintenanceOrderID")

    private static var fundsManagementFundID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FundsManagementFundID")

    private static var fundsManagementAccountID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FundsManagementAccountID")

    private static var grantID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "GrantID")

    private static var accountingCodingBlockTypeCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AccountingCodingBlockTypeCode")

    private static var accountDeterminationExpenseGroupCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AccountDeterminationExpenseGroupCode")

    private static var accountingBusinessAreaCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AccountingBusinessAreaCode")

    private static var masterFixedAssetID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MasterFixedAssetID")

    private static var fixedAssetID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FixedAssetID")

    private static var quoteID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "QuoteID")

    private static var quoteItemID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "QuoteItemID")

    private static var purchaseRequestID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "PurchaseRequestID")

    private static var purchaseRequestItemID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "PurchaseRequestItemID")

    private static var deliveryDateTime_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DeliveryDateTime")

    private static var deliveryYearWeek_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DeliveryYearWeek")

    private static var deliveryYearMonth_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DeliveryYearMonth")

    private static var productBuyerID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProductBuyerID")

    private static var batchID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BatchID")

    private static var requirementDate_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RequirementDate")

    private static var quantityFixedIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "QuantityFixedIndicator")

    private static var followUpProcessesRelevanceIndicator_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FollowUpProcessesRelevanceIndicator")

    private static var businessDocumentProcessingResultCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BusinessDocumentProcessingResultCode")

    private static var maximumLogItemSeverityCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MaximumLogItemSeverityCode")

    private static var typeID_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "TypeID")

    private static var categoryCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CategoryCode")

    private static var severityCode_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SeverityCode")

    private static var note_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Note")

    private static var webURI_: Property = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "WebURI")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1)
    }

    open class var academicTitleCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.academicTitleCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.academicTitleCode_ = value
            }
        }
    }

    open var academicTitleCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.academicTitleCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.academicTitleCode, to: StringValue.of(optional: value))
        }
    }

    open class var academicTitleName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.academicTitleName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.academicTitleName_ = value
            }
        }
    }

    open var academicTitleName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.academicTitleName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.academicTitleName, to: StringValue.of(optional: value))
        }
    }

    open class var accountDeterminationExpenseGroupCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.accountDeterminationExpenseGroupCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.accountDeterminationExpenseGroupCode_ = value
            }
        }
    }

    open var accountDeterminationExpenseGroupCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.accountDeterminationExpenseGroupCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.accountDeterminationExpenseGroupCode, to: StringValue.of(optional: value))
        }
    }

    open class var accountingBusinessAreaCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.accountingBusinessAreaCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.accountingBusinessAreaCode_ = value
            }
        }
    }

    open var accountingBusinessAreaCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.accountingBusinessAreaCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.accountingBusinessAreaCode, to: StringValue.of(optional: value))
        }
    }

    open class var accountingCodingBlockTypeCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.accountingCodingBlockTypeCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.accountingCodingBlockTypeCode_ = value
            }
        }
    }

    open var accountingCodingBlockTypeCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.accountingCodingBlockTypeCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.accountingCodingBlockTypeCode, to: StringValue.of(optional: value))
        }
    }

    open class var additionalAcademicTitleCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.additionalAcademicTitleCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.additionalAcademicTitleCode_ = value
            }
        }
    }

    open var additionalAcademicTitleCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalAcademicTitleCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalAcademicTitleCode, to: StringValue.of(optional: value))
        }
    }

    open class var additionalAcademicTitleName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.additionalAcademicTitleName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.additionalAcademicTitleName_ = value
            }
        }
    }

    open var additionalAcademicTitleName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalAcademicTitleName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalAcademicTitleName, to: StringValue.of(optional: value))
        }
    }

    open class var additionalCityName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.additionalCityName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.additionalCityName_ = value
            }
        }
    }

    open var additionalCityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalCityName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalCityName, to: StringValue.of(optional: value))
        }
    }

    open class var additionalFamilyName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.additionalFamilyName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.additionalFamilyName_ = value
            }
        }
    }

    open var additionalFamilyName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalFamilyName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalFamilyName, to: StringValue.of(optional: value))
        }
    }

    open class var additionalHouseID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.additionalHouseID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.additionalHouseID_ = value
            }
        }
    }

    open var additionalHouseID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalHouseID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalHouseID, to: StringValue.of(optional: value))
        }
    }

    open class var additionalNamePrefixCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.additionalNamePrefixCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.additionalNamePrefixCode_ = value
            }
        }
    }

    open var additionalNamePrefixCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalNamePrefixCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalNamePrefixCode, to: StringValue.of(optional: value))
        }
    }

    open class var additionalNamePrefixName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.additionalNamePrefixName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.additionalNamePrefixName_ = value
            }
        }
    }

    open var additionalNamePrefixName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalNamePrefixName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.additionalNamePrefixName, to: StringValue.of(optional: value))
        }
    }

    open class var addressGroupCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.addressGroupCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.addressGroupCode_ = value
            }
        }
    }

    open var addressGroupCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.addressGroupCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.addressGroupCode, to: StringValue.of(optional: value))
        }
    }

    open class var addressID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.addressID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.addressID_ = value
            }
        }
    }

    open var addressID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.addressID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.addressID, to: StringValue.of(optional: value))
        }
    }

    open class var alternateAllowedIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.alternateAllowedIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.alternateAllowedIndicator_ = value
            }
        }
    }

    open var alternateAllowedIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.alternateAllowedIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.alternateAllowedIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var amountUnlimitedIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.amountUnlimitedIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.amountUnlimitedIndicator_ = value
            }
        }
    }

    open var amountUnlimitedIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.amountUnlimitedIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.amountUnlimitedIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var areaID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.areaID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.areaID_ = value
            }
        }
    }

    open var areaID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.areaID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.areaID, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [PurchaseOrderERPCreateRequestConfirmationInV1] {
        return ArrayConverter.convert(from.toArray(), [PurchaseOrderERPCreateRequestConfirmationInV1]())
    }

    open class var baseCurrencyCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.baseCurrencyCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.baseCurrencyCode_ = value
            }
        }
    }

    open var baseCurrencyCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.baseCurrencyCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.baseCurrencyCode, to: StringValue.of(optional: value))
        }
    }

    open class var baseDecimalValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.baseDecimalValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.baseDecimalValue_ = value
            }
        }
    }

    open var baseDecimalValue: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.baseDecimalValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.baseDecimalValue, to: IntegerValue.of(optional: value))
        }
    }

    open class var baseMeasureUnitCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.baseMeasureUnitCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.baseMeasureUnitCode_ = value
            }
        }
    }

    open var baseMeasureUnitCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.baseMeasureUnitCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.baseMeasureUnitCode, to: StringValue.of(optional: value))
        }
    }

    open class var baseQuantityTypeCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.baseQuantityTypeCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.baseQuantityTypeCode_ = value
            }
        }
    }

    open var baseQuantityTypeCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.baseQuantityTypeCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.baseQuantityTypeCode, to: StringValue.of(optional: value))
        }
    }

    open class var batchID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.batchID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.batchID_ = value
            }
        }
    }

    open var batchID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.batchID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.batchID, to: StringValue.of(optional: value))
        }
    }

    open class var birthName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.birthName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.birthName_ = value
            }
        }
    }

    open var birthName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.birthName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.birthName, to: StringValue.of(optional: value))
        }
    }

    open class var blockedIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.blockedIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.blockedIndicator_ = value
            }
        }
    }

    open var blockedIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.blockedIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.blockedIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var buildingID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.buildingID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.buildingID_ = value
            }
        }
    }

    open var buildingID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.buildingID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.buildingID, to: StringValue.of(optional: value))
        }
    }

    open class var businessDocumentProcessingResultCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.businessDocumentProcessingResultCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.businessDocumentProcessingResultCode_ = value
            }
        }
    }

    open var businessDocumentProcessingResultCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.businessDocumentProcessingResultCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.businessDocumentProcessingResultCode, to: StringValue.of(optional: value))
        }
    }

    open class var buyerID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.buyerID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.buyerID_ = value
            }
        }
    }

    open var buyerID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.buyerID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.buyerID, to: StringValue.of(optional: value))
        }
    }

    open class var buyerReferenceNote: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.buyerReferenceNote_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.buyerReferenceNote_ = value
            }
        }
    }

    open var buyerReferenceNote: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.buyerReferenceNote))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.buyerReferenceNote, to: StringValue.of(optional: value))
        }
    }

    open class var byBidderProvidedIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.byBidderProvidedIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.byBidderProvidedIndicator_ = value
            }
        }
    }

    open var byBidderProvidedIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.byBidderProvidedIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.byBidderProvidedIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var careOfName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.careOfName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.careOfName_ = value
            }
        }
    }

    open var careOfName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.careOfName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.careOfName, to: StringValue.of(optional: value))
        }
    }

    open class var categoryCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.categoryCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.categoryCode_ = value
            }
        }
    }

    open var categoryCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.categoryCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.categoryCode, to: StringValue.of(optional: value))
        }
    }

    open class var cityName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.cityName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.cityName_ = value
            }
        }
    }

    open var cityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.cityName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.cityName, to: StringValue.of(optional: value))
        }
    }

    open class var classificationCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.classificationCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.classificationCode_ = value
            }
        }
    }

    open var classificationCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.classificationCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.classificationCode, to: StringValue.of(optional: value))
        }
    }

    open class var code: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.code_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.code_ = value
            }
        }
    }

    open var code: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.code))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.code, to: StringValue.of(optional: value))
        }
    }

    open class var companyPostalCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.companyPostalCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.companyPostalCode_ = value
            }
        }
    }

    open var companyPostalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.companyPostalCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.companyPostalCode, to: StringValue.of(optional: value))
        }
    }

    open class var compensationComponentPayrollCategoryCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.compensationComponentPayrollCategoryCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.compensationComponentPayrollCategoryCode_ = value
            }
        }
    }

    open var compensationComponentPayrollCategoryCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.compensationComponentPayrollCategoryCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.compensationComponentPayrollCategoryCode, to: StringValue.of(optional: value))
        }
    }

    open class var completenessAndValidationStatusCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.completenessAndValidationStatusCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.completenessAndValidationStatusCode_ = value
            }
        }
    }

    open var completenessAndValidationStatusCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.completenessAndValidationStatusCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.completenessAndValidationStatusCode, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> PurchaseOrderERPCreateRequestConfirmationInV1 {
        return CastRequired<PurchaseOrderERPCreateRequestConfirmationInV1>.from(self.copyEntity())
    }

    open class var correspondenceLanguageCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.correspondenceLanguageCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.correspondenceLanguageCode_ = value
            }
        }
    }

    open var correspondenceLanguageCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.correspondenceLanguageCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.correspondenceLanguageCode, to: StringValue.of(optional: value))
        }
    }

    open class var correspondenceShortName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.correspondenceShortName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.correspondenceShortName_ = value
            }
        }
    }

    open var correspondenceShortName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.correspondenceShortName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.correspondenceShortName, to: StringValue.of(optional: value))
        }
    }

    open class var costCentreID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.costCentreID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.costCentreID_ = value
            }
        }
    }

    open var costCentreID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.costCentreID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.costCentreID, to: StringValue.of(optional: value))
        }
    }

    open class var costElementID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.costElementID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.costElementID_ = value
            }
        }
    }

    open var costElementID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.costElementID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.costElementID, to: StringValue.of(optional: value))
        }
    }

    open class var countryCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.countryCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.countryCode_ = value
            }
        }
    }

    open var countryCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.countryCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.countryCode, to: StringValue.of(optional: value))
        }
    }

    open class var countryDiallingCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.countryDiallingCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.countryDiallingCode_ = value
            }
        }
    }

    open var countryDiallingCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.countryDiallingCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.countryDiallingCode, to: StringValue.of(optional: value))
        }
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.creationDate, to: value)
        }
    }

    open class var creationUserAccountID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.creationUserAccountID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.creationUserAccountID_ = value
            }
        }
    }

    open var creationUserAccountID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.creationUserAccountID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.creationUserAccountID, to: StringValue.of(optional: value))
        }
    }

    open class var currencyCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.currencyCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.currencyCode_ = value
            }
        }
    }

    open var currencyCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.currencyCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.currencyCode, to: StringValue.of(optional: value))
        }
    }

    open class var currencyCode2: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.currencyCode2_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.currencyCode2_ = value
            }
        }
    }

    open var currencyCode2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.currencyCode2))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.currencyCode2, to: StringValue.of(optional: value))
        }
    }

    open class var date: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.date_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.date_ = value
            }
        }
    }

    open var date: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.date))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.date, to: value)
        }
    }

    open class var dayOfMonthValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.dayOfMonthValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.dayOfMonthValue_ = value
            }
        }
    }

    open var dayOfMonthValue: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.dayOfMonthValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.dayOfMonthValue, to: IntValue.of(optional: value))
        }
    }

    open class var daysValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.daysValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.daysValue_ = value
            }
        }
    }

    open var daysValue: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.daysValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.daysValue, to: IntValue.of(optional: value))
        }
    }

    open class var decimalValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.decimalValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.decimalValue_ = value
            }
        }
    }

    open var decimalValue: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.decimalValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.decimalValue, to: IntegerValue.of(optional: value))
        }
    }

    open class var deliveryBasedInvoiceVerificationIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.deliveryBasedInvoiceVerificationIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.deliveryBasedInvoiceVerificationIndicator_ = value
            }
        }
    }

    open var deliveryBasedInvoiceVerificationIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.deliveryBasedInvoiceVerificationIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.deliveryBasedInvoiceVerificationIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var deliveryDateTime: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.deliveryDateTime_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.deliveryDateTime_ = value
            }
        }
    }

    open var deliveryDateTime: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.deliveryDateTime))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.deliveryDateTime, to: value)
        }
    }

    open class var deliveryPriorityCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.deliveryPriorityCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.deliveryPriorityCode_ = value
            }
        }
    }

    open var deliveryPriorityCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.deliveryPriorityCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.deliveryPriorityCode, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryYearMonth: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.deliveryYearMonth_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.deliveryYearMonth_ = value
            }
        }
    }

    open var deliveryYearMonth: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.deliveryYearMonth))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.deliveryYearMonth, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryYearWeek: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.deliveryYearWeek_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.deliveryYearWeek_ = value
            }
        }
    }

    open var deliveryYearWeek: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.deliveryYearWeek))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.deliveryYearWeek, to: StringValue.of(optional: value))
        }
    }

    open class var departmentName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.departmentName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.departmentName_ = value
            }
        }
    }

    open var departmentName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.departmentName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.departmentName, to: StringValue.of(optional: value))
        }
    }

    open class var description: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.description_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.description_ = value
            }
        }
    }

    open var description: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.description))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.description, to: StringValue.of(optional: value))
        }
    }

    open class var deviatingFullName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.deviatingFullName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.deviatingFullName_ = value
            }
        }
    }

    open var deviatingFullName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.deviatingFullName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.deviatingFullName, to: StringValue.of(optional: value))
        }
    }

    open class var districtName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.districtName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.districtName_ = value
            }
        }
    }

    open var districtName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.districtName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.districtName, to: StringValue.of(optional: value))
        }
    }

    open class var endDate: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.endDate_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.endDate_ = value
            }
        }
    }

    open var endDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.endDate))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.endDate, to: value)
        }
    }

    open class var evaluatedReceiptSettlementIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.evaluatedReceiptSettlementIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.evaluatedReceiptSettlementIndicator_ = value
            }
        }
    }

    open var evaluatedReceiptSettlementIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.evaluatedReceiptSettlementIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.evaluatedReceiptSettlementIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var exchangeRateFixedIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.exchangeRateFixedIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.exchangeRateFixedIndicator_ = value
            }
        }
    }

    open var exchangeRateFixedIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.exchangeRateFixedIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.exchangeRateFixedIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var expectedPurchasingDocumentItemConfirmationTypeCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.expectedPurchasingDocumentItemConfirmationTypeCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.expectedPurchasingDocumentItemConfirmationTypeCode_ = value
            }
        }
    }

    open var expectedPurchasingDocumentItemConfirmationTypeCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.expectedPurchasingDocumentItemConfirmationTypeCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.expectedPurchasingDocumentItemConfirmationTypeCode, to: StringValue.of(optional: value))
        }
    }

    open class var extensionID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.extensionID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.extensionID_ = value
            }
        }
    }

    open var extensionID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.extensionID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.extensionID, to: StringValue.of(optional: value))
        }
    }

    open class var familyName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.familyName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.familyName_ = value
            }
        }
    }

    open var familyName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.familyName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.familyName, to: StringValue.of(optional: value))
        }
    }

    open class var fifthInputParameterValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.fifthInputParameterValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.fifthInputParameterValue_ = value
            }
        }
    }

    open var fifthInputParameterValue: Double? {
        get {
            return DoubleValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fifthInputParameterValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fifthInputParameterValue, to: DoubleValue.of(optional: value))
        }
    }

    open class var firstInputParameterValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.firstInputParameterValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.firstInputParameterValue_ = value
            }
        }
    }

    open var firstInputParameterValue: Double? {
        get {
            return DoubleValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.firstInputParameterValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.firstInputParameterValue, to: DoubleValue.of(optional: value))
        }
    }

    open class var fixedAssetID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.fixedAssetID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.fixedAssetID_ = value
            }
        }
    }

    open var fixedAssetID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fixedAssetID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fixedAssetID, to: StringValue.of(optional: value))
        }
    }

    open class var floorID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.floorID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.floorID_ = value
            }
        }
    }

    open var floorID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.floorID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.floorID, to: StringValue.of(optional: value))
        }
    }

    open class var followUpProcessesRelevanceIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.followUpProcessesRelevanceIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.followUpProcessesRelevanceIndicator_ = value
            }
        }
    }

    open var followUpProcessesRelevanceIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.followUpProcessesRelevanceIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.followUpProcessesRelevanceIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var formOfAddressCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.formOfAddressCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.formOfAddressCode_ = value
            }
        }
    }

    open var formOfAddressCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.formOfAddressCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.formOfAddressCode, to: StringValue.of(optional: value))
        }
    }

    open class var formOfAddressName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.formOfAddressName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.formOfAddressName_ = value
            }
        }
    }

    open var formOfAddressName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.formOfAddressName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.formOfAddressName, to: StringValue.of(optional: value))
        }
    }

    open class var formattedName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.formattedName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.formattedName_ = value
            }
        }
    }

    open var formattedName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.formattedName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.formattedName, to: StringValue.of(optional: value))
        }
    }

    open class var fourthInputParameterValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.fourthInputParameterValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.fourthInputParameterValue_ = value
            }
        }
    }

    open var fourthInputParameterValue: Double? {
        get {
            return DoubleValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fourthInputParameterValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fourthInputParameterValue, to: DoubleValue.of(optional: value))
        }
    }

    open class var fullPaymentDueDaysValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.fullPaymentDueDaysValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.fullPaymentDueDaysValue_ = value
            }
        }
    }

    open var fullPaymentDueDaysValue: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fullPaymentDueDaysValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fullPaymentDueDaysValue, to: IntValue.of(optional: value))
        }
    }

    open class var functionCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.functionCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.functionCode_ = value
            }
        }
    }

    open var functionCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.functionCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.functionCode, to: StringValue.of(optional: value))
        }
    }

    open class var functionalTitleName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.functionalTitleName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.functionalTitleName_ = value
            }
        }
    }

    open var functionalTitleName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.functionalTitleName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.functionalTitleName, to: StringValue.of(optional: value))
        }
    }

    open class var fundsManagementAccountID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementAccountID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementAccountID_ = value
            }
        }
    }

    open var fundsManagementAccountID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementAccountID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementAccountID, to: StringValue.of(optional: value))
        }
    }

    open class var fundsManagementCentreID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementCentreID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementCentreID_ = value
            }
        }
    }

    open var fundsManagementCentreID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementCentreID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementCentreID, to: StringValue.of(optional: value))
        }
    }

    open class var fundsManagementFundID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementFundID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementFundID_ = value
            }
        }
    }

    open var fundsManagementFundID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementFundID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementFundID, to: StringValue.of(optional: value))
        }
    }

    open class var givenName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.givenName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.givenName_ = value
            }
        }
    }

    open var givenName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.givenName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.givenName, to: StringValue.of(optional: value))
        }
    }

    open class var grantID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.grantID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.grantID_ = value
            }
        }
    }

    open var grantID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.grantID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.grantID, to: StringValue.of(optional: value))
        }
    }

    open class var groupID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.groupID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.groupID_ = value
            }
        }
    }

    open var groupID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.groupID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.groupID, to: StringValue.of(optional: value))
        }
    }

    open class var houseID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.houseID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.houseID_ = value
            }
        }
    }

    open var houseID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.houseID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.houseID, to: StringValue.of(optional: value))
        }
    }

    open class var id: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.id_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.id_ = value
            }
        }
    }

    open var id: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.id))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.id, to: StringValue.of(optional: value))
        }
    }

    open class var inboundDeliveryCompletedIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.inboundDeliveryCompletedIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.inboundDeliveryCompletedIndicator_ = value
            }
        }
    }

    open var inboundDeliveryCompletedIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.inboundDeliveryCompletedIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.inboundDeliveryCompletedIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var inhouseMailID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.inhouseMailID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.inhouseMailID_ = value
            }
        }
    }

    open var inhouseMailID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.inhouseMailID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.inhouseMailID, to: StringValue.of(optional: value))
        }
    }

    open class var initialsName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.initialsName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.initialsName_ = value
            }
        }
    }

    open var initialsName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.initialsName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.initialsName, to: StringValue.of(optional: value))
        }
    }

    open class var internalID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.internalID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.internalID_ = value
            }
        }
    }

    open var internalID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.internalID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.internalID, to: StringValue.of(optional: value))
        }
    }

    open class var internalOrderID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.internalOrderID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.internalOrderID_ = value
            }
        }
    }

    open var internalOrderID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.internalOrderID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.internalOrderID, to: StringValue.of(optional: value))
        }
    }

    open class var inventoryValuationTypeCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.inventoryValuationTypeCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.inventoryValuationTypeCode_ = value
            }
        }
    }

    open var inventoryValuationTypeCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.inventoryValuationTypeCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.inventoryValuationTypeCode, to: StringValue.of(optional: value))
        }
    }

    open class var invoiceVerificationCompletedIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.invoiceVerificationCompletedIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.invoiceVerificationCompletedIndicator_ = value
            }
        }
    }

    open var invoiceVerificationCompletedIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.invoiceVerificationCompletedIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.invoiceVerificationCompletedIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var item: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.item_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.item_ = value
            }
        }
    }

    open var item: Item2? {
        get {
            return CastOptional<Item2>.from(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.item))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.item, to: value)
        }
    }

    open class func key(id: String?) -> EntityKey {
        return EntityKey().with(name: "ID", value: StringValue.of(optional: id))
    }

    open class var languageCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.languageCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.languageCode_ = value
            }
        }
    }

    open var languageCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.languageCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.languageCode, to: StringValue.of(optional: value))
        }
    }

    open class var maintenanceOrderID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.maintenanceOrderID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.maintenanceOrderID_ = value
            }
        }
    }

    open var maintenanceOrderID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.maintenanceOrderID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.maintenanceOrderID, to: StringValue.of(optional: value))
        }
    }

    open class var majorLevelOrdinalNumberValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.majorLevelOrdinalNumberValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.majorLevelOrdinalNumberValue_ = value
            }
        }
    }

    open var majorLevelOrdinalNumberValue: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.majorLevelOrdinalNumberValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.majorLevelOrdinalNumberValue, to: IntValue.of(optional: value))
        }
    }

    open class var masterFixedAssetID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.masterFixedAssetID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.masterFixedAssetID_ = value
            }
        }
    }

    open var masterFixedAssetID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.masterFixedAssetID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.masterFixedAssetID, to: StringValue.of(optional: value))
        }
    }

    open class var maximumLogItemSeverityCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.maximumLogItemSeverityCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.maximumLogItemSeverityCode_ = value
            }
        }
    }

    open var maximumLogItemSeverityCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.maximumLogItemSeverityCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.maximumLogItemSeverityCode, to: StringValue.of(optional: value))
        }
    }

    open class var measureUnitCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.measureUnitCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.measureUnitCode_ = value
            }
        }
    }

    open var measureUnitCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.measureUnitCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.measureUnitCode, to: StringValue.of(optional: value))
        }
    }

    open class var middleName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.middleName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.middleName_ = value
            }
        }
    }

    open var middleName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.middleName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.middleName, to: StringValue.of(optional: value))
        }
    }

    open class var minorLevelOrdinalNumberValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.minorLevelOrdinalNumberValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.minorLevelOrdinalNumberValue_ = value
            }
        }
    }

    open var minorLevelOrdinalNumberValue: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.minorLevelOrdinalNumberValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.minorLevelOrdinalNumberValue, to: IntValue.of(optional: value))
        }
    }

    open class var monthOffsetValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.monthOffsetValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.monthOffsetValue_ = value
            }
        }
    }

    open var monthOffsetValue: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.monthOffsetValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.monthOffsetValue, to: IntValue.of(optional: value))
        }
    }

    open class var nameFormatCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.nameFormatCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.nameFormatCode_ = value
            }
        }
    }

    open var nameFormatCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.nameFormatCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.nameFormatCode, to: StringValue.of(optional: value))
        }
    }

    open class var nameFormatCountryCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.nameFormatCountryCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.nameFormatCountryCode_ = value
            }
        }
    }

    open var nameFormatCountryCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.nameFormatCountryCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.nameFormatCountryCode, to: StringValue.of(optional: value))
        }
    }

    open class var namePrefixCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.namePrefixCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.namePrefixCode_ = value
            }
        }
    }

    open var namePrefixCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.namePrefixCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.namePrefixCode, to: StringValue.of(optional: value))
        }
    }

    open class var namePrefixName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.namePrefixName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.namePrefixName_ = value
            }
        }
    }

    open var namePrefixName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.namePrefixName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.namePrefixName, to: StringValue.of(optional: value))
        }
    }

    open class var nameSupplementCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.nameSupplementCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.nameSupplementCode_ = value
            }
        }
    }

    open var nameSupplementCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.nameSupplementCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.nameSupplementCode, to: StringValue.of(optional: value))
        }
    }

    open class var nameSupplementName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.nameSupplementName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.nameSupplementName_ = value
            }
        }
    }

    open var nameSupplementName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.nameSupplementName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.nameSupplementName, to: StringValue.of(optional: value))
        }
    }

    open class var nickName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.nickName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.nickName_ = value
            }
        }
    }

    open var nickName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.nickName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.nickName, to: StringValue.of(optional: value))
        }
    }

    open class var note: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.note_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.note_ = value
            }
        }
    }

    open var note: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.note))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.note, to: StringValue.of(optional: value))
        }
    }

    open class var numberDefaultIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.numberDefaultIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.numberDefaultIndicator_ = value
            }
        }
    }

    open var numberDefaultIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.numberDefaultIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.numberDefaultIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var numberUsageDenialIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.numberUsageDenialIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.numberUsageDenialIndicator_ = value
            }
        }
    }

    open var numberUsageDenialIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.numberUsageDenialIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.numberUsageDenialIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open var old: PurchaseOrderERPCreateRequestConfirmationInV1 {
        return CastRequired<PurchaseOrderERPCreateRequestConfirmationInV1>.from(self.oldEntity)
    }

    open class var ordinalNumberValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.ordinalNumberValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.ordinalNumberValue_ = value
            }
        }
    }

    open var ordinalNumberValue: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.ordinalNumberValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.ordinalNumberValue, to: IntValue.of(optional: value))
        }
    }

    open class var organisationFormattedName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.organisationFormattedName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.organisationFormattedName_ = value
            }
        }
    }

    open var organisationFormattedName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.organisationFormattedName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.organisationFormattedName, to: StringValue.of(optional: value))
        }
    }

    open class var outboundDeliveryCompletedIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.outboundDeliveryCompletedIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.outboundDeliveryCompletedIndicator_ = value
            }
        }
    }

    open var outboundDeliveryCompletedIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.outboundDeliveryCompletedIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.outboundDeliveryCompletedIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var overPercent: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.overPercent_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.overPercent_ = value
            }
        }
    }

    open var overPercent: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.overPercent))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.overPercent, to: IntegerValue.of(optional: value))
        }
    }

    open class var overPercentUnlimitedIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.overPercentUnlimitedIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.overPercentUnlimitedIndicator_ = value
            }
        }
    }

    open var overPercentUnlimitedIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.overPercentUnlimitedIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.overPercentUnlimitedIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var parentItemID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.parentItemID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.parentItemID_ = value
            }
        }
    }

    open var parentItemID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.parentItemID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.parentItemID, to: StringValue.of(optional: value))
        }
    }

    open class var percent: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.percent_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.percent_ = value
            }
        }
    }

    open var percent: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.percent))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.percent, to: IntegerValue.of(optional: value))
        }
    }

    open class var poBoxCityName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.poBoxCityName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.poBoxCityName_ = value
            }
        }
    }

    open var poBoxCityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxCityName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxCityName, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxCountryCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.poBoxCountryCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.poBoxCountryCode_ = value
            }
        }
    }

    open var poBoxCountryCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxCountryCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxCountryCode, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.poBoxID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.poBoxID_ = value
            }
        }
    }

    open var poBoxID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxID, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.poBoxIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.poBoxIndicator_ = value
            }
        }
    }

    open var poBoxIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var poBoxPostalCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.poBoxPostalCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.poBoxPostalCode_ = value
            }
        }
    }

    open var poBoxPostalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxPostalCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxPostalCode, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxRegionCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.poBoxRegionCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.poBoxRegionCode_ = value
            }
        }
    }

    open var poBoxRegionCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxRegionCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxRegionCode, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxRegionName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.poBoxRegionName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.poBoxRegionName_ = value
            }
        }
    }

    open var poBoxRegionName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxRegionName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.poBoxRegionName, to: StringValue.of(optional: value))
        }
    }

    open class var priceChangeAllowedIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.priceChangeAllowedIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.priceChangeAllowedIndicator_ = value
            }
        }
    }

    open var priceChangeAllowedIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.priceChangeAllowedIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.priceChangeAllowedIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var processingTypeCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.processingTypeCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.processingTypeCode_ = value
            }
        }
    }

    open var processingTypeCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.processingTypeCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.processingTypeCode, to: StringValue.of(optional: value))
        }
    }

    open class var productBuyerID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.productBuyerID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.productBuyerID_ = value
            }
        }
    }

    open var productBuyerID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.productBuyerID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.productBuyerID, to: StringValue.of(optional: value))
        }
    }

    open class var productProcurementArrangmentID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.productProcurementArrangmentID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.productProcurementArrangmentID_ = value
            }
        }
    }

    open var productProcurementArrangmentID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.productProcurementArrangmentID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.productProcurementArrangmentID, to: StringValue.of(optional: value))
        }
    }

    open class var productTaxationCharacteristicsCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.productTaxationCharacteristicsCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.productTaxationCharacteristicsCode_ = value
            }
        }
    }

    open var productTaxationCharacteristicsCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.productTaxationCharacteristicsCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.productTaxationCharacteristicsCode, to: StringValue.of(optional: value))
        }
    }

    open class var productionOrderID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.productionOrderID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.productionOrderID_ = value
            }
        }
    }

    open var productionOrderID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.productionOrderID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.productionOrderID, to: StringValue.of(optional: value))
        }
    }

    open class var profitCentreID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.profitCentreID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.profitCentreID_ = value
            }
        }
    }

    open var profitCentreID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.profitCentreID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.profitCentreID, to: StringValue.of(optional: value))
        }
    }

    open class var projectElementID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.projectElementID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.projectElementID_ = value
            }
        }
    }

    open var projectElementID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.projectElementID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.projectElementID, to: StringValue.of(optional: value))
        }
    }

    open class var projectElementTypeCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.projectElementTypeCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.projectElementTypeCode_ = value
            }
        }
    }

    open var projectElementTypeCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.projectElementTypeCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.projectElementTypeCode, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseRequestID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.purchaseRequestID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.purchaseRequestID_ = value
            }
        }
    }

    open var purchaseRequestID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.purchaseRequestID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.purchaseRequestID, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseRequestItemID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.purchaseRequestItemID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.purchaseRequestItemID_ = value
            }
        }
    }

    open var purchaseRequestItemID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.purchaseRequestItemID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.purchaseRequestItemID, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingContractID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.purchasingContractID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.purchasingContractID_ = value
            }
        }
    }

    open var purchasingContractID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.purchasingContractID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.purchasingContractID, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingContractItemID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.purchasingContractItemID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.purchasingContractItemID_ = value
            }
        }
    }

    open var purchasingContractItemID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.purchasingContractItemID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.purchasingContractItemID, to: StringValue.of(optional: value))
        }
    }

    open class var quantityFixedIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.quantityFixedIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.quantityFixedIndicator_ = value
            }
        }
    }

    open var quantityFixedIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.quantityFixedIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.quantityFixedIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var quotationDateTime: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.quotationDateTime_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.quotationDateTime_ = value
            }
        }
    }

    open var quotationDateTime: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.quotationDateTime))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.quotationDateTime, to: value)
        }
    }

    open class var quoteID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.quoteID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.quoteID_ = value
            }
        }
    }

    open var quoteID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.quoteID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.quoteID, to: StringValue.of(optional: value))
        }
    }

    open class var quoteItemID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.quoteItemID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.quoteItemID_ = value
            }
        }
    }

    open var quoteItemID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.quoteItemID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.quoteItemID, to: StringValue.of(optional: value))
        }
    }

    open class var quotedCurrency: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.quotedCurrency_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.quotedCurrency_ = value
            }
        }
    }

    open var quotedCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.quotedCurrency))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.quotedCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var rate: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.rate_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.rate_ = value
            }
        }
    }

    open var rate: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.rate))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.rate, to: IntegerValue.of(optional: value))
        }
    }

    open class var receivingPlantID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.receivingPlantID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.receivingPlantID_ = value
            }
        }
    }

    open var receivingPlantID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.receivingPlantID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.receivingPlantID, to: StringValue.of(optional: value))
        }
    }

    open class var referenceID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.referenceID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.referenceID_ = value
            }
        }
    }

    open var referenceID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.referenceID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.referenceID, to: StringValue.of(optional: value))
        }
    }

    open class var referenceUUID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.referenceUUID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.referenceUUID_ = value
            }
        }
    }

    open var referenceUUID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.referenceUUID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.referenceUUID, to: StringValue.of(optional: value))
        }
    }

    open class var regionCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.regionCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.regionCode_ = value
            }
        }
    }

    open var regionCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.regionCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.regionCode, to: StringValue.of(optional: value))
        }
    }

    open class var regionName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.regionName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.regionName_ = value
            }
        }
    }

    open var regionName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.regionName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.regionName, to: StringValue.of(optional: value))
        }
    }

    open class var releaseStatusCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.releaseStatusCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.releaseStatusCode_ = value
            }
        }
    }

    open var releaseStatusCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.releaseStatusCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.releaseStatusCode, to: StringValue.of(optional: value))
        }
    }

    open class var requesterPersonFormattedName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.requesterPersonFormattedName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.requesterPersonFormattedName_ = value
            }
        }
    }

    open var requesterPersonFormattedName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.requesterPersonFormattedName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.requesterPersonFormattedName, to: StringValue.of(optional: value))
        }
    }

    open class var requirementCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.requirementCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.requirementCode_ = value
            }
        }
    }

    open var requirementCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.requirementCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.requirementCode, to: StringValue.of(optional: value))
        }
    }

    open class var requirementDate: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.requirementDate_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.requirementDate_ = value
            }
        }
    }

    open var requirementDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.requirementDate))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.requirementDate, to: value)
        }
    }

    open class var responsiblePurchasingGroupParty: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.responsiblePurchasingGroupParty_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.responsiblePurchasingGroupParty_ = value
            }
        }
    }

    open var responsiblePurchasingGroupParty: ResponsiblePurchasingGroupParty? {
        get {
            return CastOptional<ResponsiblePurchasingGroupParty>.from(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.responsiblePurchasingGroupParty))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.responsiblePurchasingGroupParty, to: value)
        }
    }

    open class var responsiblePurchasingOrganisationParty: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.responsiblePurchasingOrganisationParty_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.responsiblePurchasingOrganisationParty_ = value
            }
        }
    }

    open var responsiblePurchasingOrganisationParty: ResponsiblePurchasingOrganisationParty? {
        get {
            return CastOptional<ResponsiblePurchasingOrganisationParty>.from(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.responsiblePurchasingOrganisationParty))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.responsiblePurchasingOrganisationParty, to: value)
        }
    }

    open class var retailAllocationID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.retailAllocationID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.retailAllocationID_ = value
            }
        }
    }

    open var retailAllocationID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.retailAllocationID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.retailAllocationID, to: StringValue.of(optional: value))
        }
    }

    open class var retailEventID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.retailEventID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.retailEventID_ = value
            }
        }
    }

    open var retailEventID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.retailEventID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.retailEventID, to: StringValue.of(optional: value))
        }
    }

    open class var returnsIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.returnsIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.returnsIndicator_ = value
            }
        }
    }

    open var returnsIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.returnsIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.returnsIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var revisionID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.revisionID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.revisionID_ = value
            }
        }
    }

    open var revisionID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.revisionID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.revisionID, to: StringValue.of(optional: value))
        }
    }

    open class var roleCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.roleCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.roleCode_ = value
            }
        }
    }

    open var roleCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.roleCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.roleCode, to: StringValue.of(optional: value))
        }
    }

    open class var roomID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.roomID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.roomID_ = value
            }
        }
    }

    open var roomID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.roomID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.roomID, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.salesOrderID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.salesOrderID_ = value
            }
        }
    }

    open var salesOrderID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.salesOrderID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.salesOrderID, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderItemID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.salesOrderItemID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.salesOrderItemID_ = value
            }
        }
    }

    open var salesOrderItemID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.salesOrderItemID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.salesOrderItemID, to: StringValue.of(optional: value))
        }
    }

    open class var secondInputParameterValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.secondInputParameterValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.secondInputParameterValue_ = value
            }
        }
    }

    open var secondInputParameterValue: Double? {
        get {
            return DoubleValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.secondInputParameterValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.secondInputParameterValue, to: DoubleValue.of(optional: value))
        }
    }

    open class var sellerAssortmentProductGroupID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.sellerAssortmentProductGroupID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.sellerAssortmentProductGroupID_ = value
            }
        }
    }

    open var sellerAssortmentProductGroupID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.sellerAssortmentProductGroupID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.sellerAssortmentProductGroupID, to: StringValue.of(optional: value))
        }
    }

    open class var sellerID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.sellerID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.sellerID_ = value
            }
        }
    }

    open var sellerID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.sellerID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.sellerID, to: StringValue.of(optional: value))
        }
    }

    open class var sellerReferenceNote: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.sellerReferenceNote_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.sellerReferenceNote_ = value
            }
        }
    }

    open var sellerReferenceNote: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.sellerReferenceNote))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.sellerReferenceNote, to: StringValue.of(optional: value))
        }
    }

    open class var serviceProductHierarchyItemSpecificationID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductHierarchyItemSpecificationID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductHierarchyItemSpecificationID_ = value
            }
        }
    }

    open var serviceProductHierarchyItemSpecificationID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductHierarchyItemSpecificationID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductHierarchyItemSpecificationID, to: StringValue.of(optional: value))
        }
    }

    open class var serviceProductSpecificationTextCatalogueID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductSpecificationTextCatalogueID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductSpecificationTextCatalogueID_ = value
            }
        }
    }

    open var serviceProductSpecificationTextCatalogueID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductSpecificationTextCatalogueID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductSpecificationTextCatalogueID, to: StringValue.of(optional: value))
        }
    }

    open class var serviceProductSpecificationTextCatalogueItemID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductSpecificationTextCatalogueItemID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductSpecificationTextCatalogueItemID_ = value
            }
        }
    }

    open var serviceProductSpecificationTextCatalogueItemID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductSpecificationTextCatalogueItemID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductSpecificationTextCatalogueItemID, to: StringValue.of(optional: value))
        }
    }

    open class var severityCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.severityCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.severityCode_ = value
            }
        }
    }

    open var severityCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.severityCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.severityCode, to: StringValue.of(optional: value))
        }
    }

    open class var smsEnabledIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.smsEnabledIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.smsEnabledIndicator_ = value
            }
        }
    }

    open var smsEnabledIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.smsEnabledIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.smsEnabledIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var startDate: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.startDate_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.startDate_ = value
            }
        }
    }

    open var startDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.startDate))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.startDate, to: value)
        }
    }

    open class var streetName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.streetName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.streetName_ = value
            }
        }
    }

    open var streetName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.streetName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.streetName, to: StringValue.of(optional: value))
        }
    }

    open class var streetPostalCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.streetPostalCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.streetPostalCode_ = value
            }
        }
    }

    open var streetPostalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.streetPostalCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.streetPostalCode, to: StringValue.of(optional: value))
        }
    }

    open class var streetPrefixName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.streetPrefixName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.streetPrefixName_ = value
            }
        }
    }

    open var streetPrefixName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.streetPrefixName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.streetPrefixName, to: StringValue.of(optional: value))
        }
    }

    open class var streetSuffixName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.streetSuffixName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.streetSuffixName_ = value
            }
        }
    }

    open var streetSuffixName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.streetSuffixName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.streetSuffixName, to: StringValue.of(optional: value))
        }
    }

    open class var subscriberID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.subscriberID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.subscriberID_ = value
            }
        }
    }

    open var subscriberID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.subscriberID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.subscriberID, to: StringValue.of(optional: value))
        }
    }

    open class var supplementaryIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.supplementaryIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.supplementaryIndicator_ = value
            }
        }
    }

    open var supplementaryIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.supplementaryIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.supplementaryIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var supplyingPlantID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.supplyingPlantID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.supplyingPlantID_ = value
            }
        }
    }

    open var supplyingPlantID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.supplyingPlantID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.supplyingPlantID, to: StringValue.of(optional: value))
        }
    }

    open class var supplyingPlantName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.supplyingPlantName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.supplyingPlantName_ = value
            }
        }
    }

    open var supplyingPlantName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.supplyingPlantName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.supplyingPlantName, to: StringValue.of(optional: value))
        }
    }

    open class var taxJurisdictionCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.taxJurisdictionCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.taxJurisdictionCode_ = value
            }
        }
    }

    open var taxJurisdictionCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.taxJurisdictionCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.taxJurisdictionCode, to: StringValue.of(optional: value))
        }
    }

    open class var thirdInputParameterValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.thirdInputParameterValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.thirdInputParameterValue_ = value
            }
        }
    }

    open var thirdInputParameterValue: Double? {
        get {
            return DoubleValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.thirdInputParameterValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.thirdInputParameterValue, to: DoubleValue.of(optional: value))
        }
    }

    open class var timeZoneDifferenceValue: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.timeZoneDifferenceValue_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.timeZoneDifferenceValue_ = value
            }
        }
    }

    open var timeZoneDifferenceValue: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.timeZoneDifferenceValue))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.timeZoneDifferenceValue, to: IntegerValue.of(optional: value))
        }
    }

    open class var transferLocationName: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.transferLocationName_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.transferLocationName_ = value
            }
        }
    }

    open var transferLocationName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.transferLocationName))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.transferLocationName, to: StringValue.of(optional: value))
        }
    }

    open class var transportServiceLevelCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.transportServiceLevelCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.transportServiceLevelCode_ = value
            }
        }
    }

    open var transportServiceLevelCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.transportServiceLevelCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.transportServiceLevelCode, to: StringValue.of(optional: value))
        }
    }

    open class var typeCode_: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.typeCode__
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.typeCode__ = value
            }
        }
    }

    open var typeCode_: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.typeCode_))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.typeCode_, to: StringValue.of(optional: value))
        }
    }

    open class var typeID: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.typeID_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.typeID_ = value
            }
        }
    }

    open var typeID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.typeID))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.typeID, to: StringValue.of(optional: value))
        }
    }

    open class var underPercent: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.underPercent_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.underPercent_ = value
            }
        }
    }

    open var underPercent: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.underPercent))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.underPercent, to: IntegerValue.of(optional: value))
        }
    }

    open class var unitCode: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.unitCode_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.unitCode_ = value
            }
        }
    }

    open var unitCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.unitCode))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.unitCode, to: StringValue.of(optional: value))
        }
    }

    open class var unitCurrency: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.unitCurrency_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.unitCurrency_ = value
            }
        }
    }

    open var unitCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.unitCurrency))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.unitCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var uri: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.uri_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.uri_ = value
            }
        }
    }

    open var uri: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.uri))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.uri, to: StringValue.of(optional: value))
        }
    }

    open class var uriDefaultIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.uriDefaultIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.uriDefaultIndicator_ = value
            }
        }
    }

    open var uriDefaultIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.uriDefaultIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.uriDefaultIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var uriUsageDenialIndicator: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.uriUsageDenialIndicator_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.uriUsageDenialIndicator_ = value
            }
        }
    }

    open var uriUsageDenialIndicator: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.uriUsageDenialIndicator))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.uriUsageDenialIndicator, to: BooleanValue.of(optional: value))
        }
    }

    open class var uuid: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.uuid_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.uuid_ = value
            }
        }
    }

    open var uuid: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.uuid))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.uuid, to: StringValue.of(optional: value))
        }
    }

    open class var webURI: Property {
        get {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                return PurchaseOrderERPCreateRequestConfirmationInV1.webURI_
            }
        }
        set(value) {
            objc_sync_enter(PurchaseOrderERPCreateRequestConfirmationInV1.self)
            defer { objc_sync_exit(PurchaseOrderERPCreateRequestConfirmationInV1.self) }
            do {
                PurchaseOrderERPCreateRequestConfirmationInV1.webURI_ = value
            }
        }
    }

    open var webURI: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.webURI))
        }
        set(value) {
            self.setOptionalValue(for: PurchaseOrderERPCreateRequestConfirmationInV1.webURI, to: StringValue.of(optional: value))
        }
    }
}
