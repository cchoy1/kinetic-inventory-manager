// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class APurchaseOrderScheduleLineType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var purchasingDocument_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PurchasingDocument")

    private static var purchasingDocumentItem_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PurchasingDocumentItem")

    private static var scheduleLine_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "ScheduleLine")

    private static var delivDateCategory_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "DelivDateCategory")

    private static var scheduleLineDeliveryDate_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "ScheduleLineDeliveryDate")

    private static var purchaseOrderQuantityUnit_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PurchaseOrderQuantityUnit")

    private static var scheduleLineOrderQuantity_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "ScheduleLineOrderQuantity")

    private static var scheduleLineDeliveryTime_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "ScheduleLineDeliveryTime")

    private static var schedLineStscDeliveryDate_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "SchedLineStscDeliveryDate")

    private static var purchaseRequisition_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PurchaseRequisition")

    private static var purchaseRequisitionItem_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PurchaseRequisitionItem")

    private static var scheduleLineCommittedQuantity_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "ScheduleLineCommittedQuantity")

    private static var performancePeriodStartDate_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PerformancePeriodStartDate")

    private static var performancePeriodEndDate_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PerformancePeriodEndDate")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType)
    }

    open class func array(from: EntityValueList) -> [APurchaseOrderScheduleLineType] {
        return ArrayConverter.convert(from.toArray(), [APurchaseOrderScheduleLineType]())
    }

    open func copy() -> APurchaseOrderScheduleLineType {
        return CastRequired<APurchaseOrderScheduleLineType>.from(self.copyEntity())
    }

    open class var delivDateCategory: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.delivDateCategory_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.delivDateCategory_ = value
            }
        }
    }

    open var delivDateCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderScheduleLineType.delivDateCategory))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.delivDateCategory, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(purchasingDocument: String?, purchasingDocumentItem: String?, scheduleLine: String?) -> EntityKey {
        return EntityKey().with(name: "PurchasingDocument", value: StringValue.of(optional: purchasingDocument)).with(name: "PurchasingDocumentItem", value: StringValue.of(optional: purchasingDocumentItem)).with(name: "ScheduleLine", value: StringValue.of(optional: scheduleLine))
    }

    open var old: APurchaseOrderScheduleLineType {
        return CastRequired<APurchaseOrderScheduleLineType>.from(self.oldEntity)
    }

    open class var performancePeriodEndDate: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.performancePeriodEndDate_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.performancePeriodEndDate_ = value
            }
        }
    }

    open var performancePeriodEndDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: APurchaseOrderScheduleLineType.performancePeriodEndDate))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.performancePeriodEndDate, to: value)
        }
    }

    open class var performancePeriodStartDate: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.performancePeriodStartDate_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.performancePeriodStartDate_ = value
            }
        }
    }

    open var performancePeriodStartDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: APurchaseOrderScheduleLineType.performancePeriodStartDate))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.performancePeriodStartDate, to: value)
        }
    }

    open class var purchaseOrderQuantityUnit: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.purchaseOrderQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.purchaseOrderQuantityUnit_ = value
            }
        }
    }

    open var purchaseOrderQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderScheduleLineType.purchaseOrderQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.purchaseOrderQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseRequisition: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.purchaseRequisition_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.purchaseRequisition_ = value
            }
        }
    }

    open var purchaseRequisition: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderScheduleLineType.purchaseRequisition))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.purchaseRequisition, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseRequisitionItem: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.purchaseRequisitionItem_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.purchaseRequisitionItem_ = value
            }
        }
    }

    open var purchaseRequisitionItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderScheduleLineType.purchaseRequisitionItem))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.purchaseRequisitionItem, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingDocument: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.purchasingDocument_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.purchasingDocument_ = value
            }
        }
    }

    open var purchasingDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderScheduleLineType.purchasingDocument))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.purchasingDocument, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingDocumentItem: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.purchasingDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.purchasingDocumentItem_ = value
            }
        }
    }

    open var purchasingDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderScheduleLineType.purchasingDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.purchasingDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var schedLineStscDeliveryDate: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.schedLineStscDeliveryDate_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.schedLineStscDeliveryDate_ = value
            }
        }
    }

    open var schedLineStscDeliveryDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: APurchaseOrderScheduleLineType.schedLineStscDeliveryDate))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.schedLineStscDeliveryDate, to: value)
        }
    }

    open class var scheduleLine: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.scheduleLine_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.scheduleLine_ = value
            }
        }
    }

    open var scheduleLine: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderScheduleLineType.scheduleLine))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.scheduleLine, to: StringValue.of(optional: value))
        }
    }

    open class var scheduleLineCommittedQuantity: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.scheduleLineCommittedQuantity_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.scheduleLineCommittedQuantity_ = value
            }
        }
    }

    open var scheduleLineCommittedQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurchaseOrderScheduleLineType.scheduleLineCommittedQuantity))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.scheduleLineCommittedQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var scheduleLineDeliveryDate: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.scheduleLineDeliveryDate_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.scheduleLineDeliveryDate_ = value
            }
        }
    }

    open var scheduleLineDeliveryDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: APurchaseOrderScheduleLineType.scheduleLineDeliveryDate))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.scheduleLineDeliveryDate, to: value)
        }
    }

    open class var scheduleLineDeliveryTime: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.scheduleLineDeliveryTime_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.scheduleLineDeliveryTime_ = value
            }
        }
    }

    open var scheduleLineDeliveryTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: APurchaseOrderScheduleLineType.scheduleLineDeliveryTime))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.scheduleLineDeliveryTime, to: value)
        }
    }

    open class var scheduleLineOrderQuantity: Property {
        get {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                return APurchaseOrderScheduleLineType.scheduleLineOrderQuantity_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderScheduleLineType.self)
            defer { objc_sync_exit(APurchaseOrderScheduleLineType.self) }
            do {
                APurchaseOrderScheduleLineType.scheduleLineOrderQuantity_ = value
            }
        }
    }

    open var scheduleLineOrderQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurchaseOrderScheduleLineType.scheduleLineOrderQuantity))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderScheduleLineType.scheduleLineOrderQuantity, to: DecimalValue.of(optional: value))
        }
    }
}
