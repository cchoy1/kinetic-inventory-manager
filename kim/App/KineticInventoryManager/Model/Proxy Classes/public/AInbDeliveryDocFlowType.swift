// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AInbDeliveryDocFlowType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var deliveryVersion_: Property = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "DeliveryVersion")

    private static var precedingDocument_: Property = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "PrecedingDocument")

    private static var precedingDocumentCategory_: Property = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "PrecedingDocumentCategory")

    private static var precedingDocumentItem_: Property = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "PrecedingDocumentItem")

    private static var quantityInBaseUnit_: Property = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "QuantityInBaseUnit")

    private static var sdFulfillmentCalculationRule_: Property = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "SDFulfillmentCalculationRule")

    private static var subsequentDocument_: Property = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "SubsequentDocument")

    private static var subsequentDocumentCategory_: Property = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "SubsequentDocumentCategory")

    private static var subsequentDocumentItem_: Property = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "SubsequentDocumentItem")

    private static var transferOrderInWrhsMgmtIsConfd_: Property = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "TransferOrderInWrhsMgmtIsConfd")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType)
    }

    open class func array(from: EntityValueList) -> [AInbDeliveryDocFlowType] {
        return ArrayConverter.convert(from.toArray(), [AInbDeliveryDocFlowType]())
    }

    open func copy() -> AInbDeliveryDocFlowType {
        return CastRequired<AInbDeliveryDocFlowType>.from(self.copyEntity())
    }

    open class var deliveryVersion: Property {
        get {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                return AInbDeliveryDocFlowType.deliveryVersion_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                AInbDeliveryDocFlowType.deliveryVersion_ = value
            }
        }
    }

    open var deliveryVersion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryDocFlowType.deliveryVersion))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryDocFlowType.deliveryVersion, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(precedingDocument: String?, precedingDocumentItem: String?, subsequentDocumentCategory: String?) -> EntityKey {
        return EntityKey().with(name: "PrecedingDocument", value: StringValue.of(optional: precedingDocument)).with(name: "PrecedingDocumentItem", value: StringValue.of(optional: precedingDocumentItem)).with(name: "SubsequentDocumentCategory", value: StringValue.of(optional: subsequentDocumentCategory))
    }

    open var old: AInbDeliveryDocFlowType {
        return CastRequired<AInbDeliveryDocFlowType>.from(self.oldEntity)
    }

    open class var precedingDocument: Property {
        get {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                return AInbDeliveryDocFlowType.precedingDocument_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                AInbDeliveryDocFlowType.precedingDocument_ = value
            }
        }
    }

    open var precedingDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryDocFlowType.precedingDocument))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryDocFlowType.precedingDocument, to: StringValue.of(optional: value))
        }
    }

    open class var precedingDocumentCategory: Property {
        get {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                return AInbDeliveryDocFlowType.precedingDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                AInbDeliveryDocFlowType.precedingDocumentCategory_ = value
            }
        }
    }

    open var precedingDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryDocFlowType.precedingDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryDocFlowType.precedingDocumentCategory, to: StringValue.of(optional: value))
        }
    }

    open class var precedingDocumentItem: Property {
        get {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                return AInbDeliveryDocFlowType.precedingDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                AInbDeliveryDocFlowType.precedingDocumentItem_ = value
            }
        }
    }

    open var precedingDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryDocFlowType.precedingDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryDocFlowType.precedingDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var quantityInBaseUnit: Property {
        get {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                return AInbDeliveryDocFlowType.quantityInBaseUnit_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                AInbDeliveryDocFlowType.quantityInBaseUnit_ = value
            }
        }
    }

    open var quantityInBaseUnit: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryDocFlowType.quantityInBaseUnit))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryDocFlowType.quantityInBaseUnit, to: DecimalValue.of(optional: value))
        }
    }

    open class var sdFulfillmentCalculationRule: Property {
        get {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                return AInbDeliveryDocFlowType.sdFulfillmentCalculationRule_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                AInbDeliveryDocFlowType.sdFulfillmentCalculationRule_ = value
            }
        }
    }

    open var sdFulfillmentCalculationRule: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryDocFlowType.sdFulfillmentCalculationRule))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryDocFlowType.sdFulfillmentCalculationRule, to: StringValue.of(optional: value))
        }
    }

    open class var subsequentDocument: Property {
        get {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                return AInbDeliveryDocFlowType.subsequentDocument_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                AInbDeliveryDocFlowType.subsequentDocument_ = value
            }
        }
    }

    open var subsequentDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryDocFlowType.subsequentDocument))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryDocFlowType.subsequentDocument, to: StringValue.of(optional: value))
        }
    }

    open class var subsequentDocumentCategory: Property {
        get {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                return AInbDeliveryDocFlowType.subsequentDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                AInbDeliveryDocFlowType.subsequentDocumentCategory_ = value
            }
        }
    }

    open var subsequentDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryDocFlowType.subsequentDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryDocFlowType.subsequentDocumentCategory, to: StringValue.of(optional: value))
        }
    }

    open class var subsequentDocumentItem: Property {
        get {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                return AInbDeliveryDocFlowType.subsequentDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                AInbDeliveryDocFlowType.subsequentDocumentItem_ = value
            }
        }
    }

    open var subsequentDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryDocFlowType.subsequentDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryDocFlowType.subsequentDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var transferOrderInWrhsMgmtIsConfd: Property {
        get {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                return AInbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AInbDeliveryDocFlowType.self) }
            do {
                AInbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd_ = value
            }
        }
    }

    open var transferOrderInWrhsMgmtIsConfd: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AInbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd, to: BooleanValue.of(optional: value))
        }
    }
}
