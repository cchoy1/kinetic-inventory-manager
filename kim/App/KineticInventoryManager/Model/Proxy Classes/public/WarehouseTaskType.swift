// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class WarehouseTaskType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var handlingUnitID_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "HandlingUnitID")

    private static var warehouse_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "Warehouse")

    private static var warehouseTask_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseTask")

    private static var warehouseTaskItem_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseTaskItem")

    private static var warehouseOrder_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseOrder")

    private static var creationDateTime_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "CreationDateTime")

    private static var lastChangeDateTime_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "LastChangeDateTime")

    private static var confirmationUTCDateTime_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ConfirmationUTCDateTime")

    private static var whseTaskPlannedClosingDateTime_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WhseTaskPlannedClosingDateTime")

    private static var whseTaskGoodsReceiptDateTime_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WhseTaskGoodsReceiptDateTime")

    private static var warehouseTaskStatus_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseTaskStatus")

    private static var warehouseTaskStatusName_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseTaskStatusName")

    private static var warehouseProcessType_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseProcessType")

    private static var warehouseProcessTypeName_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseProcessTypeName")

    private static var isHandlingUnitWarehouseTask_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "IsHandlingUnitWarehouseTask")

    private static var productName_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ProductName")

    private static var productDescription_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ProductDescription")

    private static var batch_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "Batch")

    private static var batchChangeIsNotAllowed_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "BatchChangeIsNotAllowed")

    private static var stockType_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "StockType")

    private static var stockTypeName_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "StockTypeName")

    private static var stockOwner_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "StockOwner")

    private static var entitledToDisposeParty_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "EntitledToDisposeParty")

    private static var stockDocumentCategory_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "StockDocumentCategory")

    private static var stockDocumentNumber_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "StockDocumentNumber")

    private static var stockItemNumber_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "StockItemNumber")

    private static var executingResource_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ExecutingResource")

    private static var productionOrder_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ProductionOrder")

    private static var productionSupplyArea_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ProductionSupplyArea")

    private static var delivery_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "Delivery")

    private static var deliveryItem_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DeliveryItem")

    private static var purchasingDocument_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "PurchasingDocument")

    private static var purchasingDocumentItem_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "PurchasingDocumentItem")

    private static var salesDocument_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SalesDocument")

    private static var salesDocumentItem_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SalesDocumentItem")

    private static var baseUnit_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "BaseUnit")

    private static var alternativeUnit_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "AlternativeUnit")

    private static var targetQuantityInBaseUnit_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "TargetQuantityInBaseUnit")

    private static var targetQuantityInAltvUnit_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "TargetQuantityInAltvUnit")

    private static var actualQuantityInBaseUnit_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ActualQuantityInBaseUnit")

    private static var actualQuantityInAltvUnit_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ActualQuantityInAltvUnit")

    private static var differenceQuantityInBaseUnit_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DifferenceQuantityInBaseUnit")

    private static var differenceQuantityInAltvUnit_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DifferenceQuantityInAltvUnit")

    private static var whseTaskNetWeightUnitOfMeasure_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WhseTaskNetWeightUnitOfMeasure")

    private static var netWeight_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "NetWeight")

    private static var whseTaskNetVolumeUnitOfMeasure_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WhseTaskNetVolumeUnitOfMeasure")

    private static var whseTaskNetVolume_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WhseTaskNetVolume")

    private static var sourceStorageType_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SourceStorageType")

    private static var sourceStorageTypeName_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SourceStorageTypeName")

    private static var sourceStorageSection_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SourceStorageSection")

    private static var sourceStorageBin_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SourceStorageBin")

    private static var destinationStorageType_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DestinationStorageType")

    private static var destinationStorageTypeName_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DestinationStorageTypeName")

    private static var destinationStorageSection_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DestinationStorageSection")

    private static var destinationStorageBin_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DestinationStorageBin")

    private static var destinationResource_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DestinationResource")

    private static var activityArea_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ActivityArea")

    private static var activityAreaName_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ActivityAreaName")

    private static var sourceHandlingUnit_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SourceHandlingUnit")

    private static var destinationHandlingUnit_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DestinationHandlingUnit")

    private static var warehouseTaskExceptionCode_: Property = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseTaskExceptionCode")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.warehouseTaskType)
    }

    open class var activityArea: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.activityArea_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.activityArea_ = value
            }
        }
    }

    open var activityArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.activityArea))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.activityArea, to: StringValue.of(optional: value))
        }
    }

    open class var activityAreaName: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.activityAreaName_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.activityAreaName_ = value
            }
        }
    }

    open var activityAreaName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.activityAreaName))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.activityAreaName, to: StringValue.of(optional: value))
        }
    }

    open class var actualQuantityInAltvUnit: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.actualQuantityInAltvUnit_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.actualQuantityInAltvUnit_ = value
            }
        }
    }

    open var actualQuantityInAltvUnit: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTaskType.actualQuantityInAltvUnit))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.actualQuantityInAltvUnit, to: DecimalValue.of(optional: value))
        }
    }

    open class var actualQuantityInBaseUnit: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.actualQuantityInBaseUnit_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.actualQuantityInBaseUnit_ = value
            }
        }
    }

    open var actualQuantityInBaseUnit: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTaskType.actualQuantityInBaseUnit))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.actualQuantityInBaseUnit, to: DecimalValue.of(optional: value))
        }
    }

    open class var alternativeUnit: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.alternativeUnit_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.alternativeUnit_ = value
            }
        }
    }

    open var alternativeUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.alternativeUnit))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.alternativeUnit, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [WarehouseTaskType] {
        return ArrayConverter.convert(from.toArray(), [WarehouseTaskType]())
    }

    open class var baseUnit: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.baseUnit_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.baseUnit_ = value
            }
        }
    }

    open var baseUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.baseUnit))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.baseUnit, to: StringValue.of(optional: value))
        }
    }

    open class var batch: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.batch_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.batch_ = value
            }
        }
    }

    open var batch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.batch))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.batch, to: StringValue.of(optional: value))
        }
    }

    open class var batchChangeIsNotAllowed: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.batchChangeIsNotAllowed_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.batchChangeIsNotAllowed_ = value
            }
        }
    }

    open var batchChangeIsNotAllowed: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: WarehouseTaskType.batchChangeIsNotAllowed))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.batchChangeIsNotAllowed, to: BooleanValue.of(optional: value))
        }
    }

    open class var confirmationUTCDateTime: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.confirmationUTCDateTime_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.confirmationUTCDateTime_ = value
            }
        }
    }

    open var confirmationUTCDateTime: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: WarehouseTaskType.confirmationUTCDateTime))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.confirmationUTCDateTime, to: value)
        }
    }

    open func copy() -> WarehouseTaskType {
        return CastRequired<WarehouseTaskType>.from(self.copyEntity())
    }

    open class var creationDateTime: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.creationDateTime_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.creationDateTime_ = value
            }
        }
    }

    open var creationDateTime: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: WarehouseTaskType.creationDateTime))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.creationDateTime, to: value)
        }
    }

    open class var delivery: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.delivery_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.delivery_ = value
            }
        }
    }

    open var delivery: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.delivery))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.delivery, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryItem: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.deliveryItem_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.deliveryItem_ = value
            }
        }
    }

    open var deliveryItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.deliveryItem))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.deliveryItem, to: StringValue.of(optional: value))
        }
    }

    open class var destinationHandlingUnit: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.destinationHandlingUnit_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.destinationHandlingUnit_ = value
            }
        }
    }

    open var destinationHandlingUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.destinationHandlingUnit))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.destinationHandlingUnit, to: StringValue.of(optional: value))
        }
    }

    open class var destinationResource: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.destinationResource_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.destinationResource_ = value
            }
        }
    }

    open var destinationResource: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.destinationResource))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.destinationResource, to: StringValue.of(optional: value))
        }
    }

    open class var destinationStorageBin: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.destinationStorageBin_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.destinationStorageBin_ = value
            }
        }
    }

    open var destinationStorageBin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.destinationStorageBin))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.destinationStorageBin, to: StringValue.of(optional: value))
        }
    }

    open class var destinationStorageSection: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.destinationStorageSection_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.destinationStorageSection_ = value
            }
        }
    }

    open var destinationStorageSection: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.destinationStorageSection))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.destinationStorageSection, to: StringValue.of(optional: value))
        }
    }

    open class var destinationStorageType: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.destinationStorageType_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.destinationStorageType_ = value
            }
        }
    }

    open var destinationStorageType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.destinationStorageType))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.destinationStorageType, to: StringValue.of(optional: value))
        }
    }

    open class var destinationStorageTypeName: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.destinationStorageTypeName_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.destinationStorageTypeName_ = value
            }
        }
    }

    open var destinationStorageTypeName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.destinationStorageTypeName))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.destinationStorageTypeName, to: StringValue.of(optional: value))
        }
    }

    open class var differenceQuantityInAltvUnit: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.differenceQuantityInAltvUnit_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.differenceQuantityInAltvUnit_ = value
            }
        }
    }

    open var differenceQuantityInAltvUnit: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTaskType.differenceQuantityInAltvUnit))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.differenceQuantityInAltvUnit, to: DecimalValue.of(optional: value))
        }
    }

    open class var differenceQuantityInBaseUnit: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.differenceQuantityInBaseUnit_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.differenceQuantityInBaseUnit_ = value
            }
        }
    }

    open var differenceQuantityInBaseUnit: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTaskType.differenceQuantityInBaseUnit))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.differenceQuantityInBaseUnit, to: DecimalValue.of(optional: value))
        }
    }

    open class var entitledToDisposeParty: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.entitledToDisposeParty_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.entitledToDisposeParty_ = value
            }
        }
    }

    open var entitledToDisposeParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.entitledToDisposeParty))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.entitledToDisposeParty, to: StringValue.of(optional: value))
        }
    }

    open class var executingResource: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.executingResource_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.executingResource_ = value
            }
        }
    }

    open var executingResource: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.executingResource))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.executingResource, to: StringValue.of(optional: value))
        }
    }

    open class var handlingUnitID: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.handlingUnitID_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.handlingUnitID_ = value
            }
        }
    }

    open var handlingUnitID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.handlingUnitID))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.handlingUnitID, to: StringValue.of(optional: value))
        }
    }

    open class var isHandlingUnitWarehouseTask: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.isHandlingUnitWarehouseTask_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.isHandlingUnitWarehouseTask_ = value
            }
        }
    }

    open var isHandlingUnitWarehouseTask: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: WarehouseTaskType.isHandlingUnitWarehouseTask))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.isHandlingUnitWarehouseTask, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(warehouse: String?) -> EntityKey {
        return EntityKey().with(name: "Warehouse", value: StringValue.of(optional: warehouse))
    }

    open class var lastChangeDateTime: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.lastChangeDateTime_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.lastChangeDateTime_ = value
            }
        }
    }

    open var lastChangeDateTime: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: WarehouseTaskType.lastChangeDateTime))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.lastChangeDateTime, to: value)
        }
    }

    open class var netWeight: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.netWeight_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.netWeight_ = value
            }
        }
    }

    open var netWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTaskType.netWeight))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.netWeight, to: DecimalValue.of(optional: value))
        }
    }

    open var old: WarehouseTaskType {
        return CastRequired<WarehouseTaskType>.from(self.oldEntity)
    }

    open class var productDescription: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.productDescription_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.productDescription_ = value
            }
        }
    }

    open var productDescription: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.productDescription))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.productDescription, to: StringValue.of(optional: value))
        }
    }

    open class var productName: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.productName_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.productName_ = value
            }
        }
    }

    open var productName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.productName))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.productName, to: StringValue.of(optional: value))
        }
    }

    open class var productionOrder: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.productionOrder_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.productionOrder_ = value
            }
        }
    }

    open var productionOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.productionOrder))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.productionOrder, to: StringValue.of(optional: value))
        }
    }

    open class var productionSupplyArea: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.productionSupplyArea_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.productionSupplyArea_ = value
            }
        }
    }

    open var productionSupplyArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.productionSupplyArea))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.productionSupplyArea, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingDocument: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.purchasingDocument_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.purchasingDocument_ = value
            }
        }
    }

    open var purchasingDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.purchasingDocument))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.purchasingDocument, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingDocumentItem: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.purchasingDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.purchasingDocumentItem_ = value
            }
        }
    }

    open var purchasingDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.purchasingDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.purchasingDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var salesDocument: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.salesDocument_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.salesDocument_ = value
            }
        }
    }

    open var salesDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.salesDocument))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.salesDocument, to: StringValue.of(optional: value))
        }
    }

    open class var salesDocumentItem: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.salesDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.salesDocumentItem_ = value
            }
        }
    }

    open var salesDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.salesDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.salesDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var sourceHandlingUnit: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.sourceHandlingUnit_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.sourceHandlingUnit_ = value
            }
        }
    }

    open var sourceHandlingUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.sourceHandlingUnit))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.sourceHandlingUnit, to: StringValue.of(optional: value))
        }
    }

    open class var sourceStorageBin: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.sourceStorageBin_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.sourceStorageBin_ = value
            }
        }
    }

    open var sourceStorageBin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.sourceStorageBin))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.sourceStorageBin, to: StringValue.of(optional: value))
        }
    }

    open class var sourceStorageSection: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.sourceStorageSection_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.sourceStorageSection_ = value
            }
        }
    }

    open var sourceStorageSection: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.sourceStorageSection))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.sourceStorageSection, to: StringValue.of(optional: value))
        }
    }

    open class var sourceStorageType: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.sourceStorageType_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.sourceStorageType_ = value
            }
        }
    }

    open var sourceStorageType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.sourceStorageType))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.sourceStorageType, to: StringValue.of(optional: value))
        }
    }

    open class var sourceStorageTypeName: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.sourceStorageTypeName_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.sourceStorageTypeName_ = value
            }
        }
    }

    open var sourceStorageTypeName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.sourceStorageTypeName))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.sourceStorageTypeName, to: StringValue.of(optional: value))
        }
    }

    open class var stockDocumentCategory: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.stockDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.stockDocumentCategory_ = value
            }
        }
    }

    open var stockDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.stockDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.stockDocumentCategory, to: StringValue.of(optional: value))
        }
    }

    open class var stockDocumentNumber: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.stockDocumentNumber_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.stockDocumentNumber_ = value
            }
        }
    }

    open var stockDocumentNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.stockDocumentNumber))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.stockDocumentNumber, to: StringValue.of(optional: value))
        }
    }

    open class var stockItemNumber: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.stockItemNumber_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.stockItemNumber_ = value
            }
        }
    }

    open var stockItemNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.stockItemNumber))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.stockItemNumber, to: StringValue.of(optional: value))
        }
    }

    open class var stockOwner: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.stockOwner_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.stockOwner_ = value
            }
        }
    }

    open var stockOwner: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.stockOwner))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.stockOwner, to: StringValue.of(optional: value))
        }
    }

    open class var stockType: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.stockType_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.stockType_ = value
            }
        }
    }

    open var stockType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.stockType))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.stockType, to: StringValue.of(optional: value))
        }
    }

    open class var stockTypeName: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.stockTypeName_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.stockTypeName_ = value
            }
        }
    }

    open var stockTypeName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.stockTypeName))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.stockTypeName, to: StringValue.of(optional: value))
        }
    }

    open class var targetQuantityInAltvUnit: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.targetQuantityInAltvUnit_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.targetQuantityInAltvUnit_ = value
            }
        }
    }

    open var targetQuantityInAltvUnit: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTaskType.targetQuantityInAltvUnit))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.targetQuantityInAltvUnit, to: DecimalValue.of(optional: value))
        }
    }

    open class var targetQuantityInBaseUnit: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.targetQuantityInBaseUnit_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.targetQuantityInBaseUnit_ = value
            }
        }
    }

    open var targetQuantityInBaseUnit: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTaskType.targetQuantityInBaseUnit))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.targetQuantityInBaseUnit, to: DecimalValue.of(optional: value))
        }
    }

    open class var warehouse: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.warehouse_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.warehouse_ = value
            }
        }
    }

    open var warehouse: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.warehouse))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.warehouse, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseOrder: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.warehouseOrder_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.warehouseOrder_ = value
            }
        }
    }

    open var warehouseOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.warehouseOrder))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.warehouseOrder, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseProcessType: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.warehouseProcessType_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.warehouseProcessType_ = value
            }
        }
    }

    open var warehouseProcessType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.warehouseProcessType))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.warehouseProcessType, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseProcessTypeName: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.warehouseProcessTypeName_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.warehouseProcessTypeName_ = value
            }
        }
    }

    open var warehouseProcessTypeName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.warehouseProcessTypeName))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.warehouseProcessTypeName, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseTask: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.warehouseTask_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.warehouseTask_ = value
            }
        }
    }

    open var warehouseTask: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.warehouseTask))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.warehouseTask, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseTaskExceptionCode: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.warehouseTaskExceptionCode_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.warehouseTaskExceptionCode_ = value
            }
        }
    }

    open var warehouseTaskExceptionCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.warehouseTaskExceptionCode))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.warehouseTaskExceptionCode, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseTaskItem: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.warehouseTaskItem_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.warehouseTaskItem_ = value
            }
        }
    }

    open var warehouseTaskItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.warehouseTaskItem))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.warehouseTaskItem, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseTaskStatus: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.warehouseTaskStatus_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.warehouseTaskStatus_ = value
            }
        }
    }

    open var warehouseTaskStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.warehouseTaskStatus))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.warehouseTaskStatus, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseTaskStatusName: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.warehouseTaskStatusName_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.warehouseTaskStatusName_ = value
            }
        }
    }

    open var warehouseTaskStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.warehouseTaskStatusName))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.warehouseTaskStatusName, to: StringValue.of(optional: value))
        }
    }

    open class var whseTaskGoodsReceiptDateTime: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.whseTaskGoodsReceiptDateTime_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.whseTaskGoodsReceiptDateTime_ = value
            }
        }
    }

    open var whseTaskGoodsReceiptDateTime: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: WarehouseTaskType.whseTaskGoodsReceiptDateTime))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.whseTaskGoodsReceiptDateTime, to: value)
        }
    }

    open class var whseTaskNetVolume: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.whseTaskNetVolume_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.whseTaskNetVolume_ = value
            }
        }
    }

    open var whseTaskNetVolume: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTaskType.whseTaskNetVolume))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.whseTaskNetVolume, to: DecimalValue.of(optional: value))
        }
    }

    open class var whseTaskNetVolumeUnitOfMeasure: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.whseTaskNetVolumeUnitOfMeasure_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.whseTaskNetVolumeUnitOfMeasure_ = value
            }
        }
    }

    open var whseTaskNetVolumeUnitOfMeasure: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.whseTaskNetVolumeUnitOfMeasure))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.whseTaskNetVolumeUnitOfMeasure, to: StringValue.of(optional: value))
        }
    }

    open class var whseTaskNetWeightUnitOfMeasure: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.whseTaskNetWeightUnitOfMeasure_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.whseTaskNetWeightUnitOfMeasure_ = value
            }
        }
    }

    open var whseTaskNetWeightUnitOfMeasure: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTaskType.whseTaskNetWeightUnitOfMeasure))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.whseTaskNetWeightUnitOfMeasure, to: StringValue.of(optional: value))
        }
    }

    open class var whseTaskPlannedClosingDateTime: Property {
        get {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                return WarehouseTaskType.whseTaskPlannedClosingDateTime_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTaskType.self)
            defer { objc_sync_exit(WarehouseTaskType.self) }
            do {
                WarehouseTaskType.whseTaskPlannedClosingDateTime_ = value
            }
        }
    }

    open var whseTaskPlannedClosingDateTime: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: WarehouseTaskType.whseTaskPlannedClosingDateTime))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTaskType.whseTaskPlannedClosingDateTime, to: value)
        }
    }
}
