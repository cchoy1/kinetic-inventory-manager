// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AInbDeliveryItemType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var actualDeliveredQtyInBaseUnit_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ActualDeliveredQtyInBaseUnit")

    private static var actualDeliveryQuantity_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ActualDeliveryQuantity")

    private static var additionalCustomerGroup1_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup1")

    private static var additionalCustomerGroup2_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup2")

    private static var additionalCustomerGroup3_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup3")

    private static var additionalCustomerGroup4_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup4")

    private static var additionalCustomerGroup5_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup5")

    private static var additionalMaterialGroup1_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup1")

    private static var additionalMaterialGroup2_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup2")

    private static var additionalMaterialGroup3_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup3")

    private static var additionalMaterialGroup4_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup4")

    private static var additionalMaterialGroup5_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup5")

    private static var alternateProductNumber_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AlternateProductNumber")

    private static var baseUnit_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BaseUnit")

    private static var batch_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Batch")

    private static var batchBySupplier_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BatchBySupplier")

    private static var batchClassification_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BatchClassification")

    private static var bomExplosion_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BOMExplosion")

    private static var businessArea_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BusinessArea")

    private static var consumptionPosting_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ConsumptionPosting")

    private static var controllingArea_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ControllingArea")

    private static var costCenter_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CostCenter")

    private static var createdByUser_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CreatedByUser")

    private static var creationDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CreationDate")

    private static var creationTime_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CreationTime")

    private static var custEngineeringChgStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CustEngineeringChgStatus")

    private static var deliveryDocument_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryDocument")

    private static var deliveryDocumentItem_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryDocumentItem")

    private static var deliveryDocumentItemCategory_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryDocumentItemCategory")

    private static var deliveryDocumentItemText_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryDocumentItemText")

    private static var deliveryGroup_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryGroup")

    private static var deliveryQuantityUnit_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryQuantityUnit")

    private static var deliveryRelatedBillingStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryRelatedBillingStatus")

    private static var deliveryToBaseQuantityDnmntr_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryToBaseQuantityDnmntr")

    private static var deliveryToBaseQuantityNmrtr_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryToBaseQuantityNmrtr")

    private static var departmentClassificationByCust_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DepartmentClassificationByCust")

    private static var distributionChannel_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DistributionChannel")

    private static var division_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Division")

    private static var fixedShipgProcgDurationInDays_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "FixedShipgProcgDurationInDays")

    private static var glAccount_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "GLAccount")

    private static var goodsMovementReasonCode_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "GoodsMovementReasonCode")

    private static var goodsMovementStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "GoodsMovementStatus")

    private static var goodsMovementType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "GoodsMovementType")

    private static var higherLevelItem_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "HigherLevelItem")

    private static var inspectionLot_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InspectionLot")

    private static var inspectionPartialLot_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InspectionPartialLot")

    private static var intercompanyBillingStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IntercompanyBillingStatus")

    private static var internationalArticleNumber_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InternationalArticleNumber")

    private static var inventorySpecialStockType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InventorySpecialStockType")

    private static var inventoryValuationType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InventoryValuationType")

    private static var isCompletelyDelivered_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IsCompletelyDelivered")

    private static var isNotGoodsMovementsRelevant_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IsNotGoodsMovementsRelevant")

    private static var isSeparateValuation_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IsSeparateValuation")

    private static var issgOrRcvgBatch_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgBatch")

    private static var issgOrRcvgMaterial_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgMaterial")

    private static var issgOrRcvgSpclStockInd_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgSpclStockInd")

    private static var issgOrRcvgStockCategory_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgStockCategory")

    private static var issgOrRcvgValuationType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgValuationType")

    private static var issuingOrReceivingPlant_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssuingOrReceivingPlant")

    private static var issuingOrReceivingStorageLoc_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssuingOrReceivingStorageLoc")

    private static var itemBillingBlockReason_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemBillingBlockReason")

    private static var itemBillingIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemBillingIncompletionStatus")

    private static var itemDeliveryIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemDeliveryIncompletionStatus")

    private static var itemGdsMvtIncompletionSts_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemGdsMvtIncompletionSts")

    private static var itemGeneralIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemGeneralIncompletionStatus")

    private static var itemGrossWeight_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemGrossWeight")

    private static var itemIsBillingRelevant_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemIsBillingRelevant")

    private static var itemNetWeight_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemNetWeight")

    private static var itemPackingIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemPackingIncompletionStatus")

    private static var itemPickingIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemPickingIncompletionStatus")

    private static var itemVolume_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemVolume")

    private static var itemVolumeUnit_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemVolumeUnit")

    private static var itemWeightUnit_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemWeightUnit")

    private static var lastChangeDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "LastChangeDate")

    private static var loadingGroup_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "LoadingGroup")

    private static var manufactureDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ManufactureDate")

    private static var material_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Material")

    private static var materialByCustomer_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialByCustomer")

    private static var materialFreightGroup_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialFreightGroup")

    private static var materialGroup_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialGroup")

    private static var materialIsBatchManaged_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialIsBatchManaged")

    private static var materialIsIntBatchManaged_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialIsIntBatchManaged")

    private static var numberOfSerialNumbers_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "NumberOfSerialNumbers")

    private static var orderID_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OrderID")

    private static var orderItem_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OrderItem")

    private static var originalDeliveryQuantity_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OriginalDeliveryQuantity")

    private static var originallyRequestedMaterial_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OriginallyRequestedMaterial")

    private static var overdelivTolrtdLmtRatioInPct_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OverdelivTolrtdLmtRatioInPct")

    private static var packingStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PackingStatus")

    private static var partialDeliveryIsAllowed_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PartialDeliveryIsAllowed")

    private static var paymentGuaranteeForm_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PaymentGuaranteeForm")

    private static var pickingConfirmationStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PickingConfirmationStatus")

    private static var pickingControl_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PickingControl")

    private static var pickingStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PickingStatus")

    private static var plant_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Plant")

    private static var primaryPostingSwitch_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PrimaryPostingSwitch")

    private static var productAvailabilityDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProductAvailabilityDate")

    private static var productAvailabilityTime_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProductAvailabilityTime")

    private static var productConfiguration_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProductConfiguration")

    private static var productHierarchyNode_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProductHierarchyNode")

    private static var profitabilitySegment_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProfitabilitySegment")

    private static var profitCenter_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProfitCenter")

    private static var proofOfDeliveryRelevanceCode_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProofOfDeliveryRelevanceCode")

    private static var proofOfDeliveryStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProofOfDeliveryStatus")

    private static var quantityIsFixed_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "QuantityIsFixed")

    private static var receivingPoint_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReceivingPoint")

    private static var referenceDocumentLogicalSystem_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReferenceDocumentLogicalSystem")

    private static var referenceSDDocument_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReferenceSDDocument")

    private static var referenceSDDocumentCategory_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReferenceSDDocumentCategory")

    private static var referenceSDDocumentItem_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReferenceSDDocumentItem")

    private static var retailPromotion_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "RetailPromotion")

    private static var salesDocumentItemType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SalesDocumentItemType")

    private static var salesGroup_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SalesGroup")

    private static var salesOffice_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SalesOffice")

    private static var sdDocumentCategory_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SDDocumentCategory")

    private static var sdProcessStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SDProcessStatus")

    private static var shelfLifeExpirationDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ShelfLifeExpirationDate")

    private static var statisticsDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StatisticsDate")

    private static var stockType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StockType")

    private static var storageBin_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StorageBin")

    private static var storageLocation_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StorageLocation")

    private static var storageType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StorageType")

    private static var subsequentMovementType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SubsequentMovementType")

    private static var transportationGroup_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "TransportationGroup")

    private static var underdelivTolrtdLmtRatioInPct_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "UnderdelivTolrtdLmtRatioInPct")

    private static var unlimitedOverdeliveryIsAllowed_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "UnlimitedOverdeliveryIsAllowed")

    private static var varblShipgProcgDurationInDays_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "VarblShipgProcgDurationInDays")

    private static var warehouse_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Warehouse")

    private static var warehouseActivityStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "WarehouseActivityStatus")

    private static var warehouseStagingArea_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "WarehouseStagingArea")

    private static var deliveryVersion_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryVersion")

    private static var warehouseStockCategory_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "WarehouseStockCategory")

    private static var warehouseStorageBin_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "WarehouseStorageBin")

    private static var stockSegment_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StockSegment")

    private static var requirementSegment_: Property = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "RequirementSegment")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aInbDeliveryItemType)
    }

    open class var actualDeliveredQtyInBaseUnit: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.actualDeliveredQtyInBaseUnit_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.actualDeliveredQtyInBaseUnit_ = value
            }
        }
    }

    open var actualDeliveredQtyInBaseUnit: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryItemType.actualDeliveredQtyInBaseUnit))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.actualDeliveredQtyInBaseUnit, to: DecimalValue.of(optional: value))
        }
    }

    open class var actualDeliveryQuantity: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.actualDeliveryQuantity_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.actualDeliveryQuantity_ = value
            }
        }
    }

    open var actualDeliveryQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryItemType.actualDeliveryQuantity))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.actualDeliveryQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var additionalCustomerGroup1: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.additionalCustomerGroup1_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.additionalCustomerGroup1_ = value
            }
        }
    }

    open var additionalCustomerGroup1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.additionalCustomerGroup1))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.additionalCustomerGroup1, to: StringValue.of(optional: value))
        }
    }

    open class var additionalCustomerGroup2: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.additionalCustomerGroup2_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.additionalCustomerGroup2_ = value
            }
        }
    }

    open var additionalCustomerGroup2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.additionalCustomerGroup2))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.additionalCustomerGroup2, to: StringValue.of(optional: value))
        }
    }

    open class var additionalCustomerGroup3: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.additionalCustomerGroup3_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.additionalCustomerGroup3_ = value
            }
        }
    }

    open var additionalCustomerGroup3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.additionalCustomerGroup3))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.additionalCustomerGroup3, to: StringValue.of(optional: value))
        }
    }

    open class var additionalCustomerGroup4: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.additionalCustomerGroup4_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.additionalCustomerGroup4_ = value
            }
        }
    }

    open var additionalCustomerGroup4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.additionalCustomerGroup4))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.additionalCustomerGroup4, to: StringValue.of(optional: value))
        }
    }

    open class var additionalCustomerGroup5: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.additionalCustomerGroup5_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.additionalCustomerGroup5_ = value
            }
        }
    }

    open var additionalCustomerGroup5: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.additionalCustomerGroup5))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.additionalCustomerGroup5, to: StringValue.of(optional: value))
        }
    }

    open class var additionalMaterialGroup1: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.additionalMaterialGroup1_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.additionalMaterialGroup1_ = value
            }
        }
    }

    open var additionalMaterialGroup1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.additionalMaterialGroup1))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.additionalMaterialGroup1, to: StringValue.of(optional: value))
        }
    }

    open class var additionalMaterialGroup2: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.additionalMaterialGroup2_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.additionalMaterialGroup2_ = value
            }
        }
    }

    open var additionalMaterialGroup2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.additionalMaterialGroup2))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.additionalMaterialGroup2, to: StringValue.of(optional: value))
        }
    }

    open class var additionalMaterialGroup3: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.additionalMaterialGroup3_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.additionalMaterialGroup3_ = value
            }
        }
    }

    open var additionalMaterialGroup3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.additionalMaterialGroup3))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.additionalMaterialGroup3, to: StringValue.of(optional: value))
        }
    }

    open class var additionalMaterialGroup4: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.additionalMaterialGroup4_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.additionalMaterialGroup4_ = value
            }
        }
    }

    open var additionalMaterialGroup4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.additionalMaterialGroup4))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.additionalMaterialGroup4, to: StringValue.of(optional: value))
        }
    }

    open class var additionalMaterialGroup5: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.additionalMaterialGroup5_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.additionalMaterialGroup5_ = value
            }
        }
    }

    open var additionalMaterialGroup5: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.additionalMaterialGroup5))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.additionalMaterialGroup5, to: StringValue.of(optional: value))
        }
    }

    open class var alternateProductNumber: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.alternateProductNumber_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.alternateProductNumber_ = value
            }
        }
    }

    open var alternateProductNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.alternateProductNumber))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.alternateProductNumber, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [AInbDeliveryItemType] {
        return ArrayConverter.convert(from.toArray(), [AInbDeliveryItemType]())
    }

    open class var baseUnit: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.baseUnit_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.baseUnit_ = value
            }
        }
    }

    open var baseUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.baseUnit))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.baseUnit, to: StringValue.of(optional: value))
        }
    }

    open class var batch: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.batch_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.batch_ = value
            }
        }
    }

    open var batch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.batch))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.batch, to: StringValue.of(optional: value))
        }
    }

    open class var batchBySupplier: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.batchBySupplier_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.batchBySupplier_ = value
            }
        }
    }

    open var batchBySupplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.batchBySupplier))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.batchBySupplier, to: StringValue.of(optional: value))
        }
    }

    open class var batchClassification: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.batchClassification_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.batchClassification_ = value
            }
        }
    }

    open var batchClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.batchClassification))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.batchClassification, to: StringValue.of(optional: value))
        }
    }

    open class var bomExplosion: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.bomExplosion_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.bomExplosion_ = value
            }
        }
    }

    open var bomExplosion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.bomExplosion))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.bomExplosion, to: StringValue.of(optional: value))
        }
    }

    open class var businessArea: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.businessArea_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.businessArea_ = value
            }
        }
    }

    open var businessArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.businessArea))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.businessArea, to: StringValue.of(optional: value))
        }
    }

    open class var consumptionPosting: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.consumptionPosting_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.consumptionPosting_ = value
            }
        }
    }

    open var consumptionPosting: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.consumptionPosting))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.consumptionPosting, to: StringValue.of(optional: value))
        }
    }

    open class var controllingArea: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.controllingArea_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.controllingArea_ = value
            }
        }
    }

    open var controllingArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.controllingArea))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.controllingArea, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> AInbDeliveryItemType {
        return CastRequired<AInbDeliveryItemType>.from(self.copyEntity())
    }

    open class var costCenter: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.costCenter_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.costCenter_ = value
            }
        }
    }

    open var costCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.costCenter))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.costCenter, to: StringValue.of(optional: value))
        }
    }

    open class var createdByUser: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.createdByUser_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.createdByUser_ = value
            }
        }
    }

    open var createdByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.createdByUser))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.createdByUser, to: StringValue.of(optional: value))
        }
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryItemType.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.creationDate, to: value)
        }
    }

    open class var creationTime: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.creationTime_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.creationTime_ = value
            }
        }
    }

    open var creationTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AInbDeliveryItemType.creationTime))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.creationTime, to: value)
        }
    }

    open class var custEngineeringChgStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.custEngineeringChgStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.custEngineeringChgStatus_ = value
            }
        }
    }

    open var custEngineeringChgStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.custEngineeringChgStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.custEngineeringChgStatus, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItem: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.deliveryDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.deliveryDocumentItem_ = value
            }
        }
    }

    open var deliveryDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.deliveryDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.deliveryDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItemCategory: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.deliveryDocumentItemCategory_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.deliveryDocumentItemCategory_ = value
            }
        }
    }

    open var deliveryDocumentItemCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.deliveryDocumentItemCategory))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.deliveryDocumentItemCategory, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItemText: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.deliveryDocumentItemText_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.deliveryDocumentItemText_ = value
            }
        }
    }

    open var deliveryDocumentItemText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.deliveryDocumentItemText))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.deliveryDocumentItemText, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryGroup: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.deliveryGroup_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.deliveryGroup_ = value
            }
        }
    }

    open var deliveryGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.deliveryGroup))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.deliveryGroup, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryQuantityUnit: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.deliveryQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.deliveryQuantityUnit_ = value
            }
        }
    }

    open var deliveryQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.deliveryQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.deliveryQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryRelatedBillingStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.deliveryRelatedBillingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.deliveryRelatedBillingStatus_ = value
            }
        }
    }

    open var deliveryRelatedBillingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.deliveryRelatedBillingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.deliveryRelatedBillingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryToBaseQuantityDnmntr: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.deliveryToBaseQuantityDnmntr_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.deliveryToBaseQuantityDnmntr_ = value
            }
        }
    }

    open var deliveryToBaseQuantityDnmntr: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: AInbDeliveryItemType.deliveryToBaseQuantityDnmntr))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.deliveryToBaseQuantityDnmntr, to: IntegerValue.of(optional: value))
        }
    }

    open class var deliveryToBaseQuantityNmrtr: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.deliveryToBaseQuantityNmrtr_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.deliveryToBaseQuantityNmrtr_ = value
            }
        }
    }

    open var deliveryToBaseQuantityNmrtr: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: AInbDeliveryItemType.deliveryToBaseQuantityNmrtr))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.deliveryToBaseQuantityNmrtr, to: IntegerValue.of(optional: value))
        }
    }

    open class var deliveryVersion: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.deliveryVersion_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.deliveryVersion_ = value
            }
        }
    }

    open var deliveryVersion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.deliveryVersion))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.deliveryVersion, to: StringValue.of(optional: value))
        }
    }

    open class var departmentClassificationByCust: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.departmentClassificationByCust_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.departmentClassificationByCust_ = value
            }
        }
    }

    open var departmentClassificationByCust: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.departmentClassificationByCust))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.departmentClassificationByCust, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var division: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.division_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.division_ = value
            }
        }
    }

    open var division: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.division))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.division, to: StringValue.of(optional: value))
        }
    }

    open class var fixedShipgProcgDurationInDays: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.fixedShipgProcgDurationInDays_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.fixedShipgProcgDurationInDays_ = value
            }
        }
    }

    open var fixedShipgProcgDurationInDays: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryItemType.fixedShipgProcgDurationInDays))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.fixedShipgProcgDurationInDays, to: DecimalValue.of(optional: value))
        }
    }

    open class var glAccount: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.glAccount_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.glAccount_ = value
            }
        }
    }

    open var glAccount: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.glAccount))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.glAccount, to: StringValue.of(optional: value))
        }
    }

    open class var goodsMovementReasonCode: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.goodsMovementReasonCode_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.goodsMovementReasonCode_ = value
            }
        }
    }

    open var goodsMovementReasonCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.goodsMovementReasonCode))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.goodsMovementReasonCode, to: StringValue.of(optional: value))
        }
    }

    open class var goodsMovementStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.goodsMovementStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.goodsMovementStatus_ = value
            }
        }
    }

    open var goodsMovementStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.goodsMovementStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.goodsMovementStatus, to: StringValue.of(optional: value))
        }
    }

    open class var goodsMovementType: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.goodsMovementType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.goodsMovementType_ = value
            }
        }
    }

    open var goodsMovementType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.goodsMovementType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.goodsMovementType, to: StringValue.of(optional: value))
        }
    }

    open class var higherLevelItem: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.higherLevelItem_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.higherLevelItem_ = value
            }
        }
    }

    open var higherLevelItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.higherLevelItem))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.higherLevelItem, to: StringValue.of(optional: value))
        }
    }

    open class var inspectionLot: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.inspectionLot_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.inspectionLot_ = value
            }
        }
    }

    open var inspectionLot: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.inspectionLot))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.inspectionLot, to: StringValue.of(optional: value))
        }
    }

    open class var inspectionPartialLot: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.inspectionPartialLot_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.inspectionPartialLot_ = value
            }
        }
    }

    open var inspectionPartialLot: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.inspectionPartialLot))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.inspectionPartialLot, to: StringValue.of(optional: value))
        }
    }

    open class var intercompanyBillingStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.intercompanyBillingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.intercompanyBillingStatus_ = value
            }
        }
    }

    open var intercompanyBillingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.intercompanyBillingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.intercompanyBillingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var internationalArticleNumber: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.internationalArticleNumber_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.internationalArticleNumber_ = value
            }
        }
    }

    open var internationalArticleNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.internationalArticleNumber))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.internationalArticleNumber, to: StringValue.of(optional: value))
        }
    }

    open class var inventorySpecialStockType: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.inventorySpecialStockType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.inventorySpecialStockType_ = value
            }
        }
    }

    open var inventorySpecialStockType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.inventorySpecialStockType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.inventorySpecialStockType, to: StringValue.of(optional: value))
        }
    }

    open class var inventoryValuationType: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.inventoryValuationType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.inventoryValuationType_ = value
            }
        }
    }

    open var inventoryValuationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.inventoryValuationType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.inventoryValuationType, to: StringValue.of(optional: value))
        }
    }

    open class var isCompletelyDelivered: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.isCompletelyDelivered_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.isCompletelyDelivered_ = value
            }
        }
    }

    open var isCompletelyDelivered: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AInbDeliveryItemType.isCompletelyDelivered))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.isCompletelyDelivered, to: BooleanValue.of(optional: value))
        }
    }

    open class var isNotGoodsMovementsRelevant: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.isNotGoodsMovementsRelevant_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.isNotGoodsMovementsRelevant_ = value
            }
        }
    }

    open var isNotGoodsMovementsRelevant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.isNotGoodsMovementsRelevant))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.isNotGoodsMovementsRelevant, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var isSeparateValuation: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.isSeparateValuation_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.isSeparateValuation_ = value
            }
        }
    }

    open var isSeparateValuation: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AInbDeliveryItemType.isSeparateValuation))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.isSeparateValuation, to: BooleanValue.of(optional: value))
        }
    }

    open class var issgOrRcvgBatch: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.issgOrRcvgBatch_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.issgOrRcvgBatch_ = value
            }
        }
    }

    open var issgOrRcvgBatch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.issgOrRcvgBatch))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.issgOrRcvgBatch, to: StringValue.of(optional: value))
        }
    }

    open class var issgOrRcvgMaterial: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.issgOrRcvgMaterial_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.issgOrRcvgMaterial_ = value
            }
        }
    }

    open var issgOrRcvgMaterial: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.issgOrRcvgMaterial))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.issgOrRcvgMaterial, to: StringValue.of(optional: value))
        }
    }

    open class var issgOrRcvgSpclStockInd: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.issgOrRcvgSpclStockInd_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.issgOrRcvgSpclStockInd_ = value
            }
        }
    }

    open var issgOrRcvgSpclStockInd: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.issgOrRcvgSpclStockInd))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.issgOrRcvgSpclStockInd, to: StringValue.of(optional: value))
        }
    }

    open class var issgOrRcvgStockCategory: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.issgOrRcvgStockCategory_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.issgOrRcvgStockCategory_ = value
            }
        }
    }

    open var issgOrRcvgStockCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.issgOrRcvgStockCategory))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.issgOrRcvgStockCategory, to: StringValue.of(optional: value))
        }
    }

    open class var issgOrRcvgValuationType: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.issgOrRcvgValuationType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.issgOrRcvgValuationType_ = value
            }
        }
    }

    open var issgOrRcvgValuationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.issgOrRcvgValuationType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.issgOrRcvgValuationType, to: StringValue.of(optional: value))
        }
    }

    open class var issuingOrReceivingPlant: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.issuingOrReceivingPlant_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.issuingOrReceivingPlant_ = value
            }
        }
    }

    open var issuingOrReceivingPlant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.issuingOrReceivingPlant))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.issuingOrReceivingPlant, to: StringValue.of(optional: value))
        }
    }

    open class var issuingOrReceivingStorageLoc: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.issuingOrReceivingStorageLoc_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.issuingOrReceivingStorageLoc_ = value
            }
        }
    }

    open var issuingOrReceivingStorageLoc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.issuingOrReceivingStorageLoc))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.issuingOrReceivingStorageLoc, to: StringValue.of(optional: value))
        }
    }

    open class var itemBillingBlockReason: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemBillingBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemBillingBlockReason_ = value
            }
        }
    }

    open var itemBillingBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemBillingBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemBillingBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var itemBillingIncompletionStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemBillingIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemBillingIncompletionStatus_ = value
            }
        }
    }

    open var itemBillingIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemBillingIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemBillingIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var itemDeliveryIncompletionStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemDeliveryIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemDeliveryIncompletionStatus_ = value
            }
        }
    }

    open var itemDeliveryIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemDeliveryIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemDeliveryIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var itemGdsMvtIncompletionSts: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemGdsMvtIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemGdsMvtIncompletionSts_ = value
            }
        }
    }

    open var itemGdsMvtIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemGdsMvtIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemGdsMvtIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var itemGeneralIncompletionStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemGeneralIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemGeneralIncompletionStatus_ = value
            }
        }
    }

    open var itemGeneralIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemGeneralIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemGeneralIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var itemGrossWeight: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemGrossWeight_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemGrossWeight_ = value
            }
        }
    }

    open var itemGrossWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemGrossWeight))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemGrossWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemIsBillingRelevant: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemIsBillingRelevant_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemIsBillingRelevant_ = value
            }
        }
    }

    open var itemIsBillingRelevant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemIsBillingRelevant))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemIsBillingRelevant, to: StringValue.of(optional: value))
        }
    }

    open class var itemNetWeight: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemNetWeight_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemNetWeight_ = value
            }
        }
    }

    open var itemNetWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemNetWeight))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemNetWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemPackingIncompletionStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemPackingIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemPackingIncompletionStatus_ = value
            }
        }
    }

    open var itemPackingIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemPackingIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemPackingIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var itemPickingIncompletionStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemPickingIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemPickingIncompletionStatus_ = value
            }
        }
    }

    open var itemPickingIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemPickingIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemPickingIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var itemVolume: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemVolume_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemVolume_ = value
            }
        }
    }

    open var itemVolume: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemVolume))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemVolume, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemVolumeUnit: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemVolumeUnit_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemVolumeUnit_ = value
            }
        }
    }

    open var itemVolumeUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemVolumeUnit))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemVolumeUnit, to: StringValue.of(optional: value))
        }
    }

    open class var itemWeightUnit: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.itemWeightUnit_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.itemWeightUnit_ = value
            }
        }
    }

    open var itemWeightUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.itemWeightUnit))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.itemWeightUnit, to: StringValue.of(optional: value))
        }
    }

    open class func key(deliveryDocument: String?, deliveryDocumentItem: String?) -> EntityKey {
        return EntityKey().with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem))
    }

    open class var lastChangeDate: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.lastChangeDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.lastChangeDate_ = value
            }
        }
    }

    open var lastChangeDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryItemType.lastChangeDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.lastChangeDate, to: value)
        }
    }

    open class var loadingGroup: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.loadingGroup_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.loadingGroup_ = value
            }
        }
    }

    open var loadingGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.loadingGroup))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.loadingGroup, to: StringValue.of(optional: value))
        }
    }

    open class var manufactureDate: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.manufactureDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.manufactureDate_ = value
            }
        }
    }

    open var manufactureDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryItemType.manufactureDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.manufactureDate, to: value)
        }
    }

    open class var material: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.material_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.material_ = value
            }
        }
    }

    open var material: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.material))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.material, to: StringValue.of(optional: value))
        }
    }

    open class var materialByCustomer: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.materialByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.materialByCustomer_ = value
            }
        }
    }

    open var materialByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.materialByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.materialByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var materialFreightGroup: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.materialFreightGroup_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.materialFreightGroup_ = value
            }
        }
    }

    open var materialFreightGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.materialFreightGroup))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.materialFreightGroup, to: StringValue.of(optional: value))
        }
    }

    open class var materialGroup: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.materialGroup_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.materialGroup_ = value
            }
        }
    }

    open var materialGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.materialGroup))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.materialGroup, to: StringValue.of(optional: value))
        }
    }

    open class var materialIsBatchManaged: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.materialIsBatchManaged_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.materialIsBatchManaged_ = value
            }
        }
    }

    open var materialIsBatchManaged: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AInbDeliveryItemType.materialIsBatchManaged))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.materialIsBatchManaged, to: BooleanValue.of(optional: value))
        }
    }

    open class var materialIsIntBatchManaged: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.materialIsIntBatchManaged_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.materialIsIntBatchManaged_ = value
            }
        }
    }

    open var materialIsIntBatchManaged: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AInbDeliveryItemType.materialIsIntBatchManaged))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.materialIsIntBatchManaged, to: BooleanValue.of(optional: value))
        }
    }

    open class var numberOfSerialNumbers: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.numberOfSerialNumbers_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.numberOfSerialNumbers_ = value
            }
        }
    }

    open var numberOfSerialNumbers: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: AInbDeliveryItemType.numberOfSerialNumbers))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.numberOfSerialNumbers, to: IntValue.of(optional: value))
        }
    }

    open var old: AInbDeliveryItemType {
        return CastRequired<AInbDeliveryItemType>.from(self.oldEntity)
    }

    open class var orderID: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.orderID_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.orderID_ = value
            }
        }
    }

    open var orderID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.orderID))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.orderID, to: StringValue.of(optional: value))
        }
    }

    open class var orderItem: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.orderItem_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.orderItem_ = value
            }
        }
    }

    open var orderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.orderItem))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.orderItem, to: StringValue.of(optional: value))
        }
    }

    open class var originalDeliveryQuantity: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.originalDeliveryQuantity_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.originalDeliveryQuantity_ = value
            }
        }
    }

    open var originalDeliveryQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryItemType.originalDeliveryQuantity))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.originalDeliveryQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var originallyRequestedMaterial: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.originallyRequestedMaterial_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.originallyRequestedMaterial_ = value
            }
        }
    }

    open var originallyRequestedMaterial: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.originallyRequestedMaterial))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.originallyRequestedMaterial, to: StringValue.of(optional: value))
        }
    }

    open class var overdelivTolrtdLmtRatioInPct: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.overdelivTolrtdLmtRatioInPct_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.overdelivTolrtdLmtRatioInPct_ = value
            }
        }
    }

    open var overdelivTolrtdLmtRatioInPct: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryItemType.overdelivTolrtdLmtRatioInPct))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.overdelivTolrtdLmtRatioInPct, to: DecimalValue.of(optional: value))
        }
    }

    open class var packingStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.packingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.packingStatus_ = value
            }
        }
    }

    open var packingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.packingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.packingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var partialDeliveryIsAllowed: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.partialDeliveryIsAllowed_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.partialDeliveryIsAllowed_ = value
            }
        }
    }

    open var partialDeliveryIsAllowed: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.partialDeliveryIsAllowed))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.partialDeliveryIsAllowed, to: StringValue.of(optional: value))
        }
    }

    open class var paymentGuaranteeForm: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.paymentGuaranteeForm_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.paymentGuaranteeForm_ = value
            }
        }
    }

    open var paymentGuaranteeForm: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.paymentGuaranteeForm))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.paymentGuaranteeForm, to: StringValue.of(optional: value))
        }
    }

    open class var pickingConfirmationStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.pickingConfirmationStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.pickingConfirmationStatus_ = value
            }
        }
    }

    open var pickingConfirmationStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.pickingConfirmationStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.pickingConfirmationStatus, to: StringValue.of(optional: value))
        }
    }

    open class var pickingControl: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.pickingControl_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.pickingControl_ = value
            }
        }
    }

    open var pickingControl: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.pickingControl))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.pickingControl, to: StringValue.of(optional: value))
        }
    }

    open class var pickingStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.pickingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.pickingStatus_ = value
            }
        }
    }

    open var pickingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.pickingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.pickingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var plant: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.plant_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.plant_ = value
            }
        }
    }

    open var plant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.plant))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.plant, to: StringValue.of(optional: value))
        }
    }

    open class var primaryPostingSwitch: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.primaryPostingSwitch_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.primaryPostingSwitch_ = value
            }
        }
    }

    open var primaryPostingSwitch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.primaryPostingSwitch))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.primaryPostingSwitch, to: StringValue.of(optional: value))
        }
    }

    open class var productAvailabilityDate: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.productAvailabilityDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.productAvailabilityDate_ = value
            }
        }
    }

    open var productAvailabilityDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryItemType.productAvailabilityDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.productAvailabilityDate, to: value)
        }
    }

    open class var productAvailabilityTime: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.productAvailabilityTime_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.productAvailabilityTime_ = value
            }
        }
    }

    open var productAvailabilityTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AInbDeliveryItemType.productAvailabilityTime))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.productAvailabilityTime, to: value)
        }
    }

    open class var productConfiguration: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.productConfiguration_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.productConfiguration_ = value
            }
        }
    }

    open var productConfiguration: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.productConfiguration))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.productConfiguration, to: StringValue.of(optional: value))
        }
    }

    open class var productHierarchyNode: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.productHierarchyNode_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.productHierarchyNode_ = value
            }
        }
    }

    open var productHierarchyNode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.productHierarchyNode))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.productHierarchyNode, to: StringValue.of(optional: value))
        }
    }

    open class var profitCenter: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.profitCenter_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.profitCenter_ = value
            }
        }
    }

    open var profitCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.profitCenter))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.profitCenter, to: StringValue.of(optional: value))
        }
    }

    open class var profitabilitySegment: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.profitabilitySegment_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.profitabilitySegment_ = value
            }
        }
    }

    open var profitabilitySegment: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.profitabilitySegment))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.profitabilitySegment, to: StringValue.of(optional: value))
        }
    }

    open class var proofOfDeliveryRelevanceCode: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.proofOfDeliveryRelevanceCode_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.proofOfDeliveryRelevanceCode_ = value
            }
        }
    }

    open var proofOfDeliveryRelevanceCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.proofOfDeliveryRelevanceCode))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.proofOfDeliveryRelevanceCode, to: StringValue.of(optional: value))
        }
    }

    open class var proofOfDeliveryStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.proofOfDeliveryStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.proofOfDeliveryStatus_ = value
            }
        }
    }

    open var proofOfDeliveryStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.proofOfDeliveryStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.proofOfDeliveryStatus, to: StringValue.of(optional: value))
        }
    }

    open class var quantityIsFixed: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.quantityIsFixed_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.quantityIsFixed_ = value
            }
        }
    }

    open var quantityIsFixed: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AInbDeliveryItemType.quantityIsFixed))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.quantityIsFixed, to: BooleanValue.of(optional: value))
        }
    }

    open class var receivingPoint: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.receivingPoint_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.receivingPoint_ = value
            }
        }
    }

    open var receivingPoint: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.receivingPoint))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.receivingPoint, to: StringValue.of(optional: value))
        }
    }

    open class var referenceDocumentLogicalSystem: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.referenceDocumentLogicalSystem_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.referenceDocumentLogicalSystem_ = value
            }
        }
    }

    open var referenceDocumentLogicalSystem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.referenceDocumentLogicalSystem))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.referenceDocumentLogicalSystem, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocument: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.referenceSDDocument_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.referenceSDDocument_ = value
            }
        }
    }

    open var referenceSDDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.referenceSDDocument))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.referenceSDDocument, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocumentCategory: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.referenceSDDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.referenceSDDocumentCategory_ = value
            }
        }
    }

    open var referenceSDDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.referenceSDDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.referenceSDDocumentCategory, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocumentItem: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.referenceSDDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.referenceSDDocumentItem_ = value
            }
        }
    }

    open var referenceSDDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.referenceSDDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.referenceSDDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var requirementSegment: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.requirementSegment_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.requirementSegment_ = value
            }
        }
    }

    open var requirementSegment: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.requirementSegment))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.requirementSegment, to: StringValue.of(optional: value))
        }
    }

    open class var retailPromotion: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.retailPromotion_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.retailPromotion_ = value
            }
        }
    }

    open var retailPromotion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.retailPromotion))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.retailPromotion, to: StringValue.of(optional: value))
        }
    }

    open class var salesDocumentItemType: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.salesDocumentItemType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.salesDocumentItemType_ = value
            }
        }
    }

    open var salesDocumentItemType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.salesDocumentItemType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.salesDocumentItemType, to: StringValue.of(optional: value))
        }
    }

    open class var salesGroup: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.salesGroup_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.salesGroup_ = value
            }
        }
    }

    open var salesGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.salesGroup))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.salesGroup, to: StringValue.of(optional: value))
        }
    }

    open class var salesOffice: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.salesOffice_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.salesOffice_ = value
            }
        }
    }

    open var salesOffice: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.salesOffice))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.salesOffice, to: StringValue.of(optional: value))
        }
    }

    open class var sdDocumentCategory: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.sdDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.sdDocumentCategory_ = value
            }
        }
    }

    open var sdDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.sdDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.sdDocumentCategory, to: StringValue.of(optional: value))
        }
    }

    open class var sdProcessStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.sdProcessStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.sdProcessStatus_ = value
            }
        }
    }

    open var sdProcessStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.sdProcessStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.sdProcessStatus, to: StringValue.of(optional: value))
        }
    }

    open class var shelfLifeExpirationDate: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.shelfLifeExpirationDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.shelfLifeExpirationDate_ = value
            }
        }
    }

    open var shelfLifeExpirationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryItemType.shelfLifeExpirationDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.shelfLifeExpirationDate, to: value)
        }
    }

    open class var statisticsDate: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.statisticsDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.statisticsDate_ = value
            }
        }
    }

    open var statisticsDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryItemType.statisticsDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.statisticsDate, to: value)
        }
    }

    open class var stockSegment: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.stockSegment_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.stockSegment_ = value
            }
        }
    }

    open var stockSegment: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.stockSegment))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.stockSegment, to: StringValue.of(optional: value))
        }
    }

    open class var stockType: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.stockType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.stockType_ = value
            }
        }
    }

    open var stockType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.stockType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.stockType, to: StringValue.of(optional: value))
        }
    }

    open class var storageBin: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.storageBin_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.storageBin_ = value
            }
        }
    }

    open var storageBin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.storageBin))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.storageBin, to: StringValue.of(optional: value))
        }
    }

    open class var storageLocation: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.storageLocation_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.storageLocation_ = value
            }
        }
    }

    open var storageLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.storageLocation))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.storageLocation, to: StringValue.of(optional: value))
        }
    }

    open class var storageType: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.storageType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.storageType_ = value
            }
        }
    }

    open var storageType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.storageType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.storageType, to: StringValue.of(optional: value))
        }
    }

    open class var subsequentMovementType: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.subsequentMovementType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.subsequentMovementType_ = value
            }
        }
    }

    open var subsequentMovementType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.subsequentMovementType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.subsequentMovementType, to: StringValue.of(optional: value))
        }
    }

    open class var transportationGroup: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.transportationGroup_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.transportationGroup_ = value
            }
        }
    }

    open var transportationGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.transportationGroup))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.transportationGroup, to: StringValue.of(optional: value))
        }
    }

    open class var underdelivTolrtdLmtRatioInPct: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.underdelivTolrtdLmtRatioInPct_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.underdelivTolrtdLmtRatioInPct_ = value
            }
        }
    }

    open var underdelivTolrtdLmtRatioInPct: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryItemType.underdelivTolrtdLmtRatioInPct))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.underdelivTolrtdLmtRatioInPct, to: DecimalValue.of(optional: value))
        }
    }

    open class var unlimitedOverdeliveryIsAllowed: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.unlimitedOverdeliveryIsAllowed_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.unlimitedOverdeliveryIsAllowed_ = value
            }
        }
    }

    open var unlimitedOverdeliveryIsAllowed: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AInbDeliveryItemType.unlimitedOverdeliveryIsAllowed))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.unlimitedOverdeliveryIsAllowed, to: BooleanValue.of(optional: value))
        }
    }

    open class var varblShipgProcgDurationInDays: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.varblShipgProcgDurationInDays_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.varblShipgProcgDurationInDays_ = value
            }
        }
    }

    open var varblShipgProcgDurationInDays: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryItemType.varblShipgProcgDurationInDays))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.varblShipgProcgDurationInDays, to: DecimalValue.of(optional: value))
        }
    }

    open class var warehouse: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.warehouse_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.warehouse_ = value
            }
        }
    }

    open var warehouse: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.warehouse))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.warehouse, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseActivityStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.warehouseActivityStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.warehouseActivityStatus_ = value
            }
        }
    }

    open var warehouseActivityStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.warehouseActivityStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.warehouseActivityStatus, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseStagingArea: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.warehouseStagingArea_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.warehouseStagingArea_ = value
            }
        }
    }

    open var warehouseStagingArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.warehouseStagingArea))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.warehouseStagingArea, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseStockCategory: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.warehouseStockCategory_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.warehouseStockCategory_ = value
            }
        }
    }

    open var warehouseStockCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.warehouseStockCategory))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.warehouseStockCategory, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseStorageBin: Property {
        get {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                return AInbDeliveryItemType.warehouseStorageBin_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryItemType.self)
            defer { objc_sync_exit(AInbDeliveryItemType.self) }
            do {
                AInbDeliveryItemType.warehouseStorageBin_ = value
            }
        }
    }

    open var warehouseStorageBin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryItemType.warehouseStorageBin))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryItemType.warehouseStorageBin, to: StringValue.of(optional: value))
        }
    }
}
