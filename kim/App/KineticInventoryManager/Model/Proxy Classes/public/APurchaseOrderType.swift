// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class APurchaseOrderType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var purchaseOrder_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchaseOrder")

    private static var companyCode_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CompanyCode")

    private static var purchaseOrderType_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchaseOrderType")

    private static var purchasingDocumentDeletionCode_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchasingDocumentDeletionCode")

    private static var purchasingProcessingStatus_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchasingProcessingStatus")

    private static var createdByUser_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CreatedByUser")

    private static var creationDate_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CreationDate")

    private static var lastChangeDateTime_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "LastChangeDateTime")

    private static var supplier_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "Supplier")

    private static var purchaseOrderSubtype_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchaseOrderSubtype")

    private static var language_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "Language")

    private static var paymentTerms_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PaymentTerms")

    private static var cashDiscount1Days_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CashDiscount1Days")

    private static var cashDiscount2Days_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CashDiscount2Days")

    private static var netPaymentDays_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "NetPaymentDays")

    private static var cashDiscount1Percent_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CashDiscount1Percent")

    private static var cashDiscount2Percent_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CashDiscount2Percent")

    private static var purchasingOrganization_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchasingOrganization")

    private static var purchasingDocumentOrigin_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchasingDocumentOrigin")

    private static var purchasingGroup_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchasingGroup")

    private static var purchaseOrderDate_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchaseOrderDate")

    private static var documentCurrency_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "DocumentCurrency")

    private static var exchangeRate_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "ExchangeRate")

    private static var validityStartDate_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "ValidityStartDate")

    private static var validityEndDate_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "ValidityEndDate")

    private static var supplierQuotationExternalID_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "SupplierQuotationExternalID")

    private static var supplierRespSalesPersonName_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "SupplierRespSalesPersonName")

    private static var supplierPhoneNumber_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "SupplierPhoneNumber")

    private static var supplyingSupplier_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "SupplyingSupplier")

    private static var supplyingPlant_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "SupplyingPlant")

    private static var incotermsClassification_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "IncotermsClassification")

    private static var correspncExternalReference_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CorrespncExternalReference")

    private static var correspncInternalReference_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CorrespncInternalReference")

    private static var invoicingParty_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "InvoicingParty")

    private static var releaseIsNotCompleted_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "ReleaseIsNotCompleted")

    private static var purchasingCompletenessStatus_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchasingCompletenessStatus")

    private static var incotermsVersion_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "IncotermsVersion")

    private static var incotermsLocation1_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "IncotermsLocation1")

    private static var incotermsLocation2_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "IncotermsLocation2")

    private static var manualSupplierAddressID_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "ManualSupplierAddressID")

    private static var isEndOfPurposeBlocked_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "IsEndOfPurposeBlocked")

    private static var addressCityName_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressCityName")

    private static var addressFaxNumber_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressFaxNumber")

    private static var addressHouseNumber_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressHouseNumber")

    private static var addressName_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressName")

    private static var addressPostalCode_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressPostalCode")

    private static var addressStreetName_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressStreetName")

    private static var addressPhoneNumber_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressPhoneNumber")

    private static var addressRegion_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressRegion")

    private static var addressCountry_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressCountry")

    private static var addressCorrespondenceLanguage_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressCorrespondenceLanguage")

    private static var toPurchaseOrderItem_: Property = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "to_PurchaseOrderItem")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aPurchaseOrderType)
    }

    open class var addressCityName: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.addressCityName_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.addressCityName_ = value
            }
        }
    }

    open var addressCityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.addressCityName))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.addressCityName, to: StringValue.of(optional: value))
        }
    }

    open class var addressCorrespondenceLanguage: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.addressCorrespondenceLanguage_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.addressCorrespondenceLanguage_ = value
            }
        }
    }

    open var addressCorrespondenceLanguage: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.addressCorrespondenceLanguage))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.addressCorrespondenceLanguage, to: StringValue.of(optional: value))
        }
    }

    open class var addressCountry: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.addressCountry_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.addressCountry_ = value
            }
        }
    }

    open var addressCountry: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.addressCountry))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.addressCountry, to: StringValue.of(optional: value))
        }
    }

    open class var addressFaxNumber: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.addressFaxNumber_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.addressFaxNumber_ = value
            }
        }
    }

    open var addressFaxNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.addressFaxNumber))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.addressFaxNumber, to: StringValue.of(optional: value))
        }
    }

    open class var addressHouseNumber: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.addressHouseNumber_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.addressHouseNumber_ = value
            }
        }
    }

    open var addressHouseNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.addressHouseNumber))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.addressHouseNumber, to: StringValue.of(optional: value))
        }
    }

    open class var addressName: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.addressName_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.addressName_ = value
            }
        }
    }

    open var addressName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.addressName))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.addressName, to: StringValue.of(optional: value))
        }
    }

    open class var addressPhoneNumber: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.addressPhoneNumber_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.addressPhoneNumber_ = value
            }
        }
    }

    open var addressPhoneNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.addressPhoneNumber))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.addressPhoneNumber, to: StringValue.of(optional: value))
        }
    }

    open class var addressPostalCode: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.addressPostalCode_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.addressPostalCode_ = value
            }
        }
    }

    open var addressPostalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.addressPostalCode))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.addressPostalCode, to: StringValue.of(optional: value))
        }
    }

    open class var addressRegion: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.addressRegion_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.addressRegion_ = value
            }
        }
    }

    open var addressRegion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.addressRegion))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.addressRegion, to: StringValue.of(optional: value))
        }
    }

    open class var addressStreetName: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.addressStreetName_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.addressStreetName_ = value
            }
        }
    }

    open var addressStreetName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.addressStreetName))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.addressStreetName, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [APurchaseOrderType] {
        return ArrayConverter.convert(from.toArray(), [APurchaseOrderType]())
    }

    open class var cashDiscount1Days: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.cashDiscount1Days_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.cashDiscount1Days_ = value
            }
        }
    }

    open var cashDiscount1Days: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: APurchaseOrderType.cashDiscount1Days))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.cashDiscount1Days, to: IntegerValue.of(optional: value))
        }
    }

    open class var cashDiscount1Percent: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.cashDiscount1Percent_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.cashDiscount1Percent_ = value
            }
        }
    }

    open var cashDiscount1Percent: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurchaseOrderType.cashDiscount1Percent))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.cashDiscount1Percent, to: DecimalValue.of(optional: value))
        }
    }

    open class var cashDiscount2Days: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.cashDiscount2Days_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.cashDiscount2Days_ = value
            }
        }
    }

    open var cashDiscount2Days: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: APurchaseOrderType.cashDiscount2Days))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.cashDiscount2Days, to: IntegerValue.of(optional: value))
        }
    }

    open class var cashDiscount2Percent: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.cashDiscount2Percent_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.cashDiscount2Percent_ = value
            }
        }
    }

    open var cashDiscount2Percent: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurchaseOrderType.cashDiscount2Percent))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.cashDiscount2Percent, to: DecimalValue.of(optional: value))
        }
    }

    open class var companyCode: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.companyCode_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.companyCode_ = value
            }
        }
    }

    open var companyCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.companyCode))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.companyCode, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> APurchaseOrderType {
        return CastRequired<APurchaseOrderType>.from(self.copyEntity())
    }

    open class var correspncExternalReference: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.correspncExternalReference_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.correspncExternalReference_ = value
            }
        }
    }

    open var correspncExternalReference: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.correspncExternalReference))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.correspncExternalReference, to: StringValue.of(optional: value))
        }
    }

    open class var correspncInternalReference: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.correspncInternalReference_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.correspncInternalReference_ = value
            }
        }
    }

    open var correspncInternalReference: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.correspncInternalReference))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.correspncInternalReference, to: StringValue.of(optional: value))
        }
    }

    open class var createdByUser: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.createdByUser_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.createdByUser_ = value
            }
        }
    }

    open var createdByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.createdByUser))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.createdByUser, to: StringValue.of(optional: value))
        }
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: APurchaseOrderType.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.creationDate, to: value)
        }
    }

    open class var documentCurrency: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.documentCurrency_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.documentCurrency_ = value
            }
        }
    }

    open var documentCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.documentCurrency))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.documentCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var exchangeRate: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.exchangeRate_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.exchangeRate_ = value
            }
        }
    }

    open var exchangeRate: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.exchangeRate))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.exchangeRate, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsClassification: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.incotermsClassification_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.incotermsClassification_ = value
            }
        }
    }

    open var incotermsClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.incotermsClassification))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.incotermsClassification, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation1: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.incotermsLocation1_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.incotermsLocation1_ = value
            }
        }
    }

    open var incotermsLocation1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.incotermsLocation1))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.incotermsLocation1, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation2: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.incotermsLocation2_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.incotermsLocation2_ = value
            }
        }
    }

    open var incotermsLocation2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.incotermsLocation2))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.incotermsLocation2, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsVersion: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.incotermsVersion_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.incotermsVersion_ = value
            }
        }
    }

    open var incotermsVersion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.incotermsVersion))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.incotermsVersion, to: StringValue.of(optional: value))
        }
    }

    open class var invoicingParty: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.invoicingParty_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.invoicingParty_ = value
            }
        }
    }

    open var invoicingParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.invoicingParty))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.invoicingParty, to: StringValue.of(optional: value))
        }
    }

    open class var isEndOfPurposeBlocked: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.isEndOfPurposeBlocked_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.isEndOfPurposeBlocked_ = value
            }
        }
    }

    open var isEndOfPurposeBlocked: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.isEndOfPurposeBlocked))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.isEndOfPurposeBlocked, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(purchaseOrder: String?) -> EntityKey {
        return EntityKey().with(name: "PurchaseOrder", value: StringValue.of(optional: purchaseOrder))
    }

    open class var language: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.language_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.language_ = value
            }
        }
    }

    open var language: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.language))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.language, to: StringValue.of(optional: value))
        }
    }

    open class var lastChangeDateTime: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.lastChangeDateTime_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.lastChangeDateTime_ = value
            }
        }
    }

    open var lastChangeDateTime: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: APurchaseOrderType.lastChangeDateTime))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.lastChangeDateTime, to: value)
        }
    }

    open class var manualSupplierAddressID: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.manualSupplierAddressID_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.manualSupplierAddressID_ = value
            }
        }
    }

    open var manualSupplierAddressID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.manualSupplierAddressID))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.manualSupplierAddressID, to: StringValue.of(optional: value))
        }
    }

    open class var netPaymentDays: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.netPaymentDays_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.netPaymentDays_ = value
            }
        }
    }

    open var netPaymentDays: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: APurchaseOrderType.netPaymentDays))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.netPaymentDays, to: IntegerValue.of(optional: value))
        }
    }

    open var old: APurchaseOrderType {
        return CastRequired<APurchaseOrderType>.from(self.oldEntity)
    }

    open class var paymentTerms: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.paymentTerms_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.paymentTerms_ = value
            }
        }
    }

    open var paymentTerms: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.paymentTerms))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.paymentTerms, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrder: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.purchaseOrder_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.purchaseOrder_ = value
            }
        }
    }

    open var purchaseOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.purchaseOrder))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.purchaseOrder, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderDate: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.purchaseOrderDate_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.purchaseOrderDate_ = value
            }
        }
    }

    open var purchaseOrderDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: APurchaseOrderType.purchaseOrderDate))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.purchaseOrderDate, to: value)
        }
    }

    open class var purchaseOrderSubtype: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.purchaseOrderSubtype_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.purchaseOrderSubtype_ = value
            }
        }
    }

    open var purchaseOrderSubtype: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.purchaseOrderSubtype))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.purchaseOrderSubtype, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderType: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.purchaseOrderType_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.purchaseOrderType_ = value
            }
        }
    }

    open var purchaseOrderType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.purchaseOrderType))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.purchaseOrderType, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingCompletenessStatus: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.purchasingCompletenessStatus_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.purchasingCompletenessStatus_ = value
            }
        }
    }

    open var purchasingCompletenessStatus: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderType.purchasingCompletenessStatus))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.purchasingCompletenessStatus, to: BooleanValue.of(optional: value))
        }
    }

    open class var purchasingDocumentDeletionCode: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.purchasingDocumentDeletionCode_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.purchasingDocumentDeletionCode_ = value
            }
        }
    }

    open var purchasingDocumentDeletionCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.purchasingDocumentDeletionCode))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.purchasingDocumentDeletionCode, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingDocumentOrigin: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.purchasingDocumentOrigin_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.purchasingDocumentOrigin_ = value
            }
        }
    }

    open var purchasingDocumentOrigin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.purchasingDocumentOrigin))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.purchasingDocumentOrigin, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingGroup: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.purchasingGroup_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.purchasingGroup_ = value
            }
        }
    }

    open var purchasingGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.purchasingGroup))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.purchasingGroup, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingOrganization: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.purchasingOrganization_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.purchasingOrganization_ = value
            }
        }
    }

    open var purchasingOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.purchasingOrganization))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.purchasingOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var purchasingProcessingStatus: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.purchasingProcessingStatus_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.purchasingProcessingStatus_ = value
            }
        }
    }

    open var purchasingProcessingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.purchasingProcessingStatus))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.purchasingProcessingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var releaseIsNotCompleted: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.releaseIsNotCompleted_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.releaseIsNotCompleted_ = value
            }
        }
    }

    open var releaseIsNotCompleted: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurchaseOrderType.releaseIsNotCompleted))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.releaseIsNotCompleted, to: BooleanValue.of(optional: value))
        }
    }

    open class var supplier: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.supplier_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.supplier_ = value
            }
        }
    }

    open var supplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.supplier))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.supplier, to: StringValue.of(optional: value))
        }
    }

    open class var supplierPhoneNumber: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.supplierPhoneNumber_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.supplierPhoneNumber_ = value
            }
        }
    }

    open var supplierPhoneNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.supplierPhoneNumber))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.supplierPhoneNumber, to: StringValue.of(optional: value))
        }
    }

    open class var supplierQuotationExternalID: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.supplierQuotationExternalID_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.supplierQuotationExternalID_ = value
            }
        }
    }

    open var supplierQuotationExternalID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.supplierQuotationExternalID))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.supplierQuotationExternalID, to: StringValue.of(optional: value))
        }
    }

    open class var supplierRespSalesPersonName: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.supplierRespSalesPersonName_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.supplierRespSalesPersonName_ = value
            }
        }
    }

    open var supplierRespSalesPersonName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.supplierRespSalesPersonName))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.supplierRespSalesPersonName, to: StringValue.of(optional: value))
        }
    }

    open class var supplyingPlant: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.supplyingPlant_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.supplyingPlant_ = value
            }
        }
    }

    open var supplyingPlant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.supplyingPlant))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.supplyingPlant, to: StringValue.of(optional: value))
        }
    }

    open class var supplyingSupplier: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.supplyingSupplier_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.supplyingSupplier_ = value
            }
        }
    }

    open var supplyingSupplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurchaseOrderType.supplyingSupplier))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.supplyingSupplier, to: StringValue.of(optional: value))
        }
    }

    open class var toPurchaseOrderItem: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.toPurchaseOrderItem_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.toPurchaseOrderItem_ = value
            }
        }
    }

    open var toPurchaseOrderItem: [APurchaseOrderItemType] {
        get {
            return ArrayConverter.convert(APurchaseOrderType.toPurchaseOrderItem.entityList(from: self).toArray(), [APurchaseOrderItemType]())
        }
        set(value) {
            APurchaseOrderType.toPurchaseOrderItem.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class var validityEndDate: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.validityEndDate_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.validityEndDate_ = value
            }
        }
    }

    open var validityEndDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: APurchaseOrderType.validityEndDate))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.validityEndDate, to: value)
        }
    }

    open class var validityStartDate: Property {
        get {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                return APurchaseOrderType.validityStartDate_
            }
        }
        set(value) {
            objc_sync_enter(APurchaseOrderType.self)
            defer { objc_sync_exit(APurchaseOrderType.self) }
            do {
                APurchaseOrderType.validityStartDate_ = value
            }
        }
    }

    open var validityStartDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: APurchaseOrderType.validityStartDate))
        }
        set(value) {
            self.setOptionalValue(for: APurchaseOrderType.validityStartDate, to: value)
        }
    }
}
