// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class HUHead: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var handlingUnitUUID_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "HandlingUnitUUID")

    private static var inboundDeliveryUUID_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "InboundDeliveryUUID")

    private static var inboundDeliveryItemUUID_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "InboundDeliveryItemUUID")

    private static var handlingUnitID_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "HandlingUnitID")

    private static var packageMaterial_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "PackageMaterial")

    private static var packageMaterialText_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "PackageMaterialText")

    private static var handlingUnitType_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "HandlingUnitType")

    private static var handlingUnitTypeText_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "HandlingUnitTypeText")

    private static var handlingUnitTopID_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "HandlingUnitTopID")

    private static var handlingUnitTopUUID_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "HandlingUnitTopUUID")

    private static var higherUUID_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "HigherUUID")

    private static var weight_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "Weight")

    private static var weightUnit_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "WeightUnit")

    private static var volume_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "Volume")

    private static var volumeUnit_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "VolumeUnit")

    private static var itemQuantity_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "ItemQuantity")

    private static var itemQuantityUnit_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "ItemQuantityUnit")

    private static var lastChangedDateTime_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "LastChangedDateTime")

    private static var numberOfHUItems_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "NumberOfHUItems")

    private static var warehouseNumber_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "WarehouseNumber")

    private static var items_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "Items")

    private static var aggregatedItems_: Property = Ec1Metadata.EntityTypes.huHead.property(withName: "AggregatedItems")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.huHead)
    }

    open class var aggregatedItems: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.aggregatedItems_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.aggregatedItems_ = value
            }
        }
    }

    open var aggregatedItems: [HUItem] {
        get {
            return ArrayConverter.convert(HUHead.aggregatedItems.entityList(from: self).toArray(), [HUItem]())
        }
        set(value) {
            HUHead.aggregatedItems.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class func array(from: EntityValueList) -> [HUHead] {
        return ArrayConverter.convert(from.toArray(), [HUHead]())
    }

    open func copy() -> HUHead {
        return CastRequired<HUHead>.from(self.copyEntity())
    }

    open class var handlingUnitID: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.handlingUnitID_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.handlingUnitID_ = value
            }
        }
    }

    open var handlingUnitID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUHead.handlingUnitID))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.handlingUnitID, to: StringValue.of(optional: value))
        }
    }

    open class var handlingUnitTopID: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.handlingUnitTopID_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.handlingUnitTopID_ = value
            }
        }
    }

    open var handlingUnitTopID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUHead.handlingUnitTopID))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.handlingUnitTopID, to: StringValue.of(optional: value))
        }
    }

    open class var handlingUnitTopUUID: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.handlingUnitTopUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.handlingUnitTopUUID_ = value
            }
        }
    }

    open var handlingUnitTopUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUHead.handlingUnitTopUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.handlingUnitTopUUID, to: value)
        }
    }

    open class var handlingUnitType: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.handlingUnitType_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.handlingUnitType_ = value
            }
        }
    }

    open var handlingUnitType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUHead.handlingUnitType))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.handlingUnitType, to: StringValue.of(optional: value))
        }
    }

    open class var handlingUnitTypeText: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.handlingUnitTypeText_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.handlingUnitTypeText_ = value
            }
        }
    }

    open var handlingUnitTypeText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUHead.handlingUnitTypeText))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.handlingUnitTypeText, to: StringValue.of(optional: value))
        }
    }

    open class var handlingUnitUUID: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.handlingUnitUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.handlingUnitUUID_ = value
            }
        }
    }

    open var handlingUnitUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUHead.handlingUnitUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.handlingUnitUUID, to: value)
        }
    }

    open class var higherUUID: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.higherUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.higherUUID_ = value
            }
        }
    }

    open var higherUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUHead.higherUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.higherUUID, to: value)
        }
    }

    open class var inboundDeliveryItemUUID: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.inboundDeliveryItemUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.inboundDeliveryItemUUID_ = value
            }
        }
    }

    open var inboundDeliveryItemUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUHead.inboundDeliveryItemUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.inboundDeliveryItemUUID, to: value)
        }
    }

    open class var inboundDeliveryUUID: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.inboundDeliveryUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.inboundDeliveryUUID_ = value
            }
        }
    }

    open var inboundDeliveryUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUHead.inboundDeliveryUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.inboundDeliveryUUID, to: value)
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var itemQuantity: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.itemQuantity_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.itemQuantity_ = value
            }
        }
    }

    open var itemQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: HUHead.itemQuantity))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.itemQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemQuantityUnit: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.itemQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.itemQuantityUnit_ = value
            }
        }
    }

    open var itemQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUHead.itemQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.itemQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var items: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.items_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.items_ = value
            }
        }
    }

    open var items: [HUItem] {
        get {
            return ArrayConverter.convert(HUHead.items.entityList(from: self).toArray(), [HUItem]())
        }
        set(value) {
            HUHead.items.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class func key(handlingUnitUUID: GuidValue?, inboundDeliveryUUID: GuidValue?, inboundDeliveryItemUUID: GuidValue?) -> EntityKey {
        return EntityKey().with(name: "HandlingUnitUUID", value: handlingUnitUUID).with(name: "InboundDeliveryUUID", value: inboundDeliveryUUID).with(name: "InboundDeliveryItemUUID", value: inboundDeliveryItemUUID)
    }

    open class var lastChangedDateTime: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.lastChangedDateTime_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.lastChangedDateTime_ = value
            }
        }
    }

    open var lastChangedDateTime: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: HUHead.lastChangedDateTime))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.lastChangedDateTime, to: value)
        }
    }

    open class var numberOfHUItems: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.numberOfHUItems_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.numberOfHUItems_ = value
            }
        }
    }

    open var numberOfHUItems: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: HUHead.numberOfHUItems))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.numberOfHUItems, to: IntValue.of(optional: value))
        }
    }

    open var old: HUHead {
        return CastRequired<HUHead>.from(self.oldEntity)
    }

    open class var packageMaterial: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.packageMaterial_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.packageMaterial_ = value
            }
        }
    }

    open var packageMaterial: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUHead.packageMaterial))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.packageMaterial, to: StringValue.of(optional: value))
        }
    }

    open class var packageMaterialText: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.packageMaterialText_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.packageMaterialText_ = value
            }
        }
    }

    open var packageMaterialText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUHead.packageMaterialText))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.packageMaterialText, to: StringValue.of(optional: value))
        }
    }

    open class var volume: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.volume_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.volume_ = value
            }
        }
    }

    open var volume: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: HUHead.volume))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.volume, to: DecimalValue.of(optional: value))
        }
    }

    open class var volumeUnit: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.volumeUnit_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.volumeUnit_ = value
            }
        }
    }

    open var volumeUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUHead.volumeUnit))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.volumeUnit, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseNumber: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.warehouseNumber_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.warehouseNumber_ = value
            }
        }
    }

    open var warehouseNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUHead.warehouseNumber))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.warehouseNumber, to: StringValue.of(optional: value))
        }
    }

    open class var weight: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.weight_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.weight_ = value
            }
        }
    }

    open var weight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: HUHead.weight))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.weight, to: DecimalValue.of(optional: value))
        }
    }

    open class var weightUnit: Property {
        get {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                return HUHead.weightUnit_
            }
        }
        set(value) {
            objc_sync_enter(HUHead.self)
            defer { objc_sync_exit(HUHead.self) }
            do {
                HUHead.weightUnit_ = value
            }
        }
    }

    open var weightUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUHead.weightUnit))
        }
        set(value) {
            self.setOptionalValue(for: HUHead.weightUnit, to: StringValue.of(optional: value))
        }
    }
}
