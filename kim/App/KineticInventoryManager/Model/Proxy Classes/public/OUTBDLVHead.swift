// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class OUTBDLVHead: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var carrier_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "Carrier")

    private static var carrierName_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "CarrierName")

    private static var goodsIssueStatus_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "GoodsIssueStatus")

    private static var goodsIssueStatusName_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "GoodsIssueStatusName")

    private static var lastChangedDateTime_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "LastChangedDateTime")

    private static var maxWeight_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "MaxWeight")

    private static var maxWeightUnit_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "MaxWeightUnit")

    private static var meansOfTransport_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "MeansOfTransport")

    private static var meansOfTransportName_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "MeansOfTransportName")

    private static var numberOfItems_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "NumberOfItems")

    private static var numberOfTranspUnit_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "NumberOfTranspUnit")

    private static var outboundDelivery_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "OutboundDelivery")

    private static var outboundDeliveryUUID_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "OutboundDeliveryUUID")

    private static var outOfYardDatePlan_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "OutOfYardDatePlan")

    private static var completionStatusName_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "CompletionStatusName")

    private static var pickingStatusName_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PickingStatusName")

    private static var pickingStatusPercent_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PickingStatusPercent")

    private static var plannedPickingStatus_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PlannedPickingStatus")

    private static var plannedPickingStatusName_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PlannedPickingStatusName")

    private static var route_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "Route")

    private static var routeName_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "RouteName")

    private static var shipToParty_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ShipToParty")

    private static var shipToPartyName_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ShipToPartyName")

    private static var stagingArea_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "StagingArea")

    private static var stagingAreaGroup_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "StagingAreaGroup")

    private static var stagingAreaBin_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "StagingAreaBin")

    private static var transpUnitActivityNumber_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "TranspUnitActivityNumber")

    private static var transpUnitExternalNumber_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "TranspUnitExternalNumber")

    private static var transpUnitInternalNumber_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "TranspUnitInternalNumber")

    private static var warehouseDoor_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "WarehouseDoor")

    private static var docNoERP_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "DocNoERP")

    private static var bskeyERP_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "BskeyERP")

    private static var salesOrder_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "SalesOrder")

    private static var numberOfSalesOrder_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "NumberOfSalesOrder")

    private static var warehouseNumber_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "WarehouseNumber")

    private static var shippingConditionCode_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ShippingConditionCode")

    private static var shippingConditionName_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ShippingConditionName")

    private static var numberOfShippingCondition_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "NumberOfShippingCondition")

    private static var includeCompletedDelivery_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "IncludeCompletedDelivery")

    private static var uxFcWarehouseDoor_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_WarehouseDoor")

    private static var uxFcOutOfYardDatePlan_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_OutOfYardDatePlan")

    private static var uxFcCarrier_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_Carrier")

    private static var uxFcStagingArea_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_StagingArea")

    private static var uxFcStagingAreaGroup_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_StagingAreaGroup")

    private static var uxFcStagingAreaBin_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_StagingAreaBin")

    private static var uxFcADeletable_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFcA_Deletable")

    private static var uxFcAGoodsIssue_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFcA_GoodsIssue")

    private static var uxFcAUpdatable_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFcA_Updatable")

    private static var outboundDeliveryType_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "OutboundDeliveryType")

    private static var outboundDeliveryTypeText_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "OutboundDeliveryTypeText")

    private static var manufacturingOrder_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ManufacturingOrder")

    private static var numberOfManufacturingOrders_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "NumberOfManufacturingOrders")

    private static var deliveryDatePlan_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "DeliveryDatePlan")

    private static var psa_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PSA")

    private static var uxFcDeliveryDatePlan_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_DeliveryDatePlan")

    private static var packingStatus_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PackingStatus")

    private static var packingStatusName_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PackingStatusName")

    private static var productionIndicator_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ProductionIndicator")

    private static var blockedOverallStatus_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "BlockedOverallStatus")

    private static var blockedOverallStatusName_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "BlockedOverallStatusName")

    private static var existsWT_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ExistsWT")

    private static var completionStatus_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "CompletionStatus")

    private static var items_: Property = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "Items")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.outbdlvHead)
    }

    open class func array(from: EntityValueList) -> [OUTBDLVHead] {
        return ArrayConverter.convert(from.toArray(), [OUTBDLVHead]())
    }

    open class var blockedOverallStatus: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.blockedOverallStatus_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.blockedOverallStatus_ = value
            }
        }
    }

    open var blockedOverallStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.blockedOverallStatus))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.blockedOverallStatus, to: StringValue.of(optional: value))
        }
    }

    open class var blockedOverallStatusName: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.blockedOverallStatusName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.blockedOverallStatusName_ = value
            }
        }
    }

    open var blockedOverallStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.blockedOverallStatusName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.blockedOverallStatusName, to: StringValue.of(optional: value))
        }
    }

    open class var bskeyERP: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.bskeyERP_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.bskeyERP_ = value
            }
        }
    }

    open var bskeyERP: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.bskeyERP))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.bskeyERP, to: StringValue.of(optional: value))
        }
    }

    open class var carrier: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.carrier_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.carrier_ = value
            }
        }
    }

    open var carrier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.carrier))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.carrier, to: StringValue.of(optional: value))
        }
    }

    open class var carrierName: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.carrierName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.carrierName_ = value
            }
        }
    }

    open var carrierName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.carrierName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.carrierName, to: StringValue.of(optional: value))
        }
    }

    open class var completionStatus: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.completionStatus_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.completionStatus_ = value
            }
        }
    }

    open var completionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.completionStatus))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.completionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var completionStatusName: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.completionStatusName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.completionStatusName_ = value
            }
        }
    }

    open var completionStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.completionStatusName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.completionStatusName, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> OUTBDLVHead {
        return CastRequired<OUTBDLVHead>.from(self.copyEntity())
    }

    open class var deliveryDatePlan: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.deliveryDatePlan_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.deliveryDatePlan_ = value
            }
        }
    }

    open var deliveryDatePlan: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: OUTBDLVHead.deliveryDatePlan))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.deliveryDatePlan, to: value)
        }
    }

    open class var docNoERP: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.docNoERP_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.docNoERP_ = value
            }
        }
    }

    open var docNoERP: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.docNoERP))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.docNoERP, to: StringValue.of(optional: value))
        }
    }

    open class var existsWT: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.existsWT_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.existsWT_ = value
            }
        }
    }

    open var existsWT: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: OUTBDLVHead.existsWT))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.existsWT, to: BooleanValue.of(optional: value))
        }
    }

    open class var goodsIssueStatus: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.goodsIssueStatus_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.goodsIssueStatus_ = value
            }
        }
    }

    open var goodsIssueStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.goodsIssueStatus))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.goodsIssueStatus, to: StringValue.of(optional: value))
        }
    }

    open class var goodsIssueStatusName: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.goodsIssueStatusName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.goodsIssueStatusName_ = value
            }
        }
    }

    open var goodsIssueStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.goodsIssueStatusName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.goodsIssueStatusName, to: StringValue.of(optional: value))
        }
    }

    open class var includeCompletedDelivery: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.includeCompletedDelivery_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.includeCompletedDelivery_ = value
            }
        }
    }

    open var includeCompletedDelivery: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: OUTBDLVHead.includeCompletedDelivery))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.includeCompletedDelivery, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var items: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.items_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.items_ = value
            }
        }
    }

    open var items: [OUTBDLVItem] {
        get {
            return ArrayConverter.convert(OUTBDLVHead.items.entityList(from: self).toArray(), [OUTBDLVItem]())
        }
        set(value) {
            OUTBDLVHead.items.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class func key(outboundDeliveryUUID: GuidValue?) -> EntityKey {
        return EntityKey().with(name: "OutboundDeliveryUUID", value: outboundDeliveryUUID)
    }

    open class var lastChangedDateTime: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.lastChangedDateTime_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.lastChangedDateTime_ = value
            }
        }
    }

    open var lastChangedDateTime: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: OUTBDLVHead.lastChangedDateTime))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.lastChangedDateTime, to: value)
        }
    }

    open class var manufacturingOrder: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.manufacturingOrder_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.manufacturingOrder_ = value
            }
        }
    }

    open var manufacturingOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.manufacturingOrder))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.manufacturingOrder, to: StringValue.of(optional: value))
        }
    }

    open class var maxWeight: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.maxWeight_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.maxWeight_ = value
            }
        }
    }

    open var maxWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: OUTBDLVHead.maxWeight))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.maxWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var maxWeightUnit: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.maxWeightUnit_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.maxWeightUnit_ = value
            }
        }
    }

    open var maxWeightUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.maxWeightUnit))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.maxWeightUnit, to: StringValue.of(optional: value))
        }
    }

    open class var meansOfTransport: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.meansOfTransport_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.meansOfTransport_ = value
            }
        }
    }

    open var meansOfTransport: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.meansOfTransport))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.meansOfTransport, to: StringValue.of(optional: value))
        }
    }

    open class var meansOfTransportName: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.meansOfTransportName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.meansOfTransportName_ = value
            }
        }
    }

    open var meansOfTransportName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.meansOfTransportName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.meansOfTransportName, to: StringValue.of(optional: value))
        }
    }

    open class var numberOfItems: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.numberOfItems_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.numberOfItems_ = value
            }
        }
    }

    open var numberOfItems: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: OUTBDLVHead.numberOfItems))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.numberOfItems, to: IntValue.of(optional: value))
        }
    }

    open class var numberOfManufacturingOrders: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.numberOfManufacturingOrders_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.numberOfManufacturingOrders_ = value
            }
        }
    }

    open var numberOfManufacturingOrders: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: OUTBDLVHead.numberOfManufacturingOrders))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.numberOfManufacturingOrders, to: IntValue.of(optional: value))
        }
    }

    open class var numberOfSalesOrder: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.numberOfSalesOrder_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.numberOfSalesOrder_ = value
            }
        }
    }

    open var numberOfSalesOrder: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: OUTBDLVHead.numberOfSalesOrder))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.numberOfSalesOrder, to: IntValue.of(optional: value))
        }
    }

    open class var numberOfShippingCondition: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.numberOfShippingCondition_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.numberOfShippingCondition_ = value
            }
        }
    }

    open var numberOfShippingCondition: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: OUTBDLVHead.numberOfShippingCondition))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.numberOfShippingCondition, to: IntValue.of(optional: value))
        }
    }

    open class var numberOfTranspUnit: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.numberOfTranspUnit_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.numberOfTranspUnit_ = value
            }
        }
    }

    open var numberOfTranspUnit: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: OUTBDLVHead.numberOfTranspUnit))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.numberOfTranspUnit, to: IntValue.of(optional: value))
        }
    }

    open var old: OUTBDLVHead {
        return CastRequired<OUTBDLVHead>.from(self.oldEntity)
    }

    open class var outOfYardDatePlan: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.outOfYardDatePlan_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.outOfYardDatePlan_ = value
            }
        }
    }

    open var outOfYardDatePlan: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: OUTBDLVHead.outOfYardDatePlan))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.outOfYardDatePlan, to: value)
        }
    }

    open class var outboundDelivery: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.outboundDelivery_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.outboundDelivery_ = value
            }
        }
    }

    open var outboundDelivery: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.outboundDelivery))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.outboundDelivery, to: StringValue.of(optional: value))
        }
    }

    open class var outboundDeliveryType: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.outboundDeliveryType_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.outboundDeliveryType_ = value
            }
        }
    }

    open var outboundDeliveryType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.outboundDeliveryType))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.outboundDeliveryType, to: StringValue.of(optional: value))
        }
    }

    open class var outboundDeliveryTypeText: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.outboundDeliveryTypeText_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.outboundDeliveryTypeText_ = value
            }
        }
    }

    open var outboundDeliveryTypeText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.outboundDeliveryTypeText))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.outboundDeliveryTypeText, to: StringValue.of(optional: value))
        }
    }

    open class var outboundDeliveryUUID: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.outboundDeliveryUUID_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.outboundDeliveryUUID_ = value
            }
        }
    }

    open var outboundDeliveryUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: OUTBDLVHead.outboundDeliveryUUID))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.outboundDeliveryUUID, to: value)
        }
    }

    open class var packingStatus: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.packingStatus_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.packingStatus_ = value
            }
        }
    }

    open var packingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.packingStatus))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.packingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var packingStatusName: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.packingStatusName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.packingStatusName_ = value
            }
        }
    }

    open var packingStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.packingStatusName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.packingStatusName, to: StringValue.of(optional: value))
        }
    }

    open class var pickingStatusName: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.pickingStatusName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.pickingStatusName_ = value
            }
        }
    }

    open var pickingStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.pickingStatusName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.pickingStatusName, to: StringValue.of(optional: value))
        }
    }

    open class var pickingStatusPercent: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.pickingStatusPercent_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.pickingStatusPercent_ = value
            }
        }
    }

    open var pickingStatusPercent: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: OUTBDLVHead.pickingStatusPercent))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.pickingStatusPercent, to: IntValue.of(optional: value))
        }
    }

    open class var plannedPickingStatus: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.plannedPickingStatus_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.plannedPickingStatus_ = value
            }
        }
    }

    open var plannedPickingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.plannedPickingStatus))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.plannedPickingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var plannedPickingStatusName: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.plannedPickingStatusName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.plannedPickingStatusName_ = value
            }
        }
    }

    open var plannedPickingStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.plannedPickingStatusName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.plannedPickingStatusName, to: StringValue.of(optional: value))
        }
    }

    open class var productionIndicator: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.productionIndicator_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.productionIndicator_ = value
            }
        }
    }

    open var productionIndicator: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.productionIndicator))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.productionIndicator, to: StringValue.of(optional: value))
        }
    }

    open class var psa: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.psa_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.psa_ = value
            }
        }
    }

    open var psa: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.psa))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.psa, to: StringValue.of(optional: value))
        }
    }

    open class var route: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.route_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.route_ = value
            }
        }
    }

    open var route: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.route))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.route, to: StringValue.of(optional: value))
        }
    }

    open class var routeName: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.routeName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.routeName_ = value
            }
        }
    }

    open var routeName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.routeName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.routeName, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrder: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.salesOrder_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.salesOrder_ = value
            }
        }
    }

    open var salesOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.salesOrder))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.salesOrder, to: StringValue.of(optional: value))
        }
    }

    open class var shipToParty: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.shipToParty_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.shipToParty_ = value
            }
        }
    }

    open var shipToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.shipToParty))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.shipToParty, to: StringValue.of(optional: value))
        }
    }

    open class var shipToPartyName: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.shipToPartyName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.shipToPartyName_ = value
            }
        }
    }

    open var shipToPartyName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.shipToPartyName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.shipToPartyName, to: StringValue.of(optional: value))
        }
    }

    open class var shippingConditionCode: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.shippingConditionCode_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.shippingConditionCode_ = value
            }
        }
    }

    open var shippingConditionCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.shippingConditionCode))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.shippingConditionCode, to: StringValue.of(optional: value))
        }
    }

    open class var shippingConditionName: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.shippingConditionName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.shippingConditionName_ = value
            }
        }
    }

    open var shippingConditionName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.shippingConditionName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.shippingConditionName, to: StringValue.of(optional: value))
        }
    }

    open class var stagingArea: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.stagingArea_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.stagingArea_ = value
            }
        }
    }

    open var stagingArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.stagingArea))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.stagingArea, to: StringValue.of(optional: value))
        }
    }

    open class var stagingAreaBin: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.stagingAreaBin_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.stagingAreaBin_ = value
            }
        }
    }

    open var stagingAreaBin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.stagingAreaBin))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.stagingAreaBin, to: StringValue.of(optional: value))
        }
    }

    open class var stagingAreaGroup: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.stagingAreaGroup_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.stagingAreaGroup_ = value
            }
        }
    }

    open var stagingAreaGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.stagingAreaGroup))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.stagingAreaGroup, to: StringValue.of(optional: value))
        }
    }

    open class var transpUnitActivityNumber: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.transpUnitActivityNumber_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.transpUnitActivityNumber_ = value
            }
        }
    }

    open var transpUnitActivityNumber: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: OUTBDLVHead.transpUnitActivityNumber))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.transpUnitActivityNumber, to: IntegerValue.of(optional: value))
        }
    }

    open class var transpUnitExternalNumber: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.transpUnitExternalNumber_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.transpUnitExternalNumber_ = value
            }
        }
    }

    open var transpUnitExternalNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.transpUnitExternalNumber))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.transpUnitExternalNumber, to: StringValue.of(optional: value))
        }
    }

    open class var transpUnitInternalNumber: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.transpUnitInternalNumber_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.transpUnitInternalNumber_ = value
            }
        }
    }

    open var transpUnitInternalNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.transpUnitInternalNumber))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.transpUnitInternalNumber, to: StringValue.of(optional: value))
        }
    }

    open class var uxFcADeletable: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.uxFcADeletable_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.uxFcADeletable_ = value
            }
        }
    }

    open var uxFcADeletable: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: OUTBDLVHead.uxFcADeletable))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.uxFcADeletable, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcAGoodsIssue: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.uxFcAGoodsIssue_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.uxFcAGoodsIssue_ = value
            }
        }
    }

    open var uxFcAGoodsIssue: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: OUTBDLVHead.uxFcAGoodsIssue))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.uxFcAGoodsIssue, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcAUpdatable: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.uxFcAUpdatable_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.uxFcAUpdatable_ = value
            }
        }
    }

    open var uxFcAUpdatable: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: OUTBDLVHead.uxFcAUpdatable))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.uxFcAUpdatable, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcCarrier: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.uxFcCarrier_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.uxFcCarrier_ = value
            }
        }
    }

    open var uxFcCarrier: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: OUTBDLVHead.uxFcCarrier))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.uxFcCarrier, to: UnsignedByte.of(optional: value))
        }
    }

    open class var uxFcDeliveryDatePlan: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.uxFcDeliveryDatePlan_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.uxFcDeliveryDatePlan_ = value
            }
        }
    }

    open var uxFcDeliveryDatePlan: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: OUTBDLVHead.uxFcDeliveryDatePlan))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.uxFcDeliveryDatePlan, to: UnsignedByte.of(optional: value))
        }
    }

    open class var uxFcOutOfYardDatePlan: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.uxFcOutOfYardDatePlan_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.uxFcOutOfYardDatePlan_ = value
            }
        }
    }

    open var uxFcOutOfYardDatePlan: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: OUTBDLVHead.uxFcOutOfYardDatePlan))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.uxFcOutOfYardDatePlan, to: UnsignedByte.of(optional: value))
        }
    }

    open class var uxFcStagingArea: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.uxFcStagingArea_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.uxFcStagingArea_ = value
            }
        }
    }

    open var uxFcStagingArea: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: OUTBDLVHead.uxFcStagingArea))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.uxFcStagingArea, to: UnsignedByte.of(optional: value))
        }
    }

    open class var uxFcStagingAreaBin: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.uxFcStagingAreaBin_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.uxFcStagingAreaBin_ = value
            }
        }
    }

    open var uxFcStagingAreaBin: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: OUTBDLVHead.uxFcStagingAreaBin))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.uxFcStagingAreaBin, to: UnsignedByte.of(optional: value))
        }
    }

    open class var uxFcStagingAreaGroup: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.uxFcStagingAreaGroup_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.uxFcStagingAreaGroup_ = value
            }
        }
    }

    open var uxFcStagingAreaGroup: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: OUTBDLVHead.uxFcStagingAreaGroup))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.uxFcStagingAreaGroup, to: UnsignedByte.of(optional: value))
        }
    }

    open class var uxFcWarehouseDoor: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.uxFcWarehouseDoor_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.uxFcWarehouseDoor_ = value
            }
        }
    }

    open var uxFcWarehouseDoor: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: OUTBDLVHead.uxFcWarehouseDoor))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.uxFcWarehouseDoor, to: UnsignedByte.of(optional: value))
        }
    }

    open class var warehouseDoor: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.warehouseDoor_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.warehouseDoor_ = value
            }
        }
    }

    open var warehouseDoor: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.warehouseDoor))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.warehouseDoor, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseNumber: Property {
        get {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                return OUTBDLVHead.warehouseNumber_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVHead.self)
            defer { objc_sync_exit(OUTBDLVHead.self) }
            do {
                OUTBDLVHead.warehouseNumber_ = value
            }
        }
    }

    open var warehouseNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVHead.warehouseNumber))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVHead.warehouseNumber, to: StringValue.of(optional: value))
        }
    }
}
