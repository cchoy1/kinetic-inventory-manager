// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class PerformPackType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var lgnum_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "Lgnum")

    private static var workstation_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "Workstation")

    private static var bin_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "Bin")

    private static var sourceID_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "SourceId")

    private static var shippingHUID_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "ShippingHUId")

    private static var sourceType_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "SourceType")

    private static var isPackAll_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "IsPackAll")

    private static var packMat_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "PackMat")

    private static var stockID_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "StockId")

    private static var srcQuan_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "SrcQuan")

    private static var srcAlterQuan_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "SrcAlterQuan")

    private static var desQuan_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "DesQuan")

    private static var desAlterQuan_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "DesAlterQuan")

    private static var desBaseUoM_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "DesBaseUoM")

    private static var desAlterUoM_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "DesAlterUoM")

    private static var packmatDesc_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "PackmatDesc")

    private static var packmatID_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "PackmatId")

    private static var product_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "Product")

    private static var huID_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "HuId")

    private static var loadingWeight_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "LoadingWeight")

    private static var weightUoM_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "WeightUoM")

    private static var msgID_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "MsgId")

    private static var msgVar_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "MsgVar")

    private static var msgKey_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "MsgKey")

    private static var msgType_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "MsgType")

    private static var msgSuccess_: Property = Ec1Metadata.EntityTypes.performPackType.property(withName: "MsgSuccess")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.performPackType)
    }

    open class func array(from: EntityValueList) -> [PerformPackType] {
        return ArrayConverter.convert(from.toArray(), [PerformPackType]())
    }

    open class var bin: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.bin_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.bin_ = value
            }
        }
    }

    open var bin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.bin))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.bin, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> PerformPackType {
        return CastRequired<PerformPackType>.from(self.copyEntity())
    }

    open class var desAlterQuan: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.desAlterQuan_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.desAlterQuan_ = value
            }
        }
    }

    open var desAlterQuan: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: PerformPackType.desAlterQuan))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.desAlterQuan, to: DecimalValue.of(optional: value))
        }
    }

    open class var desAlterUoM: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.desAlterUoM_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.desAlterUoM_ = value
            }
        }
    }

    open var desAlterUoM: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.desAlterUoM))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.desAlterUoM, to: StringValue.of(optional: value))
        }
    }

    open class var desBaseUoM: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.desBaseUoM_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.desBaseUoM_ = value
            }
        }
    }

    open var desBaseUoM: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.desBaseUoM))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.desBaseUoM, to: StringValue.of(optional: value))
        }
    }

    open class var desQuan: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.desQuan_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.desQuan_ = value
            }
        }
    }

    open var desQuan: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: PerformPackType.desQuan))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.desQuan, to: DecimalValue.of(optional: value))
        }
    }

    open class var huID: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.huID_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.huID_ = value
            }
        }
    }

    open var huID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.huID))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.huID, to: StringValue.of(optional: value))
        }
    }

    open class var isPackAll: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.isPackAll_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.isPackAll_ = value
            }
        }
    }

    open var isPackAll: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PerformPackType.isPackAll))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.isPackAll, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(msgID: String?, msgKey: String?) -> EntityKey {
        return EntityKey().with(name: "MsgId", value: StringValue.of(optional: msgID)).with(name: "MsgKey", value: StringValue.of(optional: msgKey))
    }

    open class var lgnum: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.lgnum_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.lgnum_ = value
            }
        }
    }

    open var lgnum: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.lgnum))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.lgnum, to: StringValue.of(optional: value))
        }
    }

    open class var loadingWeight: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.loadingWeight_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.loadingWeight_ = value
            }
        }
    }

    open var loadingWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: PerformPackType.loadingWeight))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.loadingWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var msgID: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.msgID_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.msgID_ = value
            }
        }
    }

    open var msgID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.msgID))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.msgID, to: StringValue.of(optional: value))
        }
    }

    open class var msgKey: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.msgKey_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.msgKey_ = value
            }
        }
    }

    open var msgKey: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.msgKey))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.msgKey, to: StringValue.of(optional: value))
        }
    }

    open class var msgSuccess: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.msgSuccess_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.msgSuccess_ = value
            }
        }
    }

    open var msgSuccess: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: PerformPackType.msgSuccess))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.msgSuccess, to: BooleanValue.of(optional: value))
        }
    }

    open class var msgType: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.msgType_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.msgType_ = value
            }
        }
    }

    open var msgType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.msgType))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.msgType, to: StringValue.of(optional: value))
        }
    }

    open class var msgVar: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.msgVar_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.msgVar_ = value
            }
        }
    }

    open var msgVar: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.msgVar))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.msgVar, to: StringValue.of(optional: value))
        }
    }

    open var old: PerformPackType {
        return CastRequired<PerformPackType>.from(self.oldEntity)
    }

    open class var packMat: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.packMat_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.packMat_ = value
            }
        }
    }

    open var packMat: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.packMat))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.packMat, to: StringValue.of(optional: value))
        }
    }

    open class var packmatDesc: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.packmatDesc_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.packmatDesc_ = value
            }
        }
    }

    open var packmatDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.packmatDesc))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.packmatDesc, to: StringValue.of(optional: value))
        }
    }

    open class var packmatID: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.packmatID_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.packmatID_ = value
            }
        }
    }

    open var packmatID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.packmatID))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.packmatID, to: StringValue.of(optional: value))
        }
    }

    open class var product: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.product_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.product_ = value
            }
        }
    }

    open var product: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.product))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.product, to: StringValue.of(optional: value))
        }
    }

    open class var shippingHUID: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.shippingHUID_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.shippingHUID_ = value
            }
        }
    }

    open var shippingHUID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.shippingHUID))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.shippingHUID, to: StringValue.of(optional: value))
        }
    }

    open class var sourceID: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.sourceID_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.sourceID_ = value
            }
        }
    }

    open var sourceID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.sourceID))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.sourceID, to: StringValue.of(optional: value))
        }
    }

    open class var sourceType: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.sourceType_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.sourceType_ = value
            }
        }
    }

    open var sourceType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.sourceType))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.sourceType, to: StringValue.of(optional: value))
        }
    }

    open class var srcAlterQuan: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.srcAlterQuan_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.srcAlterQuan_ = value
            }
        }
    }

    open var srcAlterQuan: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: PerformPackType.srcAlterQuan))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.srcAlterQuan, to: DecimalValue.of(optional: value))
        }
    }

    open class var srcQuan: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.srcQuan_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.srcQuan_ = value
            }
        }
    }

    open var srcQuan: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: PerformPackType.srcQuan))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.srcQuan, to: DecimalValue.of(optional: value))
        }
    }

    open class var stockID: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.stockID_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.stockID_ = value
            }
        }
    }

    open var stockID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: PerformPackType.stockID))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.stockID, to: value)
        }
    }

    open class var weightUoM: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.weightUoM_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.weightUoM_ = value
            }
        }
    }

    open var weightUoM: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.weightUoM))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.weightUoM, to: StringValue.of(optional: value))
        }
    }

    open class var workstation: Property {
        get {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                return PerformPackType.workstation_
            }
        }
        set(value) {
            objc_sync_enter(PerformPackType.self)
            defer { objc_sync_exit(PerformPackType.self) }
            do {
                PerformPackType.workstation_ = value
            }
        }
    }

    open var workstation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PerformPackType.workstation))
        }
        set(value) {
            self.setOptionalValue(for: PerformPackType.workstation, to: StringValue.of(optional: value))
        }
    }
}
