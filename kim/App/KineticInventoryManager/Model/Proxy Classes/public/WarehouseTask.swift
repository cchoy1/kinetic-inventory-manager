// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class WarehouseTask: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var aarea_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "AAREA")

    private static var cat_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CAT")

    private static var catText_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CAT_TEXT")

    private static var charg_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CHARG")

    private static var confirmedAt_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CONFIRMED_AT")

    private static var confirmedBy_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CONFIRMED_BY")

    private static var confirmedByFullName_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CONFIRMED_BY_FULL_NAME")

    private static var coo_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "COO")

    private static var createdAt_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CREATED_AT")

    private static var createdBy_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CREATED_BY")

    private static var createdByFullName_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CREATED_BY_FULL_NAME")

    private static var dstgrp_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "DSTGRP")

    private static var entitled_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ENTITLED")

    private static var entitledText_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ENTITLED_TEXT")

    private static var exccode_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "EXCCODE")

    private static var exccodeText_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "EXCCODE_TEXT")

    private static var homve_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "HOMVE")

    private static var maktx_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "MAKTX")

    private static var matnr_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "MATNR")

    private static var nista_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "NISTA")

    private static var nistm_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "NISTM")

    private static var nlber_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "NLBER")

    private static var nlpla_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "NLPLA")

    private static var nltyp_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "NLTYP")

    private static var nlenr_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "NLENR")

    private static var owner_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "OWNER")

    private static var ownerText_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "OWNER_TEXT")

    private static var pickCompDt_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "PICK_COMP_DT")

    private static var procty_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "PROCTY")

    private static var proctyText_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "PROCTY_TEXT")

    private static var prodOrder_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "PROD_ORDER")

    private static var psa_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "PSA")

    private static var rdoccat_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "RDOCCAT")

    private static var docno_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "DOCNO")

    private static var itemno_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ITEMNO")

    private static var route_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ROUTE")

    private static var solpo_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "SOLPO")

    private static var startedAt_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "STARTED_AT")

    private static var stockDoccat_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "STOCK_DOCCAT")

    private static var stockDocno_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "STOCK_DOCNO")

    private static var stockItmno_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "STOCK_ITMNO")

    private static var tanum_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TANUM")

    private static var tapos_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TAPOS")

    private static var tostat_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TOSTAT")

    private static var tostatText_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TOSTAT_TEXT")

    private static var tostatCriticality_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TOSTAT_CRITICALITY")

    private static var unitV_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "UNIT_V")

    private static var unitW_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "UNIT_W")

    private static var vfdat_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VFDAT")

    private static var vlenr_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VLENR")

    private static var vlpla_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VLPLA")

    private static var vltyp_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VLTYP")

    private static var volum_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VOLUM")

    private static var capa_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CAPA")

    private static var vsola_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VSOLA")

    private static var vsolm_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VSOLM")

    private static var weight_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "WEIGHT")

    private static var zeiei_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ZEIEI")

    private static var qidplate_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "QIDPLATE")

    private static var inspdocno_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "INSPDOCNO")

    private static var origNlpla_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ORIG_NLPLA")

    private static var meins_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "MEINS")

    private static var altme_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ALTME")

    private static var isOpenWt_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "IS_OPEN_WT")

    private static var isBatchRequired_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "IS_BATCH_REQUIRED")

    private static var isSourceHuAllowed_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "IS_SOURCE_HU_ALLOWED")

    private static var isSourceHuObligatory_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "IS_SOURCE_HU_OBLIGATORY")

    private static var lgnum_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "LGNUM")

    private static var exccodeMulti_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "EXCCODE_MULTI")

    private static var who_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "WHO")

    private static var matid_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "MATID")

    private static var rdoccatText_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "RDOCCAT_TEXT")

    private static var trart_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TRART")

    private static var trartText_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TRART_TEXT")

    private static var kanban_: Property = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "KANBAN")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.warehouseTask)
    }

    open class var aarea: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.aarea_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.aarea_ = value
            }
        }
    }

    open var aarea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.aarea))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.aarea, to: StringValue.of(optional: value))
        }
    }

    open class var altme: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.altme_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.altme_ = value
            }
        }
    }

    open var altme: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.altme))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.altme, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [WarehouseTask] {
        return ArrayConverter.convert(from.toArray(), [WarehouseTask]())
    }

    open class var capa: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.capa_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.capa_ = value
            }
        }
    }

    open var capa: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTask.capa))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.capa, to: DecimalValue.of(optional: value))
        }
    }

    open class var cat: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.cat_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.cat_ = value
            }
        }
    }

    open var cat: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.cat))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.cat, to: StringValue.of(optional: value))
        }
    }

    open class var catText: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.catText_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.catText_ = value
            }
        }
    }

    open var catText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.catText))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.catText, to: StringValue.of(optional: value))
        }
    }

    open class var charg: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.charg_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.charg_ = value
            }
        }
    }

    open var charg: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.charg))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.charg, to: StringValue.of(optional: value))
        }
    }

    open class var confirmedAt: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.confirmedAt_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.confirmedAt_ = value
            }
        }
    }

    open var confirmedAt: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: WarehouseTask.confirmedAt))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.confirmedAt, to: value)
        }
    }

    open class var confirmedBy: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.confirmedBy_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.confirmedBy_ = value
            }
        }
    }

    open var confirmedBy: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.confirmedBy))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.confirmedBy, to: StringValue.of(optional: value))
        }
    }

    open class var confirmedByFullName: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.confirmedByFullName_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.confirmedByFullName_ = value
            }
        }
    }

    open var confirmedByFullName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.confirmedByFullName))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.confirmedByFullName, to: StringValue.of(optional: value))
        }
    }

    open class var coo: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.coo_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.coo_ = value
            }
        }
    }

    open var coo: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.coo))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.coo, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> WarehouseTask {
        return CastRequired<WarehouseTask>.from(self.copyEntity())
    }

    open class var createdAt: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.createdAt_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.createdAt_ = value
            }
        }
    }

    open var createdAt: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: WarehouseTask.createdAt))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.createdAt, to: value)
        }
    }

    open class var createdBy: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.createdBy_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.createdBy_ = value
            }
        }
    }

    open var createdBy: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.createdBy))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.createdBy, to: StringValue.of(optional: value))
        }
    }

    open class var createdByFullName: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.createdByFullName_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.createdByFullName_ = value
            }
        }
    }

    open var createdByFullName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.createdByFullName))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.createdByFullName, to: StringValue.of(optional: value))
        }
    }

    open class var docno: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.docno_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.docno_ = value
            }
        }
    }

    open var docno: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.docno))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.docno, to: StringValue.of(optional: value))
        }
    }

    open class var dstgrp: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.dstgrp_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.dstgrp_ = value
            }
        }
    }

    open var dstgrp: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.dstgrp))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.dstgrp, to: StringValue.of(optional: value))
        }
    }

    open class var entitled: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.entitled_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.entitled_ = value
            }
        }
    }

    open var entitled: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.entitled))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.entitled, to: StringValue.of(optional: value))
        }
    }

    open class var entitledText: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.entitledText_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.entitledText_ = value
            }
        }
    }

    open var entitledText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.entitledText))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.entitledText, to: StringValue.of(optional: value))
        }
    }

    open class var exccode: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.exccode_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.exccode_ = value
            }
        }
    }

    open var exccode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.exccode))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.exccode, to: StringValue.of(optional: value))
        }
    }

    open class var exccodeMulti: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.exccodeMulti_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.exccodeMulti_ = value
            }
        }
    }

    open var exccodeMulti: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.exccodeMulti))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.exccodeMulti, to: StringValue.of(optional: value))
        }
    }

    open class var exccodeText: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.exccodeText_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.exccodeText_ = value
            }
        }
    }

    open var exccodeText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.exccodeText))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.exccodeText, to: StringValue.of(optional: value))
        }
    }

    open class var homve: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.homve_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.homve_ = value
            }
        }
    }

    open var homve: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.homve))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.homve, to: StringValue.of(optional: value))
        }
    }

    open class var inspdocno: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.inspdocno_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.inspdocno_ = value
            }
        }
    }

    open var inspdocno: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.inspdocno))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.inspdocno, to: StringValue.of(optional: value))
        }
    }

    open class var isBatchRequired: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.isBatchRequired_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.isBatchRequired_ = value
            }
        }
    }

    open var isBatchRequired: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: WarehouseTask.isBatchRequired))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.isBatchRequired, to: BooleanValue.of(optional: value))
        }
    }

    open class var isOpenWt: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.isOpenWt_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.isOpenWt_ = value
            }
        }
    }

    open var isOpenWt: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: WarehouseTask.isOpenWt))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.isOpenWt, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var isSourceHuAllowed: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.isSourceHuAllowed_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.isSourceHuAllowed_ = value
            }
        }
    }

    open var isSourceHuAllowed: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: WarehouseTask.isSourceHuAllowed))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.isSourceHuAllowed, to: BooleanValue.of(optional: value))
        }
    }

    open class var isSourceHuObligatory: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.isSourceHuObligatory_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.isSourceHuObligatory_ = value
            }
        }
    }

    open var isSourceHuObligatory: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: WarehouseTask.isSourceHuObligatory))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.isSourceHuObligatory, to: BooleanValue.of(optional: value))
        }
    }

    open class var itemno: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.itemno_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.itemno_ = value
            }
        }
    }

    open var itemno: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.itemno))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.itemno, to: StringValue.of(optional: value))
        }
    }

    open class var kanban: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.kanban_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.kanban_ = value
            }
        }
    }

    open var kanban: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.kanban))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.kanban, to: StringValue.of(optional: value))
        }
    }

    open class func key(tanum: String?, tapos: String?, lgnum: String?) -> EntityKey {
        return EntityKey().with(name: "TANUM", value: StringValue.of(optional: tanum)).with(name: "TAPOS", value: StringValue.of(optional: tapos)).with(name: "LGNUM", value: StringValue.of(optional: lgnum))
    }

    open class var lgnum: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.lgnum_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.lgnum_ = value
            }
        }
    }

    open var lgnum: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.lgnum))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.lgnum, to: StringValue.of(optional: value))
        }
    }

    open class var maktx: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.maktx_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.maktx_ = value
            }
        }
    }

    open var maktx: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.maktx))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.maktx, to: StringValue.of(optional: value))
        }
    }

    open class var matid: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.matid_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.matid_ = value
            }
        }
    }

    open var matid: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: WarehouseTask.matid))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.matid, to: value)
        }
    }

    open class var matnr: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.matnr_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.matnr_ = value
            }
        }
    }

    open var matnr: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.matnr))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.matnr, to: StringValue.of(optional: value))
        }
    }

    open class var meins: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.meins_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.meins_ = value
            }
        }
    }

    open var meins: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.meins))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.meins, to: StringValue.of(optional: value))
        }
    }

    open class var nista: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.nista_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.nista_ = value
            }
        }
    }

    open var nista: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTask.nista))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.nista, to: DecimalValue.of(optional: value))
        }
    }

    open class var nistm: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.nistm_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.nistm_ = value
            }
        }
    }

    open var nistm: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTask.nistm))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.nistm, to: DecimalValue.of(optional: value))
        }
    }

    open class var nlber: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.nlber_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.nlber_ = value
            }
        }
    }

    open var nlber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.nlber))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.nlber, to: StringValue.of(optional: value))
        }
    }

    open class var nlenr: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.nlenr_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.nlenr_ = value
            }
        }
    }

    open var nlenr: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.nlenr))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.nlenr, to: StringValue.of(optional: value))
        }
    }

    open class var nlpla: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.nlpla_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.nlpla_ = value
            }
        }
    }

    open var nlpla: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.nlpla))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.nlpla, to: StringValue.of(optional: value))
        }
    }

    open class var nltyp: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.nltyp_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.nltyp_ = value
            }
        }
    }

    open var nltyp: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.nltyp))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.nltyp, to: StringValue.of(optional: value))
        }
    }

    open var old: WarehouseTask {
        return CastRequired<WarehouseTask>.from(self.oldEntity)
    }

    open class var origNlpla: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.origNlpla_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.origNlpla_ = value
            }
        }
    }

    open var origNlpla: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.origNlpla))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.origNlpla, to: StringValue.of(optional: value))
        }
    }

    open class var owner: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.owner_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.owner_ = value
            }
        }
    }

    open var owner: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.owner))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.owner, to: StringValue.of(optional: value))
        }
    }

    open class var ownerText: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.ownerText_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.ownerText_ = value
            }
        }
    }

    open var ownerText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.ownerText))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.ownerText, to: StringValue.of(optional: value))
        }
    }

    open class var pickCompDt: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.pickCompDt_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.pickCompDt_ = value
            }
        }
    }

    open var pickCompDt: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: WarehouseTask.pickCompDt))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.pickCompDt, to: value)
        }
    }

    open class var procty: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.procty_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.procty_ = value
            }
        }
    }

    open var procty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.procty))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.procty, to: StringValue.of(optional: value))
        }
    }

    open class var proctyText: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.proctyText_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.proctyText_ = value
            }
        }
    }

    open var proctyText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.proctyText))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.proctyText, to: StringValue.of(optional: value))
        }
    }

    open class var prodOrder: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.prodOrder_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.prodOrder_ = value
            }
        }
    }

    open var prodOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.prodOrder))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.prodOrder, to: StringValue.of(optional: value))
        }
    }

    open class var psa: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.psa_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.psa_ = value
            }
        }
    }

    open var psa: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.psa))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.psa, to: StringValue.of(optional: value))
        }
    }

    open class var qidplate: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.qidplate_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.qidplate_ = value
            }
        }
    }

    open var qidplate: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.qidplate))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.qidplate, to: StringValue.of(optional: value))
        }
    }

    open class var rdoccat: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.rdoccat_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.rdoccat_ = value
            }
        }
    }

    open var rdoccat: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.rdoccat))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.rdoccat, to: StringValue.of(optional: value))
        }
    }

    open class var rdoccatText: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.rdoccatText_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.rdoccatText_ = value
            }
        }
    }

    open var rdoccatText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.rdoccatText))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.rdoccatText, to: StringValue.of(optional: value))
        }
    }

    open class var route: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.route_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.route_ = value
            }
        }
    }

    open var route: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.route))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.route, to: StringValue.of(optional: value))
        }
    }

    open class var solpo: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.solpo_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.solpo_ = value
            }
        }
    }

    open var solpo: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTask.solpo))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.solpo, to: DecimalValue.of(optional: value))
        }
    }

    open class var startedAt: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.startedAt_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.startedAt_ = value
            }
        }
    }

    open var startedAt: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: WarehouseTask.startedAt))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.startedAt, to: value)
        }
    }

    open class var stockDoccat: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.stockDoccat_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.stockDoccat_ = value
            }
        }
    }

    open var stockDoccat: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.stockDoccat))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.stockDoccat, to: StringValue.of(optional: value))
        }
    }

    open class var stockDocno: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.stockDocno_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.stockDocno_ = value
            }
        }
    }

    open var stockDocno: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.stockDocno))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.stockDocno, to: StringValue.of(optional: value))
        }
    }

    open class var stockItmno: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.stockItmno_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.stockItmno_ = value
            }
        }
    }

    open var stockItmno: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.stockItmno))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.stockItmno, to: StringValue.of(optional: value))
        }
    }

    open class var tanum: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.tanum_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.tanum_ = value
            }
        }
    }

    open var tanum: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.tanum))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.tanum, to: StringValue.of(optional: value))
        }
    }

    open class var tapos: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.tapos_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.tapos_ = value
            }
        }
    }

    open var tapos: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.tapos))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.tapos, to: StringValue.of(optional: value))
        }
    }

    open class var tostat: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.tostat_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.tostat_ = value
            }
        }
    }

    open var tostat: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.tostat))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.tostat, to: StringValue.of(optional: value))
        }
    }

    open class var tostatCriticality: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.tostatCriticality_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.tostatCriticality_ = value
            }
        }
    }

    open var tostatCriticality: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: WarehouseTask.tostatCriticality))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.tostatCriticality, to: UnsignedByte.of(optional: value))
        }
    }

    open class var tostatText: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.tostatText_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.tostatText_ = value
            }
        }
    }

    open var tostatText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.tostatText))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.tostatText, to: StringValue.of(optional: value))
        }
    }

    open class var trart: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.trart_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.trart_ = value
            }
        }
    }

    open var trart: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.trart))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.trart, to: StringValue.of(optional: value))
        }
    }

    open class var trartText: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.trartText_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.trartText_ = value
            }
        }
    }

    open var trartText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.trartText))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.trartText, to: StringValue.of(optional: value))
        }
    }

    open class var unitV: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.unitV_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.unitV_ = value
            }
        }
    }

    open var unitV: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.unitV))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.unitV, to: StringValue.of(optional: value))
        }
    }

    open class var unitW: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.unitW_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.unitW_ = value
            }
        }
    }

    open var unitW: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.unitW))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.unitW, to: StringValue.of(optional: value))
        }
    }

    open class var vfdat: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.vfdat_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.vfdat_ = value
            }
        }
    }

    open var vfdat: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: WarehouseTask.vfdat))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.vfdat, to: value)
        }
    }

    open class var vlenr: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.vlenr_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.vlenr_ = value
            }
        }
    }

    open var vlenr: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.vlenr))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.vlenr, to: StringValue.of(optional: value))
        }
    }

    open class var vlpla: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.vlpla_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.vlpla_ = value
            }
        }
    }

    open var vlpla: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.vlpla))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.vlpla, to: StringValue.of(optional: value))
        }
    }

    open class var vltyp: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.vltyp_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.vltyp_ = value
            }
        }
    }

    open var vltyp: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.vltyp))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.vltyp, to: StringValue.of(optional: value))
        }
    }

    open class var volum: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.volum_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.volum_ = value
            }
        }
    }

    open var volum: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTask.volum))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.volum, to: DecimalValue.of(optional: value))
        }
    }

    open class var vsola: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.vsola_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.vsola_ = value
            }
        }
    }

    open var vsola: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTask.vsola))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.vsola, to: DecimalValue.of(optional: value))
        }
    }

    open class var vsolm: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.vsolm_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.vsolm_ = value
            }
        }
    }

    open var vsolm: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTask.vsolm))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.vsolm, to: DecimalValue.of(optional: value))
        }
    }

    open class var weight: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.weight_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.weight_ = value
            }
        }
    }

    open var weight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: WarehouseTask.weight))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.weight, to: DecimalValue.of(optional: value))
        }
    }

    open class var who: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.who_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.who_ = value
            }
        }
    }

    open var who: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.who))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.who, to: StringValue.of(optional: value))
        }
    }

    open class var zeiei: Property {
        get {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                return WarehouseTask.zeiei_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseTask.self)
            defer { objc_sync_exit(WarehouseTask.self) }
            do {
                WarehouseTask.zeiei_ = value
            }
        }
    }

    open var zeiei: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseTask.zeiei))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseTask.zeiei, to: StringValue.of(optional: value))
        }
    }
}
