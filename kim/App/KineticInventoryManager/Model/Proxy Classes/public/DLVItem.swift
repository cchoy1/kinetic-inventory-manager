// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class DLVItem: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var inboundDeliveryItemUUID_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDeliveryItemUUID")

    private static var inboundDeliveryUUID_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDeliveryUUID")

    private static var batch_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "Batch")

    private static var countryOfOrigin_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "CountryOfOrigin")

    private static var bsKeyPO_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "BSKeyPO")

    private static var productionDate_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ProductionDate")

    private static var docNoPO_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "DocNoPO")

    private static var inboundDelivery_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDelivery")

    private static var inboundDeliveryItem_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDeliveryItem")

    private static var inboundDeliveryItemCategory_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDeliveryItemCategory")

    private static var inboundDeliveryItemType_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDeliveryItemType")

    private static var itemDeliveryQuantity_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ItemDeliveryQuantity")

    private static var itemDeliveryQuantityUnit_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ItemDeliveryQuantityUnit")

    private static var packedQuantity_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PackedQuantity")

    private static var packOpenQuantity_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PackOpenQuantity")

    private static var putawayOpenQuantity_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PutawayOpenQuantity")

    private static var batchCrtInd_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "BatchCrtInd")

    private static var batchVendor_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "BatchVendor")

    private static var relevantForPacking_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "RelevantForPacking")

    private static var alternativeQuantityUnit_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "AlternativeQuantityUnit")

    private static var itemNoPO_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ItemNoPO")

    private static var lastChangedDateTime_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "LastChangedDateTime")

    private static var numberOfHU_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "NumberOfHU")

    private static var planningPutawayStatus_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PlanningPutawayStatus")

    private static var planningPutawayStatusText_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PlanningPutawayStatusText")

    private static var goodsReceiptStatus_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "GoodsReceiptStatus")

    private static var goodsReceiptStatusText_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "GoodsReceiptStatusText")

    private static var productUUID_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ProductUUID")

    private static var product_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "Product")

    private static var productName_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ProductName")

    private static var sledBbd_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "SledBbd")

    private static var inboundDeliveryItemUUIDTemplate_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDeliveryItemUUIDTemplate")

    private static var warehouseNumber_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "WarehouseNumber")

    private static var entitled_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "Entitled")

    private static var uxFcBatch_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFc_Batch")

    private static var uxFcItemDeliveryQuantity_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFc_ItemDeliveryQuantity")

    private static var uxFcItemDeliveryQuantityUnit_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFc_ItemDeliveryQuantityUnit")

    private static var uxFcASplit_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFcA_Split")

    private static var uxFcACreateTask_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFcA_CreateTask")

    private static var uxFcADeletable_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFcA_Deletable")

    private static var uxFcAUpdatable_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFcA_Updatable")

    private static var uxFcACreateBatch_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFcA_CreateBatch")

    private static var missingWTQty_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "MissingWTQty")

    private static var missingWTQtyUoM_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "MissingWTQtyUoM")

    private static var openWTQty_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "OpenWTQty")

    private static var openWTQtyUoM_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "OpenWTQtyUoM")

    private static var existsWT_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ExistsWT")

    private static var stockType_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "StockType")

    private static var stockTypeName_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "StockTypeName")

    private static var uxFcStockType_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFc_StockType")

    private static var docNoPPO_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "DocNoPPO")

    private static var blockedOverallStatus_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "BlockedOverallStatus")

    private static var blockedOverallStatusName_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "BlockedOverallStatusName")

    private static var packingStatus_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PackingStatus")

    private static var packingStatusName_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PackingStatusName")

    private static var vendorProduct_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "VendorProduct")

    private static var stockDocument_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "StockDocument")

    private static var stockItem_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "StockItem")

    private static var stockDocumentType_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "StockDocumentType")

    private static var stockDocumentTypeText_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "StockDocumentTypeText")

    private static var inspectionLot_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InspectionLot")

    private static var topHU_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "TopHU")

    private static var huSingleItems_: Property = Ec1Metadata.EntityTypes.dlvItem.property(withName: "HUSingleItems")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.dlvItem)
    }

    open class var alternativeQuantityUnit: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.alternativeQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.alternativeQuantityUnit_ = value
            }
        }
    }

    open var alternativeQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.alternativeQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.alternativeQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [DLVItem] {
        return ArrayConverter.convert(from.toArray(), [DLVItem]())
    }

    open class var batch: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.batch_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.batch_ = value
            }
        }
    }

    open var batch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.batch))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.batch, to: StringValue.of(optional: value))
        }
    }

    open class var batchCrtInd: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.batchCrtInd_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.batchCrtInd_ = value
            }
        }
    }

    open var batchCrtInd: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVItem.batchCrtInd))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.batchCrtInd, to: BooleanValue.of(optional: value))
        }
    }

    open class var batchVendor: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.batchVendor_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.batchVendor_ = value
            }
        }
    }

    open var batchVendor: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.batchVendor))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.batchVendor, to: StringValue.of(optional: value))
        }
    }

    open class var blockedOverallStatus: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.blockedOverallStatus_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.blockedOverallStatus_ = value
            }
        }
    }

    open var blockedOverallStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.blockedOverallStatus))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.blockedOverallStatus, to: StringValue.of(optional: value))
        }
    }

    open class var blockedOverallStatusName: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.blockedOverallStatusName_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.blockedOverallStatusName_ = value
            }
        }
    }

    open var blockedOverallStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.blockedOverallStatusName))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.blockedOverallStatusName, to: StringValue.of(optional: value))
        }
    }

    open class var bsKeyPO: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.bsKeyPO_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.bsKeyPO_ = value
            }
        }
    }

    open var bsKeyPO: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.bsKeyPO))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.bsKeyPO, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> DLVItem {
        return CastRequired<DLVItem>.from(self.copyEntity())
    }

    open class var countryOfOrigin: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.countryOfOrigin_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.countryOfOrigin_ = value
            }
        }
    }

    open var countryOfOrigin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.countryOfOrigin))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.countryOfOrigin, to: StringValue.of(optional: value))
        }
    }

    open class var docNoPO: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.docNoPO_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.docNoPO_ = value
            }
        }
    }

    open var docNoPO: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.docNoPO))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.docNoPO, to: StringValue.of(optional: value))
        }
    }

    open class var docNoPPO: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.docNoPPO_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.docNoPPO_ = value
            }
        }
    }

    open var docNoPPO: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.docNoPPO))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.docNoPPO, to: StringValue.of(optional: value))
        }
    }

    open class var entitled: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.entitled_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.entitled_ = value
            }
        }
    }

    open var entitled: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.entitled))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.entitled, to: StringValue.of(optional: value))
        }
    }

    open class var existsWT: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.existsWT_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.existsWT_ = value
            }
        }
    }

    open var existsWT: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVItem.existsWT))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.existsWT, to: BooleanValue.of(optional: value))
        }
    }

    open class var goodsReceiptStatus: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.goodsReceiptStatus_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.goodsReceiptStatus_ = value
            }
        }
    }

    open var goodsReceiptStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.goodsReceiptStatus))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.goodsReceiptStatus, to: StringValue.of(optional: value))
        }
    }

    open class var goodsReceiptStatusText: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.goodsReceiptStatusText_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.goodsReceiptStatusText_ = value
            }
        }
    }

    open var goodsReceiptStatusText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.goodsReceiptStatusText))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.goodsReceiptStatusText, to: StringValue.of(optional: value))
        }
    }

    open class var huSingleItems: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.huSingleItems_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.huSingleItems_ = value
            }
        }
    }

    open var huSingleItems: [HUSingleItem] {
        get {
            return ArrayConverter.convert(DLVItem.huSingleItems.entityList(from: self).toArray(), [HUSingleItem]())
        }
        set(value) {
            DLVItem.huSingleItems.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class var inboundDelivery: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.inboundDelivery_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.inboundDelivery_ = value
            }
        }
    }

    open var inboundDelivery: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.inboundDelivery))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.inboundDelivery, to: StringValue.of(optional: value))
        }
    }

    open class var inboundDeliveryItem: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.inboundDeliveryItem_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.inboundDeliveryItem_ = value
            }
        }
    }

    open var inboundDeliveryItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.inboundDeliveryItem))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.inboundDeliveryItem, to: StringValue.of(optional: value))
        }
    }

    open class var inboundDeliveryItemCategory: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.inboundDeliveryItemCategory_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.inboundDeliveryItemCategory_ = value
            }
        }
    }

    open var inboundDeliveryItemCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.inboundDeliveryItemCategory))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.inboundDeliveryItemCategory, to: StringValue.of(optional: value))
        }
    }

    open class var inboundDeliveryItemType: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.inboundDeliveryItemType_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.inboundDeliveryItemType_ = value
            }
        }
    }

    open var inboundDeliveryItemType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.inboundDeliveryItemType))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.inboundDeliveryItemType, to: StringValue.of(optional: value))
        }
    }

    open class var inboundDeliveryItemUUID: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.inboundDeliveryItemUUID_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.inboundDeliveryItemUUID_ = value
            }
        }
    }

    open var inboundDeliveryItemUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: DLVItem.inboundDeliveryItemUUID))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.inboundDeliveryItemUUID, to: value)
        }
    }

    open class var inboundDeliveryItemUUIDTemplate: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.inboundDeliveryItemUUIDTemplate_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.inboundDeliveryItemUUIDTemplate_ = value
            }
        }
    }

    open var inboundDeliveryItemUUIDTemplate: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: DLVItem.inboundDeliveryItemUUIDTemplate))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.inboundDeliveryItemUUIDTemplate, to: value)
        }
    }

    open class var inboundDeliveryUUID: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.inboundDeliveryUUID_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.inboundDeliveryUUID_ = value
            }
        }
    }

    open var inboundDeliveryUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: DLVItem.inboundDeliveryUUID))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.inboundDeliveryUUID, to: value)
        }
    }

    open class var inspectionLot: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.inspectionLot_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.inspectionLot_ = value
            }
        }
    }

    open var inspectionLot: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.inspectionLot))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.inspectionLot, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var itemDeliveryQuantity: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.itemDeliveryQuantity_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.itemDeliveryQuantity_ = value
            }
        }
    }

    open var itemDeliveryQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: DLVItem.itemDeliveryQuantity))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.itemDeliveryQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemDeliveryQuantityUnit: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.itemDeliveryQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.itemDeliveryQuantityUnit_ = value
            }
        }
    }

    open var itemDeliveryQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.itemDeliveryQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.itemDeliveryQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var itemNoPO: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.itemNoPO_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.itemNoPO_ = value
            }
        }
    }

    open var itemNoPO: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: DLVItem.itemNoPO))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.itemNoPO, to: IntegerValue.of(optional: value))
        }
    }

    open class func key(inboundDeliveryItemUUID: GuidValue?, inboundDeliveryUUID: GuidValue?) -> EntityKey {
        return EntityKey().with(name: "InboundDeliveryItemUUID", value: inboundDeliveryItemUUID).with(name: "InboundDeliveryUUID", value: inboundDeliveryUUID)
    }

    open class var lastChangedDateTime: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.lastChangedDateTime_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.lastChangedDateTime_ = value
            }
        }
    }

    open var lastChangedDateTime: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: DLVItem.lastChangedDateTime))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.lastChangedDateTime, to: value)
        }
    }

    open class var missingWTQty: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.missingWTQty_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.missingWTQty_ = value
            }
        }
    }

    open var missingWTQty: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: DLVItem.missingWTQty))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.missingWTQty, to: DecimalValue.of(optional: value))
        }
    }

    open class var missingWTQtyUoM: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.missingWTQtyUoM_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.missingWTQtyUoM_ = value
            }
        }
    }

    open var missingWTQtyUoM: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.missingWTQtyUoM))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.missingWTQtyUoM, to: StringValue.of(optional: value))
        }
    }

    open class var numberOfHU: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.numberOfHU_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.numberOfHU_ = value
            }
        }
    }

    open var numberOfHU: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: DLVItem.numberOfHU))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.numberOfHU, to: IntValue.of(optional: value))
        }
    }

    open var old: DLVItem {
        return CastRequired<DLVItem>.from(self.oldEntity)
    }

    open class var openWTQty: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.openWTQty_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.openWTQty_ = value
            }
        }
    }

    open var openWTQty: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: DLVItem.openWTQty))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.openWTQty, to: DecimalValue.of(optional: value))
        }
    }

    open class var openWTQtyUoM: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.openWTQtyUoM_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.openWTQtyUoM_ = value
            }
        }
    }

    open var openWTQtyUoM: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.openWTQtyUoM))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.openWTQtyUoM, to: StringValue.of(optional: value))
        }
    }

    open class var packOpenQuantity: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.packOpenQuantity_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.packOpenQuantity_ = value
            }
        }
    }

    open var packOpenQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: DLVItem.packOpenQuantity))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.packOpenQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var packedQuantity: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.packedQuantity_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.packedQuantity_ = value
            }
        }
    }

    open var packedQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: DLVItem.packedQuantity))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.packedQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var packingStatus: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.packingStatus_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.packingStatus_ = value
            }
        }
    }

    open var packingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.packingStatus))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.packingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var packingStatusName: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.packingStatusName_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.packingStatusName_ = value
            }
        }
    }

    open var packingStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.packingStatusName))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.packingStatusName, to: StringValue.of(optional: value))
        }
    }

    open class var planningPutawayStatus: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.planningPutawayStatus_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.planningPutawayStatus_ = value
            }
        }
    }

    open var planningPutawayStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.planningPutawayStatus))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.planningPutawayStatus, to: StringValue.of(optional: value))
        }
    }

    open class var planningPutawayStatusText: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.planningPutawayStatusText_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.planningPutawayStatusText_ = value
            }
        }
    }

    open var planningPutawayStatusText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.planningPutawayStatusText))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.planningPutawayStatusText, to: StringValue.of(optional: value))
        }
    }

    open class var product: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.product_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.product_ = value
            }
        }
    }

    open var product: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.product))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.product, to: StringValue.of(optional: value))
        }
    }

    open class var productName: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.productName_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.productName_ = value
            }
        }
    }

    open var productName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.productName))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.productName, to: StringValue.of(optional: value))
        }
    }

    open class var productUUID: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.productUUID_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.productUUID_ = value
            }
        }
    }

    open var productUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: DLVItem.productUUID))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.productUUID, to: value)
        }
    }

    open class var productionDate: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.productionDate_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.productionDate_ = value
            }
        }
    }

    open var productionDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: DLVItem.productionDate))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.productionDate, to: value)
        }
    }

    open class var putawayOpenQuantity: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.putawayOpenQuantity_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.putawayOpenQuantity_ = value
            }
        }
    }

    open var putawayOpenQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: DLVItem.putawayOpenQuantity))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.putawayOpenQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var relevantForPacking: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.relevantForPacking_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.relevantForPacking_ = value
            }
        }
    }

    open var relevantForPacking: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVItem.relevantForPacking))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.relevantForPacking, to: BooleanValue.of(optional: value))
        }
    }

    open class var sledBbd: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.sledBbd_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.sledBbd_ = value
            }
        }
    }

    open var sledBbd: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: DLVItem.sledBbd))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.sledBbd, to: value)
        }
    }

    open class var stockDocument: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.stockDocument_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.stockDocument_ = value
            }
        }
    }

    open var stockDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.stockDocument))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.stockDocument, to: StringValue.of(optional: value))
        }
    }

    open class var stockDocumentType: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.stockDocumentType_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.stockDocumentType_ = value
            }
        }
    }

    open var stockDocumentType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.stockDocumentType))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.stockDocumentType, to: StringValue.of(optional: value))
        }
    }

    open class var stockDocumentTypeText: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.stockDocumentTypeText_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.stockDocumentTypeText_ = value
            }
        }
    }

    open var stockDocumentTypeText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.stockDocumentTypeText))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.stockDocumentTypeText, to: StringValue.of(optional: value))
        }
    }

    open class var stockItem: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.stockItem_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.stockItem_ = value
            }
        }
    }

    open var stockItem: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: DLVItem.stockItem))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.stockItem, to: IntValue.of(optional: value))
        }
    }

    open class var stockType: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.stockType_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.stockType_ = value
            }
        }
    }

    open var stockType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.stockType))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.stockType, to: StringValue.of(optional: value))
        }
    }

    open class var stockTypeName: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.stockTypeName_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.stockTypeName_ = value
            }
        }
    }

    open var stockTypeName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.stockTypeName))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.stockTypeName, to: StringValue.of(optional: value))
        }
    }

    open class var topHU: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.topHU_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.topHU_ = value
            }
        }
    }

    open var topHU: [HUHead] {
        get {
            return ArrayConverter.convert(DLVItem.topHU.entityList(from: self).toArray(), [HUHead]())
        }
        set(value) {
            DLVItem.topHU.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class var uxFcACreateBatch: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.uxFcACreateBatch_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.uxFcACreateBatch_ = value
            }
        }
    }

    open var uxFcACreateBatch: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVItem.uxFcACreateBatch))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.uxFcACreateBatch, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcACreateTask: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.uxFcACreateTask_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.uxFcACreateTask_ = value
            }
        }
    }

    open var uxFcACreateTask: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVItem.uxFcACreateTask))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.uxFcACreateTask, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcADeletable: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.uxFcADeletable_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.uxFcADeletable_ = value
            }
        }
    }

    open var uxFcADeletable: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVItem.uxFcADeletable))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.uxFcADeletable, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcASplit: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.uxFcASplit_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.uxFcASplit_ = value
            }
        }
    }

    open var uxFcASplit: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVItem.uxFcASplit))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.uxFcASplit, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcAUpdatable: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.uxFcAUpdatable_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.uxFcAUpdatable_ = value
            }
        }
    }

    open var uxFcAUpdatable: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: DLVItem.uxFcAUpdatable))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.uxFcAUpdatable, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcBatch: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.uxFcBatch_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.uxFcBatch_ = value
            }
        }
    }

    open var uxFcBatch: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: DLVItem.uxFcBatch))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.uxFcBatch, to: UnsignedByte.of(optional: value))
        }
    }

    open class var uxFcItemDeliveryQuantity: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.uxFcItemDeliveryQuantity_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.uxFcItemDeliveryQuantity_ = value
            }
        }
    }

    open var uxFcItemDeliveryQuantity: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: DLVItem.uxFcItemDeliveryQuantity))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.uxFcItemDeliveryQuantity, to: UnsignedByte.of(optional: value))
        }
    }

    open class var uxFcItemDeliveryQuantityUnit: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.uxFcItemDeliveryQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.uxFcItemDeliveryQuantityUnit_ = value
            }
        }
    }

    open var uxFcItemDeliveryQuantityUnit: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: DLVItem.uxFcItemDeliveryQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.uxFcItemDeliveryQuantityUnit, to: UnsignedByte.of(optional: value))
        }
    }

    open class var uxFcStockType: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.uxFcStockType_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.uxFcStockType_ = value
            }
        }
    }

    open var uxFcStockType: Int? {
        get {
            return UnsignedByte.optional(self.optionalValue(for: DLVItem.uxFcStockType))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.uxFcStockType, to: UnsignedByte.of(optional: value))
        }
    }

    open class var vendorProduct: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.vendorProduct_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.vendorProduct_ = value
            }
        }
    }

    open var vendorProduct: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.vendorProduct))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.vendorProduct, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseNumber: Property {
        get {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                return DLVItem.warehouseNumber_
            }
        }
        set(value) {
            objc_sync_enter(DLVItem.self)
            defer { objc_sync_exit(DLVItem.self) }
            do {
                DLVItem.warehouseNumber_ = value
            }
        }
    }

    open var warehouseNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: DLVItem.warehouseNumber))
        }
        set(value) {
            self.setOptionalValue(for: DLVItem.warehouseNumber, to: StringValue.of(optional: value))
        }
    }
}
