// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AOutbDeliveryPartnerType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var addressID_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "AddressID")

    private static var contactPerson_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "ContactPerson")

    private static var customer_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "Customer")

    private static var partnerFunction_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "PartnerFunction")

    private static var personnel_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "Personnel")

    private static var sdDocument_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "SDDocument")

    private static var sdDocumentItem_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "SDDocumentItem")

    private static var supplier_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "Supplier")

    private static var toAddress_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "to_Address")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType)
    }

    open class var addressID: Property {
        get {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                return AOutbDeliveryPartnerType.addressID_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                AOutbDeliveryPartnerType.addressID_ = value
            }
        }
    }

    open var addressID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryPartnerType.addressID))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryPartnerType.addressID, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [AOutbDeliveryPartnerType] {
        return ArrayConverter.convert(from.toArray(), [AOutbDeliveryPartnerType]())
    }

    open class var contactPerson: Property {
        get {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                return AOutbDeliveryPartnerType.contactPerson_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                AOutbDeliveryPartnerType.contactPerson_ = value
            }
        }
    }

    open var contactPerson: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryPartnerType.contactPerson))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryPartnerType.contactPerson, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> AOutbDeliveryPartnerType {
        return CastRequired<AOutbDeliveryPartnerType>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                return AOutbDeliveryPartnerType.customer_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                AOutbDeliveryPartnerType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryPartnerType.customer))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryPartnerType.customer, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(partnerFunction: String?, sdDocument: String?) -> EntityKey {
        return EntityKey().with(name: "PartnerFunction", value: StringValue.of(optional: partnerFunction)).with(name: "SDDocument", value: StringValue.of(optional: sdDocument))
    }

    open var old: AOutbDeliveryPartnerType {
        return CastRequired<AOutbDeliveryPartnerType>.from(self.oldEntity)
    }

    open class var partnerFunction: Property {
        get {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                return AOutbDeliveryPartnerType.partnerFunction_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                AOutbDeliveryPartnerType.partnerFunction_ = value
            }
        }
    }

    open var partnerFunction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryPartnerType.partnerFunction))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryPartnerType.partnerFunction, to: StringValue.of(optional: value))
        }
    }

    open class var personnel: Property {
        get {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                return AOutbDeliveryPartnerType.personnel_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                AOutbDeliveryPartnerType.personnel_ = value
            }
        }
    }

    open var personnel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryPartnerType.personnel))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryPartnerType.personnel, to: StringValue.of(optional: value))
        }
    }

    open class var sdDocument: Property {
        get {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                return AOutbDeliveryPartnerType.sdDocument_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                AOutbDeliveryPartnerType.sdDocument_ = value
            }
        }
    }

    open var sdDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryPartnerType.sdDocument))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryPartnerType.sdDocument, to: StringValue.of(optional: value))
        }
    }

    open class var sdDocumentItem: Property {
        get {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                return AOutbDeliveryPartnerType.sdDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                AOutbDeliveryPartnerType.sdDocumentItem_ = value
            }
        }
    }

    open var sdDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryPartnerType.sdDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryPartnerType.sdDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var supplier: Property {
        get {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                return AOutbDeliveryPartnerType.supplier_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                AOutbDeliveryPartnerType.supplier_ = value
            }
        }
    }

    open var supplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryPartnerType.supplier))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryPartnerType.supplier, to: StringValue.of(optional: value))
        }
    }

    open class var toAddress: Property {
        get {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                return AOutbDeliveryPartnerType.toAddress_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryPartnerType.self)
            defer { objc_sync_exit(AOutbDeliveryPartnerType.self) }
            do {
                AOutbDeliveryPartnerType.toAddress_ = value
            }
        }
    }

    open var toAddress: AOutbDeliveryAddressType? {
        get {
            return CastOptional<AOutbDeliveryAddressType>.from(self.optionalValue(for: AOutbDeliveryPartnerType.toAddress))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryPartnerType.toAddress, to: value)
        }
    }
}
