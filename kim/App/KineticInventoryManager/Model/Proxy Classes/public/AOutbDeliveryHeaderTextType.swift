// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AOutbDeliveryHeaderTextType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var deliveryDocument_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType.property(withName: "DeliveryDocument")

    private static var textElement_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType.property(withName: "TextElement")

    private static var language_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType.property(withName: "Language")

    private static var textElementDescription_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType.property(withName: "TextElementDescription")

    private static var textElementText_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType.property(withName: "TextElementText")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType)
    }

    open class func array(from: EntityValueList) -> [AOutbDeliveryHeaderTextType] {
        return ArrayConverter.convert(from.toArray(), [AOutbDeliveryHeaderTextType]())
    }

    open func copy() -> AOutbDeliveryHeaderTextType {
        return CastRequired<AOutbDeliveryHeaderTextType>.from(self.copyEntity())
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderTextType.self) }
            do {
                return AOutbDeliveryHeaderTextType.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderTextType.self) }
            do {
                AOutbDeliveryHeaderTextType.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderTextType.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderTextType.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(deliveryDocument: String?, textElement: String?, language: String?) -> EntityKey {
        return EntityKey().with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "TextElement", value: StringValue.of(optional: textElement)).with(name: "Language", value: StringValue.of(optional: language))
    }

    open class var language: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderTextType.self) }
            do {
                return AOutbDeliveryHeaderTextType.language_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderTextType.self) }
            do {
                AOutbDeliveryHeaderTextType.language_ = value
            }
        }
    }

    open var language: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderTextType.language))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderTextType.language, to: StringValue.of(optional: value))
        }
    }

    open var old: AOutbDeliveryHeaderTextType {
        return CastRequired<AOutbDeliveryHeaderTextType>.from(self.oldEntity)
    }

    open class var textElement: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderTextType.self) }
            do {
                return AOutbDeliveryHeaderTextType.textElement_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderTextType.self) }
            do {
                AOutbDeliveryHeaderTextType.textElement_ = value
            }
        }
    }

    open var textElement: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderTextType.textElement))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderTextType.textElement, to: StringValue.of(optional: value))
        }
    }

    open class var textElementDescription: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderTextType.self) }
            do {
                return AOutbDeliveryHeaderTextType.textElementDescription_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderTextType.self) }
            do {
                AOutbDeliveryHeaderTextType.textElementDescription_ = value
            }
        }
    }

    open var textElementDescription: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderTextType.textElementDescription))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderTextType.textElementDescription, to: StringValue.of(optional: value))
        }
    }

    open class var textElementText: Property {
        get {
            objc_sync_enter(AOutbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderTextType.self) }
            do {
                return AOutbDeliveryHeaderTextType.textElementText_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AOutbDeliveryHeaderTextType.self) }
            do {
                AOutbDeliveryHeaderTextType.textElementText_ = value
            }
        }
    }

    open var textElementText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryHeaderTextType.textElementText))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryHeaderTextType.textElementText, to: StringValue.of(optional: value))
        }
    }
}
