// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class ConfirmPickType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var outboundDeliveryUUID_: Property = Ec1Metadata.EntityTypes.confirmPickType.property(withName: "OutboundDeliveryUUID")

    private static var outboundDeliveryOrderItemUUID_: Property = Ec1Metadata.EntityTypes.confirmPickType.property(withName: "OutboundDeliveryOrderItemUUID")

    private static var returnBool_: Property = Ec1Metadata.EntityTypes.confirmPickType.property(withName: "ReturnBool")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.confirmPickType)
    }

    open class func array(from: EntityValueList) -> [ConfirmPickType] {
        return ArrayConverter.convert(from.toArray(), [ConfirmPickType]())
    }

    open func copy() -> ConfirmPickType {
        return CastRequired<ConfirmPickType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(outboundDeliveryUUID: GuidValue?, outboundDeliveryOrderItemUUID: GuidValue?) -> EntityKey {
        return EntityKey().with(name: "OutboundDeliveryUUID", value: outboundDeliveryUUID).with(name: "OutboundDeliveryOrderItemUUID", value: outboundDeliveryOrderItemUUID)
    }

    open var old: ConfirmPickType {
        return CastRequired<ConfirmPickType>.from(self.oldEntity)
    }

    open class var outboundDeliveryOrderItemUUID: Property {
        get {
            objc_sync_enter(ConfirmPickType.self)
            defer { objc_sync_exit(ConfirmPickType.self) }
            do {
                return ConfirmPickType.outboundDeliveryOrderItemUUID_
            }
        }
        set(value) {
            objc_sync_enter(ConfirmPickType.self)
            defer { objc_sync_exit(ConfirmPickType.self) }
            do {
                ConfirmPickType.outboundDeliveryOrderItemUUID_ = value
            }
        }
    }

    open var outboundDeliveryOrderItemUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: ConfirmPickType.outboundDeliveryOrderItemUUID))
        }
        set(value) {
            self.setOptionalValue(for: ConfirmPickType.outboundDeliveryOrderItemUUID, to: value)
        }
    }

    open class var outboundDeliveryUUID: Property {
        get {
            objc_sync_enter(ConfirmPickType.self)
            defer { objc_sync_exit(ConfirmPickType.self) }
            do {
                return ConfirmPickType.outboundDeliveryUUID_
            }
        }
        set(value) {
            objc_sync_enter(ConfirmPickType.self)
            defer { objc_sync_exit(ConfirmPickType.self) }
            do {
                ConfirmPickType.outboundDeliveryUUID_ = value
            }
        }
    }

    open var outboundDeliveryUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: ConfirmPickType.outboundDeliveryUUID))
        }
        set(value) {
            self.setOptionalValue(for: ConfirmPickType.outboundDeliveryUUID, to: value)
        }
    }

    open class var returnBool: Property {
        get {
            objc_sync_enter(ConfirmPickType.self)
            defer { objc_sync_exit(ConfirmPickType.self) }
            do {
                return ConfirmPickType.returnBool_
            }
        }
        set(value) {
            objc_sync_enter(ConfirmPickType.self)
            defer { objc_sync_exit(ConfirmPickType.self) }
            do {
                ConfirmPickType.returnBool_ = value
            }
        }
    }

    open var returnBool: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: ConfirmPickType.returnBool))
        }
        set(value) {
            self.setOptionalValue(for: ConfirmPickType.returnBool, to: BooleanValue.of(optional: value))
        }
    }
}
