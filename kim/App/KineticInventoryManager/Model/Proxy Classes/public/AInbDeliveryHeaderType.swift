// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AInbDeliveryHeaderType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var actualDeliveryRoute_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ActualDeliveryRoute")

    private static var actualGoodsMovementDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ActualGoodsMovementDate")

    private static var actualGoodsMovementTime_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ActualGoodsMovementTime")

    private static var billingDocumentDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "BillingDocumentDate")

    private static var billOfLading_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "BillOfLading")

    private static var completeDeliveryIsDefined_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CompleteDeliveryIsDefined")

    private static var confirmationTime_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ConfirmationTime")

    private static var createdByUser_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CreatedByUser")

    private static var creationDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CreationDate")

    private static var creationTime_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CreationTime")

    private static var customerGroup_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CustomerGroup")

    private static var deliveryBlockReason_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryBlockReason")

    private static var deliveryDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryDate")

    private static var deliveryDocument_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryDocument")

    private static var deliveryDocumentBySupplier_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryDocumentBySupplier")

    private static var deliveryDocumentType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryDocumentType")

    private static var deliveryIsInPlant_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryIsInPlant")

    private static var deliveryPriority_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryPriority")

    private static var deliveryTime_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryTime")

    private static var deliveryVersion_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryVersion")

    private static var depreciationPercentage_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DepreciationPercentage")

    private static var distrStatusByDecentralizedWrhs_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DistrStatusByDecentralizedWrhs")

    private static var documentDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DocumentDate")

    private static var externalIdentificationType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ExternalIdentificationType")

    private static var externalTransportSystem_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ExternalTransportSystem")

    private static var factoryCalendarByCustomer_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "FactoryCalendarByCustomer")

    private static var goodsIssueOrReceiptSlipNumber_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "GoodsIssueOrReceiptSlipNumber")

    private static var goodsIssueTime_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "GoodsIssueTime")

    private static var handlingUnitInStock_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HandlingUnitInStock")

    private static var hdrGeneralIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HdrGeneralIncompletionStatus")

    private static var hdrGoodsMvtIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HdrGoodsMvtIncompletionStatus")

    private static var headerBillgIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderBillgIncompletionStatus")

    private static var headerBillingBlockReason_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderBillingBlockReason")

    private static var headerDelivIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderDelivIncompletionStatus")

    private static var headerGrossWeight_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderGrossWeight")

    private static var headerNetWeight_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderNetWeight")

    private static var headerPackingIncompletionSts_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderPackingIncompletionSts")

    private static var headerPickgIncompletionStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderPickgIncompletionStatus")

    private static var headerVolume_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderVolume")

    private static var headerVolumeUnit_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderVolumeUnit")

    private static var headerWeightUnit_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderWeightUnit")

    private static var incotermsClassification_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IncotermsClassification")

    private static var incotermsTransferLocation_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IncotermsTransferLocation")

    private static var intercompanyBillingDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IntercompanyBillingDate")

    private static var internalFinancialDocument_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "InternalFinancialDocument")

    private static var isDeliveryForSingleWarehouse_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IsDeliveryForSingleWarehouse")

    private static var isExportDelivery_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IsExportDelivery")

    private static var lastChangeDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LastChangeDate")

    private static var lastChangedByUser_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LastChangedByUser")

    private static var loadingDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LoadingDate")

    private static var loadingPoint_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LoadingPoint")

    private static var loadingTime_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LoadingTime")

    private static var meansOfTransport_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "MeansOfTransport")

    private static var meansOfTransportRefMaterial_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "MeansOfTransportRefMaterial")

    private static var meansOfTransportType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "MeansOfTransportType")

    private static var orderCombinationIsAllowed_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OrderCombinationIsAllowed")

    private static var orderID_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OrderID")

    private static var overallDelivConfStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallDelivConfStatus")

    private static var overallDelivReltdBillgStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallDelivReltdBillgStatus")

    private static var overallGoodsMovementStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallGoodsMovementStatus")

    private static var overallIntcoBillingStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallIntcoBillingStatus")

    private static var overallPackingStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallPackingStatus")

    private static var overallPickingConfStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallPickingConfStatus")

    private static var overallPickingStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallPickingStatus")

    private static var overallProofOfDeliveryStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallProofOfDeliveryStatus")

    private static var overallSDProcessStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallSDProcessStatus")

    private static var overallWarehouseActivityStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallWarehouseActivityStatus")

    private static var ovrlItmDelivIncompletionSts_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmDelivIncompletionSts")

    private static var ovrlItmGdsMvtIncompletionSts_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmGdsMvtIncompletionSts")

    private static var ovrlItmGeneralIncompletionSts_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmGeneralIncompletionSts")

    private static var ovrlItmPackingIncompletionSts_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmPackingIncompletionSts")

    private static var ovrlItmPickingIncompletionSts_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmPickingIncompletionSts")

    private static var paymentGuaranteeProcedure_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PaymentGuaranteeProcedure")

    private static var pickedItemsLocation_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PickedItemsLocation")

    private static var pickingDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PickingDate")

    private static var pickingTime_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PickingTime")

    private static var plannedGoodsIssueDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PlannedGoodsIssueDate")

    private static var proofOfDeliveryDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ProofOfDeliveryDate")

    private static var proposedDeliveryRoute_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ProposedDeliveryRoute")

    private static var receivingPlant_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ReceivingPlant")

    private static var routeSchedule_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "RouteSchedule")

    private static var salesDistrict_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SalesDistrict")

    private static var salesOffice_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SalesOffice")

    private static var salesOrganization_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SalesOrganization")

    private static var sdDocumentCategory_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SDDocumentCategory")

    private static var shipmentBlockReason_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShipmentBlockReason")

    private static var shippingCondition_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShippingCondition")

    private static var shippingPoint_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShippingPoint")

    private static var shippingType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShippingType")

    private static var shipToParty_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShipToParty")

    private static var soldToParty_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SoldToParty")

    private static var specialProcessingCode_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SpecialProcessingCode")

    private static var statisticsCurrency_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "StatisticsCurrency")

    private static var supplier_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "Supplier")

    private static var totalBlockStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TotalBlockStatus")

    private static var totalCreditCheckStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TotalCreditCheckStatus")

    private static var totalNumberOfPackage_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TotalNumberOfPackage")

    private static var transactionCurrency_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransactionCurrency")

    private static var transportationGroup_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransportationGroup")

    private static var transportationPlanningDate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransportationPlanningDate")

    private static var transportationPlanningStatus_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransportationPlanningStatus")

    private static var transportationPlanningTime_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransportationPlanningTime")

    private static var unloadingPointName_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "UnloadingPointName")

    private static var warehouse_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "Warehouse")

    private static var warehouseGate_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "WarehouseGate")

    private static var warehouseStagingArea_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "WarehouseStagingArea")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aInbDeliveryHeaderType)
    }

    open class var actualDeliveryRoute: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.actualDeliveryRoute_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.actualDeliveryRoute_ = value
            }
        }
    }

    open var actualDeliveryRoute: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.actualDeliveryRoute))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.actualDeliveryRoute, to: StringValue.of(optional: value))
        }
    }

    open class var actualGoodsMovementDate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.actualGoodsMovementDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.actualGoodsMovementDate_ = value
            }
        }
    }

    open var actualGoodsMovementDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.actualGoodsMovementDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.actualGoodsMovementDate, to: value)
        }
    }

    open class var actualGoodsMovementTime: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.actualGoodsMovementTime_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.actualGoodsMovementTime_ = value
            }
        }
    }

    open var actualGoodsMovementTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.actualGoodsMovementTime))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.actualGoodsMovementTime, to: value)
        }
    }

    open class func array(from: EntityValueList) -> [AInbDeliveryHeaderType] {
        return ArrayConverter.convert(from.toArray(), [AInbDeliveryHeaderType]())
    }

    open class var billOfLading: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.billOfLading_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.billOfLading_ = value
            }
        }
    }

    open var billOfLading: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.billOfLading))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.billOfLading, to: StringValue.of(optional: value))
        }
    }

    open class var billingDocumentDate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.billingDocumentDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.billingDocumentDate_ = value
            }
        }
    }

    open var billingDocumentDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.billingDocumentDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.billingDocumentDate, to: value)
        }
    }

    open class var completeDeliveryIsDefined: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.completeDeliveryIsDefined_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.completeDeliveryIsDefined_ = value
            }
        }
    }

    open var completeDeliveryIsDefined: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.completeDeliveryIsDefined))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.completeDeliveryIsDefined, to: BooleanValue.of(optional: value))
        }
    }

    open class var confirmationTime: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.confirmationTime_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.confirmationTime_ = value
            }
        }
    }

    open var confirmationTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.confirmationTime))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.confirmationTime, to: value)
        }
    }

    open func copy() -> AInbDeliveryHeaderType {
        return CastRequired<AInbDeliveryHeaderType>.from(self.copyEntity())
    }

    open class var createdByUser: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.createdByUser_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.createdByUser_ = value
            }
        }
    }

    open var createdByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.createdByUser))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.createdByUser, to: StringValue.of(optional: value))
        }
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.creationDate, to: value)
        }
    }

    open class var creationTime: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.creationTime_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.creationTime_ = value
            }
        }
    }

    open var creationTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.creationTime))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.creationTime, to: value)
        }
    }

    open class var customerGroup: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.customerGroup_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.customerGroup_ = value
            }
        }
    }

    open var customerGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.customerGroup))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.customerGroup, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryBlockReason: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.deliveryBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.deliveryBlockReason_ = value
            }
        }
    }

    open var deliveryBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.deliveryBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.deliveryBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.deliveryDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.deliveryDate_ = value
            }
        }
    }

    open var deliveryDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.deliveryDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.deliveryDate, to: value)
        }
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentBySupplier: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.deliveryDocumentBySupplier_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.deliveryDocumentBySupplier_ = value
            }
        }
    }

    open var deliveryDocumentBySupplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.deliveryDocumentBySupplier))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.deliveryDocumentBySupplier, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentType: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.deliveryDocumentType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.deliveryDocumentType_ = value
            }
        }
    }

    open var deliveryDocumentType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.deliveryDocumentType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.deliveryDocumentType, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryIsInPlant: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.deliveryIsInPlant_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.deliveryIsInPlant_ = value
            }
        }
    }

    open var deliveryIsInPlant: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.deliveryIsInPlant))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.deliveryIsInPlant, to: BooleanValue.of(optional: value))
        }
    }

    open class var deliveryPriority: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.deliveryPriority_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.deliveryPriority_ = value
            }
        }
    }

    open var deliveryPriority: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.deliveryPriority))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.deliveryPriority, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryTime: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.deliveryTime_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.deliveryTime_ = value
            }
        }
    }

    open var deliveryTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.deliveryTime))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.deliveryTime, to: value)
        }
    }

    open class var deliveryVersion: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.deliveryVersion_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.deliveryVersion_ = value
            }
        }
    }

    open var deliveryVersion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.deliveryVersion))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.deliveryVersion, to: StringValue.of(optional: value))
        }
    }

    open class var depreciationPercentage: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.depreciationPercentage_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.depreciationPercentage_ = value
            }
        }
    }

    open var depreciationPercentage: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.depreciationPercentage))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.depreciationPercentage, to: DecimalValue.of(optional: value))
        }
    }

    open class var distrStatusByDecentralizedWrhs: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.distrStatusByDecentralizedWrhs_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.distrStatusByDecentralizedWrhs_ = value
            }
        }
    }

    open var distrStatusByDecentralizedWrhs: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.distrStatusByDecentralizedWrhs))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.distrStatusByDecentralizedWrhs, to: StringValue.of(optional: value))
        }
    }

    open class var documentDate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.documentDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.documentDate_ = value
            }
        }
    }

    open var documentDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.documentDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.documentDate, to: value)
        }
    }

    open class var externalIdentificationType: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.externalIdentificationType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.externalIdentificationType_ = value
            }
        }
    }

    open var externalIdentificationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.externalIdentificationType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.externalIdentificationType, to: StringValue.of(optional: value))
        }
    }

    open class var externalTransportSystem: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.externalTransportSystem_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.externalTransportSystem_ = value
            }
        }
    }

    open var externalTransportSystem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.externalTransportSystem))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.externalTransportSystem, to: StringValue.of(optional: value))
        }
    }

    open class var factoryCalendarByCustomer: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.factoryCalendarByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.factoryCalendarByCustomer_ = value
            }
        }
    }

    open var factoryCalendarByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.factoryCalendarByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.factoryCalendarByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var goodsIssueOrReceiptSlipNumber: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber_ = value
            }
        }
    }

    open var goodsIssueOrReceiptSlipNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber, to: StringValue.of(optional: value))
        }
    }

    open class var goodsIssueTime: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.goodsIssueTime_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.goodsIssueTime_ = value
            }
        }
    }

    open var goodsIssueTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.goodsIssueTime))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.goodsIssueTime, to: value)
        }
    }

    open class var handlingUnitInStock: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.handlingUnitInStock_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.handlingUnitInStock_ = value
            }
        }
    }

    open var handlingUnitInStock: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.handlingUnitInStock))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.handlingUnitInStock, to: StringValue.of(optional: value))
        }
    }

    open class var hdrGeneralIncompletionStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.hdrGeneralIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.hdrGeneralIncompletionStatus_ = value
            }
        }
    }

    open var hdrGeneralIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.hdrGeneralIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.hdrGeneralIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var hdrGoodsMvtIncompletionStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus_ = value
            }
        }
    }

    open var hdrGoodsMvtIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var headerBillgIncompletionStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.headerBillgIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.headerBillgIncompletionStatus_ = value
            }
        }
    }

    open var headerBillgIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.headerBillgIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.headerBillgIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var headerBillingBlockReason: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.headerBillingBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.headerBillingBlockReason_ = value
            }
        }
    }

    open var headerBillingBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.headerBillingBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.headerBillingBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var headerDelivIncompletionStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.headerDelivIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.headerDelivIncompletionStatus_ = value
            }
        }
    }

    open var headerDelivIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.headerDelivIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.headerDelivIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var headerGrossWeight: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.headerGrossWeight_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.headerGrossWeight_ = value
            }
        }
    }

    open var headerGrossWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.headerGrossWeight))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.headerGrossWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var headerNetWeight: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.headerNetWeight_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.headerNetWeight_ = value
            }
        }
    }

    open var headerNetWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.headerNetWeight))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.headerNetWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var headerPackingIncompletionSts: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.headerPackingIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.headerPackingIncompletionSts_ = value
            }
        }
    }

    open var headerPackingIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.headerPackingIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.headerPackingIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var headerPickgIncompletionStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.headerPickgIncompletionStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.headerPickgIncompletionStatus_ = value
            }
        }
    }

    open var headerPickgIncompletionStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.headerPickgIncompletionStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.headerPickgIncompletionStatus, to: StringValue.of(optional: value))
        }
    }

    open class var headerVolume: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.headerVolume_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.headerVolume_ = value
            }
        }
    }

    open var headerVolume: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.headerVolume))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.headerVolume, to: DecimalValue.of(optional: value))
        }
    }

    open class var headerVolumeUnit: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.headerVolumeUnit_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.headerVolumeUnit_ = value
            }
        }
    }

    open var headerVolumeUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.headerVolumeUnit))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.headerVolumeUnit, to: StringValue.of(optional: value))
        }
    }

    open class var headerWeightUnit: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.headerWeightUnit_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.headerWeightUnit_ = value
            }
        }
    }

    open var headerWeightUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.headerWeightUnit))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.headerWeightUnit, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsClassification: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.incotermsClassification_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.incotermsClassification_ = value
            }
        }
    }

    open var incotermsClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.incotermsClassification))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.incotermsClassification, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsTransferLocation: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.incotermsTransferLocation_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.incotermsTransferLocation_ = value
            }
        }
    }

    open var incotermsTransferLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.incotermsTransferLocation))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.incotermsTransferLocation, to: StringValue.of(optional: value))
        }
    }

    open class var intercompanyBillingDate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.intercompanyBillingDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.intercompanyBillingDate_ = value
            }
        }
    }

    open var intercompanyBillingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.intercompanyBillingDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.intercompanyBillingDate, to: value)
        }
    }

    open class var internalFinancialDocument: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.internalFinancialDocument_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.internalFinancialDocument_ = value
            }
        }
    }

    open var internalFinancialDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.internalFinancialDocument))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.internalFinancialDocument, to: StringValue.of(optional: value))
        }
    }

    open class var isDeliveryForSingleWarehouse: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.isDeliveryForSingleWarehouse_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.isDeliveryForSingleWarehouse_ = value
            }
        }
    }

    open var isDeliveryForSingleWarehouse: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.isDeliveryForSingleWarehouse))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.isDeliveryForSingleWarehouse, to: StringValue.of(optional: value))
        }
    }

    open class var isExportDelivery: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.isExportDelivery_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.isExportDelivery_ = value
            }
        }
    }

    open var isExportDelivery: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.isExportDelivery))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.isExportDelivery, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(deliveryDocument: String?) -> EntityKey {
        return EntityKey().with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument))
    }

    open class var lastChangeDate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.lastChangeDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.lastChangeDate_ = value
            }
        }
    }

    open var lastChangeDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.lastChangeDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.lastChangeDate, to: value)
        }
    }

    open class var lastChangedByUser: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.lastChangedByUser_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.lastChangedByUser_ = value
            }
        }
    }

    open var lastChangedByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.lastChangedByUser))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.lastChangedByUser, to: StringValue.of(optional: value))
        }
    }

    open class var loadingDate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.loadingDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.loadingDate_ = value
            }
        }
    }

    open var loadingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.loadingDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.loadingDate, to: value)
        }
    }

    open class var loadingPoint: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.loadingPoint_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.loadingPoint_ = value
            }
        }
    }

    open var loadingPoint: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.loadingPoint))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.loadingPoint, to: StringValue.of(optional: value))
        }
    }

    open class var loadingTime: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.loadingTime_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.loadingTime_ = value
            }
        }
    }

    open var loadingTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.loadingTime))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.loadingTime, to: value)
        }
    }

    open class var meansOfTransport: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.meansOfTransport_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.meansOfTransport_ = value
            }
        }
    }

    open var meansOfTransport: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.meansOfTransport))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.meansOfTransport, to: StringValue.of(optional: value))
        }
    }

    open class var meansOfTransportRefMaterial: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.meansOfTransportRefMaterial_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.meansOfTransportRefMaterial_ = value
            }
        }
    }

    open var meansOfTransportRefMaterial: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.meansOfTransportRefMaterial))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.meansOfTransportRefMaterial, to: StringValue.of(optional: value))
        }
    }

    open class var meansOfTransportType: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.meansOfTransportType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.meansOfTransportType_ = value
            }
        }
    }

    open var meansOfTransportType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.meansOfTransportType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.meansOfTransportType, to: StringValue.of(optional: value))
        }
    }

    open var old: AInbDeliveryHeaderType {
        return CastRequired<AInbDeliveryHeaderType>.from(self.oldEntity)
    }

    open class var orderCombinationIsAllowed: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.orderCombinationIsAllowed_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.orderCombinationIsAllowed_ = value
            }
        }
    }

    open var orderCombinationIsAllowed: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.orderCombinationIsAllowed))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.orderCombinationIsAllowed, to: BooleanValue.of(optional: value))
        }
    }

    open class var orderID: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.orderID_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.orderID_ = value
            }
        }
    }

    open var orderID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.orderID))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.orderID, to: StringValue.of(optional: value))
        }
    }

    open class var overallDelivConfStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.overallDelivConfStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.overallDelivConfStatus_ = value
            }
        }
    }

    open var overallDelivConfStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.overallDelivConfStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.overallDelivConfStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallDelivReltdBillgStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.overallDelivReltdBillgStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.overallDelivReltdBillgStatus_ = value
            }
        }
    }

    open var overallDelivReltdBillgStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.overallDelivReltdBillgStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.overallDelivReltdBillgStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallGoodsMovementStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.overallGoodsMovementStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.overallGoodsMovementStatus_ = value
            }
        }
    }

    open var overallGoodsMovementStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.overallGoodsMovementStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.overallGoodsMovementStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallIntcoBillingStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.overallIntcoBillingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.overallIntcoBillingStatus_ = value
            }
        }
    }

    open var overallIntcoBillingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.overallIntcoBillingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.overallIntcoBillingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallPackingStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.overallPackingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.overallPackingStatus_ = value
            }
        }
    }

    open var overallPackingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.overallPackingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.overallPackingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallPickingConfStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.overallPickingConfStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.overallPickingConfStatus_ = value
            }
        }
    }

    open var overallPickingConfStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.overallPickingConfStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.overallPickingConfStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallPickingStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.overallPickingStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.overallPickingStatus_ = value
            }
        }
    }

    open var overallPickingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.overallPickingStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.overallPickingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallProofOfDeliveryStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.overallProofOfDeliveryStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.overallProofOfDeliveryStatus_ = value
            }
        }
    }

    open var overallProofOfDeliveryStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.overallProofOfDeliveryStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.overallProofOfDeliveryStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallSDProcessStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.overallSDProcessStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.overallSDProcessStatus_ = value
            }
        }
    }

    open var overallSDProcessStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.overallSDProcessStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.overallSDProcessStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallWarehouseActivityStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.overallWarehouseActivityStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.overallWarehouseActivityStatus_ = value
            }
        }
    }

    open var overallWarehouseActivityStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.overallWarehouseActivityStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.overallWarehouseActivityStatus, to: StringValue.of(optional: value))
        }
    }

    open class var ovrlItmDelivIncompletionSts: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.ovrlItmDelivIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.ovrlItmDelivIncompletionSts_ = value
            }
        }
    }

    open var ovrlItmDelivIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.ovrlItmDelivIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.ovrlItmDelivIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var ovrlItmGdsMvtIncompletionSts: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts_ = value
            }
        }
    }

    open var ovrlItmGdsMvtIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var ovrlItmGeneralIncompletionSts: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.ovrlItmGeneralIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.ovrlItmGeneralIncompletionSts_ = value
            }
        }
    }

    open var ovrlItmGeneralIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.ovrlItmGeneralIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.ovrlItmGeneralIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var ovrlItmPackingIncompletionSts: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.ovrlItmPackingIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.ovrlItmPackingIncompletionSts_ = value
            }
        }
    }

    open var ovrlItmPackingIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.ovrlItmPackingIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.ovrlItmPackingIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var ovrlItmPickingIncompletionSts: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.ovrlItmPickingIncompletionSts_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.ovrlItmPickingIncompletionSts_ = value
            }
        }
    }

    open var ovrlItmPickingIncompletionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.ovrlItmPickingIncompletionSts))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.ovrlItmPickingIncompletionSts, to: StringValue.of(optional: value))
        }
    }

    open class var paymentGuaranteeProcedure: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.paymentGuaranteeProcedure_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.paymentGuaranteeProcedure_ = value
            }
        }
    }

    open var paymentGuaranteeProcedure: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.paymentGuaranteeProcedure))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.paymentGuaranteeProcedure, to: StringValue.of(optional: value))
        }
    }

    open class var pickedItemsLocation: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.pickedItemsLocation_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.pickedItemsLocation_ = value
            }
        }
    }

    open var pickedItemsLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.pickedItemsLocation))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.pickedItemsLocation, to: StringValue.of(optional: value))
        }
    }

    open class var pickingDate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.pickingDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.pickingDate_ = value
            }
        }
    }

    open var pickingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.pickingDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.pickingDate, to: value)
        }
    }

    open class var pickingTime: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.pickingTime_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.pickingTime_ = value
            }
        }
    }

    open var pickingTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.pickingTime))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.pickingTime, to: value)
        }
    }

    open class var plannedGoodsIssueDate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.plannedGoodsIssueDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.plannedGoodsIssueDate_ = value
            }
        }
    }

    open var plannedGoodsIssueDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.plannedGoodsIssueDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.plannedGoodsIssueDate, to: value)
        }
    }

    open class var proofOfDeliveryDate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.proofOfDeliveryDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.proofOfDeliveryDate_ = value
            }
        }
    }

    open var proofOfDeliveryDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.proofOfDeliveryDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.proofOfDeliveryDate, to: value)
        }
    }

    open class var proposedDeliveryRoute: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.proposedDeliveryRoute_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.proposedDeliveryRoute_ = value
            }
        }
    }

    open var proposedDeliveryRoute: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.proposedDeliveryRoute))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.proposedDeliveryRoute, to: StringValue.of(optional: value))
        }
    }

    open class var receivingPlant: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.receivingPlant_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.receivingPlant_ = value
            }
        }
    }

    open var receivingPlant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.receivingPlant))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.receivingPlant, to: StringValue.of(optional: value))
        }
    }

    open class var routeSchedule: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.routeSchedule_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.routeSchedule_ = value
            }
        }
    }

    open var routeSchedule: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.routeSchedule))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.routeSchedule, to: StringValue.of(optional: value))
        }
    }

    open class var salesDistrict: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.salesDistrict_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.salesDistrict_ = value
            }
        }
    }

    open var salesDistrict: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.salesDistrict))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.salesDistrict, to: StringValue.of(optional: value))
        }
    }

    open class var salesOffice: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.salesOffice_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.salesOffice_ = value
            }
        }
    }

    open var salesOffice: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.salesOffice))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.salesOffice, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var sdDocumentCategory: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.sdDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.sdDocumentCategory_ = value
            }
        }
    }

    open var sdDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.sdDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.sdDocumentCategory, to: StringValue.of(optional: value))
        }
    }

    open class var shipToParty: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.shipToParty_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.shipToParty_ = value
            }
        }
    }

    open var shipToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.shipToParty))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.shipToParty, to: StringValue.of(optional: value))
        }
    }

    open class var shipmentBlockReason: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.shipmentBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.shipmentBlockReason_ = value
            }
        }
    }

    open var shipmentBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.shipmentBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.shipmentBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var shippingCondition: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.shippingCondition_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.shippingCondition_ = value
            }
        }
    }

    open var shippingCondition: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.shippingCondition))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.shippingCondition, to: StringValue.of(optional: value))
        }
    }

    open class var shippingPoint: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.shippingPoint_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.shippingPoint_ = value
            }
        }
    }

    open var shippingPoint: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.shippingPoint))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.shippingPoint, to: StringValue.of(optional: value))
        }
    }

    open class var shippingType: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.shippingType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.shippingType_ = value
            }
        }
    }

    open var shippingType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.shippingType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.shippingType, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.soldToParty, to: StringValue.of(optional: value))
        }
    }

    open class var specialProcessingCode: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.specialProcessingCode_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.specialProcessingCode_ = value
            }
        }
    }

    open var specialProcessingCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.specialProcessingCode))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.specialProcessingCode, to: StringValue.of(optional: value))
        }
    }

    open class var statisticsCurrency: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.statisticsCurrency_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.statisticsCurrency_ = value
            }
        }
    }

    open var statisticsCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.statisticsCurrency))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.statisticsCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var supplier: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.supplier_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.supplier_ = value
            }
        }
    }

    open var supplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.supplier))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.supplier, to: StringValue.of(optional: value))
        }
    }

    open class var totalBlockStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.totalBlockStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.totalBlockStatus_ = value
            }
        }
    }

    open var totalBlockStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.totalBlockStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.totalBlockStatus, to: StringValue.of(optional: value))
        }
    }

    open class var totalCreditCheckStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.totalCreditCheckStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.totalCreditCheckStatus_ = value
            }
        }
    }

    open var totalCreditCheckStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.totalCreditCheckStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.totalCreditCheckStatus, to: StringValue.of(optional: value))
        }
    }

    open class var totalNumberOfPackage: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.totalNumberOfPackage_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.totalNumberOfPackage_ = value
            }
        }
    }

    open var totalNumberOfPackage: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.totalNumberOfPackage))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.totalNumberOfPackage, to: StringValue.of(optional: value))
        }
    }

    open class var transactionCurrency: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.transactionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.transactionCurrency_ = value
            }
        }
    }

    open var transactionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.transactionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.transactionCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var transportationGroup: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.transportationGroup_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.transportationGroup_ = value
            }
        }
    }

    open var transportationGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.transportationGroup))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.transportationGroup, to: StringValue.of(optional: value))
        }
    }

    open class var transportationPlanningDate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.transportationPlanningDate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.transportationPlanningDate_ = value
            }
        }
    }

    open var transportationPlanningDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.transportationPlanningDate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.transportationPlanningDate, to: value)
        }
    }

    open class var transportationPlanningStatus: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.transportationPlanningStatus_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.transportationPlanningStatus_ = value
            }
        }
    }

    open var transportationPlanningStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.transportationPlanningStatus))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.transportationPlanningStatus, to: StringValue.of(optional: value))
        }
    }

    open class var transportationPlanningTime: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.transportationPlanningTime_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.transportationPlanningTime_ = value
            }
        }
    }

    open var transportationPlanningTime: LocalTime? {
        get {
            return LocalTime.castOptional(self.optionalValue(for: AInbDeliveryHeaderType.transportationPlanningTime))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.transportationPlanningTime, to: value)
        }
    }

    open class var unloadingPointName: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.unloadingPointName_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.unloadingPointName_ = value
            }
        }
    }

    open var unloadingPointName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.unloadingPointName))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.unloadingPointName, to: StringValue.of(optional: value))
        }
    }

    open class var warehouse: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.warehouse_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.warehouse_ = value
            }
        }
    }

    open var warehouse: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.warehouse))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.warehouse, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseGate: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.warehouseGate_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.warehouseGate_ = value
            }
        }
    }

    open var warehouseGate: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.warehouseGate))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.warehouseGate, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseStagingArea: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                return AInbDeliveryHeaderType.warehouseStagingArea_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderType.self) }
            do {
                AInbDeliveryHeaderType.warehouseStagingArea_ = value
            }
        }
    }

    open var warehouseStagingArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderType.warehouseStagingArea))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderType.warehouseStagingArea, to: StringValue.of(optional: value))
        }
    }
}
