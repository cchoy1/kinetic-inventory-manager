// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class APurOrdPricingElementType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var purchaseOrder_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PurchaseOrder")

    private static var purchaseOrderItem_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PurchaseOrderItem")

    private static var pricingDocument_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingDocument")

    private static var pricingDocumentItem_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingDocumentItem")

    private static var pricingProcedureStep_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingProcedureStep")

    private static var pricingProcedureCounter_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingProcedureCounter")

    private static var conditionType_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionType")

    private static var conditionRateValue_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionRateValue")

    private static var conditionCurrency_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionCurrency")

    private static var priceDetnExchangeRate_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PriceDetnExchangeRate")

    private static var conditionAmount_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionAmount")

    private static var conditionQuantityUnit_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionQuantityUnit")

    private static var conditionQuantity_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionQuantity")

    private static var conditionApplication_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionApplication")

    private static var pricingDateTime_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingDateTime")

    private static var conditionCalculationType_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionCalculationType")

    private static var conditionBaseValue_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionBaseValue")

    private static var conditionToBaseQtyNmrtr_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionToBaseQtyNmrtr")

    private static var conditionToBaseQtyDnmntr_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionToBaseQtyDnmntr")

    private static var conditionCategory_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionCategory")

    private static var conditionIsForStatistics_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionIsForStatistics")

    private static var pricingScaleType_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingScaleType")

    private static var isRelevantForAccrual_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "IsRelevantForAccrual")

    private static var cndnIsRelevantForInvoiceList_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "CndnIsRelevantForInvoiceList")

    private static var conditionOrigin_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionOrigin")

    private static var isGroupCondition_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "IsGroupCondition")

    private static var cndnIsRelevantForLimitValue_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "CndnIsRelevantForLimitValue")

    private static var conditionSequentialNumber_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionSequentialNumber")

    private static var conditionControl_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionControl")

    private static var conditionInactiveReason_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionInactiveReason")

    private static var conditionClass_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionClass")

    private static var factorForConditionBasisValue_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "FactorForConditionBasisValue")

    private static var pricingScaleBasis_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingScaleBasis")

    private static var conditionScaleBasisValue_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionScaleBasisValue")

    private static var conditionScaleBasisCurrency_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionScaleBasisCurrency")

    private static var conditionScaleBasisUnit_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionScaleBasisUnit")

    private static var cndnIsRelevantForIntcoBilling_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "CndnIsRelevantForIntcoBilling")

    private static var conditionIsForConfiguration_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionIsForConfiguration")

    private static var conditionIsManuallyChanged_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionIsManuallyChanged")

    private static var conditionRecord_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionRecord")

    private static var accessNumberOfAccessSequence_: Property = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "AccessNumberOfAccessSequence")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aPurOrdPricingElementType)
    }

    open class var accessNumberOfAccessSequence: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.accessNumberOfAccessSequence_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.accessNumberOfAccessSequence_ = value
            }
        }
    }

    open var accessNumberOfAccessSequence: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.accessNumberOfAccessSequence))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.accessNumberOfAccessSequence, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [APurOrdPricingElementType] {
        return ArrayConverter.convert(from.toArray(), [APurOrdPricingElementType]())
    }

    open class var cndnIsRelevantForIntcoBilling: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.cndnIsRelevantForIntcoBilling_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.cndnIsRelevantForIntcoBilling_ = value
            }
        }
    }

    open var cndnIsRelevantForIntcoBilling: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurOrdPricingElementType.cndnIsRelevantForIntcoBilling))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.cndnIsRelevantForIntcoBilling, to: BooleanValue.of(optional: value))
        }
    }

    open class var cndnIsRelevantForInvoiceList: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.cndnIsRelevantForInvoiceList_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.cndnIsRelevantForInvoiceList_ = value
            }
        }
    }

    open var cndnIsRelevantForInvoiceList: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.cndnIsRelevantForInvoiceList))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.cndnIsRelevantForInvoiceList, to: StringValue.of(optional: value))
        }
    }

    open class var cndnIsRelevantForLimitValue: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.cndnIsRelevantForLimitValue_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.cndnIsRelevantForLimitValue_ = value
            }
        }
    }

    open var cndnIsRelevantForLimitValue: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurOrdPricingElementType.cndnIsRelevantForLimitValue))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.cndnIsRelevantForLimitValue, to: BooleanValue.of(optional: value))
        }
    }

    open class var conditionAmount: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionAmount_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionAmount_ = value
            }
        }
    }

    open var conditionAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionAmount))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionApplication: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionApplication_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionApplication_ = value
            }
        }
    }

    open var conditionApplication: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionApplication))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionApplication, to: StringValue.of(optional: value))
        }
    }

    open class var conditionBaseValue: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionBaseValue_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionBaseValue_ = value
            }
        }
    }

    open var conditionBaseValue: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionBaseValue))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionBaseValue, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionCalculationType: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionCalculationType_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionCalculationType_ = value
            }
        }
    }

    open var conditionCalculationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionCalculationType))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionCalculationType, to: StringValue.of(optional: value))
        }
    }

    open class var conditionCategory: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionCategory_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionCategory_ = value
            }
        }
    }

    open var conditionCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionCategory))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionCategory, to: StringValue.of(optional: value))
        }
    }

    open class var conditionClass: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionClass_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionClass_ = value
            }
        }
    }

    open var conditionClass: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionClass))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionClass, to: StringValue.of(optional: value))
        }
    }

    open class var conditionControl: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionControl_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionControl_ = value
            }
        }
    }

    open var conditionControl: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionControl))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionControl, to: StringValue.of(optional: value))
        }
    }

    open class var conditionCurrency: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionCurrency_ = value
            }
        }
    }

    open var conditionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var conditionInactiveReason: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionInactiveReason_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionInactiveReason_ = value
            }
        }
    }

    open var conditionInactiveReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionInactiveReason))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionInactiveReason, to: StringValue.of(optional: value))
        }
    }

    open class var conditionIsForConfiguration: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionIsForConfiguration_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionIsForConfiguration_ = value
            }
        }
    }

    open var conditionIsForConfiguration: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionIsForConfiguration))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionIsForConfiguration, to: BooleanValue.of(optional: value))
        }
    }

    open class var conditionIsForStatistics: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionIsForStatistics_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionIsForStatistics_ = value
            }
        }
    }

    open var conditionIsForStatistics: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionIsForStatistics))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionIsForStatistics, to: BooleanValue.of(optional: value))
        }
    }

    open class var conditionIsManuallyChanged: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionIsManuallyChanged_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionIsManuallyChanged_ = value
            }
        }
    }

    open var conditionIsManuallyChanged: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionIsManuallyChanged))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionIsManuallyChanged, to: BooleanValue.of(optional: value))
        }
    }

    open class var conditionOrigin: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionOrigin_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionOrigin_ = value
            }
        }
    }

    open var conditionOrigin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionOrigin))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionOrigin, to: StringValue.of(optional: value))
        }
    }

    open class var conditionQuantity: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionQuantity_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionQuantity_ = value
            }
        }
    }

    open var conditionQuantity: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionQuantity))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionQuantity, to: IntegerValue.of(optional: value))
        }
    }

    open class var conditionQuantityUnit: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionQuantityUnit_ = value
            }
        }
    }

    open var conditionQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var conditionRateValue: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionRateValue_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionRateValue_ = value
            }
        }
    }

    open var conditionRateValue: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionRateValue))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionRateValue, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionRecord: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionRecord_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionRecord_ = value
            }
        }
    }

    open var conditionRecord: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionRecord))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionRecord, to: StringValue.of(optional: value))
        }
    }

    open class var conditionScaleBasisCurrency: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionScaleBasisCurrency_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionScaleBasisCurrency_ = value
            }
        }
    }

    open var conditionScaleBasisCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionScaleBasisCurrency))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionScaleBasisCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var conditionScaleBasisUnit: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionScaleBasisUnit_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionScaleBasisUnit_ = value
            }
        }
    }

    open var conditionScaleBasisUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionScaleBasisUnit))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionScaleBasisUnit, to: StringValue.of(optional: value))
        }
    }

    open class var conditionScaleBasisValue: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionScaleBasisValue_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionScaleBasisValue_ = value
            }
        }
    }

    open var conditionScaleBasisValue: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionScaleBasisValue))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionScaleBasisValue, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionSequentialNumber: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionSequentialNumber_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionSequentialNumber_ = value
            }
        }
    }

    open var conditionSequentialNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionSequentialNumber))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionSequentialNumber, to: StringValue.of(optional: value))
        }
    }

    open class var conditionToBaseQtyDnmntr: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionToBaseQtyDnmntr_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionToBaseQtyDnmntr_ = value
            }
        }
    }

    open var conditionToBaseQtyDnmntr: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionToBaseQtyDnmntr))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionToBaseQtyDnmntr, to: IntegerValue.of(optional: value))
        }
    }

    open class var conditionToBaseQtyNmrtr: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionToBaseQtyNmrtr_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionToBaseQtyNmrtr_ = value
            }
        }
    }

    open var conditionToBaseQtyNmrtr: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionToBaseQtyNmrtr))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionToBaseQtyNmrtr, to: IntegerValue.of(optional: value))
        }
    }

    open class var conditionType: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.conditionType_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.conditionType_ = value
            }
        }
    }

    open var conditionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.conditionType))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.conditionType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> APurOrdPricingElementType {
        return CastRequired<APurOrdPricingElementType>.from(self.copyEntity())
    }

    open class var factorForConditionBasisValue: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.factorForConditionBasisValue_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.factorForConditionBasisValue_ = value
            }
        }
    }

    open var factorForConditionBasisValue: Double? {
        get {
            return DoubleValue.optional(self.optionalValue(for: APurOrdPricingElementType.factorForConditionBasisValue))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.factorForConditionBasisValue, to: DoubleValue.of(optional: value))
        }
    }

    open class var isGroupCondition: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.isGroupCondition_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.isGroupCondition_ = value
            }
        }
    }

    open var isGroupCondition: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.isGroupCondition))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.isGroupCondition, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var isRelevantForAccrual: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.isRelevantForAccrual_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.isRelevantForAccrual_ = value
            }
        }
    }

    open var isRelevantForAccrual: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurOrdPricingElementType.isRelevantForAccrual))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.isRelevantForAccrual, to: BooleanValue.of(optional: value))
        }
    }

    open class func key(purchaseOrder: String?, purchaseOrderItem: String?, pricingDocument: String?, pricingDocumentItem: String?, pricingProcedureStep: String?, pricingProcedureCounter: String?) -> EntityKey {
        return EntityKey().with(name: "PurchaseOrder", value: StringValue.of(optional: purchaseOrder)).with(name: "PurchaseOrderItem", value: StringValue.of(optional: purchaseOrderItem)).with(name: "PricingDocument", value: StringValue.of(optional: pricingDocument)).with(name: "PricingDocumentItem", value: StringValue.of(optional: pricingDocumentItem)).with(name: "PricingProcedureStep", value: StringValue.of(optional: pricingProcedureStep)).with(name: "PricingProcedureCounter", value: StringValue.of(optional: pricingProcedureCounter))
    }

    open var old: APurOrdPricingElementType {
        return CastRequired<APurOrdPricingElementType>.from(self.oldEntity)
    }

    open class var priceDetnExchangeRate: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.priceDetnExchangeRate_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.priceDetnExchangeRate_ = value
            }
        }
    }

    open var priceDetnExchangeRate: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.priceDetnExchangeRate))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.priceDetnExchangeRate, to: StringValue.of(optional: value))
        }
    }

    open class var pricingDateTime: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.pricingDateTime_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.pricingDateTime_ = value
            }
        }
    }

    open var pricingDateTime: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.pricingDateTime))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.pricingDateTime, to: StringValue.of(optional: value))
        }
    }

    open class var pricingDocument: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.pricingDocument_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.pricingDocument_ = value
            }
        }
    }

    open var pricingDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.pricingDocument))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.pricingDocument, to: StringValue.of(optional: value))
        }
    }

    open class var pricingDocumentItem: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.pricingDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.pricingDocumentItem_ = value
            }
        }
    }

    open var pricingDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.pricingDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.pricingDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var pricingProcedureCounter: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.pricingProcedureCounter_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.pricingProcedureCounter_ = value
            }
        }
    }

    open var pricingProcedureCounter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.pricingProcedureCounter))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.pricingProcedureCounter, to: StringValue.of(optional: value))
        }
    }

    open class var pricingProcedureStep: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.pricingProcedureStep_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.pricingProcedureStep_ = value
            }
        }
    }

    open var pricingProcedureStep: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.pricingProcedureStep))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.pricingProcedureStep, to: StringValue.of(optional: value))
        }
    }

    open class var pricingScaleBasis: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.pricingScaleBasis_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.pricingScaleBasis_ = value
            }
        }
    }

    open var pricingScaleBasis: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.pricingScaleBasis))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.pricingScaleBasis, to: StringValue.of(optional: value))
        }
    }

    open class var pricingScaleType: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.pricingScaleType_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.pricingScaleType_ = value
            }
        }
    }

    open var pricingScaleType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.pricingScaleType))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.pricingScaleType, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrder: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.purchaseOrder_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.purchaseOrder_ = value
            }
        }
    }

    open var purchaseOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.purchaseOrder))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.purchaseOrder, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderItem: Property {
        get {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                return APurOrdPricingElementType.purchaseOrderItem_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdPricingElementType.self)
            defer { objc_sync_exit(APurOrdPricingElementType.self) }
            do {
                APurOrdPricingElementType.purchaseOrderItem_ = value
            }
        }
    }

    open var purchaseOrderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdPricingElementType.purchaseOrderItem))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdPricingElementType.purchaseOrderItem, to: StringValue.of(optional: value))
        }
    }
}
