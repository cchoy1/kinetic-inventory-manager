// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AMaterialDocumentItemType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var materialDocumentYear_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentYear")

    private static var materialDocument_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocument")

    private static var materialDocumentItem_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentItem")

    private static var material_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Material")

    private static var plant_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Plant")

    private static var storageLocation_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "StorageLocation")

    private static var batch_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Batch")

    private static var goodsMovementType_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GoodsMovementType")

    private static var inventoryStockType_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "InventoryStockType")

    private static var inventoryValuationType_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "InventoryValuationType")

    private static var inventorySpecialStockType_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "InventorySpecialStockType")

    private static var supplier_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Supplier")

    private static var customer_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Customer")

    private static var salesOrder_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SalesOrder")

    private static var salesOrderItem_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SalesOrderItem")

    private static var salesOrderScheduleLine_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SalesOrderScheduleLine")

    private static var purchaseOrder_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "PurchaseOrder")

    private static var purchaseOrderItem_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "PurchaseOrderItem")

    private static var wbsElement_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "WBSElement")

    private static var manufacturingOrder_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ManufacturingOrder")

    private static var manufacturingOrderItem_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ManufacturingOrderItem")

    private static var goodsMovementRefDocType_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GoodsMovementRefDocType")

    private static var goodsMovementReasonCode_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GoodsMovementReasonCode")

    private static var accountAssignmentCategory_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "AccountAssignmentCategory")

    private static var costCenter_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "CostCenter")

    private static var controllingArea_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ControllingArea")

    private static var costObject_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "CostObject")

    private static var profitabilitySegment_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ProfitabilitySegment")

    private static var profitCenter_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ProfitCenter")

    private static var glAccount_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GLAccount")

    private static var functionalArea_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "FunctionalArea")

    private static var materialBaseUnit_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialBaseUnit")

    private static var quantityInBaseUnit_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "QuantityInBaseUnit")

    private static var entryUnit_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "EntryUnit")

    private static var quantityInEntryUnit_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "QuantityInEntryUnit")

    private static var companyCodeCurrency_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "CompanyCodeCurrency")

    private static var gdsMvtExtAmtInCoCodeCrcy_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GdsMvtExtAmtInCoCodeCrcy")

    private static var slsPrcAmtInclVATInCoCodeCrcy_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SlsPrcAmtInclVATInCoCodeCrcy")

    private static var fiscalYear_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "FiscalYear")

    private static var fiscalYearPeriod_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "FiscalYearPeriod")

    private static var fiscalYearVariant_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "FiscalYearVariant")

    private static var issgOrRcvgMaterial_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssgOrRcvgMaterial")

    private static var issgOrRcvgBatch_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssgOrRcvgBatch")

    private static var issuingOrReceivingPlant_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssuingOrReceivingPlant")

    private static var issuingOrReceivingStorageLoc_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssuingOrReceivingStorageLoc")

    private static var issuingOrReceivingStockType_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssuingOrReceivingStockType")

    private static var issgOrRcvgSpclStockInd_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssgOrRcvgSpclStockInd")

    private static var issuingOrReceivingValType_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssuingOrReceivingValType")

    private static var isCompletelyDelivered_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IsCompletelyDelivered")

    private static var materialDocumentItemText_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentItemText")

    private static var unloadingPointName_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "UnloadingPointName")

    private static var shelfLifeExpirationDate_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ShelfLifeExpirationDate")

    private static var manufactureDate_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ManufactureDate")

    private static var reservation_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Reservation")

    private static var reservationItem_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReservationItem")

    private static var reservationIsFinallyIssued_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReservationIsFinallyIssued")

    private static var specialStockIdfgSalesOrder_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SpecialStockIdfgSalesOrder")

    private static var specialStockIdfgSalesOrderItem_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SpecialStockIdfgSalesOrderItem")

    private static var specialStockIdfgWBSElement_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SpecialStockIdfgWBSElement")

    private static var isAutomaticallyCreated_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IsAutomaticallyCreated")

    private static var materialDocumentLine_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentLine")

    private static var materialDocumentParentLine_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentParentLine")

    private static var hierarchyNodeLevel_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "HierarchyNodeLevel")

    private static var goodsMovementIsCancelled_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GoodsMovementIsCancelled")

    private static var reversedMaterialDocumentYear_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReversedMaterialDocumentYear")

    private static var reversedMaterialDocument_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReversedMaterialDocument")

    private static var reversedMaterialDocumentItem_: Property = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReversedMaterialDocumentItem")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aMaterialDocumentItemType)
    }

    open class var accountAssignmentCategory: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.accountAssignmentCategory_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.accountAssignmentCategory_ = value
            }
        }
    }

    open var accountAssignmentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.accountAssignmentCategory))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.accountAssignmentCategory, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [AMaterialDocumentItemType] {
        return ArrayConverter.convert(from.toArray(), [AMaterialDocumentItemType]())
    }

    open class var batch: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.batch_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.batch_ = value
            }
        }
    }

    open var batch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.batch))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.batch, to: StringValue.of(optional: value))
        }
    }

    open class var companyCodeCurrency: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.companyCodeCurrency_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.companyCodeCurrency_ = value
            }
        }
    }

    open var companyCodeCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.companyCodeCurrency))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.companyCodeCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var controllingArea: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.controllingArea_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.controllingArea_ = value
            }
        }
    }

    open var controllingArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.controllingArea))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.controllingArea, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> AMaterialDocumentItemType {
        return CastRequired<AMaterialDocumentItemType>.from(self.copyEntity())
    }

    open class var costCenter: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.costCenter_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.costCenter_ = value
            }
        }
    }

    open var costCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.costCenter))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.costCenter, to: StringValue.of(optional: value))
        }
    }

    open class var costObject: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.costObject_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.costObject_ = value
            }
        }
    }

    open var costObject: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.costObject))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.costObject, to: StringValue.of(optional: value))
        }
    }

    open class var customer: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.customer_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.customer))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var entryUnit: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.entryUnit_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.entryUnit_ = value
            }
        }
    }

    open var entryUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.entryUnit))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.entryUnit, to: StringValue.of(optional: value))
        }
    }

    open class var fiscalYear: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.fiscalYear_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.fiscalYear_ = value
            }
        }
    }

    open var fiscalYear: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.fiscalYear))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.fiscalYear, to: StringValue.of(optional: value))
        }
    }

    open class var fiscalYearPeriod: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.fiscalYearPeriod_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.fiscalYearPeriod_ = value
            }
        }
    }

    open var fiscalYearPeriod: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.fiscalYearPeriod))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.fiscalYearPeriod, to: StringValue.of(optional: value))
        }
    }

    open class var fiscalYearVariant: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.fiscalYearVariant_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.fiscalYearVariant_ = value
            }
        }
    }

    open var fiscalYearVariant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.fiscalYearVariant))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.fiscalYearVariant, to: StringValue.of(optional: value))
        }
    }

    open class var functionalArea: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.functionalArea_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.functionalArea_ = value
            }
        }
    }

    open var functionalArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.functionalArea))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.functionalArea, to: StringValue.of(optional: value))
        }
    }

    open class var gdsMvtExtAmtInCoCodeCrcy: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.gdsMvtExtAmtInCoCodeCrcy_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.gdsMvtExtAmtInCoCodeCrcy_ = value
            }
        }
    }

    open var gdsMvtExtAmtInCoCodeCrcy: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AMaterialDocumentItemType.gdsMvtExtAmtInCoCodeCrcy))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.gdsMvtExtAmtInCoCodeCrcy, to: DecimalValue.of(optional: value))
        }
    }

    open class var glAccount: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.glAccount_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.glAccount_ = value
            }
        }
    }

    open var glAccount: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.glAccount))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.glAccount, to: StringValue.of(optional: value))
        }
    }

    open class var goodsMovementIsCancelled: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.goodsMovementIsCancelled_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.goodsMovementIsCancelled_ = value
            }
        }
    }

    open var goodsMovementIsCancelled: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AMaterialDocumentItemType.goodsMovementIsCancelled))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.goodsMovementIsCancelled, to: BooleanValue.of(optional: value))
        }
    }

    open class var goodsMovementReasonCode: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.goodsMovementReasonCode_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.goodsMovementReasonCode_ = value
            }
        }
    }

    open var goodsMovementReasonCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.goodsMovementReasonCode))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.goodsMovementReasonCode, to: StringValue.of(optional: value))
        }
    }

    open class var goodsMovementRefDocType: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.goodsMovementRefDocType_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.goodsMovementRefDocType_ = value
            }
        }
    }

    open var goodsMovementRefDocType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.goodsMovementRefDocType))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.goodsMovementRefDocType, to: StringValue.of(optional: value))
        }
    }

    open class var goodsMovementType: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.goodsMovementType_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.goodsMovementType_ = value
            }
        }
    }

    open var goodsMovementType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.goodsMovementType))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.goodsMovementType, to: StringValue.of(optional: value))
        }
    }

    open class var hierarchyNodeLevel: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.hierarchyNodeLevel_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.hierarchyNodeLevel_ = value
            }
        }
    }

    open var hierarchyNodeLevel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.hierarchyNodeLevel))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.hierarchyNodeLevel, to: StringValue.of(optional: value))
        }
    }

    open class var inventorySpecialStockType: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.inventorySpecialStockType_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.inventorySpecialStockType_ = value
            }
        }
    }

    open var inventorySpecialStockType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.inventorySpecialStockType))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.inventorySpecialStockType, to: StringValue.of(optional: value))
        }
    }

    open class var inventoryStockType: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.inventoryStockType_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.inventoryStockType_ = value
            }
        }
    }

    open var inventoryStockType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.inventoryStockType))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.inventoryStockType, to: StringValue.of(optional: value))
        }
    }

    open class var inventoryValuationType: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.inventoryValuationType_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.inventoryValuationType_ = value
            }
        }
    }

    open var inventoryValuationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.inventoryValuationType))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.inventoryValuationType, to: StringValue.of(optional: value))
        }
    }

    open class var isAutomaticallyCreated: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.isAutomaticallyCreated_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.isAutomaticallyCreated_ = value
            }
        }
    }

    open var isAutomaticallyCreated: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.isAutomaticallyCreated))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.isAutomaticallyCreated, to: StringValue.of(optional: value))
        }
    }

    open class var isCompletelyDelivered: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.isCompletelyDelivered_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.isCompletelyDelivered_ = value
            }
        }
    }

    open var isCompletelyDelivered: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AMaterialDocumentItemType.isCompletelyDelivered))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.isCompletelyDelivered, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var issgOrRcvgBatch: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.issgOrRcvgBatch_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.issgOrRcvgBatch_ = value
            }
        }
    }

    open var issgOrRcvgBatch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.issgOrRcvgBatch))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.issgOrRcvgBatch, to: StringValue.of(optional: value))
        }
    }

    open class var issgOrRcvgMaterial: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.issgOrRcvgMaterial_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.issgOrRcvgMaterial_ = value
            }
        }
    }

    open var issgOrRcvgMaterial: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.issgOrRcvgMaterial))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.issgOrRcvgMaterial, to: StringValue.of(optional: value))
        }
    }

    open class var issgOrRcvgSpclStockInd: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.issgOrRcvgSpclStockInd_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.issgOrRcvgSpclStockInd_ = value
            }
        }
    }

    open var issgOrRcvgSpclStockInd: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.issgOrRcvgSpclStockInd))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.issgOrRcvgSpclStockInd, to: StringValue.of(optional: value))
        }
    }

    open class var issuingOrReceivingPlant: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.issuingOrReceivingPlant_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.issuingOrReceivingPlant_ = value
            }
        }
    }

    open var issuingOrReceivingPlant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.issuingOrReceivingPlant))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.issuingOrReceivingPlant, to: StringValue.of(optional: value))
        }
    }

    open class var issuingOrReceivingStockType: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.issuingOrReceivingStockType_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.issuingOrReceivingStockType_ = value
            }
        }
    }

    open var issuingOrReceivingStockType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.issuingOrReceivingStockType))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.issuingOrReceivingStockType, to: StringValue.of(optional: value))
        }
    }

    open class var issuingOrReceivingStorageLoc: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.issuingOrReceivingStorageLoc_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.issuingOrReceivingStorageLoc_ = value
            }
        }
    }

    open var issuingOrReceivingStorageLoc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.issuingOrReceivingStorageLoc))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.issuingOrReceivingStorageLoc, to: StringValue.of(optional: value))
        }
    }

    open class var issuingOrReceivingValType: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.issuingOrReceivingValType_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.issuingOrReceivingValType_ = value
            }
        }
    }

    open var issuingOrReceivingValType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.issuingOrReceivingValType))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.issuingOrReceivingValType, to: StringValue.of(optional: value))
        }
    }

    open class func key(materialDocumentYear: String?, materialDocument: String?, materialDocumentItem: String?) -> EntityKey {
        return EntityKey().with(name: "MaterialDocumentYear", value: StringValue.of(optional: materialDocumentYear)).with(name: "MaterialDocument", value: StringValue.of(optional: materialDocument)).with(name: "MaterialDocumentItem", value: StringValue.of(optional: materialDocumentItem))
    }

    open class var manufactureDate: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.manufactureDate_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.manufactureDate_ = value
            }
        }
    }

    open var manufactureDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AMaterialDocumentItemType.manufactureDate))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.manufactureDate, to: value)
        }
    }

    open class var manufacturingOrder: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.manufacturingOrder_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.manufacturingOrder_ = value
            }
        }
    }

    open var manufacturingOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.manufacturingOrder))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.manufacturingOrder, to: StringValue.of(optional: value))
        }
    }

    open class var manufacturingOrderItem: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.manufacturingOrderItem_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.manufacturingOrderItem_ = value
            }
        }
    }

    open var manufacturingOrderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.manufacturingOrderItem))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.manufacturingOrderItem, to: StringValue.of(optional: value))
        }
    }

    open class var material: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.material_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.material_ = value
            }
        }
    }

    open var material: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.material))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.material, to: StringValue.of(optional: value))
        }
    }

    open class var materialBaseUnit: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.materialBaseUnit_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.materialBaseUnit_ = value
            }
        }
    }

    open var materialBaseUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.materialBaseUnit))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.materialBaseUnit, to: StringValue.of(optional: value))
        }
    }

    open class var materialDocument: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.materialDocument_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.materialDocument_ = value
            }
        }
    }

    open var materialDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.materialDocument))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.materialDocument, to: StringValue.of(optional: value))
        }
    }

    open class var materialDocumentItem: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.materialDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.materialDocumentItem_ = value
            }
        }
    }

    open var materialDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.materialDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.materialDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var materialDocumentItemText: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.materialDocumentItemText_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.materialDocumentItemText_ = value
            }
        }
    }

    open var materialDocumentItemText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.materialDocumentItemText))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.materialDocumentItemText, to: StringValue.of(optional: value))
        }
    }

    open class var materialDocumentLine: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.materialDocumentLine_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.materialDocumentLine_ = value
            }
        }
    }

    open var materialDocumentLine: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.materialDocumentLine))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.materialDocumentLine, to: StringValue.of(optional: value))
        }
    }

    open class var materialDocumentParentLine: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.materialDocumentParentLine_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.materialDocumentParentLine_ = value
            }
        }
    }

    open var materialDocumentParentLine: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.materialDocumentParentLine))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.materialDocumentParentLine, to: StringValue.of(optional: value))
        }
    }

    open class var materialDocumentYear: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.materialDocumentYear_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.materialDocumentYear_ = value
            }
        }
    }

    open var materialDocumentYear: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.materialDocumentYear))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.materialDocumentYear, to: StringValue.of(optional: value))
        }
    }

    open var old: AMaterialDocumentItemType {
        return CastRequired<AMaterialDocumentItemType>.from(self.oldEntity)
    }

    open class var plant: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.plant_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.plant_ = value
            }
        }
    }

    open var plant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.plant))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.plant, to: StringValue.of(optional: value))
        }
    }

    open class var profitCenter: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.profitCenter_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.profitCenter_ = value
            }
        }
    }

    open var profitCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.profitCenter))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.profitCenter, to: StringValue.of(optional: value))
        }
    }

    open class var profitabilitySegment: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.profitabilitySegment_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.profitabilitySegment_ = value
            }
        }
    }

    open var profitabilitySegment: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.profitabilitySegment))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.profitabilitySegment, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrder: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.purchaseOrder_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.purchaseOrder_ = value
            }
        }
    }

    open var purchaseOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.purchaseOrder))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.purchaseOrder, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderItem: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.purchaseOrderItem_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.purchaseOrderItem_ = value
            }
        }
    }

    open var purchaseOrderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.purchaseOrderItem))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.purchaseOrderItem, to: StringValue.of(optional: value))
        }
    }

    open class var quantityInBaseUnit: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.quantityInBaseUnit_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.quantityInBaseUnit_ = value
            }
        }
    }

    open var quantityInBaseUnit: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AMaterialDocumentItemType.quantityInBaseUnit))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.quantityInBaseUnit, to: DecimalValue.of(optional: value))
        }
    }

    open class var quantityInEntryUnit: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.quantityInEntryUnit_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.quantityInEntryUnit_ = value
            }
        }
    }

    open var quantityInEntryUnit: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AMaterialDocumentItemType.quantityInEntryUnit))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.quantityInEntryUnit, to: DecimalValue.of(optional: value))
        }
    }

    open class var reservation: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.reservation_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.reservation_ = value
            }
        }
    }

    open var reservation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.reservation))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.reservation, to: StringValue.of(optional: value))
        }
    }

    open class var reservationIsFinallyIssued: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.reservationIsFinallyIssued_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.reservationIsFinallyIssued_ = value
            }
        }
    }

    open var reservationIsFinallyIssued: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AMaterialDocumentItemType.reservationIsFinallyIssued))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.reservationIsFinallyIssued, to: BooleanValue.of(optional: value))
        }
    }

    open class var reservationItem: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.reservationItem_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.reservationItem_ = value
            }
        }
    }

    open var reservationItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.reservationItem))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.reservationItem, to: StringValue.of(optional: value))
        }
    }

    open class var reversedMaterialDocument: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.reversedMaterialDocument_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.reversedMaterialDocument_ = value
            }
        }
    }

    open var reversedMaterialDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.reversedMaterialDocument))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.reversedMaterialDocument, to: StringValue.of(optional: value))
        }
    }

    open class var reversedMaterialDocumentItem: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.reversedMaterialDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.reversedMaterialDocumentItem_ = value
            }
        }
    }

    open var reversedMaterialDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.reversedMaterialDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.reversedMaterialDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var reversedMaterialDocumentYear: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.reversedMaterialDocumentYear_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.reversedMaterialDocumentYear_ = value
            }
        }
    }

    open var reversedMaterialDocumentYear: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.reversedMaterialDocumentYear))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.reversedMaterialDocumentYear, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrder: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.salesOrder_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.salesOrder_ = value
            }
        }
    }

    open var salesOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.salesOrder))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.salesOrder, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderItem: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.salesOrderItem_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.salesOrderItem_ = value
            }
        }
    }

    open var salesOrderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.salesOrderItem))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.salesOrderItem, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderScheduleLine: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.salesOrderScheduleLine_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.salesOrderScheduleLine_ = value
            }
        }
    }

    open var salesOrderScheduleLine: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.salesOrderScheduleLine))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.salesOrderScheduleLine, to: StringValue.of(optional: value))
        }
    }

    open class var shelfLifeExpirationDate: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.shelfLifeExpirationDate_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.shelfLifeExpirationDate_ = value
            }
        }
    }

    open var shelfLifeExpirationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AMaterialDocumentItemType.shelfLifeExpirationDate))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.shelfLifeExpirationDate, to: value)
        }
    }

    open class var slsPrcAmtInclVATInCoCodeCrcy: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.slsPrcAmtInclVATInCoCodeCrcy_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.slsPrcAmtInclVATInCoCodeCrcy_ = value
            }
        }
    }

    open var slsPrcAmtInclVATInCoCodeCrcy: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AMaterialDocumentItemType.slsPrcAmtInclVATInCoCodeCrcy))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.slsPrcAmtInclVATInCoCodeCrcy, to: DecimalValue.of(optional: value))
        }
    }

    open class var specialStockIdfgSalesOrder: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.specialStockIdfgSalesOrder_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.specialStockIdfgSalesOrder_ = value
            }
        }
    }

    open var specialStockIdfgSalesOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.specialStockIdfgSalesOrder))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.specialStockIdfgSalesOrder, to: StringValue.of(optional: value))
        }
    }

    open class var specialStockIdfgSalesOrderItem: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.specialStockIdfgSalesOrderItem_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.specialStockIdfgSalesOrderItem_ = value
            }
        }
    }

    open var specialStockIdfgSalesOrderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.specialStockIdfgSalesOrderItem))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.specialStockIdfgSalesOrderItem, to: StringValue.of(optional: value))
        }
    }

    open class var specialStockIdfgWBSElement: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.specialStockIdfgWBSElement_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.specialStockIdfgWBSElement_ = value
            }
        }
    }

    open var specialStockIdfgWBSElement: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.specialStockIdfgWBSElement))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.specialStockIdfgWBSElement, to: StringValue.of(optional: value))
        }
    }

    open class var storageLocation: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.storageLocation_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.storageLocation_ = value
            }
        }
    }

    open var storageLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.storageLocation))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.storageLocation, to: StringValue.of(optional: value))
        }
    }

    open class var supplier: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.supplier_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.supplier_ = value
            }
        }
    }

    open var supplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.supplier))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.supplier, to: StringValue.of(optional: value))
        }
    }

    open class var unloadingPointName: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.unloadingPointName_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.unloadingPointName_ = value
            }
        }
    }

    open var unloadingPointName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.unloadingPointName))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.unloadingPointName, to: StringValue.of(optional: value))
        }
    }

    open class var wbsElement: Property {
        get {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                return AMaterialDocumentItemType.wbsElement_
            }
        }
        set(value) {
            objc_sync_enter(AMaterialDocumentItemType.self)
            defer { objc_sync_exit(AMaterialDocumentItemType.self) }
            do {
                AMaterialDocumentItemType.wbsElement_ = value
            }
        }
    }

    open var wbsElement: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaterialDocumentItemType.wbsElement))
        }
        set(value) {
            self.setOptionalValue(for: AMaterialDocumentItemType.wbsElement, to: StringValue.of(optional: value))
        }
    }
}
