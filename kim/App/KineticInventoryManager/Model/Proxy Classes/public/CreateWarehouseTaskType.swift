// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class CreateWarehouseTaskType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var inboundDeliveryUUID_: Property = Ec1Metadata.EntityTypes.createWarehouseTaskType.property(withName: "InboundDeliveryUUID")

    private static var returnBool_: Property = Ec1Metadata.EntityTypes.createWarehouseTaskType.property(withName: "ReturnBool")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.createWarehouseTaskType)
    }

    open class func array(from: EntityValueList) -> [CreateWarehouseTaskType] {
        return ArrayConverter.convert(from.toArray(), [CreateWarehouseTaskType]())
    }

    open func copy() -> CreateWarehouseTaskType {
        return CastRequired<CreateWarehouseTaskType>.from(self.copyEntity())
    }

    open class var inboundDeliveryUUID: Property {
        get {
            objc_sync_enter(CreateWarehouseTaskType.self)
            defer { objc_sync_exit(CreateWarehouseTaskType.self) }
            do {
                return CreateWarehouseTaskType.inboundDeliveryUUID_
            }
        }
        set(value) {
            objc_sync_enter(CreateWarehouseTaskType.self)
            defer { objc_sync_exit(CreateWarehouseTaskType.self) }
            do {
                CreateWarehouseTaskType.inboundDeliveryUUID_ = value
            }
        }
    }

    open var inboundDeliveryUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: CreateWarehouseTaskType.inboundDeliveryUUID))
        }
        set(value) {
            self.setOptionalValue(for: CreateWarehouseTaskType.inboundDeliveryUUID, to: value)
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(inboundDeliveryUUID: GuidValue?) -> EntityKey {
        return EntityKey().with(name: "InboundDeliveryUUID", value: inboundDeliveryUUID)
    }

    open var old: CreateWarehouseTaskType {
        return CastRequired<CreateWarehouseTaskType>.from(self.oldEntity)
    }

    open class var returnBool: Property {
        get {
            objc_sync_enter(CreateWarehouseTaskType.self)
            defer { objc_sync_exit(CreateWarehouseTaskType.self) }
            do {
                return CreateWarehouseTaskType.returnBool_
            }
        }
        set(value) {
            objc_sync_enter(CreateWarehouseTaskType.self)
            defer { objc_sync_exit(CreateWarehouseTaskType.self) }
            do {
                CreateWarehouseTaskType.returnBool_ = value
            }
        }
    }

    open var returnBool: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: CreateWarehouseTaskType.returnBool))
        }
        set(value) {
            self.setOptionalValue(for: CreateWarehouseTaskType.returnBool, to: BooleanValue.of(optional: value))
        }
    }
}
