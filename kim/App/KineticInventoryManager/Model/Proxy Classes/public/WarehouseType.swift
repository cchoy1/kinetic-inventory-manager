// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class WarehouseType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var warehouse_: Property = Ec1Metadata.EntityTypes.warehouseType.property(withName: "Warehouse")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.warehouseType)
    }

    open class func array(from: EntityValueList) -> [WarehouseType] {
        return ArrayConverter.convert(from.toArray(), [WarehouseType]())
    }

    open func copy() -> WarehouseType {
        return CastRequired<WarehouseType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(warehouse: String?) -> EntityKey {
        return EntityKey().with(name: "Warehouse", value: StringValue.of(optional: warehouse))
    }

    open var old: WarehouseType {
        return CastRequired<WarehouseType>.from(self.oldEntity)
    }

    open class var warehouse: Property {
        get {
            objc_sync_enter(WarehouseType.self)
            defer { objc_sync_exit(WarehouseType.self) }
            do {
                return WarehouseType.warehouse_
            }
        }
        set(value) {
            objc_sync_enter(WarehouseType.self)
            defer { objc_sync_exit(WarehouseType.self) }
            do {
                WarehouseType.warehouse_ = value
            }
        }
    }

    open var warehouse: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WarehouseType.warehouse))
        }
        set(value) {
            self.setOptionalValue(for: WarehouseType.warehouse, to: StringValue.of(optional: value))
        }
    }
}
