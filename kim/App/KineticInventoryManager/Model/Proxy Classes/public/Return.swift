// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class Return: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var collectiveProcessing_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "CollectiveProcessing")

    private static var deliveryDocument_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "DeliveryDocument")

    private static var deliveryDocumentItem_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "DeliveryDocumentItem")

    private static var scheduleLine_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "ScheduleLine")

    private static var collectiveProcessingMsgCounter_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "CollectiveProcessingMsgCounter")

    private static var systemMessageIdentification_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageIdentification")

    private static var systemMessageNumber_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageNumber")

    private static var systemMessageType_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageType")

    private static var systemMessageVariable1_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageVariable1")

    private static var systemMessageVariable2_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageVariable2")

    private static var systemMessageVariable3_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageVariable3")

    private static var systemMessageVariable4_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageVariable4")

    private static var collectiveProcessingType_: Property = Ec1Metadata.ComplexTypes.return.property(withName: "CollectiveProcessingType")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.ComplexTypes.return)
    }

    open class var collectiveProcessing: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.collectiveProcessing_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.collectiveProcessing_ = value
            }
        }
    }

    open var collectiveProcessing: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.collectiveProcessing))
        }
        set(value) {
            self.setOptionalValue(for: Return.collectiveProcessing, to: StringValue.of(optional: value))
        }
    }

    open class var collectiveProcessingMsgCounter: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.collectiveProcessingMsgCounter_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.collectiveProcessingMsgCounter_ = value
            }
        }
    }

    open var collectiveProcessingMsgCounter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.collectiveProcessingMsgCounter))
        }
        set(value) {
            self.setOptionalValue(for: Return.collectiveProcessingMsgCounter, to: StringValue.of(optional: value))
        }
    }

    open class var collectiveProcessingType: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.collectiveProcessingType_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.collectiveProcessingType_ = value
            }
        }
    }

    open var collectiveProcessingType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.collectiveProcessingType))
        }
        set(value) {
            self.setOptionalValue(for: Return.collectiveProcessingType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> Return {
        return CastRequired<Return>.from(self.copyComplex())
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: Return.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItem: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.deliveryDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.deliveryDocumentItem_ = value
            }
        }
    }

    open var deliveryDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.deliveryDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: Return.deliveryDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open var old: Return {
        return CastRequired<Return>.from(self.oldComplex)
    }

    open class var scheduleLine: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.scheduleLine_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.scheduleLine_ = value
            }
        }
    }

    open var scheduleLine: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.scheduleLine))
        }
        set(value) {
            self.setOptionalValue(for: Return.scheduleLine, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageIdentification: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.systemMessageIdentification_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.systemMessageIdentification_ = value
            }
        }
    }

    open var systemMessageIdentification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.systemMessageIdentification))
        }
        set(value) {
            self.setOptionalValue(for: Return.systemMessageIdentification, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageNumber: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.systemMessageNumber_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.systemMessageNumber_ = value
            }
        }
    }

    open var systemMessageNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.systemMessageNumber))
        }
        set(value) {
            self.setOptionalValue(for: Return.systemMessageNumber, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageType: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.systemMessageType_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.systemMessageType_ = value
            }
        }
    }

    open var systemMessageType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.systemMessageType))
        }
        set(value) {
            self.setOptionalValue(for: Return.systemMessageType, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageVariable1: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.systemMessageVariable1_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.systemMessageVariable1_ = value
            }
        }
    }

    open var systemMessageVariable1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.systemMessageVariable1))
        }
        set(value) {
            self.setOptionalValue(for: Return.systemMessageVariable1, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageVariable2: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.systemMessageVariable2_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.systemMessageVariable2_ = value
            }
        }
    }

    open var systemMessageVariable2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.systemMessageVariable2))
        }
        set(value) {
            self.setOptionalValue(for: Return.systemMessageVariable2, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageVariable3: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.systemMessageVariable3_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.systemMessageVariable3_ = value
            }
        }
    }

    open var systemMessageVariable3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.systemMessageVariable3))
        }
        set(value) {
            self.setOptionalValue(for: Return.systemMessageVariable3, to: StringValue.of(optional: value))
        }
    }

    open class var systemMessageVariable4: Property {
        get {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                return Return.systemMessageVariable4_
            }
        }
        set(value) {
            objc_sync_enter(Return.self)
            defer { objc_sync_exit(Return.self) }
            do {
                Return.systemMessageVariable4_ = value
            }
        }
    }

    open var systemMessageVariable4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Return.systemMessageVariable4))
        }
        set(value) {
            self.setOptionalValue(for: Return.systemMessageVariable4, to: StringValue.of(optional: value))
        }
    }
}
