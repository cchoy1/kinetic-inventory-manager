// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AInbDeliveryHeaderTextType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var deliveryDocument_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "DeliveryDocument")

    private static var textElement_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "TextElement")

    private static var language_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "Language")

    private static var textElementDescription_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "TextElementDescription")

    private static var textElementText_: Property = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "TextElementText")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType)
    }

    open class func array(from: EntityValueList) -> [AInbDeliveryHeaderTextType] {
        return ArrayConverter.convert(from.toArray(), [AInbDeliveryHeaderTextType]())
    }

    open func copy() -> AInbDeliveryHeaderTextType {
        return CastRequired<AInbDeliveryHeaderTextType>.from(self.copyEntity())
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderTextType.self) }
            do {
                return AInbDeliveryHeaderTextType.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderTextType.self) }
            do {
                AInbDeliveryHeaderTextType.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderTextType.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderTextType.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(deliveryDocument: String?, textElement: String?, language: String?) -> EntityKey {
        return EntityKey().with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "TextElement", value: StringValue.of(optional: textElement)).with(name: "Language", value: StringValue.of(optional: language))
    }

    open class var language: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderTextType.self) }
            do {
                return AInbDeliveryHeaderTextType.language_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderTextType.self) }
            do {
                AInbDeliveryHeaderTextType.language_ = value
            }
        }
    }

    open var language: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderTextType.language))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderTextType.language, to: StringValue.of(optional: value))
        }
    }

    open var old: AInbDeliveryHeaderTextType {
        return CastRequired<AInbDeliveryHeaderTextType>.from(self.oldEntity)
    }

    open class var textElement: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderTextType.self) }
            do {
                return AInbDeliveryHeaderTextType.textElement_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderTextType.self) }
            do {
                AInbDeliveryHeaderTextType.textElement_ = value
            }
        }
    }

    open var textElement: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderTextType.textElement))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderTextType.textElement, to: StringValue.of(optional: value))
        }
    }

    open class var textElementDescription: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderTextType.self) }
            do {
                return AInbDeliveryHeaderTextType.textElementDescription_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderTextType.self) }
            do {
                AInbDeliveryHeaderTextType.textElementDescription_ = value
            }
        }
    }

    open var textElementDescription: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderTextType.textElementDescription))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderTextType.textElementDescription, to: StringValue.of(optional: value))
        }
    }

    open class var textElementText: Property {
        get {
            objc_sync_enter(AInbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderTextType.self) }
            do {
                return AInbDeliveryHeaderTextType.textElementText_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryHeaderTextType.self)
            defer { objc_sync_exit(AInbDeliveryHeaderTextType.self) }
            do {
                AInbDeliveryHeaderTextType.textElementText_ = value
            }
        }
    }

    open var textElementText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryHeaderTextType.textElementText))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryHeaderTextType.textElementText, to: StringValue.of(optional: value))
        }
    }
}
