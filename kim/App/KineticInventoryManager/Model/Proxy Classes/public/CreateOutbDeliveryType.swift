// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class CreateOutbDeliveryType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var referenceSDDocument_: Property = Ec1Metadata.EntityTypes.createOutbDeliveryType.property(withName: "ReferenceSDDocument")

    private static var outboundDelivery_: Property = Ec1Metadata.EntityTypes.createOutbDeliveryType.property(withName: "OutboundDelivery")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.createOutbDeliveryType)
    }

    open class func array(from: EntityValueList) -> [CreateOutbDeliveryType] {
        return ArrayConverter.convert(from.toArray(), [CreateOutbDeliveryType]())
    }

    open func copy() -> CreateOutbDeliveryType {
        return CastRequired<CreateOutbDeliveryType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(referenceSDDocument: String?) -> EntityKey {
        return EntityKey().with(name: "ReferenceSDDocument", value: StringValue.of(optional: referenceSDDocument))
    }

    open var old: CreateOutbDeliveryType {
        return CastRequired<CreateOutbDeliveryType>.from(self.oldEntity)
    }

    open class var outboundDelivery: Property {
        get {
            objc_sync_enter(CreateOutbDeliveryType.self)
            defer { objc_sync_exit(CreateOutbDeliveryType.self) }
            do {
                return CreateOutbDeliveryType.outboundDelivery_
            }
        }
        set(value) {
            objc_sync_enter(CreateOutbDeliveryType.self)
            defer { objc_sync_exit(CreateOutbDeliveryType.self) }
            do {
                CreateOutbDeliveryType.outboundDelivery_ = value
            }
        }
    }

    open var outboundDelivery: String? {
        get {
            return StringValue.optional(self.optionalValue(for: CreateOutbDeliveryType.outboundDelivery))
        }
        set(value) {
            self.setOptionalValue(for: CreateOutbDeliveryType.outboundDelivery, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocument: Property {
        get {
            objc_sync_enter(CreateOutbDeliveryType.self)
            defer { objc_sync_exit(CreateOutbDeliveryType.self) }
            do {
                return CreateOutbDeliveryType.referenceSDDocument_
            }
        }
        set(value) {
            objc_sync_enter(CreateOutbDeliveryType.self)
            defer { objc_sync_exit(CreateOutbDeliveryType.self) }
            do {
                CreateOutbDeliveryType.referenceSDDocument_ = value
            }
        }
    }

    open var referenceSDDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: CreateOutbDeliveryType.referenceSDDocument))
        }
        set(value) {
            self.setOptionalValue(for: CreateOutbDeliveryType.referenceSDDocument, to: StringValue.of(optional: value))
        }
    }
}
