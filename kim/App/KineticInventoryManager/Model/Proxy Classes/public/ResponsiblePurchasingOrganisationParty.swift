// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class ResponsiblePurchasingOrganisationParty: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var internalID_: Property = Ec1Metadata.ComplexTypes.responsiblePurchasingOrganisationParty.property(withName: "InternalID")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.ComplexTypes.responsiblePurchasingOrganisationParty)
    }

    open func copy() -> ResponsiblePurchasingOrganisationParty {
        return CastRequired<ResponsiblePurchasingOrganisationParty>.from(self.copyComplex())
    }

    open class var internalID: Property {
        get {
            objc_sync_enter(ResponsiblePurchasingOrganisationParty.self)
            defer { objc_sync_exit(ResponsiblePurchasingOrganisationParty.self) }
            do {
                return ResponsiblePurchasingOrganisationParty.internalID_
            }
        }
        set(value) {
            objc_sync_enter(ResponsiblePurchasingOrganisationParty.self)
            defer { objc_sync_exit(ResponsiblePurchasingOrganisationParty.self) }
            do {
                ResponsiblePurchasingOrganisationParty.internalID_ = value
            }
        }
    }

    open var internalID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ResponsiblePurchasingOrganisationParty.internalID))
        }
        set(value) {
            self.setOptionalValue(for: ResponsiblePurchasingOrganisationParty.internalID, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open var old: ResponsiblePurchasingOrganisationParty {
        return CastRequired<ResponsiblePurchasingOrganisationParty>.from(self.oldComplex)
    }
}
