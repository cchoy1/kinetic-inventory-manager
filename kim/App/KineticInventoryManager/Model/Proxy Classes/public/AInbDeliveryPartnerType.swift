// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AInbDeliveryPartnerType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var addressID_: Property = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "AddressID")

    private static var contactPerson_: Property = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "ContactPerson")

    private static var customer_: Property = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "Customer")

    private static var partnerFunction_: Property = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "PartnerFunction")

    private static var personnel_: Property = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "Personnel")

    private static var sdDocument_: Property = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "SDDocument")

    private static var sdDocumentItem_: Property = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "SDDocumentItem")

    private static var supplier_: Property = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "Supplier")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aInbDeliveryPartnerType)
    }

    open class var addressID: Property {
        get {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                return AInbDeliveryPartnerType.addressID_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                AInbDeliveryPartnerType.addressID_ = value
            }
        }
    }

    open var addressID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryPartnerType.addressID))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryPartnerType.addressID, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [AInbDeliveryPartnerType] {
        return ArrayConverter.convert(from.toArray(), [AInbDeliveryPartnerType]())
    }

    open class var contactPerson: Property {
        get {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                return AInbDeliveryPartnerType.contactPerson_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                AInbDeliveryPartnerType.contactPerson_ = value
            }
        }
    }

    open var contactPerson: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryPartnerType.contactPerson))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryPartnerType.contactPerson, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> AInbDeliveryPartnerType {
        return CastRequired<AInbDeliveryPartnerType>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                return AInbDeliveryPartnerType.customer_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                AInbDeliveryPartnerType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryPartnerType.customer))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryPartnerType.customer, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(partnerFunction: String?, sdDocument: String?) -> EntityKey {
        return EntityKey().with(name: "PartnerFunction", value: StringValue.of(optional: partnerFunction)).with(name: "SDDocument", value: StringValue.of(optional: sdDocument))
    }

    open var old: AInbDeliveryPartnerType {
        return CastRequired<AInbDeliveryPartnerType>.from(self.oldEntity)
    }

    open class var partnerFunction: Property {
        get {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                return AInbDeliveryPartnerType.partnerFunction_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                AInbDeliveryPartnerType.partnerFunction_ = value
            }
        }
    }

    open var partnerFunction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryPartnerType.partnerFunction))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryPartnerType.partnerFunction, to: StringValue.of(optional: value))
        }
    }

    open class var personnel: Property {
        get {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                return AInbDeliveryPartnerType.personnel_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                AInbDeliveryPartnerType.personnel_ = value
            }
        }
    }

    open var personnel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryPartnerType.personnel))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryPartnerType.personnel, to: StringValue.of(optional: value))
        }
    }

    open class var sdDocument: Property {
        get {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                return AInbDeliveryPartnerType.sdDocument_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                AInbDeliveryPartnerType.sdDocument_ = value
            }
        }
    }

    open var sdDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryPartnerType.sdDocument))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryPartnerType.sdDocument, to: StringValue.of(optional: value))
        }
    }

    open class var sdDocumentItem: Property {
        get {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                return AInbDeliveryPartnerType.sdDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                AInbDeliveryPartnerType.sdDocumentItem_ = value
            }
        }
    }

    open var sdDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryPartnerType.sdDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryPartnerType.sdDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var supplier: Property {
        get {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                return AInbDeliveryPartnerType.supplier_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryPartnerType.self)
            defer { objc_sync_exit(AInbDeliveryPartnerType.self) }
            do {
                AInbDeliveryPartnerType.supplier_ = value
            }
        }
    }

    open var supplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryPartnerType.supplier))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryPartnerType.supplier, to: StringValue.of(optional: value))
        }
    }
}
