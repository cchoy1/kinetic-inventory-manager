// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class ResponsiblePurchasingGroupParty: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var internalID_: Property = Ec1Metadata.ComplexTypes.responsiblePurchasingGroupParty.property(withName: "InternalID")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.ComplexTypes.responsiblePurchasingGroupParty)
    }

    open func copy() -> ResponsiblePurchasingGroupParty {
        return CastRequired<ResponsiblePurchasingGroupParty>.from(self.copyComplex())
    }

    open class var internalID: Property {
        get {
            objc_sync_enter(ResponsiblePurchasingGroupParty.self)
            defer { objc_sync_exit(ResponsiblePurchasingGroupParty.self) }
            do {
                return ResponsiblePurchasingGroupParty.internalID_
            }
        }
        set(value) {
            objc_sync_enter(ResponsiblePurchasingGroupParty.self)
            defer { objc_sync_exit(ResponsiblePurchasingGroupParty.self) }
            do {
                ResponsiblePurchasingGroupParty.internalID_ = value
            }
        }
    }

    open var internalID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ResponsiblePurchasingGroupParty.internalID))
        }
        set(value) {
            self.setOptionalValue(for: ResponsiblePurchasingGroupParty.internalID, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open var old: ResponsiblePurchasingGroupParty {
        return CastRequired<ResponsiblePurchasingGroupParty>.from(self.oldComplex)
    }
}
