// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class ReturnActionsType: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var returnBool_: Property = Ec1Metadata.ComplexTypes.returnActionsType.property(withName: "ReturnBool")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.ComplexTypes.returnActionsType)
    }

    open func copy() -> ReturnActionsType {
        return CastRequired<ReturnActionsType>.from(self.copyComplex())
    }

    open override var isProxy: Bool {
        return true
    }

    open var old: ReturnActionsType {
        return CastRequired<ReturnActionsType>.from(self.oldComplex)
    }

    open class var returnBool: Property {
        get {
            objc_sync_enter(ReturnActionsType.self)
            defer { objc_sync_exit(ReturnActionsType.self) }
            do {
                return ReturnActionsType.returnBool_
            }
        }
        set(value) {
            objc_sync_enter(ReturnActionsType.self)
            defer { objc_sync_exit(ReturnActionsType.self) }
            do {
                ReturnActionsType.returnBool_ = value
            }
        }
    }

    open var returnBool: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: ReturnActionsType.returnBool))
        }
        set(value) {
            self.setOptionalValue(for: ReturnActionsType.returnBool, to: BooleanValue.of(optional: value))
        }
    }
}
