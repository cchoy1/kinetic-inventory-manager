// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class Quantity: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var unitCode_: Property = Ec1Metadata.ComplexTypes.quantity.property(withName: "unitCode")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.ComplexTypes.quantity)
    }

    open func copy() -> Quantity {
        return CastRequired<Quantity>.from(self.copyComplex())
    }

    open override var isProxy: Bool {
        return true
    }

    open var old: Quantity {
        return CastRequired<Quantity>.from(self.oldComplex)
    }

    open class var unitCode: Property {
        get {
            objc_sync_enter(Quantity.self)
            defer { objc_sync_exit(Quantity.self) }
            do {
                return Quantity.unitCode_
            }
        }
        set(value) {
            objc_sync_enter(Quantity.self)
            defer { objc_sync_exit(Quantity.self) }
            do {
                Quantity.unitCode_ = value
            }
        }
    }

    open var unitCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Quantity.unitCode))
        }
        set(value) {
            self.setOptionalValue(for: Quantity.unitCode, to: StringValue.of(optional: value))
        }
    }
}
