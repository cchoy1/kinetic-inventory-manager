// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class GoodsIssueType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var outboundDeliveryUUID_: Property = Ec1Metadata.EntityTypes.goodsIssueType.property(withName: "OutboundDeliveryUUID")

    private static var returnBool_: Property = Ec1Metadata.EntityTypes.goodsIssueType.property(withName: "ReturnBool")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.goodsIssueType)
    }

    open class func array(from: EntityValueList) -> [GoodsIssueType] {
        return ArrayConverter.convert(from.toArray(), [GoodsIssueType]())
    }

    open func copy() -> GoodsIssueType {
        return CastRequired<GoodsIssueType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(outboundDeliveryUUID: GuidValue?) -> EntityKey {
        return EntityKey().with(name: "OutboundDeliveryUUID", value: outboundDeliveryUUID)
    }

    open var old: GoodsIssueType {
        return CastRequired<GoodsIssueType>.from(self.oldEntity)
    }

    open class var outboundDeliveryUUID: Property {
        get {
            objc_sync_enter(GoodsIssueType.self)
            defer { objc_sync_exit(GoodsIssueType.self) }
            do {
                return GoodsIssueType.outboundDeliveryUUID_
            }
        }
        set(value) {
            objc_sync_enter(GoodsIssueType.self)
            defer { objc_sync_exit(GoodsIssueType.self) }
            do {
                GoodsIssueType.outboundDeliveryUUID_ = value
            }
        }
    }

    open var outboundDeliveryUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: GoodsIssueType.outboundDeliveryUUID))
        }
        set(value) {
            self.setOptionalValue(for: GoodsIssueType.outboundDeliveryUUID, to: value)
        }
    }

    open class var returnBool: Property {
        get {
            objc_sync_enter(GoodsIssueType.self)
            defer { objc_sync_exit(GoodsIssueType.self) }
            do {
                return GoodsIssueType.returnBool_
            }
        }
        set(value) {
            objc_sync_enter(GoodsIssueType.self)
            defer { objc_sync_exit(GoodsIssueType.self) }
            do {
                GoodsIssueType.returnBool_ = value
            }
        }
    }

    open var returnBool: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: GoodsIssueType.returnBool))
        }
        set(value) {
            self.setOptionalValue(for: GoodsIssueType.returnBool, to: BooleanValue.of(optional: value))
        }
    }
}
