// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

public class Ec1Metadata {
    private static var document_: CSDLDocument = Ec1Metadata.resolve()

    public static let lock: MetadataLock = MetadataLock()

    public static var document: CSDLDocument {
        get {
            objc_sync_enter(Ec1Metadata.self)
            defer { objc_sync_exit(Ec1Metadata.self) }
            do {
                return Ec1Metadata.document_
            }
        }
        set(value) {
            objc_sync_enter(Ec1Metadata.self)
            defer { objc_sync_exit(Ec1Metadata.self) }
            do {
                Ec1Metadata.document_ = value
            }
        }
    }

    private static func resolve() -> CSDLDocument {
        try! Ec1Factory.registerAll()
        Ec1MetadataParser.parsed.hasGeneratedProxies = true
        return Ec1MetadataParser.parsed
    }

    public class ComplexTypes {
        private static var item2_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.Item2")

        private static var pickingReport_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.PickingReport")

        private static var product_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.Product")

        private static var quantity_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.Quantity")

        private static var quantity2_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.Quantity2")

        private static var responsiblePurchasingGroupParty_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.ResponsiblePurchasingGroupParty")

        private static var responsiblePurchasingOrganisationParty_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.ResponsiblePurchasingOrganisationParty")

        private static var return_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.Return")

        private static var returnActionsType_: ComplexType = Ec1MetadataParser.parsed.complexType(withName: "S1.ReturnActionsType")

        public static var `return`: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.return_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.return_ = value
                }
            }
        }

        public static var item2: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.item2_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.item2_ = value
                }
            }
        }

        public static var pickingReport: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.pickingReport_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.pickingReport_ = value
                }
            }
        }

        public static var product: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.product_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.product_ = value
                }
            }
        }

        public static var quantity: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.quantity_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.quantity_ = value
                }
            }
        }

        public static var quantity2: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.quantity2_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.quantity2_ = value
                }
            }
        }

        public static var responsiblePurchasingGroupParty: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.responsiblePurchasingGroupParty_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.responsiblePurchasingGroupParty_ = value
                }
            }
        }

        public static var responsiblePurchasingOrganisationParty: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.responsiblePurchasingOrganisationParty_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.responsiblePurchasingOrganisationParty_ = value
                }
            }
        }

        public static var returnActionsType: ComplexType {
            get {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    return Ec1Metadata.ComplexTypes.returnActionsType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ComplexTypes.self)
                defer { objc_sync_exit(Ec1Metadata.ComplexTypes.self) }
                do {
                    Ec1Metadata.ComplexTypes.returnActionsType_ = value
                }
            }
        }
    }

    public class EntityTypes {
        private static var aInbDeliveryAddressType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryAddressType")

        private static var aInbDeliveryDocFlowType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryDocFlowType")

        private static var aInbDeliveryHeaderTextType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryHeaderTextType")

        private static var aInbDeliveryHeaderType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryHeaderType")

        private static var aInbDeliveryItemTextType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryItemTextType")

        private static var aInbDeliveryItemType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryItemType")

        private static var aInbDeliveryPartnerType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliveryPartnerType")

        private static var aInbDeliverySerialNmbrType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_InbDeliverySerialNmbrType")

        private static var aMaintenanceItemObjListType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_MaintenanceItemObjListType")

        private static var aMaintenanceItemObjectType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_MaintenanceItemObjectType")

        private static var aMaterialDocumentHeaderType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_MaterialDocumentHeaderType")

        private static var aMaterialDocumentItemType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_MaterialDocumentItemType")

        private static var aOutbDeliveryAddressType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_OutbDeliveryAddressType")

        private static var aOutbDeliveryDocFlowType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_OutbDeliveryDocFlowType")

        private static var aOutbDeliveryHeaderTextType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_OutbDeliveryHeaderTextType")

        private static var aOutbDeliveryHeaderType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_OutbDeliveryHeaderType")

        private static var aOutbDeliveryItemTextType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_OutbDeliveryItemTextType")

        private static var aOutbDeliveryItemType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_OutbDeliveryItemType")

        private static var aOutbDeliveryPartnerType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_OutbDeliveryPartnerType")

        private static var aPlantType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_PlantType")

        private static var aProductDescriptionType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_ProductDescriptionType")

        private static var aProductPlantType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_ProductPlantType")

        private static var aProductType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_ProductType")

        private static var aPurOrdAccountAssignmentType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_PurOrdAccountAssignmentType")

        private static var aPurOrdPricingElementType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_PurOrdPricingElementType")

        private static var aPurchaseOrderItemType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_PurchaseOrderItemType")

        private static var aPurchaseOrderScheduleLineType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_PurchaseOrderScheduleLineType")

        private static var aPurchaseOrderType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_PurchaseOrderType")

        private static var aSerialNmbrDeliveryType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.A_SerialNmbrDeliveryType")

        private static var confirmPickType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.ConfirmPickType")

        private static var createOutbDeliveryType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.CreateOutbDeliveryType")

        private static var createOutbWarehouseTaskType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.CreateOutbWarehouseTaskType")

        private static var createWarehouseTaskType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.CreateWarehouseTaskType")

        private static var dlvHead_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.DLVHead")

        private static var dlvItem_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.DLVItem")

        private static var goodsIssueType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.GoodsIssueType")

        private static var hu_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.HU")

        private static var huHead_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.HUHead")

        private static var huItem_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.HUItem")

        private static var huSingleItem_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.HUSingleItem")

        private static var item_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.Item")

        private static var outbdlvHead_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.OUTBDLVHead")

        private static var outbdlvItem_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.OUTBDLVItem")

        private static var performPackType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.PerformPackType")

        private static var purchaseOrderERPCreateRequestConfirmationInV1_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.PurchaseOrderERPCreateRequestConfirmation_In_V1")

        private static var wtSimpleConfirm_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.WTSimpleConfirm")

        private static var warehouseTask_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.WarehouseTask")

        private static var warehouseTaskType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.WarehouseTaskType")

        private static var warehouseType_: EntityType = Ec1MetadataParser.parsed.entityType(withName: "S1.WarehouseType")

        public static var aInbDeliveryAddressType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryAddressType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryAddressType_ = value
                }
            }
        }

        public static var aInbDeliveryDocFlowType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType_ = value
                }
            }
        }

        public static var aInbDeliveryHeaderTextType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType_ = value
                }
            }
        }

        public static var aInbDeliveryHeaderType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryHeaderType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryHeaderType_ = value
                }
            }
        }

        public static var aInbDeliveryItemTextType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryItemTextType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryItemTextType_ = value
                }
            }
        }

        public static var aInbDeliveryItemType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryItemType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryItemType_ = value
                }
            }
        }

        public static var aInbDeliveryPartnerType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliveryPartnerType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliveryPartnerType_ = value
                }
            }
        }

        public static var aInbDeliverySerialNmbrType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType_ = value
                }
            }
        }

        public static var aMaintenanceItemObjListType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aMaintenanceItemObjListType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aMaintenanceItemObjListType_ = value
                }
            }
        }

        public static var aMaintenanceItemObjectType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aMaintenanceItemObjectType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aMaintenanceItemObjectType_ = value
                }
            }
        }

        public static var aMaterialDocumentHeaderType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType_ = value
                }
            }
        }

        public static var aMaterialDocumentItemType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aMaterialDocumentItemType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aMaterialDocumentItemType_ = value
                }
            }
        }

        public static var aOutbDeliveryAddressType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aOutbDeliveryAddressType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aOutbDeliveryAddressType_ = value
                }
            }
        }

        public static var aOutbDeliveryDocFlowType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType_ = value
                }
            }
        }

        public static var aOutbDeliveryHeaderTextType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType_ = value
                }
            }
        }

        public static var aOutbDeliveryHeaderType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType_ = value
                }
            }
        }

        public static var aOutbDeliveryItemTextType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType_ = value
                }
            }
        }

        public static var aOutbDeliveryItemType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aOutbDeliveryItemType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aOutbDeliveryItemType_ = value
                }
            }
        }

        public static var aOutbDeliveryPartnerType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType_ = value
                }
            }
        }

        public static var aPlantType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aPlantType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aPlantType_ = value
                }
            }
        }

        public static var aProductDescriptionType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aProductDescriptionType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aProductDescriptionType_ = value
                }
            }
        }

        public static var aProductPlantType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aProductPlantType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aProductPlantType_ = value
                }
            }
        }

        public static var aProductType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aProductType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aProductType_ = value
                }
            }
        }

        public static var aPurOrdAccountAssignmentType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType_ = value
                }
            }
        }

        public static var aPurOrdPricingElementType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aPurOrdPricingElementType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aPurOrdPricingElementType_ = value
                }
            }
        }

        public static var aPurchaseOrderItemType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aPurchaseOrderItemType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aPurchaseOrderItemType_ = value
                }
            }
        }

        public static var aPurchaseOrderScheduleLineType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType_ = value
                }
            }
        }

        public static var aPurchaseOrderType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aPurchaseOrderType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aPurchaseOrderType_ = value
                }
            }
        }

        public static var aSerialNmbrDeliveryType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType_ = value
                }
            }
        }

        public static var confirmPickType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.confirmPickType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.confirmPickType_ = value
                }
            }
        }

        public static var createOutbDeliveryType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.createOutbDeliveryType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.createOutbDeliveryType_ = value
                }
            }
        }

        public static var createOutbWarehouseTaskType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.createOutbWarehouseTaskType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.createOutbWarehouseTaskType_ = value
                }
            }
        }

        public static var createWarehouseTaskType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.createWarehouseTaskType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.createWarehouseTaskType_ = value
                }
            }
        }

        public static var dlvHead: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.dlvHead_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.dlvHead_ = value
                }
            }
        }

        public static var dlvItem: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.dlvItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.dlvItem_ = value
                }
            }
        }

        public static var goodsIssueType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.goodsIssueType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.goodsIssueType_ = value
                }
            }
        }

        public static var hu: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.hu_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.hu_ = value
                }
            }
        }

        public static var huHead: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.huHead_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.huHead_ = value
                }
            }
        }

        public static var huItem: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.huItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.huItem_ = value
                }
            }
        }

        public static var huSingleItem: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.huSingleItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.huSingleItem_ = value
                }
            }
        }

        public static var item: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.item_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.item_ = value
                }
            }
        }

        public static var outbdlvHead: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.outbdlvHead_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.outbdlvHead_ = value
                }
            }
        }

        public static var outbdlvItem: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.outbdlvItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.outbdlvItem_ = value
                }
            }
        }

        public static var performPackType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.performPackType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.performPackType_ = value
                }
            }
        }

        public static var purchaseOrderERPCreateRequestConfirmationInV1: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1_ = value
                }
            }
        }

        public static var warehouseTask: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.warehouseTask_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.warehouseTask_ = value
                }
            }
        }

        public static var warehouseTaskType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.warehouseTaskType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.warehouseTaskType_ = value
                }
            }
        }

        public static var warehouseType: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.warehouseType_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.warehouseType_ = value
                }
            }
        }

        public static var wtSimpleConfirm: EntityType {
            get {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    return Ec1Metadata.EntityTypes.wtSimpleConfirm_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntityTypes.self)
                defer { objc_sync_exit(Ec1Metadata.EntityTypes.self) }
                do {
                    Ec1Metadata.EntityTypes.wtSimpleConfirm_ = value
                }
            }
        }
    }

    public class EntitySets {
        private static var aInbDeliveryAddressTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryAddressTypeSet")

        private static var aInbDeliveryDocFlowTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryDocFlowTypeSet")

        private static var aInbDeliveryHeader_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryHeader")

        private static var aInbDeliveryHeaderTextTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryHeaderTextTypeSet")

        private static var aInbDeliveryItem_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryItem")

        private static var aInbDeliveryItemTextTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryItemTextTypeSet")

        private static var aInbDeliveryPartner_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliveryPartner")

        private static var aInbDeliverySerialNmbrTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_InbDeliverySerialNmbrTypeSet")

        private static var aMaintenanceItemObjListTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_MaintenanceItemObjListTypeSet")

        private static var aMaintenanceItemObject_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_MaintenanceItemObject")

        private static var aMaterialDocumentHeader_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_MaterialDocumentHeader")

        private static var aMaterialDocumentItemTypeSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_MaterialDocumentItemTypeSet")

        private static var aOutbDeliveryAddress_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_OutbDeliveryAddress")

        private static var aOutbDeliveryDocFlow_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_OutbDeliveryDocFlow")

        private static var aOutbDeliveryHeader_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_OutbDeliveryHeader")

        private static var aOutbDeliveryHeaderText_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_OutbDeliveryHeaderText")

        private static var aOutbDeliveryItem_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_OutbDeliveryItem")

        private static var aOutbDeliveryItemText_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_OutbDeliveryItemText")

        private static var aOutbDeliveryPartner_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_OutbDeliveryPartner")

        private static var aPlant_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_Plant")

        private static var aProduct_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_Product")

        private static var aProductDescription_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_ProductDescription")

        private static var aProductPlant_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_ProductPlant")

        private static var aPurOrdAccountAssignment_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_PurOrdAccountAssignment")

        private static var aPurOrdPricingElement_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_PurOrdPricingElement")

        private static var aPurchaseOrder_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_PurchaseOrder")

        private static var aPurchaseOrderItem_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_PurchaseOrderItem")

        private static var aPurchaseOrderScheduleLine_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_PurchaseOrderScheduleLine")

        private static var aSerialNmbrDelivery_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "A_SerialNmbrDelivery")

        private static var confirmPick_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "ConfirmPick")

        private static var createOutbDelivery_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "CreateOutbDelivery")

        private static var createOutbWarehouseTask_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "CreateOutbWarehouseTask")

        private static var createWarehouseTask_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "CreateWarehouseTask")

        private static var dlvHeadSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "DLVHeadSet")

        private static var dlvItemSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "DLVItemSet")

        private static var goodsIssue_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "GoodsIssue")

        private static var huHeadSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "HUHeadSet")

        private static var huItemSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "HUItemSet")

        private static var huSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "HUSet")

        private static var huSingleItemSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "HUSingleItemSet")

        private static var itemSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "ItemSet")

        private static var outbdlvHeadSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "OUTBDLVHeadSet")

        private static var outbdlvItemSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "OUTBDLVItemSet")

        private static var performPack_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "PerformPack")

        private static var purchaseOrderERPCreateRequestConfirmationInV1Set_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "PurchaseOrderERPCreateRequestConfirmation_In_V1Set")

        private static var wtSimpleConfirmSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "WTSimpleConfirmSet")

        private static var warehouse_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "Warehouse")

        private static var warehouseTaskDELETE_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "WarehouseTaskDELETE")

        private static var warehouseTaskSet_: EntitySet = Ec1MetadataParser.parsed.entitySet(withName: "WarehouseTaskSet")

        public static var aInbDeliveryAddressTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryAddressTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryAddressTypeSet_ = value
                }
            }
        }

        public static var aInbDeliveryDocFlowTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryDocFlowTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryDocFlowTypeSet_ = value
                }
            }
        }

        public static var aInbDeliveryHeader: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryHeader_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryHeader_ = value
                }
            }
        }

        public static var aInbDeliveryHeaderTextTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryHeaderTextTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryHeaderTextTypeSet_ = value
                }
            }
        }

        public static var aInbDeliveryItem: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryItem_ = value
                }
            }
        }

        public static var aInbDeliveryItemTextTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryItemTextTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryItemTextTypeSet_ = value
                }
            }
        }

        public static var aInbDeliveryPartner: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliveryPartner_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliveryPartner_ = value
                }
            }
        }

        public static var aInbDeliverySerialNmbrTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aInbDeliverySerialNmbrTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aInbDeliverySerialNmbrTypeSet_ = value
                }
            }
        }

        public static var aMaintenanceItemObjListTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aMaintenanceItemObjListTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aMaintenanceItemObjListTypeSet_ = value
                }
            }
        }

        public static var aMaintenanceItemObject: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aMaintenanceItemObject_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aMaintenanceItemObject_ = value
                }
            }
        }

        public static var aMaterialDocumentHeader: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aMaterialDocumentHeader_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aMaterialDocumentHeader_ = value
                }
            }
        }

        public static var aMaterialDocumentItemTypeSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aMaterialDocumentItemTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aMaterialDocumentItemTypeSet_ = value
                }
            }
        }

        public static var aOutbDeliveryAddress: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aOutbDeliveryAddress_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aOutbDeliveryAddress_ = value
                }
            }
        }

        public static var aOutbDeliveryDocFlow: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aOutbDeliveryDocFlow_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aOutbDeliveryDocFlow_ = value
                }
            }
        }

        public static var aOutbDeliveryHeader: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aOutbDeliveryHeader_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aOutbDeliveryHeader_ = value
                }
            }
        }

        public static var aOutbDeliveryHeaderText: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aOutbDeliveryHeaderText_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aOutbDeliveryHeaderText_ = value
                }
            }
        }

        public static var aOutbDeliveryItem: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aOutbDeliveryItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aOutbDeliveryItem_ = value
                }
            }
        }

        public static var aOutbDeliveryItemText: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aOutbDeliveryItemText_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aOutbDeliveryItemText_ = value
                }
            }
        }

        public static var aOutbDeliveryPartner: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aOutbDeliveryPartner_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aOutbDeliveryPartner_ = value
                }
            }
        }

        public static var aPlant: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aPlant_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aPlant_ = value
                }
            }
        }

        public static var aProduct: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aProduct_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aProduct_ = value
                }
            }
        }

        public static var aProductDescription: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aProductDescription_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aProductDescription_ = value
                }
            }
        }

        public static var aProductPlant: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aProductPlant_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aProductPlant_ = value
                }
            }
        }

        public static var aPurOrdAccountAssignment: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aPurOrdAccountAssignment_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aPurOrdAccountAssignment_ = value
                }
            }
        }

        public static var aPurOrdPricingElement: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aPurOrdPricingElement_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aPurOrdPricingElement_ = value
                }
            }
        }

        public static var aPurchaseOrder: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aPurchaseOrder_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aPurchaseOrder_ = value
                }
            }
        }

        public static var aPurchaseOrderItem: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aPurchaseOrderItem_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aPurchaseOrderItem_ = value
                }
            }
        }

        public static var aPurchaseOrderScheduleLine: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aPurchaseOrderScheduleLine_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aPurchaseOrderScheduleLine_ = value
                }
            }
        }

        public static var aSerialNmbrDelivery: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.aSerialNmbrDelivery_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.aSerialNmbrDelivery_ = value
                }
            }
        }

        public static var confirmPick: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.confirmPick_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.confirmPick_ = value
                }
            }
        }

        public static var createOutbDelivery: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.createOutbDelivery_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.createOutbDelivery_ = value
                }
            }
        }

        public static var createOutbWarehouseTask: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.createOutbWarehouseTask_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.createOutbWarehouseTask_ = value
                }
            }
        }

        public static var createWarehouseTask: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.createWarehouseTask_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.createWarehouseTask_ = value
                }
            }
        }

        public static var dlvHeadSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.dlvHeadSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.dlvHeadSet_ = value
                }
            }
        }

        public static var dlvItemSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.dlvItemSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.dlvItemSet_ = value
                }
            }
        }

        public static var goodsIssue: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.goodsIssue_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.goodsIssue_ = value
                }
            }
        }

        public static var huHeadSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.huHeadSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.huHeadSet_ = value
                }
            }
        }

        public static var huItemSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.huItemSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.huItemSet_ = value
                }
            }
        }

        public static var huSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.huSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.huSet_ = value
                }
            }
        }

        public static var huSingleItemSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.huSingleItemSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.huSingleItemSet_ = value
                }
            }
        }

        public static var itemSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.itemSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.itemSet_ = value
                }
            }
        }

        public static var outbdlvHeadSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.outbdlvHeadSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.outbdlvHeadSet_ = value
                }
            }
        }

        public static var outbdlvItemSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.outbdlvItemSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.outbdlvItemSet_ = value
                }
            }
        }

        public static var performPack: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.performPack_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.performPack_ = value
                }
            }
        }

        public static var purchaseOrderERPCreateRequestConfirmationInV1Set: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.purchaseOrderERPCreateRequestConfirmationInV1Set_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.purchaseOrderERPCreateRequestConfirmationInV1Set_ = value
                }
            }
        }

        public static var warehouse: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.warehouse_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.warehouse_ = value
                }
            }
        }

        public static var warehouseTaskDELETE: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.warehouseTaskDELETE_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.warehouseTaskDELETE_ = value
                }
            }
        }

        public static var warehouseTaskSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.warehouseTaskSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.warehouseTaskSet_ = value
                }
            }
        }

        public static var wtSimpleConfirmSet: EntitySet {
            get {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    return Ec1Metadata.EntitySets.wtSimpleConfirmSet_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.EntitySets.self)
                defer { objc_sync_exit(Ec1Metadata.EntitySets.self) }
                do {
                    Ec1Metadata.EntitySets.wtSimpleConfirmSet_ = value
                }
            }
        }
    }

    public class ActionImports {
        private static var createTask_: DataMethod = Ec1MetadataParser.parsed.dataMethod(withName: "CreateTask")

        public static var createTask: DataMethod {
            get {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    return Ec1Metadata.ActionImports.createTask_
                }
            }
            set(value) {
                objc_sync_enter(Ec1Metadata.ActionImports.self)
                defer { objc_sync_exit(Ec1Metadata.ActionImports.self) }
                do {
                    Ec1Metadata.ActionImports.createTask_ = value
                }
            }
        }
    }
}
