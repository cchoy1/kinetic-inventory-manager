// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class Quantity2: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var unitCode_: Property = Ec1Metadata.ComplexTypes.quantity2.property(withName: "unitCode")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.ComplexTypes.quantity2)
    }

    open func copy() -> Quantity2 {
        return CastRequired<Quantity2>.from(self.copyComplex())
    }

    open override var isProxy: Bool {
        return true
    }

    open var old: Quantity2 {
        return CastRequired<Quantity2>.from(self.oldComplex)
    }

    open class var unitCode: Property {
        get {
            objc_sync_enter(Quantity2.self)
            defer { objc_sync_exit(Quantity2.self) }
            do {
                return Quantity2.unitCode_
            }
        }
        set(value) {
            objc_sync_enter(Quantity2.self)
            defer { objc_sync_exit(Quantity2.self) }
            do {
                Quantity2.unitCode_ = value
            }
        }
    }

    open var unitCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Quantity2.unitCode))
        }
        set(value) {
            self.setOptionalValue(for: Quantity2.unitCode, to: StringValue.of(optional: value))
        }
    }
}
