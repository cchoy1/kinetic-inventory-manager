// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class Item2: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var id_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "ID")

    private static var groupID_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "GroupID")

    private static var typeCode__: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "TypeCode")

    private static var processingTypeCode_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "ProcessingTypeCode")

    private static var quantity_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "Quantity")

    private static var receivingPlantID_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "ReceivingPlantID")

    private static var languageCode_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "languageCode")

    private static var currencyCode_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "currencyCode")

    private static var unitCode_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "unitCode")

    private static var requirementCode_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "RequirementCode")

    private static var internalID_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "InternalID")

    private static var intrenalID_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "IntrenalID")

    private static var product_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "Product")

    private static var transportServiceLevelCode_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "TransportServiceLevelCode")

    private static var productTaxationCharacteristicsCode_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "ProductTaxationCharacteristicsCode")

    private static var productionOrderID_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "ProductionOrderID")

    private static var maintenanceOrderID_: Property = Ec1Metadata.ComplexTypes.item2.property(withName: "MaintenanceOrderID")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.ComplexTypes.item2)
    }

    open func copy() -> Item2 {
        return CastRequired<Item2>.from(self.copyComplex())
    }

    open class var currencyCode: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.currencyCode_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.currencyCode_ = value
            }
        }
    }

    open var currencyCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.currencyCode))
        }
        set(value) {
            self.setOptionalValue(for: Item2.currencyCode, to: StringValue.of(optional: value))
        }
    }

    open class var groupID: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.groupID_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.groupID_ = value
            }
        }
    }

    open var groupID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.groupID))
        }
        set(value) {
            self.setOptionalValue(for: Item2.groupID, to: StringValue.of(optional: value))
        }
    }

    open class var id: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.id_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.id_ = value
            }
        }
    }

    open var id: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.id))
        }
        set(value) {
            self.setOptionalValue(for: Item2.id, to: StringValue.of(optional: value))
        }
    }

    open class var internalID: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.internalID_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.internalID_ = value
            }
        }
    }

    open var internalID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.internalID))
        }
        set(value) {
            self.setOptionalValue(for: Item2.internalID, to: StringValue.of(optional: value))
        }
    }

    open class var intrenalID: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.intrenalID_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.intrenalID_ = value
            }
        }
    }

    open var intrenalID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.intrenalID))
        }
        set(value) {
            self.setOptionalValue(for: Item2.intrenalID, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var languageCode: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.languageCode_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.languageCode_ = value
            }
        }
    }

    open var languageCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.languageCode))
        }
        set(value) {
            self.setOptionalValue(for: Item2.languageCode, to: StringValue.of(optional: value))
        }
    }

    open class var maintenanceOrderID: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.maintenanceOrderID_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.maintenanceOrderID_ = value
            }
        }
    }

    open var maintenanceOrderID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.maintenanceOrderID))
        }
        set(value) {
            self.setOptionalValue(for: Item2.maintenanceOrderID, to: StringValue.of(optional: value))
        }
    }

    open var old: Item2 {
        return CastRequired<Item2>.from(self.oldComplex)
    }

    open class var processingTypeCode: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.processingTypeCode_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.processingTypeCode_ = value
            }
        }
    }

    open var processingTypeCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.processingTypeCode))
        }
        set(value) {
            self.setOptionalValue(for: Item2.processingTypeCode, to: StringValue.of(optional: value))
        }
    }

    open class var product: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.product_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.product_ = value
            }
        }
    }

    open var product: Product? {
        get {
            return CastOptional<Product>.from(self.optionalValue(for: Item2.product))
        }
        set(value) {
            self.setOptionalValue(for: Item2.product, to: value)
        }
    }

    open class var productTaxationCharacteristicsCode: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.productTaxationCharacteristicsCode_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.productTaxationCharacteristicsCode_ = value
            }
        }
    }

    open var productTaxationCharacteristicsCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.productTaxationCharacteristicsCode))
        }
        set(value) {
            self.setOptionalValue(for: Item2.productTaxationCharacteristicsCode, to: StringValue.of(optional: value))
        }
    }

    open class var productionOrderID: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.productionOrderID_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.productionOrderID_ = value
            }
        }
    }

    open var productionOrderID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.productionOrderID))
        }
        set(value) {
            self.setOptionalValue(for: Item2.productionOrderID, to: StringValue.of(optional: value))
        }
    }

    open class var quantity: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.quantity_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.quantity_ = value
            }
        }
    }

    open var quantity: Quantity2? {
        get {
            return CastOptional<Quantity2>.from(self.optionalValue(for: Item2.quantity))
        }
        set(value) {
            self.setOptionalValue(for: Item2.quantity, to: value)
        }
    }

    open class var receivingPlantID: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.receivingPlantID_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.receivingPlantID_ = value
            }
        }
    }

    open var receivingPlantID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.receivingPlantID))
        }
        set(value) {
            self.setOptionalValue(for: Item2.receivingPlantID, to: StringValue.of(optional: value))
        }
    }

    open class var requirementCode: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.requirementCode_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.requirementCode_ = value
            }
        }
    }

    open var requirementCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.requirementCode))
        }
        set(value) {
            self.setOptionalValue(for: Item2.requirementCode, to: StringValue.of(optional: value))
        }
    }

    open class var transportServiceLevelCode: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.transportServiceLevelCode_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.transportServiceLevelCode_ = value
            }
        }
    }

    open var transportServiceLevelCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.transportServiceLevelCode))
        }
        set(value) {
            self.setOptionalValue(for: Item2.transportServiceLevelCode, to: StringValue.of(optional: value))
        }
    }

    open class var typeCode_: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.typeCode__
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.typeCode__ = value
            }
        }
    }

    open var typeCode_: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.typeCode_))
        }
        set(value) {
            self.setOptionalValue(for: Item2.typeCode_, to: StringValue.of(optional: value))
        }
    }

    open class var unitCode: Property {
        get {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                return Item2.unitCode_
            }
        }
        set(value) {
            objc_sync_enter(Item2.self)
            defer { objc_sync_exit(Item2.self) }
            do {
                Item2.unitCode_ = value
            }
        }
    }

    open var unitCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Item2.unitCode))
        }
        set(value) {
            self.setOptionalValue(for: Item2.unitCode, to: StringValue.of(optional: value))
        }
    }
}
