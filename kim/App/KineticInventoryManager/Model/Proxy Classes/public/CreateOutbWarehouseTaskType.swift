// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class CreateOutbWarehouseTaskType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var outboundDeliveryUUID_: Property = Ec1Metadata.EntityTypes.createOutbWarehouseTaskType.property(withName: "OutboundDeliveryUUID")

    private static var returnBool_: Property = Ec1Metadata.EntityTypes.createOutbWarehouseTaskType.property(withName: "ReturnBool")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.createOutbWarehouseTaskType)
    }

    open class func array(from: EntityValueList) -> [CreateOutbWarehouseTaskType] {
        return ArrayConverter.convert(from.toArray(), [CreateOutbWarehouseTaskType]())
    }

    open func copy() -> CreateOutbWarehouseTaskType {
        return CastRequired<CreateOutbWarehouseTaskType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(outboundDeliveryUUID: GuidValue?) -> EntityKey {
        return EntityKey().with(name: "OutboundDeliveryUUID", value: outboundDeliveryUUID)
    }

    open var old: CreateOutbWarehouseTaskType {
        return CastRequired<CreateOutbWarehouseTaskType>.from(self.oldEntity)
    }

    open class var outboundDeliveryUUID: Property {
        get {
            objc_sync_enter(CreateOutbWarehouseTaskType.self)
            defer { objc_sync_exit(CreateOutbWarehouseTaskType.self) }
            do {
                return CreateOutbWarehouseTaskType.outboundDeliveryUUID_
            }
        }
        set(value) {
            objc_sync_enter(CreateOutbWarehouseTaskType.self)
            defer { objc_sync_exit(CreateOutbWarehouseTaskType.self) }
            do {
                CreateOutbWarehouseTaskType.outboundDeliveryUUID_ = value
            }
        }
    }

    open var outboundDeliveryUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: CreateOutbWarehouseTaskType.outboundDeliveryUUID))
        }
        set(value) {
            self.setOptionalValue(for: CreateOutbWarehouseTaskType.outboundDeliveryUUID, to: value)
        }
    }

    open class var returnBool: Property {
        get {
            objc_sync_enter(CreateOutbWarehouseTaskType.self)
            defer { objc_sync_exit(CreateOutbWarehouseTaskType.self) }
            do {
                return CreateOutbWarehouseTaskType.returnBool_
            }
        }
        set(value) {
            objc_sync_enter(CreateOutbWarehouseTaskType.self)
            defer { objc_sync_exit(CreateOutbWarehouseTaskType.self) }
            do {
                CreateOutbWarehouseTaskType.returnBool_ = value
            }
        }
    }

    open var returnBool: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: CreateOutbWarehouseTaskType.returnBool))
        }
        set(value) {
            self.setOptionalValue(for: CreateOutbWarehouseTaskType.returnBool, to: BooleanValue.of(optional: value))
        }
    }
}
