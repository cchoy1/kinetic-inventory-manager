// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class HUSingleItem: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var handlingUnitItemUUID_: Property = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "HandlingUnitItemUUID")

    private static var handlingUnitHeadUUID_: Property = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "HandlingUnitHeadUUID")

    private static var handlingUnitHeadID_: Property = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "HandlingUnitHeadID")

    private static var packageMaterial_: Property = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "PackageMaterial")

    private static var handlingUnitType_: Property = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "HandlingUnitType")

    private static var itemQuantity_: Property = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "ItemQuantity")

    private static var itemQuantityUnit_: Property = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "ItemQuantityUnit")

    private static var inboundDeliveryUUID_: Property = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "InboundDeliveryUUID")

    private static var inboundDeliveryItemUUID_: Property = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "InboundDeliveryItemUUID")

    private static var warehouseNumber_: Property = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "WarehouseNumber")

    private static var lastChangedDateTime_: Property = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "LastChangedDateTime")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.huSingleItem)
    }

    open class func array(from: EntityValueList) -> [HUSingleItem] {
        return ArrayConverter.convert(from.toArray(), [HUSingleItem]())
    }

    open func copy() -> HUSingleItem {
        return CastRequired<HUSingleItem>.from(self.copyEntity())
    }

    open class var handlingUnitHeadID: Property {
        get {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                return HUSingleItem.handlingUnitHeadID_
            }
        }
        set(value) {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                HUSingleItem.handlingUnitHeadID_ = value
            }
        }
    }

    open var handlingUnitHeadID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUSingleItem.handlingUnitHeadID))
        }
        set(value) {
            self.setOptionalValue(for: HUSingleItem.handlingUnitHeadID, to: StringValue.of(optional: value))
        }
    }

    open class var handlingUnitHeadUUID: Property {
        get {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                return HUSingleItem.handlingUnitHeadUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                HUSingleItem.handlingUnitHeadUUID_ = value
            }
        }
    }

    open var handlingUnitHeadUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUSingleItem.handlingUnitHeadUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUSingleItem.handlingUnitHeadUUID, to: value)
        }
    }

    open class var handlingUnitItemUUID: Property {
        get {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                return HUSingleItem.handlingUnitItemUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                HUSingleItem.handlingUnitItemUUID_ = value
            }
        }
    }

    open var handlingUnitItemUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUSingleItem.handlingUnitItemUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUSingleItem.handlingUnitItemUUID, to: value)
        }
    }

    open class var handlingUnitType: Property {
        get {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                return HUSingleItem.handlingUnitType_
            }
        }
        set(value) {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                HUSingleItem.handlingUnitType_ = value
            }
        }
    }

    open var handlingUnitType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUSingleItem.handlingUnitType))
        }
        set(value) {
            self.setOptionalValue(for: HUSingleItem.handlingUnitType, to: StringValue.of(optional: value))
        }
    }

    open class var inboundDeliveryItemUUID: Property {
        get {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                return HUSingleItem.inboundDeliveryItemUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                HUSingleItem.inboundDeliveryItemUUID_ = value
            }
        }
    }

    open var inboundDeliveryItemUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUSingleItem.inboundDeliveryItemUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUSingleItem.inboundDeliveryItemUUID, to: value)
        }
    }

    open class var inboundDeliveryUUID: Property {
        get {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                return HUSingleItem.inboundDeliveryUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                HUSingleItem.inboundDeliveryUUID_ = value
            }
        }
    }

    open var inboundDeliveryUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUSingleItem.inboundDeliveryUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUSingleItem.inboundDeliveryUUID, to: value)
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var itemQuantity: Property {
        get {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                return HUSingleItem.itemQuantity_
            }
        }
        set(value) {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                HUSingleItem.itemQuantity_ = value
            }
        }
    }

    open var itemQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: HUSingleItem.itemQuantity))
        }
        set(value) {
            self.setOptionalValue(for: HUSingleItem.itemQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemQuantityUnit: Property {
        get {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                return HUSingleItem.itemQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                HUSingleItem.itemQuantityUnit_ = value
            }
        }
    }

    open var itemQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUSingleItem.itemQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: HUSingleItem.itemQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class func key(handlingUnitItemUUID: GuidValue?, handlingUnitHeadUUID: GuidValue?) -> EntityKey {
        return EntityKey().with(name: "HandlingUnitItemUUID", value: handlingUnitItemUUID).with(name: "HandlingUnitHeadUUID", value: handlingUnitHeadUUID)
    }

    open class var lastChangedDateTime: Property {
        get {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                return HUSingleItem.lastChangedDateTime_
            }
        }
        set(value) {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                HUSingleItem.lastChangedDateTime_ = value
            }
        }
    }

    open var lastChangedDateTime: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: HUSingleItem.lastChangedDateTime))
        }
        set(value) {
            self.setOptionalValue(for: HUSingleItem.lastChangedDateTime, to: value)
        }
    }

    open var old: HUSingleItem {
        return CastRequired<HUSingleItem>.from(self.oldEntity)
    }

    open class var packageMaterial: Property {
        get {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                return HUSingleItem.packageMaterial_
            }
        }
        set(value) {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                HUSingleItem.packageMaterial_ = value
            }
        }
    }

    open var packageMaterial: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUSingleItem.packageMaterial))
        }
        set(value) {
            self.setOptionalValue(for: HUSingleItem.packageMaterial, to: StringValue.of(optional: value))
        }
    }

    open class var warehouseNumber: Property {
        get {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                return HUSingleItem.warehouseNumber_
            }
        }
        set(value) {
            objc_sync_enter(HUSingleItem.self)
            defer { objc_sync_exit(HUSingleItem.self) }
            do {
                HUSingleItem.warehouseNumber_ = value
            }
        }
    }

    open var warehouseNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUSingleItem.warehouseNumber))
        }
        set(value) {
            self.setOptionalValue(for: HUSingleItem.warehouseNumber, to: StringValue.of(optional: value))
        }
    }
}
