// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class WTSimpleConfirm: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var lgnum_: Property = Ec1Metadata.EntityTypes.wtSimpleConfirm.property(withName: "Lgnum")

    private static var tanum_: Property = Ec1Metadata.EntityTypes.wtSimpleConfirm.property(withName: "Tanum")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.wtSimpleConfirm)
    }

    open class func array(from: EntityValueList) -> [WTSimpleConfirm] {
        return ArrayConverter.convert(from.toArray(), [WTSimpleConfirm]())
    }

    open func copy() -> WTSimpleConfirm {
        return CastRequired<WTSimpleConfirm>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(lgnum: String?, tanum: String?) -> EntityKey {
        return EntityKey().with(name: "Lgnum", value: StringValue.of(optional: lgnum)).with(name: "Tanum", value: StringValue.of(optional: tanum))
    }

    open class var lgnum: Property {
        get {
            objc_sync_enter(WTSimpleConfirm.self)
            defer { objc_sync_exit(WTSimpleConfirm.self) }
            do {
                return WTSimpleConfirm.lgnum_
            }
        }
        set(value) {
            objc_sync_enter(WTSimpleConfirm.self)
            defer { objc_sync_exit(WTSimpleConfirm.self) }
            do {
                WTSimpleConfirm.lgnum_ = value
            }
        }
    }

    open var lgnum: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WTSimpleConfirm.lgnum))
        }
        set(value) {
            self.setOptionalValue(for: WTSimpleConfirm.lgnum, to: StringValue.of(optional: value))
        }
    }

    open var old: WTSimpleConfirm {
        return CastRequired<WTSimpleConfirm>.from(self.oldEntity)
    }

    open class var tanum: Property {
        get {
            objc_sync_enter(WTSimpleConfirm.self)
            defer { objc_sync_exit(WTSimpleConfirm.self) }
            do {
                return WTSimpleConfirm.tanum_
            }
        }
        set(value) {
            objc_sync_enter(WTSimpleConfirm.self)
            defer { objc_sync_exit(WTSimpleConfirm.self) }
            do {
                WTSimpleConfirm.tanum_ = value
            }
        }
    }

    open var tanum: String? {
        get {
            return StringValue.optional(self.optionalValue(for: WTSimpleConfirm.tanum))
        }
        set(value) {
            self.setOptionalValue(for: WTSimpleConfirm.tanum, to: StringValue.of(optional: value))
        }
    }
}
