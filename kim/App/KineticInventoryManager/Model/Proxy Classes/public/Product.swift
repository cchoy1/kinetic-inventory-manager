// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class Product: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var internalID_: Property = Ec1Metadata.ComplexTypes.product.property(withName: "InternalID")

    private static var buyerID_: Property = Ec1Metadata.ComplexTypes.product.property(withName: "BuyerID")

    private static var sellerID_: Property = Ec1Metadata.ComplexTypes.product.property(withName: "SellerID")

    private static var revisionID_: Property = Ec1Metadata.ComplexTypes.product.property(withName: "RevisionID")

    private static var typeCode__: Property = Ec1Metadata.ComplexTypes.product.property(withName: "TypeCode")

    private static var inventoryValuationTypeCode_: Property = Ec1Metadata.ComplexTypes.product.property(withName: "InventoryValuationTypeCode")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.ComplexTypes.product)
    }

    open class var buyerID: Property {
        get {
            objc_sync_enter(Product.self)
            defer { objc_sync_exit(Product.self) }
            do {
                return Product.buyerID_
            }
        }
        set(value) {
            objc_sync_enter(Product.self)
            defer { objc_sync_exit(Product.self) }
            do {
                Product.buyerID_ = value
            }
        }
    }

    open var buyerID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Product.buyerID))
        }
        set(value) {
            self.setOptionalValue(for: Product.buyerID, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> Product {
        return CastRequired<Product>.from(self.copyComplex())
    }

    open class var internalID: Property {
        get {
            objc_sync_enter(Product.self)
            defer { objc_sync_exit(Product.self) }
            do {
                return Product.internalID_
            }
        }
        set(value) {
            objc_sync_enter(Product.self)
            defer { objc_sync_exit(Product.self) }
            do {
                Product.internalID_ = value
            }
        }
    }

    open var internalID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Product.internalID))
        }
        set(value) {
            self.setOptionalValue(for: Product.internalID, to: StringValue.of(optional: value))
        }
    }

    open class var inventoryValuationTypeCode: Property {
        get {
            objc_sync_enter(Product.self)
            defer { objc_sync_exit(Product.self) }
            do {
                return Product.inventoryValuationTypeCode_
            }
        }
        set(value) {
            objc_sync_enter(Product.self)
            defer { objc_sync_exit(Product.self) }
            do {
                Product.inventoryValuationTypeCode_ = value
            }
        }
    }

    open var inventoryValuationTypeCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Product.inventoryValuationTypeCode))
        }
        set(value) {
            self.setOptionalValue(for: Product.inventoryValuationTypeCode, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open var old: Product {
        return CastRequired<Product>.from(self.oldComplex)
    }

    open class var revisionID: Property {
        get {
            objc_sync_enter(Product.self)
            defer { objc_sync_exit(Product.self) }
            do {
                return Product.revisionID_
            }
        }
        set(value) {
            objc_sync_enter(Product.self)
            defer { objc_sync_exit(Product.self) }
            do {
                Product.revisionID_ = value
            }
        }
    }

    open var revisionID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Product.revisionID))
        }
        set(value) {
            self.setOptionalValue(for: Product.revisionID, to: StringValue.of(optional: value))
        }
    }

    open class var sellerID: Property {
        get {
            objc_sync_enter(Product.self)
            defer { objc_sync_exit(Product.self) }
            do {
                return Product.sellerID_
            }
        }
        set(value) {
            objc_sync_enter(Product.self)
            defer { objc_sync_exit(Product.self) }
            do {
                Product.sellerID_ = value
            }
        }
    }

    open var sellerID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Product.sellerID))
        }
        set(value) {
            self.setOptionalValue(for: Product.sellerID, to: StringValue.of(optional: value))
        }
    }

    open class var typeCode_: Property {
        get {
            objc_sync_enter(Product.self)
            defer { objc_sync_exit(Product.self) }
            do {
                return Product.typeCode__
            }
        }
        set(value) {
            objc_sync_enter(Product.self)
            defer { objc_sync_exit(Product.self) }
            do {
                Product.typeCode__ = value
            }
        }
    }

    open var typeCode_: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Product.typeCode_))
        }
        set(value) {
            self.setOptionalValue(for: Product.typeCode_, to: StringValue.of(optional: value))
        }
    }
}
