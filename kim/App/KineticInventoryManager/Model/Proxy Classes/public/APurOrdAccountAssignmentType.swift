// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class APurOrdAccountAssignmentType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var purchaseOrder_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "PurchaseOrder")

    private static var purchaseOrderItem_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "PurchaseOrderItem")

    private static var accountAssignmentNumber_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "AccountAssignmentNumber")

    private static var isDeleted__: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "IsDeleted")

    private static var purchaseOrderQuantityUnit_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "PurchaseOrderQuantityUnit")

    private static var quantity_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "Quantity")

    private static var multipleAcctAssgmtDistrPercent_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "MultipleAcctAssgmtDistrPercent")

    private static var purgDocNetAmount_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "PurgDocNetAmount")

    private static var glAccount_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "GLAccount")

    private static var businessArea_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "BusinessArea")

    private static var costCenter_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "CostCenter")

    private static var salesOrder_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "SalesOrder")

    private static var salesOrderItem_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "SalesOrderItem")

    private static var salesOrderScheduleLine_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "SalesOrderScheduleLine")

    private static var masterFixedAsset_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "MasterFixedAsset")

    private static var fixedAsset_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "FixedAsset")

    private static var goodsRecipientName_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "GoodsRecipientName")

    private static var unloadingPointName_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "UnloadingPointName")

    private static var controllingArea_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "ControllingArea")

    private static var costObject_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "CostObject")

    private static var orderID_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "OrderID")

    private static var profitCenter_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "ProfitCenter")

    private static var wbsElementInternalID_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "WBSElementInternalID")

    private static var wbsElement_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "WBSElement")

    private static var projectNetwork_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "ProjectNetwork")

    private static var realEstateObject_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "RealEstateObject")

    private static var partnerAccountNumber_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "PartnerAccountNumber")

    private static var commitmentItem_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "CommitmentItem")

    private static var jointVentureRecoveryCode_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "JointVentureRecoveryCode")

    private static var fundsCenter_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "FundsCenter")

    private static var fund_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "Fund")

    private static var functionalArea_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "FunctionalArea")

    private static var settlementReferenceDate_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "SettlementReferenceDate")

    private static var taxCode_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "TaxCode")

    private static var taxJurisdiction_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "TaxJurisdiction")

    private static var costCtrActivityType_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "CostCtrActivityType")

    private static var businessProcess_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "BusinessProcess")

    private static var earmarkedFundsDocument_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "EarmarkedFundsDocument")

    private static var grantID_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "GrantID")

    private static var budgetPeriod_: Property = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "BudgetPeriod")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType)
    }

    open class var accountAssignmentNumber: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.accountAssignmentNumber_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.accountAssignmentNumber_ = value
            }
        }
    }

    open var accountAssignmentNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.accountAssignmentNumber))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.accountAssignmentNumber, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [APurOrdAccountAssignmentType] {
        return ArrayConverter.convert(from.toArray(), [APurOrdAccountAssignmentType]())
    }

    open class var budgetPeriod: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.budgetPeriod_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.budgetPeriod_ = value
            }
        }
    }

    open var budgetPeriod: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.budgetPeriod))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.budgetPeriod, to: StringValue.of(optional: value))
        }
    }

    open class var businessArea: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.businessArea_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.businessArea_ = value
            }
        }
    }

    open var businessArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.businessArea))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.businessArea, to: StringValue.of(optional: value))
        }
    }

    open class var businessProcess: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.businessProcess_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.businessProcess_ = value
            }
        }
    }

    open var businessProcess: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.businessProcess))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.businessProcess, to: StringValue.of(optional: value))
        }
    }

    open class var commitmentItem: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.commitmentItem_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.commitmentItem_ = value
            }
        }
    }

    open var commitmentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.commitmentItem))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.commitmentItem, to: StringValue.of(optional: value))
        }
    }

    open class var controllingArea: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.controllingArea_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.controllingArea_ = value
            }
        }
    }

    open var controllingArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.controllingArea))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.controllingArea, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> APurOrdAccountAssignmentType {
        return CastRequired<APurOrdAccountAssignmentType>.from(self.copyEntity())
    }

    open class var costCenter: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.costCenter_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.costCenter_ = value
            }
        }
    }

    open var costCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.costCenter))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.costCenter, to: StringValue.of(optional: value))
        }
    }

    open class var costCtrActivityType: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.costCtrActivityType_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.costCtrActivityType_ = value
            }
        }
    }

    open var costCtrActivityType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.costCtrActivityType))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.costCtrActivityType, to: StringValue.of(optional: value))
        }
    }

    open class var costObject: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.costObject_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.costObject_ = value
            }
        }
    }

    open var costObject: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.costObject))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.costObject, to: StringValue.of(optional: value))
        }
    }

    open class var earmarkedFundsDocument: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.earmarkedFundsDocument_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.earmarkedFundsDocument_ = value
            }
        }
    }

    open var earmarkedFundsDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.earmarkedFundsDocument))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.earmarkedFundsDocument, to: StringValue.of(optional: value))
        }
    }

    open class var fixedAsset: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.fixedAsset_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.fixedAsset_ = value
            }
        }
    }

    open var fixedAsset: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.fixedAsset))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.fixedAsset, to: StringValue.of(optional: value))
        }
    }

    open class var functionalArea: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.functionalArea_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.functionalArea_ = value
            }
        }
    }

    open var functionalArea: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.functionalArea))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.functionalArea, to: StringValue.of(optional: value))
        }
    }

    open class var fund: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.fund_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.fund_ = value
            }
        }
    }

    open var fund: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.fund))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.fund, to: StringValue.of(optional: value))
        }
    }

    open class var fundsCenter: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.fundsCenter_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.fundsCenter_ = value
            }
        }
    }

    open var fundsCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.fundsCenter))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.fundsCenter, to: StringValue.of(optional: value))
        }
    }

    open class var glAccount: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.glAccount_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.glAccount_ = value
            }
        }
    }

    open var glAccount: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.glAccount))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.glAccount, to: StringValue.of(optional: value))
        }
    }

    open class var goodsRecipientName: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.goodsRecipientName_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.goodsRecipientName_ = value
            }
        }
    }

    open var goodsRecipientName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.goodsRecipientName))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.goodsRecipientName, to: StringValue.of(optional: value))
        }
    }

    open class var grantID: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.grantID_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.grantID_ = value
            }
        }
    }

    open var grantID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.grantID))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.grantID, to: StringValue.of(optional: value))
        }
    }

    open class var isDeleted_: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.isDeleted__
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.isDeleted__ = value
            }
        }
    }

    open var isDeleted_: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.isDeleted_))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.isDeleted_, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var jointVentureRecoveryCode: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.jointVentureRecoveryCode_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.jointVentureRecoveryCode_ = value
            }
        }
    }

    open var jointVentureRecoveryCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.jointVentureRecoveryCode))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.jointVentureRecoveryCode, to: StringValue.of(optional: value))
        }
    }

    open class func key(purchaseOrder: String?, purchaseOrderItem: String?, accountAssignmentNumber: String?) -> EntityKey {
        return EntityKey().with(name: "PurchaseOrder", value: StringValue.of(optional: purchaseOrder)).with(name: "PurchaseOrderItem", value: StringValue.of(optional: purchaseOrderItem)).with(name: "AccountAssignmentNumber", value: StringValue.of(optional: accountAssignmentNumber))
    }

    open class var masterFixedAsset: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.masterFixedAsset_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.masterFixedAsset_ = value
            }
        }
    }

    open var masterFixedAsset: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.masterFixedAsset))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.masterFixedAsset, to: StringValue.of(optional: value))
        }
    }

    open class var multipleAcctAssgmtDistrPercent: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.multipleAcctAssgmtDistrPercent_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.multipleAcctAssgmtDistrPercent_ = value
            }
        }
    }

    open var multipleAcctAssgmtDistrPercent: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.multipleAcctAssgmtDistrPercent))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.multipleAcctAssgmtDistrPercent, to: DecimalValue.of(optional: value))
        }
    }

    open var old: APurOrdAccountAssignmentType {
        return CastRequired<APurOrdAccountAssignmentType>.from(self.oldEntity)
    }

    open class var orderID: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.orderID_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.orderID_ = value
            }
        }
    }

    open var orderID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.orderID))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.orderID, to: StringValue.of(optional: value))
        }
    }

    open class var partnerAccountNumber: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.partnerAccountNumber_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.partnerAccountNumber_ = value
            }
        }
    }

    open var partnerAccountNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.partnerAccountNumber))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.partnerAccountNumber, to: StringValue.of(optional: value))
        }
    }

    open class var profitCenter: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.profitCenter_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.profitCenter_ = value
            }
        }
    }

    open var profitCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.profitCenter))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.profitCenter, to: StringValue.of(optional: value))
        }
    }

    open class var projectNetwork: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.projectNetwork_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.projectNetwork_ = value
            }
        }
    }

    open var projectNetwork: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.projectNetwork))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.projectNetwork, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrder: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.purchaseOrder_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.purchaseOrder_ = value
            }
        }
    }

    open var purchaseOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.purchaseOrder))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.purchaseOrder, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderItem: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.purchaseOrderItem_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.purchaseOrderItem_ = value
            }
        }
    }

    open var purchaseOrderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.purchaseOrderItem))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.purchaseOrderItem, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderQuantityUnit: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.purchaseOrderQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.purchaseOrderQuantityUnit_ = value
            }
        }
    }

    open var purchaseOrderQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.purchaseOrderQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.purchaseOrderQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var purgDocNetAmount: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.purgDocNetAmount_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.purgDocNetAmount_ = value
            }
        }
    }

    open var purgDocNetAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.purgDocNetAmount))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.purgDocNetAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var quantity: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.quantity_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.quantity_ = value
            }
        }
    }

    open var quantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.quantity))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.quantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var realEstateObject: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.realEstateObject_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.realEstateObject_ = value
            }
        }
    }

    open var realEstateObject: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.realEstateObject))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.realEstateObject, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrder: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.salesOrder_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.salesOrder_ = value
            }
        }
    }

    open var salesOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.salesOrder))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.salesOrder, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderItem: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.salesOrderItem_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.salesOrderItem_ = value
            }
        }
    }

    open var salesOrderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.salesOrderItem))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.salesOrderItem, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderScheduleLine: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.salesOrderScheduleLine_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.salesOrderScheduleLine_ = value
            }
        }
    }

    open var salesOrderScheduleLine: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.salesOrderScheduleLine))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.salesOrderScheduleLine, to: StringValue.of(optional: value))
        }
    }

    open class var settlementReferenceDate: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.settlementReferenceDate_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.settlementReferenceDate_ = value
            }
        }
    }

    open var settlementReferenceDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: APurOrdAccountAssignmentType.settlementReferenceDate))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.settlementReferenceDate, to: value)
        }
    }

    open class var taxCode: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.taxCode_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.taxCode_ = value
            }
        }
    }

    open var taxCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.taxCode))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.taxCode, to: StringValue.of(optional: value))
        }
    }

    open class var taxJurisdiction: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.taxJurisdiction_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.taxJurisdiction_ = value
            }
        }
    }

    open var taxJurisdiction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.taxJurisdiction))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.taxJurisdiction, to: StringValue.of(optional: value))
        }
    }

    open class var unloadingPointName: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.unloadingPointName_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.unloadingPointName_ = value
            }
        }
    }

    open var unloadingPointName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.unloadingPointName))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.unloadingPointName, to: StringValue.of(optional: value))
        }
    }

    open class var wbsElement: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.wbsElement_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.wbsElement_ = value
            }
        }
    }

    open var wbsElement: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.wbsElement))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.wbsElement, to: StringValue.of(optional: value))
        }
    }

    open class var wbsElementInternalID: Property {
        get {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                return APurOrdAccountAssignmentType.wbsElementInternalID_
            }
        }
        set(value) {
            objc_sync_enter(APurOrdAccountAssignmentType.self)
            defer { objc_sync_exit(APurOrdAccountAssignmentType.self) }
            do {
                APurOrdAccountAssignmentType.wbsElementInternalID_ = value
            }
        }
    }

    open var wbsElementInternalID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: APurOrdAccountAssignmentType.wbsElementInternalID))
        }
        set(value) {
            self.setOptionalValue(for: APurOrdAccountAssignmentType.wbsElementInternalID, to: StringValue.of(optional: value))
        }
    }
}
