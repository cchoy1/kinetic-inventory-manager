// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class HUItem: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var handlingUnitUUID_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "HandlingUnitUUID")

    private static var handlingUnitParentUUID_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "HandlingUnitParentUUID")

    private static var inboundDeliveryUUID_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "InboundDeliveryUUID")

    private static var inboundDeliveryItemUUID_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "InboundDeliveryItemUUID")

    private static var handlingUnitID_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "HandlingUnitID")

    private static var product_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "Product")

    private static var productName_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "ProductName")

    private static var batch_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "Batch")

    private static var itemQuantity_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemQuantity")

    private static var itemQuantityUnit_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemQuantityUnit")

    private static var docNoPO_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "DocNoPO")

    private static var itemNoPO_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemNoPO")

    private static var itemWeight_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemWeight")

    private static var itemWeightUnit_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemWeightUnit")

    private static var itemVolume_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemVolume")

    private static var itemVolumeUnit_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemVolumeUnit")

    private static var lastChangedDateTime_: Property = Ec1Metadata.EntityTypes.huItem.property(withName: "LastChangedDateTime")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.huItem)
    }

    open class func array(from: EntityValueList) -> [HUItem] {
        return ArrayConverter.convert(from.toArray(), [HUItem]())
    }

    open class var batch: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.batch_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.batch_ = value
            }
        }
    }

    open var batch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUItem.batch))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.batch, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> HUItem {
        return CastRequired<HUItem>.from(self.copyEntity())
    }

    open class var docNoPO: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.docNoPO_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.docNoPO_ = value
            }
        }
    }

    open var docNoPO: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUItem.docNoPO))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.docNoPO, to: StringValue.of(optional: value))
        }
    }

    open class var handlingUnitID: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.handlingUnitID_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.handlingUnitID_ = value
            }
        }
    }

    open var handlingUnitID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUItem.handlingUnitID))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.handlingUnitID, to: StringValue.of(optional: value))
        }
    }

    open class var handlingUnitParentUUID: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.handlingUnitParentUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.handlingUnitParentUUID_ = value
            }
        }
    }

    open var handlingUnitParentUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUItem.handlingUnitParentUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.handlingUnitParentUUID, to: value)
        }
    }

    open class var handlingUnitUUID: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.handlingUnitUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.handlingUnitUUID_ = value
            }
        }
    }

    open var handlingUnitUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUItem.handlingUnitUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.handlingUnitUUID, to: value)
        }
    }

    open class var inboundDeliveryItemUUID: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.inboundDeliveryItemUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.inboundDeliveryItemUUID_ = value
            }
        }
    }

    open var inboundDeliveryItemUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUItem.inboundDeliveryItemUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.inboundDeliveryItemUUID, to: value)
        }
    }

    open class var inboundDeliveryUUID: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.inboundDeliveryUUID_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.inboundDeliveryUUID_ = value
            }
        }
    }

    open var inboundDeliveryUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: HUItem.inboundDeliveryUUID))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.inboundDeliveryUUID, to: value)
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var itemNoPO: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.itemNoPO_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.itemNoPO_ = value
            }
        }
    }

    open var itemNoPO: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: HUItem.itemNoPO))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.itemNoPO, to: IntegerValue.of(optional: value))
        }
    }

    open class var itemQuantity: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.itemQuantity_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.itemQuantity_ = value
            }
        }
    }

    open var itemQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: HUItem.itemQuantity))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.itemQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemQuantityUnit: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.itemQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.itemQuantityUnit_ = value
            }
        }
    }

    open var itemQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUItem.itemQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.itemQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var itemVolume: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.itemVolume_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.itemVolume_ = value
            }
        }
    }

    open var itemVolume: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: HUItem.itemVolume))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.itemVolume, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemVolumeUnit: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.itemVolumeUnit_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.itemVolumeUnit_ = value
            }
        }
    }

    open var itemVolumeUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUItem.itemVolumeUnit))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.itemVolumeUnit, to: StringValue.of(optional: value))
        }
    }

    open class var itemWeight: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.itemWeight_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.itemWeight_ = value
            }
        }
    }

    open var itemWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: HUItem.itemWeight))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.itemWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemWeightUnit: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.itemWeightUnit_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.itemWeightUnit_ = value
            }
        }
    }

    open var itemWeightUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUItem.itemWeightUnit))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.itemWeightUnit, to: StringValue.of(optional: value))
        }
    }

    open class func key(handlingUnitUUID: GuidValue?, handlingUnitParentUUID: GuidValue?, inboundDeliveryUUID: GuidValue?, inboundDeliveryItemUUID: GuidValue?) -> EntityKey {
        return EntityKey().with(name: "HandlingUnitUUID", value: handlingUnitUUID).with(name: "HandlingUnitParentUUID", value: handlingUnitParentUUID).with(name: "InboundDeliveryUUID", value: inboundDeliveryUUID).with(name: "InboundDeliveryItemUUID", value: inboundDeliveryItemUUID)
    }

    open class var lastChangedDateTime: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.lastChangedDateTime_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.lastChangedDateTime_ = value
            }
        }
    }

    open var lastChangedDateTime: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: HUItem.lastChangedDateTime))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.lastChangedDateTime, to: value)
        }
    }

    open var old: HUItem {
        return CastRequired<HUItem>.from(self.oldEntity)
    }

    open class var product: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.product_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.product_ = value
            }
        }
    }

    open var product: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUItem.product))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.product, to: StringValue.of(optional: value))
        }
    }

    open class var productName: Property {
        get {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                return HUItem.productName_
            }
        }
        set(value) {
            objc_sync_enter(HUItem.self)
            defer { objc_sync_exit(HUItem.self) }
            do {
                HUItem.productName_ = value
            }
        }
    }

    open var productName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: HUItem.productName))
        }
        set(value) {
            self.setOptionalValue(for: HUItem.productName, to: StringValue.of(optional: value))
        }
    }
}
