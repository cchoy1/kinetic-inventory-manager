// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AOutbDeliveryDocFlowType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var deliveryversion_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "Deliveryversion")

    private static var precedingDocument_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "PrecedingDocument")

    private static var precedingDocumentCategory_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "PrecedingDocumentCategory")

    private static var precedingDocumentItem_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "PrecedingDocumentItem")

    private static var subsequentdocument_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "Subsequentdocument")

    private static var quantityInBaseUnit_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "QuantityInBaseUnit")

    private static var subsequentDocumentItem_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "SubsequentDocumentItem")

    private static var sdFulfillmentCalculationRule_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "SDFulfillmentCalculationRule")

    private static var subsequentDocumentCategory_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "SubsequentDocumentCategory")

    private static var transferOrderInWrhsMgmtIsConfd_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "TransferOrderInWrhsMgmtIsConfd")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType)
    }

    open class func array(from: EntityValueList) -> [AOutbDeliveryDocFlowType] {
        return ArrayConverter.convert(from.toArray(), [AOutbDeliveryDocFlowType]())
    }

    open func copy() -> AOutbDeliveryDocFlowType {
        return CastRequired<AOutbDeliveryDocFlowType>.from(self.copyEntity())
    }

    open class var deliveryversion: Property {
        get {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                return AOutbDeliveryDocFlowType.deliveryversion_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                AOutbDeliveryDocFlowType.deliveryversion_ = value
            }
        }
    }

    open var deliveryversion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryDocFlowType.deliveryversion))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryDocFlowType.deliveryversion, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(precedingDocument: String?, precedingDocumentItem: String?, subsequentDocumentCategory: String?) -> EntityKey {
        return EntityKey().with(name: "PrecedingDocument", value: StringValue.of(optional: precedingDocument)).with(name: "PrecedingDocumentItem", value: StringValue.of(optional: precedingDocumentItem)).with(name: "SubsequentDocumentCategory", value: StringValue.of(optional: subsequentDocumentCategory))
    }

    open var old: AOutbDeliveryDocFlowType {
        return CastRequired<AOutbDeliveryDocFlowType>.from(self.oldEntity)
    }

    open class var precedingDocument: Property {
        get {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                return AOutbDeliveryDocFlowType.precedingDocument_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                AOutbDeliveryDocFlowType.precedingDocument_ = value
            }
        }
    }

    open var precedingDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryDocFlowType.precedingDocument))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryDocFlowType.precedingDocument, to: StringValue.of(optional: value))
        }
    }

    open class var precedingDocumentCategory: Property {
        get {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                return AOutbDeliveryDocFlowType.precedingDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                AOutbDeliveryDocFlowType.precedingDocumentCategory_ = value
            }
        }
    }

    open var precedingDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryDocFlowType.precedingDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryDocFlowType.precedingDocumentCategory, to: StringValue.of(optional: value))
        }
    }

    open class var precedingDocumentItem: Property {
        get {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                return AOutbDeliveryDocFlowType.precedingDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                AOutbDeliveryDocFlowType.precedingDocumentItem_ = value
            }
        }
    }

    open var precedingDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryDocFlowType.precedingDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryDocFlowType.precedingDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var quantityInBaseUnit: Property {
        get {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                return AOutbDeliveryDocFlowType.quantityInBaseUnit_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                AOutbDeliveryDocFlowType.quantityInBaseUnit_ = value
            }
        }
    }

    open var quantityInBaseUnit: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: AOutbDeliveryDocFlowType.quantityInBaseUnit))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryDocFlowType.quantityInBaseUnit, to: DecimalValue.of(optional: value))
        }
    }

    open class var sdFulfillmentCalculationRule: Property {
        get {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                return AOutbDeliveryDocFlowType.sdFulfillmentCalculationRule_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                AOutbDeliveryDocFlowType.sdFulfillmentCalculationRule_ = value
            }
        }
    }

    open var sdFulfillmentCalculationRule: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryDocFlowType.sdFulfillmentCalculationRule))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryDocFlowType.sdFulfillmentCalculationRule, to: StringValue.of(optional: value))
        }
    }

    open class var subsequentDocumentCategory: Property {
        get {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                return AOutbDeliveryDocFlowType.subsequentDocumentCategory_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                AOutbDeliveryDocFlowType.subsequentDocumentCategory_ = value
            }
        }
    }

    open var subsequentDocumentCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryDocFlowType.subsequentDocumentCategory))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryDocFlowType.subsequentDocumentCategory, to: StringValue.of(optional: value))
        }
    }

    open class var subsequentDocumentItem: Property {
        get {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                return AOutbDeliveryDocFlowType.subsequentDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                AOutbDeliveryDocFlowType.subsequentDocumentItem_ = value
            }
        }
    }

    open var subsequentDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryDocFlowType.subsequentDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryDocFlowType.subsequentDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var subsequentdocument: Property {
        get {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                return AOutbDeliveryDocFlowType.subsequentdocument_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                AOutbDeliveryDocFlowType.subsequentdocument_ = value
            }
        }
    }

    open var subsequentdocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryDocFlowType.subsequentdocument))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryDocFlowType.subsequentdocument, to: StringValue.of(optional: value))
        }
    }

    open class var transferOrderInWrhsMgmtIsConfd: Property {
        get {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                return AOutbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryDocFlowType.self)
            defer { objc_sync_exit(AOutbDeliveryDocFlowType.self) }
            do {
                AOutbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd_ = value
            }
        }
    }

    open var transferOrderInWrhsMgmtIsConfd: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AOutbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd, to: BooleanValue.of(optional: value))
        }
    }
}
