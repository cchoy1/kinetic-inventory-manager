// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class OUTBDLVItem: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var batch_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "Batch")

    private static var goodsIssueStatus_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "GoodsIssueStatus")

    private static var goodsIssueStatusName_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "GoodsIssueStatusName")

    private static var itemDeliveryQuantity_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "ItemDeliveryQuantity")

    private static var itemDeliveryQuantityUnit_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "ItemDeliveryQuantityUnit")

    private static var lastChangedDateTime_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "LastChangedDateTime")

    private static var numberOfOpenWrhsTasks_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "NumberOfOpenWrhsTasks")

    private static var outboundDelivery_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "OutboundDelivery")

    private static var outboundDeliveryItem_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "OutboundDeliveryItem")

    private static var outboundDeliveryOrderItemUUID_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "OutboundDeliveryOrderItemUUID")

    private static var outboundDeliveryUUID_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "OutboundDeliveryUUID")

    private static var plannedPickingStatus_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "PlannedPickingStatus")

    private static var plannedPickingStatusName_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "PlannedPickingStatusName")

    private static var product_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "Product")

    private static var productName_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "ProductName")

    private static var sledBbd_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "SledBbd")

    private static var numberOfSledBbd_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "NumberOfSledBbd")

    private static var uxFcAAdjustDeliveryQuantities_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "UxFcA_AdjustDeliveryQuantities")

    private static var uxFcACreateTasks_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "UxFcA_CreateTasks")

    private static var uxFcADeletable_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "UxFcA_Deletable")

    private static var uxFcAUpdatable_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "UxFcA_Updatable")

    private static var warehouseNumber_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "WarehouseNumber")

    private static var entitled_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "Entitled")

    private static var missingWTQty_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "MissingWTQty")

    private static var missingWTQtyUoM_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "MissingWTQtyUoM")

    private static var openWTQty_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "OpenWTQty")

    private static var openWTQtyUoM_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "OpenWTQtyUoM")

    private static var existsWT_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "ExistsWT")

    private static var stockDocument_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "StockDocument")

    private static var stockItem_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "StockItem")

    private static var stockDocumentType_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "StockDocumentType")

    private static var stockDocumentTypeText_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "StockDocumentTypeText")

    private static var manufacturingOrder_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "ManufacturingOrder")

    private static var psa_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "PSA")

    private static var packingStatus_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "PackingStatus")

    private static var packingStatusName_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "PackingStatusName")

    private static var blockedOverallStatus_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "BlockedOverallStatus")

    private static var blockedOverallStatusName_: Property = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "BlockedOverallStatusName")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.outbdlvItem)
    }

    open class func array(from: EntityValueList) -> [OUTBDLVItem] {
        return ArrayConverter.convert(from.toArray(), [OUTBDLVItem]())
    }

    open class var batch: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.batch_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.batch_ = value
            }
        }
    }

    open var batch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.batch))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.batch, to: StringValue.of(optional: value))
        }
    }

    open class var blockedOverallStatus: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.blockedOverallStatus_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.blockedOverallStatus_ = value
            }
        }
    }

    open var blockedOverallStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.blockedOverallStatus))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.blockedOverallStatus, to: StringValue.of(optional: value))
        }
    }

    open class var blockedOverallStatusName: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.blockedOverallStatusName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.blockedOverallStatusName_ = value
            }
        }
    }

    open var blockedOverallStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.blockedOverallStatusName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.blockedOverallStatusName, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> OUTBDLVItem {
        return CastRequired<OUTBDLVItem>.from(self.copyEntity())
    }

    open class var entitled: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.entitled_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.entitled_ = value
            }
        }
    }

    open var entitled: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.entitled))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.entitled, to: StringValue.of(optional: value))
        }
    }

    open class var existsWT: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.existsWT_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.existsWT_ = value
            }
        }
    }

    open var existsWT: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: OUTBDLVItem.existsWT))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.existsWT, to: BooleanValue.of(optional: value))
        }
    }

    open class var goodsIssueStatus: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.goodsIssueStatus_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.goodsIssueStatus_ = value
            }
        }
    }

    open var goodsIssueStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.goodsIssueStatus))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.goodsIssueStatus, to: StringValue.of(optional: value))
        }
    }

    open class var goodsIssueStatusName: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.goodsIssueStatusName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.goodsIssueStatusName_ = value
            }
        }
    }

    open var goodsIssueStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.goodsIssueStatusName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.goodsIssueStatusName, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var itemDeliveryQuantity: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.itemDeliveryQuantity_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.itemDeliveryQuantity_ = value
            }
        }
    }

    open var itemDeliveryQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: OUTBDLVItem.itemDeliveryQuantity))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.itemDeliveryQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemDeliveryQuantityUnit: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.itemDeliveryQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.itemDeliveryQuantityUnit_ = value
            }
        }
    }

    open var itemDeliveryQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.itemDeliveryQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.itemDeliveryQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class func key(outboundDeliveryOrderItemUUID: GuidValue?, outboundDeliveryUUID: GuidValue?) -> EntityKey {
        return EntityKey().with(name: "OutboundDeliveryOrderItemUUID", value: outboundDeliveryOrderItemUUID).with(name: "OutboundDeliveryUUID", value: outboundDeliveryUUID)
    }

    open class var lastChangedDateTime: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.lastChangedDateTime_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.lastChangedDateTime_ = value
            }
        }
    }

    open var lastChangedDateTime: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: OUTBDLVItem.lastChangedDateTime))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.lastChangedDateTime, to: value)
        }
    }

    open class var manufacturingOrder: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.manufacturingOrder_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.manufacturingOrder_ = value
            }
        }
    }

    open var manufacturingOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.manufacturingOrder))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.manufacturingOrder, to: StringValue.of(optional: value))
        }
    }

    open class var missingWTQty: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.missingWTQty_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.missingWTQty_ = value
            }
        }
    }

    open var missingWTQty: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: OUTBDLVItem.missingWTQty))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.missingWTQty, to: DecimalValue.of(optional: value))
        }
    }

    open class var missingWTQtyUoM: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.missingWTQtyUoM_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.missingWTQtyUoM_ = value
            }
        }
    }

    open var missingWTQtyUoM: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.missingWTQtyUoM))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.missingWTQtyUoM, to: StringValue.of(optional: value))
        }
    }

    open class var numberOfOpenWrhsTasks: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.numberOfOpenWrhsTasks_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.numberOfOpenWrhsTasks_ = value
            }
        }
    }

    open var numberOfOpenWrhsTasks: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: OUTBDLVItem.numberOfOpenWrhsTasks))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.numberOfOpenWrhsTasks, to: IntValue.of(optional: value))
        }
    }

    open class var numberOfSledBbd: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.numberOfSledBbd_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.numberOfSledBbd_ = value
            }
        }
    }

    open var numberOfSledBbd: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: OUTBDLVItem.numberOfSledBbd))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.numberOfSledBbd, to: IntValue.of(optional: value))
        }
    }

    open var old: OUTBDLVItem {
        return CastRequired<OUTBDLVItem>.from(self.oldEntity)
    }

    open class var openWTQty: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.openWTQty_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.openWTQty_ = value
            }
        }
    }

    open var openWTQty: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: OUTBDLVItem.openWTQty))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.openWTQty, to: DecimalValue.of(optional: value))
        }
    }

    open class var openWTQtyUoM: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.openWTQtyUoM_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.openWTQtyUoM_ = value
            }
        }
    }

    open var openWTQtyUoM: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.openWTQtyUoM))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.openWTQtyUoM, to: StringValue.of(optional: value))
        }
    }

    open class var outboundDelivery: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.outboundDelivery_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.outboundDelivery_ = value
            }
        }
    }

    open var outboundDelivery: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.outboundDelivery))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.outboundDelivery, to: StringValue.of(optional: value))
        }
    }

    open class var outboundDeliveryItem: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.outboundDeliveryItem_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.outboundDeliveryItem_ = value
            }
        }
    }

    open var outboundDeliveryItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.outboundDeliveryItem))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.outboundDeliveryItem, to: StringValue.of(optional: value))
        }
    }

    open class var outboundDeliveryOrderItemUUID: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.outboundDeliveryOrderItemUUID_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.outboundDeliveryOrderItemUUID_ = value
            }
        }
    }

    open var outboundDeliveryOrderItemUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: OUTBDLVItem.outboundDeliveryOrderItemUUID))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.outboundDeliveryOrderItemUUID, to: value)
        }
    }

    open class var outboundDeliveryUUID: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.outboundDeliveryUUID_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.outboundDeliveryUUID_ = value
            }
        }
    }

    open var outboundDeliveryUUID: GuidValue? {
        get {
            return GuidValue.castOptional(self.optionalValue(for: OUTBDLVItem.outboundDeliveryUUID))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.outboundDeliveryUUID, to: value)
        }
    }

    open class var packingStatus: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.packingStatus_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.packingStatus_ = value
            }
        }
    }

    open var packingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.packingStatus))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.packingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var packingStatusName: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.packingStatusName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.packingStatusName_ = value
            }
        }
    }

    open var packingStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.packingStatusName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.packingStatusName, to: StringValue.of(optional: value))
        }
    }

    open class var plannedPickingStatus: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.plannedPickingStatus_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.plannedPickingStatus_ = value
            }
        }
    }

    open var plannedPickingStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.plannedPickingStatus))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.plannedPickingStatus, to: StringValue.of(optional: value))
        }
    }

    open class var plannedPickingStatusName: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.plannedPickingStatusName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.plannedPickingStatusName_ = value
            }
        }
    }

    open var plannedPickingStatusName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.plannedPickingStatusName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.plannedPickingStatusName, to: StringValue.of(optional: value))
        }
    }

    open class var product: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.product_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.product_ = value
            }
        }
    }

    open var product: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.product))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.product, to: StringValue.of(optional: value))
        }
    }

    open class var productName: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.productName_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.productName_ = value
            }
        }
    }

    open var productName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.productName))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.productName, to: StringValue.of(optional: value))
        }
    }

    open class var psa: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.psa_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.psa_ = value
            }
        }
    }

    open var psa: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.psa))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.psa, to: StringValue.of(optional: value))
        }
    }

    open class var sledBbd: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.sledBbd_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.sledBbd_ = value
            }
        }
    }

    open var sledBbd: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: OUTBDLVItem.sledBbd))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.sledBbd, to: value)
        }
    }

    open class var stockDocument: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.stockDocument_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.stockDocument_ = value
            }
        }
    }

    open var stockDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.stockDocument))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.stockDocument, to: StringValue.of(optional: value))
        }
    }

    open class var stockDocumentType: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.stockDocumentType_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.stockDocumentType_ = value
            }
        }
    }

    open var stockDocumentType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.stockDocumentType))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.stockDocumentType, to: StringValue.of(optional: value))
        }
    }

    open class var stockDocumentTypeText: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.stockDocumentTypeText_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.stockDocumentTypeText_ = value
            }
        }
    }

    open var stockDocumentTypeText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.stockDocumentTypeText))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.stockDocumentTypeText, to: StringValue.of(optional: value))
        }
    }

    open class var stockItem: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.stockItem_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.stockItem_ = value
            }
        }
    }

    open var stockItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.stockItem))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.stockItem, to: StringValue.of(optional: value))
        }
    }

    open class var uxFcAAdjustDeliveryQuantities: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.uxFcAAdjustDeliveryQuantities_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.uxFcAAdjustDeliveryQuantities_ = value
            }
        }
    }

    open var uxFcAAdjustDeliveryQuantities: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: OUTBDLVItem.uxFcAAdjustDeliveryQuantities))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.uxFcAAdjustDeliveryQuantities, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcACreateTasks: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.uxFcACreateTasks_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.uxFcACreateTasks_ = value
            }
        }
    }

    open var uxFcACreateTasks: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: OUTBDLVItem.uxFcACreateTasks))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.uxFcACreateTasks, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcADeletable: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.uxFcADeletable_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.uxFcADeletable_ = value
            }
        }
    }

    open var uxFcADeletable: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: OUTBDLVItem.uxFcADeletable))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.uxFcADeletable, to: BooleanValue.of(optional: value))
        }
    }

    open class var uxFcAUpdatable: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.uxFcAUpdatable_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.uxFcAUpdatable_ = value
            }
        }
    }

    open var uxFcAUpdatable: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: OUTBDLVItem.uxFcAUpdatable))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.uxFcAUpdatable, to: BooleanValue.of(optional: value))
        }
    }

    open class var warehouseNumber: Property {
        get {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                return OUTBDLVItem.warehouseNumber_
            }
        }
        set(value) {
            objc_sync_enter(OUTBDLVItem.self)
            defer { objc_sync_exit(OUTBDLVItem.self) }
            do {
                OUTBDLVItem.warehouseNumber_ = value
            }
        }
    }

    open var warehouseNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: OUTBDLVItem.warehouseNumber))
        }
        set(value) {
            self.setOptionalValue(for: OUTBDLVItem.warehouseNumber, to: StringValue.of(optional: value))
        }
    }
}
