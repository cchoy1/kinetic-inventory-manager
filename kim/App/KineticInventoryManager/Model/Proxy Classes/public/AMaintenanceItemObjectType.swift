// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AMaintenanceItemObjectType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var assembly_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "Assembly")

    private static var equipment_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "Equipment")

    private static var functionalLocation_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "FunctionalLocation")

    private static var maintenanceItemObject_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "MaintenanceItemObject")

    private static var maintenanceItemObjectList_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "MaintenanceItemObjectList")

    private static var maintenanceNotification_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "MaintenanceNotification")

    private static var maintObjectLocAcctAssgmtNmbr_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "MaintObjectLocAcctAssgmtNmbr")

    private static var material_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "Material")

    private static var serialNumber_: Property = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "SerialNumber")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aMaintenanceItemObjectType)
    }

    open class func array(from: EntityValueList) -> [AMaintenanceItemObjectType] {
        return ArrayConverter.convert(from.toArray(), [AMaintenanceItemObjectType]())
    }

    open class var assembly: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                return AMaintenanceItemObjectType.assembly_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                AMaintenanceItemObjectType.assembly_ = value
            }
        }
    }

    open var assembly: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjectType.assembly))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjectType.assembly, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> AMaintenanceItemObjectType {
        return CastRequired<AMaintenanceItemObjectType>.from(self.copyEntity())
    }

    open class var equipment: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                return AMaintenanceItemObjectType.equipment_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                AMaintenanceItemObjectType.equipment_ = value
            }
        }
    }

    open var equipment: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjectType.equipment))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjectType.equipment, to: StringValue.of(optional: value))
        }
    }

    open class var functionalLocation: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                return AMaintenanceItemObjectType.functionalLocation_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                AMaintenanceItemObjectType.functionalLocation_ = value
            }
        }
    }

    open var functionalLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjectType.functionalLocation))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjectType.functionalLocation, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(maintenanceItemObject: Int?, maintenanceItemObjectList: Int64?) -> EntityKey {
        return EntityKey().with(name: "MaintenanceItemObject", value: IntValue.of(optional: maintenanceItemObject)).with(name: "MaintenanceItemObjectList", value: LongValue.of(optional: maintenanceItemObjectList))
    }

    open class var maintObjectLocAcctAssgmtNmbr: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                return AMaintenanceItemObjectType.maintObjectLocAcctAssgmtNmbr_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                AMaintenanceItemObjectType.maintObjectLocAcctAssgmtNmbr_ = value
            }
        }
    }

    open var maintObjectLocAcctAssgmtNmbr: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjectType.maintObjectLocAcctAssgmtNmbr))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjectType.maintObjectLocAcctAssgmtNmbr, to: StringValue.of(optional: value))
        }
    }

    open class var maintenanceItemObject: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                return AMaintenanceItemObjectType.maintenanceItemObject_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                AMaintenanceItemObjectType.maintenanceItemObject_ = value
            }
        }
    }

    open var maintenanceItemObject: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: AMaintenanceItemObjectType.maintenanceItemObject))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjectType.maintenanceItemObject, to: IntValue.of(optional: value))
        }
    }

    open class var maintenanceItemObjectList: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                return AMaintenanceItemObjectType.maintenanceItemObjectList_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                AMaintenanceItemObjectType.maintenanceItemObjectList_ = value
            }
        }
    }

    open var maintenanceItemObjectList: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: AMaintenanceItemObjectType.maintenanceItemObjectList))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjectType.maintenanceItemObjectList, to: LongValue.of(optional: value))
        }
    }

    open class var maintenanceNotification: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                return AMaintenanceItemObjectType.maintenanceNotification_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                AMaintenanceItemObjectType.maintenanceNotification_ = value
            }
        }
    }

    open var maintenanceNotification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjectType.maintenanceNotification))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjectType.maintenanceNotification, to: StringValue.of(optional: value))
        }
    }

    open class var material: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                return AMaintenanceItemObjectType.material_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                AMaintenanceItemObjectType.material_ = value
            }
        }
    }

    open var material: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjectType.material))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjectType.material, to: StringValue.of(optional: value))
        }
    }

    open var old: AMaintenanceItemObjectType {
        return CastRequired<AMaintenanceItemObjectType>.from(self.oldEntity)
    }

    open class var serialNumber: Property {
        get {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                return AMaintenanceItemObjectType.serialNumber_
            }
        }
        set(value) {
            objc_sync_enter(AMaintenanceItemObjectType.self)
            defer { objc_sync_exit(AMaintenanceItemObjectType.self) }
            do {
                AMaintenanceItemObjectType.serialNumber_ = value
            }
        }
    }

    open var serialNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AMaintenanceItemObjectType.serialNumber))
        }
        set(value) {
            self.setOptionalValue(for: AMaintenanceItemObjectType.serialNumber, to: StringValue.of(optional: value))
        }
    }
}
