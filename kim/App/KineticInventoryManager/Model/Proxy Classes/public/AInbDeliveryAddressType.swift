// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AInbDeliveryAddressType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var addressID_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "AddressID")

    private static var additionalStreetPrefixName_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "AdditionalStreetPrefixName")

    private static var additionalStreetSuffixName_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "AdditionalStreetSuffixName")

    private static var addressTimeZone_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "AddressTimeZone")

    private static var building_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Building")

    private static var businessPartnerName1_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "BusinessPartnerName1")

    private static var businessPartnerName2_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "BusinessPartnerName2")

    private static var businessPartnerName3_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "BusinessPartnerName3")

    private static var businessPartnerName4_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "BusinessPartnerName4")

    private static var careOfName_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CareOfName")

    private static var cityCode_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CityCode")

    private static var cityName_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CityName")

    private static var citySearch_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CitySearch")

    private static var companyPostalCode_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CompanyPostalCode")

    private static var correspondenceLanguage_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CorrespondenceLanguage")

    private static var country_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Country")

    private static var county_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "County")

    private static var deliveryServiceNumber_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "DeliveryServiceNumber")

    private static var deliveryServiceTypeCode_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "DeliveryServiceTypeCode")

    private static var district_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "District")

    private static var faxNumber_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "FaxNumber")

    private static var floor_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Floor")

    private static var formOfAddress_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "FormOfAddress")

    private static var fullName_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "FullName")

    private static var homeCityName_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "HomeCityName")

    private static var houseNumber_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "HouseNumber")

    private static var houseNumberSupplementText_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "HouseNumberSupplementText")

    private static var nation_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Nation")

    private static var person_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Person")

    private static var phoneNumber_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "PhoneNumber")

    private static var poBox_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBox")

    private static var poBoxDeviatingCityName_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxDeviatingCityName")

    private static var poBoxDeviatingCountry_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxDeviatingCountry")

    private static var poBoxDeviatingRegion_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxDeviatingRegion")

    private static var poBoxIsWithoutNumber_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxIsWithoutNumber")

    private static var poBoxLobbyName_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxLobbyName")

    private static var poBoxPostalCode_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxPostalCode")

    private static var postalCode_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "PostalCode")

    private static var prfrdCommMediumType_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "PrfrdCommMediumType")

    private static var region_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Region")

    private static var roomNumber_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "RoomNumber")

    private static var searchTerm1_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "SearchTerm1")

    private static var streetName_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "StreetName")

    private static var streetPrefixName_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "StreetPrefixName")

    private static var streetSearch_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "StreetSearch")

    private static var streetSuffixName_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "StreetSuffixName")

    private static var taxJurisdiction_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "TaxJurisdiction")

    private static var transportZone_: Property = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "TransportZone")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aInbDeliveryAddressType)
    }

    open class var additionalStreetPrefixName: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.additionalStreetPrefixName_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.additionalStreetPrefixName_ = value
            }
        }
    }

    open var additionalStreetPrefixName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.additionalStreetPrefixName))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.additionalStreetPrefixName, to: StringValue.of(optional: value))
        }
    }

    open class var additionalStreetSuffixName: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.additionalStreetSuffixName_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.additionalStreetSuffixName_ = value
            }
        }
    }

    open var additionalStreetSuffixName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.additionalStreetSuffixName))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.additionalStreetSuffixName, to: StringValue.of(optional: value))
        }
    }

    open class var addressID: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.addressID_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.addressID_ = value
            }
        }
    }

    open var addressID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.addressID))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.addressID, to: StringValue.of(optional: value))
        }
    }

    open class var addressTimeZone: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.addressTimeZone_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.addressTimeZone_ = value
            }
        }
    }

    open var addressTimeZone: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.addressTimeZone))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.addressTimeZone, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [AInbDeliveryAddressType] {
        return ArrayConverter.convert(from.toArray(), [AInbDeliveryAddressType]())
    }

    open class var building: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.building_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.building_ = value
            }
        }
    }

    open var building: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.building))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.building, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerName1: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.businessPartnerName1_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.businessPartnerName1_ = value
            }
        }
    }

    open var businessPartnerName1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.businessPartnerName1))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.businessPartnerName1, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerName2: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.businessPartnerName2_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.businessPartnerName2_ = value
            }
        }
    }

    open var businessPartnerName2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.businessPartnerName2))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.businessPartnerName2, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerName3: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.businessPartnerName3_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.businessPartnerName3_ = value
            }
        }
    }

    open var businessPartnerName3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.businessPartnerName3))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.businessPartnerName3, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerName4: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.businessPartnerName4_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.businessPartnerName4_ = value
            }
        }
    }

    open var businessPartnerName4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.businessPartnerName4))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.businessPartnerName4, to: StringValue.of(optional: value))
        }
    }

    open class var careOfName: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.careOfName_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.careOfName_ = value
            }
        }
    }

    open var careOfName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.careOfName))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.careOfName, to: StringValue.of(optional: value))
        }
    }

    open class var cityCode: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.cityCode_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.cityCode_ = value
            }
        }
    }

    open var cityCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.cityCode))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.cityCode, to: StringValue.of(optional: value))
        }
    }

    open class var cityName: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.cityName_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.cityName_ = value
            }
        }
    }

    open var cityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.cityName))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.cityName, to: StringValue.of(optional: value))
        }
    }

    open class var citySearch: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.citySearch_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.citySearch_ = value
            }
        }
    }

    open var citySearch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.citySearch))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.citySearch, to: StringValue.of(optional: value))
        }
    }

    open class var companyPostalCode: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.companyPostalCode_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.companyPostalCode_ = value
            }
        }
    }

    open var companyPostalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.companyPostalCode))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.companyPostalCode, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> AInbDeliveryAddressType {
        return CastRequired<AInbDeliveryAddressType>.from(self.copyEntity())
    }

    open class var correspondenceLanguage: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.correspondenceLanguage_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.correspondenceLanguage_ = value
            }
        }
    }

    open var correspondenceLanguage: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.correspondenceLanguage))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.correspondenceLanguage, to: StringValue.of(optional: value))
        }
    }

    open class var country: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.country_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.country_ = value
            }
        }
    }

    open var country: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.country))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.country, to: StringValue.of(optional: value))
        }
    }

    open class var county: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.county_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.county_ = value
            }
        }
    }

    open var county: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.county))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.county, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryServiceNumber: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.deliveryServiceNumber_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.deliveryServiceNumber_ = value
            }
        }
    }

    open var deliveryServiceNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.deliveryServiceNumber))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.deliveryServiceNumber, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryServiceTypeCode: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.deliveryServiceTypeCode_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.deliveryServiceTypeCode_ = value
            }
        }
    }

    open var deliveryServiceTypeCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.deliveryServiceTypeCode))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.deliveryServiceTypeCode, to: StringValue.of(optional: value))
        }
    }

    open class var district: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.district_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.district_ = value
            }
        }
    }

    open var district: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.district))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.district, to: StringValue.of(optional: value))
        }
    }

    open class var faxNumber: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.faxNumber_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.faxNumber_ = value
            }
        }
    }

    open var faxNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.faxNumber))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.faxNumber, to: StringValue.of(optional: value))
        }
    }

    open class var floor: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.floor_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.floor_ = value
            }
        }
    }

    open var floor: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.floor))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.floor, to: StringValue.of(optional: value))
        }
    }

    open class var formOfAddress: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.formOfAddress_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.formOfAddress_ = value
            }
        }
    }

    open var formOfAddress: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.formOfAddress))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.formOfAddress, to: StringValue.of(optional: value))
        }
    }

    open class var fullName: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.fullName_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.fullName_ = value
            }
        }
    }

    open var fullName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.fullName))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.fullName, to: StringValue.of(optional: value))
        }
    }

    open class var homeCityName: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.homeCityName_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.homeCityName_ = value
            }
        }
    }

    open var homeCityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.homeCityName))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.homeCityName, to: StringValue.of(optional: value))
        }
    }

    open class var houseNumber: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.houseNumber_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.houseNumber_ = value
            }
        }
    }

    open var houseNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.houseNumber))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.houseNumber, to: StringValue.of(optional: value))
        }
    }

    open class var houseNumberSupplementText: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.houseNumberSupplementText_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.houseNumberSupplementText_ = value
            }
        }
    }

    open var houseNumberSupplementText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.houseNumberSupplementText))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.houseNumberSupplementText, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(addressID: String?) -> EntityKey {
        return EntityKey().with(name: "AddressID", value: StringValue.of(optional: addressID))
    }

    open class var nation: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.nation_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.nation_ = value
            }
        }
    }

    open var nation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.nation))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.nation, to: StringValue.of(optional: value))
        }
    }

    open var old: AInbDeliveryAddressType {
        return CastRequired<AInbDeliveryAddressType>.from(self.oldEntity)
    }

    open class var person: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.person_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.person_ = value
            }
        }
    }

    open var person: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.person))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.person, to: StringValue.of(optional: value))
        }
    }

    open class var phoneNumber: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.phoneNumber_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.phoneNumber_ = value
            }
        }
    }

    open var phoneNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.phoneNumber))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.phoneNumber, to: StringValue.of(optional: value))
        }
    }

    open class var poBox: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.poBox_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.poBox_ = value
            }
        }
    }

    open var poBox: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.poBox))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.poBox, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxDeviatingCityName: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.poBoxDeviatingCityName_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.poBoxDeviatingCityName_ = value
            }
        }
    }

    open var poBoxDeviatingCityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.poBoxDeviatingCityName))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.poBoxDeviatingCityName, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxDeviatingCountry: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.poBoxDeviatingCountry_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.poBoxDeviatingCountry_ = value
            }
        }
    }

    open var poBoxDeviatingCountry: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.poBoxDeviatingCountry))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.poBoxDeviatingCountry, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxDeviatingRegion: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.poBoxDeviatingRegion_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.poBoxDeviatingRegion_ = value
            }
        }
    }

    open var poBoxDeviatingRegion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.poBoxDeviatingRegion))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.poBoxDeviatingRegion, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxIsWithoutNumber: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.poBoxIsWithoutNumber_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.poBoxIsWithoutNumber_ = value
            }
        }
    }

    open var poBoxIsWithoutNumber: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: AInbDeliveryAddressType.poBoxIsWithoutNumber))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.poBoxIsWithoutNumber, to: BooleanValue.of(optional: value))
        }
    }

    open class var poBoxLobbyName: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.poBoxLobbyName_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.poBoxLobbyName_ = value
            }
        }
    }

    open var poBoxLobbyName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.poBoxLobbyName))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.poBoxLobbyName, to: StringValue.of(optional: value))
        }
    }

    open class var poBoxPostalCode: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.poBoxPostalCode_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.poBoxPostalCode_ = value
            }
        }
    }

    open var poBoxPostalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.poBoxPostalCode))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.poBoxPostalCode, to: StringValue.of(optional: value))
        }
    }

    open class var postalCode: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.postalCode_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.postalCode_ = value
            }
        }
    }

    open var postalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.postalCode))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.postalCode, to: StringValue.of(optional: value))
        }
    }

    open class var prfrdCommMediumType: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.prfrdCommMediumType_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.prfrdCommMediumType_ = value
            }
        }
    }

    open var prfrdCommMediumType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.prfrdCommMediumType))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.prfrdCommMediumType, to: StringValue.of(optional: value))
        }
    }

    open class var region: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.region_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.region_ = value
            }
        }
    }

    open var region: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.region))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.region, to: StringValue.of(optional: value))
        }
    }

    open class var roomNumber: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.roomNumber_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.roomNumber_ = value
            }
        }
    }

    open var roomNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.roomNumber))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.roomNumber, to: StringValue.of(optional: value))
        }
    }

    open class var searchTerm1: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.searchTerm1_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.searchTerm1_ = value
            }
        }
    }

    open var searchTerm1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.searchTerm1))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.searchTerm1, to: StringValue.of(optional: value))
        }
    }

    open class var streetName: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.streetName_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.streetName_ = value
            }
        }
    }

    open var streetName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.streetName))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.streetName, to: StringValue.of(optional: value))
        }
    }

    open class var streetPrefixName: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.streetPrefixName_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.streetPrefixName_ = value
            }
        }
    }

    open var streetPrefixName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.streetPrefixName))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.streetPrefixName, to: StringValue.of(optional: value))
        }
    }

    open class var streetSearch: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.streetSearch_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.streetSearch_ = value
            }
        }
    }

    open var streetSearch: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.streetSearch))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.streetSearch, to: StringValue.of(optional: value))
        }
    }

    open class var streetSuffixName: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.streetSuffixName_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.streetSuffixName_ = value
            }
        }
    }

    open var streetSuffixName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.streetSuffixName))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.streetSuffixName, to: StringValue.of(optional: value))
        }
    }

    open class var taxJurisdiction: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.taxJurisdiction_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.taxJurisdiction_ = value
            }
        }
    }

    open var taxJurisdiction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.taxJurisdiction))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.taxJurisdiction, to: StringValue.of(optional: value))
        }
    }

    open class var transportZone: Property {
        get {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                return AInbDeliveryAddressType.transportZone_
            }
        }
        set(value) {
            objc_sync_enter(AInbDeliveryAddressType.self)
            defer { objc_sync_exit(AInbDeliveryAddressType.self) }
            do {
                AInbDeliveryAddressType.transportZone_ = value
            }
        }
    }

    open var transportZone: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AInbDeliveryAddressType.transportZone))
        }
        set(value) {
            self.setOptionalValue(for: AInbDeliveryAddressType.transportZone, to: StringValue.of(optional: value))
        }
    }
}
