// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class AOutbDeliveryItemTextType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var deliveryDocument_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.property(withName: "DeliveryDocument")

    private static var deliveryDocumentItem_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.property(withName: "DeliveryDocumentItem")

    private static var textElement_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.property(withName: "TextElement")

    private static var language_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.property(withName: "Language")

    private static var textElementDescription_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.property(withName: "TextElementDescription")

    private static var textElementText_: Property = Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.property(withName: "TextElementText")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType)
    }

    open class func array(from: EntityValueList) -> [AOutbDeliveryItemTextType] {
        return ArrayConverter.convert(from.toArray(), [AOutbDeliveryItemTextType]())
    }

    open func copy() -> AOutbDeliveryItemTextType {
        return CastRequired<AOutbDeliveryItemTextType>.from(self.copyEntity())
    }

    open class var deliveryDocument: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemTextType.self)
            defer { objc_sync_exit(AOutbDeliveryItemTextType.self) }
            do {
                return AOutbDeliveryItemTextType.deliveryDocument_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemTextType.self)
            defer { objc_sync_exit(AOutbDeliveryItemTextType.self) }
            do {
                AOutbDeliveryItemTextType.deliveryDocument_ = value
            }
        }
    }

    open var deliveryDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemTextType.deliveryDocument))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemTextType.deliveryDocument, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryDocumentItem: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemTextType.self)
            defer { objc_sync_exit(AOutbDeliveryItemTextType.self) }
            do {
                return AOutbDeliveryItemTextType.deliveryDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemTextType.self)
            defer { objc_sync_exit(AOutbDeliveryItemTextType.self) }
            do {
                AOutbDeliveryItemTextType.deliveryDocumentItem_ = value
            }
        }
    }

    open var deliveryDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemTextType.deliveryDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemTextType.deliveryDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(deliveryDocument: String?, deliveryDocumentItem: String?, textElement: String?, language: String?) -> EntityKey {
        return EntityKey().with(name: "DeliveryDocument", value: StringValue.of(optional: deliveryDocument)).with(name: "DeliveryDocumentItem", value: StringValue.of(optional: deliveryDocumentItem)).with(name: "TextElement", value: StringValue.of(optional: textElement)).with(name: "Language", value: StringValue.of(optional: language))
    }

    open class var language: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemTextType.self)
            defer { objc_sync_exit(AOutbDeliveryItemTextType.self) }
            do {
                return AOutbDeliveryItemTextType.language_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemTextType.self)
            defer { objc_sync_exit(AOutbDeliveryItemTextType.self) }
            do {
                AOutbDeliveryItemTextType.language_ = value
            }
        }
    }

    open var language: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemTextType.language))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemTextType.language, to: StringValue.of(optional: value))
        }
    }

    open var old: AOutbDeliveryItemTextType {
        return CastRequired<AOutbDeliveryItemTextType>.from(self.oldEntity)
    }

    open class var textElement: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemTextType.self)
            defer { objc_sync_exit(AOutbDeliveryItemTextType.self) }
            do {
                return AOutbDeliveryItemTextType.textElement_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemTextType.self)
            defer { objc_sync_exit(AOutbDeliveryItemTextType.self) }
            do {
                AOutbDeliveryItemTextType.textElement_ = value
            }
        }
    }

    open var textElement: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemTextType.textElement))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemTextType.textElement, to: StringValue.of(optional: value))
        }
    }

    open class var textElementDescription: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemTextType.self)
            defer { objc_sync_exit(AOutbDeliveryItemTextType.self) }
            do {
                return AOutbDeliveryItemTextType.textElementDescription_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemTextType.self)
            defer { objc_sync_exit(AOutbDeliveryItemTextType.self) }
            do {
                AOutbDeliveryItemTextType.textElementDescription_ = value
            }
        }
    }

    open var textElementDescription: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemTextType.textElementDescription))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemTextType.textElementDescription, to: StringValue.of(optional: value))
        }
    }

    open class var textElementText: Property {
        get {
            objc_sync_enter(AOutbDeliveryItemTextType.self)
            defer { objc_sync_exit(AOutbDeliveryItemTextType.self) }
            do {
                return AOutbDeliveryItemTextType.textElementText_
            }
        }
        set(value) {
            objc_sync_enter(AOutbDeliveryItemTextType.self)
            defer { objc_sync_exit(AOutbDeliveryItemTextType.self) }
            do {
                AOutbDeliveryItemTextType.textElementText_ = value
            }
        }
    }

    open var textElementText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AOutbDeliveryItemTextType.textElementText))
        }
        set(value) {
            self.setOptionalValue(for: AOutbDeliveryItemTextType.textElementText, to: StringValue.of(optional: value))
        }
    }
}
