// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

open class Hu: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var huID_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "HuId")

    private static var bin_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "Bin")

    private static var lgnum_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "Lgnum")

    private static var workstation_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "Workstation")

    private static var packmat_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "Packmat")

    private static var docno_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "Docno")

    private static var isPickHu_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "IsPickHu")

    private static var totalWeight_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "TotalWeight")

    private static var weightUoM_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "WeightUoM")

    private static var loadingWeight_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "LoadingWeight")

    private static var type__: Property = Ec1Metadata.EntityTypes.hu.property(withName: "Type")

    private static var closed_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "Closed")

    private static var msgID_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "MsgId")

    private static var msgVar_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "MsgVar")

    private static var msgKey_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "MsgKey")

    private static var msgType_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "MsgType")

    private static var msgSuccess_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "MsgSuccess")

    private static var items_: Property = Ec1Metadata.EntityTypes.hu.property(withName: "Items")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: Ec1Metadata.EntityTypes.hu)
    }

    open class func array(from: EntityValueList) -> [Hu] {
        return ArrayConverter.convert(from.toArray(), [Hu]())
    }

    open class var bin: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.bin_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.bin_ = value
            }
        }
    }

    open var bin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Hu.bin))
        }
        set(value) {
            self.setOptionalValue(for: Hu.bin, to: StringValue.of(optional: value))
        }
    }

    open class var closed: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.closed_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.closed_ = value
            }
        }
    }

    open var closed: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: Hu.closed))
        }
        set(value) {
            self.setOptionalValue(for: Hu.closed, to: BooleanValue.of(optional: value))
        }
    }

    open func copy() -> Hu {
        return CastRequired<Hu>.from(self.copyEntity())
    }

    open class var docno: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.docno_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.docno_ = value
            }
        }
    }

    open var docno: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Hu.docno))
        }
        set(value) {
            self.setOptionalValue(for: Hu.docno, to: StringValue.of(optional: value))
        }
    }

    open class var huID: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.huID_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.huID_ = value
            }
        }
    }

    open var huID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Hu.huID))
        }
        set(value) {
            self.setOptionalValue(for: Hu.huID, to: StringValue.of(optional: value))
        }
    }

    open class var isPickHu: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.isPickHu_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.isPickHu_ = value
            }
        }
    }

    open var isPickHu: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: Hu.isPickHu))
        }
        set(value) {
            self.setOptionalValue(for: Hu.isPickHu, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var items: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.items_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.items_ = value
            }
        }
    }

    open var items: [Item] {
        get {
            return ArrayConverter.convert(Hu.items.entityList(from: self).toArray(), [Item]())
        }
        set(value) {
            Hu.items.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class func key(huID: String?, bin: String?, lgnum: String?, workstation: String?, type_: String?) -> EntityKey {
        return EntityKey().with(name: "HuId", value: StringValue.of(optional: huID)).with(name: "Bin", value: StringValue.of(optional: bin)).with(name: "Lgnum", value: StringValue.of(optional: lgnum)).with(name: "Workstation", value: StringValue.of(optional: workstation)).with(name: "Type", value: StringValue.of(optional: type_))
    }

    open class var lgnum: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.lgnum_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.lgnum_ = value
            }
        }
    }

    open var lgnum: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Hu.lgnum))
        }
        set(value) {
            self.setOptionalValue(for: Hu.lgnum, to: StringValue.of(optional: value))
        }
    }

    open class var loadingWeight: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.loadingWeight_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.loadingWeight_ = value
            }
        }
    }

    open var loadingWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: Hu.loadingWeight))
        }
        set(value) {
            self.setOptionalValue(for: Hu.loadingWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var msgID: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.msgID_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.msgID_ = value
            }
        }
    }

    open var msgID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Hu.msgID))
        }
        set(value) {
            self.setOptionalValue(for: Hu.msgID, to: StringValue.of(optional: value))
        }
    }

    open class var msgKey: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.msgKey_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.msgKey_ = value
            }
        }
    }

    open var msgKey: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Hu.msgKey))
        }
        set(value) {
            self.setOptionalValue(for: Hu.msgKey, to: StringValue.of(optional: value))
        }
    }

    open class var msgSuccess: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.msgSuccess_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.msgSuccess_ = value
            }
        }
    }

    open var msgSuccess: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: Hu.msgSuccess))
        }
        set(value) {
            self.setOptionalValue(for: Hu.msgSuccess, to: BooleanValue.of(optional: value))
        }
    }

    open class var msgType: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.msgType_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.msgType_ = value
            }
        }
    }

    open var msgType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Hu.msgType))
        }
        set(value) {
            self.setOptionalValue(for: Hu.msgType, to: StringValue.of(optional: value))
        }
    }

    open class var msgVar: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.msgVar_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.msgVar_ = value
            }
        }
    }

    open var msgVar: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Hu.msgVar))
        }
        set(value) {
            self.setOptionalValue(for: Hu.msgVar, to: StringValue.of(optional: value))
        }
    }

    open var old: Hu {
        return CastRequired<Hu>.from(self.oldEntity)
    }

    open class var packmat: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.packmat_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.packmat_ = value
            }
        }
    }

    open var packmat: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Hu.packmat))
        }
        set(value) {
            self.setOptionalValue(for: Hu.packmat, to: StringValue.of(optional: value))
        }
    }

    open class var totalWeight: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.totalWeight_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.totalWeight_ = value
            }
        }
    }

    open var totalWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: Hu.totalWeight))
        }
        set(value) {
            self.setOptionalValue(for: Hu.totalWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var type_: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.type__
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.type__ = value
            }
        }
    }

    open var type_: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Hu.type_))
        }
        set(value) {
            self.setOptionalValue(for: Hu.type_, to: StringValue.of(optional: value))
        }
    }

    open class var weightUoM: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.weightUoM_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.weightUoM_ = value
            }
        }
    }

    open var weightUoM: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Hu.weightUoM))
        }
        set(value) {
            self.setOptionalValue(for: Hu.weightUoM, to: StringValue.of(optional: value))
        }
    }

    open class var workstation: Property {
        get {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                return Hu.workstation_
            }
        }
        set(value) {
            objc_sync_enter(Hu.self)
            defer { objc_sync_exit(Hu.self) }
            do {
                Hu.workstation_ = value
            }
        }
    }

    open var workstation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Hu.workstation))
        }
        set(value) {
            self.setOptionalValue(for: Hu.workstation, to: StringValue.of(optional: value))
        }
    }
}
