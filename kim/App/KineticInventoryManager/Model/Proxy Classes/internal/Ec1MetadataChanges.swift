// # Proxy Compiler 20.1.0-110dca-20191219

import Foundation
import SAPOData

internal class Ec1MetadataChanges {
    static func merge(metadata: CSDLDocument) {
        metadata.hasGeneratedProxies = true
        Ec1Metadata.document = metadata
        Ec1MetadataChanges.merge1(metadata: metadata)
        Ec1MetadataChanges.merge2(metadata: metadata)
        Ec1MetadataChanges.merge3(metadata: metadata)
        Ec1MetadataChanges.merge4(metadata: metadata)
        try! Ec1Factory.registerAll()
    }

    private static func merge1(metadata: CSDLDocument) {
        Ignore.valueOf_any(metadata)
        if !Ec1Metadata.ComplexTypes.item2.isRemoved {
            Ec1Metadata.ComplexTypes.item2 = metadata.complexType(withName: "S1.Item2")
        }
        if !Ec1Metadata.ComplexTypes.pickingReport.isRemoved {
            Ec1Metadata.ComplexTypes.pickingReport = metadata.complexType(withName: "S1.PickingReport")
        }
        if !Ec1Metadata.ComplexTypes.product.isRemoved {
            Ec1Metadata.ComplexTypes.product = metadata.complexType(withName: "S1.Product")
        }
        if !Ec1Metadata.ComplexTypes.quantity.isRemoved {
            Ec1Metadata.ComplexTypes.quantity = metadata.complexType(withName: "S1.Quantity")
        }
        if !Ec1Metadata.ComplexTypes.quantity2.isRemoved {
            Ec1Metadata.ComplexTypes.quantity2 = metadata.complexType(withName: "S1.Quantity2")
        }
        if !Ec1Metadata.ComplexTypes.responsiblePurchasingGroupParty.isRemoved {
            Ec1Metadata.ComplexTypes.responsiblePurchasingGroupParty = metadata.complexType(withName: "S1.ResponsiblePurchasingGroupParty")
        }
        if !Ec1Metadata.ComplexTypes.responsiblePurchasingOrganisationParty.isRemoved {
            Ec1Metadata.ComplexTypes.responsiblePurchasingOrganisationParty = metadata.complexType(withName: "S1.ResponsiblePurchasingOrganisationParty")
        }
        if !Ec1Metadata.ComplexTypes.return.isRemoved {
            Ec1Metadata.ComplexTypes.return = metadata.complexType(withName: "S1.Return")
        }
        if !Ec1Metadata.ComplexTypes.returnActionsType.isRemoved {
            Ec1Metadata.ComplexTypes.returnActionsType = metadata.complexType(withName: "S1.ReturnActionsType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryAddressType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryAddressType = metadata.entityType(withName: "S1.A_InbDeliveryAddressType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType = metadata.entityType(withName: "S1.A_InbDeliveryDocFlowType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType = metadata.entityType(withName: "S1.A_InbDeliveryHeaderTextType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryHeaderType = metadata.entityType(withName: "S1.A_InbDeliveryHeaderType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryItemTextType = metadata.entityType(withName: "S1.A_InbDeliveryItemTextType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryItemType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryItemType = metadata.entityType(withName: "S1.A_InbDeliveryItemType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliveryPartnerType = metadata.entityType(withName: "S1.A_InbDeliveryPartnerType")
        }
        if !Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.isRemoved {
            Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType = metadata.entityType(withName: "S1.A_InbDeliverySerialNmbrType")
        }
        if !Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.isRemoved {
            Ec1Metadata.EntityTypes.aMaintenanceItemObjListType = metadata.entityType(withName: "S1.A_MaintenanceItemObjListType")
        }
        if !Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.isRemoved {
            Ec1Metadata.EntityTypes.aMaintenanceItemObjectType = metadata.entityType(withName: "S1.A_MaintenanceItemObjectType")
        }
        if !Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.isRemoved {
            Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType = metadata.entityType(withName: "S1.A_MaterialDocumentHeaderType")
        }
        if !Ec1Metadata.EntityTypes.aMaterialDocumentItemType.isRemoved {
            Ec1Metadata.EntityTypes.aMaterialDocumentItemType = metadata.entityType(withName: "S1.A_MaterialDocumentItemType")
        }
        if !Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.isRemoved {
            Ec1Metadata.EntityTypes.aOutbDeliveryAddressType = metadata.entityType(withName: "S1.A_OutbDeliveryAddressType")
        }
        if !Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.isRemoved {
            Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType = metadata.entityType(withName: "S1.A_OutbDeliveryDocFlowType")
        }
        if !Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType.isRemoved {
            Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType = metadata.entityType(withName: "S1.A_OutbDeliveryHeaderTextType")
        }
        if !Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.isRemoved {
            Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType = metadata.entityType(withName: "S1.A_OutbDeliveryHeaderType")
        }
        if !Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.isRemoved {
            Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType = metadata.entityType(withName: "S1.A_OutbDeliveryItemTextType")
        }
        if !Ec1Metadata.EntityTypes.aOutbDeliveryItemType.isRemoved {
            Ec1Metadata.EntityTypes.aOutbDeliveryItemType = metadata.entityType(withName: "S1.A_OutbDeliveryItemType")
        }
        if !Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.isRemoved {
            Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType = metadata.entityType(withName: "S1.A_OutbDeliveryPartnerType")
        }
        if !Ec1Metadata.EntityTypes.aPlantType.isRemoved {
            Ec1Metadata.EntityTypes.aPlantType = metadata.entityType(withName: "S1.A_PlantType")
        }
        if !Ec1Metadata.EntityTypes.aProductDescriptionType.isRemoved {
            Ec1Metadata.EntityTypes.aProductDescriptionType = metadata.entityType(withName: "S1.A_ProductDescriptionType")
        }
        if !Ec1Metadata.EntityTypes.aProductPlantType.isRemoved {
            Ec1Metadata.EntityTypes.aProductPlantType = metadata.entityType(withName: "S1.A_ProductPlantType")
        }
        if !Ec1Metadata.EntityTypes.aProductType.isRemoved {
            Ec1Metadata.EntityTypes.aProductType = metadata.entityType(withName: "S1.A_ProductType")
        }
        if !Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.isRemoved {
            Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType = metadata.entityType(withName: "S1.A_PurOrdAccountAssignmentType")
        }
        if !Ec1Metadata.EntityTypes.aPurOrdPricingElementType.isRemoved {
            Ec1Metadata.EntityTypes.aPurOrdPricingElementType = metadata.entityType(withName: "S1.A_PurOrdPricingElementType")
        }
        if !Ec1Metadata.EntityTypes.aPurchaseOrderItemType.isRemoved {
            Ec1Metadata.EntityTypes.aPurchaseOrderItemType = metadata.entityType(withName: "S1.A_PurchaseOrderItemType")
        }
        if !Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.isRemoved {
            Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType = metadata.entityType(withName: "S1.A_PurchaseOrderScheduleLineType")
        }
        if !Ec1Metadata.EntityTypes.aPurchaseOrderType.isRemoved {
            Ec1Metadata.EntityTypes.aPurchaseOrderType = metadata.entityType(withName: "S1.A_PurchaseOrderType")
        }
        if !Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.isRemoved {
            Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType = metadata.entityType(withName: "S1.A_SerialNmbrDeliveryType")
        }
        if !Ec1Metadata.EntityTypes.confirmPickType.isRemoved {
            Ec1Metadata.EntityTypes.confirmPickType = metadata.entityType(withName: "S1.ConfirmPickType")
        }
        if !Ec1Metadata.EntityTypes.createOutbDeliveryType.isRemoved {
            Ec1Metadata.EntityTypes.createOutbDeliveryType = metadata.entityType(withName: "S1.CreateOutbDeliveryType")
        }
        if !Ec1Metadata.EntityTypes.createOutbWarehouseTaskType.isRemoved {
            Ec1Metadata.EntityTypes.createOutbWarehouseTaskType = metadata.entityType(withName: "S1.CreateOutbWarehouseTaskType")
        }
        if !Ec1Metadata.EntityTypes.createWarehouseTaskType.isRemoved {
            Ec1Metadata.EntityTypes.createWarehouseTaskType = metadata.entityType(withName: "S1.CreateWarehouseTaskType")
        }
        if !Ec1Metadata.EntityTypes.dlvHead.isRemoved {
            Ec1Metadata.EntityTypes.dlvHead = metadata.entityType(withName: "S1.DLVHead")
        }
        if !Ec1Metadata.EntityTypes.dlvItem.isRemoved {
            Ec1Metadata.EntityTypes.dlvItem = metadata.entityType(withName: "S1.DLVItem")
        }
        if !Ec1Metadata.EntityTypes.goodsIssueType.isRemoved {
            Ec1Metadata.EntityTypes.goodsIssueType = metadata.entityType(withName: "S1.GoodsIssueType")
        }
        if !Ec1Metadata.EntityTypes.hu.isRemoved {
            Ec1Metadata.EntityTypes.hu = metadata.entityType(withName: "S1.HU")
        }
        if !Ec1Metadata.EntityTypes.huHead.isRemoved {
            Ec1Metadata.EntityTypes.huHead = metadata.entityType(withName: "S1.HUHead")
        }
        if !Ec1Metadata.EntityTypes.huItem.isRemoved {
            Ec1Metadata.EntityTypes.huItem = metadata.entityType(withName: "S1.HUItem")
        }
        if !Ec1Metadata.EntityTypes.huSingleItem.isRemoved {
            Ec1Metadata.EntityTypes.huSingleItem = metadata.entityType(withName: "S1.HUSingleItem")
        }
        if !Ec1Metadata.EntityTypes.item.isRemoved {
            Ec1Metadata.EntityTypes.item = metadata.entityType(withName: "S1.Item")
        }
        if !Ec1Metadata.EntityTypes.outbdlvHead.isRemoved {
            Ec1Metadata.EntityTypes.outbdlvHead = metadata.entityType(withName: "S1.OUTBDLVHead")
        }
        if !Ec1Metadata.EntityTypes.outbdlvItem.isRemoved {
            Ec1Metadata.EntityTypes.outbdlvItem = metadata.entityType(withName: "S1.OUTBDLVItem")
        }
        if !Ec1Metadata.EntityTypes.performPackType.isRemoved {
            Ec1Metadata.EntityTypes.performPackType = metadata.entityType(withName: "S1.PerformPackType")
        }
        if !Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.isRemoved {
            Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1 = metadata.entityType(withName: "S1.PurchaseOrderERPCreateRequestConfirmation_In_V1")
        }
        if !Ec1Metadata.EntityTypes.wtSimpleConfirm.isRemoved {
            Ec1Metadata.EntityTypes.wtSimpleConfirm = metadata.entityType(withName: "S1.WTSimpleConfirm")
        }
        if !Ec1Metadata.EntityTypes.warehouseTask.isRemoved {
            Ec1Metadata.EntityTypes.warehouseTask = metadata.entityType(withName: "S1.WarehouseTask")
        }
        if !Ec1Metadata.EntityTypes.warehouseTaskType.isRemoved {
            Ec1Metadata.EntityTypes.warehouseTaskType = metadata.entityType(withName: "S1.WarehouseTaskType")
        }
        if !Ec1Metadata.EntityTypes.warehouseType.isRemoved {
            Ec1Metadata.EntityTypes.warehouseType = metadata.entityType(withName: "S1.WarehouseType")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryAddressTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryAddressTypeSet = metadata.entitySet(withName: "A_InbDeliveryAddressTypeSet")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryDocFlowTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryDocFlowTypeSet = metadata.entitySet(withName: "A_InbDeliveryDocFlowTypeSet")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryHeader.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryHeader = metadata.entitySet(withName: "A_InbDeliveryHeader")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryHeaderTextTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryHeaderTextTypeSet = metadata.entitySet(withName: "A_InbDeliveryHeaderTextTypeSet")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryItem.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryItem = metadata.entitySet(withName: "A_InbDeliveryItem")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryItemTextTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryItemTextTypeSet = metadata.entitySet(withName: "A_InbDeliveryItemTextTypeSet")
        }
        if !Ec1Metadata.EntitySets.aInbDeliveryPartner.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliveryPartner = metadata.entitySet(withName: "A_InbDeliveryPartner")
        }
        if !Ec1Metadata.EntitySets.aInbDeliverySerialNmbrTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aInbDeliverySerialNmbrTypeSet = metadata.entitySet(withName: "A_InbDeliverySerialNmbrTypeSet")
        }
        if !Ec1Metadata.EntitySets.aMaintenanceItemObjListTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aMaintenanceItemObjListTypeSet = metadata.entitySet(withName: "A_MaintenanceItemObjListTypeSet")
        }
        if !Ec1Metadata.EntitySets.aMaintenanceItemObject.isRemoved {
            Ec1Metadata.EntitySets.aMaintenanceItemObject = metadata.entitySet(withName: "A_MaintenanceItemObject")
        }
        if !Ec1Metadata.EntitySets.aMaterialDocumentHeader.isRemoved {
            Ec1Metadata.EntitySets.aMaterialDocumentHeader = metadata.entitySet(withName: "A_MaterialDocumentHeader")
        }
        if !Ec1Metadata.EntitySets.aMaterialDocumentItemTypeSet.isRemoved {
            Ec1Metadata.EntitySets.aMaterialDocumentItemTypeSet = metadata.entitySet(withName: "A_MaterialDocumentItemTypeSet")
        }
        if !Ec1Metadata.EntitySets.aOutbDeliveryAddress.isRemoved {
            Ec1Metadata.EntitySets.aOutbDeliveryAddress = metadata.entitySet(withName: "A_OutbDeliveryAddress")
        }
        if !Ec1Metadata.EntitySets.aOutbDeliveryDocFlow.isRemoved {
            Ec1Metadata.EntitySets.aOutbDeliveryDocFlow = metadata.entitySet(withName: "A_OutbDeliveryDocFlow")
        }
        if !Ec1Metadata.EntitySets.aOutbDeliveryHeader.isRemoved {
            Ec1Metadata.EntitySets.aOutbDeliveryHeader = metadata.entitySet(withName: "A_OutbDeliveryHeader")
        }
        if !Ec1Metadata.EntitySets.aOutbDeliveryHeaderText.isRemoved {
            Ec1Metadata.EntitySets.aOutbDeliveryHeaderText = metadata.entitySet(withName: "A_OutbDeliveryHeaderText")
        }
        if !Ec1Metadata.EntitySets.aOutbDeliveryItem.isRemoved {
            Ec1Metadata.EntitySets.aOutbDeliveryItem = metadata.entitySet(withName: "A_OutbDeliveryItem")
        }
        if !Ec1Metadata.EntitySets.aOutbDeliveryItemText.isRemoved {
            Ec1Metadata.EntitySets.aOutbDeliveryItemText = metadata.entitySet(withName: "A_OutbDeliveryItemText")
        }
        if !Ec1Metadata.EntitySets.aOutbDeliveryPartner.isRemoved {
            Ec1Metadata.EntitySets.aOutbDeliveryPartner = metadata.entitySet(withName: "A_OutbDeliveryPartner")
        }
        if !Ec1Metadata.EntitySets.aPlant.isRemoved {
            Ec1Metadata.EntitySets.aPlant = metadata.entitySet(withName: "A_Plant")
        }
        if !Ec1Metadata.EntitySets.aProduct.isRemoved {
            Ec1Metadata.EntitySets.aProduct = metadata.entitySet(withName: "A_Product")
        }
        if !Ec1Metadata.EntitySets.aProductDescription.isRemoved {
            Ec1Metadata.EntitySets.aProductDescription = metadata.entitySet(withName: "A_ProductDescription")
        }
        if !Ec1Metadata.EntitySets.aProductPlant.isRemoved {
            Ec1Metadata.EntitySets.aProductPlant = metadata.entitySet(withName: "A_ProductPlant")
        }
        if !Ec1Metadata.EntitySets.aPurOrdAccountAssignment.isRemoved {
            Ec1Metadata.EntitySets.aPurOrdAccountAssignment = metadata.entitySet(withName: "A_PurOrdAccountAssignment")
        }
        if !Ec1Metadata.EntitySets.aPurOrdPricingElement.isRemoved {
            Ec1Metadata.EntitySets.aPurOrdPricingElement = metadata.entitySet(withName: "A_PurOrdPricingElement")
        }
        if !Ec1Metadata.EntitySets.aPurchaseOrder.isRemoved {
            Ec1Metadata.EntitySets.aPurchaseOrder = metadata.entitySet(withName: "A_PurchaseOrder")
        }
        if !Ec1Metadata.EntitySets.aPurchaseOrderItem.isRemoved {
            Ec1Metadata.EntitySets.aPurchaseOrderItem = metadata.entitySet(withName: "A_PurchaseOrderItem")
        }
        if !Ec1Metadata.EntitySets.aPurchaseOrderScheduleLine.isRemoved {
            Ec1Metadata.EntitySets.aPurchaseOrderScheduleLine = metadata.entitySet(withName: "A_PurchaseOrderScheduleLine")
        }
        if !Ec1Metadata.EntitySets.aSerialNmbrDelivery.isRemoved {
            Ec1Metadata.EntitySets.aSerialNmbrDelivery = metadata.entitySet(withName: "A_SerialNmbrDelivery")
        }
        if !Ec1Metadata.EntitySets.confirmPick.isRemoved {
            Ec1Metadata.EntitySets.confirmPick = metadata.entitySet(withName: "ConfirmPick")
        }
        if !Ec1Metadata.EntitySets.createOutbDelivery.isRemoved {
            Ec1Metadata.EntitySets.createOutbDelivery = metadata.entitySet(withName: "CreateOutbDelivery")
        }
        if !Ec1Metadata.EntitySets.createOutbWarehouseTask.isRemoved {
            Ec1Metadata.EntitySets.createOutbWarehouseTask = metadata.entitySet(withName: "CreateOutbWarehouseTask")
        }
        if !Ec1Metadata.EntitySets.createWarehouseTask.isRemoved {
            Ec1Metadata.EntitySets.createWarehouseTask = metadata.entitySet(withName: "CreateWarehouseTask")
        }
        if !Ec1Metadata.EntitySets.dlvHeadSet.isRemoved {
            Ec1Metadata.EntitySets.dlvHeadSet = metadata.entitySet(withName: "DLVHeadSet")
        }
        if !Ec1Metadata.EntitySets.dlvItemSet.isRemoved {
            Ec1Metadata.EntitySets.dlvItemSet = metadata.entitySet(withName: "DLVItemSet")
        }
        if !Ec1Metadata.EntitySets.goodsIssue.isRemoved {
            Ec1Metadata.EntitySets.goodsIssue = metadata.entitySet(withName: "GoodsIssue")
        }
        if !Ec1Metadata.EntitySets.huHeadSet.isRemoved {
            Ec1Metadata.EntitySets.huHeadSet = metadata.entitySet(withName: "HUHeadSet")
        }
        if !Ec1Metadata.EntitySets.huItemSet.isRemoved {
            Ec1Metadata.EntitySets.huItemSet = metadata.entitySet(withName: "HUItemSet")
        }
        if !Ec1Metadata.EntitySets.huSet.isRemoved {
            Ec1Metadata.EntitySets.huSet = metadata.entitySet(withName: "HUSet")
        }
        if !Ec1Metadata.EntitySets.huSingleItemSet.isRemoved {
            Ec1Metadata.EntitySets.huSingleItemSet = metadata.entitySet(withName: "HUSingleItemSet")
        }
        if !Ec1Metadata.EntitySets.itemSet.isRemoved {
            Ec1Metadata.EntitySets.itemSet = metadata.entitySet(withName: "ItemSet")
        }
        if !Ec1Metadata.EntitySets.outbdlvHeadSet.isRemoved {
            Ec1Metadata.EntitySets.outbdlvHeadSet = metadata.entitySet(withName: "OUTBDLVHeadSet")
        }
        if !Ec1Metadata.EntitySets.outbdlvItemSet.isRemoved {
            Ec1Metadata.EntitySets.outbdlvItemSet = metadata.entitySet(withName: "OUTBDLVItemSet")
        }
        if !Ec1Metadata.EntitySets.performPack.isRemoved {
            Ec1Metadata.EntitySets.performPack = metadata.entitySet(withName: "PerformPack")
        }
        if !Ec1Metadata.EntitySets.purchaseOrderERPCreateRequestConfirmationInV1Set.isRemoved {
            Ec1Metadata.EntitySets.purchaseOrderERPCreateRequestConfirmationInV1Set = metadata.entitySet(withName: "PurchaseOrderERPCreateRequestConfirmation_In_V1Set")
        }
        if !Ec1Metadata.EntitySets.wtSimpleConfirmSet.isRemoved {
            Ec1Metadata.EntitySets.wtSimpleConfirmSet = metadata.entitySet(withName: "WTSimpleConfirmSet")
        }
        if !Ec1Metadata.EntitySets.warehouse.isRemoved {
            Ec1Metadata.EntitySets.warehouse = metadata.entitySet(withName: "Warehouse")
        }
        if !Ec1Metadata.EntitySets.warehouseTaskDELETE.isRemoved {
            Ec1Metadata.EntitySets.warehouseTaskDELETE = metadata.entitySet(withName: "WarehouseTaskDELETE")
        }
        if !Ec1Metadata.EntitySets.warehouseTaskSet.isRemoved {
            Ec1Metadata.EntitySets.warehouseTaskSet = metadata.entitySet(withName: "WarehouseTaskSet")
        }
        if !Ec1Metadata.ActionImports.createTask.isRemoved {
            Ec1Metadata.ActionImports.createTask = metadata.dataMethod(withName: "CreateTask")
        }
        if !Item2.id.isRemoved {
            Item2.id = Ec1Metadata.ComplexTypes.item2.property(withName: "ID")
        }
        if !Item2.groupID.isRemoved {
            Item2.groupID = Ec1Metadata.ComplexTypes.item2.property(withName: "GroupID")
        }
        if !Item2.typeCode_.isRemoved {
            Item2.typeCode_ = Ec1Metadata.ComplexTypes.item2.property(withName: "TypeCode")
        }
        if !Item2.processingTypeCode.isRemoved {
            Item2.processingTypeCode = Ec1Metadata.ComplexTypes.item2.property(withName: "ProcessingTypeCode")
        }
        if !Item2.quantity.isRemoved {
            Item2.quantity = Ec1Metadata.ComplexTypes.item2.property(withName: "Quantity")
        }
        if !Item2.receivingPlantID.isRemoved {
            Item2.receivingPlantID = Ec1Metadata.ComplexTypes.item2.property(withName: "ReceivingPlantID")
        }
        if !Item2.languageCode.isRemoved {
            Item2.languageCode = Ec1Metadata.ComplexTypes.item2.property(withName: "languageCode")
        }
        if !Item2.currencyCode.isRemoved {
            Item2.currencyCode = Ec1Metadata.ComplexTypes.item2.property(withName: "currencyCode")
        }
        if !Item2.unitCode.isRemoved {
            Item2.unitCode = Ec1Metadata.ComplexTypes.item2.property(withName: "unitCode")
        }
        if !Item2.requirementCode.isRemoved {
            Item2.requirementCode = Ec1Metadata.ComplexTypes.item2.property(withName: "RequirementCode")
        }
        if !Item2.internalID.isRemoved {
            Item2.internalID = Ec1Metadata.ComplexTypes.item2.property(withName: "InternalID")
        }
        if !Item2.intrenalID.isRemoved {
            Item2.intrenalID = Ec1Metadata.ComplexTypes.item2.property(withName: "IntrenalID")
        }
        if !Item2.product.isRemoved {
            Item2.product = Ec1Metadata.ComplexTypes.item2.property(withName: "Product")
        }
        if !Item2.transportServiceLevelCode.isRemoved {
            Item2.transportServiceLevelCode = Ec1Metadata.ComplexTypes.item2.property(withName: "TransportServiceLevelCode")
        }
        if !Item2.productTaxationCharacteristicsCode.isRemoved {
            Item2.productTaxationCharacteristicsCode = Ec1Metadata.ComplexTypes.item2.property(withName: "ProductTaxationCharacteristicsCode")
        }
        if !Item2.productionOrderID.isRemoved {
            Item2.productionOrderID = Ec1Metadata.ComplexTypes.item2.property(withName: "ProductionOrderID")
        }
        if !Item2.maintenanceOrderID.isRemoved {
            Item2.maintenanceOrderID = Ec1Metadata.ComplexTypes.item2.property(withName: "MaintenanceOrderID")
        }
        if !PickingReport.systemMessageIdentification.isRemoved {
            PickingReport.systemMessageIdentification = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageIdentification")
        }
        if !PickingReport.systemMessageNumber.isRemoved {
            PickingReport.systemMessageNumber = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageNumber")
        }
        if !PickingReport.systemMessageType.isRemoved {
            PickingReport.systemMessageType = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageType")
        }
        if !PickingReport.systemMessageVariable1.isRemoved {
            PickingReport.systemMessageVariable1 = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageVariable1")
        }
        if !PickingReport.systemMessageVariable2.isRemoved {
            PickingReport.systemMessageVariable2 = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageVariable2")
        }
        if !PickingReport.systemMessageVariable3.isRemoved {
            PickingReport.systemMessageVariable3 = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageVariable3")
        }
        if !PickingReport.systemMessageVariable4.isRemoved {
            PickingReport.systemMessageVariable4 = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "SystemMessageVariable4")
        }
        if !PickingReport.batch.isRemoved {
            PickingReport.batch = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "Batch")
        }
        if !PickingReport.deliveryQuantityUnit.isRemoved {
            PickingReport.deliveryQuantityUnit = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "DeliveryQuantityUnit")
        }
        if !PickingReport.actualDeliveryQuantity.isRemoved {
            PickingReport.actualDeliveryQuantity = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "ActualDeliveryQuantity")
        }
        if !PickingReport.deliveryDocumentItemText.isRemoved {
            PickingReport.deliveryDocumentItemText = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "DeliveryDocumentItemText")
        }
        if !PickingReport.material.isRemoved {
            PickingReport.material = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "Material")
        }
        if !PickingReport.deliveryDocumentItem.isRemoved {
            PickingReport.deliveryDocumentItem = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "DeliveryDocumentItem")
        }
        if !PickingReport.deliveryDocument.isRemoved {
            PickingReport.deliveryDocument = Ec1Metadata.ComplexTypes.pickingReport.property(withName: "DeliveryDocument")
        }
        if !Product.internalID.isRemoved {
            Product.internalID = Ec1Metadata.ComplexTypes.product.property(withName: "InternalID")
        }
        if !Product.buyerID.isRemoved {
            Product.buyerID = Ec1Metadata.ComplexTypes.product.property(withName: "BuyerID")
        }
        if !Product.sellerID.isRemoved {
            Product.sellerID = Ec1Metadata.ComplexTypes.product.property(withName: "SellerID")
        }
        if !Product.revisionID.isRemoved {
            Product.revisionID = Ec1Metadata.ComplexTypes.product.property(withName: "RevisionID")
        }
        if !Product.typeCode_.isRemoved {
            Product.typeCode_ = Ec1Metadata.ComplexTypes.product.property(withName: "TypeCode")
        }
        if !Product.inventoryValuationTypeCode.isRemoved {
            Product.inventoryValuationTypeCode = Ec1Metadata.ComplexTypes.product.property(withName: "InventoryValuationTypeCode")
        }
        if !Quantity.unitCode.isRemoved {
            Quantity.unitCode = Ec1Metadata.ComplexTypes.quantity.property(withName: "unitCode")
        }
        if !Quantity2.unitCode.isRemoved {
            Quantity2.unitCode = Ec1Metadata.ComplexTypes.quantity2.property(withName: "unitCode")
        }
        if !ResponsiblePurchasingGroupParty.internalID.isRemoved {
            ResponsiblePurchasingGroupParty.internalID = Ec1Metadata.ComplexTypes.responsiblePurchasingGroupParty.property(withName: "InternalID")
        }
        if !ResponsiblePurchasingOrganisationParty.internalID.isRemoved {
            ResponsiblePurchasingOrganisationParty.internalID = Ec1Metadata.ComplexTypes.responsiblePurchasingOrganisationParty.property(withName: "InternalID")
        }
        if !Return.collectiveProcessing.isRemoved {
            Return.collectiveProcessing = Ec1Metadata.ComplexTypes.return.property(withName: "CollectiveProcessing")
        }
        if !Return.deliveryDocument.isRemoved {
            Return.deliveryDocument = Ec1Metadata.ComplexTypes.return.property(withName: "DeliveryDocument")
        }
        if !Return.deliveryDocumentItem.isRemoved {
            Return.deliveryDocumentItem = Ec1Metadata.ComplexTypes.return.property(withName: "DeliveryDocumentItem")
        }
        if !Return.scheduleLine.isRemoved {
            Return.scheduleLine = Ec1Metadata.ComplexTypes.return.property(withName: "ScheduleLine")
        }
        if !Return.collectiveProcessingMsgCounter.isRemoved {
            Return.collectiveProcessingMsgCounter = Ec1Metadata.ComplexTypes.return.property(withName: "CollectiveProcessingMsgCounter")
        }
        if !Return.systemMessageIdentification.isRemoved {
            Return.systemMessageIdentification = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageIdentification")
        }
        if !Return.systemMessageNumber.isRemoved {
            Return.systemMessageNumber = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageNumber")
        }
        if !Return.systemMessageType.isRemoved {
            Return.systemMessageType = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageType")
        }
        if !Return.systemMessageVariable1.isRemoved {
            Return.systemMessageVariable1 = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageVariable1")
        }
        if !Return.systemMessageVariable2.isRemoved {
            Return.systemMessageVariable2 = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageVariable2")
        }
        if !Return.systemMessageVariable3.isRemoved {
            Return.systemMessageVariable3 = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageVariable3")
        }
        if !Return.systemMessageVariable4.isRemoved {
            Return.systemMessageVariable4 = Ec1Metadata.ComplexTypes.return.property(withName: "SystemMessageVariable4")
        }
        if !Return.collectiveProcessingType.isRemoved {
            Return.collectiveProcessingType = Ec1Metadata.ComplexTypes.return.property(withName: "CollectiveProcessingType")
        }
        if !ReturnActionsType.returnBool.isRemoved {
            ReturnActionsType.returnBool = Ec1Metadata.ComplexTypes.returnActionsType.property(withName: "ReturnBool")
        }
        if !AInbDeliveryAddressType.addressID.isRemoved {
            AInbDeliveryAddressType.addressID = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "AddressID")
        }
        if !AInbDeliveryAddressType.additionalStreetPrefixName.isRemoved {
            AInbDeliveryAddressType.additionalStreetPrefixName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "AdditionalStreetPrefixName")
        }
        if !AInbDeliveryAddressType.additionalStreetSuffixName.isRemoved {
            AInbDeliveryAddressType.additionalStreetSuffixName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "AdditionalStreetSuffixName")
        }
        if !AInbDeliveryAddressType.addressTimeZone.isRemoved {
            AInbDeliveryAddressType.addressTimeZone = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "AddressTimeZone")
        }
        if !AInbDeliveryAddressType.building.isRemoved {
            AInbDeliveryAddressType.building = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Building")
        }
        if !AInbDeliveryAddressType.businessPartnerName1.isRemoved {
            AInbDeliveryAddressType.businessPartnerName1 = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "BusinessPartnerName1")
        }
        if !AInbDeliveryAddressType.businessPartnerName2.isRemoved {
            AInbDeliveryAddressType.businessPartnerName2 = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "BusinessPartnerName2")
        }
        if !AInbDeliveryAddressType.businessPartnerName3.isRemoved {
            AInbDeliveryAddressType.businessPartnerName3 = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "BusinessPartnerName3")
        }
        if !AInbDeliveryAddressType.businessPartnerName4.isRemoved {
            AInbDeliveryAddressType.businessPartnerName4 = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "BusinessPartnerName4")
        }
        if !AInbDeliveryAddressType.careOfName.isRemoved {
            AInbDeliveryAddressType.careOfName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CareOfName")
        }
        if !AInbDeliveryAddressType.cityCode.isRemoved {
            AInbDeliveryAddressType.cityCode = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CityCode")
        }
        if !AInbDeliveryAddressType.cityName.isRemoved {
            AInbDeliveryAddressType.cityName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CityName")
        }
        if !AInbDeliveryAddressType.citySearch.isRemoved {
            AInbDeliveryAddressType.citySearch = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CitySearch")
        }
        if !AInbDeliveryAddressType.companyPostalCode.isRemoved {
            AInbDeliveryAddressType.companyPostalCode = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CompanyPostalCode")
        }
        if !AInbDeliveryAddressType.correspondenceLanguage.isRemoved {
            AInbDeliveryAddressType.correspondenceLanguage = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "CorrespondenceLanguage")
        }
        if !AInbDeliveryAddressType.country.isRemoved {
            AInbDeliveryAddressType.country = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Country")
        }
        if !AInbDeliveryAddressType.county.isRemoved {
            AInbDeliveryAddressType.county = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "County")
        }
        if !AInbDeliveryAddressType.deliveryServiceNumber.isRemoved {
            AInbDeliveryAddressType.deliveryServiceNumber = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "DeliveryServiceNumber")
        }
        if !AInbDeliveryAddressType.deliveryServiceTypeCode.isRemoved {
            AInbDeliveryAddressType.deliveryServiceTypeCode = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "DeliveryServiceTypeCode")
        }
        if !AInbDeliveryAddressType.district.isRemoved {
            AInbDeliveryAddressType.district = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "District")
        }
        if !AInbDeliveryAddressType.faxNumber.isRemoved {
            AInbDeliveryAddressType.faxNumber = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "FaxNumber")
        }
        if !AInbDeliveryAddressType.floor.isRemoved {
            AInbDeliveryAddressType.floor = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Floor")
        }
        if !AInbDeliveryAddressType.formOfAddress.isRemoved {
            AInbDeliveryAddressType.formOfAddress = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "FormOfAddress")
        }
        if !AInbDeliveryAddressType.fullName.isRemoved {
            AInbDeliveryAddressType.fullName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "FullName")
        }
        if !AInbDeliveryAddressType.homeCityName.isRemoved {
            AInbDeliveryAddressType.homeCityName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "HomeCityName")
        }
        if !AInbDeliveryAddressType.houseNumber.isRemoved {
            AInbDeliveryAddressType.houseNumber = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "HouseNumber")
        }
        if !AInbDeliveryAddressType.houseNumberSupplementText.isRemoved {
            AInbDeliveryAddressType.houseNumberSupplementText = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "HouseNumberSupplementText")
        }
        if !AInbDeliveryAddressType.nation.isRemoved {
            AInbDeliveryAddressType.nation = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Nation")
        }
        if !AInbDeliveryAddressType.person.isRemoved {
            AInbDeliveryAddressType.person = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Person")
        }
        if !AInbDeliveryAddressType.phoneNumber.isRemoved {
            AInbDeliveryAddressType.phoneNumber = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "PhoneNumber")
        }
        if !AInbDeliveryAddressType.poBox.isRemoved {
            AInbDeliveryAddressType.poBox = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBox")
        }
        if !AInbDeliveryAddressType.poBoxDeviatingCityName.isRemoved {
            AInbDeliveryAddressType.poBoxDeviatingCityName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxDeviatingCityName")
        }
        if !AInbDeliveryAddressType.poBoxDeviatingCountry.isRemoved {
            AInbDeliveryAddressType.poBoxDeviatingCountry = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxDeviatingCountry")
        }
        if !AInbDeliveryAddressType.poBoxDeviatingRegion.isRemoved {
            AInbDeliveryAddressType.poBoxDeviatingRegion = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxDeviatingRegion")
        }
        if !AInbDeliveryAddressType.poBoxIsWithoutNumber.isRemoved {
            AInbDeliveryAddressType.poBoxIsWithoutNumber = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxIsWithoutNumber")
        }
        if !AInbDeliveryAddressType.poBoxLobbyName.isRemoved {
            AInbDeliveryAddressType.poBoxLobbyName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxLobbyName")
        }
        if !AInbDeliveryAddressType.poBoxPostalCode.isRemoved {
            AInbDeliveryAddressType.poBoxPostalCode = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "POBoxPostalCode")
        }
        if !AInbDeliveryAddressType.postalCode.isRemoved {
            AInbDeliveryAddressType.postalCode = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "PostalCode")
        }
        if !AInbDeliveryAddressType.prfrdCommMediumType.isRemoved {
            AInbDeliveryAddressType.prfrdCommMediumType = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "PrfrdCommMediumType")
        }
        if !AInbDeliveryAddressType.region.isRemoved {
            AInbDeliveryAddressType.region = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "Region")
        }
        if !AInbDeliveryAddressType.roomNumber.isRemoved {
            AInbDeliveryAddressType.roomNumber = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "RoomNumber")
        }
        if !AInbDeliveryAddressType.searchTerm1.isRemoved {
            AInbDeliveryAddressType.searchTerm1 = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "SearchTerm1")
        }
        if !AInbDeliveryAddressType.streetName.isRemoved {
            AInbDeliveryAddressType.streetName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "StreetName")
        }
        if !AInbDeliveryAddressType.streetPrefixName.isRemoved {
            AInbDeliveryAddressType.streetPrefixName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "StreetPrefixName")
        }
        if !AInbDeliveryAddressType.streetSearch.isRemoved {
            AInbDeliveryAddressType.streetSearch = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "StreetSearch")
        }
        if !AInbDeliveryAddressType.streetSuffixName.isRemoved {
            AInbDeliveryAddressType.streetSuffixName = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "StreetSuffixName")
        }
        if !AInbDeliveryAddressType.taxJurisdiction.isRemoved {
            AInbDeliveryAddressType.taxJurisdiction = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "TaxJurisdiction")
        }
        if !AInbDeliveryAddressType.transportZone.isRemoved {
            AInbDeliveryAddressType.transportZone = Ec1Metadata.EntityTypes.aInbDeliveryAddressType.property(withName: "TransportZone")
        }
        if !AInbDeliveryDocFlowType.deliveryVersion.isRemoved {
            AInbDeliveryDocFlowType.deliveryVersion = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "DeliveryVersion")
        }
        if !AInbDeliveryDocFlowType.precedingDocument.isRemoved {
            AInbDeliveryDocFlowType.precedingDocument = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "PrecedingDocument")
        }
        if !AInbDeliveryDocFlowType.precedingDocumentCategory.isRemoved {
            AInbDeliveryDocFlowType.precedingDocumentCategory = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "PrecedingDocumentCategory")
        }
        if !AInbDeliveryDocFlowType.precedingDocumentItem.isRemoved {
            AInbDeliveryDocFlowType.precedingDocumentItem = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "PrecedingDocumentItem")
        }
        if !AInbDeliveryDocFlowType.quantityInBaseUnit.isRemoved {
            AInbDeliveryDocFlowType.quantityInBaseUnit = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "QuantityInBaseUnit")
        }
        if !AInbDeliveryDocFlowType.sdFulfillmentCalculationRule.isRemoved {
            AInbDeliveryDocFlowType.sdFulfillmentCalculationRule = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "SDFulfillmentCalculationRule")
        }
        if !AInbDeliveryDocFlowType.subsequentDocument.isRemoved {
            AInbDeliveryDocFlowType.subsequentDocument = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "SubsequentDocument")
        }
        if !AInbDeliveryDocFlowType.subsequentDocumentCategory.isRemoved {
            AInbDeliveryDocFlowType.subsequentDocumentCategory = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "SubsequentDocumentCategory")
        }
        if !AInbDeliveryDocFlowType.subsequentDocumentItem.isRemoved {
            AInbDeliveryDocFlowType.subsequentDocumentItem = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "SubsequentDocumentItem")
        }
        if !AInbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd.isRemoved {
            AInbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd = Ec1Metadata.EntityTypes.aInbDeliveryDocFlowType.property(withName: "TransferOrderInWrhsMgmtIsConfd")
        }
        if !AInbDeliveryHeaderTextType.deliveryDocument.isRemoved {
            AInbDeliveryHeaderTextType.deliveryDocument = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "DeliveryDocument")
        }
        if !AInbDeliveryHeaderTextType.textElement.isRemoved {
            AInbDeliveryHeaderTextType.textElement = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "TextElement")
        }
        if !AInbDeliveryHeaderTextType.language.isRemoved {
            AInbDeliveryHeaderTextType.language = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "Language")
        }
        if !AInbDeliveryHeaderTextType.textElementDescription.isRemoved {
            AInbDeliveryHeaderTextType.textElementDescription = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "TextElementDescription")
        }
        if !AInbDeliveryHeaderTextType.textElementText.isRemoved {
            AInbDeliveryHeaderTextType.textElementText = Ec1Metadata.EntityTypes.aInbDeliveryHeaderTextType.property(withName: "TextElementText")
        }
        if !AInbDeliveryHeaderType.actualDeliveryRoute.isRemoved {
            AInbDeliveryHeaderType.actualDeliveryRoute = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ActualDeliveryRoute")
        }
        if !AInbDeliveryHeaderType.actualGoodsMovementDate.isRemoved {
            AInbDeliveryHeaderType.actualGoodsMovementDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ActualGoodsMovementDate")
        }
        if !AInbDeliveryHeaderType.actualGoodsMovementTime.isRemoved {
            AInbDeliveryHeaderType.actualGoodsMovementTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ActualGoodsMovementTime")
        }
        if !AInbDeliveryHeaderType.billingDocumentDate.isRemoved {
            AInbDeliveryHeaderType.billingDocumentDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "BillingDocumentDate")
        }
        if !AInbDeliveryHeaderType.billOfLading.isRemoved {
            AInbDeliveryHeaderType.billOfLading = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "BillOfLading")
        }
        if !AInbDeliveryHeaderType.completeDeliveryIsDefined.isRemoved {
            AInbDeliveryHeaderType.completeDeliveryIsDefined = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CompleteDeliveryIsDefined")
        }
        if !AInbDeliveryHeaderType.confirmationTime.isRemoved {
            AInbDeliveryHeaderType.confirmationTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ConfirmationTime")
        }
        if !AInbDeliveryHeaderType.createdByUser.isRemoved {
            AInbDeliveryHeaderType.createdByUser = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CreatedByUser")
        }
        if !AInbDeliveryHeaderType.creationDate.isRemoved {
            AInbDeliveryHeaderType.creationDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CreationDate")
        }
        if !AInbDeliveryHeaderType.creationTime.isRemoved {
            AInbDeliveryHeaderType.creationTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CreationTime")
        }
        if !AInbDeliveryHeaderType.customerGroup.isRemoved {
            AInbDeliveryHeaderType.customerGroup = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "CustomerGroup")
        }
        if !AInbDeliveryHeaderType.deliveryBlockReason.isRemoved {
            AInbDeliveryHeaderType.deliveryBlockReason = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryBlockReason")
        }
        if !AInbDeliveryHeaderType.deliveryDate.isRemoved {
            AInbDeliveryHeaderType.deliveryDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryDate")
        }
        if !AInbDeliveryHeaderType.deliveryDocument.isRemoved {
            AInbDeliveryHeaderType.deliveryDocument = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryDocument")
        }
        if !AInbDeliveryHeaderType.deliveryDocumentBySupplier.isRemoved {
            AInbDeliveryHeaderType.deliveryDocumentBySupplier = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryDocumentBySupplier")
        }
        if !AInbDeliveryHeaderType.deliveryDocumentType.isRemoved {
            AInbDeliveryHeaderType.deliveryDocumentType = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryDocumentType")
        }
        if !AInbDeliveryHeaderType.deliveryIsInPlant.isRemoved {
            AInbDeliveryHeaderType.deliveryIsInPlant = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryIsInPlant")
        }
        if !AInbDeliveryHeaderType.deliveryPriority.isRemoved {
            AInbDeliveryHeaderType.deliveryPriority = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryPriority")
        }
        if !AInbDeliveryHeaderType.deliveryTime.isRemoved {
            AInbDeliveryHeaderType.deliveryTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryTime")
        }
        if !AInbDeliveryHeaderType.deliveryVersion.isRemoved {
            AInbDeliveryHeaderType.deliveryVersion = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DeliveryVersion")
        }
        if !AInbDeliveryHeaderType.depreciationPercentage.isRemoved {
            AInbDeliveryHeaderType.depreciationPercentage = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DepreciationPercentage")
        }
        if !AInbDeliveryHeaderType.distrStatusByDecentralizedWrhs.isRemoved {
            AInbDeliveryHeaderType.distrStatusByDecentralizedWrhs = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DistrStatusByDecentralizedWrhs")
        }
        if !AInbDeliveryHeaderType.documentDate.isRemoved {
            AInbDeliveryHeaderType.documentDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "DocumentDate")
        }
        if !AInbDeliveryHeaderType.externalIdentificationType.isRemoved {
            AInbDeliveryHeaderType.externalIdentificationType = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ExternalIdentificationType")
        }
        if !AInbDeliveryHeaderType.externalTransportSystem.isRemoved {
            AInbDeliveryHeaderType.externalTransportSystem = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ExternalTransportSystem")
        }
        if !AInbDeliveryHeaderType.factoryCalendarByCustomer.isRemoved {
            AInbDeliveryHeaderType.factoryCalendarByCustomer = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "FactoryCalendarByCustomer")
        }
        if !AInbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber.isRemoved {
            AInbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "GoodsIssueOrReceiptSlipNumber")
        }
        if !AInbDeliveryHeaderType.goodsIssueTime.isRemoved {
            AInbDeliveryHeaderType.goodsIssueTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "GoodsIssueTime")
        }
        if !AInbDeliveryHeaderType.handlingUnitInStock.isRemoved {
            AInbDeliveryHeaderType.handlingUnitInStock = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HandlingUnitInStock")
        }
        if !AInbDeliveryHeaderType.hdrGeneralIncompletionStatus.isRemoved {
            AInbDeliveryHeaderType.hdrGeneralIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HdrGeneralIncompletionStatus")
        }
        if !AInbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus.isRemoved {
            AInbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HdrGoodsMvtIncompletionStatus")
        }
        if !AInbDeliveryHeaderType.headerBillgIncompletionStatus.isRemoved {
            AInbDeliveryHeaderType.headerBillgIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderBillgIncompletionStatus")
        }
        if !AInbDeliveryHeaderType.headerBillingBlockReason.isRemoved {
            AInbDeliveryHeaderType.headerBillingBlockReason = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderBillingBlockReason")
        }
        if !AInbDeliveryHeaderType.headerDelivIncompletionStatus.isRemoved {
            AInbDeliveryHeaderType.headerDelivIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderDelivIncompletionStatus")
        }
        if !AInbDeliveryHeaderType.headerGrossWeight.isRemoved {
            AInbDeliveryHeaderType.headerGrossWeight = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderGrossWeight")
        }
        if !AInbDeliveryHeaderType.headerNetWeight.isRemoved {
            AInbDeliveryHeaderType.headerNetWeight = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderNetWeight")
        }
        if !AInbDeliveryHeaderType.headerPackingIncompletionSts.isRemoved {
            AInbDeliveryHeaderType.headerPackingIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderPackingIncompletionSts")
        }
        if !AInbDeliveryHeaderType.headerPickgIncompletionStatus.isRemoved {
            AInbDeliveryHeaderType.headerPickgIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderPickgIncompletionStatus")
        }
        if !AInbDeliveryHeaderType.headerVolume.isRemoved {
            AInbDeliveryHeaderType.headerVolume = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderVolume")
        }
        if !AInbDeliveryHeaderType.headerVolumeUnit.isRemoved {
            AInbDeliveryHeaderType.headerVolumeUnit = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderVolumeUnit")
        }
        if !AInbDeliveryHeaderType.headerWeightUnit.isRemoved {
            AInbDeliveryHeaderType.headerWeightUnit = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "HeaderWeightUnit")
        }
        if !AInbDeliveryHeaderType.incotermsClassification.isRemoved {
            AInbDeliveryHeaderType.incotermsClassification = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IncotermsClassification")
        }
        if !AInbDeliveryHeaderType.incotermsTransferLocation.isRemoved {
            AInbDeliveryHeaderType.incotermsTransferLocation = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IncotermsTransferLocation")
        }
        if !AInbDeliveryHeaderType.intercompanyBillingDate.isRemoved {
            AInbDeliveryHeaderType.intercompanyBillingDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IntercompanyBillingDate")
        }
        if !AInbDeliveryHeaderType.internalFinancialDocument.isRemoved {
            AInbDeliveryHeaderType.internalFinancialDocument = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "InternalFinancialDocument")
        }
        if !AInbDeliveryHeaderType.isDeliveryForSingleWarehouse.isRemoved {
            AInbDeliveryHeaderType.isDeliveryForSingleWarehouse = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IsDeliveryForSingleWarehouse")
        }
        if !AInbDeliveryHeaderType.isExportDelivery.isRemoved {
            AInbDeliveryHeaderType.isExportDelivery = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "IsExportDelivery")
        }
        if !AInbDeliveryHeaderType.lastChangeDate.isRemoved {
            AInbDeliveryHeaderType.lastChangeDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LastChangeDate")
        }
        if !AInbDeliveryHeaderType.lastChangedByUser.isRemoved {
            AInbDeliveryHeaderType.lastChangedByUser = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LastChangedByUser")
        }
        if !AInbDeliveryHeaderType.loadingDate.isRemoved {
            AInbDeliveryHeaderType.loadingDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LoadingDate")
        }
        if !AInbDeliveryHeaderType.loadingPoint.isRemoved {
            AInbDeliveryHeaderType.loadingPoint = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LoadingPoint")
        }
        if !AInbDeliveryHeaderType.loadingTime.isRemoved {
            AInbDeliveryHeaderType.loadingTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "LoadingTime")
        }
        if !AInbDeliveryHeaderType.meansOfTransport.isRemoved {
            AInbDeliveryHeaderType.meansOfTransport = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "MeansOfTransport")
        }
        if !AInbDeliveryHeaderType.meansOfTransportRefMaterial.isRemoved {
            AInbDeliveryHeaderType.meansOfTransportRefMaterial = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "MeansOfTransportRefMaterial")
        }
        if !AInbDeliveryHeaderType.meansOfTransportType.isRemoved {
            AInbDeliveryHeaderType.meansOfTransportType = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "MeansOfTransportType")
        }
        if !AInbDeliveryHeaderType.orderCombinationIsAllowed.isRemoved {
            AInbDeliveryHeaderType.orderCombinationIsAllowed = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OrderCombinationIsAllowed")
        }
        if !AInbDeliveryHeaderType.orderID.isRemoved {
            AInbDeliveryHeaderType.orderID = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OrderID")
        }
        if !AInbDeliveryHeaderType.overallDelivConfStatus.isRemoved {
            AInbDeliveryHeaderType.overallDelivConfStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallDelivConfStatus")
        }
        if !AInbDeliveryHeaderType.overallDelivReltdBillgStatus.isRemoved {
            AInbDeliveryHeaderType.overallDelivReltdBillgStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallDelivReltdBillgStatus")
        }
        if !AInbDeliveryHeaderType.overallGoodsMovementStatus.isRemoved {
            AInbDeliveryHeaderType.overallGoodsMovementStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallGoodsMovementStatus")
        }
        if !AInbDeliveryHeaderType.overallIntcoBillingStatus.isRemoved {
            AInbDeliveryHeaderType.overallIntcoBillingStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallIntcoBillingStatus")
        }
        if !AInbDeliveryHeaderType.overallPackingStatus.isRemoved {
            AInbDeliveryHeaderType.overallPackingStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallPackingStatus")
        }
        if !AInbDeliveryHeaderType.overallPickingConfStatus.isRemoved {
            AInbDeliveryHeaderType.overallPickingConfStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallPickingConfStatus")
        }
        if !AInbDeliveryHeaderType.overallPickingStatus.isRemoved {
            AInbDeliveryHeaderType.overallPickingStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallPickingStatus")
        }
        if !AInbDeliveryHeaderType.overallProofOfDeliveryStatus.isRemoved {
            AInbDeliveryHeaderType.overallProofOfDeliveryStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallProofOfDeliveryStatus")
        }
        if !AInbDeliveryHeaderType.overallSDProcessStatus.isRemoved {
            AInbDeliveryHeaderType.overallSDProcessStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallSDProcessStatus")
        }
        if !AInbDeliveryHeaderType.overallWarehouseActivityStatus.isRemoved {
            AInbDeliveryHeaderType.overallWarehouseActivityStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OverallWarehouseActivityStatus")
        }
        if !AInbDeliveryHeaderType.ovrlItmDelivIncompletionSts.isRemoved {
            AInbDeliveryHeaderType.ovrlItmDelivIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmDelivIncompletionSts")
        }
        if !AInbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts.isRemoved {
            AInbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmGdsMvtIncompletionSts")
        }
        if !AInbDeliveryHeaderType.ovrlItmGeneralIncompletionSts.isRemoved {
            AInbDeliveryHeaderType.ovrlItmGeneralIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmGeneralIncompletionSts")
        }
        if !AInbDeliveryHeaderType.ovrlItmPackingIncompletionSts.isRemoved {
            AInbDeliveryHeaderType.ovrlItmPackingIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmPackingIncompletionSts")
        }
        if !AInbDeliveryHeaderType.ovrlItmPickingIncompletionSts.isRemoved {
            AInbDeliveryHeaderType.ovrlItmPickingIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "OvrlItmPickingIncompletionSts")
        }
        if !AInbDeliveryHeaderType.paymentGuaranteeProcedure.isRemoved {
            AInbDeliveryHeaderType.paymentGuaranteeProcedure = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PaymentGuaranteeProcedure")
        }
        if !AInbDeliveryHeaderType.pickedItemsLocation.isRemoved {
            AInbDeliveryHeaderType.pickedItemsLocation = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PickedItemsLocation")
        }
        if !AInbDeliveryHeaderType.pickingDate.isRemoved {
            AInbDeliveryHeaderType.pickingDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PickingDate")
        }
        if !AInbDeliveryHeaderType.pickingTime.isRemoved {
            AInbDeliveryHeaderType.pickingTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PickingTime")
        }
        if !AInbDeliveryHeaderType.plannedGoodsIssueDate.isRemoved {
            AInbDeliveryHeaderType.plannedGoodsIssueDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "PlannedGoodsIssueDate")
        }
        if !AInbDeliveryHeaderType.proofOfDeliveryDate.isRemoved {
            AInbDeliveryHeaderType.proofOfDeliveryDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ProofOfDeliveryDate")
        }
        if !AInbDeliveryHeaderType.proposedDeliveryRoute.isRemoved {
            AInbDeliveryHeaderType.proposedDeliveryRoute = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ProposedDeliveryRoute")
        }
        if !AInbDeliveryHeaderType.receivingPlant.isRemoved {
            AInbDeliveryHeaderType.receivingPlant = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ReceivingPlant")
        }
        if !AInbDeliveryHeaderType.routeSchedule.isRemoved {
            AInbDeliveryHeaderType.routeSchedule = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "RouteSchedule")
        }
        if !AInbDeliveryHeaderType.salesDistrict.isRemoved {
            AInbDeliveryHeaderType.salesDistrict = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SalesDistrict")
        }
        if !AInbDeliveryHeaderType.salesOffice.isRemoved {
            AInbDeliveryHeaderType.salesOffice = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SalesOffice")
        }
        if !AInbDeliveryHeaderType.salesOrganization.isRemoved {
            AInbDeliveryHeaderType.salesOrganization = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SalesOrganization")
        }
        if !AInbDeliveryHeaderType.sdDocumentCategory.isRemoved {
            AInbDeliveryHeaderType.sdDocumentCategory = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SDDocumentCategory")
        }
        if !AInbDeliveryHeaderType.shipmentBlockReason.isRemoved {
            AInbDeliveryHeaderType.shipmentBlockReason = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShipmentBlockReason")
        }
        if !AInbDeliveryHeaderType.shippingCondition.isRemoved {
            AInbDeliveryHeaderType.shippingCondition = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShippingCondition")
        }
        if !AInbDeliveryHeaderType.shippingPoint.isRemoved {
            AInbDeliveryHeaderType.shippingPoint = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShippingPoint")
        }
        if !AInbDeliveryHeaderType.shippingType.isRemoved {
            AInbDeliveryHeaderType.shippingType = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShippingType")
        }
        if !AInbDeliveryHeaderType.shipToParty.isRemoved {
            AInbDeliveryHeaderType.shipToParty = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "ShipToParty")
        }
        if !AInbDeliveryHeaderType.soldToParty.isRemoved {
            AInbDeliveryHeaderType.soldToParty = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SoldToParty")
        }
        if !AInbDeliveryHeaderType.specialProcessingCode.isRemoved {
            AInbDeliveryHeaderType.specialProcessingCode = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "SpecialProcessingCode")
        }
        if !AInbDeliveryHeaderType.statisticsCurrency.isRemoved {
            AInbDeliveryHeaderType.statisticsCurrency = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "StatisticsCurrency")
        }
        if !AInbDeliveryHeaderType.supplier.isRemoved {
            AInbDeliveryHeaderType.supplier = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "Supplier")
        }
        if !AInbDeliveryHeaderType.totalBlockStatus.isRemoved {
            AInbDeliveryHeaderType.totalBlockStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TotalBlockStatus")
        }
        if !AInbDeliveryHeaderType.totalCreditCheckStatus.isRemoved {
            AInbDeliveryHeaderType.totalCreditCheckStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TotalCreditCheckStatus")
        }
        if !AInbDeliveryHeaderType.totalNumberOfPackage.isRemoved {
            AInbDeliveryHeaderType.totalNumberOfPackage = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TotalNumberOfPackage")
        }
        if !AInbDeliveryHeaderType.transactionCurrency.isRemoved {
            AInbDeliveryHeaderType.transactionCurrency = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransactionCurrency")
        }
        if !AInbDeliveryHeaderType.transportationGroup.isRemoved {
            AInbDeliveryHeaderType.transportationGroup = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransportationGroup")
        }
        if !AInbDeliveryHeaderType.transportationPlanningDate.isRemoved {
            AInbDeliveryHeaderType.transportationPlanningDate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransportationPlanningDate")
        }
        if !AInbDeliveryHeaderType.transportationPlanningStatus.isRemoved {
            AInbDeliveryHeaderType.transportationPlanningStatus = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransportationPlanningStatus")
        }
        if !AInbDeliveryHeaderType.transportationPlanningTime.isRemoved {
            AInbDeliveryHeaderType.transportationPlanningTime = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "TransportationPlanningTime")
        }
        if !AInbDeliveryHeaderType.unloadingPointName.isRemoved {
            AInbDeliveryHeaderType.unloadingPointName = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "UnloadingPointName")
        }
        if !AInbDeliveryHeaderType.warehouse.isRemoved {
            AInbDeliveryHeaderType.warehouse = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "Warehouse")
        }
        if !AInbDeliveryHeaderType.warehouseGate.isRemoved {
            AInbDeliveryHeaderType.warehouseGate = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "WarehouseGate")
        }
        if !AInbDeliveryHeaderType.warehouseStagingArea.isRemoved {
            AInbDeliveryHeaderType.warehouseStagingArea = Ec1Metadata.EntityTypes.aInbDeliveryHeaderType.property(withName: "WarehouseStagingArea")
        }
        if !AInbDeliveryItemTextType.deliveryDocument.isRemoved {
            AInbDeliveryItemTextType.deliveryDocument = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "DeliveryDocument")
        }
        if !AInbDeliveryItemTextType.deliveryDocumentItem.isRemoved {
            AInbDeliveryItemTextType.deliveryDocumentItem = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "DeliveryDocumentItem")
        }
        if !AInbDeliveryItemTextType.textElement.isRemoved {
            AInbDeliveryItemTextType.textElement = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "TextElement")
        }
        if !AInbDeliveryItemTextType.language.isRemoved {
            AInbDeliveryItemTextType.language = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "Language")
        }
        if !AInbDeliveryItemTextType.textElementDescription.isRemoved {
            AInbDeliveryItemTextType.textElementDescription = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "TextElementDescription")
        }
        if !AInbDeliveryItemTextType.textElementText.isRemoved {
            AInbDeliveryItemTextType.textElementText = Ec1Metadata.EntityTypes.aInbDeliveryItemTextType.property(withName: "TextElementText")
        }
        if !AInbDeliveryItemType.actualDeliveredQtyInBaseUnit.isRemoved {
            AInbDeliveryItemType.actualDeliveredQtyInBaseUnit = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ActualDeliveredQtyInBaseUnit")
        }
        if !AInbDeliveryItemType.actualDeliveryQuantity.isRemoved {
            AInbDeliveryItemType.actualDeliveryQuantity = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ActualDeliveryQuantity")
        }
        if !AInbDeliveryItemType.additionalCustomerGroup1.isRemoved {
            AInbDeliveryItemType.additionalCustomerGroup1 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup1")
        }
        if !AInbDeliveryItemType.additionalCustomerGroup2.isRemoved {
            AInbDeliveryItemType.additionalCustomerGroup2 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup2")
        }
        if !AInbDeliveryItemType.additionalCustomerGroup3.isRemoved {
            AInbDeliveryItemType.additionalCustomerGroup3 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup3")
        }
        if !AInbDeliveryItemType.additionalCustomerGroup4.isRemoved {
            AInbDeliveryItemType.additionalCustomerGroup4 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup4")
        }
        if !AInbDeliveryItemType.additionalCustomerGroup5.isRemoved {
            AInbDeliveryItemType.additionalCustomerGroup5 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalCustomerGroup5")
        }
        if !AInbDeliveryItemType.additionalMaterialGroup1.isRemoved {
            AInbDeliveryItemType.additionalMaterialGroup1 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup1")
        }
        if !AInbDeliveryItemType.additionalMaterialGroup2.isRemoved {
            AInbDeliveryItemType.additionalMaterialGroup2 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup2")
        }
        if !AInbDeliveryItemType.additionalMaterialGroup3.isRemoved {
            AInbDeliveryItemType.additionalMaterialGroup3 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup3")
        }
        if !AInbDeliveryItemType.additionalMaterialGroup4.isRemoved {
            AInbDeliveryItemType.additionalMaterialGroup4 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup4")
        }
        if !AInbDeliveryItemType.additionalMaterialGroup5.isRemoved {
            AInbDeliveryItemType.additionalMaterialGroup5 = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AdditionalMaterialGroup5")
        }
        if !AInbDeliveryItemType.alternateProductNumber.isRemoved {
            AInbDeliveryItemType.alternateProductNumber = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "AlternateProductNumber")
        }
        if !AInbDeliveryItemType.baseUnit.isRemoved {
            AInbDeliveryItemType.baseUnit = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BaseUnit")
        }
        if !AInbDeliveryItemType.batch.isRemoved {
            AInbDeliveryItemType.batch = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Batch")
        }
        if !AInbDeliveryItemType.batchBySupplier.isRemoved {
            AInbDeliveryItemType.batchBySupplier = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BatchBySupplier")
        }
        if !AInbDeliveryItemType.batchClassification.isRemoved {
            AInbDeliveryItemType.batchClassification = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BatchClassification")
        }
        if !AInbDeliveryItemType.bomExplosion.isRemoved {
            AInbDeliveryItemType.bomExplosion = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BOMExplosion")
        }
        if !AInbDeliveryItemType.businessArea.isRemoved {
            AInbDeliveryItemType.businessArea = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "BusinessArea")
        }
        if !AInbDeliveryItemType.consumptionPosting.isRemoved {
            AInbDeliveryItemType.consumptionPosting = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ConsumptionPosting")
        }
        if !AInbDeliveryItemType.controllingArea.isRemoved {
            AInbDeliveryItemType.controllingArea = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ControllingArea")
        }
        if !AInbDeliveryItemType.costCenter.isRemoved {
            AInbDeliveryItemType.costCenter = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CostCenter")
        }
        if !AInbDeliveryItemType.createdByUser.isRemoved {
            AInbDeliveryItemType.createdByUser = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CreatedByUser")
        }
        if !AInbDeliveryItemType.creationDate.isRemoved {
            AInbDeliveryItemType.creationDate = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CreationDate")
        }
        if !AInbDeliveryItemType.creationTime.isRemoved {
            AInbDeliveryItemType.creationTime = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CreationTime")
        }
        if !AInbDeliveryItemType.custEngineeringChgStatus.isRemoved {
            AInbDeliveryItemType.custEngineeringChgStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "CustEngineeringChgStatus")
        }
        if !AInbDeliveryItemType.deliveryDocument.isRemoved {
            AInbDeliveryItemType.deliveryDocument = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryDocument")
        }
        if !AInbDeliveryItemType.deliveryDocumentItem.isRemoved {
            AInbDeliveryItemType.deliveryDocumentItem = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryDocumentItem")
        }
        if !AInbDeliveryItemType.deliveryDocumentItemCategory.isRemoved {
            AInbDeliveryItemType.deliveryDocumentItemCategory = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryDocumentItemCategory")
        }
        if !AInbDeliveryItemType.deliveryDocumentItemText.isRemoved {
            AInbDeliveryItemType.deliveryDocumentItemText = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryDocumentItemText")
        }
        if !AInbDeliveryItemType.deliveryGroup.isRemoved {
            AInbDeliveryItemType.deliveryGroup = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryGroup")
        }
        if !AInbDeliveryItemType.deliveryQuantityUnit.isRemoved {
            AInbDeliveryItemType.deliveryQuantityUnit = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryQuantityUnit")
        }
        if !AInbDeliveryItemType.deliveryRelatedBillingStatus.isRemoved {
            AInbDeliveryItemType.deliveryRelatedBillingStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryRelatedBillingStatus")
        }
        if !AInbDeliveryItemType.deliveryToBaseQuantityDnmntr.isRemoved {
            AInbDeliveryItemType.deliveryToBaseQuantityDnmntr = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryToBaseQuantityDnmntr")
        }
        if !AInbDeliveryItemType.deliveryToBaseQuantityNmrtr.isRemoved {
            AInbDeliveryItemType.deliveryToBaseQuantityNmrtr = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryToBaseQuantityNmrtr")
        }
        if !AInbDeliveryItemType.departmentClassificationByCust.isRemoved {
            AInbDeliveryItemType.departmentClassificationByCust = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DepartmentClassificationByCust")
        }
        if !AInbDeliveryItemType.distributionChannel.isRemoved {
            AInbDeliveryItemType.distributionChannel = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DistributionChannel")
        }
        if !AInbDeliveryItemType.division.isRemoved {
            AInbDeliveryItemType.division = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Division")
        }
        if !AInbDeliveryItemType.fixedShipgProcgDurationInDays.isRemoved {
            AInbDeliveryItemType.fixedShipgProcgDurationInDays = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "FixedShipgProcgDurationInDays")
        }
        if !AInbDeliveryItemType.glAccount.isRemoved {
            AInbDeliveryItemType.glAccount = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "GLAccount")
        }
        if !AInbDeliveryItemType.goodsMovementReasonCode.isRemoved {
            AInbDeliveryItemType.goodsMovementReasonCode = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "GoodsMovementReasonCode")
        }
        if !AInbDeliveryItemType.goodsMovementStatus.isRemoved {
            AInbDeliveryItemType.goodsMovementStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "GoodsMovementStatus")
        }
        if !AInbDeliveryItemType.goodsMovementType.isRemoved {
            AInbDeliveryItemType.goodsMovementType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "GoodsMovementType")
        }
        if !AInbDeliveryItemType.higherLevelItem.isRemoved {
            AInbDeliveryItemType.higherLevelItem = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "HigherLevelItem")
        }
        if !AInbDeliveryItemType.inspectionLot.isRemoved {
            AInbDeliveryItemType.inspectionLot = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InspectionLot")
        }
        if !AInbDeliveryItemType.inspectionPartialLot.isRemoved {
            AInbDeliveryItemType.inspectionPartialLot = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InspectionPartialLot")
        }
        if !AInbDeliveryItemType.intercompanyBillingStatus.isRemoved {
            AInbDeliveryItemType.intercompanyBillingStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IntercompanyBillingStatus")
        }
        if !AInbDeliveryItemType.internationalArticleNumber.isRemoved {
            AInbDeliveryItemType.internationalArticleNumber = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InternationalArticleNumber")
        }
        if !AInbDeliveryItemType.inventorySpecialStockType.isRemoved {
            AInbDeliveryItemType.inventorySpecialStockType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InventorySpecialStockType")
        }
        if !AInbDeliveryItemType.inventoryValuationType.isRemoved {
            AInbDeliveryItemType.inventoryValuationType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "InventoryValuationType")
        }
        if !AInbDeliveryItemType.isCompletelyDelivered.isRemoved {
            AInbDeliveryItemType.isCompletelyDelivered = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IsCompletelyDelivered")
        }
        if !AInbDeliveryItemType.isNotGoodsMovementsRelevant.isRemoved {
            AInbDeliveryItemType.isNotGoodsMovementsRelevant = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IsNotGoodsMovementsRelevant")
        }
        if !AInbDeliveryItemType.isSeparateValuation.isRemoved {
            AInbDeliveryItemType.isSeparateValuation = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IsSeparateValuation")
        }
        if !AInbDeliveryItemType.issgOrRcvgBatch.isRemoved {
            AInbDeliveryItemType.issgOrRcvgBatch = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgBatch")
        }
        if !AInbDeliveryItemType.issgOrRcvgMaterial.isRemoved {
            AInbDeliveryItemType.issgOrRcvgMaterial = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgMaterial")
        }
        if !AInbDeliveryItemType.issgOrRcvgSpclStockInd.isRemoved {
            AInbDeliveryItemType.issgOrRcvgSpclStockInd = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgSpclStockInd")
        }
        if !AInbDeliveryItemType.issgOrRcvgStockCategory.isRemoved {
            AInbDeliveryItemType.issgOrRcvgStockCategory = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgStockCategory")
        }
        if !AInbDeliveryItemType.issgOrRcvgValuationType.isRemoved {
            AInbDeliveryItemType.issgOrRcvgValuationType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssgOrRcvgValuationType")
        }
        if !AInbDeliveryItemType.issuingOrReceivingPlant.isRemoved {
            AInbDeliveryItemType.issuingOrReceivingPlant = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssuingOrReceivingPlant")
        }
        if !AInbDeliveryItemType.issuingOrReceivingStorageLoc.isRemoved {
            AInbDeliveryItemType.issuingOrReceivingStorageLoc = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "IssuingOrReceivingStorageLoc")
        }
        if !AInbDeliveryItemType.itemBillingBlockReason.isRemoved {
            AInbDeliveryItemType.itemBillingBlockReason = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemBillingBlockReason")
        }
        if !AInbDeliveryItemType.itemBillingIncompletionStatus.isRemoved {
            AInbDeliveryItemType.itemBillingIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemBillingIncompletionStatus")
        }
        if !AInbDeliveryItemType.itemDeliveryIncompletionStatus.isRemoved {
            AInbDeliveryItemType.itemDeliveryIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemDeliveryIncompletionStatus")
        }
        if !AInbDeliveryItemType.itemGdsMvtIncompletionSts.isRemoved {
            AInbDeliveryItemType.itemGdsMvtIncompletionSts = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemGdsMvtIncompletionSts")
        }
        if !AInbDeliveryItemType.itemGeneralIncompletionStatus.isRemoved {
            AInbDeliveryItemType.itemGeneralIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemGeneralIncompletionStatus")
        }
        if !AInbDeliveryItemType.itemGrossWeight.isRemoved {
            AInbDeliveryItemType.itemGrossWeight = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemGrossWeight")
        }
        if !AInbDeliveryItemType.itemIsBillingRelevant.isRemoved {
            AInbDeliveryItemType.itemIsBillingRelevant = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemIsBillingRelevant")
        }
        if !AInbDeliveryItemType.itemNetWeight.isRemoved {
            AInbDeliveryItemType.itemNetWeight = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemNetWeight")
        }
        if !AInbDeliveryItemType.itemPackingIncompletionStatus.isRemoved {
            AInbDeliveryItemType.itemPackingIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemPackingIncompletionStatus")
        }
        if !AInbDeliveryItemType.itemPickingIncompletionStatus.isRemoved {
            AInbDeliveryItemType.itemPickingIncompletionStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemPickingIncompletionStatus")
        }
        if !AInbDeliveryItemType.itemVolume.isRemoved {
            AInbDeliveryItemType.itemVolume = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemVolume")
        }
        if !AInbDeliveryItemType.itemVolumeUnit.isRemoved {
            AInbDeliveryItemType.itemVolumeUnit = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemVolumeUnit")
        }
        if !AInbDeliveryItemType.itemWeightUnit.isRemoved {
            AInbDeliveryItemType.itemWeightUnit = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ItemWeightUnit")
        }
        if !AInbDeliveryItemType.lastChangeDate.isRemoved {
            AInbDeliveryItemType.lastChangeDate = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "LastChangeDate")
        }
        if !AInbDeliveryItemType.loadingGroup.isRemoved {
            AInbDeliveryItemType.loadingGroup = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "LoadingGroup")
        }
        if !AInbDeliveryItemType.manufactureDate.isRemoved {
            AInbDeliveryItemType.manufactureDate = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ManufactureDate")
        }
        if !AInbDeliveryItemType.material.isRemoved {
            AInbDeliveryItemType.material = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Material")
        }
        if !AInbDeliveryItemType.materialByCustomer.isRemoved {
            AInbDeliveryItemType.materialByCustomer = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialByCustomer")
        }
        if !AInbDeliveryItemType.materialFreightGroup.isRemoved {
            AInbDeliveryItemType.materialFreightGroup = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialFreightGroup")
        }
        if !AInbDeliveryItemType.materialGroup.isRemoved {
            AInbDeliveryItemType.materialGroup = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialGroup")
        }
        if !AInbDeliveryItemType.materialIsBatchManaged.isRemoved {
            AInbDeliveryItemType.materialIsBatchManaged = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialIsBatchManaged")
        }
        if !AInbDeliveryItemType.materialIsIntBatchManaged.isRemoved {
            AInbDeliveryItemType.materialIsIntBatchManaged = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "MaterialIsIntBatchManaged")
        }
        if !AInbDeliveryItemType.numberOfSerialNumbers.isRemoved {
            AInbDeliveryItemType.numberOfSerialNumbers = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "NumberOfSerialNumbers")
        }
        if !AInbDeliveryItemType.orderID.isRemoved {
            AInbDeliveryItemType.orderID = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OrderID")
        }
        if !AInbDeliveryItemType.orderItem.isRemoved {
            AInbDeliveryItemType.orderItem = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OrderItem")
        }
        if !AInbDeliveryItemType.originalDeliveryQuantity.isRemoved {
            AInbDeliveryItemType.originalDeliveryQuantity = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OriginalDeliveryQuantity")
        }
        if !AInbDeliveryItemType.originallyRequestedMaterial.isRemoved {
            AInbDeliveryItemType.originallyRequestedMaterial = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OriginallyRequestedMaterial")
        }
        if !AInbDeliveryItemType.overdelivTolrtdLmtRatioInPct.isRemoved {
            AInbDeliveryItemType.overdelivTolrtdLmtRatioInPct = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "OverdelivTolrtdLmtRatioInPct")
        }
        if !AInbDeliveryItemType.packingStatus.isRemoved {
            AInbDeliveryItemType.packingStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PackingStatus")
        }
        if !AInbDeliveryItemType.partialDeliveryIsAllowed.isRemoved {
            AInbDeliveryItemType.partialDeliveryIsAllowed = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PartialDeliveryIsAllowed")
        }
        if !AInbDeliveryItemType.paymentGuaranteeForm.isRemoved {
            AInbDeliveryItemType.paymentGuaranteeForm = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PaymentGuaranteeForm")
        }
        if !AInbDeliveryItemType.pickingConfirmationStatus.isRemoved {
            AInbDeliveryItemType.pickingConfirmationStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PickingConfirmationStatus")
        }
        if !AInbDeliveryItemType.pickingControl.isRemoved {
            AInbDeliveryItemType.pickingControl = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PickingControl")
        }
        if !AInbDeliveryItemType.pickingStatus.isRemoved {
            AInbDeliveryItemType.pickingStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PickingStatus")
        }
        if !AInbDeliveryItemType.plant.isRemoved {
            AInbDeliveryItemType.plant = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Plant")
        }
        if !AInbDeliveryItemType.primaryPostingSwitch.isRemoved {
            AInbDeliveryItemType.primaryPostingSwitch = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "PrimaryPostingSwitch")
        }
        if !AInbDeliveryItemType.productAvailabilityDate.isRemoved {
            AInbDeliveryItemType.productAvailabilityDate = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProductAvailabilityDate")
        }
        if !AInbDeliveryItemType.productAvailabilityTime.isRemoved {
            AInbDeliveryItemType.productAvailabilityTime = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProductAvailabilityTime")
        }
        if !AInbDeliveryItemType.productConfiguration.isRemoved {
            AInbDeliveryItemType.productConfiguration = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProductConfiguration")
        }
        if !AInbDeliveryItemType.productHierarchyNode.isRemoved {
            AInbDeliveryItemType.productHierarchyNode = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProductHierarchyNode")
        }
        if !AInbDeliveryItemType.profitabilitySegment.isRemoved {
            AInbDeliveryItemType.profitabilitySegment = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProfitabilitySegment")
        }
        if !AInbDeliveryItemType.profitCenter.isRemoved {
            AInbDeliveryItemType.profitCenter = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProfitCenter")
        }
        if !AInbDeliveryItemType.proofOfDeliveryRelevanceCode.isRemoved {
            AInbDeliveryItemType.proofOfDeliveryRelevanceCode = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProofOfDeliveryRelevanceCode")
        }
        if !AInbDeliveryItemType.proofOfDeliveryStatus.isRemoved {
            AInbDeliveryItemType.proofOfDeliveryStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ProofOfDeliveryStatus")
        }
        if !AInbDeliveryItemType.quantityIsFixed.isRemoved {
            AInbDeliveryItemType.quantityIsFixed = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "QuantityIsFixed")
        }
        if !AInbDeliveryItemType.receivingPoint.isRemoved {
            AInbDeliveryItemType.receivingPoint = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReceivingPoint")
        }
        if !AInbDeliveryItemType.referenceDocumentLogicalSystem.isRemoved {
            AInbDeliveryItemType.referenceDocumentLogicalSystem = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReferenceDocumentLogicalSystem")
        }
        if !AInbDeliveryItemType.referenceSDDocument.isRemoved {
            AInbDeliveryItemType.referenceSDDocument = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReferenceSDDocument")
        }
        if !AInbDeliveryItemType.referenceSDDocumentCategory.isRemoved {
            AInbDeliveryItemType.referenceSDDocumentCategory = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReferenceSDDocumentCategory")
        }
        if !AInbDeliveryItemType.referenceSDDocumentItem.isRemoved {
            AInbDeliveryItemType.referenceSDDocumentItem = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ReferenceSDDocumentItem")
        }
        if !AInbDeliveryItemType.retailPromotion.isRemoved {
            AInbDeliveryItemType.retailPromotion = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "RetailPromotion")
        }
        if !AInbDeliveryItemType.salesDocumentItemType.isRemoved {
            AInbDeliveryItemType.salesDocumentItemType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SalesDocumentItemType")
        }
        if !AInbDeliveryItemType.salesGroup.isRemoved {
            AInbDeliveryItemType.salesGroup = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SalesGroup")
        }
        if !AInbDeliveryItemType.salesOffice.isRemoved {
            AInbDeliveryItemType.salesOffice = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SalesOffice")
        }
        if !AInbDeliveryItemType.sdDocumentCategory.isRemoved {
            AInbDeliveryItemType.sdDocumentCategory = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SDDocumentCategory")
        }
        if !AInbDeliveryItemType.sdProcessStatus.isRemoved {
            AInbDeliveryItemType.sdProcessStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SDProcessStatus")
        }
        if !AInbDeliveryItemType.shelfLifeExpirationDate.isRemoved {
            AInbDeliveryItemType.shelfLifeExpirationDate = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "ShelfLifeExpirationDate")
        }
        if !AInbDeliveryItemType.statisticsDate.isRemoved {
            AInbDeliveryItemType.statisticsDate = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StatisticsDate")
        }
        if !AInbDeliveryItemType.stockType.isRemoved {
            AInbDeliveryItemType.stockType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StockType")
        }
        if !AInbDeliveryItemType.storageBin.isRemoved {
            AInbDeliveryItemType.storageBin = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StorageBin")
        }
        if !AInbDeliveryItemType.storageLocation.isRemoved {
            AInbDeliveryItemType.storageLocation = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StorageLocation")
        }
        if !AInbDeliveryItemType.storageType.isRemoved {
            AInbDeliveryItemType.storageType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StorageType")
        }
        if !AInbDeliveryItemType.subsequentMovementType.isRemoved {
            AInbDeliveryItemType.subsequentMovementType = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "SubsequentMovementType")
        }
        if !AInbDeliveryItemType.transportationGroup.isRemoved {
            AInbDeliveryItemType.transportationGroup = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "TransportationGroup")
        }
        if !AInbDeliveryItemType.underdelivTolrtdLmtRatioInPct.isRemoved {
            AInbDeliveryItemType.underdelivTolrtdLmtRatioInPct = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "UnderdelivTolrtdLmtRatioInPct")
        }
        if !AInbDeliveryItemType.unlimitedOverdeliveryIsAllowed.isRemoved {
            AInbDeliveryItemType.unlimitedOverdeliveryIsAllowed = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "UnlimitedOverdeliveryIsAllowed")
        }
        if !AInbDeliveryItemType.varblShipgProcgDurationInDays.isRemoved {
            AInbDeliveryItemType.varblShipgProcgDurationInDays = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "VarblShipgProcgDurationInDays")
        }
        if !AInbDeliveryItemType.warehouse.isRemoved {
            AInbDeliveryItemType.warehouse = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "Warehouse")
        }
        if !AInbDeliveryItemType.warehouseActivityStatus.isRemoved {
            AInbDeliveryItemType.warehouseActivityStatus = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "WarehouseActivityStatus")
        }
        if !AInbDeliveryItemType.warehouseStagingArea.isRemoved {
            AInbDeliveryItemType.warehouseStagingArea = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "WarehouseStagingArea")
        }
        if !AInbDeliveryItemType.deliveryVersion.isRemoved {
            AInbDeliveryItemType.deliveryVersion = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "DeliveryVersion")
        }
        if !AInbDeliveryItemType.warehouseStockCategory.isRemoved {
            AInbDeliveryItemType.warehouseStockCategory = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "WarehouseStockCategory")
        }
        if !AInbDeliveryItemType.warehouseStorageBin.isRemoved {
            AInbDeliveryItemType.warehouseStorageBin = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "WarehouseStorageBin")
        }
        if !AInbDeliveryItemType.stockSegment.isRemoved {
            AInbDeliveryItemType.stockSegment = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "StockSegment")
        }
        if !AInbDeliveryItemType.requirementSegment.isRemoved {
            AInbDeliveryItemType.requirementSegment = Ec1Metadata.EntityTypes.aInbDeliveryItemType.property(withName: "RequirementSegment")
        }
        if !AInbDeliveryPartnerType.addressID.isRemoved {
            AInbDeliveryPartnerType.addressID = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "AddressID")
        }
        if !AInbDeliveryPartnerType.contactPerson.isRemoved {
            AInbDeliveryPartnerType.contactPerson = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "ContactPerson")
        }
        if !AInbDeliveryPartnerType.customer.isRemoved {
            AInbDeliveryPartnerType.customer = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "Customer")
        }
        if !AInbDeliveryPartnerType.partnerFunction.isRemoved {
            AInbDeliveryPartnerType.partnerFunction = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "PartnerFunction")
        }
        if !AInbDeliveryPartnerType.personnel.isRemoved {
            AInbDeliveryPartnerType.personnel = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "Personnel")
        }
        if !AInbDeliveryPartnerType.sdDocument.isRemoved {
            AInbDeliveryPartnerType.sdDocument = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "SDDocument")
        }
        if !AInbDeliveryPartnerType.sdDocumentItem.isRemoved {
            AInbDeliveryPartnerType.sdDocumentItem = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "SDDocumentItem")
        }
        if !AInbDeliveryPartnerType.supplier.isRemoved {
            AInbDeliveryPartnerType.supplier = Ec1Metadata.EntityTypes.aInbDeliveryPartnerType.property(withName: "Supplier")
        }
        if !AInbDeliverySerialNmbrType.deliveryDate.isRemoved {
            AInbDeliverySerialNmbrType.deliveryDate = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "DeliveryDate")
        }
        if !AInbDeliverySerialNmbrType.deliveryDocument.isRemoved {
            AInbDeliverySerialNmbrType.deliveryDocument = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "DeliveryDocument")
        }
        if !AInbDeliverySerialNmbrType.deliveryDocumentItem.isRemoved {
            AInbDeliverySerialNmbrType.deliveryDocumentItem = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "DeliveryDocumentItem")
        }
        if !AInbDeliverySerialNmbrType.maintenanceItemObjectList.isRemoved {
            AInbDeliverySerialNmbrType.maintenanceItemObjectList = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "MaintenanceItemObjectList")
        }
        if !AInbDeliverySerialNmbrType.sdDocumentCategory.isRemoved {
            AInbDeliverySerialNmbrType.sdDocumentCategory = Ec1Metadata.EntityTypes.aInbDeliverySerialNmbrType.property(withName: "SDDocumentCategory")
        }
        if !AMaintenanceItemObjListType.assembly.isRemoved {
            AMaintenanceItemObjListType.assembly = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "Assembly")
        }
        if !AMaintenanceItemObjListType.equipment.isRemoved {
            AMaintenanceItemObjListType.equipment = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "Equipment")
        }
        if !AMaintenanceItemObjListType.functionalLocation.isRemoved {
            AMaintenanceItemObjListType.functionalLocation = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "FunctionalLocation")
        }
        if !AMaintenanceItemObjListType.maintenanceItemObject.isRemoved {
            AMaintenanceItemObjListType.maintenanceItemObject = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "MaintenanceItemObject")
        }
        if !AMaintenanceItemObjListType.maintenanceItemObjectList.isRemoved {
            AMaintenanceItemObjListType.maintenanceItemObjectList = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "MaintenanceItemObjectList")
        }
        if !AMaintenanceItemObjListType.maintenanceNotification.isRemoved {
            AMaintenanceItemObjListType.maintenanceNotification = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "MaintenanceNotification")
        }
        if !AMaintenanceItemObjListType.maintObjectLocAcctAssgmtNmbr.isRemoved {
            AMaintenanceItemObjListType.maintObjectLocAcctAssgmtNmbr = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "MaintObjectLocAcctAssgmtNmbr")
        }
        if !AMaintenanceItemObjListType.material.isRemoved {
            AMaintenanceItemObjListType.material = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "Material")
        }
        if !AMaintenanceItemObjListType.serialNumber.isRemoved {
            AMaintenanceItemObjListType.serialNumber = Ec1Metadata.EntityTypes.aMaintenanceItemObjListType.property(withName: "SerialNumber")
        }
        if !AMaintenanceItemObjectType.assembly.isRemoved {
            AMaintenanceItemObjectType.assembly = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "Assembly")
        }
        if !AMaintenanceItemObjectType.equipment.isRemoved {
            AMaintenanceItemObjectType.equipment = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "Equipment")
        }
        if !AMaintenanceItemObjectType.functionalLocation.isRemoved {
            AMaintenanceItemObjectType.functionalLocation = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "FunctionalLocation")
        }
        if !AMaintenanceItemObjectType.maintenanceItemObject.isRemoved {
            AMaintenanceItemObjectType.maintenanceItemObject = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "MaintenanceItemObject")
        }
        if !AMaintenanceItemObjectType.maintenanceItemObjectList.isRemoved {
            AMaintenanceItemObjectType.maintenanceItemObjectList = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "MaintenanceItemObjectList")
        }
    }

    private static func merge2(metadata: CSDLDocument) {
        Ignore.valueOf_any(metadata)
        if !AMaintenanceItemObjectType.maintenanceNotification.isRemoved {
            AMaintenanceItemObjectType.maintenanceNotification = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "MaintenanceNotification")
        }
        if !AMaintenanceItemObjectType.maintObjectLocAcctAssgmtNmbr.isRemoved {
            AMaintenanceItemObjectType.maintObjectLocAcctAssgmtNmbr = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "MaintObjectLocAcctAssgmtNmbr")
        }
        if !AMaintenanceItemObjectType.material.isRemoved {
            AMaintenanceItemObjectType.material = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "Material")
        }
        if !AMaintenanceItemObjectType.serialNumber.isRemoved {
            AMaintenanceItemObjectType.serialNumber = Ec1Metadata.EntityTypes.aMaintenanceItemObjectType.property(withName: "SerialNumber")
        }
        if !AMaterialDocumentHeaderType.materialDocumentYear.isRemoved {
            AMaterialDocumentHeaderType.materialDocumentYear = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "MaterialDocumentYear")
        }
        if !AMaterialDocumentHeaderType.materialDocument.isRemoved {
            AMaterialDocumentHeaderType.materialDocument = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "MaterialDocument")
        }
        if !AMaterialDocumentHeaderType.inventoryTransactionType.isRemoved {
            AMaterialDocumentHeaderType.inventoryTransactionType = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "InventoryTransactionType")
        }
        if !AMaterialDocumentHeaderType.documentDate.isRemoved {
            AMaterialDocumentHeaderType.documentDate = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "DocumentDate")
        }
        if !AMaterialDocumentHeaderType.postingDate.isRemoved {
            AMaterialDocumentHeaderType.postingDate = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "PostingDate")
        }
        if !AMaterialDocumentHeaderType.creationDate.isRemoved {
            AMaterialDocumentHeaderType.creationDate = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "CreationDate")
        }
        if !AMaterialDocumentHeaderType.creationTime.isRemoved {
            AMaterialDocumentHeaderType.creationTime = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "CreationTime")
        }
        if !AMaterialDocumentHeaderType.createdByUser.isRemoved {
            AMaterialDocumentHeaderType.createdByUser = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "CreatedByUser")
        }
        if !AMaterialDocumentHeaderType.materialDocumentHeaderText.isRemoved {
            AMaterialDocumentHeaderType.materialDocumentHeaderText = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "MaterialDocumentHeaderText")
        }
        if !AMaterialDocumentHeaderType.referenceDocument.isRemoved {
            AMaterialDocumentHeaderType.referenceDocument = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "ReferenceDocument")
        }
        if !AMaterialDocumentHeaderType.goodsMovementCode.isRemoved {
            AMaterialDocumentHeaderType.goodsMovementCode = Ec1Metadata.EntityTypes.aMaterialDocumentHeaderType.property(withName: "GoodsMovementCode")
        }
        if !AMaterialDocumentItemType.materialDocumentYear.isRemoved {
            AMaterialDocumentItemType.materialDocumentYear = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentYear")
        }
        if !AMaterialDocumentItemType.materialDocument.isRemoved {
            AMaterialDocumentItemType.materialDocument = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocument")
        }
        if !AMaterialDocumentItemType.materialDocumentItem.isRemoved {
            AMaterialDocumentItemType.materialDocumentItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentItem")
        }
        if !AMaterialDocumentItemType.material.isRemoved {
            AMaterialDocumentItemType.material = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Material")
        }
        if !AMaterialDocumentItemType.plant.isRemoved {
            AMaterialDocumentItemType.plant = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Plant")
        }
        if !AMaterialDocumentItemType.storageLocation.isRemoved {
            AMaterialDocumentItemType.storageLocation = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "StorageLocation")
        }
        if !AMaterialDocumentItemType.batch.isRemoved {
            AMaterialDocumentItemType.batch = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Batch")
        }
        if !AMaterialDocumentItemType.goodsMovementType.isRemoved {
            AMaterialDocumentItemType.goodsMovementType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GoodsMovementType")
        }
        if !AMaterialDocumentItemType.inventoryStockType.isRemoved {
            AMaterialDocumentItemType.inventoryStockType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "InventoryStockType")
        }
        if !AMaterialDocumentItemType.inventoryValuationType.isRemoved {
            AMaterialDocumentItemType.inventoryValuationType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "InventoryValuationType")
        }
        if !AMaterialDocumentItemType.inventorySpecialStockType.isRemoved {
            AMaterialDocumentItemType.inventorySpecialStockType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "InventorySpecialStockType")
        }
        if !AMaterialDocumentItemType.supplier.isRemoved {
            AMaterialDocumentItemType.supplier = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Supplier")
        }
        if !AMaterialDocumentItemType.customer.isRemoved {
            AMaterialDocumentItemType.customer = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Customer")
        }
        if !AMaterialDocumentItemType.salesOrder.isRemoved {
            AMaterialDocumentItemType.salesOrder = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SalesOrder")
        }
        if !AMaterialDocumentItemType.salesOrderItem.isRemoved {
            AMaterialDocumentItemType.salesOrderItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SalesOrderItem")
        }
        if !AMaterialDocumentItemType.salesOrderScheduleLine.isRemoved {
            AMaterialDocumentItemType.salesOrderScheduleLine = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SalesOrderScheduleLine")
        }
        if !AMaterialDocumentItemType.purchaseOrder.isRemoved {
            AMaterialDocumentItemType.purchaseOrder = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "PurchaseOrder")
        }
        if !AMaterialDocumentItemType.purchaseOrderItem.isRemoved {
            AMaterialDocumentItemType.purchaseOrderItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "PurchaseOrderItem")
        }
        if !AMaterialDocumentItemType.wbsElement.isRemoved {
            AMaterialDocumentItemType.wbsElement = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "WBSElement")
        }
        if !AMaterialDocumentItemType.manufacturingOrder.isRemoved {
            AMaterialDocumentItemType.manufacturingOrder = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ManufacturingOrder")
        }
        if !AMaterialDocumentItemType.manufacturingOrderItem.isRemoved {
            AMaterialDocumentItemType.manufacturingOrderItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ManufacturingOrderItem")
        }
        if !AMaterialDocumentItemType.goodsMovementRefDocType.isRemoved {
            AMaterialDocumentItemType.goodsMovementRefDocType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GoodsMovementRefDocType")
        }
        if !AMaterialDocumentItemType.goodsMovementReasonCode.isRemoved {
            AMaterialDocumentItemType.goodsMovementReasonCode = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GoodsMovementReasonCode")
        }
        if !AMaterialDocumentItemType.accountAssignmentCategory.isRemoved {
            AMaterialDocumentItemType.accountAssignmentCategory = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "AccountAssignmentCategory")
        }
        if !AMaterialDocumentItemType.costCenter.isRemoved {
            AMaterialDocumentItemType.costCenter = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "CostCenter")
        }
        if !AMaterialDocumentItemType.controllingArea.isRemoved {
            AMaterialDocumentItemType.controllingArea = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ControllingArea")
        }
        if !AMaterialDocumentItemType.costObject.isRemoved {
            AMaterialDocumentItemType.costObject = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "CostObject")
        }
        if !AMaterialDocumentItemType.profitabilitySegment.isRemoved {
            AMaterialDocumentItemType.profitabilitySegment = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ProfitabilitySegment")
        }
        if !AMaterialDocumentItemType.profitCenter.isRemoved {
            AMaterialDocumentItemType.profitCenter = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ProfitCenter")
        }
        if !AMaterialDocumentItemType.glAccount.isRemoved {
            AMaterialDocumentItemType.glAccount = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GLAccount")
        }
        if !AMaterialDocumentItemType.functionalArea.isRemoved {
            AMaterialDocumentItemType.functionalArea = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "FunctionalArea")
        }
        if !AMaterialDocumentItemType.materialBaseUnit.isRemoved {
            AMaterialDocumentItemType.materialBaseUnit = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialBaseUnit")
        }
        if !AMaterialDocumentItemType.quantityInBaseUnit.isRemoved {
            AMaterialDocumentItemType.quantityInBaseUnit = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "QuantityInBaseUnit")
        }
        if !AMaterialDocumentItemType.entryUnit.isRemoved {
            AMaterialDocumentItemType.entryUnit = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "EntryUnit")
        }
        if !AMaterialDocumentItemType.quantityInEntryUnit.isRemoved {
            AMaterialDocumentItemType.quantityInEntryUnit = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "QuantityInEntryUnit")
        }
        if !AMaterialDocumentItemType.companyCodeCurrency.isRemoved {
            AMaterialDocumentItemType.companyCodeCurrency = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "CompanyCodeCurrency")
        }
        if !AMaterialDocumentItemType.gdsMvtExtAmtInCoCodeCrcy.isRemoved {
            AMaterialDocumentItemType.gdsMvtExtAmtInCoCodeCrcy = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GdsMvtExtAmtInCoCodeCrcy")
        }
        if !AMaterialDocumentItemType.slsPrcAmtInclVATInCoCodeCrcy.isRemoved {
            AMaterialDocumentItemType.slsPrcAmtInclVATInCoCodeCrcy = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SlsPrcAmtInclVATInCoCodeCrcy")
        }
        if !AMaterialDocumentItemType.fiscalYear.isRemoved {
            AMaterialDocumentItemType.fiscalYear = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "FiscalYear")
        }
        if !AMaterialDocumentItemType.fiscalYearPeriod.isRemoved {
            AMaterialDocumentItemType.fiscalYearPeriod = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "FiscalYearPeriod")
        }
        if !AMaterialDocumentItemType.fiscalYearVariant.isRemoved {
            AMaterialDocumentItemType.fiscalYearVariant = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "FiscalYearVariant")
        }
        if !AMaterialDocumentItemType.issgOrRcvgMaterial.isRemoved {
            AMaterialDocumentItemType.issgOrRcvgMaterial = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssgOrRcvgMaterial")
        }
        if !AMaterialDocumentItemType.issgOrRcvgBatch.isRemoved {
            AMaterialDocumentItemType.issgOrRcvgBatch = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssgOrRcvgBatch")
        }
        if !AMaterialDocumentItemType.issuingOrReceivingPlant.isRemoved {
            AMaterialDocumentItemType.issuingOrReceivingPlant = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssuingOrReceivingPlant")
        }
        if !AMaterialDocumentItemType.issuingOrReceivingStorageLoc.isRemoved {
            AMaterialDocumentItemType.issuingOrReceivingStorageLoc = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssuingOrReceivingStorageLoc")
        }
        if !AMaterialDocumentItemType.issuingOrReceivingStockType.isRemoved {
            AMaterialDocumentItemType.issuingOrReceivingStockType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssuingOrReceivingStockType")
        }
        if !AMaterialDocumentItemType.issgOrRcvgSpclStockInd.isRemoved {
            AMaterialDocumentItemType.issgOrRcvgSpclStockInd = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssgOrRcvgSpclStockInd")
        }
        if !AMaterialDocumentItemType.issuingOrReceivingValType.isRemoved {
            AMaterialDocumentItemType.issuingOrReceivingValType = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IssuingOrReceivingValType")
        }
        if !AMaterialDocumentItemType.isCompletelyDelivered.isRemoved {
            AMaterialDocumentItemType.isCompletelyDelivered = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IsCompletelyDelivered")
        }
        if !AMaterialDocumentItemType.materialDocumentItemText.isRemoved {
            AMaterialDocumentItemType.materialDocumentItemText = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentItemText")
        }
        if !AMaterialDocumentItemType.unloadingPointName.isRemoved {
            AMaterialDocumentItemType.unloadingPointName = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "UnloadingPointName")
        }
        if !AMaterialDocumentItemType.shelfLifeExpirationDate.isRemoved {
            AMaterialDocumentItemType.shelfLifeExpirationDate = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ShelfLifeExpirationDate")
        }
        if !AMaterialDocumentItemType.manufactureDate.isRemoved {
            AMaterialDocumentItemType.manufactureDate = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ManufactureDate")
        }
        if !AMaterialDocumentItemType.reservation.isRemoved {
            AMaterialDocumentItemType.reservation = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "Reservation")
        }
        if !AMaterialDocumentItemType.reservationItem.isRemoved {
            AMaterialDocumentItemType.reservationItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReservationItem")
        }
        if !AMaterialDocumentItemType.reservationIsFinallyIssued.isRemoved {
            AMaterialDocumentItemType.reservationIsFinallyIssued = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReservationIsFinallyIssued")
        }
        if !AMaterialDocumentItemType.specialStockIdfgSalesOrder.isRemoved {
            AMaterialDocumentItemType.specialStockIdfgSalesOrder = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SpecialStockIdfgSalesOrder")
        }
        if !AMaterialDocumentItemType.specialStockIdfgSalesOrderItem.isRemoved {
            AMaterialDocumentItemType.specialStockIdfgSalesOrderItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SpecialStockIdfgSalesOrderItem")
        }
        if !AMaterialDocumentItemType.specialStockIdfgWBSElement.isRemoved {
            AMaterialDocumentItemType.specialStockIdfgWBSElement = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "SpecialStockIdfgWBSElement")
        }
        if !AMaterialDocumentItemType.isAutomaticallyCreated.isRemoved {
            AMaterialDocumentItemType.isAutomaticallyCreated = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "IsAutomaticallyCreated")
        }
        if !AMaterialDocumentItemType.materialDocumentLine.isRemoved {
            AMaterialDocumentItemType.materialDocumentLine = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentLine")
        }
        if !AMaterialDocumentItemType.materialDocumentParentLine.isRemoved {
            AMaterialDocumentItemType.materialDocumentParentLine = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "MaterialDocumentParentLine")
        }
        if !AMaterialDocumentItemType.hierarchyNodeLevel.isRemoved {
            AMaterialDocumentItemType.hierarchyNodeLevel = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "HierarchyNodeLevel")
        }
        if !AMaterialDocumentItemType.goodsMovementIsCancelled.isRemoved {
            AMaterialDocumentItemType.goodsMovementIsCancelled = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "GoodsMovementIsCancelled")
        }
        if !AMaterialDocumentItemType.reversedMaterialDocumentYear.isRemoved {
            AMaterialDocumentItemType.reversedMaterialDocumentYear = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReversedMaterialDocumentYear")
        }
        if !AMaterialDocumentItemType.reversedMaterialDocument.isRemoved {
            AMaterialDocumentItemType.reversedMaterialDocument = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReversedMaterialDocument")
        }
        if !AMaterialDocumentItemType.reversedMaterialDocumentItem.isRemoved {
            AMaterialDocumentItemType.reversedMaterialDocumentItem = Ec1Metadata.EntityTypes.aMaterialDocumentItemType.property(withName: "ReversedMaterialDocumentItem")
        }
        if !AOutbDeliveryAddressType.additionalStreetPrefixName.isRemoved {
            AOutbDeliveryAddressType.additionalStreetPrefixName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "AdditionalStreetPrefixName")
        }
        if !AOutbDeliveryAddressType.additionalStreetSuffixName.isRemoved {
            AOutbDeliveryAddressType.additionalStreetSuffixName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "AdditionalStreetSuffixName")
        }
        if !AOutbDeliveryAddressType.addressID.isRemoved {
            AOutbDeliveryAddressType.addressID = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "AddressID")
        }
        if !AOutbDeliveryAddressType.addressTimeZone.isRemoved {
            AOutbDeliveryAddressType.addressTimeZone = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "AddressTimeZone")
        }
        if !AOutbDeliveryAddressType.building.isRemoved {
            AOutbDeliveryAddressType.building = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Building")
        }
        if !AOutbDeliveryAddressType.businessPartnerName1.isRemoved {
            AOutbDeliveryAddressType.businessPartnerName1 = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "BusinessPartnerName1")
        }
        if !AOutbDeliveryAddressType.businessPartnerName2.isRemoved {
            AOutbDeliveryAddressType.businessPartnerName2 = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "BusinessPartnerName2")
        }
        if !AOutbDeliveryAddressType.businessPartnerName3.isRemoved {
            AOutbDeliveryAddressType.businessPartnerName3 = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "BusinessPartnerName3")
        }
        if !AOutbDeliveryAddressType.businessPartnerName4.isRemoved {
            AOutbDeliveryAddressType.businessPartnerName4 = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "BusinessPartnerName4")
        }
        if !AOutbDeliveryAddressType.careOfName.isRemoved {
            AOutbDeliveryAddressType.careOfName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CareOfName")
        }
        if !AOutbDeliveryAddressType.cityCode.isRemoved {
            AOutbDeliveryAddressType.cityCode = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CityCode")
        }
        if !AOutbDeliveryAddressType.cityName.isRemoved {
            AOutbDeliveryAddressType.cityName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CityName")
        }
        if !AOutbDeliveryAddressType.citySearch.isRemoved {
            AOutbDeliveryAddressType.citySearch = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CitySearch")
        }
        if !AOutbDeliveryAddressType.companyPostalCode.isRemoved {
            AOutbDeliveryAddressType.companyPostalCode = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CompanyPostalCode")
        }
        if !AOutbDeliveryAddressType.correspondenceLanguage.isRemoved {
            AOutbDeliveryAddressType.correspondenceLanguage = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "CorrespondenceLanguage")
        }
        if !AOutbDeliveryAddressType.country.isRemoved {
            AOutbDeliveryAddressType.country = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Country")
        }
        if !AOutbDeliveryAddressType.county.isRemoved {
            AOutbDeliveryAddressType.county = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "County")
        }
        if !AOutbDeliveryAddressType.deliveryServiceNumber.isRemoved {
            AOutbDeliveryAddressType.deliveryServiceNumber = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "DeliveryServiceNumber")
        }
        if !AOutbDeliveryAddressType.deliveryServiceTypeCode.isRemoved {
            AOutbDeliveryAddressType.deliveryServiceTypeCode = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "DeliveryServiceTypeCode")
        }
        if !AOutbDeliveryAddressType.district.isRemoved {
            AOutbDeliveryAddressType.district = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "District")
        }
        if !AOutbDeliveryAddressType.faxNumber.isRemoved {
            AOutbDeliveryAddressType.faxNumber = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "FaxNumber")
        }
        if !AOutbDeliveryAddressType.floor.isRemoved {
            AOutbDeliveryAddressType.floor = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Floor")
        }
        if !AOutbDeliveryAddressType.formOfAddress.isRemoved {
            AOutbDeliveryAddressType.formOfAddress = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "FormOfAddress")
        }
        if !AOutbDeliveryAddressType.fullName.isRemoved {
            AOutbDeliveryAddressType.fullName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "FullName")
        }
        if !AOutbDeliveryAddressType.homeCityName.isRemoved {
            AOutbDeliveryAddressType.homeCityName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "HomeCityName")
        }
        if !AOutbDeliveryAddressType.houseNumber.isRemoved {
            AOutbDeliveryAddressType.houseNumber = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "HouseNumber")
        }
        if !AOutbDeliveryAddressType.houseNumberSupplementText.isRemoved {
            AOutbDeliveryAddressType.houseNumberSupplementText = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "HouseNumberSupplementText")
        }
        if !AOutbDeliveryAddressType.nation.isRemoved {
            AOutbDeliveryAddressType.nation = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Nation")
        }
        if !AOutbDeliveryAddressType.person.isRemoved {
            AOutbDeliveryAddressType.person = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Person")
        }
        if !AOutbDeliveryAddressType.phoneNumber.isRemoved {
            AOutbDeliveryAddressType.phoneNumber = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "PhoneNumber")
        }
        if !AOutbDeliveryAddressType.poBox.isRemoved {
            AOutbDeliveryAddressType.poBox = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBox")
        }
        if !AOutbDeliveryAddressType.poBoxDeviatingCityName.isRemoved {
            AOutbDeliveryAddressType.poBoxDeviatingCityName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxDeviatingCityName")
        }
        if !AOutbDeliveryAddressType.poBoxDeviatingCountry.isRemoved {
            AOutbDeliveryAddressType.poBoxDeviatingCountry = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxDeviatingCountry")
        }
        if !AOutbDeliveryAddressType.poBoxDeviatingRegion.isRemoved {
            AOutbDeliveryAddressType.poBoxDeviatingRegion = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxDeviatingRegion")
        }
        if !AOutbDeliveryAddressType.poBoxIsWithoutNumber.isRemoved {
            AOutbDeliveryAddressType.poBoxIsWithoutNumber = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxIsWithoutNumber")
        }
        if !AOutbDeliveryAddressType.poBoxLobbyName.isRemoved {
            AOutbDeliveryAddressType.poBoxLobbyName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxLobbyName")
        }
        if !AOutbDeliveryAddressType.poBoxPostalCode.isRemoved {
            AOutbDeliveryAddressType.poBoxPostalCode = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "POBoxPostalCode")
        }
        if !AOutbDeliveryAddressType.postalCode.isRemoved {
            AOutbDeliveryAddressType.postalCode = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "PostalCode")
        }
        if !AOutbDeliveryAddressType.prfrdCommMediumType.isRemoved {
            AOutbDeliveryAddressType.prfrdCommMediumType = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "PrfrdCommMediumType")
        }
        if !AOutbDeliveryAddressType.region.isRemoved {
            AOutbDeliveryAddressType.region = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "Region")
        }
        if !AOutbDeliveryAddressType.roomNumber.isRemoved {
            AOutbDeliveryAddressType.roomNumber = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "RoomNumber")
        }
        if !AOutbDeliveryAddressType.searchTerm1.isRemoved {
            AOutbDeliveryAddressType.searchTerm1 = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "SearchTerm1")
        }
        if !AOutbDeliveryAddressType.streetName.isRemoved {
            AOutbDeliveryAddressType.streetName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "StreetName")
        }
        if !AOutbDeliveryAddressType.streetPrefixName.isRemoved {
            AOutbDeliveryAddressType.streetPrefixName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "StreetPrefixName")
        }
        if !AOutbDeliveryAddressType.streetSearch.isRemoved {
            AOutbDeliveryAddressType.streetSearch = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "StreetSearch")
        }
        if !AOutbDeliveryAddressType.streetSuffixName.isRemoved {
            AOutbDeliveryAddressType.streetSuffixName = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "StreetSuffixName")
        }
        if !AOutbDeliveryAddressType.taxJurisdiction.isRemoved {
            AOutbDeliveryAddressType.taxJurisdiction = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "TaxJurisdiction")
        }
        if !AOutbDeliveryAddressType.transportZone.isRemoved {
            AOutbDeliveryAddressType.transportZone = Ec1Metadata.EntityTypes.aOutbDeliveryAddressType.property(withName: "TransportZone")
        }
        if !AOutbDeliveryDocFlowType.deliveryversion.isRemoved {
            AOutbDeliveryDocFlowType.deliveryversion = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "Deliveryversion")
        }
        if !AOutbDeliveryDocFlowType.precedingDocument.isRemoved {
            AOutbDeliveryDocFlowType.precedingDocument = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "PrecedingDocument")
        }
        if !AOutbDeliveryDocFlowType.precedingDocumentCategory.isRemoved {
            AOutbDeliveryDocFlowType.precedingDocumentCategory = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "PrecedingDocumentCategory")
        }
        if !AOutbDeliveryDocFlowType.precedingDocumentItem.isRemoved {
            AOutbDeliveryDocFlowType.precedingDocumentItem = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "PrecedingDocumentItem")
        }
        if !AOutbDeliveryDocFlowType.subsequentdocument.isRemoved {
            AOutbDeliveryDocFlowType.subsequentdocument = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "Subsequentdocument")
        }
        if !AOutbDeliveryDocFlowType.quantityInBaseUnit.isRemoved {
            AOutbDeliveryDocFlowType.quantityInBaseUnit = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "QuantityInBaseUnit")
        }
        if !AOutbDeliveryDocFlowType.subsequentDocumentItem.isRemoved {
            AOutbDeliveryDocFlowType.subsequentDocumentItem = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "SubsequentDocumentItem")
        }
        if !AOutbDeliveryDocFlowType.sdFulfillmentCalculationRule.isRemoved {
            AOutbDeliveryDocFlowType.sdFulfillmentCalculationRule = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "SDFulfillmentCalculationRule")
        }
        if !AOutbDeliveryDocFlowType.subsequentDocumentCategory.isRemoved {
            AOutbDeliveryDocFlowType.subsequentDocumentCategory = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "SubsequentDocumentCategory")
        }
        if !AOutbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd.isRemoved {
            AOutbDeliveryDocFlowType.transferOrderInWrhsMgmtIsConfd = Ec1Metadata.EntityTypes.aOutbDeliveryDocFlowType.property(withName: "TransferOrderInWrhsMgmtIsConfd")
        }
        if !AOutbDeliveryHeaderTextType.deliveryDocument.isRemoved {
            AOutbDeliveryHeaderTextType.deliveryDocument = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType.property(withName: "DeliveryDocument")
        }
        if !AOutbDeliveryHeaderTextType.textElement.isRemoved {
            AOutbDeliveryHeaderTextType.textElement = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType.property(withName: "TextElement")
        }
        if !AOutbDeliveryHeaderTextType.language.isRemoved {
            AOutbDeliveryHeaderTextType.language = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType.property(withName: "Language")
        }
        if !AOutbDeliveryHeaderTextType.textElementDescription.isRemoved {
            AOutbDeliveryHeaderTextType.textElementDescription = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType.property(withName: "TextElementDescription")
        }
        if !AOutbDeliveryHeaderTextType.textElementText.isRemoved {
            AOutbDeliveryHeaderTextType.textElementText = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderTextType.property(withName: "TextElementText")
        }
        if !AOutbDeliveryHeaderType.actualDeliveryRoute.isRemoved {
            AOutbDeliveryHeaderType.actualDeliveryRoute = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ActualDeliveryRoute")
        }
        if !AOutbDeliveryHeaderType.shippinglocationtimezone.isRemoved {
            AOutbDeliveryHeaderType.shippinglocationtimezone = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "Shippinglocationtimezone")
        }
        if !AOutbDeliveryHeaderType.receivinglocationtimezone.isRemoved {
            AOutbDeliveryHeaderType.receivinglocationtimezone = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "Receivinglocationtimezone")
        }
        if !AOutbDeliveryHeaderType.actualGoodsMovementDate.isRemoved {
            AOutbDeliveryHeaderType.actualGoodsMovementDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ActualGoodsMovementDate")
        }
        if !AOutbDeliveryHeaderType.actualGoodsMovementTime.isRemoved {
            AOutbDeliveryHeaderType.actualGoodsMovementTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ActualGoodsMovementTime")
        }
        if !AOutbDeliveryHeaderType.billingDocumentDate.isRemoved {
            AOutbDeliveryHeaderType.billingDocumentDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "BillingDocumentDate")
        }
        if !AOutbDeliveryHeaderType.billOfLading.isRemoved {
            AOutbDeliveryHeaderType.billOfLading = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "BillOfLading")
        }
        if !AOutbDeliveryHeaderType.completeDeliveryIsDefined.isRemoved {
            AOutbDeliveryHeaderType.completeDeliveryIsDefined = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CompleteDeliveryIsDefined")
        }
        if !AOutbDeliveryHeaderType.confirmationTime.isRemoved {
            AOutbDeliveryHeaderType.confirmationTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ConfirmationTime")
        }
        if !AOutbDeliveryHeaderType.createdByUser.isRemoved {
            AOutbDeliveryHeaderType.createdByUser = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CreatedByUser")
        }
        if !AOutbDeliveryHeaderType.creationDate.isRemoved {
            AOutbDeliveryHeaderType.creationDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CreationDate")
        }
        if !AOutbDeliveryHeaderType.creationTime.isRemoved {
            AOutbDeliveryHeaderType.creationTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CreationTime")
        }
        if !AOutbDeliveryHeaderType.customerGroup.isRemoved {
            AOutbDeliveryHeaderType.customerGroup = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "CustomerGroup")
        }
        if !AOutbDeliveryHeaderType.deliveryBlockReason.isRemoved {
            AOutbDeliveryHeaderType.deliveryBlockReason = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryBlockReason")
        }
        if !AOutbDeliveryHeaderType.deliveryDate.isRemoved {
            AOutbDeliveryHeaderType.deliveryDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryDate")
        }
        if !AOutbDeliveryHeaderType.deliveryDocument.isRemoved {
            AOutbDeliveryHeaderType.deliveryDocument = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryDocument")
        }
        if !AOutbDeliveryHeaderType.deliveryDocumentBySupplier.isRemoved {
            AOutbDeliveryHeaderType.deliveryDocumentBySupplier = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryDocumentBySupplier")
        }
        if !AOutbDeliveryHeaderType.deliveryDocumentType.isRemoved {
            AOutbDeliveryHeaderType.deliveryDocumentType = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryDocumentType")
        }
        if !AOutbDeliveryHeaderType.deliveryIsInPlant.isRemoved {
            AOutbDeliveryHeaderType.deliveryIsInPlant = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryIsInPlant")
        }
        if !AOutbDeliveryHeaderType.deliveryPriority.isRemoved {
            AOutbDeliveryHeaderType.deliveryPriority = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryPriority")
        }
        if !AOutbDeliveryHeaderType.deliveryTime.isRemoved {
            AOutbDeliveryHeaderType.deliveryTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryTime")
        }
        if !AOutbDeliveryHeaderType.deliveryVersion.isRemoved {
            AOutbDeliveryHeaderType.deliveryVersion = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DeliveryVersion")
        }
        if !AOutbDeliveryHeaderType.depreciationPercentage.isRemoved {
            AOutbDeliveryHeaderType.depreciationPercentage = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DepreciationPercentage")
        }
        if !AOutbDeliveryHeaderType.distrStatusByDecentralizedWrhs.isRemoved {
            AOutbDeliveryHeaderType.distrStatusByDecentralizedWrhs = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DistrStatusByDecentralizedWrhs")
        }
        if !AOutbDeliveryHeaderType.documentDate.isRemoved {
            AOutbDeliveryHeaderType.documentDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "DocumentDate")
        }
        if !AOutbDeliveryHeaderType.externalIdentificationType.isRemoved {
            AOutbDeliveryHeaderType.externalIdentificationType = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ExternalIdentificationType")
        }
        if !AOutbDeliveryHeaderType.externalTransportSystem.isRemoved {
            AOutbDeliveryHeaderType.externalTransportSystem = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ExternalTransportSystem")
        }
        if !AOutbDeliveryHeaderType.factoryCalendarByCustomer.isRemoved {
            AOutbDeliveryHeaderType.factoryCalendarByCustomer = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "FactoryCalendarByCustomer")
        }
        if !AOutbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber.isRemoved {
            AOutbDeliveryHeaderType.goodsIssueOrReceiptSlipNumber = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "GoodsIssueOrReceiptSlipNumber")
        }
        if !AOutbDeliveryHeaderType.goodsIssueTime.isRemoved {
            AOutbDeliveryHeaderType.goodsIssueTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "GoodsIssueTime")
        }
        if !AOutbDeliveryHeaderType.handlingUnitInStock.isRemoved {
            AOutbDeliveryHeaderType.handlingUnitInStock = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HandlingUnitInStock")
        }
        if !AOutbDeliveryHeaderType.hdrGeneralIncompletionStatus.isRemoved {
            AOutbDeliveryHeaderType.hdrGeneralIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HdrGeneralIncompletionStatus")
        }
        if !AOutbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus.isRemoved {
            AOutbDeliveryHeaderType.hdrGoodsMvtIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HdrGoodsMvtIncompletionStatus")
        }
        if !AOutbDeliveryHeaderType.headerBillgIncompletionStatus.isRemoved {
            AOutbDeliveryHeaderType.headerBillgIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderBillgIncompletionStatus")
        }
        if !AOutbDeliveryHeaderType.headerBillingBlockReason.isRemoved {
            AOutbDeliveryHeaderType.headerBillingBlockReason = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderBillingBlockReason")
        }
        if !AOutbDeliveryHeaderType.headerDelivIncompletionStatus.isRemoved {
            AOutbDeliveryHeaderType.headerDelivIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderDelivIncompletionStatus")
        }
        if !AOutbDeliveryHeaderType.headerGrossWeight.isRemoved {
            AOutbDeliveryHeaderType.headerGrossWeight = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderGrossWeight")
        }
        if !AOutbDeliveryHeaderType.headerNetWeight.isRemoved {
            AOutbDeliveryHeaderType.headerNetWeight = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderNetWeight")
        }
        if !AOutbDeliveryHeaderType.headerPackingIncompletionSts.isRemoved {
            AOutbDeliveryHeaderType.headerPackingIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderPackingIncompletionSts")
        }
        if !AOutbDeliveryHeaderType.headerPickgIncompletionStatus.isRemoved {
            AOutbDeliveryHeaderType.headerPickgIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderPickgIncompletionStatus")
        }
        if !AOutbDeliveryHeaderType.headerVolume.isRemoved {
            AOutbDeliveryHeaderType.headerVolume = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderVolume")
        }
        if !AOutbDeliveryHeaderType.headerVolumeUnit.isRemoved {
            AOutbDeliveryHeaderType.headerVolumeUnit = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderVolumeUnit")
        }
        if !AOutbDeliveryHeaderType.headerWeightUnit.isRemoved {
            AOutbDeliveryHeaderType.headerWeightUnit = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "HeaderWeightUnit")
        }
        if !AOutbDeliveryHeaderType.incotermsClassification.isRemoved {
            AOutbDeliveryHeaderType.incotermsClassification = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IncotermsClassification")
        }
        if !AOutbDeliveryHeaderType.incotermsTransferLocation.isRemoved {
            AOutbDeliveryHeaderType.incotermsTransferLocation = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IncotermsTransferLocation")
        }
        if !AOutbDeliveryHeaderType.intercompanyBillingDate.isRemoved {
            AOutbDeliveryHeaderType.intercompanyBillingDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IntercompanyBillingDate")
        }
        if !AOutbDeliveryHeaderType.internalFinancialDocument.isRemoved {
            AOutbDeliveryHeaderType.internalFinancialDocument = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "InternalFinancialDocument")
        }
        if !AOutbDeliveryHeaderType.isDeliveryForSingleWarehouse.isRemoved {
            AOutbDeliveryHeaderType.isDeliveryForSingleWarehouse = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IsDeliveryForSingleWarehouse")
        }
        if !AOutbDeliveryHeaderType.isExportDelivery.isRemoved {
            AOutbDeliveryHeaderType.isExportDelivery = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "IsExportDelivery")
        }
        if !AOutbDeliveryHeaderType.lastChangeDate.isRemoved {
            AOutbDeliveryHeaderType.lastChangeDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LastChangeDate")
        }
        if !AOutbDeliveryHeaderType.lastChangedByUser.isRemoved {
            AOutbDeliveryHeaderType.lastChangedByUser = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LastChangedByUser")
        }
        if !AOutbDeliveryHeaderType.loadingDate.isRemoved {
            AOutbDeliveryHeaderType.loadingDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LoadingDate")
        }
        if !AOutbDeliveryHeaderType.loadingPoint.isRemoved {
            AOutbDeliveryHeaderType.loadingPoint = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LoadingPoint")
        }
        if !AOutbDeliveryHeaderType.loadingTime.isRemoved {
            AOutbDeliveryHeaderType.loadingTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "LoadingTime")
        }
        if !AOutbDeliveryHeaderType.meansOfTransport.isRemoved {
            AOutbDeliveryHeaderType.meansOfTransport = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "MeansOfTransport")
        }
        if !AOutbDeliveryHeaderType.meansOfTransportRefMaterial.isRemoved {
            AOutbDeliveryHeaderType.meansOfTransportRefMaterial = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "MeansOfTransportRefMaterial")
        }
        if !AOutbDeliveryHeaderType.meansOfTransportType.isRemoved {
            AOutbDeliveryHeaderType.meansOfTransportType = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "MeansOfTransportType")
        }
        if !AOutbDeliveryHeaderType.orderCombinationIsAllowed.isRemoved {
            AOutbDeliveryHeaderType.orderCombinationIsAllowed = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OrderCombinationIsAllowed")
        }
        if !AOutbDeliveryHeaderType.orderID.isRemoved {
            AOutbDeliveryHeaderType.orderID = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OrderID")
        }
        if !AOutbDeliveryHeaderType.overallDelivConfStatus.isRemoved {
            AOutbDeliveryHeaderType.overallDelivConfStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallDelivConfStatus")
        }
        if !AOutbDeliveryHeaderType.overallDelivReltdBillgStatus.isRemoved {
            AOutbDeliveryHeaderType.overallDelivReltdBillgStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallDelivReltdBillgStatus")
        }
        if !AOutbDeliveryHeaderType.overallGoodsMovementStatus.isRemoved {
            AOutbDeliveryHeaderType.overallGoodsMovementStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallGoodsMovementStatus")
        }
        if !AOutbDeliveryHeaderType.overallIntcoBillingStatus.isRemoved {
            AOutbDeliveryHeaderType.overallIntcoBillingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallIntcoBillingStatus")
        }
        if !AOutbDeliveryHeaderType.overallPackingStatus.isRemoved {
            AOutbDeliveryHeaderType.overallPackingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallPackingStatus")
        }
        if !AOutbDeliveryHeaderType.overallPickingConfStatus.isRemoved {
            AOutbDeliveryHeaderType.overallPickingConfStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallPickingConfStatus")
        }
        if !AOutbDeliveryHeaderType.overallPickingStatus.isRemoved {
            AOutbDeliveryHeaderType.overallPickingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallPickingStatus")
        }
        if !AOutbDeliveryHeaderType.overallProofOfDeliveryStatus.isRemoved {
            AOutbDeliveryHeaderType.overallProofOfDeliveryStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallProofOfDeliveryStatus")
        }
        if !AOutbDeliveryHeaderType.overallSDProcessStatus.isRemoved {
            AOutbDeliveryHeaderType.overallSDProcessStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallSDProcessStatus")
        }
        if !AOutbDeliveryHeaderType.overallWarehouseActivityStatus.isRemoved {
            AOutbDeliveryHeaderType.overallWarehouseActivityStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OverallWarehouseActivityStatus")
        }
        if !AOutbDeliveryHeaderType.ovrlItmDelivIncompletionSts.isRemoved {
            AOutbDeliveryHeaderType.ovrlItmDelivIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmDelivIncompletionSts")
        }
        if !AOutbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts.isRemoved {
            AOutbDeliveryHeaderType.ovrlItmGdsMvtIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmGdsMvtIncompletionSts")
        }
        if !AOutbDeliveryHeaderType.ovrlItmGeneralIncompletionSts.isRemoved {
            AOutbDeliveryHeaderType.ovrlItmGeneralIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmGeneralIncompletionSts")
        }
        if !AOutbDeliveryHeaderType.ovrlItmPackingIncompletionSts.isRemoved {
            AOutbDeliveryHeaderType.ovrlItmPackingIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmPackingIncompletionSts")
        }
        if !AOutbDeliveryHeaderType.ovrlItmPickingIncompletionSts.isRemoved {
            AOutbDeliveryHeaderType.ovrlItmPickingIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "OvrlItmPickingIncompletionSts")
        }
        if !AOutbDeliveryHeaderType.paymentGuaranteeProcedure.isRemoved {
            AOutbDeliveryHeaderType.paymentGuaranteeProcedure = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PaymentGuaranteeProcedure")
        }
        if !AOutbDeliveryHeaderType.pickedItemsLocation.isRemoved {
            AOutbDeliveryHeaderType.pickedItemsLocation = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PickedItemsLocation")
        }
        if !AOutbDeliveryHeaderType.pickingDate.isRemoved {
            AOutbDeliveryHeaderType.pickingDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PickingDate")
        }
        if !AOutbDeliveryHeaderType.pickingTime.isRemoved {
            AOutbDeliveryHeaderType.pickingTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PickingTime")
        }
        if !AOutbDeliveryHeaderType.plannedGoodsIssueDate.isRemoved {
            AOutbDeliveryHeaderType.plannedGoodsIssueDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "PlannedGoodsIssueDate")
        }
        if !AOutbDeliveryHeaderType.proofOfDeliveryDate.isRemoved {
            AOutbDeliveryHeaderType.proofOfDeliveryDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ProofOfDeliveryDate")
        }
        if !AOutbDeliveryHeaderType.proposedDeliveryRoute.isRemoved {
            AOutbDeliveryHeaderType.proposedDeliveryRoute = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ProposedDeliveryRoute")
        }
        if !AOutbDeliveryHeaderType.receivingPlant.isRemoved {
            AOutbDeliveryHeaderType.receivingPlant = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ReceivingPlant")
        }
        if !AOutbDeliveryHeaderType.routeSchedule.isRemoved {
            AOutbDeliveryHeaderType.routeSchedule = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "RouteSchedule")
        }
        if !AOutbDeliveryHeaderType.salesDistrict.isRemoved {
            AOutbDeliveryHeaderType.salesDistrict = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SalesDistrict")
        }
        if !AOutbDeliveryHeaderType.salesOffice.isRemoved {
            AOutbDeliveryHeaderType.salesOffice = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SalesOffice")
        }
        if !AOutbDeliveryHeaderType.salesOrganization.isRemoved {
            AOutbDeliveryHeaderType.salesOrganization = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SalesOrganization")
        }
        if !AOutbDeliveryHeaderType.sdDocumentCategory.isRemoved {
            AOutbDeliveryHeaderType.sdDocumentCategory = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SDDocumentCategory")
        }
        if !AOutbDeliveryHeaderType.shipmentBlockReason.isRemoved {
            AOutbDeliveryHeaderType.shipmentBlockReason = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShipmentBlockReason")
        }
        if !AOutbDeliveryHeaderType.shippingCondition.isRemoved {
            AOutbDeliveryHeaderType.shippingCondition = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShippingCondition")
        }
        if !AOutbDeliveryHeaderType.shippingPoint.isRemoved {
            AOutbDeliveryHeaderType.shippingPoint = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShippingPoint")
        }
        if !AOutbDeliveryHeaderType.shippingType.isRemoved {
            AOutbDeliveryHeaderType.shippingType = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShippingType")
        }
        if !AOutbDeliveryHeaderType.shipToParty.isRemoved {
            AOutbDeliveryHeaderType.shipToParty = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "ShipToParty")
        }
        if !AOutbDeliveryHeaderType.soldToParty.isRemoved {
            AOutbDeliveryHeaderType.soldToParty = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SoldToParty")
        }
        if !AOutbDeliveryHeaderType.specialProcessingCode.isRemoved {
            AOutbDeliveryHeaderType.specialProcessingCode = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "SpecialProcessingCode")
        }
        if !AOutbDeliveryHeaderType.statisticsCurrency.isRemoved {
            AOutbDeliveryHeaderType.statisticsCurrency = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "StatisticsCurrency")
        }
        if !AOutbDeliveryHeaderType.supplier.isRemoved {
            AOutbDeliveryHeaderType.supplier = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "Supplier")
        }
        if !AOutbDeliveryHeaderType.totalBlockStatus.isRemoved {
            AOutbDeliveryHeaderType.totalBlockStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TotalBlockStatus")
        }
        if !AOutbDeliveryHeaderType.totalCreditCheckStatus.isRemoved {
            AOutbDeliveryHeaderType.totalCreditCheckStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TotalCreditCheckStatus")
        }
        if !AOutbDeliveryHeaderType.totalNumberOfPackage.isRemoved {
            AOutbDeliveryHeaderType.totalNumberOfPackage = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TotalNumberOfPackage")
        }
        if !AOutbDeliveryHeaderType.transactionCurrency.isRemoved {
            AOutbDeliveryHeaderType.transactionCurrency = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransactionCurrency")
        }
        if !AOutbDeliveryHeaderType.transportationGroup.isRemoved {
            AOutbDeliveryHeaderType.transportationGroup = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransportationGroup")
        }
        if !AOutbDeliveryHeaderType.transportationPlanningDate.isRemoved {
            AOutbDeliveryHeaderType.transportationPlanningDate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransportationPlanningDate")
        }
        if !AOutbDeliveryHeaderType.transportationPlanningStatus.isRemoved {
            AOutbDeliveryHeaderType.transportationPlanningStatus = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransportationPlanningStatus")
        }
        if !AOutbDeliveryHeaderType.transportationPlanningTime.isRemoved {
            AOutbDeliveryHeaderType.transportationPlanningTime = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "TransportationPlanningTime")
        }
        if !AOutbDeliveryHeaderType.unloadingPointName.isRemoved {
            AOutbDeliveryHeaderType.unloadingPointName = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "UnloadingPointName")
        }
        if !AOutbDeliveryHeaderType.warehouse.isRemoved {
            AOutbDeliveryHeaderType.warehouse = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "Warehouse")
        }
        if !AOutbDeliveryHeaderType.warehouseGate.isRemoved {
            AOutbDeliveryHeaderType.warehouseGate = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "WarehouseGate")
        }
        if !AOutbDeliveryHeaderType.warehouseStagingArea.isRemoved {
            AOutbDeliveryHeaderType.warehouseStagingArea = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "WarehouseStagingArea")
        }
        if !AOutbDeliveryHeaderType.toDeliveryDocumentText.isRemoved {
            AOutbDeliveryHeaderType.toDeliveryDocumentText = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "to_DeliveryDocumentText")
        }
        if !AOutbDeliveryHeaderType.toDeliveryDocumentPartner.isRemoved {
            AOutbDeliveryHeaderType.toDeliveryDocumentPartner = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "to_DeliveryDocumentPartner")
        }
        if !AOutbDeliveryHeaderType.toDeliveryDocumentItem.isRemoved {
            AOutbDeliveryHeaderType.toDeliveryDocumentItem = Ec1Metadata.EntityTypes.aOutbDeliveryHeaderType.property(withName: "to_DeliveryDocumentItem")
        }
        if !AOutbDeliveryItemTextType.deliveryDocument.isRemoved {
            AOutbDeliveryItemTextType.deliveryDocument = Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.property(withName: "DeliveryDocument")
        }
        if !AOutbDeliveryItemTextType.deliveryDocumentItem.isRemoved {
            AOutbDeliveryItemTextType.deliveryDocumentItem = Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.property(withName: "DeliveryDocumentItem")
        }
        if !AOutbDeliveryItemTextType.textElement.isRemoved {
            AOutbDeliveryItemTextType.textElement = Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.property(withName: "TextElement")
        }
        if !AOutbDeliveryItemTextType.language.isRemoved {
            AOutbDeliveryItemTextType.language = Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.property(withName: "Language")
        }
        if !AOutbDeliveryItemTextType.textElementDescription.isRemoved {
            AOutbDeliveryItemTextType.textElementDescription = Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.property(withName: "TextElementDescription")
        }
        if !AOutbDeliveryItemTextType.textElementText.isRemoved {
            AOutbDeliveryItemTextType.textElementText = Ec1Metadata.EntityTypes.aOutbDeliveryItemTextType.property(withName: "TextElementText")
        }
        if !AOutbDeliveryItemType.actualDeliveredQtyInBaseUnit.isRemoved {
            AOutbDeliveryItemType.actualDeliveredQtyInBaseUnit = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ActualDeliveredQtyInBaseUnit")
        }
        if !AOutbDeliveryItemType.deliveryVersion.isRemoved {
            AOutbDeliveryItemType.deliveryVersion = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryVersion")
        }
        if !AOutbDeliveryItemType.actualDeliveryQuantity.isRemoved {
            AOutbDeliveryItemType.actualDeliveryQuantity = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ActualDeliveryQuantity")
        }
        if !AOutbDeliveryItemType.additionalCustomerGroup1.isRemoved {
            AOutbDeliveryItemType.additionalCustomerGroup1 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup1")
        }
        if !AOutbDeliveryItemType.additionalCustomerGroup2.isRemoved {
            AOutbDeliveryItemType.additionalCustomerGroup2 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup2")
        }
        if !AOutbDeliveryItemType.additionalCustomerGroup3.isRemoved {
            AOutbDeliveryItemType.additionalCustomerGroup3 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup3")
        }
        if !AOutbDeliveryItemType.additionalCustomerGroup4.isRemoved {
            AOutbDeliveryItemType.additionalCustomerGroup4 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup4")
        }
        if !AOutbDeliveryItemType.additionalCustomerGroup5.isRemoved {
            AOutbDeliveryItemType.additionalCustomerGroup5 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalCustomerGroup5")
        }
        if !AOutbDeliveryItemType.additionalMaterialGroup1.isRemoved {
            AOutbDeliveryItemType.additionalMaterialGroup1 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup1")
        }
        if !AOutbDeliveryItemType.additionalMaterialGroup2.isRemoved {
            AOutbDeliveryItemType.additionalMaterialGroup2 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup2")
        }
        if !AOutbDeliveryItemType.additionalMaterialGroup3.isRemoved {
            AOutbDeliveryItemType.additionalMaterialGroup3 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup3")
        }
        if !AOutbDeliveryItemType.additionalMaterialGroup4.isRemoved {
            AOutbDeliveryItemType.additionalMaterialGroup4 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup4")
        }
        if !AOutbDeliveryItemType.additionalMaterialGroup5.isRemoved {
            AOutbDeliveryItemType.additionalMaterialGroup5 = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AdditionalMaterialGroup5")
        }
        if !AOutbDeliveryItemType.alternateProductNumber.isRemoved {
            AOutbDeliveryItemType.alternateProductNumber = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "AlternateProductNumber")
        }
        if !AOutbDeliveryItemType.baseUnit.isRemoved {
            AOutbDeliveryItemType.baseUnit = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BaseUnit")
        }
        if !AOutbDeliveryItemType.batch.isRemoved {
            AOutbDeliveryItemType.batch = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Batch")
        }
        if !AOutbDeliveryItemType.batchBySupplier.isRemoved {
            AOutbDeliveryItemType.batchBySupplier = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BatchBySupplier")
        }
        if !AOutbDeliveryItemType.batchClassification.isRemoved {
            AOutbDeliveryItemType.batchClassification = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BatchClassification")
        }
        if !AOutbDeliveryItemType.bomExplosion.isRemoved {
            AOutbDeliveryItemType.bomExplosion = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BOMExplosion")
        }
        if !AOutbDeliveryItemType.businessArea.isRemoved {
            AOutbDeliveryItemType.businessArea = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "BusinessArea")
        }
        if !AOutbDeliveryItemType.consumptionPosting.isRemoved {
            AOutbDeliveryItemType.consumptionPosting = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ConsumptionPosting")
        }
        if !AOutbDeliveryItemType.controllingArea.isRemoved {
            AOutbDeliveryItemType.controllingArea = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ControllingArea")
        }
        if !AOutbDeliveryItemType.costCenter.isRemoved {
            AOutbDeliveryItemType.costCenter = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CostCenter")
        }
        if !AOutbDeliveryItemType.createdByUser.isRemoved {
            AOutbDeliveryItemType.createdByUser = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CreatedByUser")
        }
        if !AOutbDeliveryItemType.creationDate.isRemoved {
            AOutbDeliveryItemType.creationDate = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CreationDate")
        }
        if !AOutbDeliveryItemType.creationTime.isRemoved {
            AOutbDeliveryItemType.creationTime = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CreationTime")
        }
        if !AOutbDeliveryItemType.custEngineeringChgStatus.isRemoved {
            AOutbDeliveryItemType.custEngineeringChgStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "CustEngineeringChgStatus")
        }
        if !AOutbDeliveryItemType.deliveryDocument.isRemoved {
            AOutbDeliveryItemType.deliveryDocument = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryDocument")
        }
        if !AOutbDeliveryItemType.deliveryDocumentItem.isRemoved {
            AOutbDeliveryItemType.deliveryDocumentItem = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryDocumentItem")
        }
        if !AOutbDeliveryItemType.deliveryDocumentItemCategory.isRemoved {
            AOutbDeliveryItemType.deliveryDocumentItemCategory = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryDocumentItemCategory")
        }
        if !AOutbDeliveryItemType.deliveryDocumentItemText.isRemoved {
            AOutbDeliveryItemType.deliveryDocumentItemText = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryDocumentItemText")
        }
        if !AOutbDeliveryItemType.deliveryGroup.isRemoved {
            AOutbDeliveryItemType.deliveryGroup = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryGroup")
        }
        if !AOutbDeliveryItemType.deliveryQuantityUnit.isRemoved {
            AOutbDeliveryItemType.deliveryQuantityUnit = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryQuantityUnit")
        }
        if !AOutbDeliveryItemType.deliveryRelatedBillingStatus.isRemoved {
            AOutbDeliveryItemType.deliveryRelatedBillingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryRelatedBillingStatus")
        }
        if !AOutbDeliveryItemType.deliveryToBaseQuantityDnmntr.isRemoved {
            AOutbDeliveryItemType.deliveryToBaseQuantityDnmntr = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryToBaseQuantityDnmntr")
        }
        if !AOutbDeliveryItemType.deliveryToBaseQuantityNmrtr.isRemoved {
            AOutbDeliveryItemType.deliveryToBaseQuantityNmrtr = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DeliveryToBaseQuantityNmrtr")
        }
        if !AOutbDeliveryItemType.departmentClassificationByCust.isRemoved {
            AOutbDeliveryItemType.departmentClassificationByCust = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DepartmentClassificationByCust")
        }
        if !AOutbDeliveryItemType.distributionChannel.isRemoved {
            AOutbDeliveryItemType.distributionChannel = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "DistributionChannel")
        }
        if !AOutbDeliveryItemType.division.isRemoved {
            AOutbDeliveryItemType.division = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Division")
        }
        if !AOutbDeliveryItemType.fixedShipgProcgDurationInDays.isRemoved {
            AOutbDeliveryItemType.fixedShipgProcgDurationInDays = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "FixedShipgProcgDurationInDays")
        }
        if !AOutbDeliveryItemType.glAccount.isRemoved {
            AOutbDeliveryItemType.glAccount = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "GLAccount")
        }
        if !AOutbDeliveryItemType.goodsMovementReasonCode.isRemoved {
            AOutbDeliveryItemType.goodsMovementReasonCode = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "GoodsMovementReasonCode")
        }
        if !AOutbDeliveryItemType.goodsMovementStatus.isRemoved {
            AOutbDeliveryItemType.goodsMovementStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "GoodsMovementStatus")
        }
        if !AOutbDeliveryItemType.goodsMovementType.isRemoved {
            AOutbDeliveryItemType.goodsMovementType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "GoodsMovementType")
        }
        if !AOutbDeliveryItemType.higherLvlItmOfBatSpltItm.isRemoved {
            AOutbDeliveryItemType.higherLvlItmOfBatSpltItm = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "HigherLvlItmOfBatSpltItm")
        }
        if !AOutbDeliveryItemType.higherLevelItem.isRemoved {
            AOutbDeliveryItemType.higherLevelItem = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "HigherLevelItem")
        }
        if !AOutbDeliveryItemType.inspectionLot.isRemoved {
            AOutbDeliveryItemType.inspectionLot = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InspectionLot")
        }
        if !AOutbDeliveryItemType.inspectionPartialLot.isRemoved {
            AOutbDeliveryItemType.inspectionPartialLot = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InspectionPartialLot")
        }
        if !AOutbDeliveryItemType.intercompanyBillingStatus.isRemoved {
            AOutbDeliveryItemType.intercompanyBillingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IntercompanyBillingStatus")
        }
        if !AOutbDeliveryItemType.internationalArticleNumber.isRemoved {
            AOutbDeliveryItemType.internationalArticleNumber = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InternationalArticleNumber")
        }
        if !AOutbDeliveryItemType.inventorySpecialStockType.isRemoved {
            AOutbDeliveryItemType.inventorySpecialStockType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InventorySpecialStockType")
        }
        if !AOutbDeliveryItemType.inventoryValuationType.isRemoved {
            AOutbDeliveryItemType.inventoryValuationType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "InventoryValuationType")
        }
        if !AOutbDeliveryItemType.isCompletelyDelivered.isRemoved {
            AOutbDeliveryItemType.isCompletelyDelivered = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IsCompletelyDelivered")
        }
        if !AOutbDeliveryItemType.isNotGoodsMovementsRelevant.isRemoved {
            AOutbDeliveryItemType.isNotGoodsMovementsRelevant = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IsNotGoodsMovementsRelevant")
        }
        if !AOutbDeliveryItemType.isSeparateValuation.isRemoved {
            AOutbDeliveryItemType.isSeparateValuation = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IsSeparateValuation")
        }
        if !AOutbDeliveryItemType.issgOrRcvgBatch.isRemoved {
            AOutbDeliveryItemType.issgOrRcvgBatch = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgBatch")
        }
        if !AOutbDeliveryItemType.issgOrRcvgMaterial.isRemoved {
            AOutbDeliveryItemType.issgOrRcvgMaterial = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgMaterial")
        }
        if !AOutbDeliveryItemType.issgOrRcvgSpclStockInd.isRemoved {
            AOutbDeliveryItemType.issgOrRcvgSpclStockInd = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgSpclStockInd")
        }
        if !AOutbDeliveryItemType.issgOrRcvgStockCategory.isRemoved {
            AOutbDeliveryItemType.issgOrRcvgStockCategory = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgStockCategory")
        }
        if !AOutbDeliveryItemType.issgOrRcvgValuationType.isRemoved {
            AOutbDeliveryItemType.issgOrRcvgValuationType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssgOrRcvgValuationType")
        }
        if !AOutbDeliveryItemType.issuingOrReceivingPlant.isRemoved {
            AOutbDeliveryItemType.issuingOrReceivingPlant = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssuingOrReceivingPlant")
        }
        if !AOutbDeliveryItemType.issuingOrReceivingStorageLoc.isRemoved {
            AOutbDeliveryItemType.issuingOrReceivingStorageLoc = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "IssuingOrReceivingStorageLoc")
        }
        if !AOutbDeliveryItemType.itemBillingBlockReason.isRemoved {
            AOutbDeliveryItemType.itemBillingBlockReason = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemBillingBlockReason")
        }
        if !AOutbDeliveryItemType.itemBillingIncompletionStatus.isRemoved {
            AOutbDeliveryItemType.itemBillingIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemBillingIncompletionStatus")
        }
        if !AOutbDeliveryItemType.itemDeliveryIncompletionStatus.isRemoved {
            AOutbDeliveryItemType.itemDeliveryIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemDeliveryIncompletionStatus")
        }
        if !AOutbDeliveryItemType.itemGdsMvtIncompletionSts.isRemoved {
            AOutbDeliveryItemType.itemGdsMvtIncompletionSts = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemGdsMvtIncompletionSts")
        }
        if !AOutbDeliveryItemType.itemGeneralIncompletionStatus.isRemoved {
            AOutbDeliveryItemType.itemGeneralIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemGeneralIncompletionStatus")
        }
        if !AOutbDeliveryItemType.itemGrossWeight.isRemoved {
            AOutbDeliveryItemType.itemGrossWeight = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemGrossWeight")
        }
        if !AOutbDeliveryItemType.itemIsBillingRelevant.isRemoved {
            AOutbDeliveryItemType.itemIsBillingRelevant = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemIsBillingRelevant")
        }
        if !AOutbDeliveryItemType.itemNetWeight.isRemoved {
            AOutbDeliveryItemType.itemNetWeight = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemNetWeight")
        }
        if !AOutbDeliveryItemType.itemPackingIncompletionStatus.isRemoved {
            AOutbDeliveryItemType.itemPackingIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemPackingIncompletionStatus")
        }
        if !AOutbDeliveryItemType.itemPickingIncompletionStatus.isRemoved {
            AOutbDeliveryItemType.itemPickingIncompletionStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemPickingIncompletionStatus")
        }
        if !AOutbDeliveryItemType.itemVolume.isRemoved {
            AOutbDeliveryItemType.itemVolume = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemVolume")
        }
        if !AOutbDeliveryItemType.itemVolumeUnit.isRemoved {
            AOutbDeliveryItemType.itemVolumeUnit = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemVolumeUnit")
        }
        if !AOutbDeliveryItemType.itemWeightUnit.isRemoved {
            AOutbDeliveryItemType.itemWeightUnit = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ItemWeightUnit")
        }
        if !AOutbDeliveryItemType.lastChangeDate.isRemoved {
            AOutbDeliveryItemType.lastChangeDate = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "LastChangeDate")
        }
        if !AOutbDeliveryItemType.loadingGroup.isRemoved {
            AOutbDeliveryItemType.loadingGroup = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "LoadingGroup")
        }
        if !AOutbDeliveryItemType.manufactureDate.isRemoved {
            AOutbDeliveryItemType.manufactureDate = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ManufactureDate")
        }
        if !AOutbDeliveryItemType.material.isRemoved {
            AOutbDeliveryItemType.material = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Material")
        }
        if !AOutbDeliveryItemType.materialByCustomer.isRemoved {
            AOutbDeliveryItemType.materialByCustomer = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialByCustomer")
        }
        if !AOutbDeliveryItemType.materialFreightGroup.isRemoved {
            AOutbDeliveryItemType.materialFreightGroup = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialFreightGroup")
        }
        if !AOutbDeliveryItemType.materialGroup.isRemoved {
            AOutbDeliveryItemType.materialGroup = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialGroup")
        }
        if !AOutbDeliveryItemType.materialIsBatchManaged.isRemoved {
            AOutbDeliveryItemType.materialIsBatchManaged = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialIsBatchManaged")
        }
        if !AOutbDeliveryItemType.materialIsIntBatchManaged.isRemoved {
            AOutbDeliveryItemType.materialIsIntBatchManaged = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "MaterialIsIntBatchManaged")
        }
        if !AOutbDeliveryItemType.numberOfSerialNumbers.isRemoved {
            AOutbDeliveryItemType.numberOfSerialNumbers = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "NumberOfSerialNumbers")
        }
        if !AOutbDeliveryItemType.orderID.isRemoved {
            AOutbDeliveryItemType.orderID = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OrderID")
        }
        if !AOutbDeliveryItemType.orderItem.isRemoved {
            AOutbDeliveryItemType.orderItem = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OrderItem")
        }
        if !AOutbDeliveryItemType.originalDeliveryQuantity.isRemoved {
            AOutbDeliveryItemType.originalDeliveryQuantity = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OriginalDeliveryQuantity")
        }
        if !AOutbDeliveryItemType.originallyRequestedMaterial.isRemoved {
            AOutbDeliveryItemType.originallyRequestedMaterial = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OriginallyRequestedMaterial")
        }
        if !AOutbDeliveryItemType.overdelivTolrtdLmtRatioInPct.isRemoved {
            AOutbDeliveryItemType.overdelivTolrtdLmtRatioInPct = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "OverdelivTolrtdLmtRatioInPct")
        }
        if !AOutbDeliveryItemType.packingStatus.isRemoved {
            AOutbDeliveryItemType.packingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PackingStatus")
        }
        if !AOutbDeliveryItemType.partialDeliveryIsAllowed.isRemoved {
            AOutbDeliveryItemType.partialDeliveryIsAllowed = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PartialDeliveryIsAllowed")
        }
        if !AOutbDeliveryItemType.paymentGuaranteeForm.isRemoved {
            AOutbDeliveryItemType.paymentGuaranteeForm = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PaymentGuaranteeForm")
        }
        if !AOutbDeliveryItemType.pickingConfirmationStatus.isRemoved {
            AOutbDeliveryItemType.pickingConfirmationStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PickingConfirmationStatus")
        }
        if !AOutbDeliveryItemType.pickingControl.isRemoved {
            AOutbDeliveryItemType.pickingControl = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PickingControl")
        }
        if !AOutbDeliveryItemType.pickingStatus.isRemoved {
            AOutbDeliveryItemType.pickingStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PickingStatus")
        }
        if !AOutbDeliveryItemType.plant.isRemoved {
            AOutbDeliveryItemType.plant = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Plant")
        }
        if !AOutbDeliveryItemType.primaryPostingSwitch.isRemoved {
            AOutbDeliveryItemType.primaryPostingSwitch = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "PrimaryPostingSwitch")
        }
        if !AOutbDeliveryItemType.productAvailabilityDate.isRemoved {
            AOutbDeliveryItemType.productAvailabilityDate = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProductAvailabilityDate")
        }
        if !AOutbDeliveryItemType.productAvailabilityTime.isRemoved {
            AOutbDeliveryItemType.productAvailabilityTime = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProductAvailabilityTime")
        }
        if !AOutbDeliveryItemType.productConfiguration.isRemoved {
            AOutbDeliveryItemType.productConfiguration = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProductConfiguration")
        }
        if !AOutbDeliveryItemType.productHierarchyNode.isRemoved {
            AOutbDeliveryItemType.productHierarchyNode = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProductHierarchyNode")
        }
        if !AOutbDeliveryItemType.profitabilitySegment.isRemoved {
            AOutbDeliveryItemType.profitabilitySegment = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProfitabilitySegment")
        }
        if !AOutbDeliveryItemType.profitCenter.isRemoved {
            AOutbDeliveryItemType.profitCenter = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProfitCenter")
        }
        if !AOutbDeliveryItemType.proofOfDeliveryRelevanceCode.isRemoved {
            AOutbDeliveryItemType.proofOfDeliveryRelevanceCode = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProofOfDeliveryRelevanceCode")
        }
        if !AOutbDeliveryItemType.proofOfDeliveryStatus.isRemoved {
            AOutbDeliveryItemType.proofOfDeliveryStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ProofOfDeliveryStatus")
        }
        if !AOutbDeliveryItemType.quantityIsFixed.isRemoved {
            AOutbDeliveryItemType.quantityIsFixed = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "QuantityIsFixed")
        }
        if !AOutbDeliveryItemType.receivingPoint.isRemoved {
            AOutbDeliveryItemType.receivingPoint = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReceivingPoint")
        }
        if !AOutbDeliveryItemType.referenceDocumentLogicalSystem.isRemoved {
            AOutbDeliveryItemType.referenceDocumentLogicalSystem = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReferenceDocumentLogicalSystem")
        }
        if !AOutbDeliveryItemType.referenceSDDocument.isRemoved {
            AOutbDeliveryItemType.referenceSDDocument = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReferenceSDDocument")
        }
        if !AOutbDeliveryItemType.referenceSDDocumentCategory.isRemoved {
            AOutbDeliveryItemType.referenceSDDocumentCategory = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReferenceSDDocumentCategory")
        }
        if !AOutbDeliveryItemType.referenceSDDocumentItem.isRemoved {
            AOutbDeliveryItemType.referenceSDDocumentItem = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ReferenceSDDocumentItem")
        }
        if !AOutbDeliveryItemType.retailPromotion.isRemoved {
            AOutbDeliveryItemType.retailPromotion = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "RetailPromotion")
        }
        if !AOutbDeliveryItemType.salesDocumentItemType.isRemoved {
            AOutbDeliveryItemType.salesDocumentItemType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SalesDocumentItemType")
        }
        if !AOutbDeliveryItemType.salesGroup.isRemoved {
            AOutbDeliveryItemType.salesGroup = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SalesGroup")
        }
        if !AOutbDeliveryItemType.salesOffice.isRemoved {
            AOutbDeliveryItemType.salesOffice = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SalesOffice")
        }
        if !AOutbDeliveryItemType.sdDocumentCategory.isRemoved {
            AOutbDeliveryItemType.sdDocumentCategory = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SDDocumentCategory")
        }
        if !AOutbDeliveryItemType.sdProcessStatus.isRemoved {
            AOutbDeliveryItemType.sdProcessStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SDProcessStatus")
        }
        if !AOutbDeliveryItemType.shelfLifeExpirationDate.isRemoved {
            AOutbDeliveryItemType.shelfLifeExpirationDate = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "ShelfLifeExpirationDate")
        }
        if !AOutbDeliveryItemType.statisticsDate.isRemoved {
            AOutbDeliveryItemType.statisticsDate = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StatisticsDate")
        }
        if !AOutbDeliveryItemType.stockType.isRemoved {
            AOutbDeliveryItemType.stockType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StockType")
        }
        if !AOutbDeliveryItemType.storageBin.isRemoved {
            AOutbDeliveryItemType.storageBin = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StorageBin")
        }
        if !AOutbDeliveryItemType.storageLocation.isRemoved {
            AOutbDeliveryItemType.storageLocation = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StorageLocation")
        }
        if !AOutbDeliveryItemType.storageType.isRemoved {
            AOutbDeliveryItemType.storageType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StorageType")
        }
        if !AOutbDeliveryItemType.subsequentMovementType.isRemoved {
            AOutbDeliveryItemType.subsequentMovementType = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "SubsequentMovementType")
        }
        if !AOutbDeliveryItemType.transportationGroup.isRemoved {
            AOutbDeliveryItemType.transportationGroup = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "TransportationGroup")
        }
        if !AOutbDeliveryItemType.underdelivTolrtdLmtRatioInPct.isRemoved {
            AOutbDeliveryItemType.underdelivTolrtdLmtRatioInPct = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "UnderdelivTolrtdLmtRatioInPct")
        }
        if !AOutbDeliveryItemType.unlimitedOverdeliveryIsAllowed.isRemoved {
            AOutbDeliveryItemType.unlimitedOverdeliveryIsAllowed = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "UnlimitedOverdeliveryIsAllowed")
        }
        if !AOutbDeliveryItemType.varblShipgProcgDurationInDays.isRemoved {
            AOutbDeliveryItemType.varblShipgProcgDurationInDays = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "VarblShipgProcgDurationInDays")
        }
        if !AOutbDeliveryItemType.warehouse.isRemoved {
            AOutbDeliveryItemType.warehouse = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "Warehouse")
        }
        if !AOutbDeliveryItemType.warehouseActivityStatus.isRemoved {
            AOutbDeliveryItemType.warehouseActivityStatus = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "WarehouseActivityStatus")
        }
        if !AOutbDeliveryItemType.warehouseStagingArea.isRemoved {
            AOutbDeliveryItemType.warehouseStagingArea = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "WarehouseStagingArea")
        }
        if !AOutbDeliveryItemType.warehouseStockCategory.isRemoved {
            AOutbDeliveryItemType.warehouseStockCategory = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "WarehouseStockCategory")
        }
        if !AOutbDeliveryItemType.warehouseStorageBin.isRemoved {
            AOutbDeliveryItemType.warehouseStorageBin = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "WarehouseStorageBin")
        }
        if !AOutbDeliveryItemType.stockSegment.isRemoved {
            AOutbDeliveryItemType.stockSegment = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "StockSegment")
        }
        if !AOutbDeliveryItemType.requirementSegment.isRemoved {
            AOutbDeliveryItemType.requirementSegment = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "RequirementSegment")
        }
        if !AOutbDeliveryItemType.toSerialDeliveryItem.isRemoved {
            AOutbDeliveryItemType.toSerialDeliveryItem = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "to_SerialDeliveryItem")
        }
        if !AOutbDeliveryItemType.toDocumentFlow.isRemoved {
            AOutbDeliveryItemType.toDocumentFlow = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "to_DocumentFlow")
        }
        if !AOutbDeliveryItemType.toDeliveryDocumentItemText.isRemoved {
            AOutbDeliveryItemType.toDeliveryDocumentItemText = Ec1Metadata.EntityTypes.aOutbDeliveryItemType.property(withName: "to_DeliveryDocumentItemText")
        }
        if !AOutbDeliveryPartnerType.addressID.isRemoved {
            AOutbDeliveryPartnerType.addressID = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "AddressID")
        }
        if !AOutbDeliveryPartnerType.contactPerson.isRemoved {
            AOutbDeliveryPartnerType.contactPerson = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "ContactPerson")
        }
        if !AOutbDeliveryPartnerType.customer.isRemoved {
            AOutbDeliveryPartnerType.customer = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "Customer")
        }
        if !AOutbDeliveryPartnerType.partnerFunction.isRemoved {
            AOutbDeliveryPartnerType.partnerFunction = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "PartnerFunction")
        }
        if !AOutbDeliveryPartnerType.personnel.isRemoved {
            AOutbDeliveryPartnerType.personnel = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "Personnel")
        }
        if !AOutbDeliveryPartnerType.sdDocument.isRemoved {
            AOutbDeliveryPartnerType.sdDocument = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "SDDocument")
        }
        if !AOutbDeliveryPartnerType.sdDocumentItem.isRemoved {
            AOutbDeliveryPartnerType.sdDocumentItem = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "SDDocumentItem")
        }
        if !AOutbDeliveryPartnerType.supplier.isRemoved {
            AOutbDeliveryPartnerType.supplier = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "Supplier")
        }
        if !AOutbDeliveryPartnerType.toAddress.isRemoved {
            AOutbDeliveryPartnerType.toAddress = Ec1Metadata.EntityTypes.aOutbDeliveryPartnerType.property(withName: "to_Address")
        }
        if !APlantType.plant.isRemoved {
            APlantType.plant = Ec1Metadata.EntityTypes.aPlantType.property(withName: "Plant")
        }
        if !APlantType.plantName.isRemoved {
            APlantType.plantName = Ec1Metadata.EntityTypes.aPlantType.property(withName: "PlantName")
        }
        if !AProductDescriptionType.product.isRemoved {
            AProductDescriptionType.product = Ec1Metadata.EntityTypes.aProductDescriptionType.property(withName: "Product")
        }
        if !AProductDescriptionType.language.isRemoved {
            AProductDescriptionType.language = Ec1Metadata.EntityTypes.aProductDescriptionType.property(withName: "Language")
        }
        if !AProductDescriptionType.productDescription.isRemoved {
            AProductDescriptionType.productDescription = Ec1Metadata.EntityTypes.aProductDescriptionType.property(withName: "ProductDescription")
        }
        if !AProductPlantType.product.isRemoved {
            AProductPlantType.product = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "Product")
        }
        if !AProductPlantType.plant.isRemoved {
            AProductPlantType.plant = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "Plant")
        }
        if !AProductPlantType.purchasingGroup.isRemoved {
            AProductPlantType.purchasingGroup = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "PurchasingGroup")
        }
        if !AProductPlantType.countryOfOrigin.isRemoved {
            AProductPlantType.countryOfOrigin = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "CountryOfOrigin")
        }
        if !AProductPlantType.regionOfOrigin.isRemoved {
            AProductPlantType.regionOfOrigin = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "RegionOfOrigin")
        }
        if !AProductPlantType.productionInvtryManagedLoc.isRemoved {
            AProductPlantType.productionInvtryManagedLoc = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "ProductionInvtryManagedLoc")
        }
        if !AProductPlantType.profileCode.isRemoved {
            AProductPlantType.profileCode = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "ProfileCode")
        }
        if !AProductPlantType.profileValidityStartDate.isRemoved {
            AProductPlantType.profileValidityStartDate = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "ProfileValidityStartDate")
        }
        if !AProductPlantType.availabilityCheckType.isRemoved {
            AProductPlantType.availabilityCheckType = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "AvailabilityCheckType")
        }
        if !AProductPlantType.fiscalYearVariant.isRemoved {
            AProductPlantType.fiscalYearVariant = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "FiscalYearVariant")
        }
        if !AProductPlantType.periodType.isRemoved {
            AProductPlantType.periodType = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "PeriodType")
        }
        if !AProductPlantType.profitCenter.isRemoved {
            AProductPlantType.profitCenter = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "ProfitCenter")
        }
        if !AProductPlantType.commodity.isRemoved {
            AProductPlantType.commodity = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "Commodity")
        }
        if !AProductPlantType.goodsReceiptDuration.isRemoved {
            AProductPlantType.goodsReceiptDuration = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "GoodsReceiptDuration")
        }
        if !AProductPlantType.maintenanceStatusName.isRemoved {
            AProductPlantType.maintenanceStatusName = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "MaintenanceStatusName")
        }
        if !AProductPlantType.isMarkedForDeletion.isRemoved {
            AProductPlantType.isMarkedForDeletion = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "IsMarkedForDeletion")
        }
        if !AProductPlantType.mrpType.isRemoved {
            AProductPlantType.mrpType = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "MRPType")
        }
        if !AProductPlantType.mrpResponsible.isRemoved {
            AProductPlantType.mrpResponsible = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "MRPResponsible")
        }
        if !AProductPlantType.abcIndicator.isRemoved {
            AProductPlantType.abcIndicator = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "ABCIndicator")
        }
        if !AProductPlantType.minimumLotSizeQuantity.isRemoved {
            AProductPlantType.minimumLotSizeQuantity = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "MinimumLotSizeQuantity")
        }
        if !AProductPlantType.maximumLotSizeQuantity.isRemoved {
            AProductPlantType.maximumLotSizeQuantity = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "MaximumLotSizeQuantity")
        }
        if !AProductPlantType.fixedLotSizeQuantity.isRemoved {
            AProductPlantType.fixedLotSizeQuantity = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "FixedLotSizeQuantity")
        }
        if !AProductPlantType.consumptionTaxCtrlCode.isRemoved {
            AProductPlantType.consumptionTaxCtrlCode = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "ConsumptionTaxCtrlCode")
        }
        if !AProductPlantType.isCoProduct.isRemoved {
            AProductPlantType.isCoProduct = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "IsCoProduct")
        }
        if !AProductPlantType.productIsConfigurable.isRemoved {
            AProductPlantType.productIsConfigurable = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "ProductIsConfigurable")
        }
        if !AProductPlantType.stockDeterminationGroup.isRemoved {
            AProductPlantType.stockDeterminationGroup = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "StockDeterminationGroup")
        }
        if !AProductPlantType.stockInTransferQuantity.isRemoved {
            AProductPlantType.stockInTransferQuantity = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "StockInTransferQuantity")
        }
        if !AProductPlantType.stockInTransitQuantity.isRemoved {
            AProductPlantType.stockInTransitQuantity = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "StockInTransitQuantity")
        }
        if !AProductPlantType.hasPostToInspectionStock.isRemoved {
            AProductPlantType.hasPostToInspectionStock = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "HasPostToInspectionStock")
        }
        if !AProductPlantType.isBatchManagementRequired.isRemoved {
            AProductPlantType.isBatchManagementRequired = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "IsBatchManagementRequired")
        }
        if !AProductPlantType.serialNumberProfile.isRemoved {
            AProductPlantType.serialNumberProfile = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "SerialNumberProfile")
        }
        if !AProductPlantType.isNegativeStockAllowed.isRemoved {
            AProductPlantType.isNegativeStockAllowed = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "IsNegativeStockAllowed")
        }
        if !AProductPlantType.goodsReceiptBlockedStockQty.isRemoved {
            AProductPlantType.goodsReceiptBlockedStockQty = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "GoodsReceiptBlockedStockQty")
        }
        if !AProductPlantType.hasConsignmentCtrl.isRemoved {
            AProductPlantType.hasConsignmentCtrl = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "HasConsignmentCtrl")
        }
        if !AProductPlantType.fiscalYearCurrentPeriod.isRemoved {
            AProductPlantType.fiscalYearCurrentPeriod = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "FiscalYearCurrentPeriod")
        }
        if !AProductPlantType.fiscalMonthCurrentPeriod.isRemoved {
            AProductPlantType.fiscalMonthCurrentPeriod = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "FiscalMonthCurrentPeriod")
        }
        if !AProductPlantType.procurementType.isRemoved {
            AProductPlantType.procurementType = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "ProcurementType")
        }
        if !AProductPlantType.isInternalBatchManaged.isRemoved {
            AProductPlantType.isInternalBatchManaged = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "IsInternalBatchManaged")
        }
        if !AProductPlantType.productCFOPCategory.isRemoved {
            AProductPlantType.productCFOPCategory = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "ProductCFOPCategory")
        }
        if !AProductPlantType.productIsExciseTaxRelevant.isRemoved {
            AProductPlantType.productIsExciseTaxRelevant = Ec1Metadata.EntityTypes.aProductPlantType.property(withName: "ProductIsExciseTaxRelevant")
        }
        if !AProductType.product.isRemoved {
            AProductType.product = Ec1Metadata.EntityTypes.aProductType.property(withName: "Product")
        }
        if !AProductType.productType.isRemoved {
            AProductType.productType = Ec1Metadata.EntityTypes.aProductType.property(withName: "ProductType")
        }
        if !AProductType.crossPlantStatus.isRemoved {
            AProductType.crossPlantStatus = Ec1Metadata.EntityTypes.aProductType.property(withName: "CrossPlantStatus")
        }
        if !AProductType.crossPlantStatusValidityDate.isRemoved {
            AProductType.crossPlantStatusValidityDate = Ec1Metadata.EntityTypes.aProductType.property(withName: "CrossPlantStatusValidityDate")
        }
        if !AProductType.creationDate.isRemoved {
            AProductType.creationDate = Ec1Metadata.EntityTypes.aProductType.property(withName: "CreationDate")
        }
        if !AProductType.createdByUser.isRemoved {
            AProductType.createdByUser = Ec1Metadata.EntityTypes.aProductType.property(withName: "CreatedByUser")
        }
        if !AProductType.lastChangeDate.isRemoved {
            AProductType.lastChangeDate = Ec1Metadata.EntityTypes.aProductType.property(withName: "LastChangeDate")
        }
        if !AProductType.lastChangedByUser.isRemoved {
            AProductType.lastChangedByUser = Ec1Metadata.EntityTypes.aProductType.property(withName: "LastChangedByUser")
        }
        if !AProductType.lastChangeDateTime.isRemoved {
            AProductType.lastChangeDateTime = Ec1Metadata.EntityTypes.aProductType.property(withName: "LastChangeDateTime")
        }
        if !AProductType.isMarkedForDeletion.isRemoved {
            AProductType.isMarkedForDeletion = Ec1Metadata.EntityTypes.aProductType.property(withName: "IsMarkedForDeletion")
        }
        if !AProductType.productOldID.isRemoved {
            AProductType.productOldID = Ec1Metadata.EntityTypes.aProductType.property(withName: "ProductOldID")
        }
        if !AProductType.grossWeight.isRemoved {
            AProductType.grossWeight = Ec1Metadata.EntityTypes.aProductType.property(withName: "GrossWeight")
        }
        if !AProductType.purchaseOrderQuantityUnit.isRemoved {
            AProductType.purchaseOrderQuantityUnit = Ec1Metadata.EntityTypes.aProductType.property(withName: "PurchaseOrderQuantityUnit")
        }
        if !AProductType.sourceOfSupply.isRemoved {
            AProductType.sourceOfSupply = Ec1Metadata.EntityTypes.aProductType.property(withName: "SourceOfSupply")
        }
        if !AProductType.weightUnit.isRemoved {
            AProductType.weightUnit = Ec1Metadata.EntityTypes.aProductType.property(withName: "WeightUnit")
        }
        if !AProductType.netWeight.isRemoved {
            AProductType.netWeight = Ec1Metadata.EntityTypes.aProductType.property(withName: "NetWeight")
        }
        if !AProductType.countryOfOrigin.isRemoved {
            AProductType.countryOfOrigin = Ec1Metadata.EntityTypes.aProductType.property(withName: "CountryOfOrigin")
        }
        if !AProductType.competitorID.isRemoved {
            AProductType.competitorID = Ec1Metadata.EntityTypes.aProductType.property(withName: "CompetitorID")
        }
        if !AProductType.productGroup.isRemoved {
            AProductType.productGroup = Ec1Metadata.EntityTypes.aProductType.property(withName: "ProductGroup")
        }
        if !AProductType.baseUnit.isRemoved {
            AProductType.baseUnit = Ec1Metadata.EntityTypes.aProductType.property(withName: "BaseUnit")
        }
        if !AProductType.itemCategoryGroup.isRemoved {
            AProductType.itemCategoryGroup = Ec1Metadata.EntityTypes.aProductType.property(withName: "ItemCategoryGroup")
        }
        if !AProductType.productHierarchy.isRemoved {
            AProductType.productHierarchy = Ec1Metadata.EntityTypes.aProductType.property(withName: "ProductHierarchy")
        }
        if !AProductType.division.isRemoved {
            AProductType.division = Ec1Metadata.EntityTypes.aProductType.property(withName: "Division")
        }
        if !AProductType.varblPurOrdUnitIsActive.isRemoved {
            AProductType.varblPurOrdUnitIsActive = Ec1Metadata.EntityTypes.aProductType.property(withName: "VarblPurOrdUnitIsActive")
        }
        if !AProductType.volumeUnit.isRemoved {
            AProductType.volumeUnit = Ec1Metadata.EntityTypes.aProductType.property(withName: "VolumeUnit")
        }
        if !AProductType.materialVolume.isRemoved {
            AProductType.materialVolume = Ec1Metadata.EntityTypes.aProductType.property(withName: "MaterialVolume")
        }
        if !AProductType.anpCode.isRemoved {
            AProductType.anpCode = Ec1Metadata.EntityTypes.aProductType.property(withName: "ANPCode")
        }
        if !AProductType.brand.isRemoved {
            AProductType.brand = Ec1Metadata.EntityTypes.aProductType.property(withName: "Brand")
        }
        if !AProductType.procurementRule.isRemoved {
            AProductType.procurementRule = Ec1Metadata.EntityTypes.aProductType.property(withName: "ProcurementRule")
        }
        if !AProductType.validityStartDate.isRemoved {
            AProductType.validityStartDate = Ec1Metadata.EntityTypes.aProductType.property(withName: "ValidityStartDate")
        }
        if !AProductType.lowLevelCode.isRemoved {
            AProductType.lowLevelCode = Ec1Metadata.EntityTypes.aProductType.property(withName: "LowLevelCode")
        }
        if !AProductType.prodNoInGenProdInPrepackProd.isRemoved {
            AProductType.prodNoInGenProdInPrepackProd = Ec1Metadata.EntityTypes.aProductType.property(withName: "ProdNoInGenProdInPrepackProd")
        }
        if !AProductType.serialIdentifierAssgmtProfile.isRemoved {
            AProductType.serialIdentifierAssgmtProfile = Ec1Metadata.EntityTypes.aProductType.property(withName: "SerialIdentifierAssgmtProfile")
        }
        if !AProductType.sizeOrDimensionText.isRemoved {
            AProductType.sizeOrDimensionText = Ec1Metadata.EntityTypes.aProductType.property(withName: "SizeOrDimensionText")
        }
        if !AProductType.industryStandardName.isRemoved {
            AProductType.industryStandardName = Ec1Metadata.EntityTypes.aProductType.property(withName: "IndustryStandardName")
        }
        if !AProductType.productStandardID.isRemoved {
            AProductType.productStandardID = Ec1Metadata.EntityTypes.aProductType.property(withName: "ProductStandardID")
        }
        if !AProductType.internationalArticleNumberCat.isRemoved {
            AProductType.internationalArticleNumberCat = Ec1Metadata.EntityTypes.aProductType.property(withName: "InternationalArticleNumberCat")
        }
        if !AProductType.productIsConfigurable.isRemoved {
            AProductType.productIsConfigurable = Ec1Metadata.EntityTypes.aProductType.property(withName: "ProductIsConfigurable")
        }
        if !AProductType.isBatchManagementRequired.isRemoved {
            AProductType.isBatchManagementRequired = Ec1Metadata.EntityTypes.aProductType.property(withName: "IsBatchManagementRequired")
        }
        if !AProductType.externalProductGroup.isRemoved {
            AProductType.externalProductGroup = Ec1Metadata.EntityTypes.aProductType.property(withName: "ExternalProductGroup")
        }
        if !AProductType.crossPlantConfigurableProduct.isRemoved {
            AProductType.crossPlantConfigurableProduct = Ec1Metadata.EntityTypes.aProductType.property(withName: "CrossPlantConfigurableProduct")
        }
        if !AProductType.serialNoExplicitnessLevel.isRemoved {
            AProductType.serialNoExplicitnessLevel = Ec1Metadata.EntityTypes.aProductType.property(withName: "SerialNoExplicitnessLevel")
        }
        if !AProductType.productManufacturerNumber.isRemoved {
            AProductType.productManufacturerNumber = Ec1Metadata.EntityTypes.aProductType.property(withName: "ProductManufacturerNumber")
        }
        if !AProductType.manufacturerNumber.isRemoved {
            AProductType.manufacturerNumber = Ec1Metadata.EntityTypes.aProductType.property(withName: "ManufacturerNumber")
        }
        if !AProductType.manufacturerPartProfile.isRemoved {
            AProductType.manufacturerPartProfile = Ec1Metadata.EntityTypes.aProductType.property(withName: "ManufacturerPartProfile")
        }
    }

    private static func merge3(metadata: CSDLDocument) {
        Ignore.valueOf_any(metadata)
        if !AProductType.qltyMgmtInProcmtIsActive.isRemoved {
            AProductType.qltyMgmtInProcmtIsActive = Ec1Metadata.EntityTypes.aProductType.property(withName: "QltyMgmtInProcmtIsActive")
        }
        if !AProductType.changeNumber.isRemoved {
            AProductType.changeNumber = Ec1Metadata.EntityTypes.aProductType.property(withName: "ChangeNumber")
        }
        if !AProductType.materialRevisionLevel.isRemoved {
            AProductType.materialRevisionLevel = Ec1Metadata.EntityTypes.aProductType.property(withName: "MaterialRevisionLevel")
        }
        if !AProductType.handlingIndicator.isRemoved {
            AProductType.handlingIndicator = Ec1Metadata.EntityTypes.aProductType.property(withName: "HandlingIndicator")
        }
        if !AProductType.warehouseProductGroup.isRemoved {
            AProductType.warehouseProductGroup = Ec1Metadata.EntityTypes.aProductType.property(withName: "WarehouseProductGroup")
        }
        if !AProductType.warehouseStorageCondition.isRemoved {
            AProductType.warehouseStorageCondition = Ec1Metadata.EntityTypes.aProductType.property(withName: "WarehouseStorageCondition")
        }
        if !AProductType.standardHandlingUnitType.isRemoved {
            AProductType.standardHandlingUnitType = Ec1Metadata.EntityTypes.aProductType.property(withName: "StandardHandlingUnitType")
        }
        if !AProductType.serialNumberProfile.isRemoved {
            AProductType.serialNumberProfile = Ec1Metadata.EntityTypes.aProductType.property(withName: "SerialNumberProfile")
        }
        if !AProductType.adjustmentProfile.isRemoved {
            AProductType.adjustmentProfile = Ec1Metadata.EntityTypes.aProductType.property(withName: "AdjustmentProfile")
        }
        if !AProductType.preferredUnitOfMeasure.isRemoved {
            AProductType.preferredUnitOfMeasure = Ec1Metadata.EntityTypes.aProductType.property(withName: "PreferredUnitOfMeasure")
        }
        if !AProductType.isPilferable.isRemoved {
            AProductType.isPilferable = Ec1Metadata.EntityTypes.aProductType.property(withName: "IsPilferable")
        }
        if !AProductType.isRelevantForHzdsSubstances.isRemoved {
            AProductType.isRelevantForHzdsSubstances = Ec1Metadata.EntityTypes.aProductType.property(withName: "IsRelevantForHzdsSubstances")
        }
        if !AProductType.quarantinePeriod.isRemoved {
            AProductType.quarantinePeriod = Ec1Metadata.EntityTypes.aProductType.property(withName: "QuarantinePeriod")
        }
        if !AProductType.timeUnitForQuarantinePeriod.isRemoved {
            AProductType.timeUnitForQuarantinePeriod = Ec1Metadata.EntityTypes.aProductType.property(withName: "TimeUnitForQuarantinePeriod")
        }
        if !AProductType.qualityInspectionGroup.isRemoved {
            AProductType.qualityInspectionGroup = Ec1Metadata.EntityTypes.aProductType.property(withName: "QualityInspectionGroup")
        }
        if !AProductType.authorizationGroup.isRemoved {
            AProductType.authorizationGroup = Ec1Metadata.EntityTypes.aProductType.property(withName: "AuthorizationGroup")
        }
        if !AProductType.handlingUnitType.isRemoved {
            AProductType.handlingUnitType = Ec1Metadata.EntityTypes.aProductType.property(withName: "HandlingUnitType")
        }
        if !AProductType.hasVariableTareWeight.isRemoved {
            AProductType.hasVariableTareWeight = Ec1Metadata.EntityTypes.aProductType.property(withName: "HasVariableTareWeight")
        }
        if !AProductType.maximumPackagingLength.isRemoved {
            AProductType.maximumPackagingLength = Ec1Metadata.EntityTypes.aProductType.property(withName: "MaximumPackagingLength")
        }
        if !AProductType.maximumPackagingWidth.isRemoved {
            AProductType.maximumPackagingWidth = Ec1Metadata.EntityTypes.aProductType.property(withName: "MaximumPackagingWidth")
        }
        if !AProductType.maximumPackagingHeight.isRemoved {
            AProductType.maximumPackagingHeight = Ec1Metadata.EntityTypes.aProductType.property(withName: "MaximumPackagingHeight")
        }
        if !AProductType.unitForMaxPackagingDimensions.isRemoved {
            AProductType.unitForMaxPackagingDimensions = Ec1Metadata.EntityTypes.aProductType.property(withName: "UnitForMaxPackagingDimensions")
        }
        if !AProductType.toDescription.isRemoved {
            AProductType.toDescription = Ec1Metadata.EntityTypes.aProductType.property(withName: "to_Description")
        }
        if !APurOrdAccountAssignmentType.purchaseOrder.isRemoved {
            APurOrdAccountAssignmentType.purchaseOrder = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "PurchaseOrder")
        }
        if !APurOrdAccountAssignmentType.purchaseOrderItem.isRemoved {
            APurOrdAccountAssignmentType.purchaseOrderItem = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "PurchaseOrderItem")
        }
        if !APurOrdAccountAssignmentType.accountAssignmentNumber.isRemoved {
            APurOrdAccountAssignmentType.accountAssignmentNumber = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "AccountAssignmentNumber")
        }
        if !APurOrdAccountAssignmentType.isDeleted_.isRemoved {
            APurOrdAccountAssignmentType.isDeleted_ = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "IsDeleted")
        }
        if !APurOrdAccountAssignmentType.purchaseOrderQuantityUnit.isRemoved {
            APurOrdAccountAssignmentType.purchaseOrderQuantityUnit = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "PurchaseOrderQuantityUnit")
        }
        if !APurOrdAccountAssignmentType.quantity.isRemoved {
            APurOrdAccountAssignmentType.quantity = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "Quantity")
        }
        if !APurOrdAccountAssignmentType.multipleAcctAssgmtDistrPercent.isRemoved {
            APurOrdAccountAssignmentType.multipleAcctAssgmtDistrPercent = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "MultipleAcctAssgmtDistrPercent")
        }
        if !APurOrdAccountAssignmentType.purgDocNetAmount.isRemoved {
            APurOrdAccountAssignmentType.purgDocNetAmount = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "PurgDocNetAmount")
        }
        if !APurOrdAccountAssignmentType.glAccount.isRemoved {
            APurOrdAccountAssignmentType.glAccount = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "GLAccount")
        }
        if !APurOrdAccountAssignmentType.businessArea.isRemoved {
            APurOrdAccountAssignmentType.businessArea = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "BusinessArea")
        }
        if !APurOrdAccountAssignmentType.costCenter.isRemoved {
            APurOrdAccountAssignmentType.costCenter = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "CostCenter")
        }
        if !APurOrdAccountAssignmentType.salesOrder.isRemoved {
            APurOrdAccountAssignmentType.salesOrder = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "SalesOrder")
        }
        if !APurOrdAccountAssignmentType.salesOrderItem.isRemoved {
            APurOrdAccountAssignmentType.salesOrderItem = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "SalesOrderItem")
        }
        if !APurOrdAccountAssignmentType.salesOrderScheduleLine.isRemoved {
            APurOrdAccountAssignmentType.salesOrderScheduleLine = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "SalesOrderScheduleLine")
        }
        if !APurOrdAccountAssignmentType.masterFixedAsset.isRemoved {
            APurOrdAccountAssignmentType.masterFixedAsset = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "MasterFixedAsset")
        }
        if !APurOrdAccountAssignmentType.fixedAsset.isRemoved {
            APurOrdAccountAssignmentType.fixedAsset = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "FixedAsset")
        }
        if !APurOrdAccountAssignmentType.goodsRecipientName.isRemoved {
            APurOrdAccountAssignmentType.goodsRecipientName = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "GoodsRecipientName")
        }
        if !APurOrdAccountAssignmentType.unloadingPointName.isRemoved {
            APurOrdAccountAssignmentType.unloadingPointName = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "UnloadingPointName")
        }
        if !APurOrdAccountAssignmentType.controllingArea.isRemoved {
            APurOrdAccountAssignmentType.controllingArea = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "ControllingArea")
        }
        if !APurOrdAccountAssignmentType.costObject.isRemoved {
            APurOrdAccountAssignmentType.costObject = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "CostObject")
        }
        if !APurOrdAccountAssignmentType.orderID.isRemoved {
            APurOrdAccountAssignmentType.orderID = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "OrderID")
        }
        if !APurOrdAccountAssignmentType.profitCenter.isRemoved {
            APurOrdAccountAssignmentType.profitCenter = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "ProfitCenter")
        }
        if !APurOrdAccountAssignmentType.wbsElementInternalID.isRemoved {
            APurOrdAccountAssignmentType.wbsElementInternalID = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "WBSElementInternalID")
        }
        if !APurOrdAccountAssignmentType.wbsElement.isRemoved {
            APurOrdAccountAssignmentType.wbsElement = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "WBSElement")
        }
        if !APurOrdAccountAssignmentType.projectNetwork.isRemoved {
            APurOrdAccountAssignmentType.projectNetwork = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "ProjectNetwork")
        }
        if !APurOrdAccountAssignmentType.realEstateObject.isRemoved {
            APurOrdAccountAssignmentType.realEstateObject = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "RealEstateObject")
        }
        if !APurOrdAccountAssignmentType.partnerAccountNumber.isRemoved {
            APurOrdAccountAssignmentType.partnerAccountNumber = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "PartnerAccountNumber")
        }
        if !APurOrdAccountAssignmentType.commitmentItem.isRemoved {
            APurOrdAccountAssignmentType.commitmentItem = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "CommitmentItem")
        }
        if !APurOrdAccountAssignmentType.jointVentureRecoveryCode.isRemoved {
            APurOrdAccountAssignmentType.jointVentureRecoveryCode = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "JointVentureRecoveryCode")
        }
        if !APurOrdAccountAssignmentType.fundsCenter.isRemoved {
            APurOrdAccountAssignmentType.fundsCenter = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "FundsCenter")
        }
        if !APurOrdAccountAssignmentType.fund.isRemoved {
            APurOrdAccountAssignmentType.fund = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "Fund")
        }
        if !APurOrdAccountAssignmentType.functionalArea.isRemoved {
            APurOrdAccountAssignmentType.functionalArea = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "FunctionalArea")
        }
        if !APurOrdAccountAssignmentType.settlementReferenceDate.isRemoved {
            APurOrdAccountAssignmentType.settlementReferenceDate = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "SettlementReferenceDate")
        }
        if !APurOrdAccountAssignmentType.taxCode.isRemoved {
            APurOrdAccountAssignmentType.taxCode = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "TaxCode")
        }
        if !APurOrdAccountAssignmentType.taxJurisdiction.isRemoved {
            APurOrdAccountAssignmentType.taxJurisdiction = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "TaxJurisdiction")
        }
        if !APurOrdAccountAssignmentType.costCtrActivityType.isRemoved {
            APurOrdAccountAssignmentType.costCtrActivityType = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "CostCtrActivityType")
        }
        if !APurOrdAccountAssignmentType.businessProcess.isRemoved {
            APurOrdAccountAssignmentType.businessProcess = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "BusinessProcess")
        }
        if !APurOrdAccountAssignmentType.earmarkedFundsDocument.isRemoved {
            APurOrdAccountAssignmentType.earmarkedFundsDocument = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "EarmarkedFundsDocument")
        }
        if !APurOrdAccountAssignmentType.grantID.isRemoved {
            APurOrdAccountAssignmentType.grantID = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "GrantID")
        }
        if !APurOrdAccountAssignmentType.budgetPeriod.isRemoved {
            APurOrdAccountAssignmentType.budgetPeriod = Ec1Metadata.EntityTypes.aPurOrdAccountAssignmentType.property(withName: "BudgetPeriod")
        }
        if !APurOrdPricingElementType.purchaseOrder.isRemoved {
            APurOrdPricingElementType.purchaseOrder = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PurchaseOrder")
        }
        if !APurOrdPricingElementType.purchaseOrderItem.isRemoved {
            APurOrdPricingElementType.purchaseOrderItem = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PurchaseOrderItem")
        }
        if !APurOrdPricingElementType.pricingDocument.isRemoved {
            APurOrdPricingElementType.pricingDocument = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingDocument")
        }
        if !APurOrdPricingElementType.pricingDocumentItem.isRemoved {
            APurOrdPricingElementType.pricingDocumentItem = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingDocumentItem")
        }
        if !APurOrdPricingElementType.pricingProcedureStep.isRemoved {
            APurOrdPricingElementType.pricingProcedureStep = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingProcedureStep")
        }
        if !APurOrdPricingElementType.pricingProcedureCounter.isRemoved {
            APurOrdPricingElementType.pricingProcedureCounter = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingProcedureCounter")
        }
        if !APurOrdPricingElementType.conditionType.isRemoved {
            APurOrdPricingElementType.conditionType = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionType")
        }
        if !APurOrdPricingElementType.conditionRateValue.isRemoved {
            APurOrdPricingElementType.conditionRateValue = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionRateValue")
        }
        if !APurOrdPricingElementType.conditionCurrency.isRemoved {
            APurOrdPricingElementType.conditionCurrency = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionCurrency")
        }
        if !APurOrdPricingElementType.priceDetnExchangeRate.isRemoved {
            APurOrdPricingElementType.priceDetnExchangeRate = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PriceDetnExchangeRate")
        }
        if !APurOrdPricingElementType.conditionAmount.isRemoved {
            APurOrdPricingElementType.conditionAmount = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionAmount")
        }
        if !APurOrdPricingElementType.conditionQuantityUnit.isRemoved {
            APurOrdPricingElementType.conditionQuantityUnit = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionQuantityUnit")
        }
        if !APurOrdPricingElementType.conditionQuantity.isRemoved {
            APurOrdPricingElementType.conditionQuantity = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionQuantity")
        }
        if !APurOrdPricingElementType.conditionApplication.isRemoved {
            APurOrdPricingElementType.conditionApplication = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionApplication")
        }
        if !APurOrdPricingElementType.pricingDateTime.isRemoved {
            APurOrdPricingElementType.pricingDateTime = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingDateTime")
        }
        if !APurOrdPricingElementType.conditionCalculationType.isRemoved {
            APurOrdPricingElementType.conditionCalculationType = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionCalculationType")
        }
        if !APurOrdPricingElementType.conditionBaseValue.isRemoved {
            APurOrdPricingElementType.conditionBaseValue = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionBaseValue")
        }
        if !APurOrdPricingElementType.conditionToBaseQtyNmrtr.isRemoved {
            APurOrdPricingElementType.conditionToBaseQtyNmrtr = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionToBaseQtyNmrtr")
        }
        if !APurOrdPricingElementType.conditionToBaseQtyDnmntr.isRemoved {
            APurOrdPricingElementType.conditionToBaseQtyDnmntr = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionToBaseQtyDnmntr")
        }
        if !APurOrdPricingElementType.conditionCategory.isRemoved {
            APurOrdPricingElementType.conditionCategory = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionCategory")
        }
        if !APurOrdPricingElementType.conditionIsForStatistics.isRemoved {
            APurOrdPricingElementType.conditionIsForStatistics = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionIsForStatistics")
        }
        if !APurOrdPricingElementType.pricingScaleType.isRemoved {
            APurOrdPricingElementType.pricingScaleType = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingScaleType")
        }
        if !APurOrdPricingElementType.isRelevantForAccrual.isRemoved {
            APurOrdPricingElementType.isRelevantForAccrual = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "IsRelevantForAccrual")
        }
        if !APurOrdPricingElementType.cndnIsRelevantForInvoiceList.isRemoved {
            APurOrdPricingElementType.cndnIsRelevantForInvoiceList = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "CndnIsRelevantForInvoiceList")
        }
        if !APurOrdPricingElementType.conditionOrigin.isRemoved {
            APurOrdPricingElementType.conditionOrigin = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionOrigin")
        }
        if !APurOrdPricingElementType.isGroupCondition.isRemoved {
            APurOrdPricingElementType.isGroupCondition = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "IsGroupCondition")
        }
        if !APurOrdPricingElementType.cndnIsRelevantForLimitValue.isRemoved {
            APurOrdPricingElementType.cndnIsRelevantForLimitValue = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "CndnIsRelevantForLimitValue")
        }
        if !APurOrdPricingElementType.conditionSequentialNumber.isRemoved {
            APurOrdPricingElementType.conditionSequentialNumber = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionSequentialNumber")
        }
        if !APurOrdPricingElementType.conditionControl.isRemoved {
            APurOrdPricingElementType.conditionControl = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionControl")
        }
        if !APurOrdPricingElementType.conditionInactiveReason.isRemoved {
            APurOrdPricingElementType.conditionInactiveReason = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionInactiveReason")
        }
        if !APurOrdPricingElementType.conditionClass.isRemoved {
            APurOrdPricingElementType.conditionClass = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionClass")
        }
        if !APurOrdPricingElementType.factorForConditionBasisValue.isRemoved {
            APurOrdPricingElementType.factorForConditionBasisValue = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "FactorForConditionBasisValue")
        }
        if !APurOrdPricingElementType.pricingScaleBasis.isRemoved {
            APurOrdPricingElementType.pricingScaleBasis = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "PricingScaleBasis")
        }
        if !APurOrdPricingElementType.conditionScaleBasisValue.isRemoved {
            APurOrdPricingElementType.conditionScaleBasisValue = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionScaleBasisValue")
        }
        if !APurOrdPricingElementType.conditionScaleBasisCurrency.isRemoved {
            APurOrdPricingElementType.conditionScaleBasisCurrency = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionScaleBasisCurrency")
        }
        if !APurOrdPricingElementType.conditionScaleBasisUnit.isRemoved {
            APurOrdPricingElementType.conditionScaleBasisUnit = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionScaleBasisUnit")
        }
        if !APurOrdPricingElementType.cndnIsRelevantForIntcoBilling.isRemoved {
            APurOrdPricingElementType.cndnIsRelevantForIntcoBilling = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "CndnIsRelevantForIntcoBilling")
        }
        if !APurOrdPricingElementType.conditionIsForConfiguration.isRemoved {
            APurOrdPricingElementType.conditionIsForConfiguration = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionIsForConfiguration")
        }
        if !APurOrdPricingElementType.conditionIsManuallyChanged.isRemoved {
            APurOrdPricingElementType.conditionIsManuallyChanged = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionIsManuallyChanged")
        }
        if !APurOrdPricingElementType.conditionRecord.isRemoved {
            APurOrdPricingElementType.conditionRecord = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "ConditionRecord")
        }
        if !APurOrdPricingElementType.accessNumberOfAccessSequence.isRemoved {
            APurOrdPricingElementType.accessNumberOfAccessSequence = Ec1Metadata.EntityTypes.aPurOrdPricingElementType.property(withName: "AccessNumberOfAccessSequence")
        }
        if !APurchaseOrderItemType.purchaseOrder.isRemoved {
            APurchaseOrderItemType.purchaseOrder = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseOrder")
        }
        if !APurchaseOrderItemType.purchaseOrderItem.isRemoved {
            APurchaseOrderItemType.purchaseOrderItem = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseOrderItem")
        }
        if !APurchaseOrderItemType.purchasingDocumentDeletionCode.isRemoved {
            APurchaseOrderItemType.purchasingDocumentDeletionCode = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchasingDocumentDeletionCode")
        }
        if !APurchaseOrderItemType.purchaseOrderItemText.isRemoved {
            APurchaseOrderItemType.purchaseOrderItemText = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseOrderItemText")
        }
        if !APurchaseOrderItemType.plant.isRemoved {
            APurchaseOrderItemType.plant = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "Plant")
        }
        if !APurchaseOrderItemType.storageLocation.isRemoved {
            APurchaseOrderItemType.storageLocation = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "StorageLocation")
        }
        if !APurchaseOrderItemType.materialGroup.isRemoved {
            APurchaseOrderItemType.materialGroup = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "MaterialGroup")
        }
        if !APurchaseOrderItemType.purchasingInfoRecord.isRemoved {
            APurchaseOrderItemType.purchasingInfoRecord = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchasingInfoRecord")
        }
        if !APurchaseOrderItemType.supplierMaterialNumber.isRemoved {
            APurchaseOrderItemType.supplierMaterialNumber = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "SupplierMaterialNumber")
        }
        if !APurchaseOrderItemType.orderQuantity.isRemoved {
            APurchaseOrderItemType.orderQuantity = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "OrderQuantity")
        }
        if !APurchaseOrderItemType.purchaseOrderQuantityUnit.isRemoved {
            APurchaseOrderItemType.purchaseOrderQuantityUnit = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseOrderQuantityUnit")
        }
        if !APurchaseOrderItemType.orderPriceUnit.isRemoved {
            APurchaseOrderItemType.orderPriceUnit = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "OrderPriceUnit")
        }
        if !APurchaseOrderItemType.orderPriceUnitToOrderUnitNmrtr.isRemoved {
            APurchaseOrderItemType.orderPriceUnitToOrderUnitNmrtr = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "OrderPriceUnitToOrderUnitNmrtr")
        }
        if !APurchaseOrderItemType.ordPriceUnitToOrderUnitDnmntr.isRemoved {
            APurchaseOrderItemType.ordPriceUnitToOrderUnitDnmntr = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "OrdPriceUnitToOrderUnitDnmntr")
        }
        if !APurchaseOrderItemType.documentCurrency.isRemoved {
            APurchaseOrderItemType.documentCurrency = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DocumentCurrency")
        }
        if !APurchaseOrderItemType.netPriceAmount.isRemoved {
            APurchaseOrderItemType.netPriceAmount = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "NetPriceAmount")
        }
        if !APurchaseOrderItemType.netPriceQuantity.isRemoved {
            APurchaseOrderItemType.netPriceQuantity = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "NetPriceQuantity")
        }
        if !APurchaseOrderItemType.taxCode.isRemoved {
            APurchaseOrderItemType.taxCode = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "TaxCode")
        }
        if !APurchaseOrderItemType.priceIsToBePrinted.isRemoved {
            APurchaseOrderItemType.priceIsToBePrinted = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PriceIsToBePrinted")
        }
        if !APurchaseOrderItemType.overdelivTolrtdLmtRatioInPct.isRemoved {
            APurchaseOrderItemType.overdelivTolrtdLmtRatioInPct = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "OverdelivTolrtdLmtRatioInPct")
        }
        if !APurchaseOrderItemType.unlimitedOverdeliveryIsAllowed.isRemoved {
            APurchaseOrderItemType.unlimitedOverdeliveryIsAllowed = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "UnlimitedOverdeliveryIsAllowed")
        }
        if !APurchaseOrderItemType.underdelivTolrtdLmtRatioInPct.isRemoved {
            APurchaseOrderItemType.underdelivTolrtdLmtRatioInPct = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "UnderdelivTolrtdLmtRatioInPct")
        }
        if !APurchaseOrderItemType.valuationType.isRemoved {
            APurchaseOrderItemType.valuationType = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ValuationType")
        }
        if !APurchaseOrderItemType.isCompletelyDelivered.isRemoved {
            APurchaseOrderItemType.isCompletelyDelivered = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IsCompletelyDelivered")
        }
        if !APurchaseOrderItemType.isFinallyInvoiced.isRemoved {
            APurchaseOrderItemType.isFinallyInvoiced = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IsFinallyInvoiced")
        }
        if !APurchaseOrderItemType.purchaseOrderItemCategory.isRemoved {
            APurchaseOrderItemType.purchaseOrderItemCategory = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseOrderItemCategory")
        }
        if !APurchaseOrderItemType.accountAssignmentCategory.isRemoved {
            APurchaseOrderItemType.accountAssignmentCategory = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "AccountAssignmentCategory")
        }
        if !APurchaseOrderItemType.multipleAcctAssgmtDistribution.isRemoved {
            APurchaseOrderItemType.multipleAcctAssgmtDistribution = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "MultipleAcctAssgmtDistribution")
        }
        if !APurchaseOrderItemType.partialInvoiceDistribution.isRemoved {
            APurchaseOrderItemType.partialInvoiceDistribution = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PartialInvoiceDistribution")
        }
        if !APurchaseOrderItemType.goodsReceiptIsExpected.isRemoved {
            APurchaseOrderItemType.goodsReceiptIsExpected = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "GoodsReceiptIsExpected")
        }
        if !APurchaseOrderItemType.goodsReceiptIsNonValuated.isRemoved {
            APurchaseOrderItemType.goodsReceiptIsNonValuated = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "GoodsReceiptIsNonValuated")
        }
        if !APurchaseOrderItemType.invoiceIsExpected.isRemoved {
            APurchaseOrderItemType.invoiceIsExpected = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "InvoiceIsExpected")
        }
        if !APurchaseOrderItemType.invoiceIsGoodsReceiptBased.isRemoved {
            APurchaseOrderItemType.invoiceIsGoodsReceiptBased = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "InvoiceIsGoodsReceiptBased")
        }
        if !APurchaseOrderItemType.purchaseContract.isRemoved {
            APurchaseOrderItemType.purchaseContract = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseContract")
        }
        if !APurchaseOrderItemType.purchaseContractItem.isRemoved {
            APurchaseOrderItemType.purchaseContractItem = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseContractItem")
        }
        if !APurchaseOrderItemType.customer.isRemoved {
            APurchaseOrderItemType.customer = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "Customer")
        }
        if !APurchaseOrderItemType.itemNetWeight.isRemoved {
            APurchaseOrderItemType.itemNetWeight = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ItemNetWeight")
        }
        if !APurchaseOrderItemType.itemWeightUnit.isRemoved {
            APurchaseOrderItemType.itemWeightUnit = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ItemWeightUnit")
        }
        if !APurchaseOrderItemType.taxJurisdiction.isRemoved {
            APurchaseOrderItemType.taxJurisdiction = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "TaxJurisdiction")
        }
        if !APurchaseOrderItemType.pricingDateControl.isRemoved {
            APurchaseOrderItemType.pricingDateControl = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PricingDateControl")
        }
        if !APurchaseOrderItemType.itemVolume.isRemoved {
            APurchaseOrderItemType.itemVolume = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ItemVolume")
        }
        if !APurchaseOrderItemType.itemVolumeUnit.isRemoved {
            APurchaseOrderItemType.itemVolumeUnit = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ItemVolumeUnit")
        }
        if !APurchaseOrderItemType.supplierConfirmationControlKey.isRemoved {
            APurchaseOrderItemType.supplierConfirmationControlKey = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "SupplierConfirmationControlKey")
        }
        if !APurchaseOrderItemType.incotermsClassification.isRemoved {
            APurchaseOrderItemType.incotermsClassification = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IncotermsClassification")
        }
        if !APurchaseOrderItemType.incotermsTransferLocation.isRemoved {
            APurchaseOrderItemType.incotermsTransferLocation = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IncotermsTransferLocation")
        }
        if !APurchaseOrderItemType.evaldRcptSettlmtIsAllowed.isRemoved {
            APurchaseOrderItemType.evaldRcptSettlmtIsAllowed = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "EvaldRcptSettlmtIsAllowed")
        }
        if !APurchaseOrderItemType.purchaseRequisition.isRemoved {
            APurchaseOrderItemType.purchaseRequisition = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseRequisition")
        }
        if !APurchaseOrderItemType.purchaseRequisitionItem.isRemoved {
            APurchaseOrderItemType.purchaseRequisitionItem = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "PurchaseRequisitionItem")
        }
        if !APurchaseOrderItemType.isReturnsItem.isRemoved {
            APurchaseOrderItemType.isReturnsItem = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IsReturnsItem")
        }
        if !APurchaseOrderItemType.requisitionerName.isRemoved {
            APurchaseOrderItemType.requisitionerName = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "RequisitionerName")
        }
        if !APurchaseOrderItemType.servicePackage.isRemoved {
            APurchaseOrderItemType.servicePackage = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ServicePackage")
        }
        if !APurchaseOrderItemType.earmarkedFunds.isRemoved {
            APurchaseOrderItemType.earmarkedFunds = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "EarmarkedFunds")
        }
        if !APurchaseOrderItemType.earmarkedFundsDocument.isRemoved {
            APurchaseOrderItemType.earmarkedFundsDocument = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "EarmarkedFundsDocument")
        }
        if !APurchaseOrderItemType.earmarkedFundsItem.isRemoved {
            APurchaseOrderItemType.earmarkedFundsItem = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "EarmarkedFundsItem")
        }
        if !APurchaseOrderItemType.earmarkedFundsDocumentItem.isRemoved {
            APurchaseOrderItemType.earmarkedFundsDocumentItem = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "EarmarkedFundsDocumentItem")
        }
        if !APurchaseOrderItemType.incotermsLocation1.isRemoved {
            APurchaseOrderItemType.incotermsLocation1 = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IncotermsLocation1")
        }
        if !APurchaseOrderItemType.incotermsLocation2.isRemoved {
            APurchaseOrderItemType.incotermsLocation2 = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "IncotermsLocation2")
        }
        if !APurchaseOrderItemType.material.isRemoved {
            APurchaseOrderItemType.material = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "Material")
        }
        if !APurchaseOrderItemType.internationalArticleNumber.isRemoved {
            APurchaseOrderItemType.internationalArticleNumber = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "InternationalArticleNumber")
        }
        if !APurchaseOrderItemType.manufacturerMaterial.isRemoved {
            APurchaseOrderItemType.manufacturerMaterial = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ManufacturerMaterial")
        }
        if !APurchaseOrderItemType.servicePerformer.isRemoved {
            APurchaseOrderItemType.servicePerformer = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ServicePerformer")
        }
        if !APurchaseOrderItemType.productType.isRemoved {
            APurchaseOrderItemType.productType = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ProductType")
        }
        if !APurchaseOrderItemType.expectedOverallLimitAmount.isRemoved {
            APurchaseOrderItemType.expectedOverallLimitAmount = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ExpectedOverallLimitAmount")
        }
        if !APurchaseOrderItemType.overallLimitAmount.isRemoved {
            APurchaseOrderItemType.overallLimitAmount = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "OverallLimitAmount")
        }
        if !APurchaseOrderItemType.deliveryAddressID.isRemoved {
            APurchaseOrderItemType.deliveryAddressID = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressID")
        }
        if !APurchaseOrderItemType.deliveryAddressName.isRemoved {
            APurchaseOrderItemType.deliveryAddressName = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressName")
        }
        if !APurchaseOrderItemType.deliveryAddressStreetName.isRemoved {
            APurchaseOrderItemType.deliveryAddressStreetName = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressStreetName")
        }
        if !APurchaseOrderItemType.deliveryAddressHouseNumber.isRemoved {
            APurchaseOrderItemType.deliveryAddressHouseNumber = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressHouseNumber")
        }
        if !APurchaseOrderItemType.deliveryAddressCityName.isRemoved {
            APurchaseOrderItemType.deliveryAddressCityName = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressCityName")
        }
        if !APurchaseOrderItemType.deliveryAddressPostalCode.isRemoved {
            APurchaseOrderItemType.deliveryAddressPostalCode = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressPostalCode")
        }
        if !APurchaseOrderItemType.deliveryAddressRegion.isRemoved {
            APurchaseOrderItemType.deliveryAddressRegion = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressRegion")
        }
        if !APurchaseOrderItemType.deliveryAddressCountry.isRemoved {
            APurchaseOrderItemType.deliveryAddressCountry = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "DeliveryAddressCountry")
        }
        if !APurchaseOrderItemType.brMaterialUsage.isRemoved {
            APurchaseOrderItemType.brMaterialUsage = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "BR_MaterialUsage")
        }
        if !APurchaseOrderItemType.brMaterialOrigin.isRemoved {
            APurchaseOrderItemType.brMaterialOrigin = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "BR_MaterialOrigin")
        }
        if !APurchaseOrderItemType.brCFOPCategory.isRemoved {
            APurchaseOrderItemType.brCFOPCategory = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "BR_CFOPCategory")
        }
        if !APurchaseOrderItemType.brIsProducedInHouse.isRemoved {
            APurchaseOrderItemType.brIsProducedInHouse = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "BR_IsProducedInHouse")
        }
        if !APurchaseOrderItemType.consumptionTaxCtrlCode.isRemoved {
            APurchaseOrderItemType.consumptionTaxCtrlCode = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "ConsumptionTaxCtrlCode")
        }
        if !APurchaseOrderItemType.stockSegment.isRemoved {
            APurchaseOrderItemType.stockSegment = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "StockSegment")
        }
        if !APurchaseOrderItemType.requirementSegment.isRemoved {
            APurchaseOrderItemType.requirementSegment = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "RequirementSegment")
        }
        if !APurchaseOrderItemType.toAccountAssignment.isRemoved {
            APurchaseOrderItemType.toAccountAssignment = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "to_AccountAssignment")
        }
        if !APurchaseOrderItemType.toPurchaseOrderPricingElement.isRemoved {
            APurchaseOrderItemType.toPurchaseOrderPricingElement = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "to_PurchaseOrderPricingElement")
        }
        if !APurchaseOrderItemType.toScheduleLine.isRemoved {
            APurchaseOrderItemType.toScheduleLine = Ec1Metadata.EntityTypes.aPurchaseOrderItemType.property(withName: "to_ScheduleLine")
        }
        if !APurchaseOrderScheduleLineType.purchasingDocument.isRemoved {
            APurchaseOrderScheduleLineType.purchasingDocument = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PurchasingDocument")
        }
        if !APurchaseOrderScheduleLineType.purchasingDocumentItem.isRemoved {
            APurchaseOrderScheduleLineType.purchasingDocumentItem = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PurchasingDocumentItem")
        }
        if !APurchaseOrderScheduleLineType.scheduleLine.isRemoved {
            APurchaseOrderScheduleLineType.scheduleLine = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "ScheduleLine")
        }
        if !APurchaseOrderScheduleLineType.delivDateCategory.isRemoved {
            APurchaseOrderScheduleLineType.delivDateCategory = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "DelivDateCategory")
        }
        if !APurchaseOrderScheduleLineType.scheduleLineDeliveryDate.isRemoved {
            APurchaseOrderScheduleLineType.scheduleLineDeliveryDate = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "ScheduleLineDeliveryDate")
        }
        if !APurchaseOrderScheduleLineType.purchaseOrderQuantityUnit.isRemoved {
            APurchaseOrderScheduleLineType.purchaseOrderQuantityUnit = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PurchaseOrderQuantityUnit")
        }
        if !APurchaseOrderScheduleLineType.scheduleLineOrderQuantity.isRemoved {
            APurchaseOrderScheduleLineType.scheduleLineOrderQuantity = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "ScheduleLineOrderQuantity")
        }
        if !APurchaseOrderScheduleLineType.scheduleLineDeliveryTime.isRemoved {
            APurchaseOrderScheduleLineType.scheduleLineDeliveryTime = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "ScheduleLineDeliveryTime")
        }
        if !APurchaseOrderScheduleLineType.schedLineStscDeliveryDate.isRemoved {
            APurchaseOrderScheduleLineType.schedLineStscDeliveryDate = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "SchedLineStscDeliveryDate")
        }
        if !APurchaseOrderScheduleLineType.purchaseRequisition.isRemoved {
            APurchaseOrderScheduleLineType.purchaseRequisition = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PurchaseRequisition")
        }
        if !APurchaseOrderScheduleLineType.purchaseRequisitionItem.isRemoved {
            APurchaseOrderScheduleLineType.purchaseRequisitionItem = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PurchaseRequisitionItem")
        }
        if !APurchaseOrderScheduleLineType.scheduleLineCommittedQuantity.isRemoved {
            APurchaseOrderScheduleLineType.scheduleLineCommittedQuantity = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "ScheduleLineCommittedQuantity")
        }
        if !APurchaseOrderScheduleLineType.performancePeriodStartDate.isRemoved {
            APurchaseOrderScheduleLineType.performancePeriodStartDate = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PerformancePeriodStartDate")
        }
        if !APurchaseOrderScheduleLineType.performancePeriodEndDate.isRemoved {
            APurchaseOrderScheduleLineType.performancePeriodEndDate = Ec1Metadata.EntityTypes.aPurchaseOrderScheduleLineType.property(withName: "PerformancePeriodEndDate")
        }
        if !APurchaseOrderType.purchaseOrder.isRemoved {
            APurchaseOrderType.purchaseOrder = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchaseOrder")
        }
        if !APurchaseOrderType.companyCode.isRemoved {
            APurchaseOrderType.companyCode = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CompanyCode")
        }
        if !APurchaseOrderType.purchaseOrderType.isRemoved {
            APurchaseOrderType.purchaseOrderType = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchaseOrderType")
        }
        if !APurchaseOrderType.purchasingDocumentDeletionCode.isRemoved {
            APurchaseOrderType.purchasingDocumentDeletionCode = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchasingDocumentDeletionCode")
        }
        if !APurchaseOrderType.purchasingProcessingStatus.isRemoved {
            APurchaseOrderType.purchasingProcessingStatus = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchasingProcessingStatus")
        }
        if !APurchaseOrderType.createdByUser.isRemoved {
            APurchaseOrderType.createdByUser = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CreatedByUser")
        }
        if !APurchaseOrderType.creationDate.isRemoved {
            APurchaseOrderType.creationDate = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CreationDate")
        }
        if !APurchaseOrderType.lastChangeDateTime.isRemoved {
            APurchaseOrderType.lastChangeDateTime = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "LastChangeDateTime")
        }
        if !APurchaseOrderType.supplier.isRemoved {
            APurchaseOrderType.supplier = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "Supplier")
        }
        if !APurchaseOrderType.purchaseOrderSubtype.isRemoved {
            APurchaseOrderType.purchaseOrderSubtype = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchaseOrderSubtype")
        }
        if !APurchaseOrderType.language.isRemoved {
            APurchaseOrderType.language = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "Language")
        }
        if !APurchaseOrderType.paymentTerms.isRemoved {
            APurchaseOrderType.paymentTerms = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PaymentTerms")
        }
        if !APurchaseOrderType.cashDiscount1Days.isRemoved {
            APurchaseOrderType.cashDiscount1Days = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CashDiscount1Days")
        }
        if !APurchaseOrderType.cashDiscount2Days.isRemoved {
            APurchaseOrderType.cashDiscount2Days = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CashDiscount2Days")
        }
        if !APurchaseOrderType.netPaymentDays.isRemoved {
            APurchaseOrderType.netPaymentDays = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "NetPaymentDays")
        }
        if !APurchaseOrderType.cashDiscount1Percent.isRemoved {
            APurchaseOrderType.cashDiscount1Percent = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CashDiscount1Percent")
        }
        if !APurchaseOrderType.cashDiscount2Percent.isRemoved {
            APurchaseOrderType.cashDiscount2Percent = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CashDiscount2Percent")
        }
        if !APurchaseOrderType.purchasingOrganization.isRemoved {
            APurchaseOrderType.purchasingOrganization = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchasingOrganization")
        }
        if !APurchaseOrderType.purchasingDocumentOrigin.isRemoved {
            APurchaseOrderType.purchasingDocumentOrigin = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchasingDocumentOrigin")
        }
        if !APurchaseOrderType.purchasingGroup.isRemoved {
            APurchaseOrderType.purchasingGroup = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchasingGroup")
        }
        if !APurchaseOrderType.purchaseOrderDate.isRemoved {
            APurchaseOrderType.purchaseOrderDate = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchaseOrderDate")
        }
        if !APurchaseOrderType.documentCurrency.isRemoved {
            APurchaseOrderType.documentCurrency = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "DocumentCurrency")
        }
        if !APurchaseOrderType.exchangeRate.isRemoved {
            APurchaseOrderType.exchangeRate = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "ExchangeRate")
        }
        if !APurchaseOrderType.validityStartDate.isRemoved {
            APurchaseOrderType.validityStartDate = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "ValidityStartDate")
        }
        if !APurchaseOrderType.validityEndDate.isRemoved {
            APurchaseOrderType.validityEndDate = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "ValidityEndDate")
        }
        if !APurchaseOrderType.supplierQuotationExternalID.isRemoved {
            APurchaseOrderType.supplierQuotationExternalID = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "SupplierQuotationExternalID")
        }
        if !APurchaseOrderType.supplierRespSalesPersonName.isRemoved {
            APurchaseOrderType.supplierRespSalesPersonName = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "SupplierRespSalesPersonName")
        }
        if !APurchaseOrderType.supplierPhoneNumber.isRemoved {
            APurchaseOrderType.supplierPhoneNumber = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "SupplierPhoneNumber")
        }
        if !APurchaseOrderType.supplyingSupplier.isRemoved {
            APurchaseOrderType.supplyingSupplier = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "SupplyingSupplier")
        }
        if !APurchaseOrderType.supplyingPlant.isRemoved {
            APurchaseOrderType.supplyingPlant = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "SupplyingPlant")
        }
        if !APurchaseOrderType.incotermsClassification.isRemoved {
            APurchaseOrderType.incotermsClassification = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "IncotermsClassification")
        }
        if !APurchaseOrderType.correspncExternalReference.isRemoved {
            APurchaseOrderType.correspncExternalReference = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CorrespncExternalReference")
        }
        if !APurchaseOrderType.correspncInternalReference.isRemoved {
            APurchaseOrderType.correspncInternalReference = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "CorrespncInternalReference")
        }
        if !APurchaseOrderType.invoicingParty.isRemoved {
            APurchaseOrderType.invoicingParty = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "InvoicingParty")
        }
        if !APurchaseOrderType.releaseIsNotCompleted.isRemoved {
            APurchaseOrderType.releaseIsNotCompleted = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "ReleaseIsNotCompleted")
        }
        if !APurchaseOrderType.purchasingCompletenessStatus.isRemoved {
            APurchaseOrderType.purchasingCompletenessStatus = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "PurchasingCompletenessStatus")
        }
        if !APurchaseOrderType.incotermsVersion.isRemoved {
            APurchaseOrderType.incotermsVersion = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "IncotermsVersion")
        }
        if !APurchaseOrderType.incotermsLocation1.isRemoved {
            APurchaseOrderType.incotermsLocation1 = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "IncotermsLocation1")
        }
        if !APurchaseOrderType.incotermsLocation2.isRemoved {
            APurchaseOrderType.incotermsLocation2 = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "IncotermsLocation2")
        }
        if !APurchaseOrderType.manualSupplierAddressID.isRemoved {
            APurchaseOrderType.manualSupplierAddressID = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "ManualSupplierAddressID")
        }
        if !APurchaseOrderType.isEndOfPurposeBlocked.isRemoved {
            APurchaseOrderType.isEndOfPurposeBlocked = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "IsEndOfPurposeBlocked")
        }
        if !APurchaseOrderType.addressCityName.isRemoved {
            APurchaseOrderType.addressCityName = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressCityName")
        }
        if !APurchaseOrderType.addressFaxNumber.isRemoved {
            APurchaseOrderType.addressFaxNumber = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressFaxNumber")
        }
        if !APurchaseOrderType.addressHouseNumber.isRemoved {
            APurchaseOrderType.addressHouseNumber = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressHouseNumber")
        }
        if !APurchaseOrderType.addressName.isRemoved {
            APurchaseOrderType.addressName = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressName")
        }
        if !APurchaseOrderType.addressPostalCode.isRemoved {
            APurchaseOrderType.addressPostalCode = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressPostalCode")
        }
        if !APurchaseOrderType.addressStreetName.isRemoved {
            APurchaseOrderType.addressStreetName = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressStreetName")
        }
        if !APurchaseOrderType.addressPhoneNumber.isRemoved {
            APurchaseOrderType.addressPhoneNumber = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressPhoneNumber")
        }
        if !APurchaseOrderType.addressRegion.isRemoved {
            APurchaseOrderType.addressRegion = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressRegion")
        }
        if !APurchaseOrderType.addressCountry.isRemoved {
            APurchaseOrderType.addressCountry = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressCountry")
        }
        if !APurchaseOrderType.addressCorrespondenceLanguage.isRemoved {
            APurchaseOrderType.addressCorrespondenceLanguage = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "AddressCorrespondenceLanguage")
        }
        if !APurchaseOrderType.toPurchaseOrderItem.isRemoved {
            APurchaseOrderType.toPurchaseOrderItem = Ec1Metadata.EntityTypes.aPurchaseOrderType.property(withName: "to_PurchaseOrderItem")
        }
        if !ASerialNmbrDeliveryType.deliveryDate.isRemoved {
            ASerialNmbrDeliveryType.deliveryDate = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "DeliveryDate")
        }
        if !ASerialNmbrDeliveryType.deliveryDocument.isRemoved {
            ASerialNmbrDeliveryType.deliveryDocument = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "DeliveryDocument")
        }
        if !ASerialNmbrDeliveryType.deliveryDocumentItem.isRemoved {
            ASerialNmbrDeliveryType.deliveryDocumentItem = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "DeliveryDocumentItem")
        }
        if !ASerialNmbrDeliveryType.maintenanceItemObjectList.isRemoved {
            ASerialNmbrDeliveryType.maintenanceItemObjectList = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "MaintenanceItemObjectList")
        }
        if !ASerialNmbrDeliveryType.sdDocumentCategory.isRemoved {
            ASerialNmbrDeliveryType.sdDocumentCategory = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "SDDocumentCategory")
        }
        if !ASerialNmbrDeliveryType.toMaintenanceItemObject.isRemoved {
            ASerialNmbrDeliveryType.toMaintenanceItemObject = Ec1Metadata.EntityTypes.aSerialNmbrDeliveryType.property(withName: "to_MaintenanceItemObject")
        }
        if !ConfirmPickType.outboundDeliveryUUID.isRemoved {
            ConfirmPickType.outboundDeliveryUUID = Ec1Metadata.EntityTypes.confirmPickType.property(withName: "OutboundDeliveryUUID")
        }
        if !ConfirmPickType.outboundDeliveryOrderItemUUID.isRemoved {
            ConfirmPickType.outboundDeliveryOrderItemUUID = Ec1Metadata.EntityTypes.confirmPickType.property(withName: "OutboundDeliveryOrderItemUUID")
        }
        if !ConfirmPickType.returnBool.isRemoved {
            ConfirmPickType.returnBool = Ec1Metadata.EntityTypes.confirmPickType.property(withName: "ReturnBool")
        }
        if !CreateOutbDeliveryType.referenceSDDocument.isRemoved {
            CreateOutbDeliveryType.referenceSDDocument = Ec1Metadata.EntityTypes.createOutbDeliveryType.property(withName: "ReferenceSDDocument")
        }
        if !CreateOutbDeliveryType.outboundDelivery.isRemoved {
            CreateOutbDeliveryType.outboundDelivery = Ec1Metadata.EntityTypes.createOutbDeliveryType.property(withName: "OutboundDelivery")
        }
        if !CreateOutbWarehouseTaskType.outboundDeliveryUUID.isRemoved {
            CreateOutbWarehouseTaskType.outboundDeliveryUUID = Ec1Metadata.EntityTypes.createOutbWarehouseTaskType.property(withName: "OutboundDeliveryUUID")
        }
        if !CreateOutbWarehouseTaskType.returnBool.isRemoved {
            CreateOutbWarehouseTaskType.returnBool = Ec1Metadata.EntityTypes.createOutbWarehouseTaskType.property(withName: "ReturnBool")
        }
        if !CreateWarehouseTaskType.inboundDeliveryUUID.isRemoved {
            CreateWarehouseTaskType.inboundDeliveryUUID = Ec1Metadata.EntityTypes.createWarehouseTaskType.property(withName: "InboundDeliveryUUID")
        }
        if !CreateWarehouseTaskType.returnBool.isRemoved {
            CreateWarehouseTaskType.returnBool = Ec1Metadata.EntityTypes.createWarehouseTaskType.property(withName: "ReturnBool")
        }
        if !DLVHead.inboundDeliveryUUID.isRemoved {
            DLVHead.inboundDeliveryUUID = Ec1Metadata.EntityTypes.dlvHead.property(withName: "InboundDeliveryUUID")
        }
        if !DLVHead.bsKeyASN.isRemoved {
            DLVHead.bsKeyASN = Ec1Metadata.EntityTypes.dlvHead.property(withName: "BSKeyASN")
        }
        if !DLVHead.bsKeyERP.isRemoved {
            DLVHead.bsKeyERP = Ec1Metadata.EntityTypes.dlvHead.property(withName: "BSKeyERP")
        }
        if !DLVHead.bsKeyPOAggr.isRemoved {
            DLVHead.bsKeyPOAggr = Ec1Metadata.EntityTypes.dlvHead.property(withName: "BSKeyPOAggr")
        }
        if !DLVHead.carrier.isRemoved {
            DLVHead.carrier = Ec1Metadata.EntityTypes.dlvHead.property(withName: "Carrier")
        }
        if !DLVHead.carrierName.isRemoved {
            DLVHead.carrierName = Ec1Metadata.EntityTypes.dlvHead.property(withName: "CarrierName")
        }
        if !DLVHead.docNoASN.isRemoved {
            DLVHead.docNoASN = Ec1Metadata.EntityTypes.dlvHead.property(withName: "DocNoASN")
        }
        if !DLVHead.docNoERP.isRemoved {
            DLVHead.docNoERP = Ec1Metadata.EntityTypes.dlvHead.property(withName: "DocNoERP")
        }
        if !DLVHead.docNoPOAggr.isRemoved {
            DLVHead.docNoPOAggr = Ec1Metadata.EntityTypes.dlvHead.property(withName: "DocNoPOAggr")
        }
        if !DLVHead.numberOfPODocuments.isRemoved {
            DLVHead.numberOfPODocuments = Ec1Metadata.EntityTypes.dlvHead.property(withName: "NumberOfPODocuments")
        }
        if !DLVHead.goodsReceiptStatus.isRemoved {
            DLVHead.goodsReceiptStatus = Ec1Metadata.EntityTypes.dlvHead.property(withName: "GoodsReceiptStatus")
        }
        if !DLVHead.goodsReceiptStatusText.isRemoved {
            DLVHead.goodsReceiptStatusText = Ec1Metadata.EntityTypes.dlvHead.property(withName: "GoodsReceiptStatusText")
        }
        if !DLVHead.completionStatusName.isRemoved {
            DLVHead.completionStatusName = Ec1Metadata.EntityTypes.dlvHead.property(withName: "CompletionStatusName")
        }
        if !DLVHead.inboundDelivery.isRemoved {
            DLVHead.inboundDelivery = Ec1Metadata.EntityTypes.dlvHead.property(withName: "InboundDelivery")
        }
        if !DLVHead.inboundDeliveryType.isRemoved {
            DLVHead.inboundDeliveryType = Ec1Metadata.EntityTypes.dlvHead.property(withName: "InboundDeliveryType")
        }
        if !DLVHead.lastChangedDateTime.isRemoved {
            DLVHead.lastChangedDateTime = Ec1Metadata.EntityTypes.dlvHead.property(withName: "LastChangedDateTime")
        }
        if !DLVHead.numberOfHU.isRemoved {
            DLVHead.numberOfHU = Ec1Metadata.EntityTypes.dlvHead.property(withName: "NumberOfHU")
        }
        if !DLVHead.numberOfItems.isRemoved {
            DLVHead.numberOfItems = Ec1Metadata.EntityTypes.dlvHead.property(withName: "NumberOfItems")
        }
        if !DLVHead.numberOfOpenItems.isRemoved {
            DLVHead.numberOfOpenItems = Ec1Metadata.EntityTypes.dlvHead.property(withName: "NumberOfOpenItems")
        }
        if !DLVHead.numberOfProducts.isRemoved {
            DLVHead.numberOfProducts = Ec1Metadata.EntityTypes.dlvHead.property(withName: "NumberOfProducts")
        }
        if !DLVHead.plannedDeliveryEndDate.isRemoved {
            DLVHead.plannedDeliveryEndDate = Ec1Metadata.EntityTypes.dlvHead.property(withName: "PlannedDeliveryEndDate")
        }
        if !DLVHead.product.isRemoved {
            DLVHead.product = Ec1Metadata.EntityTypes.dlvHead.property(withName: "Product")
        }
        if !DLVHead.productName.isRemoved {
            DLVHead.productName = Ec1Metadata.EntityTypes.dlvHead.property(withName: "ProductName")
        }
        if !DLVHead.shipFromParty.isRemoved {
            DLVHead.shipFromParty = Ec1Metadata.EntityTypes.dlvHead.property(withName: "ShipFromParty")
        }
        if !DLVHead.shipFromPartyName.isRemoved {
            DLVHead.shipFromPartyName = Ec1Metadata.EntityTypes.dlvHead.property(withName: "ShipFromPartyName")
        }
        if !DLVHead.includeCompletedDelivery.isRemoved {
            DLVHead.includeCompletedDelivery = Ec1Metadata.EntityTypes.dlvHead.property(withName: "IncludeCompletedDelivery")
        }
        if !DLVHead.uxFcCarrier.isRemoved {
            DLVHead.uxFcCarrier = Ec1Metadata.EntityTypes.dlvHead.property(withName: "UxFc_Carrier")
        }
        if !DLVHead.uxFcPlannedDeliveryEndDate.isRemoved {
            DLVHead.uxFcPlannedDeliveryEndDate = Ec1Metadata.EntityTypes.dlvHead.property(withName: "UxFc_PlannedDeliveryEndDate")
        }
        if !DLVHead.uxFcADeletable.isRemoved {
            DLVHead.uxFcADeletable = Ec1Metadata.EntityTypes.dlvHead.property(withName: "UxFcA_Deletable")
        }
        if !DLVHead.uxFcAGoodsReceipt.isRemoved {
            DLVHead.uxFcAGoodsReceipt = Ec1Metadata.EntityTypes.dlvHead.property(withName: "UxFcA_GoodsReceipt")
        }
        if !DLVHead.uxFcAUpdatable.isRemoved {
            DLVHead.uxFcAUpdatable = Ec1Metadata.EntityTypes.dlvHead.property(withName: "UxFcA_Updatable")
        }
        if !DLVHead.inboundDeliveryTypeText.isRemoved {
            DLVHead.inboundDeliveryTypeText = Ec1Metadata.EntityTypes.dlvHead.property(withName: "InboundDeliveryTypeText")
        }
        if !DLVHead.docNoPPOAggr.isRemoved {
            DLVHead.docNoPPOAggr = Ec1Metadata.EntityTypes.dlvHead.property(withName: "DocNoPPOAggr")
        }
        if !DLVHead.numberOfPPODocuments.isRemoved {
            DLVHead.numberOfPPODocuments = Ec1Metadata.EntityTypes.dlvHead.property(withName: "NumberOfPPODocuments")
        }
        if !DLVHead.warehouseNumber.isRemoved {
            DLVHead.warehouseNumber = Ec1Metadata.EntityTypes.dlvHead.property(withName: "WarehouseNumber")
        }
        if !DLVHead.productionIndicator.isRemoved {
            DLVHead.productionIndicator = Ec1Metadata.EntityTypes.dlvHead.property(withName: "ProductionIndicator")
        }
        if !DLVHead.blockedOverallStatus.isRemoved {
            DLVHead.blockedOverallStatus = Ec1Metadata.EntityTypes.dlvHead.property(withName: "BlockedOverallStatus")
        }
        if !DLVHead.blockedOverallStatusName.isRemoved {
            DLVHead.blockedOverallStatusName = Ec1Metadata.EntityTypes.dlvHead.property(withName: "BlockedOverallStatusName")
        }
        if !DLVHead.packingStatus.isRemoved {
            DLVHead.packingStatus = Ec1Metadata.EntityTypes.dlvHead.property(withName: "PackingStatus")
        }
        if !DLVHead.packingStatusName.isRemoved {
            DLVHead.packingStatusName = Ec1Metadata.EntityTypes.dlvHead.property(withName: "PackingStatusName")
        }
        if !DLVHead.existsWT.isRemoved {
            DLVHead.existsWT = Ec1Metadata.EntityTypes.dlvHead.property(withName: "ExistsWT")
        }
        if !DLVHead.items.isRemoved {
            DLVHead.items = Ec1Metadata.EntityTypes.dlvHead.property(withName: "Items")
        }
        if !DLVItem.inboundDeliveryItemUUID.isRemoved {
            DLVItem.inboundDeliveryItemUUID = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDeliveryItemUUID")
        }
        if !DLVItem.inboundDeliveryUUID.isRemoved {
            DLVItem.inboundDeliveryUUID = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDeliveryUUID")
        }
        if !DLVItem.batch.isRemoved {
            DLVItem.batch = Ec1Metadata.EntityTypes.dlvItem.property(withName: "Batch")
        }
        if !DLVItem.countryOfOrigin.isRemoved {
            DLVItem.countryOfOrigin = Ec1Metadata.EntityTypes.dlvItem.property(withName: "CountryOfOrigin")
        }
        if !DLVItem.bsKeyPO.isRemoved {
            DLVItem.bsKeyPO = Ec1Metadata.EntityTypes.dlvItem.property(withName: "BSKeyPO")
        }
        if !DLVItem.productionDate.isRemoved {
            DLVItem.productionDate = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ProductionDate")
        }
        if !DLVItem.docNoPO.isRemoved {
            DLVItem.docNoPO = Ec1Metadata.EntityTypes.dlvItem.property(withName: "DocNoPO")
        }
        if !DLVItem.inboundDelivery.isRemoved {
            DLVItem.inboundDelivery = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDelivery")
        }
        if !DLVItem.inboundDeliveryItem.isRemoved {
            DLVItem.inboundDeliveryItem = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDeliveryItem")
        }
        if !DLVItem.inboundDeliveryItemCategory.isRemoved {
            DLVItem.inboundDeliveryItemCategory = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDeliveryItemCategory")
        }
        if !DLVItem.inboundDeliveryItemType.isRemoved {
            DLVItem.inboundDeliveryItemType = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDeliveryItemType")
        }
        if !DLVItem.itemDeliveryQuantity.isRemoved {
            DLVItem.itemDeliveryQuantity = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ItemDeliveryQuantity")
        }
        if !DLVItem.itemDeliveryQuantityUnit.isRemoved {
            DLVItem.itemDeliveryQuantityUnit = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ItemDeliveryQuantityUnit")
        }
        if !DLVItem.packedQuantity.isRemoved {
            DLVItem.packedQuantity = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PackedQuantity")
        }
        if !DLVItem.packOpenQuantity.isRemoved {
            DLVItem.packOpenQuantity = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PackOpenQuantity")
        }
        if !DLVItem.putawayOpenQuantity.isRemoved {
            DLVItem.putawayOpenQuantity = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PutawayOpenQuantity")
        }
        if !DLVItem.batchCrtInd.isRemoved {
            DLVItem.batchCrtInd = Ec1Metadata.EntityTypes.dlvItem.property(withName: "BatchCrtInd")
        }
        if !DLVItem.batchVendor.isRemoved {
            DLVItem.batchVendor = Ec1Metadata.EntityTypes.dlvItem.property(withName: "BatchVendor")
        }
        if !DLVItem.relevantForPacking.isRemoved {
            DLVItem.relevantForPacking = Ec1Metadata.EntityTypes.dlvItem.property(withName: "RelevantForPacking")
        }
        if !DLVItem.alternativeQuantityUnit.isRemoved {
            DLVItem.alternativeQuantityUnit = Ec1Metadata.EntityTypes.dlvItem.property(withName: "AlternativeQuantityUnit")
        }
        if !DLVItem.itemNoPO.isRemoved {
            DLVItem.itemNoPO = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ItemNoPO")
        }
        if !DLVItem.lastChangedDateTime.isRemoved {
            DLVItem.lastChangedDateTime = Ec1Metadata.EntityTypes.dlvItem.property(withName: "LastChangedDateTime")
        }
        if !DLVItem.numberOfHU.isRemoved {
            DLVItem.numberOfHU = Ec1Metadata.EntityTypes.dlvItem.property(withName: "NumberOfHU")
        }
        if !DLVItem.planningPutawayStatus.isRemoved {
            DLVItem.planningPutawayStatus = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PlanningPutawayStatus")
        }
        if !DLVItem.planningPutawayStatusText.isRemoved {
            DLVItem.planningPutawayStatusText = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PlanningPutawayStatusText")
        }
        if !DLVItem.goodsReceiptStatus.isRemoved {
            DLVItem.goodsReceiptStatus = Ec1Metadata.EntityTypes.dlvItem.property(withName: "GoodsReceiptStatus")
        }
        if !DLVItem.goodsReceiptStatusText.isRemoved {
            DLVItem.goodsReceiptStatusText = Ec1Metadata.EntityTypes.dlvItem.property(withName: "GoodsReceiptStatusText")
        }
        if !DLVItem.productUUID.isRemoved {
            DLVItem.productUUID = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ProductUUID")
        }
        if !DLVItem.product.isRemoved {
            DLVItem.product = Ec1Metadata.EntityTypes.dlvItem.property(withName: "Product")
        }
        if !DLVItem.productName.isRemoved {
            DLVItem.productName = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ProductName")
        }
        if !DLVItem.sledBbd.isRemoved {
            DLVItem.sledBbd = Ec1Metadata.EntityTypes.dlvItem.property(withName: "SledBbd")
        }
        if !DLVItem.inboundDeliveryItemUUIDTemplate.isRemoved {
            DLVItem.inboundDeliveryItemUUIDTemplate = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InboundDeliveryItemUUIDTemplate")
        }
        if !DLVItem.warehouseNumber.isRemoved {
            DLVItem.warehouseNumber = Ec1Metadata.EntityTypes.dlvItem.property(withName: "WarehouseNumber")
        }
        if !DLVItem.entitled.isRemoved {
            DLVItem.entitled = Ec1Metadata.EntityTypes.dlvItem.property(withName: "Entitled")
        }
        if !DLVItem.uxFcBatch.isRemoved {
            DLVItem.uxFcBatch = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFc_Batch")
        }
        if !DLVItem.uxFcItemDeliveryQuantity.isRemoved {
            DLVItem.uxFcItemDeliveryQuantity = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFc_ItemDeliveryQuantity")
        }
        if !DLVItem.uxFcItemDeliveryQuantityUnit.isRemoved {
            DLVItem.uxFcItemDeliveryQuantityUnit = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFc_ItemDeliveryQuantityUnit")
        }
        if !DLVItem.uxFcASplit.isRemoved {
            DLVItem.uxFcASplit = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFcA_Split")
        }
        if !DLVItem.uxFcACreateTask.isRemoved {
            DLVItem.uxFcACreateTask = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFcA_CreateTask")
        }
        if !DLVItem.uxFcADeletable.isRemoved {
            DLVItem.uxFcADeletable = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFcA_Deletable")
        }
        if !DLVItem.uxFcAUpdatable.isRemoved {
            DLVItem.uxFcAUpdatable = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFcA_Updatable")
        }
        if !DLVItem.uxFcACreateBatch.isRemoved {
            DLVItem.uxFcACreateBatch = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFcA_CreateBatch")
        }
        if !DLVItem.missingWTQty.isRemoved {
            DLVItem.missingWTQty = Ec1Metadata.EntityTypes.dlvItem.property(withName: "MissingWTQty")
        }
        if !DLVItem.missingWTQtyUoM.isRemoved {
            DLVItem.missingWTQtyUoM = Ec1Metadata.EntityTypes.dlvItem.property(withName: "MissingWTQtyUoM")
        }
        if !DLVItem.openWTQty.isRemoved {
            DLVItem.openWTQty = Ec1Metadata.EntityTypes.dlvItem.property(withName: "OpenWTQty")
        }
        if !DLVItem.openWTQtyUoM.isRemoved {
            DLVItem.openWTQtyUoM = Ec1Metadata.EntityTypes.dlvItem.property(withName: "OpenWTQtyUoM")
        }
        if !DLVItem.existsWT.isRemoved {
            DLVItem.existsWT = Ec1Metadata.EntityTypes.dlvItem.property(withName: "ExistsWT")
        }
        if !DLVItem.stockType.isRemoved {
            DLVItem.stockType = Ec1Metadata.EntityTypes.dlvItem.property(withName: "StockType")
        }
        if !DLVItem.stockTypeName.isRemoved {
            DLVItem.stockTypeName = Ec1Metadata.EntityTypes.dlvItem.property(withName: "StockTypeName")
        }
        if !DLVItem.uxFcStockType.isRemoved {
            DLVItem.uxFcStockType = Ec1Metadata.EntityTypes.dlvItem.property(withName: "UxFc_StockType")
        }
        if !DLVItem.docNoPPO.isRemoved {
            DLVItem.docNoPPO = Ec1Metadata.EntityTypes.dlvItem.property(withName: "DocNoPPO")
        }
        if !DLVItem.blockedOverallStatus.isRemoved {
            DLVItem.blockedOverallStatus = Ec1Metadata.EntityTypes.dlvItem.property(withName: "BlockedOverallStatus")
        }
        if !DLVItem.blockedOverallStatusName.isRemoved {
            DLVItem.blockedOverallStatusName = Ec1Metadata.EntityTypes.dlvItem.property(withName: "BlockedOverallStatusName")
        }
        if !DLVItem.packingStatus.isRemoved {
            DLVItem.packingStatus = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PackingStatus")
        }
        if !DLVItem.packingStatusName.isRemoved {
            DLVItem.packingStatusName = Ec1Metadata.EntityTypes.dlvItem.property(withName: "PackingStatusName")
        }
        if !DLVItem.vendorProduct.isRemoved {
            DLVItem.vendorProduct = Ec1Metadata.EntityTypes.dlvItem.property(withName: "VendorProduct")
        }
        if !DLVItem.stockDocument.isRemoved {
            DLVItem.stockDocument = Ec1Metadata.EntityTypes.dlvItem.property(withName: "StockDocument")
        }
        if !DLVItem.stockItem.isRemoved {
            DLVItem.stockItem = Ec1Metadata.EntityTypes.dlvItem.property(withName: "StockItem")
        }
        if !DLVItem.stockDocumentType.isRemoved {
            DLVItem.stockDocumentType = Ec1Metadata.EntityTypes.dlvItem.property(withName: "StockDocumentType")
        }
        if !DLVItem.stockDocumentTypeText.isRemoved {
            DLVItem.stockDocumentTypeText = Ec1Metadata.EntityTypes.dlvItem.property(withName: "StockDocumentTypeText")
        }
        if !DLVItem.inspectionLot.isRemoved {
            DLVItem.inspectionLot = Ec1Metadata.EntityTypes.dlvItem.property(withName: "InspectionLot")
        }
        if !DLVItem.topHU.isRemoved {
            DLVItem.topHU = Ec1Metadata.EntityTypes.dlvItem.property(withName: "TopHU")
        }
        if !DLVItem.huSingleItems.isRemoved {
            DLVItem.huSingleItems = Ec1Metadata.EntityTypes.dlvItem.property(withName: "HUSingleItems")
        }
        if !GoodsIssueType.outboundDeliveryUUID.isRemoved {
            GoodsIssueType.outboundDeliveryUUID = Ec1Metadata.EntityTypes.goodsIssueType.property(withName: "OutboundDeliveryUUID")
        }
        if !GoodsIssueType.returnBool.isRemoved {
            GoodsIssueType.returnBool = Ec1Metadata.EntityTypes.goodsIssueType.property(withName: "ReturnBool")
        }
        if !Hu.huID.isRemoved {
            Hu.huID = Ec1Metadata.EntityTypes.hu.property(withName: "HuId")
        }
        if !Hu.bin.isRemoved {
            Hu.bin = Ec1Metadata.EntityTypes.hu.property(withName: "Bin")
        }
        if !Hu.lgnum.isRemoved {
            Hu.lgnum = Ec1Metadata.EntityTypes.hu.property(withName: "Lgnum")
        }
        if !Hu.workstation.isRemoved {
            Hu.workstation = Ec1Metadata.EntityTypes.hu.property(withName: "Workstation")
        }
        if !Hu.packmat.isRemoved {
            Hu.packmat = Ec1Metadata.EntityTypes.hu.property(withName: "Packmat")
        }
        if !Hu.docno.isRemoved {
            Hu.docno = Ec1Metadata.EntityTypes.hu.property(withName: "Docno")
        }
        if !Hu.isPickHu.isRemoved {
            Hu.isPickHu = Ec1Metadata.EntityTypes.hu.property(withName: "IsPickHu")
        }
        if !Hu.totalWeight.isRemoved {
            Hu.totalWeight = Ec1Metadata.EntityTypes.hu.property(withName: "TotalWeight")
        }
        if !Hu.weightUoM.isRemoved {
            Hu.weightUoM = Ec1Metadata.EntityTypes.hu.property(withName: "WeightUoM")
        }
        if !Hu.loadingWeight.isRemoved {
            Hu.loadingWeight = Ec1Metadata.EntityTypes.hu.property(withName: "LoadingWeight")
        }
        if !Hu.type_.isRemoved {
            Hu.type_ = Ec1Metadata.EntityTypes.hu.property(withName: "Type")
        }
        if !Hu.closed.isRemoved {
            Hu.closed = Ec1Metadata.EntityTypes.hu.property(withName: "Closed")
        }
        if !Hu.msgID.isRemoved {
            Hu.msgID = Ec1Metadata.EntityTypes.hu.property(withName: "MsgId")
        }
        if !Hu.msgVar.isRemoved {
            Hu.msgVar = Ec1Metadata.EntityTypes.hu.property(withName: "MsgVar")
        }
        if !Hu.msgKey.isRemoved {
            Hu.msgKey = Ec1Metadata.EntityTypes.hu.property(withName: "MsgKey")
        }
        if !Hu.msgType.isRemoved {
            Hu.msgType = Ec1Metadata.EntityTypes.hu.property(withName: "MsgType")
        }
        if !Hu.msgSuccess.isRemoved {
            Hu.msgSuccess = Ec1Metadata.EntityTypes.hu.property(withName: "MsgSuccess")
        }
        if !Hu.items.isRemoved {
            Hu.items = Ec1Metadata.EntityTypes.hu.property(withName: "Items")
        }
        if !HUHead.handlingUnitUUID.isRemoved {
            HUHead.handlingUnitUUID = Ec1Metadata.EntityTypes.huHead.property(withName: "HandlingUnitUUID")
        }
        if !HUHead.inboundDeliveryUUID.isRemoved {
            HUHead.inboundDeliveryUUID = Ec1Metadata.EntityTypes.huHead.property(withName: "InboundDeliveryUUID")
        }
        if !HUHead.inboundDeliveryItemUUID.isRemoved {
            HUHead.inboundDeliveryItemUUID = Ec1Metadata.EntityTypes.huHead.property(withName: "InboundDeliveryItemUUID")
        }
        if !HUHead.handlingUnitID.isRemoved {
            HUHead.handlingUnitID = Ec1Metadata.EntityTypes.huHead.property(withName: "HandlingUnitID")
        }
        if !HUHead.packageMaterial.isRemoved {
            HUHead.packageMaterial = Ec1Metadata.EntityTypes.huHead.property(withName: "PackageMaterial")
        }
        if !HUHead.packageMaterialText.isRemoved {
            HUHead.packageMaterialText = Ec1Metadata.EntityTypes.huHead.property(withName: "PackageMaterialText")
        }
        if !HUHead.handlingUnitType.isRemoved {
            HUHead.handlingUnitType = Ec1Metadata.EntityTypes.huHead.property(withName: "HandlingUnitType")
        }
        if !HUHead.handlingUnitTypeText.isRemoved {
            HUHead.handlingUnitTypeText = Ec1Metadata.EntityTypes.huHead.property(withName: "HandlingUnitTypeText")
        }
        if !HUHead.handlingUnitTopID.isRemoved {
            HUHead.handlingUnitTopID = Ec1Metadata.EntityTypes.huHead.property(withName: "HandlingUnitTopID")
        }
        if !HUHead.handlingUnitTopUUID.isRemoved {
            HUHead.handlingUnitTopUUID = Ec1Metadata.EntityTypes.huHead.property(withName: "HandlingUnitTopUUID")
        }
        if !HUHead.higherUUID.isRemoved {
            HUHead.higherUUID = Ec1Metadata.EntityTypes.huHead.property(withName: "HigherUUID")
        }
        if !HUHead.weight.isRemoved {
            HUHead.weight = Ec1Metadata.EntityTypes.huHead.property(withName: "Weight")
        }
        if !HUHead.weightUnit.isRemoved {
            HUHead.weightUnit = Ec1Metadata.EntityTypes.huHead.property(withName: "WeightUnit")
        }
        if !HUHead.volume.isRemoved {
            HUHead.volume = Ec1Metadata.EntityTypes.huHead.property(withName: "Volume")
        }
        if !HUHead.volumeUnit.isRemoved {
            HUHead.volumeUnit = Ec1Metadata.EntityTypes.huHead.property(withName: "VolumeUnit")
        }
        if !HUHead.itemQuantity.isRemoved {
            HUHead.itemQuantity = Ec1Metadata.EntityTypes.huHead.property(withName: "ItemQuantity")
        }
        if !HUHead.itemQuantityUnit.isRemoved {
            HUHead.itemQuantityUnit = Ec1Metadata.EntityTypes.huHead.property(withName: "ItemQuantityUnit")
        }
        if !HUHead.lastChangedDateTime.isRemoved {
            HUHead.lastChangedDateTime = Ec1Metadata.EntityTypes.huHead.property(withName: "LastChangedDateTime")
        }
        if !HUHead.numberOfHUItems.isRemoved {
            HUHead.numberOfHUItems = Ec1Metadata.EntityTypes.huHead.property(withName: "NumberOfHUItems")
        }
        if !HUHead.warehouseNumber.isRemoved {
            HUHead.warehouseNumber = Ec1Metadata.EntityTypes.huHead.property(withName: "WarehouseNumber")
        }
        if !HUHead.items.isRemoved {
            HUHead.items = Ec1Metadata.EntityTypes.huHead.property(withName: "Items")
        }
        if !HUHead.aggregatedItems.isRemoved {
            HUHead.aggregatedItems = Ec1Metadata.EntityTypes.huHead.property(withName: "AggregatedItems")
        }
        if !HUItem.handlingUnitUUID.isRemoved {
            HUItem.handlingUnitUUID = Ec1Metadata.EntityTypes.huItem.property(withName: "HandlingUnitUUID")
        }
        if !HUItem.handlingUnitParentUUID.isRemoved {
            HUItem.handlingUnitParentUUID = Ec1Metadata.EntityTypes.huItem.property(withName: "HandlingUnitParentUUID")
        }
        if !HUItem.inboundDeliveryUUID.isRemoved {
            HUItem.inboundDeliveryUUID = Ec1Metadata.EntityTypes.huItem.property(withName: "InboundDeliveryUUID")
        }
        if !HUItem.inboundDeliveryItemUUID.isRemoved {
            HUItem.inboundDeliveryItemUUID = Ec1Metadata.EntityTypes.huItem.property(withName: "InboundDeliveryItemUUID")
        }
        if !HUItem.handlingUnitID.isRemoved {
            HUItem.handlingUnitID = Ec1Metadata.EntityTypes.huItem.property(withName: "HandlingUnitID")
        }
        if !HUItem.product.isRemoved {
            HUItem.product = Ec1Metadata.EntityTypes.huItem.property(withName: "Product")
        }
        if !HUItem.productName.isRemoved {
            HUItem.productName = Ec1Metadata.EntityTypes.huItem.property(withName: "ProductName")
        }
        if !HUItem.batch.isRemoved {
            HUItem.batch = Ec1Metadata.EntityTypes.huItem.property(withName: "Batch")
        }
        if !HUItem.itemQuantity.isRemoved {
            HUItem.itemQuantity = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemQuantity")
        }
        if !HUItem.itemQuantityUnit.isRemoved {
            HUItem.itemQuantityUnit = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemQuantityUnit")
        }
        if !HUItem.docNoPO.isRemoved {
            HUItem.docNoPO = Ec1Metadata.EntityTypes.huItem.property(withName: "DocNoPO")
        }
        if !HUItem.itemNoPO.isRemoved {
            HUItem.itemNoPO = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemNoPO")
        }
        if !HUItem.itemWeight.isRemoved {
            HUItem.itemWeight = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemWeight")
        }
        if !HUItem.itemWeightUnit.isRemoved {
            HUItem.itemWeightUnit = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemWeightUnit")
        }
        if !HUItem.itemVolume.isRemoved {
            HUItem.itemVolume = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemVolume")
        }
        if !HUItem.itemVolumeUnit.isRemoved {
            HUItem.itemVolumeUnit = Ec1Metadata.EntityTypes.huItem.property(withName: "ItemVolumeUnit")
        }
        if !HUItem.lastChangedDateTime.isRemoved {
            HUItem.lastChangedDateTime = Ec1Metadata.EntityTypes.huItem.property(withName: "LastChangedDateTime")
        }
        if !HUSingleItem.handlingUnitItemUUID.isRemoved {
            HUSingleItem.handlingUnitItemUUID = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "HandlingUnitItemUUID")
        }
        if !HUSingleItem.handlingUnitHeadUUID.isRemoved {
            HUSingleItem.handlingUnitHeadUUID = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "HandlingUnitHeadUUID")
        }
        if !HUSingleItem.handlingUnitHeadID.isRemoved {
            HUSingleItem.handlingUnitHeadID = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "HandlingUnitHeadID")
        }
        if !HUSingleItem.packageMaterial.isRemoved {
            HUSingleItem.packageMaterial = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "PackageMaterial")
        }
        if !HUSingleItem.handlingUnitType.isRemoved {
            HUSingleItem.handlingUnitType = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "HandlingUnitType")
        }
        if !HUSingleItem.itemQuantity.isRemoved {
            HUSingleItem.itemQuantity = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "ItemQuantity")
        }
        if !HUSingleItem.itemQuantityUnit.isRemoved {
            HUSingleItem.itemQuantityUnit = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "ItemQuantityUnit")
        }
        if !HUSingleItem.inboundDeliveryUUID.isRemoved {
            HUSingleItem.inboundDeliveryUUID = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "InboundDeliveryUUID")
        }
        if !HUSingleItem.inboundDeliveryItemUUID.isRemoved {
            HUSingleItem.inboundDeliveryItemUUID = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "InboundDeliveryItemUUID")
        }
        if !HUSingleItem.warehouseNumber.isRemoved {
            HUSingleItem.warehouseNumber = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "WarehouseNumber")
        }
        if !HUSingleItem.lastChangedDateTime.isRemoved {
            HUSingleItem.lastChangedDateTime = Ec1Metadata.EntityTypes.huSingleItem.property(withName: "LastChangedDateTime")
        }
        if !Item.parentID.isRemoved {
            Item.parentID = Ec1Metadata.EntityTypes.item.property(withName: "ParentId")
        }
        if !Item.stockID.isRemoved {
            Item.stockID = Ec1Metadata.EntityTypes.item.property(withName: "StockId")
        }
        if !Item.docID.isRemoved {
            Item.docID = Ec1Metadata.EntityTypes.item.property(withName: "DocId")
        }
        if !Item.itmID.isRemoved {
            Item.itmID = Ec1Metadata.EntityTypes.item.property(withName: "ItmId")
        }
        if !Item.consGrp.isRemoved {
            Item.consGrp = Ec1Metadata.EntityTypes.item.property(withName: "ConsGrp")
        }
        if !Item.baseUnit.isRemoved {
            Item.baseUnit = Ec1Metadata.EntityTypes.item.property(withName: "BaseUnit")
        }
        if !Item.alterUnit.isRemoved {
            Item.alterUnit = Ec1Metadata.EntityTypes.item.property(withName: "AlterUnit")
        }
        if !Item.quan.isRemoved {
            Item.quan = Ec1Metadata.EntityTypes.item.property(withName: "Quan")
        }
        if !Item.alterQuan.isRemoved {
            Item.alterQuan = Ec1Metadata.EntityTypes.item.property(withName: "AlterQuan")
        }
        if !Item.docNo.isRemoved {
            Item.docNo = Ec1Metadata.EntityTypes.item.property(withName: "DocNo")
        }
        if !Item.huID.isRemoved {
            Item.huID = Ec1Metadata.EntityTypes.item.property(withName: "HuId")
        }
        if !Item.batch.isRemoved {
            Item.batch = Ec1Metadata.EntityTypes.item.property(withName: "Batch")
        }
        if !Item.snReq.isRemoved {
            Item.snReq = Ec1Metadata.EntityTypes.item.property(withName: "SnReq")
        }
        if !Item.snList.isRemoved {
            Item.snList = Ec1Metadata.EntityTypes.item.property(withName: "SnList")
        }
        if !Item.product.isRemoved {
            Item.product = Ec1Metadata.EntityTypes.item.property(withName: "Product")
        }
        if !Item.productDesc.isRemoved {
            Item.productDesc = Ec1Metadata.EntityTypes.item.property(withName: "ProductDesc")
        }
        if !Item.productWeight.isRemoved {
            Item.productWeight = Ec1Metadata.EntityTypes.item.property(withName: "ProductWeight")
        }
        if !Item.productUoM.isRemoved {
            Item.productUoM = Ec1Metadata.EntityTypes.item.property(withName: "ProductUoM")
        }
        if !Item.type_.isRemoved {
            Item.type_ = Ec1Metadata.EntityTypes.item.property(withName: "Type")
        }
        if !Item.workstation.isRemoved {
            Item.workstation = Ec1Metadata.EntityTypes.item.property(withName: "Workstation")
        }
        if !Item.handlingInstr.isRemoved {
            Item.handlingInstr = Ec1Metadata.EntityTypes.item.property(withName: "HandlingInstr")
        }
        if !Item.ean.isRemoved {
            Item.ean = Ec1Metadata.EntityTypes.item.property(withName: "EAN")
        }
        if !Item.prdtPicURL.isRemoved {
            Item.prdtPicURL = Ec1Metadata.EntityTypes.item.property(withName: "PrdtPicURL")
        }
        if !Item.lgnum.isRemoved {
            Item.lgnum = Ec1Metadata.EntityTypes.item.property(withName: "Lgnum")
        }
        if !Item.bin.isRemoved {
            Item.bin = Ec1Metadata.EntityTypes.item.property(withName: "Bin")
        }
        if !Item.qtyReduced.isRemoved {
            Item.qtyReduced = Ec1Metadata.EntityTypes.item.property(withName: "QtyReduced")
        }
        if !Item.isSplit.isRemoved {
            Item.isSplit = Ec1Metadata.EntityTypes.item.property(withName: "isSplit")
        }
        if !Item.isIuidActive.isRemoved {
            Item.isIuidActive = Ec1Metadata.EntityTypes.item.property(withName: "isIuidActive")
        }
        if !Item.stockType.isRemoved {
            Item.stockType = Ec1Metadata.EntityTypes.item.property(withName: "StockType")
        }
        if !Item.stockTypeText.isRemoved {
            Item.stockTypeText = Ec1Metadata.EntityTypes.item.property(withName: "StockTypeText")
        }
        if !Item.weight.isRemoved {
            Item.weight = Ec1Metadata.EntityTypes.item.property(withName: "Weight")
        }
        if !Item.weightUoM.isRemoved {
            Item.weightUoM = Ec1Metadata.EntityTypes.item.property(withName: "WeightUoM")
        }
        if !Item.volume.isRemoved {
            Item.volume = Ec1Metadata.EntityTypes.item.property(withName: "Volume")
        }
        if !Item.volumeUoM.isRemoved {
            Item.volumeUoM = Ec1Metadata.EntityTypes.item.property(withName: "VolumeUoM")
        }
        if !OUTBDLVHead.carrier.isRemoved {
            OUTBDLVHead.carrier = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "Carrier")
        }
        if !OUTBDLVHead.carrierName.isRemoved {
            OUTBDLVHead.carrierName = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "CarrierName")
        }
        if !OUTBDLVHead.goodsIssueStatus.isRemoved {
            OUTBDLVHead.goodsIssueStatus = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "GoodsIssueStatus")
        }
        if !OUTBDLVHead.goodsIssueStatusName.isRemoved {
            OUTBDLVHead.goodsIssueStatusName = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "GoodsIssueStatusName")
        }
        if !OUTBDLVHead.lastChangedDateTime.isRemoved {
            OUTBDLVHead.lastChangedDateTime = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "LastChangedDateTime")
        }
        if !OUTBDLVHead.maxWeight.isRemoved {
            OUTBDLVHead.maxWeight = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "MaxWeight")
        }
        if !OUTBDLVHead.maxWeightUnit.isRemoved {
            OUTBDLVHead.maxWeightUnit = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "MaxWeightUnit")
        }
        if !OUTBDLVHead.meansOfTransport.isRemoved {
            OUTBDLVHead.meansOfTransport = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "MeansOfTransport")
        }
        if !OUTBDLVHead.meansOfTransportName.isRemoved {
            OUTBDLVHead.meansOfTransportName = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "MeansOfTransportName")
        }
        if !OUTBDLVHead.numberOfItems.isRemoved {
            OUTBDLVHead.numberOfItems = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "NumberOfItems")
        }
        if !OUTBDLVHead.numberOfTranspUnit.isRemoved {
            OUTBDLVHead.numberOfTranspUnit = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "NumberOfTranspUnit")
        }
        if !OUTBDLVHead.outboundDelivery.isRemoved {
            OUTBDLVHead.outboundDelivery = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "OutboundDelivery")
        }
        if !OUTBDLVHead.outboundDeliveryUUID.isRemoved {
            OUTBDLVHead.outboundDeliveryUUID = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "OutboundDeliveryUUID")
        }
        if !OUTBDLVHead.outOfYardDatePlan.isRemoved {
            OUTBDLVHead.outOfYardDatePlan = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "OutOfYardDatePlan")
        }
        if !OUTBDLVHead.completionStatusName.isRemoved {
            OUTBDLVHead.completionStatusName = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "CompletionStatusName")
        }
        if !OUTBDLVHead.pickingStatusName.isRemoved {
            OUTBDLVHead.pickingStatusName = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PickingStatusName")
        }
        if !OUTBDLVHead.pickingStatusPercent.isRemoved {
            OUTBDLVHead.pickingStatusPercent = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PickingStatusPercent")
        }
        if !OUTBDLVHead.plannedPickingStatus.isRemoved {
            OUTBDLVHead.plannedPickingStatus = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PlannedPickingStatus")
        }
        if !OUTBDLVHead.plannedPickingStatusName.isRemoved {
            OUTBDLVHead.plannedPickingStatusName = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PlannedPickingStatusName")
        }
        if !OUTBDLVHead.route.isRemoved {
            OUTBDLVHead.route = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "Route")
        }
        if !OUTBDLVHead.routeName.isRemoved {
            OUTBDLVHead.routeName = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "RouteName")
        }
        if !OUTBDLVHead.shipToParty.isRemoved {
            OUTBDLVHead.shipToParty = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ShipToParty")
        }
        if !OUTBDLVHead.shipToPartyName.isRemoved {
            OUTBDLVHead.shipToPartyName = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ShipToPartyName")
        }
        if !OUTBDLVHead.stagingArea.isRemoved {
            OUTBDLVHead.stagingArea = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "StagingArea")
        }
    }

    private static func merge4(metadata: CSDLDocument) {
        Ignore.valueOf_any(metadata)
        if !OUTBDLVHead.stagingAreaGroup.isRemoved {
            OUTBDLVHead.stagingAreaGroup = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "StagingAreaGroup")
        }
        if !OUTBDLVHead.stagingAreaBin.isRemoved {
            OUTBDLVHead.stagingAreaBin = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "StagingAreaBin")
        }
        if !OUTBDLVHead.transpUnitActivityNumber.isRemoved {
            OUTBDLVHead.transpUnitActivityNumber = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "TranspUnitActivityNumber")
        }
        if !OUTBDLVHead.transpUnitExternalNumber.isRemoved {
            OUTBDLVHead.transpUnitExternalNumber = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "TranspUnitExternalNumber")
        }
        if !OUTBDLVHead.transpUnitInternalNumber.isRemoved {
            OUTBDLVHead.transpUnitInternalNumber = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "TranspUnitInternalNumber")
        }
        if !OUTBDLVHead.warehouseDoor.isRemoved {
            OUTBDLVHead.warehouseDoor = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "WarehouseDoor")
        }
        if !OUTBDLVHead.docNoERP.isRemoved {
            OUTBDLVHead.docNoERP = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "DocNoERP")
        }
        if !OUTBDLVHead.bskeyERP.isRemoved {
            OUTBDLVHead.bskeyERP = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "BskeyERP")
        }
        if !OUTBDLVHead.salesOrder.isRemoved {
            OUTBDLVHead.salesOrder = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "SalesOrder")
        }
        if !OUTBDLVHead.numberOfSalesOrder.isRemoved {
            OUTBDLVHead.numberOfSalesOrder = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "NumberOfSalesOrder")
        }
        if !OUTBDLVHead.warehouseNumber.isRemoved {
            OUTBDLVHead.warehouseNumber = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "WarehouseNumber")
        }
        if !OUTBDLVHead.shippingConditionCode.isRemoved {
            OUTBDLVHead.shippingConditionCode = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ShippingConditionCode")
        }
        if !OUTBDLVHead.shippingConditionName.isRemoved {
            OUTBDLVHead.shippingConditionName = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ShippingConditionName")
        }
        if !OUTBDLVHead.numberOfShippingCondition.isRemoved {
            OUTBDLVHead.numberOfShippingCondition = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "NumberOfShippingCondition")
        }
        if !OUTBDLVHead.includeCompletedDelivery.isRemoved {
            OUTBDLVHead.includeCompletedDelivery = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "IncludeCompletedDelivery")
        }
        if !OUTBDLVHead.uxFcWarehouseDoor.isRemoved {
            OUTBDLVHead.uxFcWarehouseDoor = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_WarehouseDoor")
        }
        if !OUTBDLVHead.uxFcOutOfYardDatePlan.isRemoved {
            OUTBDLVHead.uxFcOutOfYardDatePlan = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_OutOfYardDatePlan")
        }
        if !OUTBDLVHead.uxFcCarrier.isRemoved {
            OUTBDLVHead.uxFcCarrier = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_Carrier")
        }
        if !OUTBDLVHead.uxFcStagingArea.isRemoved {
            OUTBDLVHead.uxFcStagingArea = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_StagingArea")
        }
        if !OUTBDLVHead.uxFcStagingAreaGroup.isRemoved {
            OUTBDLVHead.uxFcStagingAreaGroup = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_StagingAreaGroup")
        }
        if !OUTBDLVHead.uxFcStagingAreaBin.isRemoved {
            OUTBDLVHead.uxFcStagingAreaBin = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_StagingAreaBin")
        }
        if !OUTBDLVHead.uxFcADeletable.isRemoved {
            OUTBDLVHead.uxFcADeletable = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFcA_Deletable")
        }
        if !OUTBDLVHead.uxFcAGoodsIssue.isRemoved {
            OUTBDLVHead.uxFcAGoodsIssue = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFcA_GoodsIssue")
        }
        if !OUTBDLVHead.uxFcAUpdatable.isRemoved {
            OUTBDLVHead.uxFcAUpdatable = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFcA_Updatable")
        }
        if !OUTBDLVHead.outboundDeliveryType.isRemoved {
            OUTBDLVHead.outboundDeliveryType = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "OutboundDeliveryType")
        }
        if !OUTBDLVHead.outboundDeliveryTypeText.isRemoved {
            OUTBDLVHead.outboundDeliveryTypeText = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "OutboundDeliveryTypeText")
        }
        if !OUTBDLVHead.manufacturingOrder.isRemoved {
            OUTBDLVHead.manufacturingOrder = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ManufacturingOrder")
        }
        if !OUTBDLVHead.numberOfManufacturingOrders.isRemoved {
            OUTBDLVHead.numberOfManufacturingOrders = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "NumberOfManufacturingOrders")
        }
        if !OUTBDLVHead.deliveryDatePlan.isRemoved {
            OUTBDLVHead.deliveryDatePlan = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "DeliveryDatePlan")
        }
        if !OUTBDLVHead.psa.isRemoved {
            OUTBDLVHead.psa = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PSA")
        }
        if !OUTBDLVHead.uxFcDeliveryDatePlan.isRemoved {
            OUTBDLVHead.uxFcDeliveryDatePlan = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "UxFc_DeliveryDatePlan")
        }
        if !OUTBDLVHead.packingStatus.isRemoved {
            OUTBDLVHead.packingStatus = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PackingStatus")
        }
        if !OUTBDLVHead.packingStatusName.isRemoved {
            OUTBDLVHead.packingStatusName = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "PackingStatusName")
        }
        if !OUTBDLVHead.productionIndicator.isRemoved {
            OUTBDLVHead.productionIndicator = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ProductionIndicator")
        }
        if !OUTBDLVHead.blockedOverallStatus.isRemoved {
            OUTBDLVHead.blockedOverallStatus = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "BlockedOverallStatus")
        }
        if !OUTBDLVHead.blockedOverallStatusName.isRemoved {
            OUTBDLVHead.blockedOverallStatusName = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "BlockedOverallStatusName")
        }
        if !OUTBDLVHead.existsWT.isRemoved {
            OUTBDLVHead.existsWT = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "ExistsWT")
        }
        if !OUTBDLVHead.completionStatus.isRemoved {
            OUTBDLVHead.completionStatus = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "CompletionStatus")
        }
        if !OUTBDLVHead.items.isRemoved {
            OUTBDLVHead.items = Ec1Metadata.EntityTypes.outbdlvHead.property(withName: "Items")
        }
        if !OUTBDLVItem.batch.isRemoved {
            OUTBDLVItem.batch = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "Batch")
        }
        if !OUTBDLVItem.goodsIssueStatus.isRemoved {
            OUTBDLVItem.goodsIssueStatus = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "GoodsIssueStatus")
        }
        if !OUTBDLVItem.goodsIssueStatusName.isRemoved {
            OUTBDLVItem.goodsIssueStatusName = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "GoodsIssueStatusName")
        }
        if !OUTBDLVItem.itemDeliveryQuantity.isRemoved {
            OUTBDLVItem.itemDeliveryQuantity = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "ItemDeliveryQuantity")
        }
        if !OUTBDLVItem.itemDeliveryQuantityUnit.isRemoved {
            OUTBDLVItem.itemDeliveryQuantityUnit = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "ItemDeliveryQuantityUnit")
        }
        if !OUTBDLVItem.lastChangedDateTime.isRemoved {
            OUTBDLVItem.lastChangedDateTime = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "LastChangedDateTime")
        }
        if !OUTBDLVItem.numberOfOpenWrhsTasks.isRemoved {
            OUTBDLVItem.numberOfOpenWrhsTasks = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "NumberOfOpenWrhsTasks")
        }
        if !OUTBDLVItem.outboundDelivery.isRemoved {
            OUTBDLVItem.outboundDelivery = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "OutboundDelivery")
        }
        if !OUTBDLVItem.outboundDeliveryItem.isRemoved {
            OUTBDLVItem.outboundDeliveryItem = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "OutboundDeliveryItem")
        }
        if !OUTBDLVItem.outboundDeliveryOrderItemUUID.isRemoved {
            OUTBDLVItem.outboundDeliveryOrderItemUUID = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "OutboundDeliveryOrderItemUUID")
        }
        if !OUTBDLVItem.outboundDeliveryUUID.isRemoved {
            OUTBDLVItem.outboundDeliveryUUID = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "OutboundDeliveryUUID")
        }
        if !OUTBDLVItem.plannedPickingStatus.isRemoved {
            OUTBDLVItem.plannedPickingStatus = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "PlannedPickingStatus")
        }
        if !OUTBDLVItem.plannedPickingStatusName.isRemoved {
            OUTBDLVItem.plannedPickingStatusName = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "PlannedPickingStatusName")
        }
        if !OUTBDLVItem.product.isRemoved {
            OUTBDLVItem.product = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "Product")
        }
        if !OUTBDLVItem.productName.isRemoved {
            OUTBDLVItem.productName = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "ProductName")
        }
        if !OUTBDLVItem.sledBbd.isRemoved {
            OUTBDLVItem.sledBbd = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "SledBbd")
        }
        if !OUTBDLVItem.numberOfSledBbd.isRemoved {
            OUTBDLVItem.numberOfSledBbd = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "NumberOfSledBbd")
        }
        if !OUTBDLVItem.uxFcAAdjustDeliveryQuantities.isRemoved {
            OUTBDLVItem.uxFcAAdjustDeliveryQuantities = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "UxFcA_AdjustDeliveryQuantities")
        }
        if !OUTBDLVItem.uxFcACreateTasks.isRemoved {
            OUTBDLVItem.uxFcACreateTasks = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "UxFcA_CreateTasks")
        }
        if !OUTBDLVItem.uxFcADeletable.isRemoved {
            OUTBDLVItem.uxFcADeletable = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "UxFcA_Deletable")
        }
        if !OUTBDLVItem.uxFcAUpdatable.isRemoved {
            OUTBDLVItem.uxFcAUpdatable = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "UxFcA_Updatable")
        }
        if !OUTBDLVItem.warehouseNumber.isRemoved {
            OUTBDLVItem.warehouseNumber = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "WarehouseNumber")
        }
        if !OUTBDLVItem.entitled.isRemoved {
            OUTBDLVItem.entitled = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "Entitled")
        }
        if !OUTBDLVItem.missingWTQty.isRemoved {
            OUTBDLVItem.missingWTQty = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "MissingWTQty")
        }
        if !OUTBDLVItem.missingWTQtyUoM.isRemoved {
            OUTBDLVItem.missingWTQtyUoM = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "MissingWTQtyUoM")
        }
        if !OUTBDLVItem.openWTQty.isRemoved {
            OUTBDLVItem.openWTQty = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "OpenWTQty")
        }
        if !OUTBDLVItem.openWTQtyUoM.isRemoved {
            OUTBDLVItem.openWTQtyUoM = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "OpenWTQtyUoM")
        }
        if !OUTBDLVItem.existsWT.isRemoved {
            OUTBDLVItem.existsWT = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "ExistsWT")
        }
        if !OUTBDLVItem.stockDocument.isRemoved {
            OUTBDLVItem.stockDocument = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "StockDocument")
        }
        if !OUTBDLVItem.stockItem.isRemoved {
            OUTBDLVItem.stockItem = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "StockItem")
        }
        if !OUTBDLVItem.stockDocumentType.isRemoved {
            OUTBDLVItem.stockDocumentType = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "StockDocumentType")
        }
        if !OUTBDLVItem.stockDocumentTypeText.isRemoved {
            OUTBDLVItem.stockDocumentTypeText = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "StockDocumentTypeText")
        }
        if !OUTBDLVItem.manufacturingOrder.isRemoved {
            OUTBDLVItem.manufacturingOrder = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "ManufacturingOrder")
        }
        if !OUTBDLVItem.psa.isRemoved {
            OUTBDLVItem.psa = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "PSA")
        }
        if !OUTBDLVItem.packingStatus.isRemoved {
            OUTBDLVItem.packingStatus = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "PackingStatus")
        }
        if !OUTBDLVItem.packingStatusName.isRemoved {
            OUTBDLVItem.packingStatusName = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "PackingStatusName")
        }
        if !OUTBDLVItem.blockedOverallStatus.isRemoved {
            OUTBDLVItem.blockedOverallStatus = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "BlockedOverallStatus")
        }
        if !OUTBDLVItem.blockedOverallStatusName.isRemoved {
            OUTBDLVItem.blockedOverallStatusName = Ec1Metadata.EntityTypes.outbdlvItem.property(withName: "BlockedOverallStatusName")
        }
        if !PerformPackType.lgnum.isRemoved {
            PerformPackType.lgnum = Ec1Metadata.EntityTypes.performPackType.property(withName: "Lgnum")
        }
        if !PerformPackType.workstation.isRemoved {
            PerformPackType.workstation = Ec1Metadata.EntityTypes.performPackType.property(withName: "Workstation")
        }
        if !PerformPackType.bin.isRemoved {
            PerformPackType.bin = Ec1Metadata.EntityTypes.performPackType.property(withName: "Bin")
        }
        if !PerformPackType.sourceID.isRemoved {
            PerformPackType.sourceID = Ec1Metadata.EntityTypes.performPackType.property(withName: "SourceId")
        }
        if !PerformPackType.shippingHUID.isRemoved {
            PerformPackType.shippingHUID = Ec1Metadata.EntityTypes.performPackType.property(withName: "ShippingHUId")
        }
        if !PerformPackType.sourceType.isRemoved {
            PerformPackType.sourceType = Ec1Metadata.EntityTypes.performPackType.property(withName: "SourceType")
        }
        if !PerformPackType.isPackAll.isRemoved {
            PerformPackType.isPackAll = Ec1Metadata.EntityTypes.performPackType.property(withName: "IsPackAll")
        }
        if !PerformPackType.packMat.isRemoved {
            PerformPackType.packMat = Ec1Metadata.EntityTypes.performPackType.property(withName: "PackMat")
        }
        if !PerformPackType.stockID.isRemoved {
            PerformPackType.stockID = Ec1Metadata.EntityTypes.performPackType.property(withName: "StockId")
        }
        if !PerformPackType.srcQuan.isRemoved {
            PerformPackType.srcQuan = Ec1Metadata.EntityTypes.performPackType.property(withName: "SrcQuan")
        }
        if !PerformPackType.srcAlterQuan.isRemoved {
            PerformPackType.srcAlterQuan = Ec1Metadata.EntityTypes.performPackType.property(withName: "SrcAlterQuan")
        }
        if !PerformPackType.desQuan.isRemoved {
            PerformPackType.desQuan = Ec1Metadata.EntityTypes.performPackType.property(withName: "DesQuan")
        }
        if !PerformPackType.desAlterQuan.isRemoved {
            PerformPackType.desAlterQuan = Ec1Metadata.EntityTypes.performPackType.property(withName: "DesAlterQuan")
        }
        if !PerformPackType.desBaseUoM.isRemoved {
            PerformPackType.desBaseUoM = Ec1Metadata.EntityTypes.performPackType.property(withName: "DesBaseUoM")
        }
        if !PerformPackType.desAlterUoM.isRemoved {
            PerformPackType.desAlterUoM = Ec1Metadata.EntityTypes.performPackType.property(withName: "DesAlterUoM")
        }
        if !PerformPackType.packmatDesc.isRemoved {
            PerformPackType.packmatDesc = Ec1Metadata.EntityTypes.performPackType.property(withName: "PackmatDesc")
        }
        if !PerformPackType.packmatID.isRemoved {
            PerformPackType.packmatID = Ec1Metadata.EntityTypes.performPackType.property(withName: "PackmatId")
        }
        if !PerformPackType.product.isRemoved {
            PerformPackType.product = Ec1Metadata.EntityTypes.performPackType.property(withName: "Product")
        }
        if !PerformPackType.huID.isRemoved {
            PerformPackType.huID = Ec1Metadata.EntityTypes.performPackType.property(withName: "HuId")
        }
        if !PerformPackType.loadingWeight.isRemoved {
            PerformPackType.loadingWeight = Ec1Metadata.EntityTypes.performPackType.property(withName: "LoadingWeight")
        }
        if !PerformPackType.weightUoM.isRemoved {
            PerformPackType.weightUoM = Ec1Metadata.EntityTypes.performPackType.property(withName: "WeightUoM")
        }
        if !PerformPackType.msgID.isRemoved {
            PerformPackType.msgID = Ec1Metadata.EntityTypes.performPackType.property(withName: "MsgId")
        }
        if !PerformPackType.msgVar.isRemoved {
            PerformPackType.msgVar = Ec1Metadata.EntityTypes.performPackType.property(withName: "MsgVar")
        }
        if !PerformPackType.msgKey.isRemoved {
            PerformPackType.msgKey = Ec1Metadata.EntityTypes.performPackType.property(withName: "MsgKey")
        }
        if !PerformPackType.msgType.isRemoved {
            PerformPackType.msgType = Ec1Metadata.EntityTypes.performPackType.property(withName: "MsgType")
        }
        if !PerformPackType.msgSuccess.isRemoved {
            PerformPackType.msgSuccess = Ec1Metadata.EntityTypes.performPackType.property(withName: "MsgSuccess")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.id.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.id = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.processingTypeCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.processingTypeCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProcessingTypeCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.date.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.date = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Date")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.supplyingPlantID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.supplyingPlantID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SupplyingPlantID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.responsiblePurchasingOrganisationParty.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.responsiblePurchasingOrganisationParty = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ResponsiblePurchasingOrganisationParty")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.responsiblePurchasingGroupParty.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.responsiblePurchasingGroupParty = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ResponsiblePurchasingGroupParty")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.internalID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.internalID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InternalID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.languageCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.languageCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "languageCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.item.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.item = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Item")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.uuid.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.uuid = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "UUID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.referenceID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.referenceID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ReferenceID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.referenceUUID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.referenceUUID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ReferenceUUID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.creationDate.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.creationDate = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CreationDate")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.creationUserAccountID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.creationUserAccountID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CreationUserAccountID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.supplyingPlantName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.supplyingPlantName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SupplyingPlantName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.completenessAndValidationStatusCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.completenessAndValidationStatusCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CompletenessAndValidationStatusCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.releaseStatusCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.releaseStatusCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ReleaseStatusCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.unitCurrency.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.unitCurrency = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "UnitCurrency")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.quotedCurrency.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.quotedCurrency = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "QuotedCurrency")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.rate.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.rate = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Rate")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.quotationDateTime.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.quotationDateTime = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "QuotationDateTime")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.exchangeRateFixedIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.exchangeRateFixedIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ExchangeRateFixedIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.sellerReferenceNote.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.sellerReferenceNote = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SellerReferenceNote")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.buyerReferenceNote.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.buyerReferenceNote = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BuyerReferenceNote")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.formattedName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.formattedName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FormattedName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.roleCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.roleCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RoleCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.classificationCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.classificationCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ClassificationCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.transferLocationName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.transferLocationName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "TransferLocationName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.code.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.code = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Code")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.daysValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.daysValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DaysValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.dayOfMonthValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.dayOfMonthValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DayOfMonthValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.monthOffsetValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.monthOffsetValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MonthOffsetValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.endDate.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.endDate = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "EndDate")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.percent.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.percent = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Percent")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.fullPaymentDueDaysValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.fullPaymentDueDaysValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FullPaymentDueDaysValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.typeCode_.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.typeCode_ = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "TypeCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.majorLevelOrdinalNumberValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.majorLevelOrdinalNumberValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MajorLevelOrdinalNumberValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.minorLevelOrdinalNumberValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.minorLevelOrdinalNumberValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MinorLevelOrdinalNumberValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.decimalValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.decimalValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DecimalValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.measureUnitCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.measureUnitCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MeasureUnitCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.currencyCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.currencyCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CurrencyCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.baseDecimalValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.baseDecimalValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BaseDecimalValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.baseMeasureUnitCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.baseMeasureUnitCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BaseMeasureUnitCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.baseCurrencyCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.baseCurrencyCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BaseCurrencyCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.groupID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.groupID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "GroupID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.expectedPurchasingDocumentItemConfirmationTypeCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.expectedPurchasingDocumentItemConfirmationTypeCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ExpectedPurchasingDocumentItemConfirmationTypeCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.blockedIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.blockedIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BlockedIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.returnsIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.returnsIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ReturnsIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.deliveryBasedInvoiceVerificationIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.deliveryBasedInvoiceVerificationIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DeliveryBasedInvoiceVerificationIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.evaluatedReceiptSettlementIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.evaluatedReceiptSettlementIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "EvaluatedReceiptSettlementIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.inboundDeliveryCompletedIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.inboundDeliveryCompletedIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InboundDeliveryCompletedIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.outboundDeliveryCompletedIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.outboundDeliveryCompletedIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "OutboundDeliveryCompletedIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.invoiceVerificationCompletedIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.invoiceVerificationCompletedIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InvoiceVerificationCompletedIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.unitCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.unitCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "unitCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.requesterPersonFormattedName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.requesterPersonFormattedName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RequesterPersonFormattedName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.receivingPlantID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.receivingPlantID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ReceivingPlantID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.productProcurementArrangmentID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.productProcurementArrangmentID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProductProcurementArrangmentID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.retailEventID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.retailEventID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RetailEventID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.retailAllocationID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.retailAllocationID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RetailAllocationID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.sellerAssortmentProductGroupID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.sellerAssortmentProductGroupID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SellerAssortmentProductGroupID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.currencyCode2.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.currencyCode2 = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "currencyCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.baseQuantityTypeCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.baseQuantityTypeCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BaseQuantityTypeCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.requirementCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.requirementCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RequirementCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.parentItemID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.parentItemID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ParentItemID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.description.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.description = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Description")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.addressID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.addressID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AddressID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.organisationFormattedName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.organisationFormattedName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "OrganisationFormattedName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.formOfAddressCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.formOfAddressCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FormOfAddressCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.formOfAddressName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.formOfAddressName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FormOfAddressName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.givenName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.givenName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "GivenName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.middleName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.middleName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MiddleName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.familyName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.familyName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FamilyName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.additionalFamilyName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.additionalFamilyName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalFamilyName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.birthName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.birthName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BirthName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.nickName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.nickName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NickName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.initialsName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.initialsName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InitialsName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.academicTitleCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.academicTitleCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AcademicTitleCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.academicTitleName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.academicTitleName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AcademicTitleName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.additionalAcademicTitleCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.additionalAcademicTitleCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalAcademicTitleCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.additionalAcademicTitleName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.additionalAcademicTitleName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalAcademicTitleName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.namePrefixCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.namePrefixCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NamePrefixCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.namePrefixName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.namePrefixName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NamePrefixName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.additionalNamePrefixCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.additionalNamePrefixCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalNamePrefixCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.additionalNamePrefixName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.additionalNamePrefixName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalNamePrefixName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.nameSupplementCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.nameSupplementCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NameSupplementCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.nameSupplementName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.nameSupplementName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NameSupplementName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.deviatingFullName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.deviatingFullName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DeviatingFullName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.nameFormatCountryCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.nameFormatCountryCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NameFormatCountryCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.nameFormatCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.nameFormatCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NameFormatCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.functionalTitleName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.functionalTitleName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FunctionalTitleName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.departmentName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.departmentName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DepartmentName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.buildingID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.buildingID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BuildingID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.floorID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.floorID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FloorID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.roomID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.roomID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RoomID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.inhouseMailID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.inhouseMailID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InhouseMailID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.correspondenceShortName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.correspondenceShortName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CorrespondenceShortName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.countryCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.countryCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CountryCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.regionCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.regionCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RegionCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.regionName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.regionName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RegionName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.streetPostalCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.streetPostalCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "StreetPostalCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.poBoxPostalCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.poBoxPostalCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxPostalCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.companyPostalCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.companyPostalCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CompanyPostalCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.cityName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.cityName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CityName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.additionalCityName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.additionalCityName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalCityName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.districtName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.districtName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DistrictName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.poBoxID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.poBoxID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.poBoxIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.poBoxIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.poBoxCountryCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.poBoxCountryCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxCountryCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.poBoxRegionCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.poBoxRegionCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxRegionCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.poBoxRegionName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.poBoxRegionName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxRegionName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.poBoxCityName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.poBoxCityName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "POBoxCityName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.streetName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.streetName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "StreetName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.streetPrefixName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.streetPrefixName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "StreetPrefixName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.streetSuffixName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.streetSuffixName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "StreetSuffixName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.houseID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.houseID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "HouseID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.additionalHouseID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.additionalHouseID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AdditionalHouseID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.careOfName.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.careOfName = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CareOfName")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.taxJurisdictionCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.taxJurisdictionCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "TaxJurisdictionCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.timeZoneDifferenceValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.timeZoneDifferenceValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "TimeZoneDifferenceValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.correspondenceLanguageCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.correspondenceLanguageCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CorrespondenceLanguageCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.areaID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.areaID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AreaID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.subscriberID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.subscriberID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SubscriberID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.extensionID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.extensionID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ExtensionID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.countryDiallingCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.countryDiallingCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CountryDiallingCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.smsEnabledIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.smsEnabledIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SMSEnabledIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.numberDefaultIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.numberDefaultIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NumberDefaultIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.numberUsageDenialIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.numberUsageDenialIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "NumberUsageDenialIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.uri.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.uri = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "URI")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.uriDefaultIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.uriDefaultIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "URIDefaultIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.uriUsageDenialIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.uriUsageDenialIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "URIUsageDenialIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.addressGroupCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.addressGroupCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AddressGroupCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.buyerID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.buyerID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BuyerID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.sellerID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.sellerID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SellerID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.revisionID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.revisionID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RevisionID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.inventoryValuationTypeCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.inventoryValuationTypeCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InventoryValuationTypeCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductSpecificationTextCatalogueID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductSpecificationTextCatalogueID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ServiceProductSpecificationTextCatalogueID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductSpecificationTextCatalogueItemID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductSpecificationTextCatalogueItemID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ServiceProductSpecificationTextCatalogueItemID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.alternateAllowedIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.alternateAllowedIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AlternateAllowedIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.byBidderProvidedIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.byBidderProvidedIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ByBidderProvidedIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.supplementaryIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.supplementaryIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SupplementaryIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.priceChangeAllowedIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.priceChangeAllowedIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "PriceChangeAllowedIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.compensationComponentPayrollCategoryCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.compensationComponentPayrollCategoryCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CompensationComponentPayrollCategoryCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.functionCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.functionCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FunctionCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.firstInputParameterValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.firstInputParameterValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FirstInputParameterValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.secondInputParameterValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.secondInputParameterValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SecondInputParameterValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.thirdInputParameterValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.thirdInputParameterValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ThirdInputParameterValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.fourthInputParameterValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.fourthInputParameterValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FourthInputParameterValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.fifthInputParameterValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.fifthInputParameterValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FifthInputParameterValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.startDate.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.startDate = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "StartDate")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.deliveryPriorityCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.deliveryPriorityCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DeliveryPriorityCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.overPercent.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.overPercent = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "OverPercent")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.overPercentUnlimitedIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.overPercentUnlimitedIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "OverPercentUnlimitedIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.underPercent.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.underPercent = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "UnderPercent")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.transportServiceLevelCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.transportServiceLevelCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "TransportServiceLevelCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.productTaxationCharacteristicsCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.productTaxationCharacteristicsCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProductTaxationCharacteristicsCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.amountUnlimitedIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.amountUnlimitedIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AmountUnlimitedIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.costElementID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.costElementID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CostElementID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductHierarchyItemSpecificationID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.serviceProductHierarchyItemSpecificationID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ServiceProductHierarchyItemSpecificationID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.purchasingContractID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.purchasingContractID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "PurchasingContractID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.purchasingContractItemID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.purchasingContractItemID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "PurchasingContractItemID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.ordinalNumberValue.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.ordinalNumberValue = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "OrdinalNumberValue")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.profitCentreID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.profitCentreID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProfitCentreID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.costCentreID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.costCentreID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CostCentreID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.projectElementID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.projectElementID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProjectElementID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.projectElementTypeCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.projectElementTypeCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProjectElementTypeCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementCentreID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementCentreID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FundsManagementCentreID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.internalOrderID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.internalOrderID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "InternalOrderID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.salesOrderID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.salesOrderID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SalesOrderID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.salesOrderItemID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.salesOrderItemID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SalesOrderItemID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.productionOrderID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.productionOrderID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProductionOrderID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.maintenanceOrderID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.maintenanceOrderID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MaintenanceOrderID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementFundID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementFundID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FundsManagementFundID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementAccountID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.fundsManagementAccountID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FundsManagementAccountID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.grantID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.grantID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "GrantID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.accountingCodingBlockTypeCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.accountingCodingBlockTypeCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AccountingCodingBlockTypeCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.accountDeterminationExpenseGroupCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.accountDeterminationExpenseGroupCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AccountDeterminationExpenseGroupCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.accountingBusinessAreaCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.accountingBusinessAreaCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "AccountingBusinessAreaCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.masterFixedAssetID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.masterFixedAssetID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MasterFixedAssetID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.fixedAssetID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.fixedAssetID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FixedAssetID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.quoteID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.quoteID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "QuoteID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.quoteItemID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.quoteItemID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "QuoteItemID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.purchaseRequestID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.purchaseRequestID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "PurchaseRequestID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.purchaseRequestItemID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.purchaseRequestItemID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "PurchaseRequestItemID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.deliveryDateTime.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.deliveryDateTime = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DeliveryDateTime")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.deliveryYearWeek.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.deliveryYearWeek = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DeliveryYearWeek")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.deliveryYearMonth.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.deliveryYearMonth = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "DeliveryYearMonth")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.productBuyerID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.productBuyerID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "ProductBuyerID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.batchID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.batchID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BatchID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.requirementDate.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.requirementDate = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "RequirementDate")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.quantityFixedIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.quantityFixedIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "QuantityFixedIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.followUpProcessesRelevanceIndicator.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.followUpProcessesRelevanceIndicator = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "FollowUpProcessesRelevanceIndicator")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.businessDocumentProcessingResultCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.businessDocumentProcessingResultCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "BusinessDocumentProcessingResultCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.maximumLogItemSeverityCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.maximumLogItemSeverityCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "MaximumLogItemSeverityCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.typeID.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.typeID = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "TypeID")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.categoryCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.categoryCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "CategoryCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.severityCode.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.severityCode = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "SeverityCode")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.note.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.note = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "Note")
        }
        if !PurchaseOrderERPCreateRequestConfirmationInV1.webURI.isRemoved {
            PurchaseOrderERPCreateRequestConfirmationInV1.webURI = Ec1Metadata.EntityTypes.purchaseOrderERPCreateRequestConfirmationInV1.property(withName: "WebURI")
        }
        if !WTSimpleConfirm.lgnum.isRemoved {
            WTSimpleConfirm.lgnum = Ec1Metadata.EntityTypes.wtSimpleConfirm.property(withName: "Lgnum")
        }
        if !WTSimpleConfirm.tanum.isRemoved {
            WTSimpleConfirm.tanum = Ec1Metadata.EntityTypes.wtSimpleConfirm.property(withName: "Tanum")
        }
        if !WarehouseTask.aarea.isRemoved {
            WarehouseTask.aarea = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "AAREA")
        }
        if !WarehouseTask.cat.isRemoved {
            WarehouseTask.cat = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CAT")
        }
        if !WarehouseTask.catText.isRemoved {
            WarehouseTask.catText = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CAT_TEXT")
        }
        if !WarehouseTask.charg.isRemoved {
            WarehouseTask.charg = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CHARG")
        }
        if !WarehouseTask.confirmedAt.isRemoved {
            WarehouseTask.confirmedAt = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CONFIRMED_AT")
        }
        if !WarehouseTask.confirmedBy.isRemoved {
            WarehouseTask.confirmedBy = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CONFIRMED_BY")
        }
        if !WarehouseTask.confirmedByFullName.isRemoved {
            WarehouseTask.confirmedByFullName = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CONFIRMED_BY_FULL_NAME")
        }
        if !WarehouseTask.coo.isRemoved {
            WarehouseTask.coo = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "COO")
        }
        if !WarehouseTask.createdAt.isRemoved {
            WarehouseTask.createdAt = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CREATED_AT")
        }
        if !WarehouseTask.createdBy.isRemoved {
            WarehouseTask.createdBy = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CREATED_BY")
        }
        if !WarehouseTask.createdByFullName.isRemoved {
            WarehouseTask.createdByFullName = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CREATED_BY_FULL_NAME")
        }
        if !WarehouseTask.dstgrp.isRemoved {
            WarehouseTask.dstgrp = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "DSTGRP")
        }
        if !WarehouseTask.entitled.isRemoved {
            WarehouseTask.entitled = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ENTITLED")
        }
        if !WarehouseTask.entitledText.isRemoved {
            WarehouseTask.entitledText = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ENTITLED_TEXT")
        }
        if !WarehouseTask.exccode.isRemoved {
            WarehouseTask.exccode = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "EXCCODE")
        }
        if !WarehouseTask.exccodeText.isRemoved {
            WarehouseTask.exccodeText = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "EXCCODE_TEXT")
        }
        if !WarehouseTask.homve.isRemoved {
            WarehouseTask.homve = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "HOMVE")
        }
        if !WarehouseTask.maktx.isRemoved {
            WarehouseTask.maktx = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "MAKTX")
        }
        if !WarehouseTask.matnr.isRemoved {
            WarehouseTask.matnr = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "MATNR")
        }
        if !WarehouseTask.nista.isRemoved {
            WarehouseTask.nista = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "NISTA")
        }
        if !WarehouseTask.nistm.isRemoved {
            WarehouseTask.nistm = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "NISTM")
        }
        if !WarehouseTask.nlber.isRemoved {
            WarehouseTask.nlber = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "NLBER")
        }
        if !WarehouseTask.nlpla.isRemoved {
            WarehouseTask.nlpla = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "NLPLA")
        }
        if !WarehouseTask.nltyp.isRemoved {
            WarehouseTask.nltyp = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "NLTYP")
        }
        if !WarehouseTask.nlenr.isRemoved {
            WarehouseTask.nlenr = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "NLENR")
        }
        if !WarehouseTask.owner.isRemoved {
            WarehouseTask.owner = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "OWNER")
        }
        if !WarehouseTask.ownerText.isRemoved {
            WarehouseTask.ownerText = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "OWNER_TEXT")
        }
        if !WarehouseTask.pickCompDt.isRemoved {
            WarehouseTask.pickCompDt = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "PICK_COMP_DT")
        }
        if !WarehouseTask.procty.isRemoved {
            WarehouseTask.procty = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "PROCTY")
        }
        if !WarehouseTask.proctyText.isRemoved {
            WarehouseTask.proctyText = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "PROCTY_TEXT")
        }
        if !WarehouseTask.prodOrder.isRemoved {
            WarehouseTask.prodOrder = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "PROD_ORDER")
        }
        if !WarehouseTask.psa.isRemoved {
            WarehouseTask.psa = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "PSA")
        }
        if !WarehouseTask.rdoccat.isRemoved {
            WarehouseTask.rdoccat = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "RDOCCAT")
        }
        if !WarehouseTask.docno.isRemoved {
            WarehouseTask.docno = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "DOCNO")
        }
        if !WarehouseTask.itemno.isRemoved {
            WarehouseTask.itemno = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ITEMNO")
        }
        if !WarehouseTask.route.isRemoved {
            WarehouseTask.route = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ROUTE")
        }
        if !WarehouseTask.solpo.isRemoved {
            WarehouseTask.solpo = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "SOLPO")
        }
        if !WarehouseTask.startedAt.isRemoved {
            WarehouseTask.startedAt = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "STARTED_AT")
        }
        if !WarehouseTask.stockDoccat.isRemoved {
            WarehouseTask.stockDoccat = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "STOCK_DOCCAT")
        }
        if !WarehouseTask.stockDocno.isRemoved {
            WarehouseTask.stockDocno = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "STOCK_DOCNO")
        }
        if !WarehouseTask.stockItmno.isRemoved {
            WarehouseTask.stockItmno = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "STOCK_ITMNO")
        }
        if !WarehouseTask.tanum.isRemoved {
            WarehouseTask.tanum = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TANUM")
        }
        if !WarehouseTask.tapos.isRemoved {
            WarehouseTask.tapos = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TAPOS")
        }
        if !WarehouseTask.tostat.isRemoved {
            WarehouseTask.tostat = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TOSTAT")
        }
        if !WarehouseTask.tostatText.isRemoved {
            WarehouseTask.tostatText = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TOSTAT_TEXT")
        }
        if !WarehouseTask.tostatCriticality.isRemoved {
            WarehouseTask.tostatCriticality = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TOSTAT_CRITICALITY")
        }
        if !WarehouseTask.unitV.isRemoved {
            WarehouseTask.unitV = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "UNIT_V")
        }
        if !WarehouseTask.unitW.isRemoved {
            WarehouseTask.unitW = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "UNIT_W")
        }
        if !WarehouseTask.vfdat.isRemoved {
            WarehouseTask.vfdat = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VFDAT")
        }
        if !WarehouseTask.vlenr.isRemoved {
            WarehouseTask.vlenr = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VLENR")
        }
        if !WarehouseTask.vlpla.isRemoved {
            WarehouseTask.vlpla = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VLPLA")
        }
        if !WarehouseTask.vltyp.isRemoved {
            WarehouseTask.vltyp = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VLTYP")
        }
        if !WarehouseTask.volum.isRemoved {
            WarehouseTask.volum = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VOLUM")
        }
        if !WarehouseTask.capa.isRemoved {
            WarehouseTask.capa = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "CAPA")
        }
        if !WarehouseTask.vsola.isRemoved {
            WarehouseTask.vsola = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VSOLA")
        }
        if !WarehouseTask.vsolm.isRemoved {
            WarehouseTask.vsolm = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "VSOLM")
        }
        if !WarehouseTask.weight.isRemoved {
            WarehouseTask.weight = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "WEIGHT")
        }
        if !WarehouseTask.zeiei.isRemoved {
            WarehouseTask.zeiei = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ZEIEI")
        }
        if !WarehouseTask.qidplate.isRemoved {
            WarehouseTask.qidplate = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "QIDPLATE")
        }
        if !WarehouseTask.inspdocno.isRemoved {
            WarehouseTask.inspdocno = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "INSPDOCNO")
        }
        if !WarehouseTask.origNlpla.isRemoved {
            WarehouseTask.origNlpla = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ORIG_NLPLA")
        }
        if !WarehouseTask.meins.isRemoved {
            WarehouseTask.meins = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "MEINS")
        }
        if !WarehouseTask.altme.isRemoved {
            WarehouseTask.altme = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "ALTME")
        }
        if !WarehouseTask.isOpenWt.isRemoved {
            WarehouseTask.isOpenWt = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "IS_OPEN_WT")
        }
        if !WarehouseTask.isBatchRequired.isRemoved {
            WarehouseTask.isBatchRequired = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "IS_BATCH_REQUIRED")
        }
        if !WarehouseTask.isSourceHuAllowed.isRemoved {
            WarehouseTask.isSourceHuAllowed = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "IS_SOURCE_HU_ALLOWED")
        }
        if !WarehouseTask.isSourceHuObligatory.isRemoved {
            WarehouseTask.isSourceHuObligatory = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "IS_SOURCE_HU_OBLIGATORY")
        }
        if !WarehouseTask.lgnum.isRemoved {
            WarehouseTask.lgnum = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "LGNUM")
        }
        if !WarehouseTask.exccodeMulti.isRemoved {
            WarehouseTask.exccodeMulti = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "EXCCODE_MULTI")
        }
        if !WarehouseTask.who.isRemoved {
            WarehouseTask.who = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "WHO")
        }
        if !WarehouseTask.matid.isRemoved {
            WarehouseTask.matid = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "MATID")
        }
        if !WarehouseTask.rdoccatText.isRemoved {
            WarehouseTask.rdoccatText = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "RDOCCAT_TEXT")
        }
        if !WarehouseTask.trart.isRemoved {
            WarehouseTask.trart = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TRART")
        }
        if !WarehouseTask.trartText.isRemoved {
            WarehouseTask.trartText = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "TRART_TEXT")
        }
        if !WarehouseTask.kanban.isRemoved {
            WarehouseTask.kanban = Ec1Metadata.EntityTypes.warehouseTask.property(withName: "KANBAN")
        }
        if !WarehouseTaskType.handlingUnitID.isRemoved {
            WarehouseTaskType.handlingUnitID = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "HandlingUnitID")
        }
        if !WarehouseTaskType.warehouse.isRemoved {
            WarehouseTaskType.warehouse = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "Warehouse")
        }
        if !WarehouseTaskType.warehouseTask.isRemoved {
            WarehouseTaskType.warehouseTask = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseTask")
        }
        if !WarehouseTaskType.warehouseTaskItem.isRemoved {
            WarehouseTaskType.warehouseTaskItem = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseTaskItem")
        }
        if !WarehouseTaskType.warehouseOrder.isRemoved {
            WarehouseTaskType.warehouseOrder = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseOrder")
        }
        if !WarehouseTaskType.creationDateTime.isRemoved {
            WarehouseTaskType.creationDateTime = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "CreationDateTime")
        }
        if !WarehouseTaskType.lastChangeDateTime.isRemoved {
            WarehouseTaskType.lastChangeDateTime = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "LastChangeDateTime")
        }
        if !WarehouseTaskType.confirmationUTCDateTime.isRemoved {
            WarehouseTaskType.confirmationUTCDateTime = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ConfirmationUTCDateTime")
        }
        if !WarehouseTaskType.whseTaskPlannedClosingDateTime.isRemoved {
            WarehouseTaskType.whseTaskPlannedClosingDateTime = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WhseTaskPlannedClosingDateTime")
        }
        if !WarehouseTaskType.whseTaskGoodsReceiptDateTime.isRemoved {
            WarehouseTaskType.whseTaskGoodsReceiptDateTime = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WhseTaskGoodsReceiptDateTime")
        }
        if !WarehouseTaskType.warehouseTaskStatus.isRemoved {
            WarehouseTaskType.warehouseTaskStatus = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseTaskStatus")
        }
        if !WarehouseTaskType.warehouseTaskStatusName.isRemoved {
            WarehouseTaskType.warehouseTaskStatusName = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseTaskStatusName")
        }
        if !WarehouseTaskType.warehouseProcessType.isRemoved {
            WarehouseTaskType.warehouseProcessType = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseProcessType")
        }
        if !WarehouseTaskType.warehouseProcessTypeName.isRemoved {
            WarehouseTaskType.warehouseProcessTypeName = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseProcessTypeName")
        }
        if !WarehouseTaskType.isHandlingUnitWarehouseTask.isRemoved {
            WarehouseTaskType.isHandlingUnitWarehouseTask = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "IsHandlingUnitWarehouseTask")
        }
        if !WarehouseTaskType.productName.isRemoved {
            WarehouseTaskType.productName = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ProductName")
        }
        if !WarehouseTaskType.productDescription.isRemoved {
            WarehouseTaskType.productDescription = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ProductDescription")
        }
        if !WarehouseTaskType.batch.isRemoved {
            WarehouseTaskType.batch = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "Batch")
        }
        if !WarehouseTaskType.batchChangeIsNotAllowed.isRemoved {
            WarehouseTaskType.batchChangeIsNotAllowed = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "BatchChangeIsNotAllowed")
        }
        if !WarehouseTaskType.stockType.isRemoved {
            WarehouseTaskType.stockType = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "StockType")
        }
        if !WarehouseTaskType.stockTypeName.isRemoved {
            WarehouseTaskType.stockTypeName = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "StockTypeName")
        }
        if !WarehouseTaskType.stockOwner.isRemoved {
            WarehouseTaskType.stockOwner = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "StockOwner")
        }
        if !WarehouseTaskType.entitledToDisposeParty.isRemoved {
            WarehouseTaskType.entitledToDisposeParty = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "EntitledToDisposeParty")
        }
        if !WarehouseTaskType.stockDocumentCategory.isRemoved {
            WarehouseTaskType.stockDocumentCategory = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "StockDocumentCategory")
        }
        if !WarehouseTaskType.stockDocumentNumber.isRemoved {
            WarehouseTaskType.stockDocumentNumber = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "StockDocumentNumber")
        }
        if !WarehouseTaskType.stockItemNumber.isRemoved {
            WarehouseTaskType.stockItemNumber = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "StockItemNumber")
        }
        if !WarehouseTaskType.executingResource.isRemoved {
            WarehouseTaskType.executingResource = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ExecutingResource")
        }
        if !WarehouseTaskType.productionOrder.isRemoved {
            WarehouseTaskType.productionOrder = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ProductionOrder")
        }
        if !WarehouseTaskType.productionSupplyArea.isRemoved {
            WarehouseTaskType.productionSupplyArea = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ProductionSupplyArea")
        }
        if !WarehouseTaskType.delivery.isRemoved {
            WarehouseTaskType.delivery = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "Delivery")
        }
        if !WarehouseTaskType.deliveryItem.isRemoved {
            WarehouseTaskType.deliveryItem = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DeliveryItem")
        }
        if !WarehouseTaskType.purchasingDocument.isRemoved {
            WarehouseTaskType.purchasingDocument = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "PurchasingDocument")
        }
        if !WarehouseTaskType.purchasingDocumentItem.isRemoved {
            WarehouseTaskType.purchasingDocumentItem = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "PurchasingDocumentItem")
        }
        if !WarehouseTaskType.salesDocument.isRemoved {
            WarehouseTaskType.salesDocument = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SalesDocument")
        }
        if !WarehouseTaskType.salesDocumentItem.isRemoved {
            WarehouseTaskType.salesDocumentItem = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SalesDocumentItem")
        }
        if !WarehouseTaskType.baseUnit.isRemoved {
            WarehouseTaskType.baseUnit = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "BaseUnit")
        }
        if !WarehouseTaskType.alternativeUnit.isRemoved {
            WarehouseTaskType.alternativeUnit = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "AlternativeUnit")
        }
        if !WarehouseTaskType.targetQuantityInBaseUnit.isRemoved {
            WarehouseTaskType.targetQuantityInBaseUnit = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "TargetQuantityInBaseUnit")
        }
        if !WarehouseTaskType.targetQuantityInAltvUnit.isRemoved {
            WarehouseTaskType.targetQuantityInAltvUnit = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "TargetQuantityInAltvUnit")
        }
        if !WarehouseTaskType.actualQuantityInBaseUnit.isRemoved {
            WarehouseTaskType.actualQuantityInBaseUnit = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ActualQuantityInBaseUnit")
        }
        if !WarehouseTaskType.actualQuantityInAltvUnit.isRemoved {
            WarehouseTaskType.actualQuantityInAltvUnit = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ActualQuantityInAltvUnit")
        }
        if !WarehouseTaskType.differenceQuantityInBaseUnit.isRemoved {
            WarehouseTaskType.differenceQuantityInBaseUnit = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DifferenceQuantityInBaseUnit")
        }
        if !WarehouseTaskType.differenceQuantityInAltvUnit.isRemoved {
            WarehouseTaskType.differenceQuantityInAltvUnit = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DifferenceQuantityInAltvUnit")
        }
        if !WarehouseTaskType.whseTaskNetWeightUnitOfMeasure.isRemoved {
            WarehouseTaskType.whseTaskNetWeightUnitOfMeasure = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WhseTaskNetWeightUnitOfMeasure")
        }
        if !WarehouseTaskType.netWeight.isRemoved {
            WarehouseTaskType.netWeight = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "NetWeight")
        }
        if !WarehouseTaskType.whseTaskNetVolumeUnitOfMeasure.isRemoved {
            WarehouseTaskType.whseTaskNetVolumeUnitOfMeasure = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WhseTaskNetVolumeUnitOfMeasure")
        }
        if !WarehouseTaskType.whseTaskNetVolume.isRemoved {
            WarehouseTaskType.whseTaskNetVolume = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WhseTaskNetVolume")
        }
        if !WarehouseTaskType.sourceStorageType.isRemoved {
            WarehouseTaskType.sourceStorageType = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SourceStorageType")
        }
        if !WarehouseTaskType.sourceStorageTypeName.isRemoved {
            WarehouseTaskType.sourceStorageTypeName = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SourceStorageTypeName")
        }
        if !WarehouseTaskType.sourceStorageSection.isRemoved {
            WarehouseTaskType.sourceStorageSection = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SourceStorageSection")
        }
        if !WarehouseTaskType.sourceStorageBin.isRemoved {
            WarehouseTaskType.sourceStorageBin = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SourceStorageBin")
        }
        if !WarehouseTaskType.destinationStorageType.isRemoved {
            WarehouseTaskType.destinationStorageType = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DestinationStorageType")
        }
        if !WarehouseTaskType.destinationStorageTypeName.isRemoved {
            WarehouseTaskType.destinationStorageTypeName = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DestinationStorageTypeName")
        }
        if !WarehouseTaskType.destinationStorageSection.isRemoved {
            WarehouseTaskType.destinationStorageSection = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DestinationStorageSection")
        }
        if !WarehouseTaskType.destinationStorageBin.isRemoved {
            WarehouseTaskType.destinationStorageBin = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DestinationStorageBin")
        }
        if !WarehouseTaskType.destinationResource.isRemoved {
            WarehouseTaskType.destinationResource = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DestinationResource")
        }
        if !WarehouseTaskType.activityArea.isRemoved {
            WarehouseTaskType.activityArea = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ActivityArea")
        }
        if !WarehouseTaskType.activityAreaName.isRemoved {
            WarehouseTaskType.activityAreaName = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "ActivityAreaName")
        }
        if !WarehouseTaskType.sourceHandlingUnit.isRemoved {
            WarehouseTaskType.sourceHandlingUnit = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "SourceHandlingUnit")
        }
        if !WarehouseTaskType.destinationHandlingUnit.isRemoved {
            WarehouseTaskType.destinationHandlingUnit = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "DestinationHandlingUnit")
        }
        if !WarehouseTaskType.warehouseTaskExceptionCode.isRemoved {
            WarehouseTaskType.warehouseTaskExceptionCode = Ec1Metadata.EntityTypes.warehouseTaskType.property(withName: "WarehouseTaskExceptionCode")
        }
        if !WarehouseType.warehouse.isRemoved {
            WarehouseType.warehouse = Ec1Metadata.EntityTypes.warehouseType.property(withName: "Warehouse")
        }
    }
}
