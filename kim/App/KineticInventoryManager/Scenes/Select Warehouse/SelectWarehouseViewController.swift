//
//  SelectWarehouseViewController.swift
//  KineticInventoryManager
//
//  Created by Raka Chowdhury on 01/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

protocol SelectWarehouseViewControllerDelegate: class {
    func didReturnSelectedWarehouse(with warehouseId: String, warehouseName: String)
}

class SelectWarehouseViewController: KIMBaseViewController, BaseTableViewable {
    @IBOutlet var tableView: FioriBaseTableView!
    @IBOutlet var searchBar: UISearchBar!
    
    var dataProvider: TableDataProviderable!
    var warehouseService: WarehouseService!
    
    var warehouseData: [APlantType] = [APlantType]()
    var filteredSearchFeedData: [APlantType] = [APlantType]()
    var searchActive : Bool = false
    
    weak var delegate: SelectWarehouseViewControllerDelegate?
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewAppearance()
    }
    
    // MARK: - Setup View Appearance
    func setupViewAppearance() {
        setupTableView()
    }
    
    // MARK: - Setup Table
    private func setupTableView() {
        setupDefaultTableData()
        setupTableView(with: dataProvider, delegate: dataProvider)
        refresh()
    }
    
    // MARK: - Setup Search Bar
    private func setupSearchbar() {
        searchBar.delegate = self
    }
    
    // MARK: - Refresh
    func refresh() {
        if warehouseService == nil {
            warehouseService = WarehouseService(with: onlineODataProvider)
        }
        warehouseService.fetchSearchFeed() { (error, data) in
            self.warehouseData = data
            self.update(with: data)
            if let _ = error {
                DispatchQueue.main.async {
                    self.presentOkAlert(message: R.string.generic.errorGenericMessage())
                }
            }
        }
    }
    
    func setupDefaultTableData() {
        setupNavigationBar(title: R.string.goods.selectLocation(), hidesBackButton: false)
        let searchFeedViewModel = SelectWarehouseViewModel(with: warehouseData)
        dataProvider = SelectWarehouseDataProvider(with: tableView, viewModel: searchFeedViewModel, delegate: self)
    }
}

// MARK: - DataProviderDelegate
extension SelectWarehouseViewController: DataProviderDelegate {
    func reloadData() {}
    
    func didSelect(_ field: Field) {
        delegate?.didReturnSelectedWarehouse(with: field.id, warehouseName: field.title)
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - DataProviderDelegate
extension SelectWarehouseViewController {
    @IBAction func didTouchUpInsideDoneButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - UISearchBarDelegate
extension SelectWarehouseViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteringSearchItems(with: searchText)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func filteringSearchItems(with searchText: String) {
        filteredSearchFeedData = warehouseData.filter({ (plant) -> Bool in
            let tmp: String = plant.plantName!  //TODO: Need to make it more robust
            return tmp.contains(searchText)
        })
        if(filteredSearchFeedData.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        filterList()
    }
    
    func filterList() {
        var searchData: [APlantType] = [APlantType]()
        if(searchActive) {
            searchData = filteredSearchFeedData
        }
        else {
            searchData = warehouseData
        }
        let selectWarehouseViewModel = SelectWarehouseViewModel(with: searchData)
        dataProvider.update(with: selectWarehouseViewModel)
    }
    
    func update(with data: [APlantType]) {
        warehouseData = data
        let selectWarehouseViewModel = SelectWarehouseViewModel(with: warehouseData)
        if dataProvider == nil {
            dataProvider = SelectWarehouseDataProvider(with: tableView, viewModel: selectWarehouseViewModel, delegate: self)
        } else {
            dataProvider.update(with: selectWarehouseViewModel)
        }
    }
}
