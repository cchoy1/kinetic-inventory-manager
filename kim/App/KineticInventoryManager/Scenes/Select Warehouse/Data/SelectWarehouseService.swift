//
//  SearchFeedService.swift
//  KineticInventoryManager
//
//  Created by Raka Chowdhury on 03/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation
import SAPOData
import SAPFioriFlows
import SAPFoundation

class WarehouseService {
    var ec1: Ec1<OnlineODataProvider>
    
    init(with ec1: Ec1<OnlineODataProvider>) {
        self.ec1 = ec1
    }
    
    func fetchSearchFeed(with completionHandler: @escaping (Error?, [APlantType]) -> Void) {
        var searchFeedData: [APlantType] = [APlantType]()
        //Filter warehouses to demo warehouse locations (MVP - To only show two specific warehouse)
        let aWarehouse = ["1710","1712"]
        let query = DataQuery().filter(APlantType.plant.equal(aWarehouse[0]).or(APlantType.plant.equal(aWarehouse[1])))
        ec1.fetchAPlant(matching: query) { (entities, error) in
            if let error = error {
                completionHandler(error, [])
                return
            } else {
                searchFeedData = entities ?? []
                completionHandler(nil, searchFeedData)
            }
        }
    }
}
