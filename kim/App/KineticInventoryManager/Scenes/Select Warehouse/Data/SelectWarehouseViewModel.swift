//
//  SearchFeedViewModel.swift
//  KineticInventoryManager
//
//  Created by Raka Chowdhury on 01/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

final class SelectWarehouseViewModel: BaseViewModel {
    init(with searchFeed: [APlantType]) {
        super.init(with: [SectionItem]())
        let itemsSortedAphabetically = SelectWarehouseViewModel.sortIntoAphabeticalSectionsDictionary(searchFeed)
        let sectionTitles = [String](itemsSortedAphabetically.keys).sorted()

        for sectionTitle in sectionTitles {
            if let sectionItems = itemsSortedAphabetically[sectionTitle] {
                sections.append(SelectWarehouseViewModel.searchSection(with: sectionItems, title: sectionTitle.uppercased()))
            }
        }
    }
}

// MARK: Sections
extension SelectWarehouseViewModel {
    static func searchSection(with searchList: [APlantType], title: String) -> SectionItem {
        var fields = [Field]()
        for searchItem in searchList {
            fields.append(Field(with: searchItem.plant ?? "", title: searchItem.plantName ?? ""))
        }
        return SectionItem(title: title, fields: fields)
    }
    
    // MARK: Section Splitting
    static func sortIntoAphabeticalSectionsDictionary(_ items: [APlantType]) -> [String : [APlantType]] {
        var aplhabeticallySortedDictionary = [String : [APlantType]]()
        for item in items {
            let key = "\(item.plantName![item.plantName!.startIndex])"
            let lower = key.lowercased()
            if var contactValue = aplhabeticallySortedDictionary[lower] {
                contactValue.append(item)
                aplhabeticallySortedDictionary[lower] = contactValue
            } else {
                aplhabeticallySortedDictionary[lower] = [item]
            }
        }
        return aplhabeticallySortedDictionary
    }
}
