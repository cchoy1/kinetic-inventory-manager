//
//  StockFeedViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

class StockFeedViewController: KIMBaseViewController, BaseTableViewable {
    @IBOutlet var tableView: FioriBaseTableView!
    @IBOutlet var selectLocationButton: UIButton!
    @IBOutlet var buttonHeaderView: ButtonHeaderView!
    
    var dataProvider: TableDataProviderable!
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationItems()
        setupTableView()
        refresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateLocationButtonTitle()
    }
    
    // MARK: - Setup View
    private func updateLocationButtonTitle() {
        selectLocationButton.setTitle(DefaultWarehouseHelper.fetchDefaultWarehouseName(), for: .normal)
    }
    
    // MARK: - Setup Table
    private func setupTableView() {
        setupDefaultTableData()
        setupTableView(with: dataProvider, delegate: dataProvider)
    }
    
    private func setupNavigationItems() {
        setupNavigationBar(title: R.string.goods.stock(), largeTitle: true)
        setupSelectLocationButton()
    }
    
    private func setupSelectLocationButton() {
        updateLocationButtonTitle()
        selectLocationButton.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
    }
    
    // MARK: - Setup Default Table Data
    func setupDefaultTableData() {
        let stockFeedViewModel = StockFeedViewModel()
        dataProvider = StockFeedDataProvider(with: tableView, viewModel: stockFeedViewModel, delegate: self)
        
        let buttonHeaderField = StockFeedViewModel.headerViewField()
        buttonHeaderView.update(with: self)
        buttonHeaderView.update(with: buttonHeaderField)
    }
    
    // MARK: - Reload Data
    func refresh() {
        updateLocationButtonTitle()
        dataProvider.reloadTableView()
    }
}

// MARK: - IBActions
extension StockFeedViewController: DataProviderDelegate {
    @IBAction func didTouchUpInsideSearchButton(_ sender: Any) {
        selectWarehouse()
    }
}

// MARK: - DataProviderDelegate
extension StockFeedViewController {
    func reloadData() {
        refresh()
    }
    
    func didSelect(_ field: Field) {
        if field.id == StockFeedItem.stockIn.rawValue {
            showStockIn()
        } else if field.id == StockFeedItem.stockOut.rawValue {
            showStockOut()
        }
    }
}

// MARK: - ButtonHeaderViewDelegate
extension StockFeedViewController: ButtonHeaderViewDelegate {
    func didTouchUpInsideHeaderViewButton() {
        showTransferStock()
    }
}

// MARK: - SelectWarehouseViewControllerDelegate
extension StockFeedViewController: SelectWarehouseViewControllerDelegate {
    func didReturnSelectedWarehouse(with warehouseId: String, warehouseName: String) {
        self.showFioriLoadingIndicator()
        DefaultWarehouseHelper.saveDefaultWarehouse(with: onlineODataProvider, id: warehouseId, name: warehouseName) { (error) in
            if let _ = error {
                self.presentOkAlert(message: R.string.generic.errorGenericMessage())
                return
            } else {
                DispatchQueue.main.async {
                    self.hideFioriLoadingIndicator()
                    self.selectLocationButton.setTitle(warehouseName, for: .normal)
                }
            }
        }
    }
}

// MARK: - Navigation
extension StockFeedViewController {    
    func selectWarehouse() {
          guard let selectWarehouseViewController = R.storyboard.selectWarehouse.selectWarehouseViewController() else {
              return
          }
          selectWarehouseViewController.delegate = self
          selectWarehouseViewController.onlineODataProvider = onlineODataProvider
          let navigationController = UINavigationController(rootViewController: selectWarehouseViewController)
          self.navigationController?.present(navigationController, animated: true, completion: nil)
      }
    
    func showTransferStock() {
        guard let requestStockTransferViewController = R.storyboard.requestStockTransfer.requestStockTransferViewController() else {
            return
        }
        requestStockTransferViewController.onlineODataProvider = onlineODataProvider
        let navigationController = UINavigationController(rootViewController: requestStockTransferViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func showStockOut() {}
    
    func showStockIn() {}
}
