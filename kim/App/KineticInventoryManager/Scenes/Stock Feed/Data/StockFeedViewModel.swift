//
//  StockFeedViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

final class StockFeedViewModel: BaseViewModel {
    init() {
        super.init(with: [SectionItem]())
        sections.append(StockFeedViewModel.stockSection())
    }
}

// MARK: Sections
extension StockFeedViewModel {
    static func imageField(with feedItem: StockFeedItem) -> Field {
        let imageInfo = ImageInfo(imageURL: feedItem.imageURL, imageType: .asset)
        return ImageField(with: feedItem.rawValue, title: feedItem.title, imageInfo: imageInfo)
    }
    
    static func stockSection() -> SectionItem {
        let fields = [
            StockFeedViewModel.imageField(with: .stockIn),
            StockFeedViewModel.imageField(with: .stockOut)
        ]
        return SectionItem(title: "", fields: fields)
    }
    
    static func headerViewField() -> ButtonField {
        return ButtonField(with: R.string.goods.transferStock())
    }
}
