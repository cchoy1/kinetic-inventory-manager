//
//  StockFeedDataProvider.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

final class StockFeedDataProvider: FioriBaseTableDataProvider {}

// MARK: - Override UITableViewDataSource
extension StockFeedDataProvider {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let field = viewModel.item(for: indexPath) as? Field,
            let cell = dequeCell(with: field, for: tableView) else {
                return UITableViewCell()
        }
        return cell
    }
    
    func dequeCell(with field: Field, for tableView: UITableView) -> UITableViewCell? {
        if let imageField = field as? ImageField, let cell = tableView.dequeueReusableCell(withIdentifier: CardImageTableViewCell.className) as? CardImageTableViewCell {
            cell.update(with: imageField)
            return cell
        }
        return nil
    }
}
