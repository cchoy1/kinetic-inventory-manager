//
//  StockFeedSection.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

enum StockFeedItem: String {
    case stockIn
    case stockOut
    
    var title: String {
        switch self {
        case .stockIn:
            return R.string.goods.stockIn()
        case .stockOut:
            return R.string.goods.stockOut()
        }
    }
    
    var imageURL: String {
        switch self {
        case .stockIn:
            return "StockInGraph"
        case .stockOut:
            return "StockOutGraph"
        }
    }
}
