//
//  GoodsOutViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 14/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

final class GoodsOutViewModel: BaseViewModel {
    private var goodsOut: [OUTBDLVHead]
    
    init(with goodsIn: [OUTBDLVHead]) {
        self.goodsOut = []
        super.init(with: [SectionItem]())
        self.update(with: goodsIn)
    }
    
    func update(with goods: [OUTBDLVHead]) {
        self.goodsOut = goods
        
        let goodsSortedByDate = goodsOut.sorted(by: {
            $0.lastChangedDateTime!.utc().compare($1.lastChangedDateTime!.utc()) == .orderedDescending
        })
        let pendingGoods = goodsSortedByDate.filter { $0.packingStatusName == "Not started" } // case sensitive
        let packedGoods = goodsSortedByDate.filter { $0.packingStatusName == "Completed" && $0.completionStatusName == "Not Started"} // case sensitive
        let inTransitGoods = goodsSortedByDate.filter { $0.packingStatusName == "Completed" && $0.completionStatusName == "Completed" }
   
        if pendingGoods.count > 0 {
            sections.append(GoodsOutViewModel.goodsSection(with: pendingGoods, sectionInfo: .pending))
        }
        if packedGoods.count > 0 {
            sections.append(GoodsOutViewModel.goodsSection(with: packedGoods, sectionInfo: .packed))
        }
        if inTransitGoods.count > 0 {
            sections.append(GoodsOutViewModel.goodsSection(with: inTransitGoods, sectionInfo: .inTransit))
        }
        
        print("First Outbound Good ID")
        print(pendingGoods.first?.outboundDelivery ?? "")
    }
    
    func order(with id: String) -> OUTBDLVHead? {
        return goodsOut.first(where: { $0.outboundDelivery ?? "" == id })
    }
}

// MARK: Sections
extension GoodsOutViewModel {
    static func goodsSection(with items: [OUTBDLVHead], sectionInfo: OrderStatus) -> SectionItem {
        var fields = [Field]()
        for item in items {
            let field = OrderDetailField(with: item)
            fields.append(field)
        }
        return SectionItem(title: sectionInfo.title, fields: fields)
    }
    
    static func toolbarButtonField(forPackedGoods: Bool) -> ButtonField {
        let imageInfo = ImageInfo(imageURL: "barcode.viewfinder", imageType: .sfSymbol)
        if forPackedGoods {
            return ButtonField(with: R.string.goods.scanToLoadGoods(), imageInfo: imageInfo)
        } else {
            return ButtonField(with: R.string.goods.scanToPackGoods(), imageInfo: imageInfo)
        }
    }
}
