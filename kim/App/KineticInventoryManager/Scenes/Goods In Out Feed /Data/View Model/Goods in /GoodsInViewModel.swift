//
//  GoodsInViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 23/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

final class GoodsInViewModel: BaseViewModel {
    private var goodsIn: [DLVHead]
    
    init(with goodsIn: [DLVHead]) {
        self.goodsIn = []
        super.init(with: [SectionItem]())
        self.update(with: goodsIn)
    }
    
    func update(with goods: [DLVHead]) {
        self.goodsIn = goods
        
        let goodsSortedByDate = goodsIn.sorted(by: {
            $0.plannedDeliveryEndDate!.utc().compare($1.plannedDeliveryEndDate!.utc()) == .orderedDescending
        })
        let pendingGoods = goodsSortedByDate.filter { $0.completionStatusName == "Not Started" }
        let deliveredGoods = goodsSortedByDate.filter { $0.completionStatusName == "Completed" }
        
        if pendingGoods.count > 0 {
            sections.append(GoodsInViewModel.goodsSection(with: pendingGoods, sectionInfo: .inTransit))
        }
        if deliveredGoods.count > 0 {
            sections.append(GoodsInViewModel.goodsSection(with: deliveredGoods, sectionInfo: .delivered))
        }
    }
    
    func order(with id: String) -> DLVHead? {
        return goodsIn.first(where: { $0.inboundDelivery ?? "" == id })
    }
}

// MARK: Sections
extension GoodsInViewModel {
    static func goodsSection(with items: [DLVHead], sectionInfo: OrderStatus) -> SectionItem {
        var fields = [Field]()
        for item in items {
            let field = OrderDetailField(with: item)
            fields.append(field)
        }
        return SectionItem(title: sectionInfo.title, fields: fields)
    }
    
    static func toolbarButtonField() -> ButtonField {
        let imageInfo = ImageInfo(imageURL: "barcode.viewfinder", imageType: .sfSymbol)
        return ButtonField(with: R.string.goods.scanToUnloadGoods(), imageInfo: imageInfo)
    }
}
