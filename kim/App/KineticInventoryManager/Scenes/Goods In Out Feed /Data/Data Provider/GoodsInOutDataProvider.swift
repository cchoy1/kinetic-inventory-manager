//
//  GoodsInOutDataProvider.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 23/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

final class GoodsInOutDataProvider: FioriBaseTableDataProvider {}

// MARK: - Override UITableViewDataSource
extension GoodsInOutDataProvider {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let field = viewModel.item(for: indexPath) as? Field,
            let cell = dequeCell(with: field, for: tableView) else {
            return UITableViewCell()
        }
        return cell
    }
    
      func dequeCell(with field: Field, for tableView: UITableView) -> UITableViewCell? {
        if let orderDetailField = field as? OrderDetailField,
            let cell = tableView.dequeueReusableCell(withIdentifier: ObjectCellTableViewCell.className) as? ObjectCellTableViewCell {
             cell.update(with: orderDetailField)
             return cell
         }
         return nil
     }
}
