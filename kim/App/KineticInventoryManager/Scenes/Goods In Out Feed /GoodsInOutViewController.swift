//
//  GoodsInOutViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 23/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class GoodsInOutViewController: KIMBaseViewController, BaseTableViewable {
    @IBOutlet var tableView: FioriBaseTableView!
    @IBOutlet var toolBarButtonView: ToolbarButtonView!
    
    var isGoodsIn: Bool!
    
    var homeFeedData: HomeFeedData!
    var dataProvider: TableDataProviderable!
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewAppearance()
        refresh()
    }
    
    // MARK: - Setup View Appearance
    private func setupViewAppearance() {
        setupView(forGoodsIn: isGoodsIn)
        setupTableView()
    }
    
    private func setupView(forGoodsIn: Bool) {
        var toolbarButtonField: ButtonField!
        if forGoodsIn {
            toolbarButtonField = GoodsInViewModel.toolbarButtonField()
            setupNavigationBar(title: R.string.goods.arrivingTodayShort(), isHidden: false, hidesBackButton: true)
        } else {
            toolbarButtonField = GoodsOutViewModel.toolbarButtonField(forPackedGoods: false)
            setupNavigationBar(title: R.string.goods.leavingToday(), isHidden: false, hidesBackButton: true)
        }
        toolBarButtonView.update(with: self)
        toolBarButtonView.update(with: toolbarButtonField)
    }
    
    // MARK: - Setup Table
    private func setupTableView() {
        setupDefaultTableData()
        setupTableView(with: dataProvider, delegate: dataProvider)
    }
    
    // MARK: - Setup Table: BaseTableViewable
    private func setupDefaultTableData() {
        if homeFeedData == nil {
            homeFeedData = HomeFeedData(goodsIn: [], goodsOut: [])
        }
        
        let viewModel = fetchViewModel(with: homeFeedData, forGoodsIn: isGoodsIn)
        dataProvider = GoodsInOutDataProvider(with: tableView, viewModel: viewModel, delegate: self)
        
        setupView(forGoodsIn: isGoodsIn)
    }
    
    // MARK: - Reload Data
    func refresh() {
        if isGoodsIn {
            refreshGoodsIn()
        } else {
            refreshGoodsOut()
        }
    }
    
    func refreshTable(with data: HomeFeedData, forGoodsIn: Bool) {
        let viewModel = fetchViewModel(with: data, forGoodsIn: forGoodsIn)
        if dataProvider == nil {
            dataProvider = GoodsInOutDataProvider(with: tableView, viewModel: viewModel, delegate: self)
        } else {
            dataProvider.update(with: viewModel)
        }
        hideFioriLoadingIndicator()
    }
}

// MARK: - Refresh
extension GoodsInOutViewController {
    func update(data: HomeFeedData, forGoodsIn: Bool) {
        homeFeedData = data
        isGoodsIn = forGoodsIn
    }
    
    private func refreshGoodsIn() {
        showFioriLoadingIndicator()
        GoodsService.fetchInboundGoods(with: onlineODataProvider, warehouseId: DefaultWarehouseHelper.fetchDefaultWarehouseId()) { (error, inboundGoods) in
            if let _ = error {
                DispatchQueue.main.async {
                    self.presentOkAlert(message: R.string.generic.errorGenericMessage())
                }
            }
            self.homeFeedData.goodsIn = inboundGoods ?? []
            self.refreshTable(with: self.homeFeedData, forGoodsIn: self.isGoodsIn)
        }
    }
    
    private func refreshGoodsOut() {
        showFioriLoadingIndicator()
        GoodsService.fetchOutboundGoods(with: onlineODataProvider, warehouseId: DefaultWarehouseHelper.fetchDefaultWarehouseId()) { (error, outboundGoods) in
            if let _ = error {
                DispatchQueue.main.async {
                    self.presentOkAlert(message: R.string.generic.errorGenericMessage())
                }
            }
            self.homeFeedData.goodsOut = outboundGoods ?? []
            self.refreshTable(with: self.homeFeedData, forGoodsIn: self.isGoodsIn)
        }
    }
    
    private func fetchViewModel(with data: HomeFeedData, forGoodsIn: Bool) -> BaseViewModel {
        var viewModel: BaseViewModel = BaseViewModel(with: [])
        if forGoodsIn {
            viewModel = GoodsInViewModel(with: data.goodsIn)
        } else {
            viewModel = GoodsOutViewModel(with: data.goodsOut)
        }
        return viewModel
    }
}

// MARK: - Done Button Pressed
extension GoodsInOutViewController {
    @IBAction func didTouchUpInsideDoneButton(_ sender: Any) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
}

// MARK: - DataProviderDelegate
extension GoodsInOutViewController: DataProviderDelegate {
    func reloadData() {
        refresh()
    }
    
    func didSelect(_ field: Field) {
        if isGoodsIn {
            guard let viewModel = dataProvider.viewModel as? GoodsInViewModel, let order = viewModel.order(with: field.id) else {
                return
            }
            goToOrderDetail(with: order)
        } else {
            guard let viewModel = dataProvider.viewModel as? GoodsOutViewModel, let order = viewModel.order(with: field.id) else {
                return
            }
            goToOrderDetail(with: order)
        }
    }
}

extension GoodsInOutViewController: OrderDetailViewControllerDelegate {
    func didSubmitOrder() {
        refresh()
    }
}

// MARK: - ToolbarButtonViewDelegate
extension GoodsInOutViewController: ToolbarButtonViewDelegate {
    func didTouchUpInsideToolbarButtonView() {
        goToScan()
    }
}

// MARK: - Navigation
extension GoodsInOutViewController {
    func showRequestGoods() {
        guard let requestGoodsViewController = R.storyboard.requestStockTransfer.requestStockTransferViewController() else {
            return
        }
        requestGoodsViewController.onlineODataProvider = onlineODataProvider
        let navigationController = UINavigationController(rootViewController: requestGoodsViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func goToOrderDetail(with order: Any) {
        guard let orderDetailViewController = R.storyboard.orderDetail.orderDetailViewController() else {
            return
        }
        orderDetailViewController.onlineODataProvider = onlineODataProvider
        orderDetailViewController.set(order: order, delegate: self, homeFeedData: homeFeedData)
        navigationController?.pushViewController(orderDetailViewController, animated: true)
    }
    
    func goToScan() {
        if isGoodsIn {
            guard let scanGoodsInViewController = R.storyboard.scanGoods.scanGoodsInViewController() else {
                return
            }
            scanGoodsInViewController.onlineODataProvider = onlineODataProvider
            scanGoodsInViewController.update(with: homeFeedData, isGoodsIn: true)
            let navigationController = UINavigationController(rootViewController: scanGoodsInViewController)
            navigationController.modalPresentationStyle = .fullScreen
            self.navigationController?.present(navigationController, animated: true, completion: nil)
        } else {
            guard let scanGoodsOutViewController = R.storyboard.scanGoods.scanGoodsOutViewController() else {
                return
            }
            scanGoodsOutViewController.onlineODataProvider = onlineODataProvider
            scanGoodsOutViewController.update(with: homeFeedData, isGoodsIn: false)
            let navigationController = UINavigationController(rootViewController: scanGoodsOutViewController)
            navigationController.modalPresentationStyle = .fullScreen
            self.navigationController?.present(navigationController, animated: true, completion: nil)
        }
    }
}
