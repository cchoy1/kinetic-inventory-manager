//
//  ProductInfoViewController.swift
//  KineticInventoryManager
//
//  Created by Raka Chowdhury on 25/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class ProductInfoViewController: KIMBaseViewController, BaseTableViewable {
    @IBOutlet var tableView: FioriBaseTableView!
    
    var product: Any!
    var dataProvider: TableDataProviderable!
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewAppearance()
    }
    
    // MARK: - Setup View Appearance
    func setupViewAppearance() {
        setupTableView()
    }
    
    // MARK: - Setup Table
    private func setupTableView() {
        setupDefaultTableData()
        registerCells()
        setupTableView(with: dataProvider, delegate: dataProvider)
    }
    
    func refresh() {}
    
    // MARK: - Setup Table: BaseTableViewable
    func setupDefaultTableData() {
        update(with: product!)
        let viewModel = fetchViewModel(with: product!)
        dataProvider = ProductInfoDataProvider(with: tableView, viewModel: viewModel, delegate: self)
    }
    
    func update(with product: Any) {
        var title = ""
        if let product = product as? DLVItem {
            title = product.productName ?? ""
        } else if let product = product as? OUTBDLVItem {
            title = product.productName ?? ""
        } else if let product = product as? AProductType {
            title = product.product ?? ""
        }
        setupNavigationBar(title: title, hidesBackButton: false)
    }
    
    func registerCells() {
        let quantityCellTableViewCellNib = UINib(nibName: QuantityCell.className, bundle: .main)
        tableView.register(quantityCellTableViewCellNib, forCellReuseIdentifier: QuantityCell.className)
        
        let imageTableViewCellNib = UINib(nibName: ImageTableViewCell.className, bundle: .main)
        tableView.register(imageTableViewCellNib, forCellReuseIdentifier: ImageTableViewCell.className)
    }
    
    // MARK: - IBActions
    @IBAction func didTouchUpInsideCancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - Data
extension ProductInfoViewController {
    private func fetchViewModel(with product: Any) -> BaseViewModel {
        var viewModel: BaseViewModel = BaseViewModel(with: [])
        if let product = product as? DLVItem {
            viewModel = DeliveredProductViewModel(with: product)
        } else if let product = product as? OUTBDLVItem {
            viewModel = OutboundProductViewModel(with: product)
        } else if let product = product as? AProductType {
            viewModel = RequestedProductViewModel(with: product)
       }
        return viewModel
    }
}

// MARK: - DataProviderDelegate
extension ProductInfoViewController: DataProviderDelegate {
    func reloadData() {}
    
    func didSelect(_ field: Field) {}
}

// MARK: - ButtonHeaderViewDelegate
extension ProductInfoViewController: ButtonHeaderViewDelegate {
    func didTouchUpInsideHeaderViewButton() {}
}
