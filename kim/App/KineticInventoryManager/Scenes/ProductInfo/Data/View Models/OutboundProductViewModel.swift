//
//  OutboundProductViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 15/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

final class OutboundProductViewModel: BaseViewModel {
    init(with product: OUTBDLVItem) {
        super.init(with: [SectionItem]())
        let images = ProductInfoViewModel.images()
        sections.append(OutboundProductViewModel.requestQuantitySection(with: R.string.goods.quantityRequired(), product: product))
        sections.append(OutboundProductViewModel.packingSection(with: R.string.goods.packingInstructions()))
        sections.append(OutboundProductViewModel.locationSection(with: R.string.goods.quantity()))
        sections.append(OutboundProductViewModel.productInfoSection(with: product, title: R.string.goods.productDescription()))
        sections.append(ProductInfoViewModel.imagesSection(with: images, title: R.string.goods.images()))
    }
}

// MARK: Sections
extension OutboundProductViewModel {
    static func requestQuantitySection(with title: String, product: OUTBDLVItem) -> SectionItem {
        var fields = [Field]()
        let maxQuantity = product.itemDeliveryQuantity?.intValue() ?? 0
        var scannedQuantity = 0
        if product.packingStatusName == "Completed" {
            scannedQuantity = maxQuantity
        }
        fields.append(
            QuantityField(with: scannedQuantity, maxQuantity: maxQuantity, editable: false)
        )
        return SectionItem(title: title, fields: fields)
    }
    
    static func packingSection(with title: String) -> SectionItem {
        var fields = [Field]()
        fields.append(Field(title: "Box 1"))
        return SectionItem(title: title, fields: fields)
    }
    
    static func locationSection(with title: String) -> SectionItem {
        var fields = [Field]()
        let imageInfo = ImageInfo(imageURL: "Location", imageType: .asset)
        fields.append(ImageField(imageInfo: imageInfo))
        return SectionItem(title: title, fields: fields)
    }
    
    static func productInfoSection(with product: OUTBDLVItem, title: String) -> SectionItem {
        var fields = [Field]()
        
        fields.append(Field(title: R.string.goods.size(), detail: ""))
        fields.append(Field(title: R.string.goods.colour(), detail: ""))
        fields.append(Field(title: R.string.goods.sole(), detail: ""))
        fields.append(Field(title: R.string.goods.upper(), detail: ""))
        fields.append(Field(title: R.string.goods.sku(), detail: ""))
        
        return SectionItem(title: title, fields: fields)
    }
    
    static func imagesSection(with products: [[String: Any]], title: String) -> SectionItem {
        var fields = [Field]()
        for product in products {
            fields.append(ProductImageField(with: product))
        }
        return SectionItem(title: title, fields: fields)
    }
}
