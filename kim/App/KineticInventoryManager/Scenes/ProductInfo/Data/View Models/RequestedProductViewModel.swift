//
//  RequestedProductViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 01/05/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

final class RequestedProductViewModel: BaseViewModel {
    init(with product: AProductType) {
        super.init(with: [SectionItem]())
        let images = ProductInfoViewModel.images()
        sections.append(RequestedProductViewModel.requestQuantitySection(with: R.string.goods.quantityRequired(), product: product))
        sections.append(RequestedProductViewModel.packingSection(with: R.string.goods.packingInstructions()))
        sections.append(RequestedProductViewModel.locationSection(with: R.string.goods.quantity()))
        sections.append(RequestedProductViewModel.productInfoSection(with: product, title: R.string.goods.productDescription()))
        sections.append(ProductInfoViewModel.imagesSection(with: images, title: R.string.goods.images()))
    }
}

// MARK: Sections
extension RequestedProductViewModel {
    static func requestQuantitySection(with title: String, product: AProductType) -> SectionItem {
        var fields = [Field]()
        let maxQuantity = Int(product.purchaseOrderQuantityUnit ?? "0") ?? 0
        let scannedQuantity = maxQuantity
        fields.append(
            QuantityField(with: scannedQuantity, maxQuantity: maxQuantity, editable: false)
        )
        return SectionItem(title: title, fields: fields)
    }
    
    static func packingSection(with title: String) -> SectionItem {
        var fields = [Field]()
        fields.append(Field(title: "Box 1"))
        return SectionItem(title: title, fields: fields)
    }
    
    static func locationSection(with title: String) -> SectionItem {
        var fields = [Field]()
        let imageInfo = ImageInfo(imageURL: "Location", imageType: .asset)
        fields.append(ImageField(imageInfo: imageInfo))
        return SectionItem(title: title, fields: fields)
    }
    
    static func productInfoSection(with product: AProductType, title: String) -> SectionItem {
        var fields = [Field]()
        
        fields.append(Field(title: R.string.goods.size(), detail: ""))
        fields.append(Field(title: R.string.goods.colour(), detail: ""))
        fields.append(Field(title: R.string.goods.sole(), detail: ""))
        fields.append(Field(title: R.string.goods.upper(), detail: ""))
        fields.append(Field(title: R.string.goods.sku(), detail: ""))
        
        return SectionItem(title: title, fields: fields)
    }
    
    static func imagesSection(with products: [[String: Any]], title: String) -> SectionItem {
        var fields = [Field]()
        for product in products {
            fields.append(ProductImageField(with: product))
        }
        return SectionItem(title: title, fields: fields)
    }
}
