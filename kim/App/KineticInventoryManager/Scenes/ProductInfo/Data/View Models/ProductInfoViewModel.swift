//
//  ProductInfoViewModel.swift
//  KineticInventoryManager
//
//  Created by Raka Chowdhury on 25/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

final class ProductInfoViewModel: BaseViewModel {
    static func imagesSection(with products: [[String: Any]], title: String) -> SectionItem {
        var fields = [Field]()
        for product in products {
            fields.append(ProductImageField(with: product))
        }
        return SectionItem(title: title, fields: fields)
    }
    
    static func images() -> [[String: Any]]{
        return [
            [
                "imageURL" : "SAPLogo",
                "description" : "Side",
            ],
            [
                "imageURL" : "SAPLogo",
                "description" : "Front",
            ],
            [
                "imageURL" : "SAPLogo",
                "description" : "Back",
            ]
        ]
    }
}
