//
//  DeliveredProductViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 06/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

final class DeliveredProductViewModel: BaseViewModel {
    init(with product: DLVItem) {
        super.init(with: [SectionItem]())
        let images = ProductInfoViewModel.images()
        sections.append(DeliveredProductViewModel.requestQuantitySection(with: R.string.goods.quantityRequired(), product: product))
        sections.append(DeliveredProductViewModel.packingSection(with: "Packing Instructions"))
        sections.append(DeliveredProductViewModel.locationSection(with: "Location"))
        sections.append(DeliveredProductViewModel.productInfoSection(with: product, title: R.string.goods.productDescription()))
        sections.append(ProductInfoViewModel.imagesSection(with: images, title: R.string.goods.images()))
    }
}

// MARK: Sections
extension DeliveredProductViewModel {
    static func requestQuantitySection(with title: String, product: DLVItem) -> SectionItem {
        var fields = [Field]()
        fields.append(
            QuantityField(with: product.itemDeliveryQuantity?.intValue() ?? 0, editable: false)
        )
        return SectionItem(title: title, fields: fields)
    }
    
    static func packingSection(with title: String) -> SectionItem {
        var fields = [Field]()
        fields.append(Field(title: "Box 1"))
        return SectionItem(title: title, fields: fields)
    }
    
    static func locationSection(with title: String) -> SectionItem {
        var fields = [Field]()
        let imageInfo = ImageInfo(imageURL: "Location", imageType: .asset)
        fields.append(ImageField(imageInfo: imageInfo))
        return SectionItem(title: title, fields: fields)
    }
    
    static func productInfoSection(with product: DLVItem, title: String) -> SectionItem {
        var fields = [Field]()
        
        fields.append(Field(title: R.string.goods.size(), detail: ""))
        fields.append(Field(title: R.string.goods.colour(), detail: ""))
        fields.append(Field(title: R.string.goods.sole(), detail: ""))
        fields.append(Field(title: R.string.goods.upper(), detail: ""))
        fields.append(Field(title: R.string.goods.sku(), detail: ""))
        
        return SectionItem(title: title, fields: fields)
    }
    
    static func imagesSection(with products: [[String: Any]], title: String) -> SectionItem {
        var fields = [Field]()
        for product in products {
            fields.append(ProductImageField(with: product))
        }
        return SectionItem(title: title, fields: fields)
    }
}
