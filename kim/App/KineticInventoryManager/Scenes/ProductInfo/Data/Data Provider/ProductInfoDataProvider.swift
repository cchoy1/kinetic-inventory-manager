//
//  ProductInfoDataProvider.swift
//  KineticInventoryManager
//
//  Created by Raka Chowdhury on 25/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

final class ProductInfoDataProvider: FioriBaseTableDataProvider {}

// MARK: - Override UITableViewDataSource
extension ProductInfoDataProvider {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let field = viewModel.item(for: indexPath) as? Field,
            let cell = dequeCell(with: field, for: tableView) else {
            return UITableViewCell()
        }
        return cell
    }
    
    func dequeCell(with field: Field, for tableView: UITableView) -> UITableViewCell? {
        if let productImageField = field as? ProductImageField,
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleImageTableViewCell.className) as? TitleImageTableViewCell {
            cell.update(with: productImageField)
            return cell
        } else if let quantityField = field as? QuantityField,
            let cell = tableView.dequeueReusableCell(withIdentifier: QuantityCell.className) as? QuantityCell {
            cell.update(with: quantityField)
            return cell
        } else if let imageField = field as? ImageField,
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageTableViewCell.className) as? ImageTableViewCell {
            cell.update(with: imageField)
            return cell
        } else if let cell = tableView.dequeueReusableCell(withIdentifier: TitleDetailTableViewCell.className) as? TitleDetailTableViewCell {
            cell.update(with: field)
            return cell
        }
        return nil
    }
}
