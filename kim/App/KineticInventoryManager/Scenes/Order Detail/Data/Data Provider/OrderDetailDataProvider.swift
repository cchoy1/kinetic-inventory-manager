//
//  OrderDetailDataProvider.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 27/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

final class OrderDetailDataProvider: FioriBaseTableDataProvider {}

// MARK: - Override UITableViewDataSource
extension OrderDetailDataProvider {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let field = viewModel.item(for: indexPath) as? Field,
            let cell = dequeCell(with: field, for: tableView) else {
                return UITableViewCell()
        }
        return cell
    }
    
    func dequeCell(with field: Field, for tableView: UITableView) -> UITableViewCell? {
        if let orderDetailField = field as? ProductDetailField,
            let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailTableViewCell.className) as? ProductDetailTableViewCell {
            cell.update(with: orderDetailField)
            return cell
        } else if let cell = tableView.dequeueReusableCell(withIdentifier: TitleDetailTableViewCell.className) as? TitleDetailTableViewCell {
            cell.update(with: field)
            return cell
        }
        return nil
    }
}
