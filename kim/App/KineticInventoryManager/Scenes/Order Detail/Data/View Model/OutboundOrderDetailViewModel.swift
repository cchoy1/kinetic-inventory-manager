//
//  OutboundOrderDetailViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 15/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

final class OutboundOrderDetailViewModel: BaseViewModel {
    init(with order: OUTBDLVHead) {
        super.init(with: [SectionItem]())
        let goods = order.items
        let pendingGoods = goods.filter { $0.packingStatusName == "Not started" } // case sensitive
        let packedGoods = goods.filter { $0.packingStatusName == "Completed" } // case sensitive
        if packedGoods.count > 0 {
            sections.append(OutboundOrderDetailViewModel.orderProductsSection(with: packedGoods, title: R.string.goods.itemsLoaded()))
        }
        if pendingGoods.count > 0 {
            sections.append(OutboundOrderDetailViewModel.orderProductsSection(with: pendingGoods, title: R.string.goods.itemsNeeded()))
        }
        sections.append(OutboundOrderDetailViewModel.orderIssuesSection(with: order, title: R.string.goods.issues()))
        sections.append(OutboundOrderDetailViewModel.orderDetailsSection(with: order, title: R.string.goods.orderDetails()))
    }
}

extension OutboundOrderDetailViewModel {
    static func orderProductsSection(with products: [OUTBDLVItem], title: String) -> SectionItem {
        var fields = [Field]()
        for product in products {
            fields.append(ProductDetailField(withOutbound: product))
        }
        return SectionItem(title: title, fields: fields)
    }
    
    static func orderIssuesSection(with order: OUTBDLVHead, title: String) -> SectionItem {
        var fields = [Field]()
        fields.append(
            Field(title:R.string.goods.deliveryFrom(order.shipToPartyName ?? ""), detail: "1 Missing")
        )
        return SectionItem(title: title, fields: fields)
    }
    
    static func orderDetailsSection(with order: OUTBDLVHead, title: String) -> SectionItem {
        var fields = [Field]()
        let status: OrderStatus = .pending
        fields.append(
            Field(title: R.string.goods.status(), detail: status.title)
        )
        
        let boxesCount = ""
        fields.append(
            Field(title: R.string.goods.noOfBoxes(), detail: "\(boxesCount)")
        )
        
        if let date = order.deliveryDatePlan {
            fields.append(
                Field(title: R.string.goods.collection(), detail: GoodsHelper.format(date.utc()))
            )
        } else {
            fields.append(
                Field(title: R.string.goods.collection(), detail: " ")
            )
        }
        
        fields.append(
            Field(title: R.string.goods.box1HuNo(), detail: "")
        )
        
        fields.append(
            Field(title: R.string.goods.deliveryNumber(), detail: order.outboundDelivery ?? "")
        )
        
        fields.append(
            Field(title: R.string.goods.from(), detail: DefaultWarehouseHelper.fetchDefaultWarehouseName() ?? "")
        )
        
        fields.append(
            Field(title: R.string.goods.to(), detail: order.shipToPartyName ?? "")
        )
        
        let quantity = order.numberOfItems ?? 0
        fields.append(
            Field(title: R.string.goods.totalItems(), detail: "\(quantity)")
        )
        
        fields.append(
            Field(title: R.string.goods.requestedBy(), detail: "")
        )
        
        fields.append(
            Field(title: R.string.goods.courier(), detail: order.carrierName ?? "")
        )

        
        return SectionItem(title: title, fields: fields)
    }
}
