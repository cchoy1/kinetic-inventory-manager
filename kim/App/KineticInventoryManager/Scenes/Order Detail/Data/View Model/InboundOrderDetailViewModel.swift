//
//  OrderDetailViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 27/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

final class InboundOrderDetailViewModel: BaseViewModel {
    init(with order: DLVHead) {
        super.init(with: [SectionItem]())
        sections.append(InboundOrderDetailViewModel.orderProductsSection(with: order, title: R.string.goods.itemsLoaded()))
        sections.append(InboundOrderDetailViewModel.orderIssuesSection(with: order, title: R.string.goods.issues()))
        sections.append(InboundOrderDetailViewModel.orderDetailsSection(with: order, title: R.string.goods.orderDetails()))
    }
}

extension InboundOrderDetailViewModel {
    static func orderProductsSection(with order: DLVHead, title: String) -> SectionItem {
        var fields = [Field]()
        let products = order.items
        for product in products {
            fields.append(ProductDetailField(withDelivered: product))
        }
        return SectionItem(title: title, fields: fields)
    }
    
    static func orderIssuesSection(with order: DLVHead, title: String) -> SectionItem {
        var fields = [Field]()
        fields.append(
            //TODO Removing for demo purposes
            //Field(title: order.productName ?? "", detail: "1 Missing")
            Field(title: order.productName ?? "", detail: "")
        )
        return SectionItem(title: title, fields: fields)
    }
    
    static func orderDetailsSection(with order: DLVHead, title: String) -> SectionItem {
        var fields = [Field]()
        let status: OrderStatus = .pending
        fields.append(
            Field(title: R.string.goods.status(), detail: status.title)
        )
        
        if let date = order.plannedDeliveryEndDate {
            fields.append(
                Field(title: R.string.goods.arrival(), detail: GoodsHelper.format(date.utc()))
            )
        } else {
            fields.append(
                Field(title: R.string.goods.arrival(), detail: " ")
            )
        }
        
        let boxesCount = ""
        fields.append(
            Field(title: R.string.goods.noOfBoxes(), detail: "\(boxesCount)")
        )
        
        fields.append(
            Field(title: R.string.goods.box1HuNo(), detail: "")
        )
        
        fields.append(
            Field(title: R.string.goods.from(), detail: order.shipFromPartyName ?? "")
        )
        
        fields.append(
            Field(title: R.string.goods.to(), detail: "")
        )
        
        let quantity = order.numberOfItems ?? 0
        fields.append(
            Field(title: R.string.goods.totalItems(), detail: "\(quantity)")
        )
        
        fields.append(
            Field(title: R.string.goods.requestedBy(), detail: "")
        )
        
        fields.append(
            Field(title: R.string.goods.courier(), detail: order.carrierName ?? "")
        )
        
        fields.append(
            Field(title: R.string.goods.materialDocument(), detail: "")
        )
        
        fields.append(
            Field(title: R.string.goods.inboundDelivery(), detail: order.inboundDelivery ?? "")
        )
        
        fields.append(
            Field(title: R.string.goods.outboundDelivery(), detail: "")
        )
        
        fields.append(
            Field(title: R.string.goods.deliveryBay(), detail: "")
        )
        
        if let date = order.plannedDeliveryEndDate {
            fields.append(
                Field(title: R.string.goods.collection(), detail: GoodsHelper.format(date.utc()))
            )
        }
            
        return SectionItem(title: title, fields: fields)
    }
}
