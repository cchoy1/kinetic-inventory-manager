//
//  OrderDetailViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 27/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

protocol OrderDetailViewControllerDelegate: class {
    func didSubmitOrder()
}

class OrderDetailViewController: KIMBaseViewController, BaseTableViewable {
    @IBOutlet var tableView: FioriBaseTableView!
    @IBOutlet var submitButton: UIBarButtonItem!
    @IBOutlet var toolBarButtonView: ToolbarButtonView!
    
    var dataProvider: TableDataProviderable!
    
    private var order: Any!
    private var isPackGoods: Bool = false
    private var homeFeedData: HomeFeedData!
    private weak var delegate: OrderDetailViewControllerDelegate?
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewAppearance()
        refresh()
    }
    
    // MARK: - Setup View Appearance
    func setupViewAppearance() {
        setup(with: order!)
        setupTableView()
        localise()
    }
    
    func setup(with order: Any) {
        var title = ""
        var toolbarButtonField: ButtonField!
        if let deliveredOrder = order as? DLVHead {
            title = deliveredOrder.productName ?? ""
            if deliveredOrder.completionStatusName == "Completed" {
                navigationItem.rightBarButtonItem = nil
            }
            toolbarButtonField = GoodsInViewModel.toolbarButtonField()
            print(deliveredOrder.inboundDelivery ?? "")
        } else if let outboundOrder = order as? OUTBDLVHead {
            title = R.string.goods.deliveryTo(outboundOrder.shipToPartyName ?? "")
            if outboundOrder.completionStatusName == "Completed" {
                navigationItem.rightBarButtonItem = nil
            }
            isPackGoods = outboundOrder.allItemsPacked() ? false : true
            toolbarButtonField = GoodsOutViewModel.toolbarButtonField(forPackedGoods: outboundOrder.allItemsPacked())
            print(outboundOrder.outboundDeliveryUUID ?? "")
        }
        setupNavigationBar(title: title, isHidden: false, hidesBackButton: false)
        toolBarButtonView.update(with: self)
        toolBarButtonView.update(with: toolbarButtonField)
    }
    
    func localise() {
        if submitButton != nil {
            submitButton.title = R.string.generic.submit()
        }
    }
    
    // MARK: - Setup Table
    private func setupTableView() {
        setupDefaultTableData()
        setupTableView(with: dataProvider, delegate: dataProvider)
    }
    
    // MARK: - Setup Table: BaseTableViewable
    func setupDefaultTableData() {
        let viewModel = fetchViewModel(with: order!)
        self.dataProvider = OrderDetailDataProvider(with: tableView, viewModel: viewModel, delegate: self)
        self.dataProvider.reloadTableView()
    }
    
    // MARK: - Reload Data
    func refresh() {
        if let outboundOrder = order as? OUTBDLVHead, submitButton != nil {
            submitButton.isEnabled = outboundOrder.allItemsPacked() ? true : false
        }
    }
    
    func set(order: Any, delegate: OrderDetailViewControllerDelegate?, homeFeedData: HomeFeedData) {
        self.order = order
        self.delegate = delegate
        self.homeFeedData = homeFeedData
    }
}

extension OrderDetailViewController {
    private func fetchViewModel(with order: Any) -> BaseViewModel {
        var viewModel: BaseViewModel = BaseViewModel(with: [])
        if let deliveredOrder = order as? DLVHead {
            viewModel = InboundOrderDetailViewModel(with: deliveredOrder)
        } else if let outboundOrder = order as? OUTBDLVHead {
            viewModel = OutboundOrderDetailViewModel(with: outboundOrder)
        }
        return viewModel
    }
}

// MARK: - IBActions
extension OrderDetailViewController {
    @IBAction func didTouchUpInsideSubmitButton(_ sender: Any) {
        if let deliveredOrder = order as? DLVHead {
            showFioriLoadingIndicator()
            WarehouseTaskService.submitInboundOrder(with: onlineODataProvider, deliveredOrder: deliveredOrder) { (error, result) in
                DispatchQueue.main.async {
                    self.hideFioriLoadingIndicator()
                    if let error = error {
                        self.presentOkAlert(message: error.localizedDescription)
                    } else {
                        self.delegate?.didSubmitOrder()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        } else if let outboundOrder = order as? OUTBDLVHead {
            if outboundOrder.allItemsPacked() && isPackGoods == false {
                showFioriLoadingIndicator()
                LoadGoodsService.loadGoods(with: onlineODataProvider, outboundOrder: outboundOrder) { (error, result) in
                    DispatchQueue.main.async {
                        self.hideFioriLoadingIndicator()
                        if let error = error {
                            self.presentOkAlert(message: error.localizedDescription)
                        } else {
                            self.delegate?.didSubmitOrder()
                            if self.isBeingPresented {
                                self.navigationController?.dismiss(animated: true, completion: nil)
                            } else {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                }
            } else {
                guard outboundOrder.items.count > 0 else {
                    presentOkAlert(message: "Items not packed")
                    return
                }
                showFioriLoadingIndicator()
                PackGoodsService.submitOutboundOrder(with: onlineODataProvider, warehouseID: DefaultWarehouseHelper.fetchDefaultWarehouseId(), outboundOrder: outboundOrder) { (error, result) in
                    DispatchQueue.main.async {
                        self.hideFioriLoadingIndicator()
                        if let error = error {
                            self.presentOkAlert(message: error.localizedDescription)
                        } else {
                            self.delegate?.didSubmitOrder()
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
}

// MARK: - ToolbarButtonViewDelegate
extension OrderDetailViewController: ToolbarButtonViewDelegate {
    func didTouchUpInsideToolbarButtonView() {
        goToScan()
    }
}

// MARK: - DataProviderDelegate
extension OrderDetailViewController: DataProviderDelegate {
    func reloadData() {
        refresh()
    }
    
    func didSelect(_ field: Field) {
        
        if let deliveredOrder = order as? DLVHead, let productDetailField = field as? ProductDetailField {
            if let product = deliveredOrder.items.first(where: { $0.inboundDeliveryItem ?? "" == productDetailField.id }) {
                showInfo(for: product)
            }
        } else if let outboundOrder = order as? OUTBDLVHead, let productDetailField = field as? ProductDetailField {
            if let product = outboundOrder.items.first(where: { $0.outboundDeliveryItem ?? "" == productDetailField.id }) {
                showInfo(for: product)
            }
        }
    }
}

// MARK: - ScanGoodsViewControllerDelegate
extension OrderDetailViewController: ScanGoodsViewControllerDelegate {
    func didReturn(updatedOrder: Any) {
        order = updatedOrder
        let viewModel = fetchViewModel(with: order!)
        dataProvider.update(with: viewModel)
        refresh()
    }
}

// MARK: - Navigation
extension OrderDetailViewController {
    func showInfo(for product: Any) {
        guard let productInfoViewController = R.storyboard.productInfo.productInfoViewController() else {
            return
        }
        productInfoViewController.product = product
        self.navigationController?.pushViewController(productInfoViewController, animated: true)
    }
    
    func goToScan() {
        var viewController: UIViewController!
        if let _ = order as? DLVHead {
            // Scan to Unload goods
            guard let scanGoodsInViewController = R.storyboard.scanGoods.scanGoodsInViewController() else {
                return
            }
            scanGoodsInViewController.onlineODataProvider = onlineODataProvider
            scanGoodsInViewController.update(with: homeFeedData, isGoodsIn: true, delegate: self)
            viewController = scanGoodsInViewController
        } else if let _ = order as? OUTBDLVHead {
            // Scan to Load Goods (Goods Issue) or Pack Goods
            guard let scanGoodsOutViewController = R.storyboard.scanGoods.scanGoodsOutViewController() else {
                return
            }
            scanGoodsOutViewController.onlineODataProvider = onlineODataProvider
            scanGoodsOutViewController.update(with: homeFeedData, isGoodsIn: false, delegate: self)
            viewController = scanGoodsOutViewController
        }
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
}
