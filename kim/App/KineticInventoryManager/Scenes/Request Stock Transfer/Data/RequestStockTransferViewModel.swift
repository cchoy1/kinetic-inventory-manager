//
//  RequestGoodsViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 19/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

enum RequestStockTransferItems: String {
    case from
    case to
    case courier
    case collection
    case requestedBy
}

final class RequestStockTransferViewModel: BaseViewModel {
    
    var supplyingPlant: APlantType?
    var receivingPlant: APlantType?
    
    
    init(with request: PurchaseOrderERPCreateRequestConfirmationInV1, productItem: ProductItem? = nil, courier: String? = nil, collection: CollectionDate? = nil, supplyingPlant: APlantType? = nil, receivingPlant: APlantType? = nil) {
        super.init(with: [SectionItem]())
        if let product = productItem {
            sections.append(RequestStockTransferViewModel.productsSection(with: product, title: R.string.goods.itemsRequested()))
        }
        sections.append(
            RequestStockTransferViewModel.orderDetailsSection(with: request, supplyingPlant: supplyingPlant, receivingPlant: receivingPlant, collection: collection, courier: courier, title: R.string.goods.orderDetails())
        )
        sections.append(RequestStockTransferViewModel.notesSection(with: request, title: ""))
    }
}

// MARK: Sections
extension RequestStockTransferViewModel {
    static func toolbarButtonField() -> ButtonField {
        let imageInfo = ImageInfo(imageURL: "barcode.viewfinder", imageType: .sfSymbol)
        return ButtonField(with: R.string.goods.scanToAddGoods(), imageInfo: imageInfo)
    }
    
    static func productsSection(with productItem: ProductItem, title: String) -> SectionItem {
        var fields = [Field]()
        fields.append(
            ProductDetailField(withRequested: productItem)
        )
        return SectionItem(title: title, fields: fields)
    }

    static func orderDetailsSection(with orderRequest: PurchaseOrderERPCreateRequestConfirmationInV1, supplyingPlant: APlantType?, receivingPlant: APlantType?, collection: CollectionDate?, courier: String?, title: String) -> SectionItem {
        var fields = [Field]()
        fields.append(
            Field(with: RequestStockTransferItems.from.rawValue, title: R.string.goods.from(), detail: supplyingPlant?.plantName ?? "", accessoryType: .disclosureIndicator)
        )
        fields.append(
            Field(with: RequestStockTransferItems.to.rawValue, title: R.string.goods.to(), detail: receivingPlant?.plantName ?? "", accessoryType: .disclosureIndicator)
        )
        
        fields.append(
            Field(with: RequestStockTransferItems.courier.rawValue, title: R.string.goods.courier(), detail: courier ?? "", accessoryType: .disclosureIndicator)
        )
        
        var dateTime = ""
        if let collectionDate = collection {
            dateTime = TransferStockModel.format(collectionDate: collectionDate)
        }
        
        if let _ = courier {
            fields.append(
                Field(with: RequestStockTransferItems.collection.rawValue, title: R.string.goods.collection(), detail: dateTime, accessoryType: .disclosureIndicator)
            )
        }
        
        fields.append(
            Field(with: RequestStockTransferItems.requestedBy.rawValue, title: R.string.goods.requestedBy(), detail: "")
        )
        
        return SectionItem(title: title, fields: fields)
    }
    
    static func notesSection(with request: PurchaseOrderERPCreateRequestConfirmationInV1, title: String) -> SectionItem {
        var fields = [Field]()
        fields.append(
            NotesField(with: "", placeholder: R.string.generic.notes(), editable: true)
        )
        return SectionItem(title: title, fields: fields)
    }
}
