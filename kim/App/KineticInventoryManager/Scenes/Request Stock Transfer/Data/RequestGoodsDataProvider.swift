//
//  RequestGoodsDataProvider.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 19/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit
import SAPFiori

final class RequestStockTransferDataProvider: FioriBaseTableDataProvider {}

// MARK: - Override UITableViewDataSource
extension RequestStockTransferDataProvider {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let field = viewModel.item(for: indexPath) as? Field,
            let cell = dequeCell(with: field, for: tableView) else {
            return UITableViewCell()
        }
        return cell
    }
    
    func dequeCell(with field: Field, for tableView: UITableView) -> UITableViewCell? {
        if let notesField = field as? NotesField,
            let cell = tableView.dequeueReusableCell(withIdentifier: NoteTableViewCell.className) as? NoteTableViewCell {
            cell.update(with: notesField)
            return cell
        } else if let productDetailField = field as? ProductDetailField,
            let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailTableViewCell.className) as? ProductDetailTableViewCell {
            cell.update(with: productDetailField)
            return cell
        } else if let cell = tableView.dequeueReusableCell(withIdentifier: TitleDetailTableViewCell.className) as? TitleDetailTableViewCell {
            cell.update(with: field)
            return cell
        }
        return nil
    }
}
