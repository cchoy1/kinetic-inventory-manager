//
//  RequestGoodsViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 19/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class RequestStockTransferViewController: KIMBaseViewController, BaseTableViewable {
    @IBOutlet var submitButton: UIBarButtonItem!
    
    @IBOutlet var tableView: FioriBaseTableView!
    @IBOutlet var toolBarButtonView: ToolbarButtonView!
    
    var isSelectingWarehouseFrom: Bool!
    var dataProvider: TableDataProviderable!
    
    var courier: String?
    var supplyingPlant: APlantType?
    var receivingPlant: APlantType?
    var productItem: ProductItem?
    var collectionDate: CollectionDate?
    
    var purchaseOrder: PurchaseOrderERPCreateRequestConfirmationInV1!
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewAppearance()
        refresh()
    }
    
    // MARK: - Setup View Appearance
    func setupViewAppearance() {
        setupNavigationBar(title: R.string.goods.request(), isHidden: false)
        setupTableView()
        localise()
    }
    
    func localise() {
        submitButton.title = R.string.generic.submit()
        submitButton.isEnabled = false
    }
    
    // MARK: - Setup Table
    private func setupTableView() {
        setupDefaultTableData()
        setupTableView(with: dataProvider, delegate: dataProvider)
    }
    
    // MARK: - Setup Table: BaseTableViewable
    func setupDefaultTableData() {
        if purchaseOrder == nil {
            purchaseOrder = PurchaseOrderERPCreateRequestConfirmationInV1()
        }
        
        let requestGoodsViewModel = RequestStockTransferViewModel(with: purchaseOrder)
        dataProvider = RequestStockTransferDataProvider(with: tableView, viewModel: requestGoodsViewModel, delegate: self)
        
        let toolbarButtonField = RequestStockTransferViewModel.toolbarButtonField()
        toolBarButtonView.update(with: self)
        toolBarButtonView.update(with: toolbarButtonField)
    }
    
    // MARK: - Reload Data
    func refresh() {
        self.updateSubmitButton()
        let updatedRequestGoodsViewModel = RequestStockTransferViewModel(with: purchaseOrder,
                                                                         productItem: productItem,
                                                                         courier: courier,
                                                                         collection: collectionDate,
                                                                         supplyingPlant: supplyingPlant,
                                                                         receivingPlant: receivingPlant)
        self.dataProvider.update(with: updatedRequestGoodsViewModel)
    }
    
    func updateSubmitButton() {
        submitButton.isEnabled = canSubmit()
    }
    
    func validate() {
        guard let _ = supplyingPlant else {
            presentOkAlert(message: "Select a Warehouse to send to")
            return
        }
        
        guard let _ = receivingPlant else {
            presentOkAlert(message: "Select a Warehouse to ship to")
            return
        }
        
        guard let _ = courier else {
            presentOkAlert(message: "Select a Courier to ship with")
            return
        }
        
        guard let _ = productItem else {
            presentOkAlert(message: "Select at least one Product to ship")
            return
        }
    }
    
    func canSubmit() -> Bool {
        guard let _ = supplyingPlant else {
            return false
        }
        
        guard let _ = receivingPlant else {
            return false
        }
        
        guard let _ = courier else {
            return false
        }
        
        guard let _ = productItem else {
            return false
        }
        return true
    }
}

// MARK: - IBActions
extension RequestStockTransferViewController {
    @IBAction func didTouchUpInsideCancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // TODO: - Move request to a service
    @IBAction func didTouchUpInsideSubmitButton(_ sender: Any) {
        guard let plantTo = receivingPlant, let plantFrom = supplyingPlant else {
            return
        }
        showFioriLoadingIndicator()
        // Order
        purchaseOrder.processingTypeCode = "UB"
        purchaseOrder.supplyingPlantID = plantFrom.plant
        
        let responsiblePurchasingOrganisationParty = ResponsiblePurchasingOrganisationParty()
        //Setting this to 1710 as it is configured in SAP, we have just one Purchasing Organization for the whole country
        //responsiblePurchasingOrganisationParty.internalID = plantFrom.plant
        responsiblePurchasingOrganisationParty.internalID = "1710"
        purchaseOrder.responsiblePurchasingOrganisationParty = responsiblePurchasingOrganisationParty
        
        let responsiblePurchasingGroupParty = ResponsiblePurchasingGroupParty()
        responsiblePurchasingGroupParty.internalID = "003"
        purchaseOrder.responsiblePurchasingGroupParty = responsiblePurchasingGroupParty
        
        // Item
        purchaseOrder.item?.id = "10"
        purchaseOrder.item?.typeCode_ = "18"
        purchaseOrder.item?.processingTypeCode = "7"
        purchaseOrder.item?.unitCode = "2"
        
        // Product
        purchaseOrder.item?.receivingPlantID = plantTo.plant
        
        // Submit purchase order
        onlineODataProvider.createEntity(purchaseOrder) { error in
            if let _ = error {
                self.presentOkAlert(message: "Unable to submit Stock Transfer Request")
            } else {
                print("purchaseOrder.id")
                print(self.purchaseOrder.id ?? "")
                let outboundDeliveryType = CreateOutbDeliveryType()
                outboundDeliveryType.referenceSDDocument = self.purchaseOrder.id ?? ""
                self.onlineODataProvider.createEntity(outboundDeliveryType) { error in
                    self.hideFioriLoadingIndicator()
                    if let _ = error {
                        self.presentOkAlert(message: "Unable to submit Stock Transfer Request")
                    } else {
                        self.hideFioriLoadingIndicator()
                        self.navigationController?.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
}

// MARK: - DataProviderDelegate
extension RequestStockTransferViewController: DataProviderDelegate {
    func reloadData() {
        refresh()
    }
    
    func didSelect(_ field: Field) {
        if field is ProductDetailField {
            goToProductInfo()
        } else if field.id == RequestStockTransferItems.from.rawValue {
            isSelectingWarehouseFrom = true
            selectWarehouse()
        } else if field.id == RequestStockTransferItems.to.rawValue {
            isSelectingWarehouseFrom = false
            selectWarehouse()
        } else if field.id == RequestStockTransferItems.courier.rawValue {
            goToSelectItemFeedViewController(with: TransferStockModel.couriers(),
                                             navigationBarTitle: "Courier")
        } else if field.id == RequestStockTransferItems.collection.rawValue {
            let collectionDate = CollectionDate(date: Date(), hasSetTime: false)
            goToSelectItemFeedViewController(with: TransferStockModel.dateTime(with: collectionDate),
                                             navigationBarTitle: "Collection",
                                             isSelectCollectionDate: true)
        }
    }
}

// MARK: - SelectItemFeedViewControllerDelegate
extension RequestStockTransferViewController: SelectItemFeedViewControllerDelegate {
    func didSelectItem(with field: Field) {
        if field.type == .courier {
            courier = field.title
        }
        
        self.refresh()
    }
}

extension RequestStockTransferViewController: SelectCollectionDateDelegate {
    func didEndSelection(with collectionDate: CollectionDate) {
        self.collectionDate = collectionDate
        
        self.refresh()
    }
}

// MARK: - SelectWarehouseViewControllerDelegate
extension RequestStockTransferViewController: ScanProductsForSTOViewControllerDelegate {
    func didReturnSelectedProduct(with productItem: ProductItem) {
        self.productItem = productItem
        // Item
        purchaseOrder.item?.id = "10"
        purchaseOrder.item?.typeCode_ = "18"
        purchaseOrder.item?.processingTypeCode = "7"
        purchaseOrder.item?.unitCode = "1"
        
        // Product
        let product = Product()
        product.typeCode_ = "1"
        product.internalID = productItem.product.product // "EWMS4-50"
        purchaseOrder.item?.product = product
        
        refresh()
    }
}

// MARK: - SelectWarehouseViewControllerDelegate
extension RequestStockTransferViewController: SelectWarehouseViewControllerDelegate {
    func didReturnSelectedWarehouse(with warehouseId: String, warehouseName: String) {
        if isSelectingWarehouseFrom {
            guard warehouseId != receivingPlant?.plant ?? "" else {
                presentOkAlert(message: "The Warehouse 'From' must be different to the Warehouse 'To'")
                return
            }
            supplyingPlant = APlantType()
            supplyingPlant?.plant = warehouseId
            supplyingPlant?.plantName = warehouseName
        } else {
            guard warehouseId != supplyingPlant?.plant ?? "" else {
                presentOkAlert(message: "The Warehouse 'From' must be different to the Warehouse 'To'")
                return
            }
            receivingPlant = APlantType()
            receivingPlant?.plant = warehouseId
            receivingPlant?.plantName = warehouseName
        }
        refresh()
    }
}

// MARK: - ToolbarButtonViewDelegate
extension RequestStockTransferViewController: ToolbarButtonViewDelegate {
    func didTouchUpInsideToolbarButtonView() {
        // goToScan()
        goToAddGoods()
    }
}

// MARK: - Navigation
extension RequestStockTransferViewController {
    func goToAddGoods() {
        guard let scanProductsForSTOViewController = R.storyboard.scanGoods.scanProductsForSTOViewController() else {
            return
        }
        scanProductsForSTOViewController.onlineODataProvider = onlineODataProvider
        scanProductsForSTOViewController.update(with: self)
        let navigationController = UINavigationController(rootViewController: scanProductsForSTOViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func goToProductInfo() {
        guard let productInfoViewController = R.storyboard.productInfo.productInfoViewController() else {
            return
        }
        productInfoViewController.product = productItem?.product
        productInfoViewController.onlineODataProvider = onlineODataProvider
        let navigationController = UINavigationController(rootViewController: productInfoViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func goToScan() {
        guard let scanGoodsViewController = R.storyboard.scanGoods.scanGoodsViewController() else {
            return
        }
        scanGoodsViewController.onlineODataProvider = onlineODataProvider
        let navigationController = UINavigationController(rootViewController: scanGoodsViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func selectWarehouse() {
        guard let selectWarehouseViewController = R.storyboard.selectWarehouse.selectWarehouseViewController() else {
            return
        }
        selectWarehouseViewController.delegate = self
        selectWarehouseViewController.onlineODataProvider = onlineODataProvider
        let navigationController = UINavigationController(rootViewController: selectWarehouseViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func goToSelectItemFeedViewController(with items: [[String: Any]], navigationBarTitle: String, isSelectCollectionDate: Bool = false) {
        guard let selectItemFeedViewController = R.storyboard.selectItemFeed.selectItemFeedViewController() else {
            return
        }
        selectItemFeedViewController.onlineODataProvider = onlineODataProvider
        selectItemFeedViewController.update(with: self,
                                            selectCollectionDateDelegate: self,
                                            viewModel: SelectItemFeedViewModel(with: items),
                                            isSelectCollectionDate: isSelectCollectionDate,
                                            navigationBarTitle: navigationBarTitle)
        
        let navigationController = UINavigationController(rootViewController: selectItemFeedViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
}
