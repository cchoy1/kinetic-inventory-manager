//
//  KIMBaseViewController+Drawer.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 03/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

// MARK: - Drawer Appearence
extension KIMBaseViewController {
    func drawerView(with viewController: KIMBaseViewController,
                    snapPositions: [DrawerPosition] = [.collapsed, .open, .partiallyOpen],
                    blurEffect: UIBlurEffect.Style? = nil,
                    id: String? = nil) -> DrawerView {
        let drawerView = self.addDrawerView(withViewController: viewController)
        viewController.drawerView = drawerView
        drawerView.delegate = self
        drawerView.viewController = viewController
        drawerView.snapPositions = snapPositions
        drawerView.insetAdjustmentBehavior = .automatic
        drawerView.backgroundColor = .systemBackground
        if let controllerId = id {
            drawerView.setDrawerId(controllerId)
        }
        if let effectStyle = blurEffect {
            drawerView.backgroundEffect = UIBlurEffect(style: effectStyle)
        }
        drawerView.cornerRadius = 14
        return drawerView
    }
    
    func updateViewAppearanceForDrawer() {
          if drawerView == nil {
              setBackgroundColour(as: .clear)
          } else {
              setBackgroundColour(as: .white)
          }
          view.roundCorners(corners: [.topLeft, .topRight], radius: 12.0)
      }
      
      func setBackgroundColour(as colour: UIColor = .clear) {
          view.backgroundColor = colour
          if drawerView != nil {
              drawerView.backgroundColor = colour
          }
      }
}

// MARK: - Drawer Utility Methods
extension KIMBaseViewController {
    func showDrawer(drawer: DrawerView?, animated: Bool) {
        for another in drawers.compactMap({ $0.drawer }) {
            if another !== drawer {
                another.setConcealed(true, animated: animated)
            } else if another.isConcealed {
                another.setConcealed(false, animated: animated)
            } else if let nextPosition = another.getNextPosition(offsetBy: 1) ?? another.getNextPosition(offsetBy: -1) {
                currentDrawerId = another.getDrawerId()
                another.setPosition(nextPosition, animated: animated)
            }
        }
    }
    
    func load(drawerView: DrawerView) {
        hideCurrentDrawer()
        
        let drawerItem = DrawerItem(drawerView.getDrawerId(), drawerView)
        drawers = [drawerItem]
        showDrawer(drawer: drawerItem.drawer, animated: true)
    }
    
    func hideCurrentDrawer() {
        guard let activeDrawerId = currentDrawerId else {
            return
        }
        for drawer in drawers.compactMap({ $0.drawer }) {
            if drawer.getDrawerId() == activeDrawerId && drawer.isConcealed == false {
                drawer.setConcealed(true, animated: true)
                currentDrawerId = nil
                drawers = []
            }
        }
    }
    
    func hideCurrentPopup() {
        if drawers.count > 0 {
            hideCurrentDrawer()
        }
    }
    
    func isShowingDrawer(with id: String) -> Bool {
        for drawer in drawers.compactMap({ $0.drawer }) {
            if drawer.getDrawerId() == id {
                return true
            }
        }
        return false
    }
}

// MARK: - DrawerViewDelegate
extension KIMBaseViewController: DrawerViewDelegate {
    func drawer(_ drawerView: DrawerView,
                willTransitionFrom startPosition: DrawerPosition,
                to targetPosition: DrawerPosition) {
        print("drawer(_:willTransitionFrom: \(startPosition) to: \(targetPosition))")
        if startPosition == .open {}
    }
    
    func drawer(_ drawerView: DrawerView, didTransitionTo position: DrawerPosition) {
        print("drawerView(_:didTransitionTo: \(position))")
    }
    
    func drawerWillBeginDragging(_ drawerView: DrawerView) {
        print("drawerWillBeginDragging")
    }
    
    func drawerWillEndDragging(_ drawerView: DrawerView) {
        print("drawerWillEndDragging")
    }
    
    func drawerDidMove(_ drawerView: DrawerView, drawerOffset: CGFloat) {
        drawerView.cornerRadius = 12.0
    }
}
