//
//  TestViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 18/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit
import Foundation
import os
import SAPCommon
import SAPFiori
import SAPFoundation
import SAPOData

protocol DrawerViewable: class {
    var drawerView: DrawerView! { get set }
}

typealias DrawerItem = (key: String, drawer: DrawerView?)

class KIMBaseViewController: UIViewController, SAPFioriLoadingIndicator {
    var loadingIndicator: FUILoadingIndicatorView?
    var onlineODataProvider: Ec1<OnlineODataProvider>!
    
    var drawerView: DrawerView!
    
    var drawers: [DrawerItem] = []
    var currentDrawerId: String?
}

// MARK: - Setup Navigation Bar
extension KIMBaseViewController {
    func setupNavigationBar(title: String = "", largeTitle: Bool = false, isHidden: Bool = false, hidesBackButton: Bool = false, isClear: Bool = false, tintColour: UIColor? = nil) {
        self.title = title
        navigationItem.largeTitleDisplayMode = largeTitle ? .always : .automatic
        navigationController?.navigationBar.prefersLargeTitles = largeTitle
        if isHidden == false {
            if hidesBackButton {
                hideBackButton()
            }
            if isClear {
                makeNavigationBarClear()
            }
        } else {
            navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }
    
    private func hideBackButton() {
        navigationItem.leftBarButtonItem = nil
        navigationItem.setHidesBackButton(true, animated: false)
    }
    
    private func makeNavigationBarClear() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }
}

// MARK: - HUD
extension KIMBaseViewController {
    func updateHUD(toShow: Bool, with message: String = "") {
        if toShow {
            loadingIndicator?.show()
        } else {
            loadingIndicator?.dismiss()
        }
    }
}

// MARK: Log
extension KIMBaseViewController {
    func log(message: String) {
        os_log((""), String(describing: message))
    }
}

// MARK: Alerts
extension KIMBaseViewController {
    func presentOkAlert(title: String? = R.string.generic.errorTitle(), message: String?, alertTitle: String? = R.string.generic.ok()) {
        let okAction = UIAlertAction(title: alertTitle, style: .default, handler: nil)
        self.presentAlertController(title: title, message: message, style: .alert, actions: [okAction], completion: nil)
    }
    
    func presentAlertController(title: String?, message: String?, style: UIAlertController.Style, actions: [UIAlertAction], completion: (() -> Void)?) {
        let alertController = self.alertController(title: title, message: message, style: style, actions: actions)
        DispatchQueue.main.async {
            self.updateHUD(toShow: false)
            self.hideFioriLoadingIndicator()
            self.present(alertController, animated: true, completion: completion)
        }
    }
    
    func alertController(title: String?, message: String?, style: UIAlertController.Style, actions: [UIAlertAction]) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        for action in actions {
            alertController.addAction(action)
        }
        return alertController
    }
}
