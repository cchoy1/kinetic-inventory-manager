//
//  HomeFeedSection.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 17/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

enum HomeFeedSection: String {
    case goodsIn
    case goodsOut
    case schedule
    
    var title: String {
        switch self {
        case .goodsIn:
            return R.string.goods.arrivingToday()
        case .goodsOut:
            return R.string.goods.leavingToday()
        case .schedule:
            return R.string.goods.schedule()
        }
    }
    
    var imageURL: String {
        switch self {
        case .goodsIn:
            return "GoodsIn"
        case .goodsOut:
            return "GoodsOut"
        case .schedule:
            return "Schedule"
        }
    }
}
