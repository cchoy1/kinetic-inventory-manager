//
//  HomeFeedViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 18/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

final class HomeFeedViewModel: BaseViewModel {
    init(with goodsIn: Int, goodsOut: Int) {
        super.init(with: [SectionItem]())
        sections.append(HomeFeedViewModel.goodsSection(with: goodsIn, goodsOut: goodsOut))
        sections.append(HomeFeedViewModel.sheduleSection())
    }
}

// MARK: Sections
extension HomeFeedViewModel {
    static func goodsIn(with value: Int, type: HomeFeedSection) -> Field {
        var imageInfo: ImageInfo!
        if #available(iOS 13.0, *) {
            imageInfo = ImageInfo(imageURL: type.imageURL, imageType: .asset, tintColour: .link)
        } else {
            imageInfo = ImageInfo(imageURL: type.imageURL, imageType: .asset, tintColour: .systemBlue)
        }
        return ImageField(with: type.rawValue, title: type.title, detail: "\(value)", imageInfo: imageInfo)
    }
    
    static func goodsSection(with goodsIn: Int, goodsOut: Int) -> SectionItem {
        let goodsInField = HomeFeedViewModel.goodsIn(with: goodsIn, type: .goodsIn)
        let goodsOutField = HomeFeedViewModel.goodsIn(with: goodsOut, type: .goodsOut)
        let fields = [goodsInField, goodsOutField]
        return SectionItem(title: "", fields: fields)
    }
    
    static func sheduleSection() -> SectionItem {
        let section = HomeFeedSection.schedule
        let scheduleImageInfo = ImageInfo(imageURL: section.imageURL, imageType: .asset)
        let scheduleField = ImageField(with: section.rawValue, imageInfo: scheduleImageInfo)
        return SectionItem(title: section.title, fields: [scheduleField])
    }
    
    static func headerViewField() -> ButtonField {
        return ButtonField(with: R.string.goods.request())
    }
}
