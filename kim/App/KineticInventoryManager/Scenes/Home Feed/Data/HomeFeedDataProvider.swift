//
//  HomeFeedDataProvider.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 18/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

final class HomeFeedDataProvider: FioriBaseTableDataProvider {}

// MARK: - Override UITableViewDataSource
extension HomeFeedDataProvider {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let field = viewModel.item(for: indexPath) as? Field,
            let cell = dequeCell(with: field, for: tableView) else {
            return UITableViewCell()
        }
        return cell
    }
    
    func dequeCell(with field: Field, for tableView: UITableView) -> UITableViewCell? {
        if let imageField = field as? ImageField {
            if imageField.id == HomeFeedSection.schedule.rawValue, let cell = tableView.dequeueReusableCell(withIdentifier: CardImageTableViewCell.className) as? CardImageTableViewCell {
                cell.update(with: imageField)
                return cell
            } else if let cell = tableView.dequeueReusableCell(withIdentifier: GoodsTableViewCell.className) as? GoodsTableViewCell {
                cell.update(with: imageField)
                return cell
            }
        }
        return nil
    }
}
