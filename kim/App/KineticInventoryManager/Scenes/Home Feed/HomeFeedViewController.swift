//
//  HomeFeedViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 18/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class HomeFeedViewController: KIMBaseViewController, BaseTableViewable {
    @IBOutlet var tableView: FioriBaseTableView!
    @IBOutlet var selectLocationButton: UIButton!
    
    var dataProvider: TableDataProviderable!
    
    var homeFeedData: HomeFeedData!
    var homeFeedService: HomeFeedService!
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationItems()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refresh()
    }
    
    // MARK: - Setup View
    private func setupNavigationItems() {
        setupNavigationBar(title: R.string.goods.deliveries(), largeTitle: true)
        setupSelectLocationButton()
    }
    
    private func setupSelectLocationButton() {
        updateLocationButtonTitle()
        selectLocationButton.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
    }
    
    private func updateLocationButtonTitle() {
        selectLocationButton.setTitle(DefaultWarehouseHelper.fetchDefaultWarehouseName(), for: .normal)
    }
    
    // MARK: - Setup Table
    private func setupTableView() {
        registerCells()
        setupDefaultTableData()
        setupTableView(with: dataProvider, delegate: dataProvider)
    }
    
    func registerCells() {
        let goodsCellTableViewCellNib = UINib(nibName: GoodsTableViewCell.className, bundle: .main)
        tableView.register(goodsCellTableViewCellNib, forCellReuseIdentifier: GoodsTableViewCell.className)
    }
    
    // MARK: - Setup Default Table Data
    func setupDefaultTableData() {
        homeFeedData = HomeFeedData(goodsIn: [], goodsOut: [])
        update(with: homeFeedData)
    }
    
    func update(with data: HomeFeedData) {
        homeFeedData = data
        let homeFeedViewModel = HomeFeedViewModel(with: homeFeedData.goodsIn.count, goodsOut: homeFeedData.goodsOut.count)
        if dataProvider == nil {
            dataProvider = HomeFeedDataProvider(with: tableView, viewModel: homeFeedViewModel, delegate: self)
        } else {
            dataProvider.update(with: homeFeedViewModel)
        }
    }
    
    // MARK: - Reload Data
    func refresh() {
        updateLocationButtonTitle()
        if homeFeedService == nil {
            homeFeedService = HomeFeedService(with: onlineODataProvider)
        }
        homeFeedService.fetchHomeFeedData(with: DefaultWarehouseHelper.fetchDefaultWarehouseId()) { (error, data) in
            if let feedData = data {
                self.update(with: feedData)
            }
            if let _ = error {
                DispatchQueue.main.async {
                    self.presentOkAlert(message: R.string.generic.errorGenericMessage())
                }
            }
            self.hideFioriLoadingIndicator()
        }
    }
}

// MARK: - IBActions
extension HomeFeedViewController: DataProviderDelegate {
    @IBAction func didTouchUpInsideSearchButton(_ sender: Any) {
        selectWarehouse()
    }
}

// MARK: - DataProviderDelegate
extension HomeFeedViewController {
    func reloadData() {
        refresh()
    }
    
    func didSelect(_ field: Field) {
        if field.id == HomeFeedSection.goodsIn.rawValue {
            showGoodsIn()
        } else if field.id == HomeFeedSection.goodsOut.rawValue {
            showGoodsOut()
        }
    }
}

// MARK: - SelectWarehouseViewControllerDelegate
extension HomeFeedViewController: SelectWarehouseViewControllerDelegate {
    func didReturnSelectedWarehouse(with warehouseId: String, warehouseName: String) {
        self.showFioriLoadingIndicator()
        DefaultWarehouseHelper.saveDefaultWarehouse(with: onlineODataProvider, id: warehouseId, name: warehouseName) { (error) in
            if let _ = error {
                self.presentOkAlert(message: R.string.generic.errorGenericMessage())
                return
            } else {
                DispatchQueue.main.async {
                    self.hideFioriLoadingIndicator()
                    self.selectLocationButton.setTitle(warehouseName, for: .normal)
                    self.refresh()
                }
            }
        }
    }
}

// MARK: - Navigation
extension HomeFeedViewController {
    func showGoodsIn() {
        showGoodsFeed(forGoodsIn: true)
    }
    
    func showGoodsOut() {
        showGoodsFeed(forGoodsIn: false)
    }
    
    func showGoodsFeed(forGoodsIn: Bool) {
        guard let goodsInViewController = R.storyboard.goodsInOut.goodsInOutViewController() else {
            return
        }
        goodsInViewController.onlineODataProvider = onlineODataProvider
        goodsInViewController.update(data: homeFeedData, forGoodsIn: forGoodsIn)
        let navigationController = UINavigationController(rootViewController: goodsInViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func showProductDetails() {
        guard let productDetailViewController = R.storyboard.productDetail.productDetailViewController() else {
            return
        }
        productDetailViewController.onlineODataProvider = onlineODataProvider
        let navigationController = UINavigationController(rootViewController: productDetailViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func selectWarehouse() {
        guard let selectWarehouseViewController = R.storyboard.selectWarehouse.selectWarehouseViewController() else {
            return
        }
        selectWarehouseViewController.delegate = self
        selectWarehouseViewController.onlineODataProvider = onlineODataProvider
        let navigationController = UINavigationController(rootViewController: selectWarehouseViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
}
