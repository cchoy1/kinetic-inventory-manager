//
//  GoodsCellTableViewCell.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 23/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class GoodsTableViewCell: UITableViewCell {
    @IBOutlet var goodsImageView: UIImageView!
    @IBOutlet var goodsCountLabel: UILabel!
    @IBOutlet var goodsTitleLabel: UILabel!
    @IBOutlet var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 12.0
    }
    
    func update(with imageField: ImageField) {
        goodsImageView.update(with: imageField.imageInfo)
        goodsTitleLabel.text = imageField.title
        goodsCountLabel.text = imageField.detail
    }
}
