//
//  ProductDetailViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 20/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class ProductDetailViewController: KIMBaseViewController, BaseTableViewable {
    @IBOutlet var tableView: FioriBaseTableView!
    @IBOutlet var addButton: UIBarButtonItem!
    
    var request: [String: Any]!
    var dataProvider: TableDataProviderable!
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewAppearance()
        refresh()
    }
    
    // MARK: - Setup View Appearance
    func setupViewAppearance() {
        setupTableView()
        localise()
    }
    
    func localise() {
        addButton.title = R.string.generic.add()
    }
    
    // MARK: - Setup Table
    private func setupTableView() {
        setupDefaultTableData()
        setupTableView(with: dataProvider, delegate: dataProvider)
    }
    
    // MARK: - Setup Table: BaseTableViewable
    func setupDefaultTableData() {
        let productDetailViewModel = ProductDetailViewModel(with: [:])
        dataProvider = ProductDetailDataProvider(with: tableView, viewModel: productDetailViewModel, delegate: self)
    }
    
    // MARK: - Reload Data
    func refresh() {
        request = ProductDetailViewController.fetchRequest()
        setupNavigationBar(title: request["name"] as? String ?? "", hidesBackButton: false)
        let updatedRequestGoodsViewModel = ProductDetailViewModel(with: request)
        self.dataProvider.update(with: updatedRequestGoodsViewModel)
    }
    
    // MARK: - IBActions
    @IBAction func didTouchUpInsideCancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTouchUpInsideSubmitButton(_ sender: Any) {}
}

// MARK: - DataProviderDelegate
extension ProductDetailViewController: DataProviderDelegate {
    func reloadData() {
        refresh()
    }
    
    func didSelect(_ field: Field) {}
}

// MARK: - ButtonHeaderViewDelegate
extension ProductDetailViewController: ButtonHeaderViewDelegate {
    func didTouchUpInsideHeaderViewButton() {}
}

// MARK: - Navigation
extension ProductDetailViewController {
    func goToAddGoods() {}
}

// MARK: - TEMP DATA
extension ProductDetailViewController {
    static func fetchRequest() -> [String: Any] {
        let result: [String : Any] = [
            "name" : "Adi Predator",
            "quantityRequired" : 10,
            "size" : "899300",
            "colour" : "Black",
            "product": ProductDetailViewController.fetchProduct()
        ]
        return result
    }
    
    static func fetchProduct() -> [String: Any] {
        let product: [String : Any] = [
            "name" : "Adi Predator",
            "size" : "UK6-10",
            "colour" : "Black",
            "sole" : "Rubber",
            "upper" : "Polyester",
            "sku" : "899300",
            "images" : [
                [
                    "imageURL" : "SAPLogo",
                    "description" : "Box",
                ],
                [
                    "imageURL" : "SAPLogo",
                    "description" : "Front",
                ],
                [
                    "imageURL" : "SAPLogo",
                    "description" : "Back",
                ]
            ],
        ]
        return product
    }
}
