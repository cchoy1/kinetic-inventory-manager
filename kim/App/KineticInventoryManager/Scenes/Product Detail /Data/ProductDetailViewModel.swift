//
//  ProductDetailViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 20/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

final class ProductDetailViewModel: BaseViewModel {
    init(with request: [String: Any]) {
        super.init(with: [SectionItem]())
        let product = request["product"] as? [String : Any] ?? [:]
        let images = product["images"] as? [[String : Any]] ?? []
        sections.append(ProductDetailViewModel.requestDetailsSection(with: product, title: R.string.goods.requestDetails()))
        sections.append(ProductDetailViewModel.productDetailSection(with: product, title: R.string.goods.productDescription()))
        sections.append(ProductDetailViewModel.imagesSection(with: images, title: R.string.goods.images()))
    }
}

// MARK: Sections
extension ProductDetailViewModel {
    static func requestDetailsSection(with request: [String: Any], title: String) -> SectionItem {
        var fields = [Field]()
        fields.append(
            ItemTextField(with: R.string.goods.quantity(), text: request["quantity"] as? String ?? "", placeholder: R.string.goods.quantityRequired(), editable: true)
        )
        fields.append(Field(title: R.string.goods.size(), detail: request["size"] as? String ?? "", accessoryType: .disclosureIndicator))
        fields.append(Field(title: R.string.goods.colour(), detail: request["colour"] as? String ?? "", accessoryType: .disclosureIndicator))
        return SectionItem(title: title, fields: fields)
    }
    
    static func productDetailSection(with product: [String: Any], title: String) -> SectionItem {
        var fields = [Field]()
        fields.append(Field(title: R.string.goods.size(), detail: product["size"] as? String ?? ""))
        fields.append(Field(title: R.string.goods.colour(), detail: product["colour"] as? String ?? ""))
        fields.append(Field(title: R.string.goods.sole(), detail: product["sole"] as? String ?? ""))
        fields.append(Field(title: R.string.goods.upper(), detail: product["upper"] as? String ?? ""))
        fields.append(Field(title: R.string.goods.sku(), detail: product["sku"] as? String ?? ""))
        return SectionItem(title: title, fields: fields)
    }
    
    static func imagesSection(with products: [[String: Any]], title: String) -> SectionItem {
        var fields = [Field]()
        for productImage in products {
            fields.append(ProductImageField(with: productImage))
        }
        return SectionItem(title: title, fields: fields)
    }
}
