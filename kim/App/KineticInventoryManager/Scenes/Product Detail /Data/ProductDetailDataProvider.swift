//
//  ProductDetailDataProvider.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 20/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

final class ProductDetailDataProvider: FioriBaseTableDataProvider {}

// MARK: - Override UITableViewDataSource
extension ProductDetailDataProvider {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let field = viewModel.item(for: indexPath) as? Field,
            let cell = dequeCell(with: field, for: tableView) else {
            return UITableViewCell()
        }
        return cell
    }
    
    func dequeCell(with field: Field, for tableView: UITableView) -> UITableViewCell? {
        if let itemTextField = field as? ItemTextField,
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.className) as? TextFieldTableViewCell {
            cell.update(with: itemTextField)
        } else if let productImageField = field as? ProductImageField,
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleImageTableViewCell.className) as? TitleImageTableViewCell {
            cell.update(with: productImageField)
            return cell
        } else if let cell = tableView.dequeueReusableCell(withIdentifier: TitleDetailTableViewCell.className) as? TitleDetailTableViewCell {
            cell.update(with: field)
            return cell
        }
        return nil
    }
}
