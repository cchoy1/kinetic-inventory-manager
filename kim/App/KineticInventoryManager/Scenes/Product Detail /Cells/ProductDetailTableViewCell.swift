//
//  ProductDetailTableViewCell.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 01/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

class ProductDetailTableViewCell: UITableViewCell {
    @IBOutlet var objectImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var substatusLabel: UILabel!
    
    func update(with productDetailField: ProductDetailField) {
        objectImageView.update(with: productDetailField.imageInfo)
        titleLabel.text = productDetailField.title
        detailLabel.text = productDetailField.sku
        statusLabel.text = productDetailField.detail
        substatusLabel.text = productDetailField.colour
        accessoryType = productDetailField.accessoryType
    }
}
