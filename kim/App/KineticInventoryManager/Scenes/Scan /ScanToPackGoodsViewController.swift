//
//  ScanToPackGoodsViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 29/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

class ScanGoodsOutViewController: ScanGoodsViewController {
    override func processScannedBarCode(_ code: String) {
        if didHandleAlreadyScannedOrder(with: code) {
            return
        } else if didHandleScannedOrder(with: code) {
            return
        } else if didHandleNewScannedOrder(with: code) {
            return
        } else {
            presentOkAlert(message: R.string.generic.errorScannedItemNotFound())
        }
    }
}
    
extension ScanGoodsOutViewController {
    // If we have already scanned a good and it's the Code scanned == the ID of the new object we use that -> Tell user that they have already scanned
    private func didHandleAlreadyScannedOrder(with barCode: String) -> Bool {
        guard let scannedOrder = scannedGoodsViewModel?.order as? OUTBDLVHead, scannedOrder.outboundDeliveryUUID?.toString() ?? "" == barCode else {
            return false
        }
        presentOkAlert(message: R.string.generic.errorAlreadyScanned())
        return true
    }
    
    // If we have not scanned a good can fetch one given the ID and it's not already in transit - populate popup with this order
    private func didHandleScannedOrder(with barCode: String) -> Bool {
        guard scannedGoodsViewModel?.order == nil, let scannedOrder = scanGoodsViewModel.fetchOrder(with: barCode, isGoodsIn: false) else {
            return false
        }
        guard let outboundGood = scannedOrder as? OUTBDLVHead, outboundGood.completionStatusName == "Not Started" else {
            presentOkAlert(message: "Order has already in transit")
            return false
        }
        handleOrderToPack(with: scannedOrder)
        return true
    }
    
    // An order has already been scanned and the user is scanning a new one, handle it
    private func didHandleNewScannedOrder(with barCode: String) -> Bool {
        guard scannedGoodsViewModel?.order != nil, let scannedOrder = scanGoodsViewModel.fetchOrder(with: barCode, isGoodsIn: false) else {
            return false
        }
        handleOrderToPack(with: scannedOrder)
        return true
    }
    
    private func handleOrderToPack(with scannedOrder: Any) {
        if scannedGoodsViewModel == nil {
            scannedGoodsViewModel = ScannedGoodsViewModel(order: scannedOrder)
        } else {
            scannedGoodsViewModel?.update(with: scannedOrder)
        }
        doneButton.isEnabled = true
        showScannedGoodsDrawer(with: scannedOrder)
    }
}
