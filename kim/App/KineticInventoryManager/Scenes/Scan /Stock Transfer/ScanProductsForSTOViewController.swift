//
//  ScanProductsForSTOViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 01/05/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

protocol ScanProductsForSTOViewControllerDelegate: class {
    func didReturnSelectedProduct(with productItem: ProductItem)
}

class ScanProductsForSTOViewController: ScanGoodsViewController {
    private var productItems: [ProductItem]!
    private weak var scanProductsForSTOViewDelegate: ScanProductsForSTOViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = nil
        updateScanning(enabled: false)
        refresh()
    }
    
    override func processScannedBarCode(_ code: String) {
        if didHandleScannedProduct(with: code) {
            return
        } else {
            presentOkAlert(message: R.string.generic.errorScannedItemNotFound())
        }
    }
    
    func update(with delegate: ScanProductsForSTOViewControllerDelegate) {
        self.scanProductsForSTOViewDelegate = delegate
    }
}

// MARK: - Refresh
extension ScanProductsForSTOViewController {
    func refresh() {
        self.showFioriLoadingIndicator()
        PurchaseOrderItemsService.fetchPurchaseOrderProductItems(with: onlineODataProvider, warehouseId: DefaultWarehouseHelper.fetchDefaultWarehouseId()) { (error, results) in
            DispatchQueue.main.async {
                if let _ = error {
                    self.presentOkAlert(message: R.string.generic.errorGenericMessage())
                } else if let products = results {
                    self.productItems = products
                }
                self.updateScanning(enabled: true)
                self.hideFioriLoadingIndicator()
                self.loadScannedProductsForSTOViewController()
            }
        }
    }
}

// MARK: - Handle Scanned Bar Code
extension ScanProductsForSTOViewController {
    private func didHandleScannedProduct(with barCode: String) -> Bool {
        guard barCode.isEmpty == false, let productItem = productItems.first(where: { $0.product.product ?? "" == barCode }) else {
            return false
        }
        scanProductsForSTOViewDelegate?.didReturnSelectedProduct(with: productItem)
        navigationController?.dismiss(animated: true, completion: nil)
        return true
    }
}

// MARK: - Navigation
extension ScanProductsForSTOViewController {
    private func loadScannedProductsForSTOViewController() {
        guard let scannedProductsForSTOViewController = R.storyboard.scannedProductsForSTO.scannedProductsForSTOViewController() else {
            return
        }
        let drawerId = ScannedProductsForSTOViewController.className
        let snapPositions: [DrawerPosition] = [.collapsed, .partiallyOpen]
        let drawerView = self.drawerView(with: scannedProductsForSTOViewController,
                                         snapPositions: snapPositions, blurEffect: .regular, id: drawerId)

        scannedProductsForSTOViewController.drawerView = drawerView
        scannedProductsForSTOViewController.update(with: self)
        scannedProductsForSTOViewController.drawerView.setPosition(.collapsed, animated: true)
        load(drawerView: drawerView)
    }
    
    private func loadSelectProducts() {
        guard let selectProductsViewController = R.storyboard.selectProducts.selectProductsViewController() else {
            return
        }
        selectProductsViewController.update(with: productItems)
        selectProductsViewController.delegate = self
        selectProductsViewController.onlineODataProvider = onlineODataProvider
    
        let navigationController = UINavigationController(rootViewController: selectProductsViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
}

// MARK: - ScannedProductsForSTOViewControllerDelegate
extension ScanProductsForSTOViewController: ScannedProductsForSTOViewControllerDelegate {
    func openSelectProductTable() {
        loadSelectProducts()
    }
}

// MARK: - SelectProductsViewControllerDelegate
extension ScanProductsForSTOViewController: SelectProductsViewControllerDelegate {
    func didReturnSelectedProduct(with productItem: ProductItem) {
        scanProductsForSTOViewDelegate?.didReturnSelectedProduct(with: productItem)
        navigationController?.dismiss(animated: true, completion: nil)
    }
}
