//
//  ScannedProductsForSTOViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 01/05/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

protocol ScannedProductsForSTOViewControllerDelegate: class {
    func openSelectProductTable()
}

class ScannedProductsForSTOViewController: KIMBaseViewController {
    private weak var delegate: ScannedProductsForSTOViewControllerDelegate?
  
    @IBOutlet var scanGoodsHeader: ScanGoodsHeader!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scanGoodsHeader.update(with: self)
    }
    
    func update(with delegate: ScannedProductsForSTOViewControllerDelegate) {
        self.delegate = delegate
    }
}

extension ScannedProductsForSTOViewController: ScanGoodsHeaderDelegate {
    func didTouchUpInsideKeyboardProductViewButton() {}
    
    func didTouchUpInsideSearchProductViewButton() {
        delegate?.openSelectProductTable()
    }
}
