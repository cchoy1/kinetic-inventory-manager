//
//  ScanToUnoadGoodsViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 29/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

class ScanGoodsInViewController: ScanGoodsViewController {
    override func processScannedBarCode(_ code: String) {
        if didHandleInboundOrder(with: code) {
            return
        } else {
            presentOkAlert(message: R.string.generic.errorScannedItemNotFound())
        }
    }
}

extension ScanGoodsInViewController {
    /// Unpack Goods Flow - Fetches and handles Inbound Order object
    ///
    /// - Parameter barCode: Scanned bar code
    /// - Returns: bool determining whether an order was fetched and handled in this method
    private func didHandleInboundOrder(with barCode: String) -> Bool {
        guard let inboundOrder = goodsInOutViewModel.fetchOrder(with: barCode, isGoodsIn: true) as? DLVHead else {
            return false
        }
        unload(inboundOrder)
        return true
    }
    
    private func unload(_ inboundOrder: DLVHead) {
        inboundOrder.unloadAllItems()
        update(with: inboundOrder)
        doneButton.isEnabled = true
    }
    
    private func update(with scannedOrder: Any) {
        if goodsInOutItemsViewModel == nil {
            goodsInOutItemsViewModel = GoodsInOutItemsViewModel(order: scannedOrder)
        } else {
            goodsInOutItemsViewModel?.update(with: scannedOrder)
        }
        showScannedGoodsDrawer(with: scannedOrder, isPackGoods: false)
    }
}
