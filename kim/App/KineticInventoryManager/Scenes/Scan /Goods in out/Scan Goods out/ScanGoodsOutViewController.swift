//
//  ScanToPackGoodsViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 29/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

class ScanGoodsOutViewController: ScanGoodsViewController {
    private var isPackGoods = false
    
    override func processScannedBarCode(_ code: String) {
        if didHandleAlreadyScannedOrder(with: code) {
            return
        } else if didHandleScannedOrder(with: code) {
            return
        } else if didHandleScannedOrderItem(with: code) {
            return
        } else {
            presentOkAlert(message: R.string.generic.errorScannedItemNotFound())
        }
    }
}

extension ScanGoodsOutViewController {
    /// Pack Goods Flow + Load Goods Flow - Fetches and handles Outbound Order object
    /// For: OUTBDLVHead - If we have already scanned an order and it's ID == the BarCode -> Tell user that they have already scanned
    ///
    /// - Parameter barCode: Scanned bar code
    /// - Returns: bool determining whether an order was fetched and handled in this method
    private func didHandleAlreadyScannedOrder(with barCode: String) -> Bool {
        guard let outboundOrder = goodsInOutItemsViewModel?.order as? OUTBDLVHead, outboundOrder.outboundDelivery ?? "" == barCode else {
            return false
        }
        presentOkAlert(message: R.string.generic.errorAlreadyScanned())
        return true
    }
    
    /// Pack Goods Flow + Load Goods Flow - Fetches and handles Outbound Order object
    /// For: OUTBDLVHead - If we have not already scanned a order, we can fetch it using the BarCode, if the fetched order is not already in transit - populate popup with this order
    ///
    /// - Parameter barCode: Scanned bar code
    /// - Returns: bool determining whether an order was fetched and handled in this method
    private func didHandleScannedOrder(with barCode: String) -> Bool {
        guard goodsInOutItemsViewModel?.order == nil, let outboundOrder = goodsInOutViewModel.fetchOrder(with: barCode, isGoodsIn: false) as? OUTBDLVHead else {
            return false
        }
        guard outboundOrder.completionStatusName == "Not Started" else {
            presentOkAlert(message: "Order has already in transit")
            return false
        }
        if outboundOrder.allItemsPacked() == false {
            isPackGoods = true
        }
        handle(outboundOrder: outboundOrder)
        return true
    }
    
    /// Pack Goods Flow + Load Goods Flow - Fetches and handles Outbound Order object
    /// For: OUTBDLVItem - If an order item has already been scanned
    ///
    /// - Parameter barCode: Scanned bar code
    /// - Returns: bool determining whether an order was fetched and handled in this method
    private func didHandleScannedOrderItem(with barCode: String) -> Bool {
        guard goodsInOutItemsViewModel?.order != nil, let orderItem = goodsInOutItemsViewModel?.fetchOrderItem(with: barCode) else {
            return false
        }
        handleScannedOrderItem(orderItem, isPackGoods: isPackGoods)
        return true
    }
    
    private func handle(outboundOrder: OUTBDLVHead) {
        if outboundOrder.allItemsPacked() {
            doneButton.isEnabled = true
        }
        update(with: outboundOrder, isPackGoods: isPackGoods)
    }
    
    private func handleScannedOrderItem(_ orderItem: Any, isPackGoods: Bool) {
        if let outboundGood = orderItem as? OUTBDLVItem {
            goodsInOutItemsViewModel?.updateOutboundOrder(with: outboundGood)
            guard let order = goodsInOutItemsViewModel?.order as? OUTBDLVHead else {
                return
            }
            if order.allItemsPacked() {
                doneButton.isEnabled = true
            }
            showScannedGoodsDrawer(with: order, isPackGoods: isPackGoods)
        } else {
            presentOkAlert(message: R.string.generic.errorScannedItemNotFound())
        }
    }
}
