//
//  GoodsInScanToUnloadViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 30/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

final class GoodsInScanToUnloadViewModel: BaseViewModel {
    init(with order: DLVHead) {
        super.init(with: [SectionItem]())
        sections.append(GoodsInScanToUnloadViewModel.boxesSection(with: order))
        if order.items.count > 0 {
            sections.append(GoodsInScanToUnloadViewModel.deliveredGoodsSection(with: order.items))
        }
    }
}

// MARK: Sections
extension GoodsInScanToUnloadViewModel {
    static func boxesSection(with order: DLVHead) -> SectionItem {
        let title = R.string.goods.deliveryFrom(order.shipFromParty ?? "")
        let field = ScannedItemField(forItemWithTitle: title, scannedStatus: .scanned)
        return SectionItem(title: R.string.goods.goodsOut(), fields: [field])
    }
    
    static func deliveredGoodsSection(with inboundDeliveryItems: [DLVItem]) -> SectionItem {
        var fields = [Field]()
        for inboundDeliveryItem in inboundDeliveryItems {
            fields.append(
                ScannedItemField(for: inboundDeliveryItem)
            )
        }
        return SectionItem(title: R.string.goods.itemsInBox("\(inboundDeliveryItems.count)"), fields: fields)
    }
}
