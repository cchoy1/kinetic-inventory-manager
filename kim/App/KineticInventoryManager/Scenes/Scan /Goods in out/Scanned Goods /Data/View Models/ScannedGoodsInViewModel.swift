//
//  ScanndedGoodsViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 26/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import Foundation

final class ScannedGoodsInViewModel: BaseViewModel {
    init(with order: Any) {
        super.init(with: [SectionItem]())
        sections.append(ScannedGoodsInViewModel.boxesSection(with: order))
        if let goodsSection = ScannedGoodsInViewModel.goodsSection(with: order) {
            sections.append(goodsSection)
        }
    }
}

// MARK: Sections
extension ScannedGoodsInViewModel {
    static func boxesSection(with order: Any) -> SectionItem {
        var title = ""
        if let deliveredOrder = order as? DLVHead {
            title = R.string.goods.deliveryFrom(deliveredOrder.shipFromParty ?? "")
        } else if let outboundOrder = order as? OUTBDLVHead {
            title = R.string.goods.deliveryTo(outboundOrder.shipToPartyName ?? "")
        }
        let field = ScannedItemField(forItemWithTitle: title, scannedStatus: .scanned)
        return SectionItem(title: R.string.goods.goodsOut(), fields: [field])
    }
 
    static func goodsSection(with order: Any) -> SectionItem? {
         if let deliveredOrder = order as? DLVHead {
            return ScannedGoodsInViewModel.deliveredGoodsSection(with: deliveredOrder.items)
         } else if let outboundOrder = order as? OUTBDLVHead {
            if outboundOrder.packingStatusName == "Completed" {
                return ScannedGoodsInViewModel.outboundGoodsPackedSection(with: outboundOrder.items)
            } else {
                return ScannedGoodsInViewModel.outboundGoodsToBePackedSection(with: outboundOrder.items)
            }
        }
        return nil
     }
    
    static func outboundGoodsToBePackedSection(with products: [OUTBDLVItem]) -> SectionItem {
        var fields = [Field]()
        for product in products {
            var scannedStatus = ScannedStatus.pending
            if product.packingStatusName == "Completed" {
                scannedStatus = .scanned
            }
            fields.append(
                ScannedItemField(forItemWithTitle: product.productName ?? "",
                                 detail: product.itemDeliveryQuantity?.toString() ?? "0",
                                 scannedStatus: scannedStatus)
            )
        }
        return SectionItem(title: R.string.goods.itemsInBox("\(products.count)"), fields: fields)
    }
    
    static func outboundGoodsPackedSection(with products: [OUTBDLVItem]) -> SectionItem {
        var fields = [Field]()
        for product in products {
            fields.append(
                ScannedItemField(forItemWithTitle: product.productName ?? "",
                                 detail: product.itemDeliveryQuantity?.toString() ?? "0",
                                 scannedStatus: .none)
            )
        }
        return SectionItem(title: R.string.goods.itemsInBox("\(products.count)"), fields: fields)
    }
    
    static func deliveredGoodsSection(with products: [DLVItem]) -> SectionItem {
        var fields = [Field]()
        for product in products {
            fields.append(
                ScannedItemField(forItemWithTitle: product.productName ?? "",
                                 detail: product.itemDeliveryQuantity?.toString() ?? "0",
                                 scannedStatus: .none)
            )
        }
        return SectionItem(title: R.string.goods.itemsInBox("\(products.count)"), fields: fields)
    }
}
