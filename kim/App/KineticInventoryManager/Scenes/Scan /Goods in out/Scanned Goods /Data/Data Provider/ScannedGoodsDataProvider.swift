//
//  ScanndedGoodsDataProvider.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 26/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

final class ScannedGoodsDataProvider: FioriBaseTableDataProvider {}

// MARK: - Override UITableViewDataSource
extension ScannedGoodsDataProvider {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let field = viewModel.item(for: indexPath) as? Field,
            let cell = dequeCell(with: field, for: tableView) else {
            return UITableViewCell()
        }
        return cell
    }
    
    func dequeCell(with field: Field, for tableView: UITableView) -> UITableViewCell? {
        if let scannedItemField = field as? ScannedItemField {
            if scannedItemField.hasStepper, let cell = tableView.dequeueReusableCell(withIdentifier: PackGoodTableViewCell.className) as? PackGoodTableViewCell {
                cell.update(with: scannedItemField)
                return cell
            } else if let cell = tableView.dequeueReusableCell(withIdentifier: ScannedGoodTableViewCell.className) as? ScannedGoodTableViewCell {
                cell.update(with: scannedItemField)
                return cell
            }
        }
        return nil
    }
}

