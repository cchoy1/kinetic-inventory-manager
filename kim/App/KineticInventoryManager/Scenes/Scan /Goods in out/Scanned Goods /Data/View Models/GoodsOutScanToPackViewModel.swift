//
//  GoodsOutScanToPackViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 30/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

final class GoodsOutScanToPackViewModel: BaseViewModel {
    init(with order: OUTBDLVHead) {
        super.init(with: [SectionItem]())
        sections.append(GoodsOutScanToPackViewModel.boxesSection(with: order))
        sections.append(GoodsOutScanToPackViewModel.outboundGoodsToBePackedSection(with: order.items))
    }
}

// MARK: Sections
extension GoodsOutScanToPackViewModel {
    static func boxesSection(with order: OUTBDLVHead) -> SectionItem {
        let title = R.string.goods.deliveryTo(order.shipToPartyName ?? "")
        let field = ScannedItemField(forItemWithTitle: title, scannedStatus: .scanned)
        return SectionItem(title: R.string.goods.goodsOut(), fields: [field])
    }
 
    static func outboundGoodsToBePackedSection(with outboundDeliveryItems: [OUTBDLVItem]) -> SectionItem {
        var fields = [Field]()
        for outboundDeliveryItem in outboundDeliveryItems {
            fields.append(
                ScannedItemField(for: outboundDeliveryItem, hasStepper: true)
            )
        }
        return SectionItem(title: R.string.goods.itemsInBox("\(outboundDeliveryItems.count)"), fields: fields)
    }
}
