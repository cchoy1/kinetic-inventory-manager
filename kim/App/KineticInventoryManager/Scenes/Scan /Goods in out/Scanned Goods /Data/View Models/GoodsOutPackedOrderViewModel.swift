//
//  GoodsOutPackedOrderViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 30/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

final class GoodsOutPackedOrderViewModel: BaseViewModel {
    init(with order: OUTBDLVHead) {
        super.init(with: [SectionItem]())
        sections.append(GoodsOutPackedOrderViewModel.boxesSection(with: order))
        if order.items.count > 0 {
            sections.append(GoodsOutPackedOrderViewModel.outboundGoodsPackedSection(with: order.items))
        }
    }
}

// MARK: Sections
extension GoodsOutPackedOrderViewModel {
    static func boxesSection(with order: OUTBDLVHead) -> SectionItem {
        let title = R.string.goods.deliveryTo(order.shipToPartyName ?? "")
        let field = ScannedItemField(forItemWithTitle: title, scannedStatus: .scanned)
        return SectionItem(title: R.string.goods.goodsOut(), fields: [field])
    }
    
    static func outboundGoodsPackedSection(with products: [OUTBDLVItem]) -> SectionItem {
        var fields = [Field]()
        for product in products {
            fields.append(
                ScannedItemField(forItemWithTitle: product.productName ?? "",
                                 detail: product.itemDeliveryQuantity?.toString() ?? "0",
                                 scannedStatus: .none)
            )
        }
        return SectionItem(title: R.string.goods.itemsInBox("\(products.count)"), fields: fields)
    }
}
