//
//  ScannedGoodTableViewCell.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 25/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class ScannedGoodTableViewCell: UITableViewCell {
    @IBOutlet var statusImageView: UIImageView!
    @IBOutlet var productNameLabel: UILabel!
    @IBOutlet var itemCountLabel: UILabel!
    
    func update(with scannedItemField: ScannedItemField) {
        if let imageInfo = scannedItemField.imageInfo {
            statusImageView.update(with: imageInfo)
        } else {
            statusImageView.isHidden = true
        }
        productNameLabel.text = scannedItemField.title
        itemCountLabel.text = scannedItemField.detail
    }
}
