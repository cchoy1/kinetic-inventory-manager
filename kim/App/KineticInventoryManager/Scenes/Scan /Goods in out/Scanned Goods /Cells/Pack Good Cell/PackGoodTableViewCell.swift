//
//  PackGoodTableViewCell.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 12/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

class PackGoodTableViewCell: UITableViewCell {
    @IBOutlet var statusImageView: UIImageView!
    @IBOutlet var productNameLabel: UILabel!
    @IBOutlet var itemCountLabel: UILabel!
    @IBOutlet var stepper: UIStepper!
    
    func update(with scannedItemField: ScannedItemField) {
        if let imageInfo = scannedItemField.imageInfo {
            statusImageView.update(with: imageInfo)
        } else {
            statusImageView.isHidden = true
        }
        productNameLabel.text = scannedItemField.title
        itemCountLabel.text = "\(scannedItemField.scannedCount)" + "/" + "\(scannedItemField.quantity)"
        stepper.maximumValue = Double(scannedItemField.quantity)
        stepper.value = Double(scannedItemField.scannedCount)
    }
}
