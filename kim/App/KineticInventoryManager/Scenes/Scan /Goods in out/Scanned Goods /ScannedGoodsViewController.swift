//
//  ScannedGoodsViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 25/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

class ScannedGoodsViewController: KIMBaseViewController, BaseTableViewable {
    @IBOutlet var tableView: FioriBaseTableView!
    var dataProvider: TableDataProviderable!
    
    private var order: Any!
    private var product: Any?
    private var goodOut: AOutbDeliveryHeaderType?
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewAppearance()
        refresh()
    }
    
    // MARK: - Setup View Appearance
    private func setupViewAppearance() {
        setupTableView()
        updateViewAppearanceForDrawer()
    }
}

// MARK: - Setup Table
extension ScannedGoodsViewController {
    private func setupTableView() {
        registerCells()
        setupDefaultTableData()
        setupTableView(with: dataProvider, delegate: dataProvider)
    }
    
    private func registerCells() {
        let goodsCellTableViewCellNib = UINib(nibName: ScannedGoodTableViewCell.className, bundle: .main)
        tableView.register(goodsCellTableViewCellNib, forCellReuseIdentifier: ScannedGoodTableViewCell.className)
        
        let packGoodTableViewCellNib = UINib(nibName: PackGoodTableViewCell.className, bundle: .main)
        tableView.register(packGoodTableViewCellNib, forCellReuseIdentifier: PackGoodTableViewCell.className)
    }
}

// MARK: - Update View Controller
extension ScannedGoodsViewController {
    func update(with order: Any, isPackGoods: Bool) {
        self.order = order
        if let inboundOrder = order as? DLVHead {
            let goodsInScanToUnloadViewModel = GoodsInScanToUnloadViewModel(with: inboundOrder)
            self.dataProvider.update(with: goodsInScanToUnloadViewModel)
        } else if let outboundOrder = order as? OUTBDLVHead {
            if outboundOrder.allItemsPacked(), isPackGoods == false {
                let goodsOutScanToPackViewModel = GoodsOutPackedOrderViewModel(with: outboundOrder)
                self.dataProvider.update(with: goodsOutScanToPackViewModel)
            } else {
                let goodsOutScanToPackViewModel = GoodsOutScanToPackViewModel(with: outboundOrder)
                self.dataProvider.update(with: goodsOutScanToPackViewModel)
            }
        }
    }
}

// MARK: - Setup Data
extension ScannedGoodsViewController {
    func setupDefaultTableData() {
        let goodsInViewModel = ScannedGoodsInViewModel(with: [:])
        dataProvider = ScannedGoodsDataProvider(with: tableView, viewModel: goodsInViewModel, delegate: self)
    }
    
    func refresh() {}
}

// MARK: - DataProviderDelegate
extension ScannedGoodsViewController: DataProviderDelegate {
    func reloadData() {
        refresh()
    }
    
    func didSelect(_ field: Field) {}
}
