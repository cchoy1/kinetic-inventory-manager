//
//  ScannedGoodsViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 16/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

struct ScannedOrderItemsViewModel {
    var order: Any?
    
    mutating func update(with order: Any) {
        self.order = order
    }
    
    func fetchOrderItem(with id: String) -> Any? {
        if let inboundOrder = order as? DLVHead {
            return inboundOrder.items.first(where: { $0.inboundDeliveryUUID?.toString() ?? "" == id })
        } else if let outboundOrder = order as? OUTBDLVHead {
            return outboundOrder.items.first(where: { $0.outboundDeliveryOrderItemUUID?.toString() ?? "" == id })
        } else {
            return nil
        }
    }
    
    func updateInboundOrder(with inboundItem: OUTBDLVItem) {
        if let inboundOrder = order as? DLVHead {
            
        }
    }
    
    mutating func updateOutboundOrder(with outboundItem: OUTBDLVItem) {
        guard let outboundOrder = order as? OUTBDLVHead else {
            return
        }
        if outboundOrder.setPackedStatusComplete(for: outboundItem) {
            update(with: outboundOrder)
        }
    }
}
