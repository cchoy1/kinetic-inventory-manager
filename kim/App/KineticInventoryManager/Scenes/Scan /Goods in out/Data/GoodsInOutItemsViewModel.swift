//
//  ScannedGoodsViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 16/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

struct GoodsInOutItemsViewModel {
    var order: Any?
    
    mutating func update(with order: Any) {
        self.order = order
    }
    
    func fetchOrderItem(with id: String) -> Any? {
        if let inboundOrder = order as? DLVHead {
            return inboundOrder.items.first(where: { $0.inboundDelivery ?? "" == id })
        } else if let outboundOrder = order as? OUTBDLVHead {
            return outboundOrder.items.first(where: { $0.product ?? "" == id })
        } else {
            return nil
        }
    }
    
    mutating func updateInboundOrder(with inboundItem: DLVItem) {
        guard let inboundOrder = order as? DLVHead else {
            return
        }
        if inboundOrder.setUnloadedStatusComplete(for: inboundItem) {
            update(with: inboundOrder)
        }
    }
    
    mutating func updateOutboundOrder(with outboundItem: OUTBDLVItem) {
        guard let outboundOrder = order as? OUTBDLVHead else {
            return
        }
        if outboundOrder.setPackedStatusComplete(for: outboundItem) {
            update(with: outboundOrder)
        }
    }
}
