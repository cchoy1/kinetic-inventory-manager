//
//  ScanGoodsViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 03/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import Foundation

struct GoodsInOutViewModel {
    var homeFeedData: HomeFeedData
    
    func fetchOrder(with id: String, isGoodsIn: Bool) -> Any? {
        if isGoodsIn {
            guard let goodIn = homeFeedData.goodsIn.first(where: { $0.inboundDelivery ?? "" == id }) else {
                return nil
            }
            return goodIn
        } else if let goodOut = homeFeedData.goodsOut.first(where: { $0.outboundDelivery ?? "" == id }) {
            return goodOut
        } else {
            return nil
        }
    }
}
