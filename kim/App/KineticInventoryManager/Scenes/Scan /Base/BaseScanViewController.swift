//
//  ScanViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 25/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit
import ScanditBarcodeCapture

class BaseScanViewController: KIMBaseViewController {
    private var context: DataCaptureContext = .licensed
    private var camera: Camera? = Camera.default
    private var captureView: DataCaptureView!
    private var overlay: BarcodeCaptureOverlay!
    private lazy var barcodeCapture: BarcodeCapture = {
        let settings = configureBarcodeCaptureSettings()
        return BarcodeCapture(context: context, settings: settings)
    }()
    private weak var delegate: ScanGoodsViewControllerDelegate?
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        startScanning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopScanning()
    }
    
    func processScannedBarCode(_ code: String) {}
    
    func setupView() {
        setupRecognition()
        setupNavigationBar(title: R.string.generic.scan(), largeTitle: false, isHidden: false)
    }
}

// MARK: - Public Methods
extension BaseScanViewController {
    func update(with delegate: ScanGoodsViewControllerDelegate) {
        self.delegate = delegate
    }
    
    func startScanning() {
        updateScanning(enabled: true)
    }
    
    func stopScanning() {
        updateScanning(enabled: false)
        context.removeAllModes()
    }
    
    func updateScanning(enabled: Bool) {
        if enabled {
            barcodeCapture.isEnabled = true
            camera?.switch(toDesiredState: .on)
        } else {
            barcodeCapture.isEnabled = false
            camera?.switch(toDesiredState: .off)
        }
    }
}

// MARK: - Setup Camera & Recognition
extension BaseScanViewController {
    private func setupRecognition() {
        context.setFrameSource(camera, completionHandler: nil)
        // Use the recommended camera settings for the BarcodeCapture mode.
        let recommenededCameraSettings = BarcodeCapture.recommendedCameraSettings
        camera?.apply(recommenededCameraSettings)
        barcodeCapture.addListener(self)
        
        // Setup a data capture view that renders the camera preview.
        // View must be connected to the data capture context.
        captureView = DataCaptureView(context: context, frame: view.bounds)
        captureView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(captureView)
        
        // Add a barcode capture overlay to the data capture view to render the location of captured barcodes on top of
        // the video preview. This is optional, but recommended for better visual feedback.
        overlay = BarcodeCaptureOverlay(barcodeCapture: barcodeCapture)
        overlay.viewfinder = RectangularViewfinder()
        
        captureView.addOverlay(overlay)
    }
    
    private func configureBarcodeCaptureSettings() -> BarcodeCaptureSettings {
        let settings = BarcodeCaptureSettings()
        settings.set(symbology: .code128, enabled: true)
            
        let symbologySettings = settings.settings(for: .code128)
        symbologySettings.activeSymbolCounts = Set(7...100) as Set<NSNumber>
                
        // Restrict scanned area to the visible box to improve performance optimised for iPhone XR / iPhone 11 sized screens
        let size = SizeWithUnit(width: FloatWithUnit(value: 0.8, unit: .fraction),
                                height: FloatWithUnit(value: 0.33, unit: .fraction))
        settings.locationSelection = RectangularLocationSelection(size: size)
        return settings
    }
}

// MARK: - BarcodeCaptureListener
extension BaseScanViewController: BarcodeCaptureListener {
    func barcodeCapture(_ barcodeCapture: BarcodeCapture,
                        didScanIn session: BarcodeCaptureSession,
                        frameData: FrameData) {
        guard let barcode = session.newlyRecognizedBarcodes.first else {
            return
        }
        // Stop recognizing barcodes for as long as we are displaying the result. There won't be any new results until
        // the capture mode is enabled again. Note that disabling the capture mode does not stop the camera, the camera
        // continues to stream frames until it is turned off.
        barcodeCapture.isEnabled = false
        
        // If you are not disabling barcode capture here and want to continue scanning, consider setting the
        // codeDuplicateFilter when creating the barcode capture settings to around 500 or even -1 if you do not want
        // codes to be scanned more than once.
        if let barcodeString = barcode.data {
            DispatchQueue.main.async {
                self.processScannedBarCode(barcodeString)
                let _ = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { timer in
                    self.barcodeCapture.isEnabled = true
                }
            }
        }
    }
}
