//
//  ScanGoodsViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 27/03/2020.
//  Copyright © 2020 Deloitte. All rights reserved.
//

import UIKit

protocol ScanGoodsViewControllerDelegate: class {
    func didReturn(updatedOrder: Any)
}

class ScanGoodsViewController: BaseScanViewController {
    @IBOutlet var doneButton: UIBarButtonItem!
    
    private var isGoodsIn: Bool!
    var goodsInOutViewModel: GoodsInOutViewModel!
    var goodsInOutItemsViewModel: GoodsInOutItemsViewModel?
   
    var scannedGoodsViewController: ScannedGoodsViewController?
    weak var delegate: ScanGoodsViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupView() {
        super.setupView()
        setupVariables()
        setupNavigationItems()
    }
}

// MARK: - Setup View
extension ScanGoodsViewController {
    private func setupVariables() {
        goodsInOutItemsViewModel = GoodsInOutItemsViewModel()
    }
    
    private func setupNavigationItems() {
        doneButton.isEnabled = false
    }
}

// MARK: - Public Methods
extension ScanGoodsViewController {
    func update(with homeFeedData: HomeFeedData, isGoodsIn: Bool, delegate: ScanGoodsViewControllerDelegate? = nil) {
        self.delegate = delegate
        self.isGoodsIn = isGoodsIn
        self.goodsInOutViewModel = GoodsInOutViewModel(homeFeedData: homeFeedData)
    }

    func update(with scannedOrder: Any, isPackGoods: Bool) {
        if goodsInOutItemsViewModel == nil {
            goodsInOutItemsViewModel = GoodsInOutItemsViewModel(order: scannedOrder)
        } else {
            goodsInOutItemsViewModel?.update(with: scannedOrder)
        }
        showScannedGoodsDrawer(with: scannedOrder, isPackGoods: isPackGoods)
    }
}

// MARK: - IBActions
extension ScanGoodsViewController {
    @IBAction private func cancelButtonPressed(_ sender: Any) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func doneButtonPressed(_ sender: Any) {
        guard let scannedOrder = goodsInOutItemsViewModel?.order else {
            return
        }
        if let scanGoodsViewControllerDelegate = delegate {
            scanGoodsViewControllerDelegate.didReturn(updatedOrder: scannedOrder)
            navigationController?.dismiss(animated: true, completion: nil)
        } else {
            viewDelivery(order: scannedOrder)
        }
    }
}

// MARK: - Navigation
extension ScanGoodsViewController {
    func showScannedGoodsDrawer(with order: Any, isPackGoods: Bool) {
        if scannedGoodsViewController == nil {
            // Instantatiate scannedGoodsViewController with new pallette order
            guard let goodsViewController = R.storyboard.scannedGoods.scannedGoodsViewController() else {
                return
            }
            // Drawer view should be presented once, and should remain throughout the entire presentation of self
            let drawerId = goodsViewController.className
            let snapPositions: [DrawerPosition] = [.partiallyOpen, .open, .collapsed]
            let drawerView = self.drawerView(with: goodsViewController, snapPositions: snapPositions, blurEffect: .regular, id:drawerId)
            
            goodsViewController.drawerView = drawerView
            goodsViewController.update(with: order, isPackGoods: isPackGoods)
            scannedGoodsViewController = goodsViewController
            
            load(drawerView: drawerView)
        } else {
            // Update the already presented goodsViewController with new pallette order
            guard let goodsViewController = scannedGoodsViewController else {
                return
            }
            goodsViewController.update(with: order, isPackGoods: isPackGoods)
        }
    }
    
    private func viewDelivery(order: Any) {
        guard let orderDetailViewController = R.storyboard.orderDetail.orderDetailViewController() else {
            return
        }
        orderDetailViewController.set(order: order, delegate: nil, homeFeedData: goodsInOutViewModel.homeFeedData)
        self.navigationController?.pushViewController(orderDetailViewController, animated: true)
    }
}
