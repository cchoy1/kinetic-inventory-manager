//
//  SelectProductsViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 27/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

protocol SelectProductsViewControllerDelegate: class {
    func didReturnSelectedProduct(with productItem: ProductItem)
}

class SelectProductsViewController: KIMBaseViewController, BaseTableViewable {
    @IBOutlet var tableView: FioriBaseTableView!
    @IBOutlet var searchBar: UISearchBar!
    
    private var productItems: [ProductItem]!
    
    var dataProvider: TableDataProviderable!
    var searchActive: Bool = false
    
    weak var delegate: SelectProductsViewControllerDelegate?
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchbar()
        setupViewAppearance()
        dataProvider.reloadTableView()
    }
    
    // MARK: - Setup View Appearance
    func setupViewAppearance() {
        setupNavigationBar(title: R.string.goods.selectProducts(), hidesBackButton: false)
        setupTableView()
    }
    
    // MARK: - Setup Table
    private func setupTableView() {
        setupDefaultTableData()
        setupTableView(with: dataProvider, delegate: dataProvider)
    }
    
    // MARK: - Setup Search Bar
    private func setupSearchbar() {
        searchBar.delegate = self
    }
    
    // MARK: - Refresh
    func refresh() {
        self.showFioriLoadingIndicator()
        PurchaseOrderItemsService.fetchPurchaseOrderProductItems(with: onlineODataProvider, warehouseId: DefaultWarehouseHelper.fetchDefaultWarehouseId()) { (error, results) in
            DispatchQueue.main.async {
                if let _ = error {
                    self.presentOkAlert(message: R.string.generic.errorGenericMessage())
                } else if let products = results {
                    self.update(with: products)
                }
                self.hideFioriLoadingIndicator()
            }
        }
    }
    
    func setupDefaultTableData() {
        if productItems == nil {
            productItems = []
        }
        let viewModel = SelectProductsViewModel(with: productItems)
        dataProvider = SelectProductsDataProvider(with: tableView, viewModel: viewModel, delegate: self)
    }
    
    func update(with products: [ProductItem]) {
        self.productItems = products
        if dataProvider != nil {
            let viewModel = SelectProductsViewModel(with: self.productItems)
            dataProvider.update(with: viewModel)
        }
    }
}

// MARK: - IBActions
extension SelectProductsViewController {
    @IBAction func didTouchUpInsideCancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - DataProviderDelegate
extension SelectProductsViewController: DataProviderDelegate {
    func reloadData() {}
    
    func didSelect(_ field: Field) {
        if let viewModel = dataProvider.viewModel as? SelectProductsViewModel, let productItem = viewModel.fetchProductItem(with: field.id) {
            navigationController?.dismiss(animated: true, completion: {
                self.delegate?.didReturnSelectedProduct(with: productItem)
            })
        }
    }
}

// MARK: - UISearchBarDelegate
extension SelectProductsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let viewModel = dataProvider.viewModel as? SelectProductsViewModel {
            viewModel.update(with: searchText)
            dataProvider.update(with: viewModel)
        }
    }
}
