//
//  SelectProductsViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 27/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

final class SelectProductsViewModel: BaseViewModel {
    private var searchActive: Bool
    private var productItems: [ProductItem]
    private var filteredProductItems: [ProductItem]
    
    init(with productItems: [ProductItem]) {
        self.searchActive = false
        self.productItems = productItems
        self.filteredProductItems = []
        super.init(with: [SectionItem]())
        
        self.update(with: productItems)
    }
}

// MARK: - Public Methods
extension SelectProductsViewModel {
    func deactivateSearch() {
        filteredProductItems = []
        update(with: productItems)
    }
    
    func update(with searchQueryText: String) {
        var feedItems = productItems
        if searchQueryText.isEmpty == false {
            feedItems = SelectProductsViewModel.filterSearchItems(with: feedItems, searchQueryText: searchQueryText)
            filteredProductItems = feedItems
        }
        searchActive = true
        update(with: feedItems)
    }
    
    func fetchProductItem(with sku: String) -> ProductItem? {
        return productItems.first(where: { $0.productPlantType.product == sku })
    }
}

// MARK: - Setup Sections
extension SelectProductsViewModel {
    private func update(with feedItems: [ProductItem]) {
        sections = []
        
        let aphabeticallySortedProducts = SelectProductsViewModel.sortIntoAphabeticalSectionsDictionary(feedItems)
        let sectionTitles = [String](aphabeticallySortedProducts.keys).sorted()
        
        for sectionTitle in sectionTitles {
            if let sectionItems = aphabeticallySortedProducts[sectionTitle] {
                sections.append(SelectProductsViewModel.productsSection(with: sectionItems, title: sectionTitle.uppercased()))
            }
        }
    }
}

// MARK: - Sections
extension SelectProductsViewModel {
    static func productsSection(with productItems: [ProductItem], title: String) -> SectionItem {
        var fields = [Field]()
        for productItem in productItems {
            fields.append(Field(with: productItem.product.product ?? "", title: productItem.productPlantType.product ?? "", detail: productItem.product.product ?? ""))
        }
        return SectionItem(title: title, fields: fields)
    }
    
    static func sortIntoAphabeticalSectionsDictionary(_ productItems: [ProductItem]) -> [String : [ProductItem]] {
        var aplhabeticallySortedDictionary = [String : [ProductItem]]()
        for product in productItems {
            if let productKey = product.product.product?.prefix(1) {
                let key = productKey.uppercased()
                if var value = aplhabeticallySortedDictionary[key] {
                    value.append(product)
                    aplhabeticallySortedDictionary[key] = value
                } else {
                    aplhabeticallySortedDictionary[key] = [product]
                }
            }
        }
        return aplhabeticallySortedDictionary
    }
    
    static func filterSearchItems(with searchFeed: [ProductItem], searchQueryText: String) -> [ProductItem] {
        let filteredSearchFeedData = searchFeed.filter({ (product) -> Bool in
            let productName = product.product.product?.lowercased() ?? ""
            return productName.contains(searchQueryText.lowercased())
        })
        return filteredSearchFeedData
    }
}
