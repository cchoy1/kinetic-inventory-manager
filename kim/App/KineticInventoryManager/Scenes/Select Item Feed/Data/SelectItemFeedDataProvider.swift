//
//  SelectItemFeedDataProvider.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 20/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//
import UIKit

final class SelectItemFeedDataProvider: FioriBaseTableDataProvider {}

// MARK: - Override UITableViewDataSource
extension SelectItemFeedDataProvider {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let field = viewModel.item(for: indexPath) as? Field,
            let cell = dequeCell(with: field, for: tableView) else {
                return UITableViewCell()
        }
        return cell
    }
    
    func dequeCell(with field: Field, for tableView: UITableView) -> UITableViewCell? {
       if let cell = tableView.dequeueReusableCell(withIdentifier: TitleDetailTableViewCell.className) as? TitleDetailTableViewCell {
            cell.update(with: field)
            return cell
        }
        return nil
    }
}
