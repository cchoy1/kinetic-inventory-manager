//
//  SelectItemFeedViewModel.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 20/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

final class SelectItemFeedViewModel: BaseViewModel {
    init(with item: Any) {
        super.init(with: [SectionItem]())
        if let items = item as? [[String: Any]], items.count > 0 {
            sections.append(SelectItemFeedViewModel.section(with: items))
        }
    }
}

// MARK: Sections
extension SelectItemFeedViewModel {
    static func section(with items: [[String: Any]]) -> SectionItem {
        var fields = [Field]()
        for item in items {
            let id = item["id"] as? String ?? ""
            let title = item["title"] as? String ?? ""
            let type = item["type"] as? String ?? ""
            let selectionFieldType = SelectionFieldType(rawValue: type) ?? .none
            var accessory = UITableViewCell.AccessoryType.none
            if let accessoryType = item["accessoryType"] as? UITableViewCell.AccessoryType {
                accessory = accessoryType
            }
            if selectionFieldType == .courier {
                fields.append(
                    Field(with: id, title: title, selectionFieldType: selectionFieldType)
                )
            } else if let date = item["date"] as? Date {
                fields.append(
                    DateField(with: id, date: date, title: title, selectionFieldType: selectionFieldType, accessoryType: accessory)
                )
            } else {
                let detail = item["detail"] as? String ?? ""
                let field = Field(with: id, title: title, detail: detail, selectionFieldType: selectionFieldType)
                field.accessoryType = accessory
                fields.append(field)
            }
        }
        return SectionItem(title: "", fields: fields)
    }
}
