//
//  SelectItemFeedViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 20/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit

protocol SelectItemFeedViewControllerDelegate: class {
    func didSelectItem(with field: Field)
}

protocol SelectCollectionDateDelegate: class {
    func didEndSelection(with collectionDate: CollectionDate)
}

class SelectItemFeedViewController: KIMBaseViewController, BaseTableViewable {
    @IBOutlet var tableView: FioriBaseTableView!
    
    var dataProvider: TableDataProviderable!
    private var viewModel: BaseViewModel!
    
    private var isSelectCollectionDate = false
    private var isSubselectCollectionDate = false
    private var collectionDate = CollectionDate(date: Date(), hasSetTime: false)
    
    private weak var delegate: SelectItemFeedViewControllerDelegate?
    private weak var selectCollectionDateDelegate: SelectCollectionDateDelegate?
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarAppearance()
        setupTableView()
    }
    
    // MARK: - Setup Table
    private func setupTableView() {
        setupDefaultTableData()
        setupTableView(with: dataProvider, delegate: dataProvider)
    }
    
    func setupNavigationBarAppearance() {
        if isSubselectCollectionDate {
            setupNavigationBar(title: title ?? "", isHidden: false)
        } else {
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(closeViewController))
            self.navigationItem.leftBarButtonItem  = cancelButton
        }
    }
    
    // MARK: - Setup Default Table Data
    func setupDefaultTableData() {
        if viewModel == nil {
            viewModel = SelectItemFeedViewModel(with: [:])
        }
        dataProvider = SelectItemFeedDataProvider(with: tableView, viewModel: viewModel, delegate: self)
    }
    
    // MARK: - Reload Data
    func refresh() {
         if dataProvider != nil, viewModel != nil {
            dataProvider.update(with: viewModel)
        }
    }
    
    // MARK: - Update
    func update(with delegate: SelectItemFeedViewControllerDelegate?,
                selectCollectionDateDelegate: SelectCollectionDateDelegate?,
                viewModel: BaseViewModel,
                isSelectCollectionDate: Bool = false,
                isSubselectCollectionDate: Bool = false,
                navigationBarTitle: String) {
        self.viewModel = viewModel
        self.delegate = delegate
        self.isSelectCollectionDate = isSelectCollectionDate
        self.isSubselectCollectionDate = isSubselectCollectionDate
        self.selectCollectionDateDelegate = selectCollectionDateDelegate
        self.title = navigationBarTitle
        if dataProvider != nil {
            dataProvider.update(with: viewModel)
        }
    }
}

// MARK: - IBActions
extension SelectItemFeedViewController {
    @objc func closeViewController() {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - DataProviderDelegate
extension SelectItemFeedViewController: DataProviderDelegate {
    func reloadData() {
        refresh()
    }
    
    func didSelect(_ field: Field) {
        if isSelectCollectionDate {
            if isSubselectCollectionDate {
                delegate?.didSelectItem(with: field)
                navigationController?.popViewController(animated: true)
            } else {
                if field.id.lowercased() == "day" {
                    goToSelectItemFeedViewController(with: TransferStockModel.days(), navigationBarTitle: "Day")
                } else if field.id.lowercased() == "time" {
                    goToSelectItemFeedViewController(with: TransferStockModel.timeSlots(with: collectionDate.date), navigationBarTitle: "Time")
                }
            }
        } else {
            delegate?.didSelectItem(with: field)
            closeViewController()
        }
    }
}

// MARK: - SelectItemFeedViewControllerDelegate
extension SelectItemFeedViewController: SelectItemFeedViewControllerDelegate {
    func didSelectItem(with field: Field) {
        if let dateField = field as? DateField {
            collectionDate.date = dateField.date
            if dateField.type == .time {
                collectionDate.hasSetTime = true
                selectCollectionDateDelegate?.didEndSelection(with: collectionDate)
            }
            let data = TransferStockModel.dateTime(with: collectionDate)
            viewModel = SelectItemFeedViewModel(with: data)
            refresh()
            if dateField.type == .time {
                closeViewController()
            }
        }
    }
}

// MARK: - Navigation
extension SelectItemFeedViewController {
    func goToSelectItemFeedViewController(with items: [[String: Any]], navigationBarTitle: String) {
        guard let selectItemFeedViewController = R.storyboard.selectItemFeed.selectItemFeedViewController() else {
            return
        }
        selectItemFeedViewController.update(with: self,
                                            selectCollectionDateDelegate: nil,
                                            viewModel: SelectItemFeedViewModel(with: items),
                                            isSelectCollectionDate: true,
                                            isSubselectCollectionDate: true,
                                            navigationBarTitle: navigationBarTitle)
        navigationController?.pushViewController(selectItemFeedViewController, animated: true)
    }
}
