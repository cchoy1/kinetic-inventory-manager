//
//  KIMTabBarViewController.swift
//  KineticInventoryManager
//
//  Created by Takomborerwa Mazarura on 09/04/2020.
//  Copyright © 2020 SAP. All rights reserved.
//

import UIKit
import SAPCommon
import SAPFiori
import SAPFoundation
import SAPOData

private enum Tab: String {
    case goodsInOut
    case stock
}

class KIMTabBarViewController: UITabBarController, Storyboarded {
    private var onlineODataProvider: Ec1<OnlineODataProvider>!
    private var displayedTabs: [String] {
        return [Tab.goodsInOut.rawValue, Tab.stock.rawValue]
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    
    // MARK: View Controller Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    // MARK: Update with OdataProvider
    func update(with onlineODataProvider: Ec1<OnlineODataProvider>)  {
        self.onlineODataProvider = onlineODataProvider
        self.setup()
    }
}

extension KIMTabBarViewController {
    private func setup() {
        var viewControllers = [UIViewController]()
        for tabString in displayedTabs {
            guard let tab = Tab(rawValue: tabString) else {
                continue
            }
            switch tab {
            case .goodsInOut:
                let homeFeedViewController = R.storyboard.homeFeed.homeFeedViewController()!
                homeFeedViewController.onlineODataProvider = onlineODataProvider
                let navigationController = UINavigationController(rootViewController: homeFeedViewController)
                navigationController.tabBarItem.title = R.string.goods.goodsInOut()
                navigationController.tabBarItem.image = R.image.van()
                navigationController.tabBarItem.selectedImage = R.image.vanSelected()
                navigationController.navigationBar.prefersLargeTitles = true
                viewControllers.append(navigationController)
            case .stock:
                let stockFeedViewController = R.storyboard.stockFeed.stockFeedViewController()!
                stockFeedViewController.onlineODataProvider = onlineODataProvider
                let navigationController = UINavigationController(rootViewController: stockFeedViewController)
                navigationController.tabBarItem.title = R.string.goods.stock()
                navigationController.tabBarItem.image = R.image.stock()
                navigationController.tabBarItem.selectedImage = R.image.stockSelected()
                navigationController.navigationBar.prefersLargeTitles = true
                viewControllers.append(navigationController)
            }
        }
        self.setViewControllers(viewControllers, animated: false)
    }
}
